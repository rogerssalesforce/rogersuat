<!-- 
    Apttus Config & Pricing
    Criteria.component
     
    @2015-2016 Apttus Inc. All rights reserved.
 -->
 <apex:component controller="Apttus_Config2.CriteriaComponentController"
    access="public" 
    allowDML="true"
    layout="block">

    <apex:attribute name="componentKey" 
        type="String" 
        assignTo="{!Key}" 
        description="The key given to this component so the page can easily get access to it" />

    <apex:attribute name="thePageController" 
        type="Apttus_Config2.CriteriaPageControllerBase" 
        assignTo="{!pageController}" 
        required="true" 
        description="The controller for the page." />

    <apex:attribute name="theDataSources" 
        type="SelectOption[]" 
        assignTo="{!dataSources}" 
        required="true" 
        description="List of Data Sources" />

    <apex:attribute name="theFilterSpec" 
                    assignTo="{!filterSpec}" 
                    type="Apttus_Config2.SearchFilter" 
                    access="public"
                    description="Filter spec object from Manager page to get/set Search Filter information." 
                    required="true" />
    <script>
        var j$ = jQuery.noConflict();

        function esc(myid) {
           return '#' + myid.replace(/(:|\.)/g,'\\\\$1');
        }

        j$(document).ready(function() {
            var $metricType = j$('#rollupMetricTypeField');

            j$(esc('{!$Component.incentiveForm.ddlRollupFunction}')).bind("change", function() {
                if(j$(this).val() === 'SUM') {
                    $metricType.show();
                } else {
                    $metricType.hide();
                }
            });
        });

        function displaySpinner(){
            j$('.overlay').show();
            disableScroll();
        }

        function hideSpinner() {
            $overlay.hide();
            enableScroll();
        }

        var keys = {37: 1, 38: 1, 39: 1, 40: 1};

        function preventDefault(e) {
          e = e || window.event;
          if (e.preventDefault)
              e.preventDefault();
          e.returnValue = false;  
        }

        function preventDefaultForScrollKeys(e) {
            if (keys[e.keyCode]) {
                preventDefault(e);
                return false;
            }
        }

        function disableScroll() {
          if (window.addEventListener) // older FF
              window.addEventListener('DOMMouseScroll', preventDefault, false);
          window.onwheel = preventDefault; // modern standard
          window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
          window.ontouchmove  = preventDefault; // mobile
          document.onkeydown  = preventDefaultForScrollKeys;
        }

        function enableScroll() {
            if (window.removeEventListener)
                window.removeEventListener('DOMMouseScroll', preventDefault, false);
            window.onmousewheel = document.onmousewheel = null; 
            window.onwheel = null; 
            window.ontouchmove = null;  
            document.onkeydown = null;  
        }
        
    </script>
    <style>
    .overlay{
        display:none;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 1000000px;
        z-index: 10;
        background-color: rgba(0,0,0,0.5); /*dim the background*/
    }
    .overlay img{
        position: fixed;
        top: 50%;
        left: 50%;
    }
    .ui-autocomplete-loading { 
        background: white url(/img/loading32.gif) right center no-repeat;
        background-size:15px 15px; 
    }
    .row-number{
        width: 1%;
        font-weight: bold;
    }

    .incentiveCriteriaPanel input[type="text"], .incentiveCriteriaPanel select{
        width:120px;
    }
    
    .incentiveCriteriaPanel .criteria-expression{
        margin-top: 20px;
    }
    .incentiveCriteriaPanel .criteria-expression h3{
        font-size: 16px;
        color: #555555;
    }
    .incentiveCriteriaPanel .criteria-expression input{
        width: 500px;
    }
    .incentiveCriteriaPanel .row-number{
        font-weight: bold;
        padding-right: 10px;
    }
    .incentiveCriteriaPanel input[type="text"].hidden-text{
        display: none;
    }
    .incentiveCriteriaPanel div.field-value-cell{
        position: relative;
        margin-top: 5px;
    }
    .incentiveCriteriaPanel div.field-value-cell .spy-glass{
      position: absolute;
      bottom: 5px;
      right: 10px;
      color: #079CCE;
      font-size: 20px;
    }
    .incentiveCriteriaPanel select.field-value-multi{
          width: 100%;
          height: 55px;
    }
    .incentiveCriteriaPanel .field-value-cell input, .incentiveCriteriaPanel .field-value-cell select{
        width:240px;
    }
    .incentiveCriteriaPanel .multi-select-widget {
        width: 240px;
        height: 31px;
    }
    .incentiveCriteriaPanel .multi-select-widget input[type="text"] {
        margin-top: 0;
        width: 147px;
    }

    </style>
    <script>
    var LHSOjbectNames = [],
        LHSFields = [],
        operators = [],
        RHSOjbectNames = [],
        RHSFields = [],
        stepSelected,
        $sectionContainer,
        $overlay = j$('.overlay');

    j$(document).ready(function() {
        $overlay = j$('.overlay');
        $sectionContainer = j$("#incentiveCriteria");

        init();

        currentIncentiveComponent = "criteria-nav";
        j$('.main ol li.criteria-nav').addClass("active");
        j$('body').off('click.criteria');
        j$('body').on('click.criteria', '.main ol li', function(){
            stepSelected = j$(this).text();
            displaySpinner();
            saveCriteriaAndRemoveBlanksStep();
        });
    });
    </script>
    
    <apex:outputPanel layout="block" id="incentiveCriteriaPanel" styleClass="incentiveCriteriaPanel inner-container-maximize">
        <div id="incentiveCriteria" class="section-advanced-criteria">
            <apex:outputPanel id="idPredicateTable">
            <apex:pageMessages id="msgs" />
            <script>
                j$(document).ready(function(){

                    j$( ".sort-select" ).each(function( index ) {
                        var my_options = j$(this).children();
                        var selected = j$(this).val(); /* preserving original selection, step 1 */

                        my_options.sort(function(a,b) {
                            if (a.text > b.text) return 1;
                            else if (a.text < b.text) return -1;
                            else return 0
                        })

                        j$(this).empty().append( my_options );
                        j$(this).val(selected);

                    });

                    j$(".add-row").on("click", function() {
                        var index = j$(this).closest('tr').index();
                        displaySpinner();
                        addRow(index);
                    });

                    j$(".remove-row").on("click", function() {
                        var index = j$(this).closest('tr').index();
                        displaySpinner();
                        removeRow(index);
                    });

                    j$(".lhs-ojbect-name").on("change", function() {
                        var index = j$(this).closest('tr').index();
                        LHSOjbectNames[index] = j$(this).val();

                        if(j$(this).val().indexOf('_childfilter') != -1){
                            j$('.LHSChildFilterName' + index).val(j$(this).val().replace('_childfilter'));

                        }
                        displaySpinner();
                        refreshTableAndClearField('left', index);
                    });

                    j$(".lhs-field-name").on("change", function() {
                        var index = j$(this).closest('tr').index();
                        LHSFields[index] = j$(this).val();
                        j$(this).siblings('input').val(j$(this).val());
                        displaySpinner();
                        updateFieldTypeLabel(index);

                    });


                    j$(".operator").on("change", function() {
                        var index = j$(this).closest('tr').index();
                        operators[index] = j$(this).val();
                        displaySpinner();
                        refreshTable();

                    });

                    j$(".rhs-object-name").on("change", function() {
                        var index = j$(this).closest('tr').index();
                        RHSOjbectNames[index] = j$(this).val();

                        if(j$(this).val().indexOf('_childfilter') != -1){
                            j$('.RHSChildFilterName' + index).val(j$(this).val().replace('_childfilter'));

                        }
                        displaySpinner();
                        refreshTableAndClearField('right', index);

                    });

                    j$(".rhs-field-name").on("change", function() {
                        var index = j$(this).closest('tr').index();
                        RHSFields[index] = j$(this).val();
                        j$(this).siblings('input').val(j$(this).val());
                        displaySpinner();
                        updateRHSFieldTypeLabel(index);

                    });

                    j$(".field-value").on("change", function() {
                        j$(this).closest('.field-value-cell').find('.hidden-text.field-value').val(j$(this).val());

                    });

                    j$('.field-value-multi').on("change", function() {
                        var fieldVal = '';
                        j$(this).children('option:selected').each(function() {
                            fieldVal += j$(this).val() + ',';
                        
                        });
                        fieldVal = fieldVal.substring(0, fieldVal.length - 1);
                        console.log(j$(this).closest('.field-value'));
                        j$(this).parent().siblings('.field-value').val(fieldVal);

                    });

                    j$(".value-type").on("change", function() {
                        var index = j$(this).closest('tr').index() + 1;
                        if(j$(this).val().indexOf('Offset') != -1){
                            j$(this).closest('tr').find('.field-value-cell input,.field-value-cell select').hide();
                            j$('.offsetValue' + index).show();
                            j$('.fieldValue' + index).val(':' + j$(this).val());
                            
                        }else{
                            j$(this).closest('tr').find('.shown-text').show();
                            j$('.offsetValue' + index).hide();
                            j$('.offsetValue' + index).val('');
                            j$('.hiddenFieldValue' + index).val(j$('.hiddenFieldValue' + index).val().replace(':' + j$('.hiddenFieldValue' + index).val(),''));
                        }
                    });
                });
            </script>
            <table class="criteria" style="width:100%;">
                <thead>
                    <th></th>
                    <th>{!$Label.DataSource}</th>
                    <th>{!$Label.Field}</th>
                    <th>{!$Label.Operator}</th>
                    <th>{!$Label.DataSource}</th>
                    <th>{!$Label.Field}</th>
                    <th>{!$Label.ValueType}</th>
                    <th>{!$Label.Value}</th>
                    <th></th>
                </thead>
                <tbody>
               
                <apex:variable value="{!1}" var="rowNum"/>
                <apex:repeat value="{!predicates}" var="predicate">
                <tr>
                    <td class="row-number">
                        {!rowNum}.
                        <script>
                            j$(document).ready(function(){
                                if(LHSOjbectNames[{!rowNum-1}] != undefined){
                                    j$('.LHSOjbectName{!rowNum}').val(LHSOjbectNames[{!rowNum-1}]);
                                }else{
                                    j$('.LHSOjbectName{!rowNum}').val('{!predicate.LHSSObjectName}');
                                    LHSOjbectNames[{!rowNum-1}] = '{!predicate.LHSSObjectName}';
                                }

                                if(LHSFields[{!rowNum-1}] != undefined){
                                    j$('select.LHFieldName{!rowNum}').val(LHSFields[{!rowNum-1}]);
                                }else{
                                    j$('select.LHFieldName{!rowNum}').val('{!predicate.FieldName}');
                                    LHSFields[{!rowNum-1}] = '{!predicate.FieldName}';
                                }

                                if(operators[{!rowNum-1}] != undefined){
                                    j$('.operator{!rowNum}').val(operators[{!rowNum-1}]);
                                }else{
                                    j$('.operator{!rowNum}').val('{!predicate.CompOper}');
                                    operators[{!rowNum-1}] = '{!predicate.CompOper}';
                                }


                                if(RHSOjbectNames[{!rowNum-1}] != undefined){
                                    j$('.RHSOjbectName{!rowNum}').val(RHSOjbectNames[{!rowNum-1}]);
                                }else{
                                    j$('.RHSOjbectName{!rowNum}').val('{!predicate.RHSSObjectName}');
                                    RHSOjbectNames[{!rowNum-1}] = '{!predicate.RHSSObjectName}';
                                }

                                if(RHSFields[{!rowNum-1}] != undefined){
                                    j$('select.RHFieldName{!rowNum}').val(RHSFields[{!rowNum-1}]);
                                }else{
                                    j$('select.RHFieldName{!rowNum}').val('{!predicate.RHSFieldName}');
                                    RHSFields[{!rowNum-1}] = '{!predicate.RHSFieldName}';
                                }

                                RHSFieldName = '{!predicate.FieldValue}';
                                
                                if('{!predicate.LHSSObjectName}' != '' && '{!predicate.RHSSObjectName}' == '') {
                                    var $ddlValueType = j$('.valueType{!rowNum}');
                                    $ddlValueType.val('{!PicklistValueTypeConstant}');
                                    $ddlValueType.attr('disabled', true);
                                } else if('{!predicate.RHSFieldName}' != '' && '{!predicate.RHSFieldName}' != ' ' && RHSFieldName.indexOf(':Offset') == -1) {
                                    j$('.valueType{!rowNum}').val('{!PicklistValueTypeFieldValue}');
                                
                                } else if('{!predicate.OffsetValue}' != '' && '{!predicate.FieldValue}'.indexOf(':Offset') != -1){
                                    j$('.valueType{!rowNum}').val('{!PicklistValueTypeOffsetMonthsPlus}');
                                    j$('.offsetValue{!rowNum}').closest('tr').find('.field-value-cell input,.field-value-cell select').hide();
                                    j$('.offsetValue{!rowNum}').show();
                                
                                } else if('{!predicate.FieldValue}' != ''){
                                    j$('.valueType{!rowNum}').val('{!PicklistValueTypeConstant}');
                                }

                                if('{!predicate.FieldValue}'.indexOf(':Offset') != -1){
                                    j$('.valueType{!rowNum}').val('{!predicate.FieldValue}'.replace(':',''));
                                }
                                
                                j$('.LHSOjbectName{!rowNum}').val('{!predicate.LHSSObjectName}');
                                j$('.LHFieldName{!rowNum}').val('{!predicate.FieldName}');
                                j$('.operator{!rowNum}').val('{!predicate.CompOper}');
                                j$('.RHSOjbectName{!rowNum}').val('{!predicate.RHSSObjectName}');
                                j$('.RHFieldName{!rowNum}').val('{!predicate.RHSFieldName}');
                            });
                        </script>
                    </td>
                    <td>
                        <apex:selectList styleClass="lhs-ojbect-name LHSOjbectName{!rowNum} sort-select" value="{!predicate.LHSSObjectName}" multiselect="false" size="1">
                            <apex:selectOptions value="{!dataSources}"/>
                        </apex:selectList>
                        <apex:inputText styleClass="lhs-filter-name LHSChildFilterName{!rowNum} hidden-text" value="{!predicate.LHSChildFilterName}" />
                    </td>
                    <td>
                        <apex:selectList styleClass="lhs-field-name LHFieldName{!rowNum} sort-select" value="{!predicate.FieldName}" multiselect="false" size="1">
                            <apex:selectOptions value="{!predicate.LHSSObjectFieldOptions}"/>
                        </apex:selectList>
                    </td>
                    <td>
                        <apex:selectList styleClass="operator operator{!rowNum}" value="{!predicate.CompOper}" multiselect="false" size="1">
                            <apex:selectOptions value="{!predicate.operators}"/>
                        </apex:selectList>
                    </td>
                    <td>
                        <apex:selectList styleClass="rhs-object-name RHSOjbectName{!rowNum} sort-select" value="{!predicate.RHSSObjectName}" multiselect="false" size="1">
                            <apex:selectOptions value="{!dataSources}"/>
                            <apex:selectOptions value="{!childFilters}"/>
                        </apex:selectList>
                        <apex:inputText styleClass="rhs-field-name RHSChildFilterName{!rowNum} hidden-text" value="{!predicate.RHSChildFilterName}" />
                    </td>
                    <td>
                        <apex:selectList styleClass="rhs-field-name RHFieldName{!rowNum} sort-select" value="{!predicate.RHSFieldName}" multiselect="false" size="1">
                            <apex:selectOptions value="{!predicate.RHSSObjectFieldOptions}"/>
                        </apex:selectList>
                    </td>
                    <td>
                        <apex:selectList rendered="{!predicate.displayValueType}" styleClass="value-type valueType{!rowNum} sort-select" multiselect="false" size="1">
                                <apex:selectOptions value="{!predicate.valueTypes}"/>
                        </apex:selectList>
                    </td>
                    <td>
                    <div class="field-value-cell">
                        <apex:outputPanel rendered="{!predicate.displayTextInput}">
                            <apex:inputText value="{!predicate.FieldValue}" styleClass="field-value fieldValue{!rowNum} shown-text" />
                        </apex:outputPanel>
                        <apex:outputPanel rendered="{!predicate.displayMultiPicklist || predicate.displaySelectList || predicate.displaySearchInput}">
                            <input id="txtMultiSelectValues{!rowNum}" class="fieldValue{!rowNum} search-field field-value shown-text" html-placeholder="{!$Label.Search}" type="text" />
                            <script type="text/javascript">
                                j$(document).ready(function() {
                                    var objectList,
                                        fieldOptions = [],
                                        $inputCtrl = j$(".hdnMultiSelectValues{!rowNum}"),
                                        dataSourceMethod = function(request, response) {
                                                console.log(response);
                                                {!NSPrefix}CriteriaComponentController.searchFieldValue(
                                                    j$('.LHSOjbectName{!rowNum}').val(),
                                                    j$('.LHFieldName{!rowNum}').val(), 
                                                    j$('.fieldValue{!rowNum}').val(), 
                                            function(result, event){
                                                if(event.type == 'exception') {
                                                    console.log(event.message);
                                                } else {
                                                    console.log(result);
                                                    response(normalizeData(result, "Name", "Id"));
                                                }
                                            });
                                        },
                                        manuallyAddingItems = true,
                                        populateMultiPickSearchControl = function($inputCtrl, isMultipick, searchWidget, currentRow) {
                                            var savedValues = ('{!HTMLENCODE(predicate.SearchFieldValues)}' == '') ? '' : {!predicate.SearchFieldValues};
                                            if(savedValues != '' && savedValues != undefined){
                                                if(isMultipick) {
                                                    if({!predicate.isReference}){
                                                        for(value in savedValues){
                                                            if(savedValues[value].Name != undefined && savedValues[value].Id != undefined){
                                                                searchWidget.multipickSearch("addSelectedItem", {label:savedValues[value].Name, value:savedValues[value].Id});
                                                            }
                                                        }
                                                    }else{
                                                        for(i = 0; i < savedValues.length; i++){
                                                            searchWidget.multipickSearch("addSelectedItem", {label:savedValues[i], value:savedValues[i]});
                                                        }
                                                    }
                                                } else {
                                                    if({!predicate.isReference}){
                                                        for(value in savedValues){
                                                            if(savedValues[value].Name != undefined && savedValues[value].Id != undefined){
                                                                j$(".fieldValue" + currentRow).val(savedValues[value].Name);
                                                            }
                                                        }
                                                    }else{
                                                        j$(".fieldValue" + currentRow).val(savedValues[0]);
                                                    }
                                                    
                                                }
                                            }
                                        };

                                    <apex:repeat value="{!predicate.LHSSFieldSelectOptions}" var="fieldOption">
                                        <apex:outputPanel rendered="{!NOT(fieldOption.Value == ' ')}" layout="none">
                                        fieldOptions.push({label:'{!JSENCODE(fieldOption.Label)}', value:'{!JSENCODE(fieldOption.Value)}'});
                                        </apex:outputPanel>
                                    </apex:repeat>

                                    var fieldMultiPickList = j$("#txtMultiSelectValues{!rowNum}").multipickSearch({
                                            lblResultsItemsTitle:"{!$Label.AvailableItems}",
                                            lblSelectedItemsTitle:"{!$Label.SelectedItems}",
                                            lblClose:"{!$Label.Close}",
                                            lblNoResults:"{!$Label.NoResultsAvailable}",
                                            multipick: {!CONTAINS(predicate.CompOper,'in')},
                                            dataSourceFunction:function(request, response) {
                                                if({!predicate.displaySearchInput}){
                                                    dataSourceMethod(request, response);
                                                } else {
                                                    response(searchItems(request.term, fieldOptions));    
                                                }
                                            },
                                            defaultResults: function(request, response) {
                                                if({!predicate.displaySearchInput}){
                                                    request.term = "";
                                                    dataSourceMethod(request, response);
                                                } else {
                                                    response(fieldOptions);    
                                                }
                                            },
                                            onItemSelected: function(settings, item) {
                                                if(manuallyAddingItems) return;

                                                if({!CONTAINS(predicate.CompOper,'in')}){
                                                    $inputCtrl.val(updateFieldOptions($inputCtrl.val(), item.value, ",", true));
                                                } else {
                                                    $inputCtrl.val(item.value);
                                                }
                                            },
                                            onDelete: function(settings, item) {
                                                if({!CONTAINS(predicate.CompOper,'in')}){
                                                    $inputCtrl.val(updateFieldOptions($inputCtrl.val(), item.value, ",", false));
                                                } else {
                                                    $inputCtrl.val('');
                                                }
                                            }
                                        });

                                    manuallyAddingItems = true;
                                    populateMultiPickSearchControl($inputCtrl, {!CONTAINS(predicate.CompOper,'in')}, fieldMultiPickList, {!rowNum});
                                    manuallyAddingItems = false;

                                });
                            </script>

                            <apex:inputText id="hdnMultiSelectValues" style="display:none;" styleClass="hdnMultiSelectValues{!rowNum}" value="{!predicate.FieldValue}" />
                        </apex:outputPanel>
                        <apex:inputText value="{!predicate.OffsetValue}" styleClass="hidden-text hiddenOffsetValue{!rowNum} offsetValue{!rowNum}" />
                    </div>
                    </td>
                    <td>
                        <a href="javascript:void(0);" class="circle-btn cancel-btn remove-row"><i class="fa fa-times"></i></a>
                        <a href="javascript:void(0);" class="circle-btn add-btn add-row"><i class="fa fa-plus"></i></a>
                    </td>
                    
                </tr>
                <apex:variable var="rowNum" value="{!rowNum + 1}"/>
                </apex:repeat>
                </tbody>
            </table>
            <div class="criteria-expression">
                <h3>{!$Label.CriteriaExpression}</h3>
                <apex:inputText value="{!condExpr}" />
            </div>
            </apex:outputPanel>
        </div>
    </apex:outputPanel>  

    <apex:outputPanel id="dummy" />
    <apex:actionFunction name="saveCriteria" action="{!saveCriteria}" reRender="dummy" oncomplete="hideSpinner();" status="loadingStatus"/>
    <apex:actionFunction name="saveCriteriaAndRemoveBlanksSave" action="{!saveCriteriaAndRemoveBlanks}" oncomplete="hideSpinner();" reRender="dummy"/>
    <apex:actionFunction name="saveCriteriaAndRemoveBlanksStep" action="{!saveCriteriaAndRemoveBlanks}" reRender="dummy" oncomplete="gotoSelectedStep(stepSelected);hideSpinner();j$('body').off('click.criteria');" status="loadingStatus"/>

    <apex:actionFunction name="init" reRender="incentiveCriteriaPanel" action="{!init}" oncomplete="hideSpinner();" status="loadingStatus" />
    <apex:actionFunction name="updateFieldTypeLabel" action="{!updateFieldTypeLabel}" reRender="idPredicateTable" oncomplete="hideSpinner();" status="loadingStatus">
        <apex:param name="number" value=""/>
    </apex:actionFunction>
    <apex:actionFunction name="updateRHSFieldTypeLabel" action="{!updateRHSFieldTypeLabel}" reRender="idPredicateTable" oncomplete="hideSpinner();" status="loadingStatus">
        <apex:param name="number" value=""/>
    </apex:actionFunction>
    <apex:actionFunction name="refreshTable" reRender="idPredicateTable" status="loadingStatus" />
    <apex:actionFunction name="refreshTableAndClearField" action="{!clearField}" reRender="idPredicateTable" oncomplete="hideSpinner();" status="loadingStatus">
        <apex:param name="leftOrRight" value=""/>
        <apex:param name="number" value=""/>
    </apex:actionFunction>
    <apex:actionFunction name="addRow" action="{!addRow}" reRender="idPredicateTable" oncomplete="saveCriteria();" status="loadingStatus">
        <apex:param name="number" value=""/>
    </apex:actionFunction>
    <apex:actionFunction name="removeRow" action="{!removeRow}" reRender="idPredicateTable" oncomplete="saveCriteria();" status="loadingStatus">
        <apex:param name="number" value=""/>
    </apex:actionFunction>
    <apex:actionStatus id="loadingStatus" onstart="displaySpinner();" onstop="hideSpinner();"/>
    <div class="overlay"><img src="/img/loading32.gif" align="loading..." /></div>
</apex:component>