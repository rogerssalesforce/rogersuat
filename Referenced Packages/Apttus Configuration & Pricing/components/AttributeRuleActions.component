<!--
    Apttus Config & Pricing
    AttributeRuleAction
     
    @2015-2016 Apttus Inc. All rights reserved.

 -->

<apex:component id="idAttributeRuleActionComponent" 
				controller="Apttus_Config2.AttributeRuleActionController"
				extensions="Apttus_Config2.ProductAttributeRemoteActionController" 
                access="public" 
                allowDML="true" 
                layout="block">

    <script type="text/javascript">
	    j$(document).ready(function() {
	    	j$('#idActionsSection').tabs();
	    	return false;
	    });
    </script>

    <apex:attribute name="attributeRuleActionsSO"
    				assignTo="{!attributeRuleActions}"
    				access="public"
    				required="true"
    				type="Apttus_Config2__ProductAttributeRuleAction__c[]"
    				description="Product Attribute Rule object from attribute rule page to get/set attribute rule criteria." />

	<div class="formbg">
		<div id="idActionsSection" class="section actionsection">
	        <div class="header">
				<h3 class="collapsible"><i class="fa fa-caret-down"></i>{!$Label.Action}</h3>
	        </div> 
	        <div class="innerContent">
				<ul class="tab-links">
			        <li class="active"><a href="#section-basic-actions">{!$Label.Basic}</a></li>
			        <li><a href="#section-advanced-actions">{!$Label.Advanced}</a></li>
			    </ul>

		        <script type="text/javascript">
		        	var productFamilyRetrievalMethod = function(request, response) {
		                    Visualforce.remoting.Manager.invokeAction(
                                '{!$RemoteAction.ProductAttributeRemoteActionController.searchProductFamily}',	
                                request.term, 
                                function(result, event) {
                                    if(event.type == 'exception') {
                                          console.log(event.message);
                                    } else {
                                         productFamilyList = result;
                                         response(result);
                                    }
                                }
                            );
	                	},
						productsRetrievalMethod = function(request, response) {
							Visualforce.remoting.Manager.invokeAction(
								'{!$RemoteAction.ProductAttributeRemoteActionController.searchProductsNoPriceList}', 
								request.term,
					            $actionProdFamilyHiddenCtrl.val(), 
					            'in', 
					            function(result, event) {
					                if(event.type == 'exception') {
					                    console.log(event.message);
					                } else {
					                    response(normalizeData(result, "Name", "Id"));
					                }
					            }
				            );
			        	},
						productGroupsRetrievalMethod = function(request, response) {
			                Visualforce.remoting.Manager.invokeAction(
			                    '{!$RemoteAction.ProductAttributeRemoteActionController.searchProductGroups}', 
			                    request.term,
                                function(result, event) {
                                    if(event.type == 'exception') {
                                        console.log(event.message);
                                    } else {
                                        response(normalizeData(result, "Name", "Id"));
                                    }
                                }
                            );
			            },
			            $actionToProductsMap = JSON.parse("{!JSENCODE(actionToProductsJSON)}"),
						$actionToProductGroupsMap = JSON.parse("{!JSENCODE(actionToProductGroupsJSON)}"),
			            targetAttributesRetrievalMethod = function(request, response) {
			            	 Visualforce.remoting.Manager.invokeAction(
			            	 	'{!$RemoteAction.ProductAttributeRemoteActionController.searchTargetAttributes}', 
			            	 	request.term, 
	                        	function(result, event) {
		                            if (event.type == 'exception') {
		                                console.log(event.message);
		                            } else {
		                                response(result);
		                            }
	                        	}
                        	);
			            },
			            setAttributeLabel = function(fieldValue, picklistEl) {
		            	 	Visualforce.remoting.Manager.invokeAction(
			            	 	'{!$RemoteAction.ProductAttributeRemoteActionController.getLabelFromValue}', 
			            	 	fieldValue, 
	                        	function(result, event) {
		                            if (event.type == 'exception') {
		                                console.log(event.message);
		                            } else {
		                                if (result !== null) {
									      	picklistEl.val(result[0].label);
		                                }
		                            }
	                        	}
                        	);
			            };
		        </script>

		        <div id="section-basic-actions" class="paddingLR20 innerContent">
		            <apex:outputPanel id="idPredicateTable">
		            	<script type="text/javascript">
		            		j$(document).ready(function() {
					        	j$('.add-action-row').on("click", function() {
					        		var index = j$(this).closest('tr').index();
					        		displaySpinner();
					        		addActionRow(index);
					        	});

					        	j$('.remove-action-row').on("click", function() {
					        		var index = j$(this).closest('tr').index();
					        		displaySpinner();
					        		removeActionRow(index);
					        	});
					        });
		            	</script>
			            <table class="criteria" style="width:70%;">
		            		<thead>
		            			<th></th>
		            			<th>{!$Label.TargetAttribute}</th>
		            			<th>{!$Label.Action} {!$Label.Type}</th>
		            			<th>{!$Label.ValueExpression}</th>
		                        <th></th>
		            		</thead>
			           		<tbody>
				           		<apex:variable value="{!1}" var="rowNum"/>
					           	<apex:repeat value="{!attributeRuleActions}" var="action">
					           		<tr id="attribute-action-row-{!action.Id}">
						           		<td class="row-number">
				            				{!rowNum}.
				            			</td>
				            			<td>
					            			<!-- these two fields used for type ahead functionality -->
											<apex:inputText id="idBasicActionTargetAttr" onblur="setAdvancedTargetAttr(this);" />
											
											<!-- this single, hidden field is shared between the Basic and Advanced tabs -->
											<apex:inputHidden id="idActionTargetAttrHidden" value="{!action.Apttus_Config2__Field__c}" />

					            			<script type="text/javascript">
					            				/* 
					            				 * Sets the value of the Advanced tab target field, so that 
					            				 * both Basic and Advanced remain in sync. 
					            				 * The same applies to the functions:
					            				 *   - setAdvancedActionType(actionTypeEl)
					            				 *   - setAdvancedActionValueExpr(valueExpressionEl) 
					            				 */
					            				function setAdvancedTargetAttr(targetAttrEl) {
				            						var targetAttrCellIdRegex = ':[0-9]+:[A-Z]*[a-z]*';
				            						targetAttrIdEndsWith = targetAttrEl.id.substr(targetAttrEl.id.search(targetAttrCellIdRegex)).replace(/:/g, '\\:');
				            						j$("[id$=" + targetAttrIdEndsWith.replace("{!$Label.Basic}", "{!$Label.Advanced}") + "]").val(
					            						j$("[id$=" + targetAttrIdEndsWith.replace("{!$Label.Advanced}", "{!$Label.Basic}") + "]").val()
					            					);
					            				}
					            				
					            				// this variable is shared between the Basic and Advanced tabs (reference to hidden input)
					            				var $actionTargetAttributeHidden = j$("[id$=" + ({!rowNum}-1) + "\\:idActionTargetAttrHidden]");
				            				    
				            				    var $basicActionTargetAttributeMultipickList = j$("[id$=" + ({!rowNum}-1) + "\\:idBasicActionTargetAttr]").multipickSearch({
				            				    	lblResultsItemsTitle: "{!$Label.AvailableItems}",
								                    lblSelectedItemsTitle: "{!$Label.SelectedItems}",
								                    lblClose: "{!$Label.Close}",
								                    lblNoResults: "{!$Label.NoResultsAvailable}",
								                    multipick: false, 
								                    showMenu: true,
								                    dataSourceFunction: function (request, response) { 
								                    	targetAttributesRetrievalMethod(request, response);
								                    },
													defaultAvailableView: true,
								                    defaultResults: function (request, response) { 
								                        request.term = "";
								                        targetAttributesRetrievalMethod(request, response);
								                    },
								                    onItemSelected: function (settings, item) {
				                                    	// set value of hidden field
				                                    	$actionTargetAttributeHidden = j$("[id$=" + ({!rowNum}-1) + "\\:idActionTargetAttrHidden]");
				                                    	$actionTargetAttributeHidden.val(item.value);
								                    },
								                    onDelete: function (settings, item) {
								                    	$actionTargetAttributeHidden = j$("[id$=" + ({!rowNum}-1) + "\\:idActionTargetAttrHidden]");
								                    	$actionTargetAttributeHidden.val("");
								                    }  
							                    });  
												
											    // set the selected item on type ahead field when opening or refreshing page
											    $actionTargetAttributeHidden = j$("[id$=" + ({!rowNum}-1) + "\\:idActionTargetAttrHidden]");
											    setAttributeLabel($actionTargetAttributeHidden.val(), 
					            					       		  $basicActionTargetAttributeMultipickList);
					            			</script>
				            			</td>
				            			<td>
					            			<apex:inputField id="idBasicActionType" value="{!action.Apttus_Config2__Action__c}" onchange="setAdvancedActionType(this);" />
					            			<script type="text/javascript">
					            				function setAdvancedActionType(actionTypeEl) {
				            						var actionTypeCellIdRegex = ':[0-9]+:[A-Z]*[a-z]*';
				            						actionTypeIdEndsWith = actionTypeEl.id.substr(actionTypeEl.id.search(actionTypeCellIdRegex)).replace(/:/g, '\\:');
				            						j$("[id$=" + actionTypeIdEndsWith.replace("{!$Label.Basic}", "{!$Label.Advanced}") + "]").val(
					            						j$("[id$=" + actionTypeIdEndsWith.replace("{!$Label.Advanced}", "{!$Label.Basic}") + "]").val()
					            					);
				            					}
					            			</script>            			
					            		</td>
					            		<td>
				            				<apex:inputText id="idBasicActionValueExpr" value="{!action.Apttus_Config2__ValueExpression__c}" html-placeholder="Enter Value" onchange="setAdvancedActionValueExpr(this);" />
				            				<script type="text/javascript">
					            				function setAdvancedActionValueExpr(valueExpressionEl) {
				            						var valueExprCellIdRegex = ':[0-9]+:[A-Z]*[a-z]*';
				            						valueExprIdEndsWith = valueExpressionEl.id.substr(valueExpressionEl.id.search(valueExprCellIdRegex)).replace(/:/g, '\\:');
				            						j$("[id$=" + valueExprIdEndsWith.replace('Basic', 'Advanced') + "]").val(
					            						j$("[id$=" + valueExprIdEndsWith.replace('Advanced', 'Basic') + "]").val()
					            					);
					            				}
				            				</script>
				            			</td>
				            			<td>
					            			<a href="javascript:void(0);" class="circle-btn cancel-btn remove-action-row"><i class="fa fa-times"></i></a>
				            			</td>
				            			<td>
					                        <a href="javascript:void(0);" class="circle-btn add-btn add-action-row"><i class="fa fa-plus"></i></a>
				            			</td>
			                        </tr>
			                        <apex:variable var="rowNum" value="{!rowNum + 1}"/>
					           	</apex:repeat>
				           	</tbody>
			           	</table>
		           	</apex:outputPanel>
				</div>
				<div id="section-advanced-actions" class="paddingLR20">
					<apex:outputPanel id="idAdvancedActionTable">
						<table class="criteria" style="width:70%;">
							<thead>
								<th></th>
								<th>{!$Label.TargetAttribute}</th>
		            			<th>{!$Label.ProductFamily}</th>
		            			<th>{!$Label.Product}</th>
		            			<th>{!$Label.ProductGroup}</th>
		            			<th>{!$Label.ActionTypes}</th>
		            			<th>{!$Label.ValueExpression}</th>
		            			<th></th>
		            		</thead>
		            		<tbody>
		            			<apex:variable value="{!1}" var="rowNum"/>
					           	<apex:repeat value="{!attributeRuleActions}" var="action">
					           		<tr id="advanced-attribute-action-row-{!action.Id}">
						           		<td class="row-number">
				            				{!rowNum}.
				            			</td>
				            			<td>
											<apex:inputText id="idAdvancedActionTargetAttr" onblur="setBasicTargetAttr(this);" />

					            			<script type="text/javascript">
					            				function setBasicTargetAttr(targetAttrEl) {
				            						var targetAttrCellIdRegex = ':[0-9]+:[A-Z]*[a-z]*';
				            						targetAttrIdEndsWith = targetAttrEl.id.substr(targetAttrEl.id.search(targetAttrCellIdRegex)).replace(/:/g, '\\:');
													j$("[id$=" + targetAttrIdEndsWith.replace('Advanced', 'Basic') + "]").val(
					            						j$("[id$=" + targetAttrIdEndsWith.replace('Basic', 'Advanced') + "]").val()
					            					);
					            				}

					            				var $advancedActionTargetAttributeMultipickList = j$("[id$=" + ({!rowNum}-1) + "\\:idAdvancedActionTargetAttr]").multipickSearch({
				            				    	lblResultsItemsTitle: "{!$Label.AvailableItems}",
								                    lblSelectedItemsTitle: "{!$Label.SelectedItems}",
								                    lblClose: "{!$Label.Close}",
								                    lblNoResults: "{!$Label.NoResultsAvailable}",
								                    multipick: false, 
								                    showMenu: true,
								                    dataSourceFunction: function (request, response) { 
								                    	targetAttributesRetrievalMethod(request, response);
								                    },
													defaultAvailableView: true,
								                    defaultResults: function (request, response) { 
								                        request.term = "";
								                        targetAttributesRetrievalMethod(request, response);
								                    },
								                    onItemSelected: function (settings, item) {
				                                    	// set value of hidden field (variable declared in Basic tab)
				                                    	$actionTargetAttributeHidden = j$("[id$=" + ({!rowNum}-1) + "\\:idActionTargetAttrHidden]");
				                                    	$actionTargetAttributeHidden.val(item.value);
								                    },
								                    onDelete: function (settings, item) {
								                        $actionTargetAttributeHidden = j$("[id$=" + ({!rowNum}-1) + "\\:idActionTargetAttrHidden]");
				                                    	$actionTargetAttributeHidden.val("");
								                    }  
							                    });  
		
											    // set the selected item on type ahead field when opening, re-rendering, or refreshing page
											    setAttributeLabel(j$("[id$=" + ({!rowNum}-1) + "\\:idActionTargetAttrHidden]").val(),
											    	              $advancedActionTargetAttributeMultipickList);
					            			</script>
				            			</td>
				            			<td>
				            				<div class="field">
												<apex:inputText id="idActionProductFamilySearch" html-placeholder="{!$Label.apttus_config2__Search}" value="{!action.Apttus_Config2__ProductFamilyScope__c}" />
												<apex:inputHidden id="actionProductFamilyId" value="{!action.Apttus_Config2__ProductFamilyScope__c}" />
											</div>
											<script type="text/javascript">
												
												var $actionProdFamilyHiddenCtrl = j$("[id$=" + ({!rowNum}-1) + "\\:actionProductFamilyId]"),

												prodFamilyMultipickList = j$("[id$=" + ({!rowNum}-1) + "\\:idActionProductFamilySearch]").multipickSearch({
								                    lblResultsItemsTitle:"{!$Label.AvailableItems}",
								                    lblSelectedItemsTitle:"{!$Label.SelectedItems}",
								                    lblClose:"{!$Label.Close}",
								                    lblNoResults:"{!$Label.NoResultsAvailable}",
								                    dataSourceFunction:productFamilyRetrievalMethod,
								                    defaultResults: function(request, response) { 
								                        request.term = "";
								                        productFamilyRetrievalMethod(request, response);
								                    },
								                    onItemSelected: function(settings, item){
								                    	$actionProdFamilyHiddenCtrl = j$("[id$=" + ({!rowNum}-1) + "\\:actionProductFamilyId]");
								                        onAddSelectedItem($actionProdFamilyHiddenCtrl, item.value);
								                    },
								                    onDelete:function(settings, item) { // remove item from selected list
								                        onDeleteEvent(item.value, ({!rowNum}-1) + "\\:actionProductFamilyId");                        
								                    }                    
								            	});
											
												$actionProdFamilyHiddenCtrl.val('');
												var prodFamilies = j$("[id$=" + ({!rowNum}-1) + "\\:idActionProductFamilySearch]").val().split(';');
												if(prodFamilies != '') {
													for (var i=0; i<prodFamilies.length; i++) {
														prodFamilyMultipickList.multipickSearch("addSelectedItem", {label:prodFamilies[i], value:prodFamilies[i]});
													}
												}
												j$("[id$=" + ({!rowNum}-1) + "\\:idActionProductFamilySearch]").val('');
											</script>
				            			</td>
				            			<td>
					            			<div class="field">
												<apex:inputText id="idActionProductScopeSearch" html-placeholder="{!$Label.apttus_config2__Search}" />
												<apex:inputHidden id="idActionProductScopeHidden" value="{!action.Apttus_Config2__ProductScope__c}" />
											</div> 
											<script type="text/javascript">
									        	// get selected Products from dataabse
									            var $productHiddenCtrl = j$("[id$=" + ({!rowNum}-1) + "\\:idActionProductScopeHidden]"),
									        	productMultipickList = j$("[id$=" + ({!rowNum}-1) + "\\:idActionProductScopeSearch]").multipickSearch({
									                lblResultsItemsTitle:"{!$Label.AvailableItems}",
									                lblSelectedItemsTitle:"{!$Label.SelectedItems}",
									                lblClose:"{!$Label.Close}",
									                lblNoResults:"{!$Label.NoResultsAvailable}",
									                dataSourceFunction: productsRetrievalMethod,
									                defaultResults: function(request, response) { 
									                    request.term = "";
									                    productsRetrievalMethod(request, response);
									                },
									                onItemSelected: function(settings, item){
									                	$productHiddenCtrl = j$("[id$=" + ({!rowNum}-1) + "\\:idActionProductScopeHidden]");
									                    onAddSelectedItem($productHiddenCtrl, item.value);
									                },
									                onDelete: function(settings, item){
									                    onDeleteEvent(item.value, "idActionProductScopeHidden");
									                }
									            });
												$productHiddenCtrl.val('');
									            if("{!action.Apttus_Config2__ProductScope__c}" != 'All') {
									            	var actionId = "{!JSENCODE(action.Id)}";
									            	var productList = $actionToProductsMap['{!JSENCODE(action.Id)}'];
										         	for(var prod in productList) {
										         		if(productList.hasOwnProperty(prod)) {
										         			productMultipickList.multipickSearch("addSelectedItem", {label:productList[prod].Name, value:productList[prod].Id});
										         		}
										         	}
										         	
									            }
											</script>
				            			</td>
				            			<td>
				            				<div class="field">
							                    <apex:inputText id="idActionProductGroupScopeSearch" html-placeholder="{!$Label.apttus_config2__Search}" />
							                    <apex:inputHidden id="idActionProductGroupScopeHidden" value="{!action.Apttus_Config2__ProductGroupScope__c}" />
							                </div>
							                <script type="text/javascript">
										        var $productGroupHiddenCtrl = j$("[id$=" + ({!rowNum}-1) + "\\:idActionProductGroupScopeHidden]"),
										            productGroupMultipickList = j$("[id$=" + ({!rowNum}-1) + "\\:idActionProductGroupScopeSearch]").multipickSearch({
										                lblResultsItemsTitle:"{!$Label.AvailableItems}",
										                lblSelectedItemsTitle:"{!$Label.SelectedItems}",
										                lblClose:"{!$Label.Close}",
										                lblNoResults:"{!$Label.NoResultsAvailable}",
										                dataSourceFunction: productGroupsRetrievalMethod,
										                defaultResults: function(request, response) { 
										                    request.term = "";
										                    productGroupsRetrievalMethod(request, response);
										                },
										                onItemSelected: function(settings, item){
										                	$productGroupHiddenCtrl = j$("[id$=" + ({!rowNum}-1) + "\\:idActionProductGroupScopeHidden]");
										                    onAddSelectedItem($productGroupHiddenCtrl, item.value);
										                },
										                onDelete: function(settings, item){
										                    onDeleteEvent(item.value, "idActionProductGroupScopeHidden");
										                }
										            });
													
													$productGroupHiddenCtrl.val('');
													if("{!action.Apttus_Config2__ProductGroupScope__c}" != 'All') {
										            	var actionId = "{!JSENCODE(action.Id)}";
										            	var productGroupList = $actionToProductGroupsMap['{!JSENCODE(action.Id)}'];
											         	for(var prodGrp in productGroupList) {
											         		if(productGroupList.hasOwnProperty(prodGrp)) {
											         			productGroupMultipickList.multipickSearch("addSelectedItem", {label:productGroupList[prodGrp].Name, value:productGroupList[prodGrp].Id});
											         		}
											         	}
											         	
										            }

							                </script>
				            			</td>
				            			<td>
				            				<apex:inputField id="idAdvancedActionType" value="{!action.Apttus_Config2__Action__c}" onchange="setBasicActionType(this);" />
				            				<script type="text/javascript">
				            					function setBasicActionType(actionType) {
				            						var actionTypeCellIdRegex = ':[0-9]+:[A-Z]*[a-z]*';
				            						actionTypeIdEndsWith = actionType.id.substr(actionType.id.search(actionTypeCellIdRegex)).replace(/:/g, '\\:');
				            						j$("[id$=" + actionTypeIdEndsWith.replace("{!$Label.Advanced}", "{!$Label.Basic}") + "]").val(
					            						j$("[id$=" + actionTypeIdEndsWith.replace("{!$Label.Basic}", "{!$Label.Advanced}") + "]").val()
					            					);
				            					}
				            				</script>
				            			</td>
				            			<td>
				            				<apex:inputText id="idAdvancedActionValueExpr" value="{!action.Apttus_Config2__ValueExpression__c}" onchange="setBasicActionValueExpr(this);" />
				            				<script type="text/javascript">
				            					function setBasicActionValueExpr(valueExpressionEl) {
				            						var valueExprCellIdRegex = ':[0-9]+:[A-Z]*[a-z]*';
				            						valueExprIdEndsWith = valueExpressionEl.id.substr(valueExpressionEl.id.search(valueExprCellIdRegex)).replace(/:/g, '\\:');
				            						j$("[id$=" + valueExprIdEndsWith.replace("{!$Label.Advanced}", "{!$Label.Basic}") + "]").val(
					            						j$("[id$=" + valueExprIdEndsWith.replace("{!$Label.Basic}", "{!$Label.Advanced}") + "]").val()
					            					);
					            				}
				            				</script>
				            			</td>
				            			<td>
					            			<a href="javascript:void(0);" class="circle-btn cancel-btn remove-action-row"><i class="fa fa-times"></i></a>
				            			</td>
				            			<td>
					                        <a href="javascript:void(0);" class="circle-btn add-btn add-action-row"><i class="fa fa-plus"></i></a>
				            			</td>
				            		</tr>
						           	<apex:variable var="rowNum" value="{!rowNum + 1}" />
					           	</apex:repeat>
		            		</tbody>
						</table>
					</apex:outputPanel>
				</div>
			</div>
		</div>
	</div>

	<apex:actionFunction name="addActionRow" 
						 action="{!addActionRow}" 
						 reRender="idPredicateTable, idAdvancedActionTable" 
						 oncomplete="hideSpinner();"> 
        <apex:param name="number" value=""/>
    </apex:actionFunction>

   	<apex:actionFunction name="removeActionRow" 
   						 action="{!removeActionRow}" 
   						 status="loadingStatus" 
   						 reRender="idPredicateTable, idAdvancedActionTable" 
   						 oncomplete="hideSpinner();" > 
        <apex:param name="number" value="" />
    </apex:actionFunction>
    
</apex:component>