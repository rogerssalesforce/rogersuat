<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Represents a product or service line item associated with a deal</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>BasePrice__c</fullName>
        <deprecated>false</deprecated>
        <description>The base price is List price + Automatic Adjustments for individual products and options</description>
        <externalId>false</externalId>
        <label>Base Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ChargeType__c</fullName>
        <deprecated>false</deprecated>
        <description>The charge type associated with the product or service</description>
        <externalId>false</externalId>
        <label>Charge Type</label>
        <picklist>
            <picklistValues>
                <fullName>Standard Price</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>License Fee</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Subscription Fee</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Implementation Fee</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Installation Fee</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Maintenance Fee</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Cost__c</fullName>
        <deprecated>false</deprecated>
        <description>The cost for the product or service with no adjustments</description>
        <externalId>false</externalId>
        <label>Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>DealId__c</fullName>
        <deprecated>false</deprecated>
        <description>ID of the associated deal</description>
        <externalId>false</externalId>
        <inlineHelpText>ID of the associated deal</inlineHelpText>
        <label>Deal</label>
        <referenceTo>Deal__c</referenceTo>
        <relationshipLabel>Deal Line Items</relationshipLabel>
        <relationshipName>Deal_Line_Items</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>DealSummaryGroupId__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>ID of the summary group associated with the item</description>
        <externalId>false</externalId>
        <label>Deal Summary Group</label>
        <referenceTo>DealSummaryGroup__c</referenceTo>
        <relationshipLabel>Deal Line Items</relationshipLabel>
        <relationshipName>DealLineItems</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ExtendedCost__c</fullName>
        <deprecated>false</deprecated>
        <description>The extended cost is Quantity X Selling Term X Cost</description>
        <externalId>false</externalId>
        <label>Extended Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ExtendedListPrice__c</fullName>
        <deprecated>false</deprecated>
        <description>The extended price is Quantity X Selling Term X List Price</description>
        <externalId>false</externalId>
        <label>Extended List Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ExtendedPrice__c</fullName>
        <deprecated>false</deprecated>
        <description>The extended price is Quantity X Selling Term X Base Price</description>
        <externalId>false</externalId>
        <label>Extended Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>IsPrimaryLine__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Indicates whether the line is the primary line for the product or option</description>
        <externalId>false</externalId>
        <label>Is Primary Line</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ItemSequence__c</fullName>
        <deprecated>false</deprecated>
        <description>The sequence of items belonging to a line</description>
        <externalId>false</externalId>
        <label>Item Sequence</label>
        <precision>5</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LineNumber__c</fullName>
        <deprecated>false</deprecated>
        <description>The line number</description>
        <externalId>false</externalId>
        <label>Line Number</label>
        <precision>5</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ListPrice__c</fullName>
        <deprecated>false</deprecated>
        <description>The list price for the product or service with no adjustments</description>
        <externalId>false</externalId>
        <label>List Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>NetPrice__c</fullName>
        <deprecated>false</deprecated>
        <description>he net price for the product or service is the price after applying all manual adjustments</description>
        <externalId>false</externalId>
        <label>Net Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>PriceType__c</fullName>
        <deprecated>false</deprecated>
        <description>The price type</description>
        <externalId>false</externalId>
        <label>Price Type</label>
        <picklist>
            <picklistValues>
                <fullName>One Time</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Recurring</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Usage</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>ProductFamily__c</fullName>
        <deprecated>false</deprecated>
        <description>The product family</description>
        <externalId>false</externalId>
        <label>Product Family</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ProductId__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>ID of the product or service</description>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Deal Line Items</relationshipLabel>
        <relationshipName>DealLineItems</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <deprecated>false</deprecated>
        <description>The product or service quantity</description>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SellingFrequency__c</fullName>
        <deprecated>false</deprecated>
        <description>The selling frequency associated with the recurring or usage price</description>
        <externalId>false</externalId>
        <label>Selling Frequency</label>
        <picklist>
            <picklistValues>
                <fullName>Hourly</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Daily</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Monthly</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Quarterly</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Half Yearly</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Yearly</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>One Time</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>SellingTerm__c</fullName>
        <deprecated>false</deprecated>
        <description>The selling term associated with the product or service</description>
        <externalId>false</externalId>
        <label>Selling Term</label>
        <precision>18</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Deal Line Item</label>
    <nameField>
        <displayFormat>DI-{0000000000}</displayFormat>
        <label>Line Item Id</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Deal Line Items</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
