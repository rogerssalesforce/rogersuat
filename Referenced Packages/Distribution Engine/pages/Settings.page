<apex:page Controller="n2de.SettingsController" sidebar="false" tabstyle="Settings__tab" action="{!init}">
<apex:form >
<script type="text/javascript" src="{!URLFOR($Resource.DEResources, 'scripts/jquery-1.7.1.min.js')}"></script>
<script src="{!URLFOR($Resource.DEResources, 'scripts/jquery-ui-1.10.4.min.js')}" type="text/javascript"></script>
<script type="text/javascript" src="{!URLFOR($Resource.DEResources, 'iphone-style-checkboxes/scripts/iphone-style-checkboxes.js')}"></script>
<apex:actionFunction action="{!startScheduler}" name="startSchedulerAF" reRender="page_block" oncomplete="init()" />
<apex:actionFunction action="{!stopScheduler}" name="stopSchedulerAF" reRender="page_block" oncomplete="init()" />
<apex:actionFunction action="{!startHealthChecker}" name="startHealthCheckerAF" reRender="page_block" oncomplete="init()"/>
<apex:actionFunction action="{!stopHealthChecker}" name="stopHealthCheckerAF" reRender="page_block" oncomplete="init()"/>
<apex:actionFunction action="{!updateSettings}" name="updateSettingsAF" reRender="page_block" oncomplete="init()"/>
<link type="text/css" rel="stylesheet" href="{!URLFOR($Resource.DEResources, 'iphone-style-checkboxes/style.css')}"/>
<script type="text/javascript">
$(document).ready(function() {
	init();
});
function updateSettings(){
	showMask();
	updateSettingsAF();
}
function toggleUserAuditLogging() {
	if ($('.user_login_check').attr('checked')) {
		$('.user_logging').prop('disabled', false);
		$('.user_reason_check').prop('disabled', false);	
	} else {
		$('.user_logging').prop('disabled', true);
		$('.user_reason_check').prop('disabled', true);
	}
}
function init(){

	toggleUserAuditLogging();
	
	var $loggingPicklist = $('.logging');
	$loggingPicklist.data('logging',  $loggingPicklist.val() );
	
	var onchange_checkbox = ($('.start_scheduler')).iphoneStyle({
	    onChange: function(elem, isOn) {
	    	showMask();
	    	if(isOn == true){
	    		startSchedulerAF();
	    	}else{
	    		stopSchedulerAF();
	    	}
	    }
	});
	$('.healthCheckSlider').iphoneStyle({
	    onChange: function(elem, isOn) {
	    	showMask();
	    	if(isOn == true){
	    		startHealthCheckerAF();
	    	}else{
	    		stopHealthCheckerAF();
	    	}
	    }
	});
	$('.logging').change(function(){
		var newDuration = $(this).val();
		var continueResult = true;
		if(newDuration != 'Forever'){
			var msg = 'Logs older than ' + newDuration + ' will be removed.';
			if(newDuration == 'No logging'){
				msg = 'No audit history will be held. This means you will not have any data in Distribution Log or Distribution Analytics. ';			
			}
			msg += ' Are you sure you want to change audit logging duration?';
			continueResult = confirm(msg);
		}
		if(continueResult == true){
			updateSettingsAF();
		}else{
			var oldValue = $(this).data('logging');
			$(this).val(oldValue);
		}
	});
	$('.frequency').mousedown(function(){
		var isSchedulerRunning = $(".start_scheduler").is(":checked");
		if(isSchedulerRunning){
			alert('You cannot change the frequency while the scheduler is running. Please stop the scheduler first');
			return false;
		}
	});
	$('.processing').mousedown(function(){
		var isSchedulerRunning = $(".start_scheduler").is(":checked");
		if(isSchedulerRunning){
			alert('You cannot change the processing method while the scheduler is running. Please stop the scheduler first');
			return false;
		}
	});
	$('.frequency').change(function(){
		updateSettings();
	});
	$('.processing').change(function(){
		updateSettings();
	});
	
	$('.user_login_check').change(function(){
		updateSettings();
	});
	$('.user_logging').change(function(){
		updateSettings();
	});
	$('.user_reason_check').change(function(){
		updateSettings();
	});
	
	$('.hc_notify_admin').change(function(){
		updateSettings();
	});
	$('.hc_auto_restart').change(function(){
		updateSettings();
	});
	$('.hc_notify_support').change(function(){
		updateSettings();
	});
	hideMask();
}
</script>
<apex:sectionHeader title="Settings" subtitle="Home"/>
<c:CommonMask />
<c:CommonHelp help_page="distribution-settings"/>
<apex:pageBlock title="Distribution settings" id="page_block" mode="edit">
	<apex:pageMessages />
	<apex:pageBlockSection title="Scheduler Settings" columns="2" collapsible="false" rendered="{!isAdmin}">
		<apex:pageBlockSectionItem helpText="Turn the Distribution Engine scheduler on or off by moving the slider">
			<apex:outputLabel value="Distribution Engine scheduler"/>
			<apex:inputCheckbox value="{!schedulerOn}" styleClass="start_scheduler"/>	
		</apex:pageBlockSectionItem>
		<apex:pageBlockSectionItem helpText="Determine how frequently the scheduled job which initiates distribution will run. This cannot be changed while the scheduler is already running." >
			<apex:outputLabel value="Scheduler frequency" />
			<apex:selectList value="{!frequency}" size="1" styleClass="frequency">
				<apex:selectOptions value="{!frequencyOptions}" />
			</apex:selectList>	
		</apex:pageBlockSectionItem>
		<apex:pageBlockSectionItem helpText="Determine how long to keep Distribution Logs - these are required for analytics, reports and dashboards">
			<apex:outputLabel value="Maintain audit logs"/>
			<apex:selectList value="{!logDuration}" size="1" styleClass="logging">
				<apex:selectOption itemValue="1 week" itemLabel="1 week"/>
				<apex:selectOption itemValue="1 month" itemLabel="1 month"/>
				<apex:selectOption itemValue="3 months" itemLabel="3 months"/>
				<apex:selectOption itemValue="No logging" itemLabel="No logging"/>
			</apex:selectList>	
		</apex:pageBlockSectionItem>
		<apex:pageBlockSectionItem rendered="{!showProcessingOptions}">
			<apex:outputLabel value="Processing method" />
			<apex:selectList value="{!processingMethod}" size="1" styleClass="processing">
				<apex:selectOption itemValue="Synchronous" itemLabel="Synchronous"/>
				<apex:selectOption itemValue="Asynchronous" itemLabel="Asynchronous"/>
				<apex:selectOption itemValue="MultipleSchedulers" itemLabel="MultipleSchedulers"/>
			</apex:selectList>	
		</apex:pageBlockSectionItem>
	</apex:pageBlockSection>
	
	<apex:pageBlockSection title="User login settings" columns="2" collapsible="false" rendered="{!isAdmin}">
		<apex:pageBlockSectionItem helpText="Allow users to use the On/Off slider in the Distribution Engine widget. This allows them to control when they are available for distribution">
			<apex:outputLabel value="User login mode" />
			<apex:inputCheckbox label="User login mode" value="{!userLoginMode}" styleClass="user_login_check"/>
		</apex:pageBlockSectionItem>
		<apex:pageBlockSectionItem helpText="Determine how long to maintain user activity logs">
			<apex:outputLabel value="Maintain user activity logs"/>
			<apex:selectList value="{!userLogDuration}" size="1" styleClass="user_logging">
				<apex:selectOptions value="{!daysOptions}"/>
			</apex:selectList>	
		</apex:pageBlockSectionItem>
		<apex:pageBlockSectionItem helpText="Determines whether users must provide a reason when they log off from the widget.">
			<apex:outputLabel value="Require user activity reason" />
			<apex:inputCheckbox label="Require user activity reason" value="{!userLogTrackReason}" styleClass="user_reason_check"/>
		</apex:pageBlockSectionItem>
	</apex:pageBlockSection>
	
	<apex:pageBlockSection title="Health Check Settings" columns="2" collapsible="false" rendered="{!AND(isAdmin, healthCheckerEnabled)}">
		<apex:pageBlockSectionItem helpText="Turn the health checker on or off. The health checker monitors the Distribution Engine every hour to check everything is running correctly.">
			<apex:outputLabel value="Health checker"/>
			<apex:inputCheckbox value="{!healthCheckerStarted}" styleClass="healthCheckSlider"/>
		</apex:pageBlockSectionItem>
		<apex:pageBlockSectionItem helpText="In the event of an issue send an email to the administrator using the email address shown">
			<apex:outputLabel value="Notify admin ({!SchedulerStartedByEmail})"/>
			<apex:inputCheckbox value="{!healthCheckerNotifyAdmin}" styleClass="hc_notify_admin"/>
		</apex:pageBlockSectionItem>
		<apex:pageBlockSectionItem helpText="In the event of an issue automatically restart the Distribution Engine scheduled job (recommended)">
			<apex:outputLabel value="Auto restart scheduler"/>
			<apex:inputCheckbox value="{!healthCheckerRestartScheduler}" styleClass="hc_auto_restart"/>
		</apex:pageBlockSectionItem>
		<apex:pageBlockSectionItem helpText="In the event of an issue notify NC Squared support (recommended)">
			<apex:outputLabel value="Notify Distribution Engine support"/>
			<apex:inputCheckbox value="{!healthCheckerNotifySupport}" styleClass="hc_notify_support"/>
		</apex:pageBlockSectionItem>
	</apex:pageBlockSection>
	
	<apex:pageBlockSection title="Licence Settings" columns="2" collapsible="false">
		<apex:pageBlockSectionItem helpText="Once you have purchased licences for the Distribution Engine you will be provided with an activation key. Paste the key here and click Activate.">
			<apex:outputLabel value="Licence activation Key"/>
			<apex:outputPanel >
				<apex:inputText value="{!LicenceSecurityKey}" size="50"/>
				&nbsp;&nbsp;
				<apex:commandButton value="Activate" action="{!updateLicencedUsers}"/>
			</apex:outputPanel>
		</apex:pageBlockSectionItem>
		<apex:pageBlockSectionItem />
		<apex:pageBlockSectionItem >
			<apex:outputLabel value="Number of licenced users"/>
			<apex:outputText value="{!LicencedUsers}"/>
		</apex:pageBlockSectionItem>
	</apex:pageBlockSection>
	
	<apex:pageBlockSection title="Grant Licences" columns="2" collapsible="false" rendered="{!renderGrantLicences}">
		<apex:pageBlockSectionItem >
			<apex:outputLabel value="Licences to grant"/>
			<apex:inputText value="{!LicencesToGrant}"/>
		</apex:pageBlockSectionItem>
		<apex:pageBlockSectionItem >
			<apex:outputLabel value="Licenced organization id"/>
			<apex:inputText value="{!LicencedOrgId}"/>
		</apex:pageBlockSectionItem>
		<apex:pageBlockSectionItem >
			<apex:outputLabel value="Ti edition"/>
			<apex:inputCheckBox value="{!ProEditionToGrant}"/>
		</apex:pageBlockSectionItem>
		<apex:pageBlockSectionItem >
			<apex:outputLabel value=""/>
			<apex:commandButton value="Generate security Key" action="{!grantLicencedUsers}"/>
		</apex:pageBlockSectionItem>
		<apex:pageBlockSectionItem >
			<apex:outputLabel value="Security Key"/>
			<apex:outputText value="{!GeneratedSecurityKey}"/>
		</apex:pageBlockSectionItem>
	</apex:pageBlockSection>
	
</apex:pageBlock>

</apex:form>
</apex:page>