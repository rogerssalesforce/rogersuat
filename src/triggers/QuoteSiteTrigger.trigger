trigger QuoteSiteTrigger on Quote_Site__c (before delete) {
    if(Label.isTriggerActive.equalsIgnoreCase('true')){
    
    Set<Id> quoteIds= new Set<Id>();
    Set<Id> siteIds = new Set<Id>();
    Map<Id, Id> quoteSiteMap = new Map<Id, Id>();
    List<QuoteLineItem> lineItems = new List<QuoteLineItem>();
    List<QuoteLineItem> tempLineItems = new List<QuoteLineItem>();
    
    for (Quote_Site__c qs : trigger.old){
        quoteIds.add(qs.Quote__c);
        siteIds.add(qs.Site__c);
        quoteSiteMap.put(qs.Quote__c, qs.Site__c);
    }
    
    // This will grab more than I need so I better remove some of the line
    // items that I want to keep - those that are not Quote_Sites
    tempLineItems = [SELECT Id, QuoteId, Site__c FROM QuoteLineItem where QuoteId IN :quoteIds AND Site__c IN :siteIds ];
    
    for (QuoteLineItem qli : tempLineItems){
         if (quoteSiteMap.get(qli.QuoteId) == qli.Site__c)
             lineItems.add(qli);
    }

    List<QuoteDocument> quoteDocuments = [SELECT Id, quoteId FROM QuoteDocument where quoteId IN :quoteIds];
    
    try {
        DELETE quoteDocuments;  
        DELETE lineItems;
    }catch(DMLException ex){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
        ApexPages.addMessage(myMsg);
    }
   }
    
}