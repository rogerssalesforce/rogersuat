trigger userTrigger on User (after update,before update) {
    Set<id> inactiveUserIds = new Set<Id>();
    Set<id> UsersAccOwnerTypeIds = new Set<id>();
    userTriggerHelper obj = new userTriggerHelper();
    for(integer i=0; i<Trigger.new.size(); i++){
        if(Trigger.new[i].isActive==false && Trigger.new[i].isActive != Trigger.old[i].isActive)
            inactiveUserIds.add(Trigger.new[i].Id);
        
        if(Trigger.new[i].isActive==true && Trigger.old[i].Owner_Type__c == 'Account' && Trigger.new[i].Owner_Type__c != Trigger.old[i].Owner_Type__c)    
            UsersAccOwnerTypeIds.add(Trigger.new[i].Id);
    }
    
     if(trigger.isUpdate && trigger.isAfter) 
        userTriggerHelper.handleInactiveUsers(inactiveUserIds);
   
    if(trigger.isUpdate && trigger.isbefore)
    {  
        userTriggerHelper.UsersAccOwnerType(UsersAccOwnerTypeIds);
        
        LIST<Id> validatedList = userTriggerHelper.UsersAccOwnerType(UsersAccOwnerTypeIds);
        if(validatedList != null && validatedList.size() > 0 )
        {  
            for(integer i =0; i<validatedList.size() ; i++)
            {
                for(integer j=0; j<Trigger.new.size(); j++)
                {
                    if(Trigger.new[j].id == validatedList[i] )
                        Trigger.new[j].addError('OwnerType of the User Cannot be updated');
                }
               /* User actualRecord;
                actualRecord.id= validatedList[i]; 
                actualRecord.Owner_Type__c.addError('OwnerType of the User Cannot be updated');*/
            }
        }
    }
    
}