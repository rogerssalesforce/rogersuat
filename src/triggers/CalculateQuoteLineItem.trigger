/*
    * <summary>Trigger is used to calculate EMRR and Price for the Quote Line Items.</summary>
    * 2014-04-09    Jocsan Diaz    2.0    Remove code to delete opportunity line items. This is not longer required as all product family records must be kept.
    */
        
trigger CalculateQuoteLineItem on Quote_Line_Item__c (before insert, after insert, before update, after update, before delete, after delete) {

  private Set<String> productIds;
    private Set<String> quoteLineItemsIds;
    
    private Set<String> parentQLIIds;
    
    private Map<Id, Product2> productsMap;
    private Map<Id, Quote_Line_Item__c> QLIsMap;
    
    private set<Id> theQuoteByPassValidationIdSet = new set<Id>();
  if(Label.isTriggerActive.equalsIgnoreCase('true')){    
    // exit trigger if reaching limit
    if (Limits.getLimitQueries() - Limits.getQueries() < 20) return;
    
    if(Trigger.isInsert && Trigger.isBefore) { // before insert
        productIds = new Set<String>();
        
        for(Quote_Line_Item__c newQLI : Trigger.new) {
            if(!newQLI.Is_Cloned__c) {
                productIds.add(newQLI.Product__c);
            }
        }
        
        if(!productIds.isEmpty()) {
            productsMap = new Map<Id, Product2>([
                    SELECT Id, Period__c, Family
                    FROM Product2
                    WHERE Id IN :productIds LIMIT 50000]);
        }
        
        for(Quote_Line_Item__c newQLI : Trigger.new) {
            if(!newQLI.Is_Cloned__c) {
                if(newQLI.Type__c != 'Custom') {
                    newQLI = calculateEMRR(newQLI);
                } else {
                    newQLI.EMRR__c = 0;
                    newQLI.OTC_Special__c = 0;
                    
                    /*the code has been copied from calculateEMRR TODO include into calculation*/
                    //calculate for single Custom Service Item BEGIN
                    if(newQLI.Commission_Type__c == 'New Revenue (Commission = Contract Renewal)' || newQLI.Commission_Type__c == 'New Revenue') {
                        if(newQLI.Quantity__c == NULL || newQLI.Quantity__c == 0) {
                            newQLI.EMRR__c = 0;
                        } else if(newQLI.Price__c == NULL || newQLI.Price__c == 0) {
                            newQLI.EMRR__c = 0;
                        } else if(newQLI.Discount__c == NULL) {
                            newQLI.EMRR__c = newQLI.Quantity__c * newQLI.Price__c;
                        } else {
                            if(newQLI.Period__c == NULL || newQLI.Period__c == '0') {
                                newQLI.EMRR__c = 0; //as per additional clarifcation from Jocsan Diaz on 05/07/2012
                                newQLI.OTC_Special__c = newQLI.Quantity__c * newQLI.Price__c * (100 - newQLI.Discount__c) / 100;
                            } else {
                                Integer customPeriod = Integer.valueOf(newQLI.Period__c);
                                newQLI.EMRR__c =
                                    newQLI.Quantity__c * newQLI.Price__c * (100 - newQLI.Discount__c) / 100 / customPeriod;
                            }
                        }
                    } else if(newQLI.Commission_Type__c == 'Service Upgrade') {
                        newQLI.EMRR__c = newQLI.Upgrade_Amount__c;
                    }
                    //calculate for single Custom Service Item END
                }
            }
        }
    } else if(Trigger.isInsert && Trigger.isAfter) { // after insert
        refreshPriceForParentQLIs();
    } else if(Trigger.isUpdate && Trigger.isBefore) { // before update
        productIds = new Set<String>();
        quoteLineItemsIds = new Set<String>();
        
        for(Quote_Line_Item__c newQLI : Trigger.new) {
            if(!newQLI.Is_Cloned__c && !Trigger.oldMap.get(newQLI.Id).Is_Cloned__c) {
                productIds.add(newQLI.Product__c);
                quoteLineItemsIds.add(newQLI.Id);
            }
            
            // allow Sales Coordinators to update two fields
            if(newQLI.Commission_Type__c != trigger.oldMap.get(newQLI.Id).Commission_Type__c) {
                theQuoteByPassValidationIdSet.add(newQLI.Quote__c);
            } else if(newQLI.Upgrade_Amount__c != trigger.oldMap.get(newQLI.Id).Upgrade_Amount__c) {
                theQuoteByPassValidationIdSet.add(newQLI.Quote__c);
            }            
        }
        
        if(!productIds.isEmpty()) {
            productsMap = new Map<Id, Product2>([
                    SELECT Id, Period__c, Family
                    FROM Product2
                    WHERE Id IN :productIds LIMIT 20000]);
        }
        
        if(!quoteLineItemsIds.isEmpty()) {
            QLIsMap = new Map<Id, Quote_Line_Item__c>([
                    SELECT Id, Quote__c,
                        (SELECT Price__c, Quantity__c, Discount__c
                        FROM Quote_Line_Items__r)
                    FROM Quote_Line_Item__c
                    WHERE Id IN :quoteLineItemsIds LIMIT 30000]);
        }
        
        for(Quote_Line_Item__c newQLI : Trigger.new) {

            if(!newQLI.Is_Cloned__c && !Trigger.oldMap.get(newQLI.Id).Is_Cloned__c) {
                if(QLIsMap.get(newQLI.Id).Quote_Line_Items__r != NULL && (!QLIsMap.get(newQLI.Id).Quote_Line_Items__r.isEmpty())) {
                    // if QLI is parent then clear Price and the 6 fields
                    newQLI.Price__c = 0;
                    newQLI.RollupType__c = null;
                    newQLI.RollupValue__c = null;
                    newQLI.Family__c = null;
                    newQLI.Service_Class__c = null;
                    newQLI.Sub_Class__c = null;
                    newQLI.Service_Group__c = null;
                    newQLI.Is_Parent__c = true;
                    

                    //CHILD items price and EMRR calculation
                    for(Quote_Line_Item__c childQLI : QLIsMap.get(newQLI.Id).Quote_Line_Items__r) {
                        Decimal childPrice = (childQLI.Price__c != NULL ? childQLI.Price__c : 0);
                        Decimal childQuantity = (childQLI.Quantity__c != NULL ? childQLI.Quantity__c : 0);
                        Decimal childDiscount = (childQLI.Discount__c != NULL ? childQLI.Discount__c : 0);
                        
                        newQLI.Price__c += childPrice * childQuantity * ((100 - childDiscount) / 100);
                    }
                    
                    // Jocsan changed this
                    if (newQLI.Commission_Type__c != 'Service Upgrade') {
                        newQLI = calculateEMRR(newQLI);
                    } else {
                        newQLI.EMRR__c = 0;
                    }   
                } else if(newQLI.Type__c != 'Custom') {
                    newQLI = calculateEMRR(newQLI);
                } else {
                    newQLI.EMRR__c = 0;
                    newQLI.OTC_Special__c = 0;
                    newQLI.Is_Parent__c = false;
                    // To do refactor
                    if(newQLI.Commission_Type__c == 'New Revenue (Commission = Contract Renewal)' || newQLI.Commission_Type__c == 'New Revenue') {
                        if(newQLI.Quantity__c == NULL || newQLI.Quantity__c == 0) {
                            newQLI.EMRR__c = 0;
                        } else if(newQLI.Price__c == NULL || newQLI.Price__c == 0) {
                            newQLI.EMRR__c = 0;
                        } else if(newQLI.Discount__c == NULL) {
                            newQLI.EMRR__c = newQLI.Quantity__c * newQLI.Price__c;
                        } else {
                            if(newQLI.Period__c == NULL || newQLI.Period__c == '0') {
                                newQLI.EMRR__c = 0;//as per additional clarifcation from Jocsan Diaz on 05/07/2012  
                                newQLI.OTC_Special__c = newQLI.Quantity__c * newQLI.Price__c * (100 - newQLI.Discount__c) / 100;                            
                            } else {
                                Integer customPeriod = Integer.valueOf(newQLI.Period__c);
                                newQLI.EMRR__c =
                                    newQLI.Quantity__c * newQLI.Price__c * (100 - newQLI.Discount__c) / 100 / customPeriod;
                            }
                        }
                    } else if(newQLI.Commission_Type__c == 'Service Upgrade') {
                        newQLI.EMRR__c = newQLI.Upgrade_Amount__c;
                    }
                }
            }
        }
    } else if(Trigger.isUpdate && Trigger.isAfter) { // after update
        for(Quote_Line_Item__c newQLI : Trigger.new) {         
            // allow Sales Coordinators to update two fields
            if(newQLI.Commission_Type__c != trigger.oldMap.get(newQLI.Id).Commission_Type__c) {
                theQuoteByPassValidationIdSet.add(newQLI.Quote__c);
            } else if(newQLI.Upgrade_Amount__c != trigger.oldMap.get(newQLI.Id).Upgrade_Amount__c) {
                theQuoteByPassValidationIdSet.add(newQLI.Quote__c);
            }            
        }       
        refreshPriceForParentQLIs();
    } else if(Trigger.isDelete && Trigger.isBefore) {
        refreshPriceForParentQLIs();
    } else if(Trigger.isDelete && Trigger.isAfter) {
        refreshPriceForParentQLIs();
    }

 }   
    // Calculate EMRR price for the Quote Line Item
    private Quote_Line_Item__c calculateEMRR(Quote_Line_Item__c p_QLI) {
        if(p_QLI.Commission_Type__c == 'New Revenue' || p_QLI.Commission_Type__c == 'New Revenue (Commission = Contract Renewal)') {
            if(productsMap.containsKey(p_QLI.Product__c) &&
                (productsMap.get(p_QLI.Product__c).Period__c == '0' || productsMap.get(p_QLI.Product__c).Period__c == NULL)) {
                p_QLI.EMRR__c = 0;
                p_QLI.OTC_Special__c = 0;
                
                if(p_QLI.Quantity__c == NULL || p_QLI.Quantity__c == 0) {
                    p_QLI.OTC_Special__c = 0;
                } else if(p_QLI.Price__c == NULL || p_QLI.Price__c == 0) {
                    p_QLI.OTC_Special__c = 0;
                } else if(p_QLI.Discount__c == NULL) {
                    p_QLI.OTC_Special__c = p_QLI.Quantity__c * p_QLI.Price__c;
                } else {
                    p_QLI.OTC_Special__c = p_QLI.Quantity__c * p_QLI.Price__c * (100 - p_QLI.Discount__c) / 100;
                    system.debug('==============>p_QLI.OTC_Special__c'+p_QLI.OTC_Special__c);
                }
                
                if(productsMap.get(p_QLI.Product__c).Family == 'OTC_SPECIAL') {
                    p_QLI.EMRR__c = p_QLI.OTC_Special__c * (0.0833333333333333);
                    p_QLI.OTC_Special__c = 0;
                } else {
                    p_QLI.EMRR__c = 0;
                }
                
            } else if(productsMap.containsKey(p_QLI.Product__c) && productsMap.get(p_QLI.Product__c).Period__c != NULL) {
                if(p_QLI.Quantity__c == NULL || p_QLI.Quantity__c == 0) {
                    p_QLI.EMRR__c = 0;
                    p_QLI.OTC_Special__c = 0;
                } else if(p_QLI.Price__c == NULL || p_QLI.Price__c == 0) {
                    p_QLI.EMRR__c = 0;
                    p_QLI.OTC_Special__c = 0;
                } else if(p_QLI.Discount__c == NULL) {
                    if(productsMap.get(p_QLI.Product__c).Family != 'OTC_SPECIAL' || productsMap.get(p_QLI.Product__c).Family == 'OTC_SPECIAL') {
                        p_QLI.EMRR__c = p_QLI.Quantity__c * p_QLI.Price__c /
                            Integer.valueOf(productsMap.get(p_QLI.Product__c).Period__c);
                        p_QLI.OTC_Special__c = 0;
                    } else {
                        p_QLI.OTC_Special__c = p_QLI.Quantity__c * p_QLI.Price__c /
                            Integer.valueOf(productsMap.get(p_QLI.Product__c).Period__c);
                        p_QLI.EMRR__c = 0;
                    }
                } else {
                    if(productsMap.get(p_QLI.Product__c).Family != 'OTC_SPECIAL' || productsMap.get(p_QLI.Product__c).Family == 'OTC_SPECIAL') {
                        p_QLI.EMRR__c = p_QLI.Quantity__c * p_QLI.Price__c * (100 - p_QLI.Discount__c) / 100 /
                            Integer.valueOf(productsMap.get(p_QLI.Product__c).Period__c);
                        p_QLI.OTC_Special__c = 0;
                    } else {
                        p_QLI.OTC_Special__c = p_QLI.Quantity__c * p_QLI.Price__c * (100 - p_QLI.Discount__c) / 100 /
                            Integer.valueOf(productsMap.get(p_QLI.Product__c).Period__c);
                        p_QLI.EMRR__c = 0;
                    }
                }
            }
        } else if(p_QLI.Commission_Type__c == 'Service Upgrade') {
            if(productsMap.get(p_QLI.Product__c).Family != 'OTC_SPECIAL' ||
                    productsMap.get(p_QLI.Product__c).Family == 'OTC_SPECIAL') {
                
                p_QLI.EMRR__c = p_QLI.Upgrade_Amount__c;
                p_QLI.OTC_Special__c = 0;
            } else {
                p_QLI.OTC_Special__c = p_QLI.Upgrade_Amount__c;
                p_QLI.EMRR__c = 0;
            }
        } else {
            p_QLI.EMRR__c = 0;
            p_QLI.OTC_Special__c = 0;
        }

        return p_QLI;
    }
    
    // Refresh Sales Price for the parent Quote Line Items
    private void refreshPriceForParentQLIs() {
      System.debug('#Shift_Ed refreshPriceForParentQLIs was called ');
        parentQLIIds = new Set<String>();
        Set<String> quoteIds = new Set<String>();
        Set<String> quoteLineItemIds = new Set<String>();
        
        if(!Trigger.isDelete) {
            if(Trigger.isInsert) {
                for(Quote_Line_Item__c newQLI : Trigger.new) {
                    if(!newQLI.Is_Cloned__c) {
                        parentQLIIds.add(newQLI.Parent_Quote_Line_Item__c);
                        quoteIds.add(newQLI.Quote__c);
                        quoteLineItemIds.add(newQLI.Id);
                    }
                }
            } else {
                for(Quote_Line_Item__c newQLI : Trigger.new) {
                    if(!newQLI.Is_Cloned__c && !Trigger.oldMap.get(newQLI.Id).Is_Cloned__c) {
                        parentQLIIds.add(newQLI.Parent_Quote_Line_Item__c);
                        parentQLIIds.add(Trigger.oldMap.get(newQLI.Id).Parent_Quote_Line_Item__c);
                        quoteIds.add(newQLI.Quote__c);
                        quoteLineItemIds.add(newQLI.Id);
                    }
                }
            }
        } else {
            for(Quote_Line_Item__c newQLI : Trigger.old) {
                if(!newQLI.Is_Cloned__c) {
                    parentQLIIds.add(newQLI.Parent_Quote_Line_Item__c);
                    quoteIds.add(newQLI.Quote__c);
                    quoteLineItemIds.add(newQLI.Id);
                }
            }
        }

        System.debug('#Shift_Ed parentQLIIds = '+parentQLIIds);
        System.debug('#Shift_Ed  quoteIds = '+quoteIds);
        System.debug('#Shift_Ed  quoteLineItemIds = '+quoteLineItemIds);

        List<Quote_Line_Item__c> parentQLIsToUpdate = new List<Quote_Line_Item__c>();
        if(!parentQLIIds.isEmpty()) {
            parentQLIsToUpdate = [
                    SELECT Id, Quote__c
                    FROM Quote_Line_Item__c
                    WHERE Id IN :parentQLIIds LIMIT 15000];      
        }
        
        map<Id, Quote__c> quotes = new map<Id, Quote__c>([
                    SELECT Id, Opportunity_Name__c, Validation_Exception__c
                    FROM Quote__c
                    WHERE Id IN :quoteIds LIMIT 10000]);
        
        for (Quote__c q : quotes.values()) {
            if (theQuoteByPassValidationIdSet.contains(q.Id)) q.Validation_Exception__c = !q.Validation_Exception__c;
        }        
        
        System.debug('#Shift_Ed parentQLIsToUpdate = '+parentQLIsToUpdate);
        System.debug('#Shift_Ed quotes = '+quotes);
        
        if(!parentQLIsToUpdate.isEmpty()) {
            Database.update(parentQLIsToUpdate);
        }
        if(!quotes.isEmpty()) {
            Database.update(quotes.values());
        }
        
        if(Trigger.isDelete && Trigger.isAfter) {
            
        } else {
            if(!quoteLineItemIds.isEmpty()) {
                refreshOpportunityProductsPrice(quoteLineItemIds);
            }
        }
    }

  // refresh oli records from quote. // 2.0
  private void refreshOpportunityProductsPrice(set<String> quoteLineItemIds) {
    // get quote line items
        List<Quote_Line_Item__c> quoteLineItems = [
                SELECT Quote__r.Opportunity_Name__c, EMRR__c, OTC_Special__c, Product__r.Family, Family__c
                    FROM Quote_Line_Item__c
                        WHERE Id IN :quoteLineItemIds AND Quote__r.Syncing__c = TRUE LIMIT 10000];
        
        // get opportunity and quote ids    
        Set<Id> oppIds = new Set<Id>();  
    Set<Id> quoteIds = new Set<Id>();                  
        for(Quote_Line_Item__c quoteLineItem : quoteLineItems) {
            oppIds.add(quoteLineItem.Quote__r.Opportunity_Name__c);
            quoteIds.add(quoteLineItem.Quote__c);
        } 
        
        // get opportunity and oli records
        map<Id, Opportunity> opportunities = new map<Id, Opportunity>([
                SELECT Name,
                    (SELECT UnitPrice, Line_Item_Product_Family__c FROM OpportunityLineItems)
                    FROM Opportunity
                      WHERE Id IN :oppIds LIMIT 10000]);
        system.debug('opportunities map = '+ opportunities); 
        
        // aggregate qli data
    AggregateResult[] groupedResults = [
      select Quote__r.Opportunity_Name__c theOpportunityId, Family__c theFamily, SUM(EMRR__c) theSUM 
        from Quote_Line_Item__c 
          where Quote__c =: quoteIds 
            group by Quote__r.Opportunity_Name__c, Family__c];
    // populate EMRR map        
    map<Id, map<string, decimal>> theEMRRMap = new map<Id, map<string, decimal>>();
    for (AggregateResult ar : groupedResults) {
      system.debug('theOpportunityId' + ar.get('theOpportunityId'));
      system.debug('theFamily' + ar.get('theFamily'));
      system.debug('theSUM' + ar.get('theSUM'));
      
      Id theOpportunityId = (Id)ar.get('theOpportunityId');
      string theFamily = (string)ar.get('theFamily');
      decimal theSUM = (decimal)ar.get('theSUM'); 
      
      if (!theEMRRMap.containsKey(theOpportunityId)) {
        map<string, decimal> t = new map<string, decimal>();
        t.put(theFamily, theSUM);
        theEMRRMap.put(theOpportunityId, t);
      } else {
        theEMRRMap.get(theOpportunityId).put(theFamily, theSUM);
      }
    } 
    system.debug(theEMRRMap); 
    
    // update oli records
    list<OpportunityLineItem> theOLI2Upsert = new list<OpportunityLineItem>();
        for (Opportunity opp : opportunities.values()) {           
            if(opp.OpportunityLineItems != NULL && (!opp.OpportunityLineItems.isEmpty())) {
                for(OpportunityLineItem oli : opp.OpportunityLineItems) {
                  oli.UnitPrice = 0; // 2.0 reset price to 0 for all lines
                    if (theEMRRMap.containsKey(opp.Id)) {
                      if (theEMRRMap.get(opp.Id).containsKey(oli.Line_Item_Product_Family__c)) oli.UnitPrice = theEMRRMap.get(opp.Id).get(oli.Line_Item_Product_Family__c);
                    }
                    theOLI2Upsert.add(oli);
                }
            }
        }
        
        // upsert oli records
        if (!theOLI2Upsert.isEmpty()) Database.upsert(theOLI2Upsert);
  }
    
    /*
    // Get all opportunity products.
    // Create new opportunity products by type if opportunity products with such type do not exist.
    // Update SalesPrice or remove if SalesPrice equals or less than 0.
    private void refreshOpportunityProductsPrice(Set<String> quoteLineItemIds) {
        // define map to store the pricebook mapped to family
        Map<String, PricebookEntry> typeToPriceBook = new Map<String, PricebookEntry>();
        
        // get all aggregated products. 2.0
        list<PricebookEntry> thePBEList = new list<PricebookEntry>([
            select Id, Product2.Family from PricebookEntry 
                where Product2.Aggregated_Product__c = true and Product2.Family != null and Product2.IsActive = true]);
        
        // add PBEs to map
        for (PricebookEntry pbe : thePBEList) {
            typeToPriceBook.put(pbe.Product2.Family, pbe);
        }        
        System.debug('typeToPriceBook = '+typeToPriceBook);     
        List<Quote_Line_Item__c> quoteLineItems = [
                SELECT Quote__r.Opportunity_Name__c, EMRR__c, OTC_Special__c, Product__r.Family, Family__c
                    FROM Quote_Line_Item__c
                        WHERE Id IN :quoteLineItemIds AND Quote__r.Syncing__c = TRUE LIMIT 15000];
        System.debug(' quoteLineItems ='+quoteLineItems);
        Set<String> oppIds = new Set<String>();
        Set<String> revenueTypes = new Set<String>();
        
        for(Quote_Line_Item__c quoteLineItem : quoteLineItems) {
            oppIds.add(quoteLineItem.Quote__r.Opportunity_Name__c);
            
            String revenueType = '';
            
            system.debug(Config__c.getInstance('Config').Default_Product_Family__c);
            if (quoteLineItem.Product__r.Family == NULL) {
                // it could be a custom line
                if (quoteLineItem.Family__c == null) {
                    revenueType = Config__c.getInstance('Config').Default_Product_Family__c; // 2.0 default to a valid value if family is null. don't care the order.
                } else {
                    revenueType = quoteLineItem.Family__c;
                }
            } else {
                revenueType = quoteLineItem.Product__r.Family;
            }
            
            revenueTypes.add(revenueType);
        }
        system.debug('revenueTypes: ' + revenueTypes);
        
        Map<Id, Opportunity> opportunities = new Map<Id, Opportunity>([
                SELECT Name,
                    (SELECT UnitPrice, Line_Item_Product_Family__c
                    FROM OpportunityLineItems 
                    WHERE Line_Item_Product_Family__c IN :revenueTypes LIMIT 4)
                FROM Opportunity
                WHERE Id IN :oppIds LIMIT 9996]);
        System.debug('opportunities map = '+opportunities);
        List<OpportunityLineItem> lineItemsToUpsert = new List<OpportunityLineItem>();
        //List<OpportunityLineItem> lineItemsToDelete = new List<OpportunityLineItem>(); // 2.0
        
        for(Opportunity opp : opportunities.values()) {
            Map<String, OpportunityLineItem> typeToOppProducts = new Map<String, OpportunityLineItem>();
            
            if(opp.OpportunityLineItems != NULL && (!opp.OpportunityLineItems.isEmpty())) {
                for(OpportunityLineItem oppLineItem : opp.OpportunityLineItems) {
                  oppLineItem.UnitPrice = 0; // 2.0 reset price to 0 for all lines
                    typeToOppProducts.put(oppLineItem.Line_Item_Product_Family__c, oppLineItem);
                }
            }
            
            if(Trigger.isInsert) {
                for(String revenueType : revenueTypes) {
                    if(!typeToOppProducts.containsKey(revenueType)) {
                        typeToOppProducts.put(revenueType, new OpportunityLineItem(
                                OpportunityId = opp.Id,
                                UnitPrice = 0,
                                Quantity = 1,
                                PricebookEntryId = typeToPriceBook.get(revenueType).Id,
                                Line_Item_Product_Family__c = revenueType));
                    }
                }
                
                for(Quote_Line_Item__c quoteLineItem : quoteLineItems) {
                    
                    String revenueType = '';

                    system.debug(Config__c.getInstance('Config').Default_Product_Family__c);
                    if (quoteLineItem.Product__r.Family == NULL) {
                        // it could be a custom line
                        if (quoteLineItem.Family__c == null) {
                            revenueType = Config__c.getInstance('Config').Default_Product_Family__c; // 2.0 default to a valid value if family is null. don't care the order.
                        } else {
                            revenueType = quoteLineItem.Family__c;
                        }
                    } else {
                        revenueType = quoteLineItem.Product__r.Family;
                    }
                    
                    OpportunityLineItem oppLineItem = typeToOppProducts.get(revenueType);
                    
                    oppLineItem.UnitPrice += (quoteLineItem.EMRR__c != NULL ? quoteLineItem.EMRR__c : 0);
                    
                    typeToOppProducts.put(revenueType, oppLineItem);
                }
                
                for(OpportunityLineItem oppLineItem : typeToOppProducts.values()) {
                    if(oppLineItem.UnitPrice >= 0) {
                        lineItemsToUpsert.add(oppLineItem);
                    }
                }
            } else if(Trigger.isUpdate) {
                for (String revenueType : revenueTypes) {
                    if(!typeToOppProducts.containsKey(revenueType)) {
                        typeToOppProducts.put(revenueType, new OpportunityLineItem(
                                OpportunityId = opp.Id,
                                UnitPrice = 0,
                                Quantity = 1,
                                PricebookEntryId = typeToPriceBook.get(revenueType).Id,
                                Line_Item_Product_Family__c = revenueType));
                    }
                }
                system.debug(' typeToOppProducts ='+typeToOppProducts);
                
                for(Quote_Line_Item__c quoteLineItem : quoteLineItems) {
                    
                    String revenueType = '';

                    system.debug(Config__c.getInstance('Config').Default_Product_Family__c);
                    if (quoteLineItem.Product__r.Family == NULL) {
                        // it could be a custom line
                        if (quoteLineItem.Family__c == null) {
                            revenueType = Config__c.getInstance('Config').Default_Product_Family__c; // 2.0 default to a valid value if family is null. don't care the order.
                        } else {
                            revenueType = quoteLineItem.Family__c;
                        }
                    } else {
                        revenueType = quoteLineItem.Product__r.Family;
                    }
                    System.debug('inside Loop  revenueType = '+revenueType);
                    OpportunityLineItem oppLineItem = typeToOppProducts.get(revenueType);
                    Decimal oldPrice = 0;
                    Decimal newPrice = 0;
                    
                    system.debug('inside loop oppLineItem = '+oppLineItem);
                    
                    oldPrice = (Trigger.oldMap.get(quoteLineItem.Id).EMRR__c != NULL ? Trigger.oldMap.get(quoteLineItem.Id).EMRR__c : 0);    
                    newPrice = (Trigger.newMap.get(quoteLineItem.Id).EMRR__c != NULL ? Trigger.newMap.get(quoteLineItem.Id).EMRR__c : 0);
                    system.debug('inside Loop oppLineItem.UnitPrice '+oppLineItem.UnitPrice+' oldPrice = '+oldPrice+' newPrice = '+newPrice);
                    
                    oppLineItem.UnitPrice = oppLineItem.UnitPrice - oldPrice + newPrice;
                    system.debug('inside loop oppLineItem.UnitPrice = '+oppLineItem.UnitPrice);
                    
                    typeToOppProducts.put(revenueType, oppLineItem);
                }
                system.debug('2 typeToOppProducts ='+typeToOppProducts);
                
                for(OpportunityLineItem oppLineItem : typeToOppProducts.values()) {
                    //if((oppLineItem.UnitPrice < 0) && oppLineItem.Id != NULL) {
                    //    lineItemsToDelete.add(oppLineItem);
                    //} else if(oppLineItem.UnitPrice >= 0) {
                    //    lineItemsToUpsert.add(oppLineItem);
                    //} // 2.0
                    
                    // we still want to upsert records. 2.0
                    if (oppLineItem.UnitPrice >= 0) {
                        lineItemsToUpsert.add(oppLineItem);
                    }
                }
            } else if(Trigger.isDelete) {
                for(Quote_Line_Item__c quoteLineItem : quoteLineItems) {
                    
                    String revenueType = '';

                    system.debug(Config__c.getInstance('Config').Default_Product_Family__c);
                    if (quoteLineItem.Product__r.Family == NULL) {
                        // it could be a custom line
                        if (quoteLineItem.Family__c == null) {
                            revenueType = Config__c.getInstance('Config').Default_Product_Family__c; // 2.0 default to a valid value if family is null. don't care the order.
                        } else {
                            revenueType = quoteLineItem.Family__c;
                        }
                    } else {
                        revenueType = quoteLineItem.Product__r.Family;
                    }
                    
                    if(typeToOppProducts.containsKey(revenueType)) {
                        OpportunityLineItem oppLineItem = typeToOppProducts.get(revenueType);
                        
                        oppLineItem.UnitPrice =
                            oppLineItem.UnitPrice - (quoteLineItem.EMRR__c != NULL ? quoteLineItem.EMRR__c : 0);
                        
                        typeToOppProducts.put(revenueType, oppLineItem);
                    }
                }
                
                for(OpportunityLineItem oppLineItem : typeToOppProducts.values()) {
                    if(oppLineItem.UnitPrice < 0) {
                        //lineItemsToDelete.add(oppLineItem); 2.0
                    } else {
                        lineItemsToUpsert.add(oppLineItem);
                    }
                }
            }
        }
        System.debug('before upserting lineItemsToUpsert '+lineItemsToUpsert);
        if(!lineItemsToUpsert.isEmpty()) {
            Database.upsert(lineItemsToUpsert);
        }
        
        //if(!lineItemsToDelete.isEmpty()) {
        //    Database.delete(lineItemsToDelete);
        //} // 2.0
    }*/
    
}