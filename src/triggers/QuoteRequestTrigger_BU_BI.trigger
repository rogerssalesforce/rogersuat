trigger QuoteRequestTrigger_BU_BI on Quote_Request__c (before insert, before update) {
 if(Label.isTriggerActive.equalsIgnoreCase('true')){
    if (Trigger.isInsert){
        for (Quote_Request__c qr : trigger.new){
            if (qr.Sales_Engineer__c == null) 
                qr.Sales_Engineer__c = qr.OwnerId;
        }
    }
    
    List<Id> originalQuoteRequestIds = new List<Id>();
    for (Quote_Request__c q : Trigger.New){
        if (q.Original_Quote_Request__c!=null)
            originalQuoteRequestIds.add(q.Original_Quote_Request__c);
        
        if (q.Version__c == null){
            q.Version__c = 1;
            q.Number_Of_Derived_Quote_Requests__c = 0;
        }
    }
    
    if (Trigger.isInsert && originalQuoteRequestIds.size()!=null){
        Map<Id, Quote_Request__c> originalQuoteMap = new Map<Id, Quote_Request__c>([SELECT id, Number_Of_Derived_Quote_Requests__c FROM Quote_Request__c WHERE Id IN :originalQuoteRequestIds]);
        for (Quote_Request__c q : Trigger.New){
            Quote_Request__c originalQuoteRequest = originalQuoteMap.get(q.Original_Quote_Request__c);
            if (originalQuoteRequest!=null){
                if (originalQuoteRequest.Number_Of_Derived_Quote_Requests__c != null)
                    q.version__c = originalQuoteRequest.Number_Of_Derived_Quote_Requests__c + 2;
                else 
                    q.version__c = 2;
            }
        }
    }
    
    if (Trigger.isUpdate){
         for (Quote_Request__c qr : trigger.new){
            if (qr.Quoting_Stage__c == 'Waiting for SE Acceptance') 
                qr.Closed_By__c = System.Userinfo.getUserId();
        }
        
        List<Id> qIds = new List<Id>();
        for (Quote_Request__c q : Trigger.New){
             qIds.add(q.Id);
        }
        List<Carrier_Quote__c> cQuotes = new List<Carrier_Quote__c>();
        
        try{
            cQuotes = [SELECT Id, Selected__c, Quote_Request__c FROM Carrier_Quote__c WHERE Selected__c = true AND Quote_Request__c IN :qIds];
        }catch(Exception ex){
            
        }
        
        Map<Id, Integer> numberOfSelected = new Map<Id, Integer>();
        
        for (Carrier_Quote__c c : cQuotes){
            Integer i = numberOfSelected.get(c.Quote_Request__c);
            i = i==null ? 0 : i;
            numberOfSelected.put(c.Quote_Request__c, ++i);
        }
        
        Set<Id> ids = new Set<Id>();
        for (Quote_Request__c q : Trigger.New){
            if (q.Quoting_Stage__c == 'Closed by Quote Desk'){
                if (numberOfSelected.get(q.Id) == 0 || numberOfSelected.get(q.Id) == null){
                    q.addError('Unable to close the request until atleast one Carrier Quote is selected as preferred.');
                }
            }
            
                if ((q.Quoting_Stage__c == 'Closed by Quote Desk')|| (q.Quoting_Stage__c == 'Rejected by Quote Desk')){
                    ids.add(q.id);
                }
        }
        if (ids.size()>0){
        //  try{
                List<ProcessInstance> approvals = [SELECT Id,  
                                        Status, 
                                        TargetObjectId, 
                                            (SELECT Id, 
                                                StepStatus, 
                                                ActorId,
                                                Actor.Name, 
                                                OriginalActor.Name, 
                                                CreatedDate, Comments 
                                                FROM StepsAndWorkitems Order By CreatedDate DESC) 
                                        FROM ProcessInstance 
                                        WHERE TargetObjectId IN :ids Order By LastModifiedDate DESC LIMIT 1];
            
                for (ProcessInstance approval : approvals){
                    for (Quote_Request__c q : Trigger.New){
                        if (q.Id == approval.TargetObjectId){
                            q.Recent_Approval_Notes__c  = approval.StepsAndWorkitems[0].Comments;
                        }
                    }
                }
        //  }catch(Exception ex){
                
       //   }
        }
        
        }
        
    }
}