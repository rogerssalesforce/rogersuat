/**************************************************************************************************************************
Class Name      : CPQ_OpportunityTriggerHelper
Description     : This Trigger calls the helper class 'CPQ_AgreementTriggerHelper' 
Created By      : Aakanksha Patel
Created Date    : 19-Oct-15
Modification Log:
--------------------------------------------------------------------------------------------------------------------------
Developer                   Date                     Description
------------------------------------------------------------------------------------------------------------------------- 
Aakanksha Patel           19-Oct-2015         US-0115. Method to associate a Temporary PriceBook every time a new Opportunity is created.(US-0115)
**************************************************************************************************************************/
trigger CPQ_OpportunityTrigger on Opportunity (before insert,before update) {
    
    If(trigger.isBefore && Trigger.isInsert){
        CPQ_OpportunityTriggerHelper.assignPricebookToOpp(trigger.new);
    }
    If(trigger.isBefore && (Trigger.isInsert ||Trigger.isUpdate) ){       
     CPQ_OpportunityTriggerHelper.validateOpportunityStageQualify(trigger.new);
    }
    
}