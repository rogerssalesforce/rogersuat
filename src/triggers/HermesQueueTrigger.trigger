trigger HermesQueueTrigger on Hermes_Queue__c (before insert) {

    if (Trigger.isInsert && Trigger.isBefore){
        new HermesQueueHandler().handleBeforeInsert(Trigger.new);
    }
}