/**************************************************************************************************************************
Class Name      : CPQ_ProposalTrigger
Description     : This Trigger calls the helper class 'CPQ_ProposalTriggerHelper' 
Created By      : Akshay Ahluwalia
Created Date    : 24-Sep-15
Modification Log:
--------------------------------------------------------------------------------------------------------------------------
Developer                   Date                     Description
------------------------------------------------------------------------------------------------------------------------- 
Akshay Ahluwalia         24-Sep-15                Original Version
Alina Balan              28-October-2015          US-0332  see only the quotes for Accounts where the current user belong to the Account Team
Ramiya Kandeepan         19-November-2015         US-0405  Default the price list based on the opportunity price list and account type
Alina Balan              08-January-2016          US-0507  Added "setSharingsRulesBasedOnOpportunityTeam"
Joseph Toms              08-Jan-2016              US-0491  to calculate threshold amount group by product family ,type and price type
Aakanksha Patel          11-Jan-2016              US-0506  "UpdateWirelineTermAndSEStatus" method
                                                  US-0160  "UpdateQuoteStatusToNoneForCloned" method
**************************************************************************************************************************/

trigger CPQ_ProposalTrigger on Apttus_Proposal__Proposal__c (Before Update, Before Insert, After Insert) {

    if(!CPQ_ProposalTriggerHelper.blTriggersPreviouslyRan)
    {
       if(trigger.isBefore && trigger.isUpdate)
       {
           CPQ_ProposalTriggerHelper.UpdateWirelineTermAndSEStatus(trigger.new, trigger.old);
           CPQ_ProposalTriggerHelper.updateQuoteStatus(trigger.new, trigger.oldMap);
       }
       if(trigger.isBefore && trigger.isInsert){
           CPQ_ProposalTriggerHelper.UpdatePriceListBasedOnAccount(trigger.new);
           CPQ_ProposalTriggerHelper.UpdateQuotePriceListBasedOnOpportunity(trigger.new);
          
       }
       if(trigger.isAfter && trigger.isInsert) {
           CPQ_ProposalTriggerHelper.setSharingsRules(trigger.new);
           CPQ_ProposalTriggerHelper.UpdateQuoteStatusToNoneForCloned(trigger.new);
       }
    
       //if(trigger.isBefore && ( trigger.isInsert||trigger.isUpdate)){
       //  CPQ_ProposalTriggerHelper.updateProposalThresHold(trigger.new);
       //}
    }
    
    if(trigger.isAfter)
        CPQ_ProposalTriggerHelper.blTriggersPreviouslyRan = true;
}