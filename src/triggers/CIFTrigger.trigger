trigger CIFTrigger on CIF__c (before insert, before update) {

	if(Label.isTriggerActive.equalsIgnoreCase('true')){
	List<Id> eBillingContacts = new List<Id>();

	for(CIF__c cif : Trigger.NEW){
		cif.BaseURLEmailTemplate__c = URL.getSalesforceBaseUrl().toExternalForm();
		// Power Of One - to display a checkbox on the COP Detail Page indicating there
		// is a incorrect Site coming from the Customer Facing COP
		cif.Contains_Incorrect_Site_Details1__c = (cif.Contains_Incorrect_Site_Details__c > 0);
		// We need to set all of the eBilling information
		if (cif.eBilling_Admin_Contact__c!=null)
			eBillingContacts.add(cif.eBilling_Admin_Contact__c);
	}
	Map<Id, Contact> eBillingContactMap;
	if (!eBillingContacts.isEmpty())
		eBillingContactMap = new Map<Id, Contact>([SELECT Id, Email, Phone, FirstName, LastName, MobilePhone, Title FROM Contact WHERE Id IN :eBillingContacts]);
	
	for(CIF__c cif : Trigger.NEW){
		if (cif.eBilling_Admin_Contact__c!=null){
			Contact c = eBillingContactMap.get(cif.eBilling_Admin_Contact__c);
			if (c!=null){
				cif.eBilling_Admin_Contact_Phone__c = c.Phone;
				cif.eBilling_Admin_Contact_Mobile__c = c.MobilePhone;
				cif.eBilling_Admin_Contact_Job_Title__c = c.Title;
				cif.eBilling_Admin_Contact_Name__c = c.FirstName + ' ' + c.LastName;
				cif.eBilling_Admin_Contact_Email__c = c.Email;
			}
			
		}else{
				cif.eBilling_Admin_Contact_Phone__c = null;
				cif.eBilling_Admin_Contact_Mobile__c = null;
				cif.eBilling_Admin_Contact_Job_Title__c = null;
				cif.eBilling_Admin_Contact_Name__c = null;
				cif.eBilling_Admin_Contact_Email__c = null;
			}
		
	}
	
	
	if (Trigger.isUpdate){
		
		Map<Id, CIF__c> cifMap = new Map<Id, CIF__c>([SELECT Id, Opportunity_Name__r.Id, Opportunity_Name__r.Contact__c, Opportunity_Name__r.OwnerId, Opportunity_Name__r.Account.OwnerId, Opportunity_Name__r.Account.Owner.AM_ID__c FROM CIF__c WHERE Id in :Trigger.newMap.keySet()]);
		
		for(CIF__c cif : Trigger.NEW){
			
			CIF__c tempCIF = cifMap.get(cif.Id);
			if (tempCIF != null){
				cif.Opportunity_Name__c = tempCIF.Opportunity_Name__r.Id;
		    //    cif.Contact_Name__c = tempCIF.Opportunity_Name__r.Contact__c;
		        cif.Opportunity__c = tempCIF.Opportunity_Name__r.OwnerId;
		        cif.Account_Owner__c = tempCIF.Opportunity_Name__r.Account.OwnerId;
		        cif.AM_ID__c = tempCIF.Opportunity_Name__r.Account.Owner.AM_ID__c;
			}
		}
	}
  }
}