/*
@Name:  ShiftCreateDefaultOLI 
@Description: This trigger creates default Opportunity Line Items based on Aggregated Products
@Dependancies: ShiftCreateDefaultOLISupport
@Version: 1.0.0

===VERSION HISTORY ===
Date        | Version Number | Author | Description
04-14-2014  | 1.0.0          | Jocsan | Initial
*/
trigger ShiftCreateDefaultOLI on Opportunity (after insert, after update) {
if(Label.isTriggerActive.equalsIgnoreCase('true')){
      if(Recursion_blocker.dataCenterOPPPriceBookUpdate){   
            Recursion_blocker.dataCenterOPPPriceBookUpdate= false; 
    system.debug('hasRun: ' + ShiftCreateDefaultOLISupport.hasRun);
    // Only run if this trigger hasn't run before
    Map<Id,Opportunity > mapOpp = new Map<Id,Opportunity >();
    for(Opportunity opp: Trigger.new){
        if(opp.Business_Unit__c == 'Data Centre')
            mapOpp.put(opp.id,opp);
    }
    if(mapOpp!=null && mapOpp.size()>0){
    if (!ShiftCreateDefaultOLISupport.hasRun) ShiftCreateDefaultOLISupport.theCreateDefaultOLI(mapOpp);
    }
    }    
  }
}