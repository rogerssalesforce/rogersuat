trigger shift_update_owner_info on SPR__c (before insert, before update) {
    if(Label.isTriggerActive.equalsIgnoreCase('true')){
    /*
    Copy owner's information to custom objects
    Developed by Jocsan Diaz on November 2nd, 2012
    */
    
    // query all active users
    map<Id, User> theUserMap = new map<Id, User>([select Id, Name from User where isActive = true]);
    
    // query all queues associated to SPR object
    map<Id, QueueSobject> theQueueMap = new map<Id, QueueSobject>();
    list<QueueSobject> theQueueSobjectList = new list<QueueSobject>([select q.SobjectType, q.Queue.Name, q.Queue.Type, q.Queue.Id, q.QueueId, q.Id 
                                                                        from QueueSobject q 
                                                                            where q.SobjectType =: 'SPR__c']);
    for (QueueSobject q : theQueueSobjectList) {
        theQueueMap.put(q.Queue.Id, q);
    }
    
    
    // update owner name      
    for (SPR__c spr : trigger.new) {
    /*    Commenting out section below to proceed with deployment.
          Owner does not exist for SPR.                                                              
        if (theUserMap.containsKey(spr.OwnerId)) {
            spr.Owner_Name__c = theUserMap.get(spr.OwnerId).Name;
        } else if (theQueueMap.containsKey(spr.OwnerId)) {
            spr.Owner_Name__c = theQueueMap.get(spr.OwnerId).Queue.Name;
        }    
        */
    }      
   }	
}