/* Mar/02/2015 Paul Saini    Create trigger      
     As per requirement provided   
   Populate “Opportunity_Owner_Email__c”: Obtain Email from the record associated with the Case in the Opportunity__c field and push into the oppty Case custom email field “Opportunity_Owner_Email__c”. 
       This email address is needed to send the Oppty Owner an email should the “Wireline Customer Status Update” case be rejected.
    Populate “Case_Owner_Manager_Email”: Obtain Email from the Case Owner’s User record and push into the oppty Case custom email field 
     “Opportunity_Owner_Email__c”. This email address is needed to send the Case Owner’s Manager  an email should the “Wireline Customer Status Update” not be approved/rejected within 7 days.

*/

trigger CaseTrigger_PopulateFormulaFields on Case (before insert, before update) {
 if(Label.isTriggerActive.equalsIgnoreCase('true')){
     Set<Id> setUserId=new Set<Id>();
     set<Id> setOppId= new set<Id>();
    
 if (Trigger.isUpdate || Trigger.isInsert){
 
    for (Case c : Trigger.New){
        
        if(c.Opportunity__c != null){ 
            System.Debug('TPS:EH c.Opportunity__c=' + c.Opportunity__c);            
            setOppId.Add(c.Opportunity__c);
        }
        
        if(c.OwnerId != null){
            System.Debug('TPS:EH c.OwnerId=' + c.OwnerId);           
            setUserId.Add(c.OwnerId);
        }
    }
    
    Map<Id, User> mapUsers= new Map<Id, User>(
        [Select Id, Name, Manager.Email, Email from User Where ID IN:setUserId]);   
    
    Map<Id, Opportunity> mapOpp= new Map<Id, Opportunity>(
        [Select Id, Owner.Email from Opportunity Where ID IN:setOppId]);
   
     for (Case c : Trigger.New){        
         if(c.Opportunity__c != null){            
             Opportunity o=mapOpp.get(c.Opportunity__c);
             if(o!= null && o.Owner.Email !=null){
                 System.Debug('TPS:EH Opportunity_Owner_Email__c=' + o.Owner.Email);
                 c.Opportunity_Owner_Email__c=o.Owner.Email;
             }
         } 
            
         if(c.OwnerId != null){            
             User u=mapUsers.get(c.OwnerId);
             if(u!= null){                   
                 System.Debug('TPS:EH Case_Owner_Manager_Email__c=' + u.Manager.Email);
                 c.Case_Owner_Manager_Email__c=u.Manager.Email;                         
             } 
         }                     
     }    
 }
}
}