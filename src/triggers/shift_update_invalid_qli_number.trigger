trigger shift_update_invalid_qli_number on Quote__c (before update) {
   if(Label.isTriggerActive.equalsIgnoreCase('true')){ 
    /*
    Validate line items and flag quote.
    Developed 1.0 by Jocsan Diaz on 07/08/2012
    Changed to 2.0 by Jocsan Diaz on 01/02/2012 
    */
    
    // exit trigger if reaching limit
    if (Limits.getLimitQueries() - Limits.getQueries() < 50) return;    
    
    // get list of parent custom lines
    list<Quote_Line_Item__c> theQLIList = new list<Quote_Line_Item__c>([select Parent_Quote_Line_Item__c from Quote_Line_Item__c where Quote__c in: trigger.newMap.keySet()]);
    // create set of parent ids
    set<Id> theParentQLISet = new set<Id>();
    for (Quote_Line_Item__c q : theQLIList) {
        theParentQLISet.add(q.Parent_Quote_Line_Item__c);
    }
    system.debug(theParentQLISet);
    
    // find out the number of invalid custom lines
    AggregateResult[] groupedResults = [select Quote__c, Count(Id) theCount from Quote_Line_Item__c 
                                            where Quote__c in: trigger.newMap.keySet() and Is_valid_line_item__c = 'No' and Id not in: theParentQLISet 
                                                group by Quote__c];
    system.debug(groupedResults);
    
    // create a map of quote Ids and number of invalid custom lines
    map<Id, Integer> theInvalidMap = new map<Id, Integer>();
    for (AggregateResult ar : groupedResults) {
        System.debug('Quote Id: ' + ar.get('Quote__c'));
        System.debug('Count: ' + ar.get('theCount'));
        
        theInvalidMap.put((Id)ar.get('Quote__c'), (Integer)ar.get('theCount'));
    } 
    system.debug(theInvalidMap); 
    
    // update quotes
    for (Quote__c q : trigger.new) {
        if (theInvalidMap.containsKey(q.Id)) {
            q.Number_of_invalid_custom_line_items__c = theInvalidMap.get(q.Id);
        } else {
            q.Number_of_invalid_custom_line_items__c = 0;
        }    
    }
  }	
}