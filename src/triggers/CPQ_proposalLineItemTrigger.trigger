/**************************************************************************************************************************
Class Name      : CPQ_proposalLineItemTrigger
Description     : This Trigger calls the helper class 'CPQ_ProposalLineTriggerHelper' 
Created By      : Akshay Ahluwalia
Created Date    : 07-Jan-16
Modification Log:
--------------------------------------------------------------------------------------------------------------------------
Developer                   Date                     Description
------------------------------------------------------------------------------------------------------------------------- 
Akshay Ahluwalia         07-Jan-16                Original Version
**************************************************************************************************************************/
trigger CPQ_proposalLineItemTrigger on Apttus_Proposal__Proposal_Line_Item__c(Before insert, After insert, After update, Before Update) {
    
    if(!CPQ_ProposalLineTriggerHelper.blTriggersPreviouslyRan)
    {
        if(trigger.isAfter && trigger.isInsert){
            CPQ_ProposalLineTriggerHelper.updateCSOWOnProposal(trigger.new);
            CPQ_ProposalLineTriggerHelper.updateProductFamiliesOnProposal(trigger.new);
        }
        else if(trigger.isAfter && trigger.isUpdate){
            CPQ_ProposalLineTriggerHelper.updateCSOWOnProposal(trigger.new);
            CPQ_ProposalLineTriggerHelper.updateProductFamiliesOnProposal(trigger.new);
        }
    }
    
    if(trigger.isAfter)
        CPQ_ProposalLineTriggerHelper.blTriggersPreviouslyRan = true;
}