trigger Case_UpdateRelatedAccMAL_AU on Case (After Update,Before Insert) {
    if(Label.isTriggerActive.equalsIgnoreCase('true')){
		case_UpdateRelatedAccMAL_AU obj = new case_UpdateRelatedAccMAL_AU();
		if(Trigger.isAfter &&  Trigger.isUpdate){
			obj.updateAccountMAL(Trigger.new,Trigger.old);   
		}
		if(Trigger.isBefore &&  Trigger.isInsert){
		   obj.updateOwnerId(Trigger.new);   
		}
	
	}
   
}