trigger shift_opp_has_quote on Quote__c (after insert) {
    if(Label.isTriggerActive.equalsIgnoreCase('true')){
    // on creation of Quote, mark master Opp as Has Quote
    shift_quote_trigger_support.applyHasQuoteinOpp(trigger.new);
	}
}