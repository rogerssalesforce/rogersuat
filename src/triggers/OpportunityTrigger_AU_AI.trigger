/* Jan/27/2015 Paul Saini    Edit trigger
    Function: Update trigegr such that it should only fire if the Opportunity.Business_Unit__c = 'Wireline'
    Check for condition if  Opportunity.Business_Unit__c is not populated.
    Put a if block around processing and only process records when Opportunity.Business_Unit__c='Wireline'. 
    Feb 24, 2015 Replace Opportunity.Actual_Sign_Date__c with Opportunity.CloseDate
*/

trigger OpportunityTrigger_AU_AI on Opportunity (after insert, after update) {
    if(Label.isTriggerActive.equalsIgnoreCase('true')){
    // Only run if this trigger hasn't run before
    system.debug('hasRun: ' + ShiftOpportunityTrigger_AU_AISupport.hasRun); 
    if (ShiftOpportunityTrigger_AU_AISupport.hasRun) return;
    ShiftOpportunityTrigger_AU_AISupport.hasRun = true;
    
    System.debug('About to clone the prospect sites - 1');
        if (Trigger.isInsert){
            System.debug('About to clone the prospect sites - 2');
            for (Opportunity o : Trigger.new){
                // skip records where business unit is not populated
                if (o.Business_Unit__c == null) continue;                
                // skip records where business unit !='Wireline'
                if (o.Business_Unit__c !='Wireline') continue;
        
                System.debug('About to clone the prospect sites - 3');
                if (o.RecordTypeId == '012V0000000Co6x'){   // 012V0000000Co6x //012a0000001RM35
                    System.debug('About to clone the prospect sites - 4');
                    if (!Utils.cloneSourceSites(o.parent_opportunity__c, o.Id))
                        o.addError('Unable to create the temporary opportunity - There was a problem when cloning the prospect sites.');
                }
            }
        }
    List<Opportunity> listOpp = [SELECT Id, Owner.Name,IsWon, (SELECT Id, OppWON__c FROM Quotes), SyncedQuoteId, SyncedQuote.QuoteNumber__c, Name, Account.Name, CloseDate, CreatedDate, AccountId, Account.Owner.Name,(SELECT Id, Name, Account__r.Name, Account__r.BillingCity, Account__r.BillingPostalCode, Account__r.BillingCountry, Account__r.BillingStreet, Account__r.BillingState, Access_Type__c, Account__c, Access_Type_Group__c, Suite_Floor__c, Street_Number__c, Street_Name__c, Street_Type__c, Street_Direction__c, City__c, Postal_Code__c, Province_Code__c, Country__c FROM Sites__r) FROM Opportunity WHERE Id IN :Trigger.newMap.keySet() AND Business_Unit__c='Wireline'];
    if (Trigger.isUpdate){
     System.debug('*******1*****');
        /////////////////////////////////////////////////////////////////////////////////
        // When an Opportunity Status is set to WON we need to move the prospect sites
        // to the Account as Account Active Sites and the products to the Active Products
        // for these Active Sites.  
        //
        // If the active sites aleady exist then we need to update these active sites
        // instead of adding new ones.
        ////////////////////////////////////////////////////////////////////////////////
        
        // Used to hold the the ids of the opps that we need to deal with
        // as a result of resently being set to WON
        Set<Id> oppIds = new Set<Id>();
        
        //Assign the before and after the change into a Map
        Map<Id,Opportunity> newOppMap = Trigger.newMap;
        Map<Id,Opportunity> oldOppMap = Trigger.oldMap;
    
        /* Start - Bang the Gong */
        
        List<FeedItem> feedsToUpdate = new List<FeedItem>();
        Map<Id, Id> ownerIdMap = new Map<Id, Id>();
        Map<Id, Id> accountIdMap = new Map<Id, Id>();
        
        /* End - Bang the Gong */
        
        //Loop through the map
        for(Id oppId : newOppMap.keySet()){
            Opportunity myNewOpp = newOppMap.get(oppId);
            Opportunity myOldOpp = oldOppMap.get(oppId);
            
            // If these have been recently changed to the WON stage we will need 
            // to deal with them so we will store these in a list.
            if ((myNewOpp.isWON != myOldOpp.isWON)&&(myNewOpp.isWON) && mynewOpp.Business_Unit__c !=null){
                if(mynewOpp.Business_Unit__c=='Wireline'){
                    oppIds.add(myNewOpp.Id);
                   
                    /* Start - Bang the Gong */
                    ownerIdMap.put(myNewOpp.Id, myNewOpp.OwnerId);
                    accountIdMap.put(myNewOpp.Id, myNewOpp.AccountId);
                    /* End - Bang the Gong */ 
                }  
            }
        }       
        
        /* Start - Access Type Report - DELETE records if it is no longer won */ 
        if (!Test.isRunningTest()){
            /*
            Set<Id> oldOppIds = new Set<Id>();
            
            if ((myNewOpp.isWON != myOldOpp.isWON)&&(!myNewOpp.isWON)){
                oldOppIds.add(myNewOpp.Id);
            }
            */
            // PS: Added filter for Business_Unit__c='Wireline'
            List<Access_Type_Sales__c> staleAccessTypes = [SELECT Id, Quote__c, Quote__r.Opportunity.Id FROM Access_Type_Sales__c WHERE Quote__r.Opportunity.Id IN : Trigger.newMap.keySet() AND Quote__r.Opportunity.Business_Unit__c='Wireline'];
            System.debug('Test: ' + staleAccessTypes);
               
            if (staleAccessTypes.size()>0){
                DELETE staleAccessTypes;
            }
        }
            /* End - Access Type Report - DELETE records if it is no longer won */
        
        /* Start - Bang the Gong */
         if (!Test.isRunningTest()){
            List<Id> ownerIds = ownerIdMap.values();
            List<Id> accountIds = accountIdMap.values();
            
            Map<Id, User> ownerMap = new Map<Id, User>([SELECT Id, Name FROM User WHERE Id IN : ownerIds]);
            Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, Name, RecordType.Name FROM Account WHERE Id IN : accountIds]);
            CollaborationGroup[] grpRec = [Select Id, g.Name From CollaborationGroup g where g.Name = 'Bang the Gong!!!!' limit 1];
             System.debug('*******2*****');
            
            for(Id oppId : newOppMap.keySet()){
                if (oppIds.contains(oppId) ){
                    Opportunity myNewOpp = newOppMap.get(oppId);
                    
                    if (myNewOpp.Big_Win__c == 1){
                        String salesRepName = ownerMap.get(myNewOpp.ownerId).Name;
                        String accountName = accountMap.get(myNewOpp.accountId).Name;
                        String acctRecordType = accountMap.get(myNewOpp.accountId).RecordType.Name;
                        NumberTranslation nt = new NumberTranslation();
                        System.debug('Bang the Gong: ' + acctRecordType);
                        if (acctRecordType.toLowerCase().contains('enterprise')){
                            FeedItem tempFeed = new FeedItem(Body='Congratulations to ' + salesRepName + ' for closing '+ accountName +' for ' + nt.formatDouble(myNewOpp.Won_MRC1__c, true, 'en') + ' in MRR, with a TCV of ' + nt.formatDouble(myNewOpp.Won_Total_Contract_Value_TCV1__c, true, 'en') + '!', ParentId = grpRec[0].Id, Type='TextPost');
                            feedsToUpdate.add(tempFeed);
                        }
                    }   
                }
            }
            if(!feedsToUpdate.isEmpty())
                insert feedsToUpdate;
         }
        /* End - Bang the Gong */
        
        /* Start - Access Type Report */
        if (!Test.isRunningTest()){
            try{
                // Added filter for Business_Unit__c='Wireline'
                Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();
                for(Opportunity opp : listOpp){
                    if(opp.IsWon == true && opp.SyncedQuoteId!= NULL)
                        oppMap.put(opp.id,opp);
                }
                System.debug('*******3*****');
                List<Opportunity> oppList = oppMap.values();
                
                Set<Id> syncedQuoteIds = new Set<Id>();
                
                for (Opportunity o : oppList){
                    syncedQuoteIds.add(o.SyncedQuote.Id);
                }
                
                List<Access_Type_Sales__c> accessTypeRows = new List<Access_Type_Sales__c>();
                Map<String, Set<QuoteLineItem>> mapAccessTypeRevised = new Map<String, Set<QuoteLineItem>>();                 
                Map<String, QuoteLineItem> mapAccessTypeRevisedAccess = new Map<String, QuoteLineItem>();
                Map<Id, QuoteLineItem> qliMap = new Map<Id, QuoteLineItem>();  
                Map<Id, String> qliAccessRevisedMap = new Map<Id, String>();              
                Map<Id, String> qliCarrierRevisedMap = new Map<Id, String>();

                for (List<QuoteLineItem> qlis : [SELECT Id, Type__c, Quote.OpportunityId, Site__r.Id, Site__r.Display_Name__c, Site__r.ServiceableLocation__r.Access_Type__c, Charge_Type__c, TotalPrice, Service_Type__c, UPC_L1__c,UPC_L2__c,UPC_L3__c,UPC_L4__c,UPC_L5__c, Carrier_Name__c, PricebookEntry.Product2.Name, PricebookEntry.Product2.Access_Type__c, PricebookEntry.Product2.Access_Type_Group__c, PricebookEntry.Product2.Category__c FROM QuoteLineItem WHERE QuoteId IN :syncedQuoteIds]){
                    // Create the map with the Access/site as the key
                    for (QuoteLineItem qli2 : qlis){
                        qliMap.put(qli2.Id, qli2);
                        if (qli2.PricebookEntry.Product2.Category__c.equalsIgnoreCase('ACCESS')){
                            // we found an access so we will add it to the map and the list - if it is already there and this is not offnet we can override the access 
                            if (mapAccessTypeRevisedAccess.get(qli2.Site__r.Id + '~' +qli2.PricebookEntry.Product2.Access_Type_Group__c)== null || !qli2.PricebookEntry.Product2.Access_Type__c.toUpperCase().startsWith('OFF')){
                                Set<QuoteLineItem> listItem = new Set<QuoteLineItem>();
                                listItem.add(qli2);
                                mapAccessTypeRevisedAccess.put(qli2.Site__r.Id + '~' +qli2.PricebookEntry.Product2.Access_Type_Group__c, qli2);
                                mapAccessTypeRevised.put(qli2.Site__r.Id + '~' +qli2.PricebookEntry.Product2.Access_Type_Group__c, listItem);
                            }
                        }                        
                    }
                    
                    // Add all of the other items to the set and tie it to the site/access in the map.
                    for (QuoteLineItem qli2 : qlis){
                        // This is not an access and is not in the map already
                        if (mapAccessTypeRevised.get(qli2.Site__r.Id + '~' +qli2.PricebookEntry.Product2.Access_Type_Group__c) != null){
                            // parent line item
                            QuoteLineItem lineItem = mapAccessTypeRevisedAccess.get(qli2.Site__r.Id + '~' +qli2.PricebookEntry.Product2.Access_Type_Group__c);
                            if (lineItem != null && lineItem.Id != qli2.Id){
                                Set<QuoteLineItem> qliSet = mapAccessTypeRevised.get(qli2.Site__r.Id + '~' +qli2.PricebookEntry.Product2.Access_Type_Group__c);
                                qliSet.add(qli2);
                                mapAccessTypeRevised.put(qli2.Site__r.Id + '~' +qli2.PricebookEntry.Product2.Access_Type_Group__c, qliSet);
                                
                                QuoteLineItem qli = mapAccessTypeRevisedAccess.get(qli2.Site__r.Id + '~' +qli2.PricebookEntry.Product2.Access_Type_Group__c);
                                qliAccessRevisedMap.put(qli2.Id, (qli.Site__r.ServiceableLocation__r != null)? qli.Site__r.ServiceableLocation__r.Access_Type__c : qli.PricebookEntry.Product2.Access_Type__c);
                                qliCarrierRevisedMap.put(qli2.Id, qli.Carrier_Name__c);
                            }
                        }
                    }
                    
                    for (QuoteLineItem qli : qlis){
                    system.debug('testing: ' + qlis);
                        Access_Type_Sales__c temp = new Access_Type_Sales__c();
                        temp.Quote__c = qli.QuoteId;
                        temp.isNew__c = true;
                        temp.New_Renewal__c = qli.Type__c;
                        temp.Service_Description__c = qli.Service_Type__c;
                        temp.UPC_L1__c = qli.UPC_L1__c;
                        temp.UPC_L2__c = qli.UPC_L2__c;
                        temp.UPC_L3__c = qli.UPC_L3__c;
                        temp.UPC_L4__c = qli.UPC_L4__c;
                        temp.UPC_L5__c = qli.UPC_L5__c;
                        temp.Opportunity__c = qli.Quote.OpportunityId;
                        temp.Account__c = oppMap.get(qli.Quote.OpportunityId).AccountId;
                        temp.Carrier_Name__c = qli.Carrier_Name__c;
                        temp.Carrier_Name_Revised__c = (qliCarrierRevisedMap.get(qli.Id)!=NULL)?qliCarrierRevisedMap.get(qli.Id):temp.Carrier_Name__c;
                        temp.Product_Name1__c = qli.PricebookEntry.Product2.Name;                        
                        temp.Opportunity_Age_in_Days__c = (oppMap.get(qli.Quote.OpportunityId).CreatedDate.date()).daysBetween(oppMap.get(qli.Quote.OpportunityId).CloseDate);
                        temp.Account_Owner_Name__c = oppMap.get(qli.Quote.OpportunityId).Account.Owner.Name;
                        temp.Access_Type__c = (qli.Site__r.ServiceableLocation__r != null)? qli.Site__r.ServiceableLocation__r.Access_Type__c : qli.PricebookEntry.Product2.Access_Type__c;
                        temp.Access_Type__c = (temp.Access_Type__c.startsWith('ONNET') || temp.Access_Type__c.startsWith('ON NET'))?'ON NET' : temp.Access_Type__c;  
                        temp.Access_Type_Revised__c = (qliAccessRevisedMap.get(qli.Id)!=NULL)?qliAccessRevisedMap.get(qli.Id):temp.Access_Type__c;
                        temp.Access_Type_Revised__c = (temp.Access_Type_Revised__c.startsWith('ONNET') || temp.Access_Type_Revised__c.startsWith('ON NET'))?'ON NET' : temp.Access_Type_Revised__c;
                        temp.MRR_Price__c = (qli.Charge_Type__c == 'MRC')?qli.TotalPrice : 0;
                        temp.NRR_Price__c = (qli.Charge_Type__c == 'NRC')?qli.TotalPrice : 0;
                        temp.Sales_Rep_Name__c = oppMap.get(qli.Quote.OpportunityId).Owner.Name; 
                        temp.Site__c = qli.Site__r.Display_Name__c;
                        accessTypeRows.add(temp);
                    }
                }
                
                INSERT accessTypeRows;
            }catch(Exception ex){
                // Update the Access Type Report
                system.debug('reason failing: '+ ex.getMessage());
                System.debug('Issue with updating the Sales Access Report Records.');
                Utils.sendEmail(ex.getMessage(), 'OpportunityTrigger_AU_AI.trigger: Error Message - Issue with updating the Sales Access Report Records.', new String[] {Label.adminEmail});
            }
        }
        /* End - Access Type Report */
        
        
                
        // Using the opportunities in this trigger we will update the oppWON field so that we can use it to fire a WF rule only 
        // when the Opp has been changed to one.  We cannot access the isWON field for the Opp in a WF Rule meant for a quote. 
        List<Quote> quotesToUpdate = new List<Quote>();
        try{
            List<Id> ids = new List<Id>(newOppMap.keySet());
            List<Opportunity> opps1 = new List<Opportunity>();
            opps1.addall(listOpp); // PS: Added filter for Business_Unit__c='Wireline'
            System.debug('*******4*****');
            // We now have the quotes that are linked to the Opp and we can set the OppWon field to fire the WF Rule.
            for (Opportunity o1 : opps1){
                for (Quote q : o1.Quotes){
                    if (q.oppWON__c != o1.isWON){
                        q.oppWON__c = o1.isWON;
                        quotesToUpdate.add(q);
                    }
                }
            }
            if (!quotesToUpdate.isEmpty())  
                UPSERT quotesToUpdate;
        }catch(DMLException ex){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error updating the Quote.  Contact your administrator with the quote Id.');
            ApexPages.addMessage(myMsg);
            Utils.sendEmail(ex.getMessage(), 'OpportunityTrigger_AU_AI.trigger: Error Message', new String[] {Label.adminEmail});
            return;
        }

        // Obtain the Opportunities in the state that we need them - as the trigger only has the first level a data in it
        // These are the opportunities that have recently been set to WON Stage
       
        List<Opportunity> opps = new List<Opportunity>();
            opps.addall(listOpp); 
        // Obtain a list of quote ids that are the synched quotes for the opps that we need to deal with as they are recently WON
        // These can then be used to obtain the quoteSites that we need to deal with when creating the Active Sites for the Account 
        // as a result of this opportunity being WON
        List<Id> quoteIds = new List<Id>();
        for (Opportunity o : opps){
            quoteIds.add(o.SyncedQuoteId);
        } 
        
        List<Quote_Site__c> quoteSites = [SELECT Id, Quote__c, Site__c, Site__r.LocationKey_del__c, Site__r.Account__r.Name, Site__r.Account__r.BillingCity, Site__r.Account__r.BillingPostalCode, Site__r.Account__r.BillingCountry, Site__r.Account__r.BillingStreet, Site__r.Account__r.BillingState, Site__r.Access_Type__c, Site__r.Account__c, Site__r.Access_Type_Group__c, Site__r.Suite_Floor__c, Site__r.Street_Number__c, Site__r.Street_Name__c, Site__r.Street_Type__c, Site__r.Street_Direction__c, Site__r.City__c, Site__r.Postal_Code__c, Site__r.Province_Code__c, Site__r.Country__c, Site__r.Display_Name__c FROM Quote_Site__c WHERE Quote__r.isSyncing = true AND Quote__c IN :quoteIds];
              
        // Map with QuoteId as the key and the quotesites as the value
        Map<Id, List<Quote_Site__c>> qSiteMap = new Map<Id, List<Quote_Site__c>>(); 
        for (Quote_Site__c qSite : quoteSites){
            List<Quote_Site__c> qSites;
            if (qSiteMap.get(qSite.Quote__c) == null){
                qSites = new List<Quote_Site__c>();
            }else{
                qSites = qSiteMap.get(qSite.Quote__c);
            }
            qSites.add(qSite);
            qSiteMap.put(qSite.Quote__c, qSites);
        }
        
        
        // Get the Active Sites that are currently already in the system
        // We can use the new activesitekey which is a concatenation of the siteloc key and the acct id
        // to see if it currently exists.  Those are the ones that we will update and not insert.
        
        List<String> aSiteKeyList = new List<String>();
        system.debug(quoteSites);
        for (Quote_Site__c qs : quoteSites){
            aSiteKeyList.add(qs.Site__r.LocationKey_del__c + ((String)(qs.Site__r.Account__c)).substring(0,15));
        }
        System.debug('*******5*****');
        //  Create a set we can use to quickly check if a aSite curretly exists already.
        Map<Id, Active_Site__c> existingActiveSiteMap = new Map<Id, Active_Site__c>([SELECT Id, ActiveSiteKey__c, Site__c, Site__r.LocationKey_del__c, Site__r.Account__c, (SELECT Id, MRR__c,activeProductKey__c, Product__c, Active_Sites__c FROM Active_Products__r) FROM Active_Site__c WHERE ActiveSiteKey__c IN :aSiteKeyList]);
              
        // This map can be used to see if we need to create the active products later on in the trigger.
        // The key is the siteKey and the value is a list of the product keys.
        Map<String, Set<String>> aProductKeyMap = new Map<String, Set<String>>();
        Map<String, Active_Product__c> aProductsMap = new Map<String, Active_Product__c>(); 
        
        Set<String> aSiteKeys = new Set<String>();
        for (Active_Site__c aSite : existingActiveSiteMap.values()){
            aSiteKeys.add(aSite.ActiveSiteKey__c);
            Set<String> aProductKeys = new Set<String>();
            for (Active_Product__c ap : aSite.Active_Products__r){
                aProductKeys.add(ap.activeProductKey__c);
                aProductsMap.put(ap.activeProductKey__c, ap);
            }
            aProductKeyMap.put(aSite.ActiveSiteKey__c, aProductKeys);
        }
        
        // Need the QuoteLieItems with the Site and Quote
        Set<Id> siteIds = new Set<Id>();
        
        // These are the active Sites that we will need to insert
        List<Active_Site__c> activeSitesToInsert = new List<Active_Site__c>();
        List<Active_Site__c> activeSitesToUpdate = new List<Active_Site__c>();
        List<Active_Site__c> allActiveSites = new List<Active_Site__c>();
        Active_Site__c aSite = new Active_Site__c(); 
        for (Opportunity o : opps){
            // Get the quote Sites for the synched Quote on this opp.
            // Create Active Sites for each of these.
            List<Quote_Site__c> qSites = qSiteMap.get(o.SyncedQuoteId);
            if (qSites!=null){
                for (Quote_Site__c qs : qSites){
                    // Keep track of the Prospect Sites that we created Active Sites for so we can obtain their products
                    // and then create Active Products from them
                    siteIds.add(qs.Site__c);
                    aSite = new Active_Site__c();
                    aSite.Site__c = qs.Site__c;
                    aSite.Site__r = qs.Site__r;
                    aSite.Name = qs.Site__r.Display_Name__c;
                    aSite.Access_Type__c = qs.Site__r.Access_Type__c;
                    aSite.Access_Type_Group__c = qs.Site__r.Access_Type_Group__c;
                    aSite.Service_City__c = qs.Site__r.City__c;
                    aSite.Service_Country__c = qs.Site__r.Country__c;
                    aSite.Service_Postal_Code__c = qs.Site__r.Postal_Code__c;
                    aSite.Service_Street_Number__c = qs.Site__r.Street_Number__c; 
                    aSite.Service_Suite_Floor__c = qs.Site__r.Suite_Floor__c;
                    aSite.Service_Province__c = qs.Site__r.Province_Code__c;
                    aSite.Service_Street_Direction__c = qs.Site__r.Street_Direction__c;
                    aSite.Service_Street_Name__c = qs.Site__r.Street_Name__c;
                    aSite.Service_Street_Type__c = qs.Site__r.Street_Type__c;
                    aSite.Account__c = qs.Site__r.Account__c;
                    aSite.City_Address__c = qs.Site__r.account__r.BillingCity;
                    aSite.Postal_Zip_Code__c = qs.Site__r.account__r.BillingPostalCode;
                    aSite.Country__c = qs.Site__r.account__r.BillingCountry;
                    aSite.State_Province_Code__c = qs.Site__r.account__r.BillingState;
                    aSite.Street_Address__c = qs.Site__r.account__r.BillingStreet;                      
                    
                    if (!aSiteKeys.contains(qs.Site__r.LocationKey_del__c + '' + ((String)(qs.Site__r.Account__c)).substring(0,15))){
                        activeSitesToInsert.add(aSite);
                    }else{
                        activeSitesToUpdate.add(aSite);
                    }
                }
            }
        }
        
        INSERT activeSitesToInsert;
        
        allActiveSites.addAll(activeSitesToInsert);
        allActiveSites.addAll(existingActiveSiteMap.values());
        
        // Create a Map with the Site Id as the Key and the ActiveSite as the Value
        // We will use this map when we need to set the Active Site when creating the Active Products. 
        Map<String, Active_Site__c> activeSiteMap = new Map<String, Active_Site__c>();
        for (Active_Site__c aSiteTemp : allActiveSites){
           // activeSiteMap.put(aSiteTemp.Site__c, aSiteTemp);
           activeSiteMap.put(aSiteTemp.Site__r.LocationKey_del__c + ((String)(aSiteTemp.Site__r.Account__c)).substring(0,15), aSiteTemp);
        }
        
        List<Id> siteIdList = new List<Id>(siteIds);
        List<Id> quoteIdList = new List<Id>(quoteIds);
        
        // Create the active products based upon the quote line items on that quote.
        List<QuoteLineItem> qlItems = [SELECT Id, QuoteId, Site__c, TotalPrice, Charge_Type__c, PriceBookEntry.Product2Id, Site__r.LocationKey_del__c, Site__r.Account__c FROM QuoteLineItem WHERE Site__c IN :siteIdList AND QuoteId IN :quoteIdList];
        Set<Id> duplicateCheck = new Set<Id>();
        List<Active_Product__c> newActiveProducts = new List<Active_Product__c>();
        List<Active_Product__c> existingActiveProducts = new List<Active_Product__c>();
        for (QuoteLineItem qli : qlItems){
            Active_Site__c a = activeSiteMap.get(qli.Site__r.LocationKey_del__c + '' + ((String)(qli.Site__r.Account__c)).substring(0,15));
            if (a!=null){
                Set<String> products = null;
                
                if (aProductKeyMap != null)
                    products = aProductKeyMap.get(a.Site__r.LocationKey_del__c + ((String)a.Site__r.Account__c).substring(0,15));
                
                // We only add the new new Active Products.
                if ((products==null)||(!products.contains(((String)a.Id).substring(0,15) +  ((String)qli.PricebookEntry.Product2Id).substring(0,15)))){
                    Active_Product__c ap = new Active_Product__c(Active_Sites__c = a.Id, Product__c = qli.PricebookEntry.Product2Id);
                    if (qli.Charge_Type__c.equals('MRC')){
                        ap.MRR__c = qli.TotalPrice;
                    }
                    newActiveProducts.add(ap);
                }else{
                    // get the Active Product and update the MRR
                //  System.debug(aProductsMap);
                //  System.debug('Key: ' + ((String)a.Id).substring(0,15) +  ((String)qli.PricebookEntry.Product2Id).substring(0,15));
                    Active_Product__c ap = aProductsMap.get(((String)a.Id).substring(0,15) +  ((String)qli.PricebookEntry.Product2Id).substring(0,15));
                    
                    
                    if (qli.Charge_Type__c.equals('MRC')){
                        ap.MRR__c += qli.TotalPrice;
                    }
                    if (!duplicateCheck.contains(ap.Id)){
                        duplicateCheck.add(ap.Id);
                        existingActiveProducts.add(ap);
                    }
                } 
            }
        }
        if (newActiveProducts != null)
            UPSERT newActiveProducts;
        if (existingActiveProducts != null)
            UPSERT existingActiveProducts;
    }else{
        // Get all of the active sites if there are any and add these as prospect sites.
        List<Id> acctIds = new List<Id>();
        for (Opportunity o: Trigger.new){
            acctIds.add(o.AccountId);
        }
        
        List<A_Z_Site__c> oldAZSites = new List<A_Z_Site__c>();
        List<A_Z_Site__c> newAZSites = new List<A_Z_Site__c>();
        List<Id> siteAIds = new List<Id>();
        List<Active_Site__c> activeSites = null;
        Map<Id, List<Active_Site__c>> aSiteMap = new Map<Id, List<Active_Site__c>>();
        try {
            activeSites = [SELECT Id, Account__c, Site__r.Access_Type__c, Site__r.Access_Type_Group__c, Site__r.City__c,
                        Site__r.Country__c, Site__r.Postal_Code__c, Site__r.Street_Number__c, Site__r.Province_Code__c, Site__r.Suite_Floor__c,
                        Site__r.Street_Direction__c, Site__r.Street_Name__c, Site__r.Street_Type__c, Site__r.Account__c, Site__r.Z_Site__r.Suite_Floor__c,
                        
                        Site__r.Z_Site__r.Country__c,Site__r.Z_Site__r.Postal_Code__c,Site__r.Z_Site__r.Street_Number__c,Site__r.Z_Site__r.Province_Code__c,
                        Site__r.Z_Site__r.Street_Direction__c,Site__r.Z_Site__r.Street_Name__c,Site__r.Z_Site__r.Street_Type__c,Site__r.Z_Site__r.City__c,
                        
                        
                        Site__r.ServiceableLocation__c,  Site__r.UniqueKey__c, Site__r.LocationKey_del__c, Site__r.CLLI_SWC__c, Site__r.Is_a_Z_Site__c, Site__r.Z_Site__c,
                        Site__r.Type__c, Site__r.Approved__c FROM Active_Site__c WHERE Account__c IN :acctIds];
                        
            for (Active_Site__c activeSite : activeSites){
                if (!activeSite.Site__r.Is_a_Z_Site__c){
                    siteAIds.add(activeSite.Site__c);
                }
            }
            
            oldAZSites = [SELECT Site_A__c, Site_Z__c FROM A_Z_Site__c WHERE Site_A__c IN :siteAIds];
            
            List<Active_Site__c> aSites;
            if (activeSites!=null){
                for (Active_Site__c aSite : activeSites){
                    if (aSiteMap.get(aSite.Account__c)==null){
                        aSites = new List<Active_Site__c>();
                    }else{
                        aSites = aSiteMap.get(aSite.Account__c);
                    }
                    aSites.add(aSite);
                    aSiteMap.put(aSite.Account__c, aSites);
                }
            }
        }catch(Exception ex){
            
        }

        // We now have the Active Sites by Account
        List<Site__c> pSites = new List<Site__c>();
        List<Site__c> pZSites = new List<Site__c>();
        Map<Id, String> oldIdToUniqueKeyA = new Map<Id, String>();    // Added ability to have more then one Z Site on a A Site.
        Map<Id, String> oldIdToUniqueKeyZ = new Map<Id, String>();    // Added ability to have more then one Z Site on a A Site.
        
        Map<String, Id> uniqueKeyToNewIdA = new Map<String, Id>();
        Map<String, Id> uniqueKeyToNewIdZ = new Map<String, Id>();
        Map<String, String> uniqueKeyAToUniqueKeyZ = new Map<String, String>();
        for (Opportunity o: Trigger.new){
            if (o.RecordTypeId != '012V0000000Co6x'){
                // Go through each of the Opportunities and add create a Prospect Site (tie it to the opp - based upon the Active Sites)
                List<Active_Site__c> aSitesToAdd = aSiteMap.get(o.AccountId);
                if (aSitesToAdd!=null){
                    // Let's create the Prospect Sites and add it to the list
                    
                    for (Active_Site__c a : aSitesToAdd){
                        Site__c site1 = new Site__c();
                        
                        site1.opportunity__c = o.Id;
                        site1.Access_Type__c = a.Site__r.Access_Type__c;
                        site1.Access_Type_Group__c = a.Site__r.Access_Type_Group__c;
                        site1.City__c = a.Site__r.City__c;
                        site1.Country__c = a.Site__r.Country__c;
                        site1.Postal_Code__c = a.Site__r.Postal_Code__c;
                        site1.Street_Number__c = a.Site__r.Street_Number__c;
                        site1.Suite_Floor__c = a.Site__r.Suite_Floor__c;
                        site1.Province_Code__c = a.Site__r.Province_Code__c;
                        site1.Street_Direction__c = a.Site__r.Street_Direction__c;
                        site1.Street_Name__c = a.Site__r.Street_Name__c;
                        site1.Street_Type__c = a.Site__r.Street_Type__c;
                        site1.Account__c = a.Site__r.Account__c;
                        site1.ServiceableLocation__c = a.Site__r.ServiceableLocation__c;
                        site1.CLLI_SWC__c = a.Site__r.CLLI_SWC__c;
                        site1.Is_a_Z_Site__c = a.Site__r.Is_a_Z_Site__c;
                        site1.Type__c = a.Site__r.Type__c;
                        site1.Approved__c = a.Site__r.Approved__c;
                       
                        if (!site1.Is_a_Z_Site__c){
                            oldIdToUniqueKeyA.put(a.Site__c, a.Site__r.Suite_Floor__c + a.Site__r.Street_Number__c + a.Site__r.Street_Name__c + a.Site__r.Street_Type__c+ a.Site__r.Street_Direction__c + a.Site__r.City__c + a.Site__r.Province_Code__c+ a.Site__r.Postal_Code__c + '' + site1.opportunity__c); 
                            pSites.add(site1);
                        }else{
                            oldIdToUniqueKeyZ.put(a.Site__c, a.Site__r.Suite_Floor__c + a.Site__r.Street_Number__c + a.Site__r.Street_Name__c + a.Site__r.Street_Type__c+ a.Site__r.Street_Direction__c + a.Site__r.City__c + a.Site__r.Province_Code__c+ a.Site__r.Postal_Code__c + '' + site1.opportunity__c);
                            pZSites.add(site1);
                        }
                    }
                }
            }
        }
        
        
        //  There may be a point where the site exists already.
        Database.SaveResult[] pSiteResults = Database.Insert(pSites, false);
        Database.SaveResult[] pzSiteResults = Database.Insert(pZSites, false);
                        
        //  INSERT pSites;    // We will insert the sites now since we do not link the Z sites yet
        //  INSERT pZSites;
        
        
        
        
        // we need to add the AZSite Junction object, when we add these the trigger will update the Z_Sites.  We no longer need to set the Z_Site__c field        
      
        if (!pZSites.isEmpty()){
            for (Site__c ps : pZSites){
                uniqueKeyToNewIdZ.put(ps.Suite_Floor__c + ps.Street_Number__c + ps.Street_Name__c + ps.Street_Type__c+ ps.Street_Direction__c + ps.City__c + ps.Province_Code__c+ ps.Postal_Code__c + '' + ps.opportunity__c, ps.Id);
            }
        }
        
        if (!pSites.isEmpty()){
            for (Site__c ps : pSites){
                uniqueKeyToNewIdA.put(ps.Suite_Floor__c + ps.Street_Number__c + ps.Street_Name__c + ps.Street_Type__c+ ps.Street_Direction__c + ps.City__c + ps.Province_Code__c+ ps.Postal_Code__c + '' + ps.opportunity__c, ps.Id);
            }
        }
        
        System.debug('Old AZ Sites' + oldAZSites);
        
        for (A_Z_Site__c az : oldAZSites){
            A_Z_Site__c newAZSite = new A_Z_Site__c();
            newAZSite.Site_A__c = uniqueKeyToNewIdA.get(oldIdToUniqueKeyA.get(az.Site_A__c));
            newAZSite.Site_Z__c = uniqueKeyToNewIdZ.get(oldIdToUniqueKeyZ.get(az.Site_Z__c));
            if (newAZSite.Site_A__c != null && newAZSite.Site_Z__c != null)
                newAZSites.add(newAZSite);
        }
        
        if (!newAZSites.isEmpty())
            INSERT newAZSites;
        
    }
    }
}