trigger SiteTrigger_AU_AI on Site__c (after insert, after update) {
	if(Label.isTriggerActive.equalsIgnoreCase('true')){
//	if(!Test.isRunningTest()){
		Map<Id, Site__c> oldMap = Trigger.oldMap;
		
		List<Id> zSiteIds = new List<Id>();
		Map<Id, String> azSiteMap = new Map<Id, String>();
		
		
		if (Trigger.isInsert){
			for (Site__c s : Trigger.New){
				if (s.Is_a_Z_Site__c){
					zSiteIds.add(s.Id);
				}	
			}
		}else{
			for (Site__c s : Trigger.New){
				/* This is a Z Site and the Display Name changed */
				/* As a result, we need to ensure that the A Site has its Z Site display names updated. */
				if ((s.Is_a_Z_Site__c)/* && (s.Display_Name__c != oldMap.get(s.Id).Display_Name__c)*/){
				   zSiteIds.add(s.Id);
				}
			}
		}
		
		// We have all of the Z Site Ids that we need the A_Z_Sites for.
		List<A_Z_Site__c> azSitesTemp = [SELECT Site_A__c, Site_Z__c, Site_Z__r.Display_Name__c FROM A_Z_Site__c WHERE Site_Z__c IN :zSiteIds];
		List<Id> aSiteIds = new List<Id>();
		for (A_Z_Site__c azTemp : azSitesTemp){
			aSiteIds.add(azTemp.Site_A__c);
		}
//		if(!Test.isRunningTest()){
			List<A_Z_Site__c> azSites = [SELECT Site_A__c, Site_Z__c, Site_Z__r.Display_Name__c FROM A_Z_Site__c WHERE Site_A__c IN :aSiteIds];
			
			
			for (A_Z_Site__c az : azSites){
				String zSiteList = azSiteMap.get(az.Site_A__c);
		        if (Utils.isEmpty(zSiteList)){
		        	zSiteList = '';
		        }
		        
		        zSiteList += (az.Site_Z__r.Display_Name__c + '\n');
		        azSiteMap.put(az.Site_A__c, zSiteList);
		    }
			
			List<Site__c> aSites = [SELECT Id, Z_Sites__c FROM Site__c WHERE Id IN :aSiteIds];
		
			for (Site__c a : aSites){
				a.Z_Sites__c = azSiteMap.get(a.Id);
			}
		
			if (!aSites.isEmpty())
				UPDATE aSites;
//		}
	}
}