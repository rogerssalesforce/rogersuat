/**************************************************************************************************************************
Class Name      : CPQ_PricebookTrigger
Description     : This Trigger calls the helper class 'CPQ_PricebookTriggerHelper' 
Created By      : Alina Balan
Created Date    : 04-November-2015
Modification Log:
--------------------------------------------------------------------------------------------------------------------------
Developer                   Date                     Description
------------------------------------------------------------------------------------------------------------------------- 
Alina Balan            04-November-2015              Original version
**************************************************************************************************************************/
trigger CPQ_PricebookTrigger on Pricebook2 (before delete) {
    if (trigger.isDelete && trigger.isBefore) {
        CPQ_PricebookTriggerHelper.preventDummyPricebookDeletion(trigger.old);
    }
}