/**************************************************************************************************************************
Class Name      : CPQ_OpportunityProduct 
Description     : This Trigger calls the helper class 'CPQ_OpportunityProductHelper' 
Created By      : Deepika Rawat   
Created Date    : 24-Nov-2015 
Modification Log:
----------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
-----------------------------------------------------------------------------------------------------------  
Deepika Rawat            24-Nov-2015            Initial version.                                               
**************************************************************************************************************************/
trigger CPQ_OpportunityProduct on OpportunityLineItem (after insert) {
   if(trigger.isAfter && trigger.isInsert){
       //Call helper class method to calculate  TCV
       CPQ_OpportunityProductHelper.calculateTCV(trigger.new);
   }

}