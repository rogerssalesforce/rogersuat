/*
 * Description: CarrierQuote after insert/update
 *          
 *      Only one can be selected
 *      Assumption: no two Carrier Quote of the same Quote Request are selected at the same time/batch
 * Initial Version: 20120602
 * Last Update: 20120602
 *
 */
trigger CarrierQuoteTrigger_AU_AI on Carrier_Quote__c (after insert, after update) {
     if(Label.isTriggerActive.equalsIgnoreCase('true')){
    /*
    if (TriggerHelperClass.firstRunOn_CarrierQuote_AU_AI)
    {
        TriggerHelperClass.firstRunOn_CarrierQuote_AU_AI = false;
    
    Set<Id> quoteRequestIds = new Set<Id>();
    Set<Id> newSelectedCarrierQuoteIds = new Set<Id>();
    Map<Id,Carrier_Quote__c> quoteRequestToNewCarrierQuoteMap = new Map<Id,Carrier_Quote__c>();
    
    for (Carrier_Quote__c cQuote : trigger.new){
        Carrier_Quote__c oldCQuote;
        if (trigger.isUpdate) {
            oldCQuote = trigger.oldMap.get(cQuote.Id);
        }
        if ( (cQuote.Selected__c == true)
            && ( trigger.isInsert
                || (oldCQuote.Selected__c == false)) )
        {
            quoteRequestIds.add(cQuote.Quote_Request__c);
            newSelectedCarrierQuoteIds.add(cQuote.Id);
            quoteRequestToNewCarrierQuoteMap.put(cQuote.Quote_Request__c, cQuote);
        } else if ((cQuote.Selected__c == false)
                && (trigger.isUpdate)
                && (oldCQuote.Selected__c == true)) {
            // this is for the case a carrier quote is change from Selected to not Selected
            quoteRequestIds.add(oldCQuote.Quote_Request__c);
        }
    }
    
    // update the Quote Request to point to the new one
    List<Quote_Request__c> quoteRequestList = [select Id, Selected_Carrier_Quote__c
            , MRC__c, NRC__c, Build_Cost__c
        from Quote_Request__c
        where Id in : quoteRequestIds];
    List <Quote_Request__c> updatedQuoteRequests = new List<Quote_Request__c>();
    //System.assertEquals( 1, quoteRequestList.size());
    
    for (Quote_Request__c qRequest : quoteRequestList) {
        Carrier_Quote__c newSelectedCR = quoteRequestToNewCarrierQuoteMap.get(qRequest.Id);
        if (newSelectedCR == null){
            qRequest.Selected_Carrier_Quote__c = null;
            qRequest.Carrier_Name__c = null;
            qRequest.MRC__c = null;
            qRequest.NRC__c = null;
            qRequest.Build_Cost__c = null;
            updatedQuoteRequests.add(qRequest);
        } else if (qRequest.Selected_Carrier_Quote__c != newSelectedCR.Id) 
        {
            qRequest.Selected_Carrier_Quote__c = newSelectedCR.Id;
            qRequest.Carrier_Name__c = newSelectedCR.Carrier_Name__c;
            qRequest.MRC__c = newSelectedCR.MRC__c;
            qRequest.NRC__c = newSelectedCR.NRC__c;
            qRequest.Build_Cost__c = newSelectedCR.Build_Cost__c;
            updatedQuoteRequests.add(qRequest);
        }
    }
    update updatedQuoteRequests;
    
    // change the sibling of the newly selected carrier quote
    List<Carrier_Quote__c> oldSelectedCQ = [select Id, Selected__c, Quote_Request__c
        from Carrier_Quote__c
        where Selected__c = true
            and Quote_Request__c in : quoteRequestIds
            and Id not in : newSelectedCarrierQuoteIds ];
    for (Carrier_Quote__c oldCQ : oldSelectedCQ) {
        oldCQ.Selected__c = false;
    }
    update oldSelectedCQ;
    }
    */
    }
}