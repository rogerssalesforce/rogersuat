/**************************************************************************************************************************
Class Name      : CPQ_AgreementLineItemTrigger 
Description     : This Trigger calls the helper class 'CPQ_AgreementLine_Trigger_Helper' 
Created By      : Mitali Telang
Created Date    : 28-Sept-15
Modification Log:
--------------------------------------------------------------------------------------------------------------------------
Developer                   Date                     Description
------------------------------------------------------------------------------------------------------------------------- 
Mitali Telang           28-Sept-15         US-0048. Updating the Product Schedules on Agreement Lines
Akshay Ahluwalia        14-Dec-15          US-152. Added Method to Populate CSOW flag on Agreement 
                                           if any product has CSOW Required Checked
**************************************************************************************************************************/

trigger CPQ_AgreementLineItemTrigger on Apttus__AgreementLineItem__c (after insert, before insert) {

    if(Trigger.isAfter){
        if(Trigger.isInsert){
            CPQ_AgreementLine_Trigger_Helper.findProductSchedulesForAgreement(trigger.New);
            CPQ_AgreementLine_Trigger_Helper.populateCSOWoNAgreement(trigger.New);
        }
    }
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            CPQ_AgreementLine_Trigger_Helper.includeProductSchedulesForAgreementLine(trigger.New);
        }
    }

}