/**************************************************************************************************************************
Class Name      : CPQ_COPTrigger
Description     : This Trigger calls the helper class 'CPQ_COPTriggerHelper' 
Created By      : Aakanksha Patel
Created Date    : 23-Nov-2015
Modification Log:
--------------------------------------------------------------------------------------------------------------------------
Developer                   Date                     Description
------------------------------------------------------------------------------------------------------------------------- 
Aakanksha Patel           23-Nov-2015         (US-0351) Updating the 'Latest Wireless COP' field on the Account

**************************************************************************************************************************/
trigger CPQ_COPTrigger on COP__c (after update, after insert) 
{
    if(trigger.isAfter && trigger.isUpdate )
    { 
         CPQ_COPTriggerHelper.updateWirelessCOPAccount(trigger.new , trigger.OldMap);
    }
 
}