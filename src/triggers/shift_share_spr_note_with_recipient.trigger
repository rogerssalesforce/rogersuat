trigger shift_share_spr_note_with_recipient on SPR_Note__c (after insert, after update) {
if(Label.isTriggerActive.equalsIgnoreCase('true')){
    /*
    "- Recipient 1 on SPR note will have a ""Request Ack"" checkbox next to it.
    - If that is checked, Recipient 1 will receive a notification that he has to ACK on it. 
    - Recipient 1 will automatically have edit access to that Request Ack checkbox (only).
    - If the Note is Private and that the Ack Recipient does not originally have access to that private note, then nothing should happen. 
        (No notification, no sharing to the recipient)
    - It is ok that the sharing rule remains after the recipient uncheck Request ACK. (would disappear from the list view at least)". 

    Developed by Jocsan Diaz on November 2nd, 2012
    */

    // share SPR notes. assuming one record gets to the trigger. this cannot be a bulk trigger.
    if (trigger.isInsert) {
        if (trigger.new.get(0).Recipient_1__c != null && trigger.new.get(0).Request_ACK__c == true) {
            shift_share_spr_note_support.theShareSPRNote(trigger.new.get(0).Id);
        }  
    } else {
        if ((trigger.new.get(0).Recipient_1__c != trigger.old.get(0).Recipient_1__c) || (trigger.new.get(0).Request_ACK__c != trigger.old.get(0).Request_ACK__c)) {
            if (trigger.new.get(0).Recipient_1__c != null && trigger.new.get(0).Request_ACK__c == true) {
                shift_share_spr_note_support.theShareSPRNote(trigger.new.get(0).Id);
            }      
        }    
    }
  }
}