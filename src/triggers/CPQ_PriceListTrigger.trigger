/**************************************************************************************************************************
Class Name      : CPQ_PriceListTrigger
Description     : This Trigger calls the helper class 'CPQ_PriceListTriggerHelper' 
Created By      : Ramiya Kandeepan
Created Date    : 19-November-2015
Modification Log:
--------------------------------------------------------------------------------------------------------------------------
Developer                   Date                     Description
------------------------------------------------------------------------------------------------------------------------- 
Ramiya Kandeepan            19-November-2015              Original version
**************************************************************************************************************************/
trigger CPQ_PriceListTrigger on Apttus_Config2__PriceList__c (before delete) {
    if(trigger.isDelete && trigger.isBefore) {
        CPQ_PriceListTriggerHelper.preventPriceListDeletion(trigger.old);
    }
}