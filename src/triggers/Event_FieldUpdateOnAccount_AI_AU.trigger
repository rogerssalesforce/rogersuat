trigger Event_FieldUpdateOnAccount_AI_AU on Event (After Insert,After Update) {
if(Label.isTriggerActive.equalsIgnoreCase('true')){
    if (trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
            event_FieldUpdateOnAccount_AI_AU obj = new event_FieldUpdateOnAccount_AI_AU();
            obj.updateAccountInfo(Trigger.new);
    }
  }
}