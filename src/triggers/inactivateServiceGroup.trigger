/**************************************************************************************
Apex Trigger Name     : inactivateServiceGroup
Version             :  
Created Date        : 9th Feb 2015
Function            : This is an Apex Trigger for ensuring that if a Service Group has 
                        been set to inactive, all Account Assignments are already 
                        inactive, otherwise, the system prevents the inactivation of
                        the service group              
Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Samuel Bong                 20150209               Original Version  
*************************************************************************************/

trigger inactivateServiceGroup on Service_Group__c (after update) {
	if(Label.isTriggerActive.equalsIgnoreCase('true')){
/*
    Find all of the Service Groups that are being set to "Inactive"
    Then query for all "Inactive" Account Assignments oSearch  n each Service Group
*/
    
    // query list of Service Groups that are entering the trigger and have been set to Inactive
    List<Service_Group__c> serviceGroups = [SELECT Id, Inactive__c FROM Service_Group__c WHERE (Id = :Trigger.new AND Inactive__c = true)];
          
    // create of Service Groups to update
    List<Service_Group__c> serviceGroupsToUpdate = new List<Service_Group__c>();
    
    for (Service_Group__c inLoopSG : serviceGroups) {
      // are there any active account assignments for this Service Group?
      List<Account_Assignment__c> activeAccountAssignments = [SELECT Id, Name, Inactive__c FROM Account_Assignment__c WHERE (Inactive__c = false AND Service_Group__c = :inLoopSG.Id) ];
      // if there are no active Account Assignments
      if (activeAccountAssignments.size() == 0) {
          // add to list to update
          serviceGroupsToUpdate.add(inLoopSG);
      } else {
          // display error message
          inLoopSG.Inactive__c = false;
          Trigger.New[0].addError('Service Groups cannot be set to Inactive if there is at least one Active Account Assignment child record');
      }
    }
    
    // query list of active Service Group Members
    List<Service_Group_Member__c> activeSGMs = [SELECT Id, Name, Inactive__c FROM Service_Group_Member__c WHERE (Inactive__c = false AND Service_Group__c = :serviceGroupsToUpdate)];    
    
    // create list of Service Group Members to update
    List<Service_Group_Member__c> membersToUpdate = new List<Service_Group_Member__c>();
    
    // update Service Group Members
    for (Service_Group_Member__c SGM : activeSGMs) {
        SGM.Inactive__c = true;
        membersToUpdate.add(SGM);
    }
    if(!membersToUpdate.isEmpty()) {
        update membersToUpdate;
    }     
  }
}