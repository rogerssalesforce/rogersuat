trigger CampaignMember_AssigntoFieldUpdate_BI_BU on CampaignMember (Before Insert,Before Update) {
if(Label.isTriggerActive.equalsIgnoreCase('true')){   
   if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate )){
        string triggerType;
        if(Trigger.isInsert){
            triggerType='BI';
        }else{
            triggerType='BU';
        }
        CampaignMember_AssigntoFieldUpdate_BI_BU obj = new CampaignMember_AssigntoFieldUpdate_BI_BU();
        obj.updateAssignTo(Trigger.new,triggerType);
    }
 }	
}