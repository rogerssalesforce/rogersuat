/* Change Log:
    Jan/27/2015 Paul Saini    Edit trigger
    Function: Update trigegr such that it should only fire if the Opportunity.Business_Unit__c = 'Data Centre'
    Check for condition if  Opportunity.Business_Unit__c is not populated.
    Put a if block around processing and only process records when Opportunity.Business_Unit__c='Data Centre'.
*/
trigger shift_update_owner_user on Opportunity (before insert, before update) {
	if(Label.isTriggerActive.equalsIgnoreCase('true')){
    System.Debug('TPS:EH shift_update_owner_user starts');
    // update Owner_User__c with the OwnerId on insert and update
    for (Opportunity o : trigger.new) {
        // skip records where business unit is not populated
        if (o.Business_Unit__c == null) continue;
        
        // only process records where business unit 'Data Centre'        
        if(o.Business_Unit__c == 'Data Centre'){
            o.Owner_User__c = o.OwnerId;
        }
    }
  }
}