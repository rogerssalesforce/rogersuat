/*
    * Trigger is used to calculate EMRR and Price for the Quote level.
    * 27/06/2012 Calculation depending by Primary was added(After Update)
    * 2014-04-09    Jocsan Diaz    2.0    Remove code to delete opportunity line items. This is not longer required as all product family records must be kept.   
    * 2014-04-30    Edward Chow    2.0.1  Added error detection to refreshDataCentre method 
    */

trigger CalculateQuote on Quote__c (before update, after update, before delete) {
private Set<String> quoteIds = new Set<String>();
if(Label.isTriggerActive.equalsIgnoreCase('true')){
    
    
    if(Trigger.isUpdate && Trigger.isBefore) { // before update
                
        for(Quote__c quote : Trigger.new) {
            if(!quote.Is_Cloned__c && !Trigger.oldMap.get(quote.Id).Is_Cloned__c) { // 2.0
                quoteIds.add(quote.Id);
                quote.Total_EMRR__c = 0;
                quote.One_Time_Fees__c = 0;
            }
        }
        
        List<Quote_Line_Item__c> quoteLineItems = new List<Quote_Line_Item__c>();
        if(!quoteIds.isEmpty()) {
            quoteLineItems = [
                    SELECT OTC_Special__c, EMRR__c, Quote__r.Id, Quote__r.Opportunity_Name__c, Data_Center__c, of_Cabinets__c, Quote__r.Syncing__c
                    FROM Quote_Line_Item__c
                    WHERE Quote__r.Id IN :quoteIds LIMIT 50000];
        }
        
        for(Quote_Line_Item__c quoteLineItem : quoteLineItems) {
            Quote__c quote = Trigger.newMap.get(quoteLineItem.Quote__r.Id);
            
            if(quote.Total_EMRR__c == 0 && quoteLineItem.EMRR__c != NULL) {
                quote.Total_EMRR__c = quoteLineItem.EMRR__c;
            } else if(quoteLineItem.EMRR__c != NULL) {
                quote.Total_EMRR__c += quoteLineItem.EMRR__c;
            }
            
            if(quote.One_Time_Fees__c == 0 && quoteLineItem.OTC_Special__c != NULL) {
                quote.One_Time_Fees__c = quoteLineItem.OTC_Special__c;
            } else if(quoteLineItem.OTC_Special__c != NULL) {
                quote.One_Time_Fees__c += quoteLineItem.OTC_Special__c;
            }
        }
    } else if(Trigger.isUpdate && Trigger.isAfter) { // after update
        Set<String> primaryQuoteIds = new Set<String>();
        Set<String> notPrimaryQuoteIDs = new Set<String>();
        
        for(Quote__c quote : Trigger.new) {
            if(!quote.Is_Cloned__c) {
                if(quote.Syncing__c && (Trigger.newMap.get(quote.Id).Syncing__c != Trigger.oldMap.get(quote.Id).Syncing__c)) {
                    primaryQuoteIds.add(quote.Id);
                } else if(!quote.Syncing__c && (Trigger.newMap.get(quote.Id).Syncing__c != Trigger.oldMap.get(quote.Id).Syncing__c)) {
                    notPrimaryQuoteIDs.add(quote.id);
                }
                
                if(quote.Syncing__c && Trigger.oldMap.get(quote.Id).Is_Cloned__c) {
                    primaryQuoteIds.add(quote.Id);
                }
            }
        }
        
        List<Quote_Line_Item__c> primaryQLIs = new List<Quote_Line_Item__c>();
        if(!primaryQuoteIds.isEmpty()) {
            primaryQLIs = [
                    SELECT Quote__r.Opportunity_Name__c, EMRR__c, OTC_Special__c, Product__r.Family, Data_Center__c, of_Cabinets__c
                    FROM Quote_Line_Item__c
                    WHERE Quote__r.Id IN :primaryQuoteIds LIMIT 15000];
        }
        
        List<Quote_Line_Item__c> notPrimaryQLIs = new List<Quote_Line_Item__c>();
        if(!notPrimaryQuoteIDs.isEmpty()) {
            notPrimaryQLIs = [
                    SELECT Quote__r.Opportunity_Name__c, EMRR__c, OTC_Special__c, Product__r.Family, Data_Center__c, of_Cabinets__c
                    FROM Quote_Line_Item__c
                    WHERE Quote__r.Id IN :notPrimaryQuoteIDs LIMIT 15000];
        }
        
        if(!primaryQLIs.isEmpty()) {
            increaseOLIsPrice(primaryQLIs);
        }
        
        if(!notPrimaryQLIs.isEmpty()) {
            reduceOLIsPrice(notPrimaryQLIs);
        }
         if(Recursion_blocker.dataCenterUpdate){   
            Recursion_blocker.dataCenterUpdate = false; 
            //refresh data centre information at the opportunity
            refreshDataCentre(trigger.newMap);
         }
                
    } else if(Trigger.isDelete) { // is delete
        quoteIds = new Set<String>();
        for(Quote__c quote : Trigger.old) {
            quoteIds.add(quote.Id);
        }
        
        List<Quote_Line_Item__c> lineItemToDelete = new List<Quote_Line_Item__c>();
        if(!quoteIds.isEmpty()) {
            lineItemToDelete = [
                SELECT Id
                FROM Quote_Line_Item__c
                WHERE Quote__r.Id IN :quoteIds];
        }
        
        if(!lineItemToDelete.isEmpty()) {
            Database.delete(lineItemToDelete);
        }
    }
}
    
    
    // Increase sales price for the OLIs when quotes became Primary
    private void increaseOLIsPrice(List<Quote_Line_Item__c> primaryQLIs) {
        //refreshProductPrice('addition', primaryQLIs);
        
        // get id from qli
        set<String> quoteLineItemIds = new set<String>(); // 2.0
        for (Quote_Line_Item__c qli : primaryQLIs) {
          quoteLineItemIds.add(qli.Id);
        }
        
        // refresh oli records
        refreshOpportunityProductsPrice(quoteLineItemIds);
    }
    
    // Reduce sales price for the OLIs when quotes became not Primary
    private void reduceOLIsPrice(List<Quote_Line_Item__c> notPrimaryQLIs) {
        //refreshProductPrice('subtraction', notPrimaryQLIs); // 2.0 leave the old values there if quote flagged as non-primary
    }

  // refresh oli records from quote. // 2.0
  private void refreshOpportunityProductsPrice(set<String> quoteLineItemIds) {
    // get quote line items
        List<Quote_Line_Item__c> quoteLineItems = [
                SELECT Quote__r.Opportunity_Name__c, EMRR__c, OTC_Special__c, Product__r.Family, Family__c
                    FROM Quote_Line_Item__c
                        WHERE Id IN :quoteLineItemIds AND Quote__r.Syncing__c = TRUE LIMIT 10000];
        
        // get opportunity and quote ids    
        Set<Id> oppIds = new Set<Id>();  
        Set<Id> qIds = new Set<Id>();                  
        for(Quote_Line_Item__c quoteLineItem : quoteLineItems) {
            oppIds.add(quoteLineItem.Quote__r.Opportunity_Name__c);
            qIds.add(quoteLineItem.Quote__c);
        } 
        
        // get opportunity and oli records
        map<Id, Opportunity> opportunities = new map<Id, Opportunity>([
                SELECT Name,
                    (SELECT UnitPrice, Line_Item_Product_Family__c FROM OpportunityLineItems)
                    FROM Opportunity
                      WHERE Id IN :oppIds LIMIT 10000]);
        system.debug('opportunities map = '+ opportunities); 
        
        // aggregate qli data
    AggregateResult[] groupedResults = [
      select Quote__r.Opportunity_Name__c theOpportunityId, Family__c theFamily, SUM(EMRR__c) theSUM 
        from Quote_Line_Item__c 
          where Quote__c =: qIds 
            group by Quote__r.Opportunity_Name__c, Family__c];
    // populate EMRR map        
    map<Id, map<string, decimal>> theEMRRMap = new map<Id, map<string, decimal>>();
    for (AggregateResult ar : groupedResults) {
      system.debug('theOpportunityId' + ar.get('theOpportunityId'));
      system.debug('theFamily' + ar.get('theFamily'));
      system.debug('theSUM' + ar.get('theSUM'));
      
      Id theOpportunityId = (Id)ar.get('theOpportunityId');
      string theFamily = (string)ar.get('theFamily');
      decimal theSUM = (decimal)ar.get('theSUM'); 
      
      if (!theEMRRMap.containsKey(theOpportunityId)) {
        map<string, decimal> t = new map<string, decimal>();
        t.put(theFamily, theSUM);
        theEMRRMap.put(theOpportunityId, t);
      } else {
        theEMRRMap.get(theOpportunityId).put(theFamily, theSUM);
      }
    } 
    system.debug(theEMRRMap); 
    
    // update oli records
    list<OpportunityLineItem> theOLI2Upsert = new list<OpportunityLineItem>();
        for (Opportunity opp : opportunities.values()) {           
            if(opp.OpportunityLineItems != NULL && (!opp.OpportunityLineItems.isEmpty())) {
                for(OpportunityLineItem oli : opp.OpportunityLineItems) {
                  oli.UnitPrice = 0; // 2.0 reset price to 0 for all lines
                    if (theEMRRMap.containsKey(opp.Id)) {
                      if (theEMRRMap.get(opp.Id).containsKey(oli.Line_Item_Product_Family__c)) oli.UnitPrice = theEMRRMap.get(opp.Id).get(oli.Line_Item_Product_Family__c);
                    }
                    theOLI2Upsert.add(oli);
                }
            }
        }
        
        // upsert oli records
        if (!theOLI2Upsert.isEmpty()) Database.upsert(theOLI2Upsert);
  }  
    
    /*
    // Refresh product price depending by math operation
    private void refreshProductPrice(String mathOperation, List<Quote_Line_Item__c> quoteLineItems) {
        Set<String> revenueTypes = getRevenueTypes(quoteLineItems);
        Map<Id, Opportunity> opportunitiesToRefresh = getOpportunities(quoteLineItems, revenueTypes);
        
        Map<String, PricebookEntry> typeToPriceBook = initDefaultProduct();
       
        List<OpportunityLineItem> lineItemsToUpsert = new List<OpportunityLineItem>();
        // List<OpportunityLineItem> lineItemsToDelete = new List<OpportunityLineItem>(); 2.0
        
        for(Opportunity opp : opportunitiesToRefresh.values()) {
            Map<String, OpportunityLineItem> typeToOppProducts = new Map<String, OpportunityLineItem>();
            
            if(opp.OpportunityLineItems != NULL && (!opp.OpportunityLineItems.isEmpty())) {
                for(OpportunityLineItem oppLineItem : opp.OpportunityLineItems) {
                    oppLineItem.UnitPrice = 0; // 2.0 reset to 0 for each line calculation
                    typeToOppProducts.put(oppLineItem.Line_Item_Product_Family__c, oppLineItem);
                }
            }
            
            if(mathOperation == 'addition') {
                system.debug(revenueTypes);
                for(String revenueType : revenueTypes) {
                    if(!typeToOppProducts.containsKey(revenueType)) {
                        system.debug(typeToPriceBook);
                        system.debug(revenueType);
                        typeToOppProducts.put(revenueType, new OpportunityLineItem(
                                OpportunityId = opp.Id,
                                UnitPrice = 0,
                                Quantity = 1,
                                PricebookEntryId = typeToPriceBook.get(revenueType).Id,
                                Line_Item_Product_Family__c = revenueType));
                    }
                }
            }
        
            for(Quote_Line_Item__c quoteLineItem : quoteLineItems) {
                
                String revenueType = '';
                              
                system.debug(Config__c.getInstance('Config').Default_Product_Family__c);
                if (quoteLineItem.Product__r.Family == NULL) {
                    revenueType = Config__c.getInstance('Config').Default_Product_Family__c; // 2.0 default to a valid value if family is null. don't care the order.
                } else {
                    revenueType = quoteLineItem.Product__r.Family;
                }                  
                
                if(typeToOppProducts.containsKey(revenueType)) {
                    OpportunityLineItem oppLineItem = typeToOppProducts.get(revenueType);
                    
                    if(mathOperation == 'addition') {
                        oppLineItem.UnitPrice += (quoteLineItem.EMRR__c != NULL ? quoteLineItem.EMRR__c : 0);
                    } else if(mathOperation == 'subtraction') {
                        oppLineItem.UnitPrice -= (quoteLineItem.EMRR__c != NULL ? quoteLineItem.EMRR__c : 0);
                    }
                    
                    typeToOppProducts.put(revenueType, oppLineItem);
                }
            }
            
            for(OpportunityLineItem oppLineItem : typeToOppProducts.values()) {
                //if(oppLineItem.UnitPrice >= 0) {
                //    lineItemsToUpsert.add(oppLineItem);
                //} else if((oppLineItem.UnitPrice < 0) && oppLineItem.ID != NULL) {
                //    lineItemsToDelete.add(oppLineItem);
                //} // 2.0
                
                // we still want to upsert existing records. 2.0
                if (oppLineItem.UnitPrice >= 0) {
                    lineItemsToUpsert.add(oppLineItem);
                }    
            }
        }
        
        system.debug(lineItemsToUpsert);
        if(!lineItemsToUpsert.isEmpty()) {
            Database.upsert(lineItemsToUpsert);
        }
        
        //if(!lineItemsToDelete.isEmpty()) {
        //    Database.delete(lineItemsToDelete);
        //} // 2.0
    }*/
    
   // Get revenue types
    private Set<String> getRevenueTypes(List<Quote_Line_Item__c> quoteLineItems) {
        Set<String> revenueTypes = new Set<String>();
        
        for(Quote_Line_Item__c qliEntry : quoteLineItems) {
            
            String revenueType = '';
                       
            system.debug(Config__c.getInstance('Config').Default_Product_Family__c);
            if (qliEntry.Product__r.Family == NULL) {
                revenueType = Config__c.getInstance('Config').Default_Product_Family__c; // 2.0 default to a valid value if family is null. don't care the order.
            } else {
                revenueType = qliEntry.Product__r.Family;
            }            
            
            revenueTypes.add(revenueType);
        }
        
        return revenueTypes;
    }
    
    // Get opportunities to update
    private Map<Id, Opportunity> getOpportunities(List<Quote_Line_Item__c> quoteLineItems, Set<String> revenueTypes) {
        Set<String> oppIds = new Set<String>();
        
        for(Quote_Line_Item__c qliEntry : quoteLineItems) {
            oppIds.add(qliEntry.Quote__r.Opportunity_Name__c);
        }
        
        return new Map<Id, Opportunity>([
                SELECT Name,
                    (SELECT UnitPrice, Line_Item_Product_Family__c
                    FROM OpportunityLineItems 
                    //WHERE Line_Item_Product_Family__c IN :revenueTypes LIMIT 4 // 2.0
                    )
                FROM Opportunity
                WHERE Id IN :oppIds LIMIT 9996]);
    }

    // Init Default Product Map. Defined on the fly. // 2.0
    private Map<String, PricebookEntry> initDefaultProduct() {
        // define map to store the pricebook mapped to family
        Map<String, PricebookEntry> typeToPriceBook = new Map<String, PricebookEntry>();
        
        // get all aggregated products
        list<PricebookEntry> thePBEList = new list<PricebookEntry>([
            select Id, Product2.Family from PricebookEntry 
                where Product2.Aggregated_Product__c = true and Product2.Family != null and Product2.IsActive = true]);
        
        // add PBEs to map
        for (PricebookEntry pbe : thePBEList) {
            typeToPriceBook.put(pbe.Product2.Family, pbe);
        }
               
        return typeToPriceBook;
    }
    
    // Refresh Data Centre information on the opportunity. // 2.0
    private void refreshDataCentre(map<Id, Quote__c> theQuoteMap) {
        
        list<Opportunity_Data_Centre__c> theODCList2Insert = new list<Opportunity_Data_Centre__c>();
        
        // get existing Opportunity Ids
        map<Id, Id> theQuoteOpportunityIdMap = new map<Id, Id>();
        set<Id> theOpportunityIdSet = new set<Id>();
        for (Quote__c q : theQuoteMap.values()) {
            // get opportunity ids
            theQuoteOpportunityIdMap.put(q.Id, q.Opportunity_Name__c);
            
            // remove from Opportunity
            theOpportunityIdSet.add(q.Opportunity_Name__c); 
        }
        system.debug(theQuoteOpportunityIdMap);
        
        AggregateResult[] groupedResults = [
            select Quote__c theQuoteId, Data_Center__c theDataCentre, sum(of_cabinets__c) theTotalCabinets 
                from Quote_Line_Item__c 
                    where Quote__r.Syncing__c = true and of_cabinets__c > 0 and Quote__c in: theQuoteOpportunityIdMap.keySet()
                        group by Quote__c, Data_Center__c];
        system.debug(groupedResults);
        
        for (AggregateResult ar : groupedResults) {
            system.debug('theQuoteId ' + ar.get('theQuoteId'));
            system.debug('theDataCentre ' + ar.get('theDataCentre'));
            system.debug('theTotalCabinets ' + ar.get('theTotalCabinets'));
            
            // create Opportunity Data Centre records
            Opportunity_Data_Centre__c odc = new Opportunity_Data_Centre__c(Opportunity__c = theQuoteOpportunityIdMap.get((Id)ar.get('theQuoteId')));
            string theDataCenterName;
            theDataCenterName = (string)ar.get('theDataCentre');
            odc.Name = (theDataCenterName == null) ? 'Missing Data Centre' : theDataCenterName;
            odc.of_Cabinets__c = (decimal)ar.get('theTotalCabinets');
            theODCList2Insert.add(odc); 
        }
                
        // get existing Opportunity Data Centre records
        theOpportunityIdSet.retainAll(theQuoteOpportunityIdMap.values());
        list<Opportunity_Data_Centre__c> theODCList = new list<Opportunity_Data_Centre__c>([
            select Id, Name, of_Cabinets__c 
                from Opportunity_Data_Centre__c 
                    where Opportunity__c in: theOpportunityIdSet]);
        system.debug(theODCList);
        
        try {
            // delete existing lines
            delete theODCList;
            
            // recreate Opportunity Data Centre records from Quote Line Items
            insert theODCList2Insert;
        
        } catch (DMLException ex) {
            for (Quote__c q : theQuoteMap.values()) q.addError(System.Label.Quote_Save_Error + '. Error: ' + ex.getMessage());
        }

    }
    
}