trigger Quote_Request_Trigger_AU_AI on Quote_Request__c (after insert, after update) {
	if(Label.isTriggerActive.equalsIgnoreCase('true')){
	List<Id> quoteRequestids = new List<Id>();
    
    if (Trigger.isInsert){
        List<Id> originalQuoteRequestIds = new List<Id>();
        List<Id> parentQuoteRequestIds = new List<Id>();
        for (Quote_Request__c q : Trigger.New){
            quoteRequestids.add(q.id);
            if (q.Original_Quote_Request__c!=null)
                originalQuoteRequestIds.add(q.Original_Quote_Request__c);
            if (q.parentQuoteRequest__c!=null)
                parentQuoteRequestIds.add(q.parentQuoteRequest__c);
        }
        Map<Id, Quote_Request__c> originalQuoteRequestMap;
        Map<Id, Quote_Request__c> parentQuoteRequestMap;
        
        Set<Quote_Request__c> originalQuoteRequests = new Set<Quote_Request__c>();
        if (originalQuoteRequestIds.size()!=null){
            originalQuoteRequestMap = new Map<Id, Quote_Request__c>([SELECT id, Number_Of_Derived_Quote_Requests__c FROM Quote_Request__c WHERE Id IN :originalQuoteRequestIds]);
            for (Quote_Request__c q : Trigger.New){
                Quote_Request__c originalQuoteRequest = originalQuoteRequestMap.get(q.Original_Quote_Request__c);
                if (originalQuoteRequest!=null){
                    if (originalQuoteRequest.Number_Of_Derived_Quote_Requests__c!=null)
                        originalQuoteRequest.Number_Of_Derived_Quote_Requests__c++;
                    else
                        originalQuoteRequest.Number_Of_Derived_Quote_Requests__c = 1;
                    originalQuoteRequest.Version__c = 1;
                    originalQuoteRequests.add(originalQuoteRequest);
                }
            }
        }
        
        Set<Quote_Request__c> parentQuoteRequests = new Set<Quote_Request__c>();
        if (parentQuoteRequestIds.size()!=null){
            parentQuoteRequestMap = new Map<Id, Quote_Request__c>([SELECT id, Number_Of_Derived_Quote_Requests__c FROM Quote_Request__c WHERE Id IN :parentQuoteRequestIds]);
            for (Quote_Request__c q : Trigger.New){
                Quote_Request__c parentQuoteRequest = parentQuoteRequestMap.get(q.parentQuoteRequest__c);
                Quote_Request__c originalQuoteRequest = originalQuoteRequestMap.get(q.Original_Quote_Request__c);
                
                if (parentQuoteRequest!=null && originalQuoteRequest != null && (parentQuoteRequest.id != originalQuoteRequest.id)){
                    if (parentQuoteRequest.Number_Of_Derived_Quote_Requests__c!=null)
                        parentQuoteRequest.Number_Of_Derived_Quote_Requests__c++;
                    else
                        parentQuoteRequest.Number_Of_Derived_Quote_Requests__c = 1;
                    parentQuoteRequests.add(parentQuoteRequest);
                }
            }
        }
        if (originalQuoteRequests.size()>0)
            UPDATE new List<Quote_Request__c>(originalQuoteRequests);
            
        if (parentQuoteRequests.size()>0)
            UPDATE new List<Quote_Request__c>(parentQuoteRequests);
	}
  }
}