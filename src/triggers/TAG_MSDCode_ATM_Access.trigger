/*Trigger Name  : TAG_MSDCode_ATM_Access
*Description : This Trigger will support the logic for the 
            existing Account Team Members need to be granted access to this new record
*Created By  : Jayavardhan.
*Created Date :02/03/2015.
* Modification Log :
--------------------------------------------------------------------------------------------------------------------------------------------------
                Developer         Version       Date        Description
                Jayavardhan         1.0       02/03/2015    Get list of all Account team members for an account of this new  MSD_Code__c
                                                            record and update Access Level based on the Custom setting value Read/Edit
--------------------------------------------------------------------------------------------------------------------------------------------------
*
*/  
Trigger TAG_MSDCode_ATM_Access on MSD_Code__c (after insert, before insert) {
  if(Label.isTriggerActive.equalsIgnoreCase('true')){
    Set<Id> setAllUsers = new Set<Id>();
    Map<Id, Set<Id>> mapAccUserIdsToAdd = new Map<Id, Set<Id>>();
    Map<Id, Set<Id>> mapMSDCodeUserIdsToAdd = new Map<Id, Set<Id>>();
    Map<Id, Id> mapAccContId = new Map<Id,Id>();
    
    
    
    // We only execute the trigger after a Job record has been inserted 
    // because we need the Id of the Job record to already exist.
    if(Trigger.isAfter &&  Trigger.isInsert){
        
         Set<Id> setAllAccounts = new Set<Id>();
        for(integer i=0; Trigger.new.size()>i; i++){            
            //if(Trigger.new[i].Account__c!=null)
            setAllAccounts.add(Trigger.new[i].Account__c);
            mapAccContId.put(Trigger.new[i].Account__c,Trigger.new[i].Id);
        //    setAllUsers.add(Trigger.new[i].Account__r.OwnerId);
            system.debug('*****acc ownerids' + setAllUsers);           
        }  
       
      
        
        List<AccountTeamMember> parentAccMSDMembers = [Select Id, UserId,TeamMemberRole,Account.Id from AccountTeamMember where Account.Id in:setAllAccounts];      
        for(AccountTeamMember teamMem : parentAccMSDMembers){            
            Set<Id> userIds = mapAccUserIdsToAdd.get(teamMem.Account.Id);
            If(userIds==null)
            userIds = new Set<Id>();
            userIds.add(teamMem.UserId);
            mapAccUserIdsToAdd.put(teamMem.Account.Id,userIds);
            if(mapAccContId.containskey(teamMem.Account.Id))
            mapMSDCodeUserIdsToAdd.put(mapAccContId.get(teamMem.Account.Id),userIds);
        }       
        
        //Call TAG_updateAccountTeamAccessHelper class to Give access from Users after updating AccountTeam
        TAG_updateAccountTeamAccessHelper accessCls = new TAG_updateAccountTeamAccessHelper();                
        accessCls.giveAccesstoMSDCodeTeam(mapMSDCodeUserIdsToAdd);
    }
     if(Trigger.isBefore &&  Trigger.isInsert)
     {
          Set<Id> setAllAccounts = new Set<Id>();
         for(integer i=0; Trigger.new.size()>i; i++)
         {            
            //if(Trigger.new[i].Account__c!=null)
            setAllAccounts.add(Trigger.new[i].Account__c);
         }
         Map<id,Account> mapAccwidParent = new Map<id,Account>([Select id, OwnerId, ParentId,District__c, Parent.OwnerId,Parent.District__c, Account_Status__c from Account where id in: setAllAccounts]);
        
         for( integer i=0; i<Trigger.new.size();i++)
         {
                if(Trigger.new[i].Account__c!=null && mapAccwidParent.get(Trigger.new[i].Account__c).ParentId== null)
                {
                    if(mapAccwidParent.get(Trigger.new[i].Account__c)!=null)
                    {
                            Trigger.new[i].OwnerId = mapAccwidParent.get(Trigger.new[i].Account__c).OwnerId;
                            if(mapAccwidParent.get(Trigger.new[i].Account__c).District__c !=null)
                                Trigger.new[i].District__c = mapAccwidParent.get(Trigger.new[i].Account__c).District__c;
                    }
                }
                else if(Trigger.new[i].Account__c!=null && mapAccwidParent.get(Trigger.new[i].Account__c).ParentId!= null)
                {
                    Trigger.new[i].OwnerId =  mapAccwidParent.get(Trigger.new[i].Account__c).Parent.OwnerId;
                    if(mapAccwidParent.get(Trigger.new[i].Account__c).Parent.District__c !=null)
                        Trigger.new[i].District__c = mapAccwidParent.get(Trigger.new[i].Account__c).Parent.District__c;
                    
                }
         } 
    
    
    }
  }
}