/**************************************************************************************************************************
Class Name      : CPQ_AgreementTrigger
Description     : This Trigger calls the helper class 'CPQ_AgreementTriggerHelper' 
Created By      : Deepika Rawat
Created Date    : 26-Aug-15
Modification Log:
--------------------------------------------------------------------------------------------------------------------------
Developer                   Date                     Description
------------------------------------------------------------------------------------------------------------------------- 
Aakanksha Patel           26-Aug-2015         US-0051. Updating the 'Has MECA' field on the Account
                          09-Oct-2015         US-0295. Method for Case Creation after approval of MECA Amendment Agreement 
Deepika Rawat             31-Aug-2015         US-0050. Copy primary quote document from
                                              related Quote to Agreement.
Akshay Ahluwalia          24-Sept-2015        US-0051 Updating Agreement Record type on Insert
Alina Balan               17-December-2015    US-0436, Updating Agreement status when a doc is sent to eSignature
Alina Balan               08-January-2016     US-0507, Added "setSharingsRulesBasedOnOpportunityTeam"
**************************************************************************************************************************/
trigger CPQ_AgreementTrigger on Apttus__APTS_Agreement__c (after update, after insert, before insert, before update) {
    CPQ_Settings__c cpqSetting = CPQ_Settings__c.getOrgDefaults();
    Boolean disableTrigger = Boolean.valueOf(cpqSetting.Disable_Agreement_Trigger__c);
    //check the setting value
    if (!disableTrigger) {
        if(trigger.isAfter && trigger.isUpdate){
            CPQ_AgreementTriggerHelper.updateAccountOnAgreementUpdate(trigger.new, trigger.oldMap);
            CPQ_AgreementTriggerHelper.closeOpportunities(trigger.new, trigger.oldMap);
            CPQ_AgreementTriggerHelper.createCaseRecAgreement(trigger.new); 
        }
    
        if (trigger.isBefore && trigger.isUpdate){
            CPQ_AgreementTriggerHelper.setESignatureAgreementStatuses(trigger.new, trigger.oldMap);
        }
    
        if(trigger.isAfter && trigger.isInsert){
            CPQ_AgreementTriggerHelper.setSharingsRules(trigger.new);
            CPQ_AgreementTriggerHelper.createDocumentOnAgreement(Trigger.New);
            
        }
    
        If(trigger.isBefore && Trigger.isInsert){
        CPQ_AgreementTriggerHelper.updateRecordTypeOnInsert(trigger.new);
        }
    }
}