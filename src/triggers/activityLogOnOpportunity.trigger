trigger activityLogOnOpportunity on Opportunity (after insert, after update, before delete) {
 if(Label.isTriggerActive.equalsIgnoreCase('true')){
  //  List<Activity_Log__c> lstActlogFinal = new List<Activity_Log__c>();
    Set<Activity_Log__c> setActlogFinal = new Set<Activity_Log__c>();
    List<BIS_Activity_Tracking__c> lstBISupdate= new List<BIS_Activity_Tracking__c>();
    List<Activity_Log__c> lstActlogDelete = new List<Activity_Log__c>();
    Map<String,Activity_Log__c> mapActivityLog = new Map<String,Activity_Log__c>();
    set<String> newLstBU= new set<String>();
   
    //get the list of channels coming under BIS users
    for(BIS_Users__c bU:BIS_Users__c.getAll().values()){
        newLstBU.add(bU.Name);  
    }
    
    //map the user with there corresponding channel
    Map<id,String> mapUserChannel= new Map<id,String>();
    Map<id,String> mapUserName= new Map<id,String>();
    
    //EBU Consolidation deployment error fix JK
    List<User> users = new List<User>();
    if(!Test.isRunningTest()){
        users = [select id,Name,Channel__c from User];
    } else {
        users = [select id,Name,Channel__c from User limit 1];
     }
    
    for(User u:users){
        mapUserChannel.put(u.id,u.channel__c);
        mapUserName.put(u.id,u.Name);
    }
    if(trigger.isUpdate || trigger.isInsert){
        List<String> oppIdsNIS = new List<String>();
        List<String> oppIdsBIS = new List<String>();
        for(Opportunity opp: Trigger.new){
        // Check weather the channel of the current user is from BIS or NIS.
            if( newLstBU.Contains(mapUserChannel.get(opp.OwnerId))){
                oppIdsBIS.add(opp.id);
            }else{
                oppIdsNIS.add(opp.id);
            }
        }
        // If NIS activity then follow the Below Process
        if(oppIdsNIS.size() !=0){
            List<Activity_Log__c> lstActLog = new List<Activity_Log__c>([select Activity_Date__c, Activity_Type__c, Related_Record__c, RPC_Call__c,Sales_Rep__c, Subject__c from Activity_Log__c where isdeleted=false AND Related_Record__c in: oppIdsNIS]);
           
            //Start: create a map of all the BIS records related to non BIS users, create a map for the same that will be user fot finding records related to not BIS Opps.
            Map<id,List<BIS_Activity_Tracking__c>> mapIdAT= new Map<id,list<BIS_Activity_Tracking__c>>();
            
            List<BIS_Activity_Tracking__c> ListBIS = new List<BIS_Activity_Tracking__c>([select id,Name,Related_Record__c,Note__c,Sales_Rep_ID_Activity_ID__c from BIS_Activity_Tracking__c where isdeleted=false AND Deleted__c=false AND Activity_Type__c=:'Closed Opp' AND Related_Record__c in: oppIdsNIS]);
            for(BIS_Activity_Tracking__c aT:ListBIS){
                if(mapIdAT.get(aT.Related_Record__c)== null){
                    List<BIS_Activity_Tracking__c> atLst= new List<BIS_Activity_Tracking__c>();
                    atLst.add(aT);
                    mapIdAT.put(aT.Related_Record__c,atLst);
                }else{
                    List<BIS_Activity_Tracking__c> atLst= mapIdAT.get(aT.Related_Record__c);
                    atLst.add(aT);
                    mapIdAT.put(aT.Related_Record__c,atLst);
                }
               
            }
            //*****************************End:
            
            for(Activity_Log__c act:lstActLog){
                mapActivityLog.put(act.Related_Record__c,act);
            }
            for(integer i=0; i<trigger.new.size();i++){
                //Start BIS code: Check the related BIS records and there status 
                if(trigger.new[i].CampaignId==trigger.old[i].CampaignId && trigger.old !=null && mapIdAT.get(trigger.new[i].Id)!=null && (trigger.old[i].StageName==label.Closed_Lost || trigger.old[i].StageName==label.Closed_Won)){
                    for(BIS_Activity_Tracking__c aT:mapIdAT.get(trigger.new[i].Id)){
                        aT.Deleted__c=true;
                        aT.Sales_Rep_ID_Activity_ID__c=aT.Sales_Rep_ID_Activity_ID__c+'Deleted';
                        string s;
                        if(aT.Note__c!='null' && aT.Note__c!=null){
                            s=aT.Note__c;
                        }
                        s = s+'; ' + label.Opportunity_Owner_is_changed_to +' '+ mapUserName.get(trigger.new[i].OwnerId)+ ' on ' + + system.Now().format('MM/dd/yyyy HH:mm:ss','America/New_York') + ' by ' +UserInfo.GetFirstName()+' '+UserInfo.GetLastName();
                        if(s.length() >200){
                        aT.Note__c = s.SubString(s.length()-200,s.length());
                        }else{
                        aT.Note__c = s;
                        }  
                        lstBISupdate.add(aT);
                    }
                }
                //******************* End:
                
                Activity_Log__c act = mapActivityLog.get(trigger.new[i].id);
                if(act!=null){
             /*      System.debug('*****Trigger.new***'+trigger.new[i]);
                   System.debug('*****Trigger.old***'+trigger.old[i]);
                   System.debug('*****Trigger.new NAME***'+trigger.new[i].Name);
                   System.debug('*****Trigger.old NAME***'+trigger.old[i].Name);
              */     
                   
                    if(Trigger.isUpdate && trigger.new[i]!=null && trigger.new[i].Name!=null && trigger.old[i]!=null && trigger.old[i].Name!=null&& trigger.new[i].Name != trigger.old[i].Name){
                        act.Subject__c = label.New_Opportunity_1+' '+ trigger.new[i].Name; 
                        setActlogFinal.add(act);
                    }
                }
                else{
                    act = new Activity_Log__c();
                    act.Sales_Rep__c = trigger.new[i].OwnerId; 
                    act.Subject__c = label.New_Opportunity_1+' ' + trigger.new[i].Name;
                    act.Activity_Date__c  =Date.valueof(trigger.new[i].CreatedDate);
                    act.Activity_Type__c  = label.New_Opportunity;
                    act.RPC_Call__c = false;
                    act.Related_Record__c =trigger.new[i].id;
                    setActlogFinal.add(act);
                }
            }
        }

        //If BIS activity Follow the Given Process
        
        map<String,List<BIS_Activity_Tracking__c>> mapoppBIS= new map<String,List<BIS_Activity_Tracking__c>>();
        if(oppIdsBIS.size() !=0){
            List<BIS_Activity_Tracking__c> lstBISActTrak = new List<BIS_Activity_Tracking__c>([select Activity_Date__c, Activity_Type__c,OwnerId, RPC_Call__c,Subject__c,Related_Record__c,Deleted__c,Note__c from BIS_Activity_Tracking__c where isdeleted=false AND Deleted__c=false AND Related_Record__c in: oppIdsBIS]);
            // in Case current Opportunity is changed from NIS to BIS then search for all Activity Logs related and add to delete list
            lstActlogDelete.addAll([select id,Name from Activity_log__c where isdeleted=false AND Related_Record__c in: oppIdsBIS]);
            for(BIS_Activity_Tracking__c bisAct:lstBISActTrak){
                if(mapoppBIS.get(bisAct.Related_Record__c) !=null){
                    list<BIS_Activity_Tracking__c> bisLst= mapoppBIS.get(bisAct.Related_Record__c);
                    bisLst.add(bisAct);
                    mapoppBIS.put(bisAct.Related_Record__c,bisLst);
                }else{
                    list<BIS_Activity_Tracking__c> bisLst= new list<BIS_Activity_Tracking__c>();
                    bisLst.add(bisAct);
                    mapoppBIS.put(bisAct.Related_Record__c,bisLst);
                }
                
            }
            for(integer i=0; i<trigger.new.size();i++){
                List<BIS_Activity_Tracking__c> bisActLst = mapoppBIS.get(trigger.new[i].id);
                
                 
                //  IF opportunity is changed to Closed Lost or Closed Won AND Opp. Owner is BIS rep, then create a record in BIS Activity Tracking
                //  IF opportunity is already Closed Lost or Closed Won AND Opportunity Owner is Change from a non-BIS user to a BIS user, then then create a new BIS Activity Tracking record for the new opportunity owner
               if(bisActLst!=null){
                    boolean checkClosedPresent=false;
                    for(BIS_Activity_Tracking__c bis:bisActLst){
                        if(bis.Activity_Type__c == label.Closed_Opp){
                           checkClosedPresent=true; 
                        }
                    }
                    if(trigger.new[i].CampaignId==trigger.old[i].CampaignId &&(((bisActLst[0].OwnerID != trigger.new[i].OwnerID) && trigger.old[i].StageName == label.Closed_Won)|| (bisActLst[0].OwnerID != trigger.new[i].OwnerID) && trigger.old[i].StageName == label.Closed_Lost)){
                            for(BIS_Activity_Tracking__c bis:bisActLst){
                                bis.OwnerID = trigger.new[i].OwnerID;
                                string s;
                                if(bis.Note__c !=null){
                                    s=bis.Note__c;
                                } 
                                s = s+'; ' + label.Opportunity_Owner_is_changed_to+' ' + mapUserName.get(trigger.new[i].OwnerId) +' '+ label.on+' ' +  system.Now().format('MM/dd/yyyy HH:mm:ss','America/New_York') +  ' '+label.by  + ' '+UserInfo.GetFirstName()+' '+UserInfo.GetLastName();
                                if(s.length() >255){
                                bis.Note__c = s.SubString(s.length()-255,s.length()); 
                                }else{
                                bis.Note__c = s;
                                } 
                                lstBISupdate.add(bis); 
                            } 
                    }else if(trigger.new[i].CampaignId==trigger.old[i].CampaignId && (trigger.old !=null && !checkClosedPresent && ((trigger.new[i].StageName == label.Closed_Lost && trigger.old[i].StageName != label.Closed_Lost) 
                    || (trigger.new[i].StageName == label.Closed_Won && trigger.old[i].StageName != label.Closed_Won)|| ( trigger.new[i].OwnerID !=null && trigger.new[i].StageName == label.Closed_Lost && newLstBU.Contains(mapUserChannel.get(trigger.new[i].OwnerID))) 
                    || ( trigger.new[i].OwnerID !=null && trigger.new[i].StageName == label.Closed_Won && newLstBU.Contains(mapUserChannel.get(trigger.new[i].OwnerID))))
                    )){
                        BIS_Activity_Tracking__c bisActNew= new BIS_Activity_Tracking__c();
                        bisActNew.OwnerID = trigger.new[i].OwnerID;
                        bisActNew.Subject__c = label.Closed_Opportunity_1 + trigger.new[i].Name;
                        bisActNew.Activity_Date__c =Date.ValueOf(trigger.new[i].createddate);
                        bisActNew.Activity_Type__c = label.Closed_Opp;
                        bisActNew.RPC_Call__c = FALSE;
                        bisActNew.Related_Record__c = trigger.new[i].ID;
                        bisActNew.Note__c = label.Opportunity_Closed;
                        If(trigger.new[i].OwnerID !=null){
                            bisActNew.Sales_Rep_ID_Activity_ID__c=String.Valueof(trigger.new[i].OwnerID)+String.Valueof(trigger.new[i].Id)+label.Closed_Opp;
                        }
                        lstBISupdate.add(bisActNew); 
                    }
                }else if(trigger.new[i].CampaignId==trigger.old[i].CampaignId && ((trigger.old !=null && 
                    ((trigger.new[i].StageName == label.Closed_Lost && trigger.old[i].StageName != label.Closed_Lost) 
                    || (trigger.new[i].StageName == label.Closed_Won && trigger.old[i].StageName != label.Closed_Won)))
                    || ( trigger.new[i].OwnerID !=null && trigger.new[i].StageName == label.Closed_Lost && newLstBU.Contains(mapUserChannel.get(trigger.new[i].OwnerID))) 
                    || ( trigger.new[i].OwnerID !=null && trigger.new[i].StageName == label.Closed_Won && newLstBU.Contains(mapUserChannel.get(trigger.new[i].OwnerID))))){
                        BIS_Activity_Tracking__c bisActNew= new BIS_Activity_Tracking__c();
                        bisActNew.OwnerID = trigger.new[i].OwnerID;
                        bisActNew.Subject__c = label.Closed_Opportunity_1 + trigger.new[i].Name;
                        bisActNew.Activity_Date__c =Date.ValueOf(trigger.new[i].createddate);
                        bisActNew.Activity_Type__c = label.Closed_Opp;
                        bisActNew.RPC_Call__c = FALSE;
                        bisActNew.Related_Record__c = trigger.new[i].ID;
                        bisActNew.Note__c = label.Opportunity_Closed;
                        If(trigger.new[i].OwnerID !=null){
                            bisActNew.Sales_Rep_ID_Activity_ID__c=String.Valueof(trigger.new[i].OwnerID)+String.Valueof(trigger.new[i].Id)+label.Created_Opp;
                        }
                        lstBISupdate.add(bisActNew);
                    }else if(trigger.new[i].CampaignId==trigger.old[i].CampaignId){
                        BIS_Activity_Tracking__c bisActNew= new BIS_Activity_Tracking__c();
                        bisActNew.OwnerID = trigger.new[i].CreatedByID;
                        bisActNew.Subject__c = label.New_Opportunity_1 + trigger.new[i].Name;
                        bisActNew.Activity_Date__c =Date.ValueOf(trigger.new[i].createddate);
                        bisActNew.Activity_Type__c = label.Created_Opp;
                        bisActNew.RPC_Call__c = FALSE;
                        bisActNew.Related_Record__c = trigger.new[i].ID;
                        bisActNew.Note__c = label.Opportunity_Creator;
                        If(trigger.new[i].OwnerID !=null){
                            bisActNew.Sales_Rep_ID_Activity_ID__c=String.Valueof(trigger.new[i].OwnerID)+String.Valueof(trigger.new[i].Id)+label.Created_Opp;
                        }
                        lstBISupdate.add(bisActNew);
                    }
               }
        }
    }
    if( trigger.isdelete ){
         List<String> oppIds = new List<String>();
        for(Opportunity opp: Trigger.old){
            oppIds.add(opp.id);
        }
        // get the list of all the related Activity log and BIS Activity Tracker and add the same to delete list.
        lstActlogDelete.addAll([select Activity_Date__c, Activity_Type__c, Related_Record__c, RPC_Call__c,Sales_Rep__c, Subject__c from Activity_Log__c where isdeleted=false AND Related_Record__c in: oppIds]);
        for(BIS_Activity_Tracking__c aT:[select id,Name,Deleted__c,Note__c from BIS_Activity_Tracking__c where isdeleted=false AND Deleted__c=:false AND Related_Record__c in: oppIds]){
            aT.Deleted__c=true;
            string s;
            if(aT.Note__c!=null){
                s=aT.Note__c;
            }
            s=s+'; '+UserInfo.GetFirstName()+' '+UserInfo.GetLastName() +' '+ label.deleted_the_opportunity_on+' '+ system.Now().format('MM/dd/yyyy HH:mm:ss','America/New_York');
            if(s.length() >255){
            aT.Note__c = s.SubString(s.length()-255,s.length()); 
            }else{
            aT.Note__c = s;
            } 
            lstBISupdate.add(aT);
        }

    }
    if(setActlogFinal.size()>0)
    {
          List<Activity_Log__c> lstActlogFinal = new List<Activity_Log__c>();
          lstActlogFinal.addAll(setActlogFinal);
          set<Activity_Log__c> temp = new set<Activity_Log__c>(lstActlogFinal);

          lstActlogFinal.clear();
          lstActlogFinal.addAll(temp);
          upsert lstActlogFinal;
     
     //   lstActlogFinal.addAll(setActlogFinal);
      //  upsert lstActlogFinal;
    } 
    if(lstBISupdate.size()>0){
        upsert lstBISupdate;
    }  
    if(lstActlogDelete.size()>0){
        delete lstActlogDelete;
    }
    }
     
}