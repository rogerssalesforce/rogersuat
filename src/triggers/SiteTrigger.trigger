/**************************************************************************************************************************
Class Name      : SiteTrigger
Description     : This Trigger calls the helper class 'CPQ_SiteTriggerHelper' 
Created By      : 
Created Date    : 
Modification Log:
--------------------------------------------------------------------------------------------------------------------------
Developer                   Date                     Description
------------------------------------------------------------------------------------------------------------------------- 
Alina Balan           07-December-2015         US-0088 Added "createAccountLocations", "updateAccountLocations", "deleteAccountLocations"
**************************************************************************************************************************/

trigger SiteTrigger on Site__c (before insert, before update, before delete, after insert, after update) {
   
    if (Label.isTriggerActive.equalsIgnoreCase('true')) {
        if(trigger.isAfter) {
            if (trigger.isInsert) {
                CPQ_SiteTriggerHelper.createAccountLocations((List<Site__c>)trigger.new);
            }
            if (trigger.isUpdate) {
                CPQ_SiteTriggerHelper.updateAccountLocations((Map<Id, Site__c>)trigger.newMap);
            }
        } 
        if (trigger.isBefore && trigger.isDelete) {
            CPQ_SiteTriggerHelper.deleteAccountLocations(trigger.oldMap);
        }
    }
   
   if(trigger.isBefore && Label.isTriggerActive.equalsIgnoreCase('true') && !trigger.isDelete){     
    List <Site__c> lS = new List <Site__c>(); 
    List <Opportunity> lO = new List <Opportunity>(); 
    Set <id> sO = new Set <Id>();
    Map <id,id> mapO = new Map<id,id>();
    Map <id,id> mapA = new Map<id,id>();
    
    
    
    for(Site__c s2: trigger.new){
        mapO.put(s2.id,s2.Opportunity__c);
        sO.add(s2.Opportunity__c);
    }

    lO = [select id,AccountId from Opportunity where id in :sO];
    
    for(Opportunity O2: lO){
        mapA.put(O2.id,O2.AccountId);
    }
    
    
    if((trigger.isBefore && trigger.isInsert)){
        for(Site__c s: trigger.new){
            if (s.UniqueKey__c == null){
                s.UniqueKey__c = s.Suite_Floor__c + ' ' + s.Street_Number__c + ' ' + s.Street_Name__c + ' ' + s.Street_Type__c + ' ' + s.Street_Direction__c + ' ' + s.City__c + ' ' + s.Province_Code__c + ' ' + s.Postal_Code__c + ' ' + s.opportunity__c; 
                s.Account__c = mapA.get(mapO.get(s.id));
            }
        }
     }
    
    if (trigger.isBefore && trigger.isUpdate){
        for(Site__c s1 : trigger.new){
            s1.UniqueKey__c = s1.Suite_Floor__c + ' ' + s1.Street_Number__c + ' ' + s1.Street_Name__c + ' ' + s1.Street_Type__c + ' ' + s1.Street_Direction__c + ' ' + s1.City__c + ' ' + s1.Province_Code__c + ' ' + s1.Postal_Code__c + ' ' + s1.opportunity__c;
            s1.Account__c = mapA.get(mapO.get(s1.id));
        }
    }
    
    SiteManager.handleSiteNameUpdate(Trigger.old, Trigger.new);
   } 
}