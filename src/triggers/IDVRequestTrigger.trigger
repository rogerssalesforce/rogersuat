/*
Trigger to check only one request is with Final status
Also  share the record to related user 
*/
trigger IDVRequestTrigger on IDV_Pricing_Request__c (before insert, before update,after update) {

if(Label.isTriggerActive.equalsIgnoreCase('true')){
    for(IDV_Pricing_Request__c IDV0 : Trigger.new){
    if(IDV0.Final_IDV_Request__c==TRUE && Trigger.isBefore){
        List<id> OppIdList = new List<id>();
          for(IDV_Pricing_Request__c IDV : Trigger.new){
                OppIdList.Add(IDV.IDV_Request_Opportunity__c);
            }
            List<IDV_Pricing_Request__c> AllIDVsfromOppIdList = new List<IDV_Pricing_Request__c>([SELECT Id, Name, IDV_Request_Opportunity__c, Final_IDV_Request__c FROM IDV_Pricing_Request__c WHERE IDV_Request_Opportunity__c IN :OppIdList AND Final_IDV_Request__c=True]);

           for(IDV_Pricing_Request__c IDV2 : Trigger.new){
               for(IDV_Pricing_Request__c IDV3 : AllIDVsfromOppIdList){
               if (IDV2.IDV_Request_Opportunity__c == IDV3.IDV_Request_Opportunity__c && IDV2.id != IDV3.id){
                   IDV2.AddError('The associated opportunity already has another IDV  Request marked as the Final Request');
               }
            }
           }
           
           }         
           
                
       }
       
          if(Trigger.isUpdate && Trigger.isAfter){
          system.debug('After update trigger fired');
          IDVRequestTriggerHelper idvTriggerHelper = new IDVRequestTriggerHelper();
          Set<id>  contractIds  = new Set<id>();
          Set<id>  evpIds  = new Set<id>();
          
          for(IDV_Pricing_Request__c IDV4 : Trigger.new){
          
          // Share record to contract team 
           if(IDV4.Status__c =='With Contracts Team' && IDV4.Contracts_Prime__c !=null ){  
            system.debug('Create share record for Contract team');                 
             if(IDVRequestTriggerHelper.isStatusChanged(Trigger.oldMap.get(IDV4.Id).Status__c ,IDV4.Status__c)) {  
              system.debug('Status Changed');          
              contractIds.add(IDV4.id); 
              }                   
                                 
           }
           
           // Share record to EVP team 
            if( IDV4.Status__c =='With EVP Team' && IDV4.EVP_Prime__c!=null ){  
             system.debug('Share record to evp Team');
              if(IDVRequestTriggerHelper.isStatusChanged(Trigger.oldMap.get(IDV4.Id).Status__c ,IDV4.Status__c)) {  
              system.debug('Status Changed');                     
              evpIds.add(IDV4.id);
              }                                         
           }
           
           }
           
           
           if(contractIds.size()>0){
           idvTriggerHelper.shareRecords(contractIds,true);
           idvTriggerHelper.sendEmailToContractTeam(contractIds);
           }
           
           if(evpIds.size()>0){
           idvTriggerHelper.shareRecords(evpIds,false);
           }
           
           
          
          //Delete the shared record once IDV request completed 
            Set<id> parentIds = new Set<id>();
            Set<id> userIds = new Set<id>();
          for(IDV_Pricing_Request__c IDV5 : Trigger.new){
          
           if(IDV5.Status__c =='Completed' ){ 
           
            if(IDVRequestTriggerHelper.isStatusChanged(Trigger.oldMap.get(IDV5.Id).Status__c ,IDV5.Status__c)) {  
              system.debug('Status Changed');             
                parentIds.add(IDV5.id);
               if(IDV5.EVP_Prime__c!=null)
               userIds.add(IDV5.EVP_Prime__c);
               
              if(IDV5.Contracts_Prime__c !=null)
              userIds.add(IDV5.Contracts_Prime__c);
              
              }
           }
           
           }
           
           if(userIds.size()>0) {
           idvTriggerHelper.deleteShare(parentIds,userIds);
           }
           
         
              
   }
   
   }

}