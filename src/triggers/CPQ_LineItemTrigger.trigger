/***************************************************************************************************
Trigger Name    : CPQ_LineItemTrigger
Description     : This is a trigger on Apttus_Config2__LineItem__c 
                  page.
Created By      : Deepika Rawat 
Created Date    : 04-Nov-15  
Modification Log:
----------------------------------------------------------------------------------------------------
Developer                Date                Description
----------------------------------------------------------------------------------------------------
Deepika Rawat          04-Nov-15           Created. Added logic to restrict user from deleting line
                                           items based on Quote approval stage and User's profile
Deepika Rawat          04-Dec-15           Added Before and After update logic  
****************************************************************************************************/
trigger CPQ_LineItemTrigger on Apttus_Config2__LineItem__c (before delete, after update, before Update) {
     
     if(Trigger.isDelete && Trigger.isBefore){
         CPQ_LineItemTriggerHelper.restrictCartLineItemDeletion(Trigger.old);
     }
     
     if(Trigger.isUpdate && Trigger.isBefore){
         CPQ_LineItemTriggerHelper.restoreBasePriceOverride(Trigger.newMap, Trigger.oldMap);
     }
         
     if(Trigger.isUpdate && Trigger.isAfter){
         CPQ_LineItemTriggerHelper.makeFromCustomProductFlagFalse(Trigger.new);
     }
}