trigger ServiceableLocationTrigger on ServiceableLocation__c (before insert, before update) {
   if(Label.isTriggerActive.equalsIgnoreCase('true')){ 
    /* To identify whether a Serviceable Location is a CNI or an NNI we need an identifier on 
       the object to specify whether the record is either a CNI or NNI.  Since the access type
       group indicates CNI or NNI we can use this to set the value of the CNI NNI Type.
       We can then use this field to display or query whether the object is a CNI or NNI.
    */
    
    for(ServiceableLocation__c s: trigger.new){
    	if (!Utils.isEmpty(s.Access_Type_Group__c)){
	    	if(s.Access_Type_Group__c.contains('CNI'))
	    		s.CNINNI__c = 'CNI';
	    	else if(s.Access_Type_Group__c.contains('NNI'))
	    		s.CNINNI__c = 'NNI';
    	}
    }
  }
}