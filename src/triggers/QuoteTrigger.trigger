/* ********************************************************************
 * Name : QuoteTrigger
 * Description : Trigger. Before Insert and Before Update Trigger
 * Modification Log
  =====================================================================
 * Ver    Date          Author               Modification
 ----------------------------------------------------------------------
 * 1.x    -/--/2011     Kevin DesLauriers    Initial Version 
 * 1.2    5/28/2012     Kevin DesLauriers    Added Version Control
 * 1.3    6/25/2012     Kevin DesLauriers    Allow for Discount Approval Tracking
 * 1.4    7/5/2012      Kevin DesLauriers    Empty ListPrice Discount Approval  
 * 1.5    8/26/2012     Kevin DesLauriers    Sales Manager Disc Approval Reminder
 * 1.6    9/4/2012      Kevin DesLauriers    Add Inconsistent Pricing (Cost Price does not equal List Price)
 * 1.7    11/4/012      Kevin DesLauriers    Add Product Sheets
 * 1.8    2/3/2013      Kevin DesLauriers    Updated the sales reminder so that it uses the managers email.
 * 1.9    8/2/2013      Kevin DesLauriers    Added temp fields to remove escape sequence for popup reasons.
 * 1.10   9/1/2013      Kevin DesLauriers    Add Roll-up for Number of NON ONNET Quote Sites on Quote for new Approval Process
 **********************************************************************/

trigger QuoteTrigger on Quote (before insert, before update) {
     if(Label.isTriggerActive.equalsIgnoreCase('true')){
    // Only run if this trigger hasn't run before
    system.debug('hasRun: ' + ShiftQuoteTriggerSupport.hasRun); 
    if (ShiftQuoteTriggerSupport.hasRun) return;
    //ShiftQuoteTriggerSupport.hasRun = true;
    
    if(!Test.isRunningTest()){
    numberTranslation nt = new numberTranslation();
   
    List<Id> quoteids = new List<Id>();
    list<Quote> quoteToEscalate = new List<Quote>();
    
    List<Id> originalQuoteIds = new List<Id>();
    for (Quote q : Trigger.New){
        q.Finance_Email_Box__c = q.Finance_Email_Box2__c;
        if (Trigger.isBefore){
            if (q.Sales_Discount_Reminder__c){
                Quote oldQuote = Trigger.oldMap.get(q.id); 
                if (q.Sales_Discount_Reminder__c != oldQuote.Sales_Discount_Reminder__c) { 
                    q.Sales_Discount_Reminder__c = false;
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setReplyTo('jose.camacho@rci.rogers.com');
                    mail.setSenderDisplayName('Salesforce Administrator');
                    mail.setTemplateId('00Xi0000000FUkX'); //EMAIL Template: Reminder Approval Template Notification PM
                    
                    // we need the manager's id of the opp owner
                    mail.setTargetObjectId(q.Manager_s_id__c);
                    mail.setWhatId(q.id);
                    mail.setSaveAsActivity(false);
                    try{
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                    }catch(Exception ex){
                        Utils.sendEmail(ex.getMessage(), 'QuoteTrigger.trigger: Error Message', new String[] {Label.adminEmail});
                    }
                    
                }
            }
        }
        
        quoteids.add(q.id);
        /* 5/28/2012 KD - Update Version Control Started */
        if (q.Original_Quote__c!=null)
            originalQuoteIds.add(q.Original_Quote__c);
        if (q.Version__c == null){
            q.Version__c = 1;
            q.Number_Of_Derived_Quotes__c = 0;
        }
        /* 5/28/2012 KD - Update Version Control Completed */
        
        /* 6/1/2012 KD - Update Flexible Term Field for Layout Started */
        if (Utils.isEmpty(q.actualTerm__c))
            q.actualTerm__c = q.term__c.split('-')[0];
        /* 6/1/2012 KD - Update Flexible Term Field for Layout Ended */
        
    }
    
    /* 9/1/2013 KD - Access Type Fields at Quote Level */
    // Grab all of the Quote Sites and check how many are ONNET / OFF NET / NEARNET - and make a map out if it with Quote Id as the key
    Map<Id, List<Quote_Site__c>> quoteSiteMap = new Map<Id, List<Quote_Site__c>>();
    List<Quote_Site__c> qSiteList = null;
    for (Quote_Site__c qSite : [SELECT id, Access_Type__c, Quote__r.Id, Site__r.Display_Name__c FROM Quote_Site__c WHERE Quote__r.Id IN :quoteIds]){
         qSiteList = quoteSiteMap.get(qSite.Quote__r.Id);
         if (qSiteList == null)
            qSiteList = new List<Quote_Site__c>();
         qSiteList.add(qSite);
         quoteSiteMap.put(qSite.Quote__r.Id, qSiteList);
    }
    
    // We now have all of the Quote Sites grouped by Quote
    for (Quote q : Trigger.New){
        List<String> nonONNETSites = new List<String>();
        if (quoteSiteMap.get(q.Id) != null){
            for (Quote_Site__c qSite : quoteSiteMap.get(q.Id)){
                if (qSite.Access_Type__c != null && (!qSite.Access_Type__c.toLowerCase().contains('on net') && !qSite.Access_Type__c.toLowerCase().contains('onnet'))){
                    nonONNETSites.add(qSite.Site__r.Display_Name__c + ' - ' + qSite.Access_Type__c);
                }else if (qSite.Access_Type__c == null){
                    nonONNETSites.add(qSite.Site__r.Display_Name__c + ' - Unspecified');
                }
            }
        }
        q.NON_ON_NET_Quote_Sites__c = Utils.parseList(nonONNETSites, '\n');
        q.NON_ON_NET_Quote_Sites__c = Utils.removeHTML(q.NON_ON_NET_Quote_Sites__c);
        q.Number_of_OFFNET_NEARNET_Quote_Sites__c = nonONNETSites.size();
        
        q.Contains_ON_NET_Only__c = q.Number_of_OFFNET_NEARNET_Quote_Sites__c == 0;
        
    }
    
    /* 5/28/2012 KD - Update Version Control Started */
    if (Trigger.isInsert && originalQuoteIds.size()!=null){
        Map<Id, Quote> originalQuoteMap = new Map<Id, Quote>([SELECT id, Number_Of_Derived_Quotes__c FROM Quote WHERE Id IN :originalQuoteIds]);
        for (Quote q : Trigger.New){
            Quote originalQuote = originalQuoteMap.get(q.Original_Quote__c);
            if (originalQuote!=null){   // From a cloned quote
                if (originalQuote.Number_Of_Derived_Quotes__c != null)
                    q.version__c = originalQuote.Number_Of_Derived_Quotes__c + 2;
                else 
                    q.version__c = 2;
            }
        }
    }
    /* 5/28/2012 KD - Update Version Control Completed */
    
    List<QuoteLineItem> qliMasterList = [SELECT Id, Product_Information_Sheet__c, quoteid, Service_Type__c, discount, ListPrice, Cost_Price__c, TotalPrice, isEmptyListPrice__c, isNRRDiscount__c, isSmallDiscount__c, isLargeDiscount__c, isMediumDiscount__c, isInconsitentPrice__c  FROM QuoteLineItem WHERE quoteId IN :quoteids AND (isInconsitentPrice__c = 1 OR isLargeDiscount__c = 1 OR isSmallDiscount__c = 1 OR isMediumDiscount__c = 1 OR isNRRDiscount__c = 1 OR isEmptyListPrice__c = 1 OR Product_Information_Sheet__c != null)];
        
     List<QuoteLineItem> qliInconsistentPricing = new List<QuoteLineItem>();
     List<QuoteLineItem> qliLargeDiscount = new List<QuoteLineItem>();
     List<QuoteLineItem> qliMediumDiscount = new List<QuoteLineItem>();
     List<QuoteLineItem> qliSmallDiscount = new List<QuoteLineItem>();
     List<QuoteLineItem> qliNRRDiscount = new List<QuoteLineItem>();
     List<QuoteLineItem> qliEmptyListPrice = new List<QuoteLineItem>();

     
     // Product Information Sheets
     List<QuoteLineItem> qliSip = new List<QuoteLineItem>();
     List<QuoteLineItem> qliRdi = new List<QuoteLineItem>();


     List<QuoteLineItem> qliSipRdiBundle = new List<QuoteLineItem>();
     
     for (QuoteLineItem qliChild : qliMasterList){
     
         if (qliChild.isInconsitentPrice__c == 1){
             qliInconsistentPricing.add(qliChild); 
         }
         if (qliChild.isLargeDiscount__c == 1){
             qliLargeDiscount.add(qliChild); 
         }
         if (qliChild.isSmallDiscount__c == 1){
             qliSmallDiscount.add(qliChild); 
         }
         if (qliChild.isNRRDiscount__c == 1){
             qliNRRDiscount.add(qliChild); 
         }
         if (qliChild.isEmptyListPrice__c == 1){
             qliEmptyListPrice.add(qliChild); 
         }

         if (qliChild.isMediumDiscount__c == 1){
             qliMediumDiscount.add(qliChild); 
         }


         
         if (!Utils.isEmpty(qliChild.Product_Information_Sheet__c)){
             if (qliChild.Product_Information_Sheet__c.equals('SIP Trunking')){
                 qliSip.add(qliChild); 
             }
             
             if (qliChild.Product_Information_Sheet__c.equals('RBS Dedicated Internet')){
                 qliRdi.add(qliChild); 
             }
             

             if (qliChild.Product_Information_Sheet__c.equals('SIP and RDI Bundle')){
                 qliSipRdiBundle.add(qliChild); 
             }

         }
     }
      
   
     // Create Map for QuoteLineItems to key=quoteid
     Map<Id, List<QuoteLineItem>> mapLargeDiscountItems = new Map<Id, List<QuoteLineItem>>();
     Map<Id, List<QuoteLineItem>> mapSmallDiscountItems = new Map<Id, List<QuoteLineItem>>();
Map<Id, List<QuoteLineItem>> mapMediumDiscountItems = new Map<Id, List<QuoteLineItem>>();
     Map<Id, List<QuoteLineItem>> mapNRRDiscountItems = new Map<Id, List<QuoteLineItem>>();
     Map<Id, List<QuoteLineItem>> mapEmptyListPriceItems = new Map<Id, List<QuoteLineItem>>();
     Map<Id, List<QuoteLineItem>> mapInconsistentPriceItems = new Map<Id, List<QuoteLineItem>>();
     
     // Product Sheet Map
     Map<Id, List<QuoteLineItem>> mapSipItems = new Map<Id, List<QuoteLineItem>>();
     Map<Id, List<QuoteLineItem>> mapRdiItems = new Map<Id, List<QuoteLineItem>>();


     Map<Id, List<QuoteLineItem>> mapSipRdiBundleItems = new Map<Id, List<QuoteLineItem>>();
            
     for (QuoteLineItem ql : qliLargeDiscount){
         List<QuoteLineItem> largeList = null;
         if (mapLargeDiscountItems.get(ql.quoteId)!=null){
             largeList = mapLargeDiscountItems.get(ql.quoteId);
         }else{
             largeList = new List<QuoteLineItem>();
         }
         
         largeList.add(ql);
         mapLargeDiscountItems.put(ql.quoteId, largeList);
    }
    
    for (QuoteLineItem ql : qliEmptyListPrice){
         List<QuoteLineItem> emptyListPriceList = null;
         if (mapEmptyListPriceItems.get(ql.quoteId)!=null){
             emptyListPriceList = mapEmptyListPriceItems.get(ql.quoteId);
         }else{
             emptyListPriceList = new List<QuoteLineItem>();
         }
         
         emptyListPriceList.add(ql);
         mapEmptyListPriceItems.put(ql.quoteId, emptyListPriceList);
    }
    
      for (QuoteLineItem qlSmall : qliSmallDiscount){
         List<QuoteLineItem> smallList = null;
         if (mapSmallDiscountItems.get(qlSmall.quoteId)!=null){
             smallList= mapSmallDiscountItems.get(qlSmall.quoteId);
         }else{
             smallList = new List<QuoteLineItem>();
         }
         
         smallList.add(qlSmall);
         mapSmallDiscountItems.put(qlSmall.quoteId, smallList);
    }
    
    for (QuoteLineItem qlMedium : qliMediumDiscount){
         List<QuoteLineItem> MediumList = null;
         if (mapMediumDiscountItems.get(qlMedium.quoteId)!=null){
             MediumList= mapMediumDiscountItems.get(qlMedium.quoteId);
         }else{
             MediumList = new List<QuoteLineItem>();
         }
         
         MediumList.add(qlMedium);
         mapMediumDiscountItems.put(qlMedium.quoteId, MediumList);
    }


    for (QuoteLineItem ql : qliNRRDiscount){
         List<QuoteLineItem> NRRList = null;
         if (mapNRRDiscountItems.get(ql.quoteId)!=null){
             NRRList = mapNRRDiscountItems.get(ql.quoteId);
         }else{
             NRRList = new List<QuoteLineItem>();
         }
         
         NRRList.add(ql);
         mapNRRDiscountItems.put(ql.quoteId, NRRList);
    }
    
     for (QuoteLineItem ql : qliInconsistentPricing){
         List<QuoteLineItem> inconsistentPriceList = null;
         if (mapInconsistentPriceItems.get(ql.quoteId)!=null){
             inconsistentPriceList = mapInconsistentPriceItems.get(ql.quoteId);
         }else{
             inconsistentPriceList = new List<QuoteLineItem>();
         }
         
         inconsistentPriceList.add(ql);
         mapInconsistentPriceItems.put(ql.quoteId, inconsistentPriceList);
    }
    
   for (QuoteLineItem qlSip : qliSip){
         List<QuoteLineItem> sipList = null;
         if (mapSipItems.get(qlSip.quoteId)!=null){
             sipList= mapSipItems.get(qlSip.quoteId);
         }else{
             sipList = new List<QuoteLineItem>();
         }
         
         sipList.add(qlSip);
         mapSipItems.put(qlSip.quoteId, sipList);
    }
         
    
    for (QuoteLineItem ql : qliRdi){
         List<QuoteLineItem> RDIList = null;
         if (mapRdiItems.get(ql.quoteId)!=null){
             RDIList = mapRdiItems.get(ql.quoteId);
         }else{
             RDIList = new List<QuoteLineItem>();
         }
         
         RDIList.add(ql);
         mapRdiItems.put(ql.quoteId, RDIList);
    }
    

    for (QuoteLineItem ql : qliSipRdiBundle){
         List<QuoteLineItem> sipRdiBundleList = null;
         if (mapSipRdiBundleItems.get(ql.quoteId)!=null){
             sipRdiBundleList = mapSipRdiBundleItems.get(ql.quoteId);
         }else{
             sipRdiBundleList = new List<QuoteLineItem>();
         }
         
         sipRdiBundleList.add(ql);
         mapSipRdiBundleItems.put(ql.quoteId, sipRdiBundleList);
    }

    
        for (Quote q : Trigger.New){            
             List<QuoteLineItem> largeItems = mapLargeDiscountItems.get(q.id);
             q.Large_Discount_Services__c= '';
             
             
             if (largeItems != null){
                 for (QuoteLineItem qli : largeItems ){
                     q.Large_Discount_Services__c += qli.Service_Type__c + ' (Discount: '+nt.formatDouble(qli.discount, false, '', 2 )+'%)\n';
                 }
             } 
             

             List<QuoteLineItem> mediumItems = mapMediumDiscountItems.get(q.id);
             q.Medium_Discount_Services__c= '';
             
             
             if (mediumItems != null){
                 for (QuoteLineItem qliMedium : mediumItems ){
                     q.medium_Discount_Services__c += qliMedium.Service_Type__c + ' (Discount: '+nt.formatDouble(qliMedium.discount, false, '', 2 )+'%)\n';
                 }
             } 
             
             List<QuoteLineItem> smallItems = mapSmallDiscountItems.get(q.id);
             q.Small_Discount_Services__c= '';
              if (smallItems != null){
                  for (QuoteLineItem qliSmall : smallItems ){
                         q.Small_Discount_Services__c += qliSmall.Service_Type__c + ' (Discount: '+nt.formatDouble(qliSmall.discount, false, '', 2 )+'%)\n';
                     }
                  }
             
              List<QuoteLineItem> emptyCostItems = mapEmptyListPriceItems.get(q.id);
             q.Empty_List_Prices_Services__c= '';
             
             
             if (emptyCostItems != null){
                 for (QuoteLineItem qli : emptyCostItems ){
                     q.Empty_List_Prices_Services__c += qli.Service_Type__c + ' (Total Price: '+nt.formatDouble(qli.totalPrice, true, '')+')\n';
                 }
             }   
             
             List<String> productSheets = new List<String>();
             // Product Information Sheets - Need to update the quote picklist default values
             if (!qliSip.isEmpty()){
                productSheets.add('SIP Trunking');
             }
             
             if (!qliRdi.isEmpty()){
                productSheets.add('RBS Dedicated Internet'); 
             }
             
             if (!qliSip.isEmpty() && !qliRdi.isEmpty()){ 

                productSheets.add('SIP and RDI Bundle');
             }   
             
             if (!qliSipRdiBundle.isEmpty()){
                productSheets.add('SIP and RDI Bundle');
                productSheets.add('SIP Trunking');
                productSheets.add('RBS Dedicated Internet');
             }

             q.Allowable_Product_Information_Sheets__c = Utils.makeMultiPicklist(productSheets);
             
             List<String> selectedSheets = new List<String>();
             // If the user selects prooduct sheets that are not available then display a validation error.
             
             if (!Utils.isEmpty(q.Product_Information_Sheet__c)){
             
                selectedSheets = q.Product_Information_Sheet__c.split(';');
                
                Set<String> availableSheets = new Set<String>(productSheets);
                Set<String> selected = new Set<String>(selectedSheets);
                Set<String> updatedSheets = new Set<String>();
                

                if (selected.contains(Label.SIP_and_RDI_Bundle)){
                    selected.add(Label.SIP_Trunking);
                    selected.add(Label.RBS_Dedicated_Internet);
                    selected.remove(Label.SIP_and_RDI_Bundle);
                }

                
                for (String s : selected){
                    if (availableSheets.contains(s)){
                        updatedSheets.add(s);
                    }
                }
                
                

                if (updatedSheets.contains(Label.SIP_Trunking) && updatedSheets.contains(Label.RBS_Dedicated_Internet)){
                    updatedSheets.remove(Label.SIP_Trunking);
                    updatedSheets.remove(Label.RBS_Dedicated_Internet);
                    updatedSheets.add(Label.SIP_and_RDI_Bundle);
                    
                }

                
                q.Product_Information_Sheet__c = Utils.makeMultiPicklist(new List<String>(updatedSheets));
                /*
                if (!availableSheets.containsAll(selected)){
                    q.addError('Invalid Closing Kit(s) selected.  The allowed closing kit(s) are: ' + (productSheets.isEmpty()?'None' : Utils.parseList(productSheets, ',')));
                }
                */
             }
             
                
             List<QuoteLineItem> inconsistentPriceItems = mapInconsistentPriceItems.get(q.id);
             q.Inconsistent_Pricing_Services__c= '';
             
             
             if (inconsistentPriceItems != null){
                 for (QuoteLineItem qli : inconsistentPriceItems ){
                     q.Inconsistent_Pricing_Services__c += qli.Service_Type__c + ' (List Price: '+nt.formatDouble(qli.ListPrice, true, '')+' - Cost Price: '+(qli.Cost_Price__c!=null?nt.formatDouble(qli.Cost_Price__c, true, ''):'$0.00')+')\n';
                 }
             }     
                
             List<QuoteLineItem> NRRItems = mapNRRDiscountItems.get(q.id);
             q.NRR_Discount_Services__c= '';
              if (NRRItems != null){
                  for (QuoteLineItem qliNRR : NRRItems ){
                         q.NRR_Discount_Services__c += qliNRR.Service_Type__c + ' (Discount: '+nt.formatDouble(qliNRR.discount, false, '', 2 )+'%)\n';
                     }
                  }
             
                }
   
     Map<Id, List<QuoteLineItem>> lineItemMap = new Map<Id, List<QuoteLineItem>>();
     Set<Id> ids;
    
    if (Trigger.isUpdate){
        ids = Trigger.newMap.keySet();
        
       
        
        try{
            List<QuoteLineItem> qlis = [SELECT Id, quoteid, discount, TotalPrice, SubTotal, Charge_Type__c,UnitPrice FROM QuoteLineItem WHERE quoteId IN :ids];
           
        
            
            System.debug(qlis);
            if (qlis != null && !qlis.isEmpty()){
                for (QuoteLineItem qli : qlis){
                    List<QuoteLineItem> tempItems = lineItemMap.get(qli.QuoteId);
                    if (tempItems== null){
                        tempItems = new List<QuoteLineItem>();
                    }
                    
                    tempItems.add(qli);
                    lineItemMap.put(qli.QuoteId,tempItems);
                }
            }
        }catch(Exception ex){
            System.debug('No Quote Line Items with MRC');
        }
   
  
            
          
            
        
        
      }
      
 List <Scheduled_Quote_Approval_History__c> schedQuotes = new List<Scheduled_Quote_Approval_History__c>();
   // List <Scheduled_Quote_Approval_History__c> schedQuotesToUpdate = new List<Scheduled_Quote_Approval_History__c>();
   
    //--------Changes By Deepika. Mar 27th. START.-----------------
    //Created Map for Process Instance.
     Set<ID> quoteAllIds = new Set<ID>();
     Set<Id> qIds = new Set<Id>();
    for (Quote q : trigger.new){
        quoteAllIds.add(q.Id);
    }
    if (Trigger.isUpdate){
        for (Quote q1 : Trigger.New){
            if ((q1.Expiry_Extension_Status__c == 'Rejected') || (q1.Expiry_Extension_Status__c == 'Approved') || (q1.Discount_Approval_Status__c == 'NSA Rejected') || (q1.Discount_Approval_Status__c == 'Final Approved') || (q1.Discount_Approval_Status__c == 'Final Rejection') || (q1.Discount_Approval_Status__c == 'Awaiting Discount Approval Submission') || (q1.Discount_Approval_Status__c == 'Sales Manager Approved') || (q1.Discount_Approval_Status__c == 'VP Approved') || (q1.Discount_Approval_Status__c == 'Director Approved')){
                qIds.add(q1.id);
            }
        }
    }
    List<Scheduled_Quote_Approval_History__c> listAllStep = [SELECT id, Approval_History__c, Quote__c, ProcessInstanceId__c FROM Scheduled_Quote_Approval_History__c WHERE Quote__c in :quoteAllIds Order by CreatedDate desc LIMIT 1];
    Map<id,Scheduled_Quote_Approval_History__c> mapQuoteHistory = new Map<id,Scheduled_Quote_Approval_History__c>();
    for(Scheduled_Quote_Approval_History__c qHistory : listAllStep){
        if(mapQuoteHistory.get(qHistory.Quote__c)==null)
            mapQuoteHistory.put(qHistory.Quote__c, qHistory);
    }
    Map<id, ProcessInstance> mapProcessInstance = new Map<id, ProcessInstance>();
    // TODO: Place this in a map
    List<ProcessInstance> listAllProcess = [SELECT Id, TargetObjectId FROM ProcessInstance WHERE TargetObjectId in :quoteAllIds Order BY LastModifiedDate desc  ];
    for(ProcessInstance p : listAllProcess){
        if(mapProcessInstance.get(p.TargetObjectId)==null)
            mapProcessInstance.put(p.TargetObjectId, p);
    }
    //Moved Map quoteMap outside of the for loop.
    Map<Id, Quote> quoteMap = new Map<Id, Quote>([SELECT id, Opportunity.Owner.Use_SAM_Approval_Process__c FROM Quote WHERE id IN :ids]);
    //Moved List approvals outside of the for loop.
    List<ProcessInstance> approvals = [SELECT Id,  
                                            Status, 
                                            TargetObjectId, 
                                                (SELECT Id, 
                                                    StepStatus, 
                                                    ActorId,
                                                    Actor.Name, 
                                                    OriginalActor.Name, 
                                                    CreatedDate, Comments 
                                                    FROM StepsAndWorkitems Order By CreatedDate DESC) 
                                            FROM ProcessInstance 
                                            WHERE TargetObjectId IN :qIds Order By LastModifiedDate DESC LIMIT 1];
    ////--------Changes By Deepika. Mar 27th. STOP.-----------------
    for (Quote q : trigger.new){
        Decimal subTotal_MRC = 0;
        Decimal totalPrice_MRC = 0;
        Decimal subTotal_NRC = 0;
        Decimal totalPrice_NRC = 0;
        
        List<QuoteLineItem> qlItems = lineItemMap.get(q.Id);
        if (qlItems!=null){
            for (QuoteLineItem qli1 : qlItems){
                if (qli1.charge_Type__c.equals('MRC')){
                    totalPrice_MRC += qli1.TotalPrice;
                    subTotal_MRC += qli1.Subtotal;   
                }else if(qli1.charge_Type__c.equals('NRC')){
                    totalPrice_NRC += qli1.TotalPrice;
                    subTotal_NRC += qli1.Subtotal;
                }
            }
        }
        
        if (Trigger.isUpdate){
            for (Quote q1 : Trigger.New){
                q1.Owner_has_SAM_Approval_Ability__c = quoteMap.get(q1.id).Opportunity.Owner.Use_SAM_Approval_Process__c;
                if (q1.proceedWithRequestForExtension__c){
                    q1.ExpirationDate = q1.Requested_Expiry_Date__c;
                    //q1.Requested_Expiry_Date__c = null;           
                    q1.requestExtension__c = false;
                    q1.Extension_Requested_days__c = null;
                    q1.proceedWithRequestForExtension__c = false;
                }
            }
            if (ids.size()>0){
            //  try{
                    for (ProcessInstance approval : approvals){
                        for (Quote q1 : Trigger.New){
                            if (approval != null && q1.Id == approval.TargetObjectId && approval.StepsAndWorkitems != null && approval.StepsAndWorkitems.size()>0){
                                q1.Recent_Approval_Notes__c  = approval.StepsAndWorkitems[0].Comments;
                            }
                        }
                    }
            //  }catch(Exception ex){
                    
           //   }
            }
            
            // Allow for Discount Approval Process Tracking - KD - 6/25/2012
            if (q.Discount_Approval_Status__c !=null){
                //Assign the before and after the change into a Map
                Map<Id,Quote> newQuoteMap = Trigger.newMap;
                Map<Id,Quote> oldQuoteMap = Trigger.oldMap;
            
                //Loop through the map
                for(Id quoteId : newQuoteMap.keySet()){
                    Quote myNewQuote = newQuoteMap.get(quoteId);
                    Quote myOldQuote = oldQuoteMap.get(quoteId);                

                    if (myNewQuote.Discount_Approval_Status__c != myOldQuote.Discount_Approval_Status__c){
                        if (q.Discount_Approval_Status__c.equals('Approval Started') || ([SELECT count() FROM Discount_Approval_History__c WHERE quote__c = :q.Id] != 0) || ([SELECT count() FROM Scheduled_Quote_Approval_History__c WHERE quote__c = :q.Id] != 0)){
                            // TODO: Place this in a map
                            ProcessInstance lastProcess = mapProcessInstance.get(q.id); //Removed direct query. Using Map mapProcessInstance now. Changes by : Deepika Mar_27
                            Scheduled_Quote_Approval_History__c schedQuote;
                            Scheduled_Quote_Approval_History__c lastStep;
                            try{
                                lastStep = mapQuoteHistory.get(q.Id);
                            }catch(Exception ex){
                                
                            }
                            
                            if (lastStep != null && lastStep.Approval_History__c == q.Discount_Approval_Status__c){
                                // We want to update not add
                                schedQuote = lastStep;
                            }else{
                                schedQuote = new Scheduled_Quote_Approval_History__c(Quote__c = q.Id, Approval_History__c = q.Discount_Approval_Status__c, ProcessInstanceId__c = lastProcess.Id);
                                
                            }
                            
                            if (q.Quote_Type__c.equals('Carrier'))
                                schedQuote.isCarrier__c = true;
                            
                            schedQuote.Discount_Reasons__c = q.Discount_Reasons__c;
                            
                            if (schedQuote.Approval_History__c != 'Approval Started')
                                schedQuote.Approver_Comment__c = q.Recent_Approval_Notes__c;
                            else
                                schedQuote.Approver_Comment__c = '';
                            
                            schedQuote.Processed_By_Finance__c = q.Finance_Approved__c;
                            schedQuotes.add(schedQuote);
                        }
                    }
                }
            }
            
        }
        
        
        if (q.discountProcessStarted__c){

        if (Trigger.isUpdate){
            
            
            Decimal mrcDiscount = 0;
            Decimal nrcDiscount = 0;
            Decimal totalAfterDiscount = 0;
            Decimal totalBeforeDiscount = 0;
            Decimal totalNRCAfterDiscount = 0;
            Decimal totalNRCBeforeDiscount = 0;

            List<QuoteLineItem> lineItems = lineItemMap.get(q.Id);
            System.debug(lineItems);
            if (lineItems != null && !lineItems.isEmpty()){
                for (QuoteLineItem qli : lineItems){
                    if (qli.charge_Type__c.equals('MRC') && qli.UnitPrice>0){
                        totalBeforeDiscount += qli.UnitPrice;
                        if (qli.Discount!=null)
                              totalAfterDiscount += ((100 - qli.Discount) * qli.UnitPrice)/100;
                        else 
                            totalAfterDiscount = totalBeforeDiscount;
                    }else if (qli.charge_Type__c.equals('NRC') && qli.UnitPrice>0){
                        totalNRCBeforeDiscount += qli.UnitPrice;
                        if (qli.Discount!=null)
                              totalNRCAfterDiscount += ((100 - qli.Discount) * qli.UnitPrice)/100;
                        else 
                            totalNRCAfterDiscount = totalNRCBeforeDiscount;
                    }
                }
            }
            
            if (totalBeforeDiscount>0){
                mrcDiscount = (1 - (totalAfterDiscount / totalBeforeDiscount)) * 100;
            }
            
            if (totalNRCBeforeDiscount>0){
                nrcDiscount = (1 - (totalNRCAfterDiscount / totalNRCBeforeDiscount)) * 100;
            }
            
             q.Discount_NRC__c = nrcDiscount;
             q.Discount_MRC__c = mrcDiscount;
             q.discountProcessStarted__c = false;
            
             
            }else{
                q.discountProcessStarted__c = false;
            }
        }
        q.Total_Price_NRC__c = totalPrice_NRC;
        q.Subtotal_NRC__c = subTotal_NRC;
        q.Total_Price_MRC__c = totalPrice_MRC;
        q.Subtotal_MRC__c = subTotal_MRC;
        q.Non_Serviceable_Site_on_Quote__c = (q.Contains_Non_Serviceable_Site__c>0?true:false);
        
        /* Both sets of reasons are sent to a Popup via a button.  Existing formatting causes an error - so this is the work around */
        if (!Utils.isEmpty(q.Discount_Reasons__c))
            q.TempDiscountReasons__c = q.Discount_Reasons__c.replace('\r', '\\r').replace('\n', '\\n').replace('\'', '\\\'').replace('\"', '\\\"');
        else
            q.TempDiscountReasons__c =  null;
            
        if (!Utils.isEmpty(q.Competitor__c))
            q.TempCompetitorName__c = q.Competitor__c.replace('\r', '\\r').replace('\n', '\\n').replace('\'', '\\\'').replace('\"', '\\\"');
        else
            q.TempCompetitorName__c =  null;
        
        if (!Utils.isEmpty(q.Extension_Reasons__c))
            q.TempExtensionReasons__c = q.Extension_Reasons__c.replace('\r', '\\r').replace('\n', '\\n').replace('\'', '\\\'').replace('\"', '\\\"');
        else
            q.TempExtensionReasons__c =  null;
            
        /* Let's update the account team on the quote so we can use it in the NSA Approval Notification */
        /*
         String aTeamHTML = '';
         try{
            List<AccountTeamMember> accountTeam = [SELECT AccountId, Id, UserId, User.Name, User.Alias FROM AccountTeamMember WHERE accountId = :q.AccountId__c AND TeamMemberRole IN ('Network Solution Architect - Primary','Network Solution Architect - Back Up')];    
            String aTeam = '';
            for (AccountTeamMember aTeamMember : accountTeam){
                aTeam += '[' + aTeamMember.User.Alias + ']';
            }
            q.Account_Team_NSAs__c = aTeam;
        }catch(Exception ex){
            System.debug('Exception: Unable to obtain the Team members for the account');
            q.Account_Team_NSAs__c = '';
        }
        */
    }    
    if (!schedQuotes.isEmpty())
        UPSERT schedQuotes;
   }else{
    for (Quote q : trigger.new){
        q.Non_Serviceable_Site_on_Quote__c = (q.Contains_Non_Serviceable_Site__c>0?true:false);
    }
    
   }
  }  
}