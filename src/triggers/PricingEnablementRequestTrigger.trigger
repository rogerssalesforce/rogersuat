/*===============================================================================
 Class Name   : PricingEnablementRequestTrigger
===============================================================================
PURPOSE:    Trigger on PER to ensure its not marked final if another PER of associated Opp is marked final 

Developer: Shazib Mahmood
Date: 06/11/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
06/11/2015          Shazib                    Original Version
===============================================================================
*/

trigger PricingEnablementRequestTrigger on Pricing_Enablement_Request__c (before insert, before update) {
if(Label.isTriggerActive.equalsIgnoreCase('true')){
for(Pricing_Enablement_Request__c PCR0 : Trigger.new){
    if(PCR0.Final_Request__c==TRUE){
    List<id> OppIdList = new List<id>();
	for(Pricing_Enablement_Request__c PCR : Trigger.new){
        OppIdList.Add(PCR.Opportunity__c);
    }
List<Pricing_Enablement_Request__c> AllPCRsfromOppIdList = new List<Pricing_Enablement_Request__c>([SELECT Id, Name, Opportunity__c, Final_Request__c FROM Pricing_Enablement_Request__c WHERE Opportunity__c IN :OppIdList AND Final_Request__c=True]);

   for(Pricing_Enablement_Request__c PCR2 : Trigger.new){
       for(Pricing_Enablement_Request__c PCR3 : AllPCRsfromOppIdList){
       if (PCR2.Opportunity__c == PCR3.Opportunity__c && PCR2.id != PCR3.id){
           PCR2.AddError('The associated opportunity already has another Pricing Enablement Request marked as the Final Request');
       }
    }
   }

/*
for(Pricing_Enablement_Request__c PCR4 : Trigger.new){
    if(PCR4.Final_Request__c==TRUE && Trigger.oldMap.get(PCR4.id).Final_Request__c != Trigger.newMap.get(PCR4.Id).Final_Request__c){
		Pricing_Enablement_Request__c PCR5 = [SELECT Id, Name, Opportunity__c, Final_Request__c, Opportunity__r.Final_Pricing_Enablement_Request__c FROM Pricing_Enablement_Request__c WHERE id=:PCR4.Id];
        PCR5.Opportunity__r.Final_Pricing_Enablement_Request__c=PCR4.Id;
    }
    if(PCR4.Final_Request__c==False && Trigger.oldMap.get(PCR4.id).Final_Request__c != Trigger.newMap.get(PCR4.Id).Final_Request__c){
    PCR4.Opportunity__r.Final_Pricing_Enablement_Request__c='';   
    }
} */ 
}
}
}
}