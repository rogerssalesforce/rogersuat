/*Trigger Name  : TAG_UpdateDistrictOwner
*Description : This Trigger will support the logic for District Owner after Update.
*Created By  : Jayavardhan.
*Created Date :27/01/2015.
* Modification Log :
------------------------------------------------------------------------------------------------------------------------------
                Developer         Version       Date        Description
                Jayavardhan         1.0       27/01/2015    Get list of all Account,MSD,Shared MSD for a District Owner and 
                                                            update them with owner and set a flag which is used for MAL integration and Account recalculation 
------------------------------------------------------------------------------------------------------------------------------
*
*/
Trigger TAG_UpdateDistrictOwner on District__c (before update, after update) {
    if(Label.isTriggerActive.equalsIgnoreCase('true')){
    Map<ID,District__c> mapIdDistrict  =  new Map<ID,District__c>(); 
    
    if(Trigger.isAfter &&  Trigger.isUpdate){ 
        
        for(District__c distRec:Trigger.new){
            system.debug('============>Newmap-OldMap'+Trigger.newMap.get(distRec.ID).District_Owner__c+'======'+Trigger.oldMap.get(distRec.ID).District_Owner__c+'=========='+Trigger.newMap.get(distRec.ID).Inactive__c);
            if(Trigger.newMap.get(distRec.ID).District_Owner__c!= Trigger.oldMap.get(distRec.ID).District_Owner__c && Trigger.newMap.get(distRec.ID).Inactive__c == False){
                mapIdDistrict.put(distRec.ID,Trigger.newMap.get(distRec.ID));                
            }            
        } 
        
        if(!mapIdDistrict.isEmpty()){
            system.debug('============>distsize'+mapIdDistrict.keyset().size());
          //  TAG_DistrictOwner_Upd_Acc_MSD_SharedMSD.UpdatedDistrictOwner(mapIdDistrict);
           
           Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
           Integer batchSize = Integer.ValueOf(TAGsettings.DistrictOwnerUpdateBatch_size__c);
        
           TAG_DistOwnrUpdate_Account_batch sc1 = new TAG_DistOwnrUpdate_Account_batch(mapIdDistrict);
           ID batchprocessid = Database.executeBatch(sc1,batchSize);
        }   
    }
  }
}