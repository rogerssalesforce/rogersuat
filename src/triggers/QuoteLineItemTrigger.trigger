/* ********************************************************************
 * Name : QuoteTrigger_AU_AI
 * Description : Trigger. After Insert and Before Update Trigger
 * Modification Log
  =====================================================================
 * Ver    Date          Author                  Modification
 ----------------------------------------------------------------------
 * 1.0    6/--/2011     Kevin DesLauriers       Initial Version  
 * 1.1    7/19/2012     Kevin DesLauriers       Remove storing of quoteIds outside isBefore
 * 1.2    11/18/2012    Kevin DesLauriers       Overage for Carrier
 * 1.3    09/23/2013    Kevin DesLauriers       Medium Discount for Carrier 
 **********************************************************************/
trigger QuoteLineItemTrigger on QuoteLineItem (before insert, before update, after update, after insert) {
    if(Label.isTriggerActive.equalsIgnoreCase('true')){ 
    // Only run if this trigger hasn't run before
    system.debug('hasRun: ' + ShiftQuoteLineItemTriggerSupport.hasRun); 
    if (ShiftQuoteLineItemTriggerSupport.hasRun) return;
    //ShiftQuoteLineItemTriggerSupport.hasRun = true;  
    
    
    // When you edit a quote line item you are essentially modifying a quote
    // If that quote has a pdf attached to it already then we must delete it
    // and remove the flag on the quote stating that it was approved since it
    // is now out of date.
    Set<Id> quoteIds = new Set<Id>();
    Set<Id> quoteLineItemIds = new Set<Id>();
    
    for (QuoteLineItem qli : trigger.new){  
        if (trigger.isBefore){
            
           
            qli.serviceableSite1__c = qli.serviceableSite__c; 
     
            Integer largeDiscount = 0;
            System.debug('DEBUGGER: Discount: ' + qli.Discount);
            System.debug('DEBUGGER: Large Custom Discount: ' + qli.Custom_Product_Discount_Threshold__c);
            System.debug('DEBUGGER: Large Default Discount: ' + qli.Default_Large_Discount_Threshold__c);
            System.debug('DEBUGGER: Quantity: ' + qli.Quantity);
            System.debug('DEBUGGER: Max Quantity: ' + qli.Max_Required_for_Custom_Large_Discount__c);
            System.debug('DEBUGGER: Min Quantity: ' + qli.Min_Required_for_Custom_Large_Discount__c);
            
            if (
                ( 
                    qli.Discount > qli.Default_Large_Discount_Threshold__c &&  
                    Utils.isEmpty(qli.Custom_Product_Discount_Threshold__c) 
                )|| 
                ( 
                    !(Utils.isEmpty(qli.Custom_Product_Discount_Threshold__c)) && 
                    ( 
                        ( 
                            Utils.isEmpty(qli.Max_Required_for_Custom_Large_Discount__c) && 
                            Utils.isEmpty(qli.Min_Required_for_Custom_Large_Discount__c) && 
                            qli.Discount > qli.Custom_Product_Discount_Threshold__c 
                        )|| 
                        ( 
                            qli.Discount > qli.Custom_Product_Discount_Threshold__c && 
                            !(Utils.isEmpty(qli.Max_Required_for_Custom_Large_Discount__c)) && 
                            !(Utils.isEmpty(qli.Min_Required_for_Custom_Large_Discount__c)) && 
                            qli.Quantity <= qli.Max_Required_for_Custom_Large_Discount__c && 
                            qli.Quantity >= qli.Min_Required_for_Custom_Large_Discount__c 
                        )|| 
                        ( 
                            qli.Discount > qli.Default_Large_Discount_Threshold__c && 
                            !(Utils.isEmpty(qli.Max_Required_for_Custom_Large_Discount__c)) && 
                            !(Utils.isEmpty(qli.Min_Required_for_Custom_Large_Discount__c)) && 
                            ( 
                                qli.Quantity > qli.Max_Required_for_Custom_Large_Discount__c || 
                                qli.Quantity < qli.Min_Required_for_Custom_Large_Discount__c 
                            ) 
                        )|| 
                        ( 
                            qli.Discount > qli.Default_Large_Discount_Threshold__c && 
                            !(Utils.isEmpty(qli.Max_Required_for_Custom_Large_Discount__c)) && 
                            qli.Quantity > qli.Max_Required_for_Custom_Large_Discount__c 
                        ) || 
                        ( 
                            qli.Discount > qli.Default_Large_Discount_Threshold__c && 
                            !(Utils.isEmpty(qli.Min_Required_for_Custom_Large_Discount__c)) && 
                            qli.Quantity < qli.Min_Required_for_Custom_Large_Discount__c 
                        ) || 
                        ( 
                            qli.Discount > qli.Custom_Product_Discount_Threshold__c && 
                            !(Utils.isEmpty(qli.Max_Required_for_Custom_Large_Discount__c)) && 
                            qli.Quantity < qli.Max_Required_for_Custom_Large_Discount__c && 
                            !(Utils.isEmpty(qli.Min_Required_for_Custom_Large_Discount__c))
                        ) || 
                        ( 
                            qli.Discount > qli.Custom_Product_Discount_Threshold__c && 
                            !(Utils.isEmpty(qli.Min_Required_for_Custom_Large_Discount__c)) && 
                            qli.Quantity > qli.Min_Required_for_Custom_Large_Discount__c && 
                            !(Utils.isEmpty(qli.Max_Required_for_Custom_Large_Discount__c))
                        ) 
                    ) 
                ) 
            ){
                largeDiscount = 1;
            }
            
            Integer mediumDiscount = 0;
    
            if (
                ( 
                    qli.Discount > qli.Default_Medium_Discount_Threshold__c &&  
                    Utils.isEmpty(qli.Custom_Medium_Product_Discount_Threshold__c) 
                )|| 
                ( 
                    !(Utils.isEmpty(qli.Custom_Medium_Product_Discount_Threshold__c)) && 
                    ( 
                        ( 
                            Utils.isEmpty(qli.Max_Required_for_Custom_Medium_Discount__c) && 
                            Utils.isEmpty(qli.Min_Required_for_Custom_Medium_Discount__c) && 
                            qli.Discount > qli.Custom_Medium_Product_Discount_Threshold__c/100 
                        )|| 
                        ( 
                            qli.Discount > qli.Custom_Medium_Product_Discount_Threshold__c/100 && 
                            !(Utils.isEmpty(qli.Max_Required_for_Custom_Medium_Discount__c)) && 
                            !(Utils.isEmpty(qli.Min_Required_for_Custom_Medium_Discount__c)) && 
                            qli.Quantity < qli.Max_Required_for_Custom_Medium_Discount__c && 
                            qli.Quantity > qli.Min_Required_for_Custom_Medium_Discount__c 
                        )|| 
                        ( 
                            qli.Discount > qli.Default_Medium_Discount_Threshold__c && 
                            !(Utils.isEmpty(qli.Max_Required_for_Custom_Medium_Discount__c)) && 
                            !(Utils.isEmpty(qli.Min_Required_for_Custom_Medium_Discount__c)) && 
                            ( 
                                qli.Quantity > qli.Max_Required_for_Custom_Medium_Discount__c || 
                                qli.Quantity < qli.Min_Required_for_Custom_Medium_Discount__c 
                            ) 
                        )|| 
                        ( 
                            qli.Discount > qli.Default_Medium_Discount_Threshold__c && 
                            !(Utils.isEmpty(qli.Max_Required_for_Custom_Medium_Discount__c)) && 
                            qli.Quantity >= qli.Max_Required_for_Custom_Medium_Discount__c 
                        ) || 
                        ( 
                            qli.Discount > qli.Default_Medium_Discount_Threshold__c && 
                            !(Utils.isEmpty(qli.Min_Required_for_Custom_Medium_Discount__c)) && 
                            qli.Quantity <= qli.Min_Required_for_Custom_Medium_Discount__c 
                        ) || 
                        ( 
                            qli.Discount > qli.Custom_Medium_Product_Discount_Threshold__c/100 && 
                            !(Utils.isEmpty(qli.Max_Required_for_Custom_Medium_Discount__c)) && 
                            qli.Quantity < qli.Max_Required_for_Custom_Medium_Discount__c  && 
                            !(Utils.isEmpty(qli.Max_Required_for_Custom_Medium_Discount__c))
                        ) || 
                        ( 
                            qli.Discount > qli.Custom_Medium_Product_Discount_Threshold__c/100 && 
                            !(Utils.isEmpty(qli.Min_Required_for_Custom_Medium_Discount__c)) && 
                            qli.Quantity > qli.Min_Required_for_Custom_Medium_Discount__c  && 
                            !(Utils.isEmpty(qli.Min_Required_for_Custom_Medium_Discount__c))
                        ) 
                    ) 
                ) 
            ){
                mediumDiscount = 1;
            }
    
    
    
            if (qli.Charge_Type__c != null && qli.Charge_Type__c.equals('MRC') && largeDiscount>0){
                qli.isLargeDiscount__c = 1;
            }else{
                qli.isLargeDiscount__c = 0;
            }
            
            if (qli.Charge_Type__c != null && qli.Charge_Type__c.equals('MRC') && mediumDiscount>0 && qli.isLargeDiscount__c < 1){
                qli.isMediumDiscount__c = 1;
            }else{
                qli.isMediumDiscount__c = 0;
            }
            
            if (qli.Charge_Type__c != null && qli.Charge_Type__c.equals('MRC') && qli.Small_Discount__c>0 && largeDiscount < 1&& mediumDiscount < 1){
                qli.isSmallDiscount__c = 1;
            }else{
                qli.isSmallDiscount__c = 0;
            }
            
            
            if (qli.Charge_Type__c != null && qli.Charge_Type__c.equals('NRC') && qli.Discount>0){
                qli.isNRRDiscount__c = 1;
            }else{
                qli.isNRRDiscount__c = 0;
            }
            
             if (qli.Charge_Type__c != null && qli.Charge_Type__c.equals('PMO')){
                qli.isOverage__c = 1;
            }else{
                qli.isOverage__c = 0;
            }
            
            
        }
        quoteLineItemIds.add(qli.Id);
        quoteIds.add(qli.quoteid);
        System.Debug(qli.quoteid);
    }
    
    if (Trigger.isInsert && Trigger.isBefore){  
        Set<Id> quoteIdsInsert = new Set<Id>();
        for (QuoteLineItem qli : Trigger.New){
            quoteIdsInsert.add(qli.quoteId);
        }
        
        Map<Id, Quote> quoteMap = new Map<Id, Quote>([SELECT id, Original_Quote__c, Opportunity_Type__c, Activate_Renewal_New_Date__c FROM Quote WHERE id IN :quoteIds]);

        for (QuoteLineItem qli : Trigger.New){
            if (quoteMap.get(qli.quoteid).Original_Quote__c == null && quoteMap.get(qli.quoteid).Activate_Renewal_New_Date__c <= Date.today()){
                if (quoteMap.get(qli.quoteid).Opportunity_Type__c!=null) { //RYAN ADDED
                    qli.Type__c = (quoteMap.get(qli.quoteid).Opportunity_Type__c).contains('Renewal')?'R':(quoteMap.get(qli.quoteid).Opportunity_Type__c).contains('Upsell')?'U':'N';
                }
            }
        }
    }
        
    if (!trigger.isAfter){
        List<QuoteDocument> quoteDocuments = [SELECT Id, quoteId FROM QuoteDocument where quoteId IN :quoteIds];
        DELETE quoteDocuments;
    }
    }
}