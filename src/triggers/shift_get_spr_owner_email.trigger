trigger shift_get_spr_owner_email on SPR_Note__c (before insert, before update) {
   if(Label.isTriggerActive.equalsIgnoreCase('true')){ 
    /*
    Trigger to capture the SPR owner email on the SPR Note.
    Developed by Jocsan Diaz on July 30th, 2012
    Modified by Jocsan Diaz on November 2nd, 2012
    */
    
    // get SPR's Ids
    set<Id> theSPRIdSet = new set<Id>();
    for (SPR_Note__c s : trigger.new) {
        theSPRIdSet.add(s.SPR__c);
    }
    
    // query all active users
    map<Id, User> theUserMap = new map<Id, User>([select Id, Name, Email from User where isActive = true]);
    
    // query all queues associated to SPR object
    map<Id, QueueSobject> theQueueEmailMap = new map<Id, QueueSobject>();
    list<QueueSobject> theQueueSobjectList = new list<QueueSobject>([select q.SobjectType, q.Queue.Email, q.Queue.Name, q.Queue.Type, q.Queue.Id, q.QueueId, q.Id 
                                                                        from QueueSobject q 
                                                                            where q.SobjectType =: 'SPR__c']);
    for (QueueSobject q : theQueueSobjectList) {
        theQueueEmailMap.put(q.Queue.Id, q);
    }
    
    // query SPR notes in order to get the SPR Owner
    map<Id, SPR__c> theSPRMap = new map<Id, SPR__c>([select  s.Id, s.Requested_By__c, s.Global_Recipient_5__c, s.Global_Recipient_4__c, 
                                                                s.Global_Recipient_3__c, s.Global_Recipient_2__c, s.Global_Recipient_1__c 
                                                        from SPR__c s where Id in: theSPRIdSet]);
    
    // update owner email field with user's or queue's email
    for (SPR_Note__c s : trigger.new) {
        // if no SPR set then continue
        if (s.SPR__c == null) continue;
        
        // copy other emails over to the SPT note
        if (theUserMap.containsKey(theSPRMap.get(s.SPR__c).Requested_By__c)) // Requested_By__c
            s.SPR_Requestor_Email__c = theUserMap.get(theSPRMap.get(s.SPR__c).Requested_By__c).Email;          
        if (theUserMap.containsKey(theSPRMap.get(s.SPR__c).Global_Recipient_1__c)) // Global_Recipient_1__c
            s.Global_Recipient_1_Email__c = theUserMap.get(theSPRMap.get(s.SPR__c).Global_Recipient_1__c).Email;        
        if (theUserMap.containsKey(theSPRMap.get(s.SPR__c).Global_Recipient_2__c)) // Global_Recipient_2__c
            s.Global_Recipient_2_Email__c = theUserMap.get(theSPRMap.get(s.SPR__c).Global_Recipient_2__c).Email; 
        if (theUserMap.containsKey(theSPRMap.get(s.SPR__c).Global_Recipient_3__c)) // Global_Recipient_3__c
            s.Global_Recipient_3_Email__c = theUserMap.get(theSPRMap.get(s.SPR__c).Global_Recipient_3__c).Email; 
        if (theUserMap.containsKey(theSPRMap.get(s.SPR__c).Global_Recipient_4__c)) // Global_Recipient_4__c
            s.Global_Recipient_4_Email__c = theUserMap.get(theSPRMap.get(s.SPR__c).Global_Recipient_4__c).Email;           
        if (theUserMap.containsKey(theSPRMap.get(s.SPR__c).Global_Recipient_5__c)) // Global_Recipient_5__c 
            s.Global_Recipient_5_Email__c = theUserMap.get(theSPRMap.get(s.SPR__c).Global_Recipient_5__c).Email;                            
        
        /* Commenting out section below to proceed with deployment. Owner does not exist for SPR// check SPR owner and set email accordingly
        if (theUserMap.containsKey(theSPRMap.get(s.SPR__c).OwnerId)) { // for user
            s.SPR_Owner_Email__c = theUserMap.get(theSPRMap.get(s.SPR__c).OwnerId).Email;
        } else if (theQueueEmailMap.containsKey(theSPRMap.get(s.SPR__c).OwnerId)) { // for queue
            s.SPR_Owner_Email__c = theQueueEmailMap.get(theSPRMap.get(s.SPR__c).OwnerId).Queue.Email;
        }    */
    }
  }  
}