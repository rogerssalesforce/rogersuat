<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_New_User</fullName>
        <description>Email to New User</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Various_Templates/New_User_Created</template>
    </alerts>
    <fieldUpdates>
        <fullName>Populate_Rogers_Emp_ID</fullName>
        <field>Employee_Id__c</field>
        <formula>Dealer_Rep_ID__c</formula>
        <name>Populate Rogers Emp ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Dealer ID to blank Employee ID</fullName>
        <actions>
            <name>Populate_Rogers_Emp_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Employee_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <description>Copy the Dealer Auto ID to Employee ID (if blank)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>User Creation Notification</fullName>
        <actions>
            <name>Email_to_New_User</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Standard,CSN Only</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
