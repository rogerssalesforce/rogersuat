<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>This_Email_Alert_is_used_to_inform_SOC_that_a_Quote_has_been_modified_and_as_a_r</fullName>
        <description>This Email Alert is used to inform SOC that a Quote has been modified and as a result the COP has been updated.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/COP_Updated</template>
    </alerts>
    <rules>
        <fullName>COP has been Updated</fullName>
        <actions>
            <name>This_Email_Alert_is_used_to_inform_SOC_that_a_Quote_has_been_modified_and_as_a_r</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CIF__c.UpdatedCOP__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>As a result of a Quote Changing - the COP has been updated.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
