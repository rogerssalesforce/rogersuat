<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Sales_Empowerment_Rejected</fullName>
        <description>Sales Empowerment Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Various_Templates/Sales_Empowerment_Request_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Sales_Empowerment_Request_Approved</fullName>
        <description>Sales Empowerment Request Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Various_Templates/Sales_Empowerment_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Start_Contract</fullName>
        <description>Start Contract</description>
        <protected>false</protected>
        <recipients>
            <field>Contracts_Prime__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Various_Templates/PER_Email_to_Start_Contract</template>
    </alerts>
    <alerts>
        <fullName>Terminate_Contract</fullName>
        <description>Terminate Contract</description>
        <protected>false</protected>
        <recipients>
            <field>Contracts_Prime__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Various_Templates/PER_Email_to_Terminate_Contract</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved_Status</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approved Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PER_Last_Approval_Date_Time_Stamp</fullName>
        <field>Date_Time_of_Last_Approval__c</field>
        <formula>NOW()</formula>
        <name>PER - Last Approval Date/Time Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PER_Previous_Approver</fullName>
        <field>Previous_Approver__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>PER - Previous Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PER_Submitter</fullName>
        <field>Previous_Approver__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>PER - Submitter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_to_Approved_PER</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Approved_Pricing_Enablement_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type to Approved PER</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_to_In_Approval_PER</fullName>
        <field>RecordTypeId</field>
        <lookupValue>In_Approval_Pricing_Enablement_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type to In-Approval PER</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_to_Unapproved_PER</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Unapproved_Pricing_Enablement_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type to Unapproved PER</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_Status</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Rejected Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Shared_Data</fullName>
        <field>Monthly_Recurring_Add_Ons_pre_tax_WF__c</field>
        <formula>Monthly_Recurring_Add_Ons_pre_tax__c+
Monthly_Recurring_Intl_Shared_Add_Ons__c +  
Monthly_Recurring_US_Shared_Add_Ons__c</formula>
        <name>Shared Data</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_for_Approval_Status</fullName>
        <description>Update the status of PER to Submitted for Approval</description>
        <field>Status__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Submitted for Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Int_Roaming</fullName>
        <field>Int_Roaming_Calc__c</field>
        <formula>Int_Roaming_Add_Ons__c</formula>
        <name>Update Int Roaming</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Monthly_Recurring_Add_Ons</fullName>
        <description>Updates the Monthly Recurring Add-Ons (pre-tax) custom field on Pricing Enablement Request custom object</description>
        <field>Monthly_Recurring_Add_Ons_pre_tax_WF__c</field>
        <formula>Int_Roaming_Calc__c+ 
Non_Roaming_Calc__c+ 
US_Roaming_Calc__c+ 
Zone_2_Calc__c+ 
Zone_3_Caribbean_Calc__c+ 
Zone_3_Major_Eastern_Add_Ons__c+ 
Zone_3_Other_Calc__c+ 
Zone_4_Africa_Calc__c+ 
Zone_4_Egypt_Calc__c+
Monthly_Recurring_Intl_Shared_Add_Ons__c +  
Monthly_Recurring_US_Shared_Add_Ons__c</formula>
        <name>Update Monthly Recurring Add-Ons</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Non_Roaming</fullName>
        <field>Non_Roaming_Calc__c</field>
        <formula>Non_Roaming_Add_Ons__c</formula>
        <name>Update Non Roaming</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Zone_2</fullName>
        <field>Zone_2_Calc__c</field>
        <formula>Zone_2_Add_Ons__c</formula>
        <name>Update Zone 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Zone_3_C</fullName>
        <field>Zone_3_Caribbean_Calc__c</field>
        <formula>Zone_3_Caribbean_Add_Ons__c</formula>
        <name>Update Zone 3 - C</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Zone_3_M</fullName>
        <field>Zone_3_Major__c</field>
        <formula>Zone_3_Major_Eastern_Add_Ons__c</formula>
        <name>Update Zone 3 - M</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Zone_3_O</fullName>
        <field>Zone_3_Other_Calc__c</field>
        <formula>Zone_3_Other_Add_Ons__c</formula>
        <name>Update Zone 3 - O</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Zone_4_A</fullName>
        <field>Zone_4_Africa_Calc__c</field>
        <formula>Zone_4_Africa_Add_Ons__c</formula>
        <name>Update Zone 4 - A</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Zone_4_E</fullName>
        <field>Zone_4_Egypt_Calc__c</field>
        <formula>Zone_4_Egypt_South_Africa_Add_Ons__c</formula>
        <name>Update Zone 4 - E</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_us_Roaming</fullName>
        <field>US_Roaming_Calc__c</field>
        <formula>US_Roaming_Add_Ons__c</formula>
        <name>Update us Roaming</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Sales Empowerment Request is edited</fullName>
        <actions>
            <name>Update_Monthly_Recurring_Add_Ons</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Pricing_Enablement_Request__c.LastModifiedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Triggers every time a record is modified</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Total Calculator</fullName>
        <actions>
            <name>Shared_Data</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Int_Roaming</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Non_Roaming</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Zone_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Zone_3_C</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Zone_3_M</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Zone_3_O</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Zone_4_A</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Zone_4_E</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_us_Roaming</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Pricing_Enablement_Request__c.LastModifiedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
