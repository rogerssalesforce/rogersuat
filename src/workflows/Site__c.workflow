<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Prospect_Site_Approval_Notification</fullName>
        <description>Prospect Site Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Prospect_Site_Approval_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Site_Approved</fullName>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Update Site Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
