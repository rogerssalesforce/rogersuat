<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Out</fullName>
        <description>User added to Opportunity Team</description>
        <protected>false</protected>
        <recipients>
            <field>UserId</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Various_Templates/User_added_to_Opportunity_Team</template>
    </alerts>
    <rules>
        <fullName>Opp Team member alert</fullName>
        <actions>
            <name>Email_Out</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityTeamMember.UserId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Notifies a user they have been added to an Opportunity Team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
