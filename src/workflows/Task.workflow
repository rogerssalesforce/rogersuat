<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Already_have_card_payment_solution</fullName>
        <description>Already have card payment solution</description>
        <field>Disposition__c</field>
        <literalValue>Already have card payment solution</literalValue>
        <name>Already have card payment solution</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Attended</fullName>
        <description>Field Status Change copy Subject to Disposition</description>
        <field>Disposition__c</field>
        <literalValue>Attended</literalValue>
        <name>Attended</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Booked_Meeting</fullName>
        <description>Field Status Change copy Subject to Disposition</description>
        <field>Disposition__c</field>
        <literalValue>Booked Meeting</literalValue>
        <name>Booked Meeting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_3rd_Call</fullName>
        <field>Disposition__c</field>
        <literalValue>3rd+ attempt</literalValue>
        <name>Call 3rd+ Call</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Activity_assigned_to_wrong_contact</fullName>
        <field>Disposition__c</field>
        <literalValue>Activity assigned to wrong contact</literalValue>
        <name>Call Activity assigned to wrong contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Activity_assigned_to_wrong_rep</fullName>
        <field>Disposition__c</field>
        <literalValue>Activity assigned to wrong rep</literalValue>
        <name>Call Activity assigned to wrong rep</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Already_in_Renewal_Process</fullName>
        <field>Disposition__c</field>
        <literalValue>Already in Renewal Process</literalValue>
        <name>Call Already in Renewal Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Can_t_reach_Decision_Maker</fullName>
        <field>Disposition__c</field>
        <literalValue>Can&apos;t reach Decision Maker</literalValue>
        <name>Call Can&apos;t reach Decision Maker</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Company_Out_of_Business_Non_Existen</fullName>
        <field>Disposition__c</field>
        <literalValue>Company Out of Business/Non-Existent</literalValue>
        <name>Call Company Out of Business/Non-Existen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Contact_Emailed</fullName>
        <field>Disposition__c</field>
        <literalValue>Contact Emailed</literalValue>
        <name>Call Contact Emailed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Contact_Responded_to_Email</fullName>
        <field>Disposition__c</field>
        <literalValue>Contact Responded to Email</literalValue>
        <name>Call Contact Responded to Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Credit_Issues_Collections</fullName>
        <field>Disposition__c</field>
        <literalValue>Credit Issues/Collections</literalValue>
        <name>Call Credit Issues/Collections</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Needs_Analysis</fullName>
        <field>Disposition__c</field>
        <literalValue>Needs Analysis</literalValue>
        <name>Call Needs Analysis</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_No_Opportunity</fullName>
        <field>Disposition__c</field>
        <literalValue>No Opportunity</literalValue>
        <name>Call No Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_No_Opportunity_Bad_experience</fullName>
        <field>Disposition__c</field>
        <literalValue>No Opportunity (Unwilling to talk /Bad Experience with Rogers)</literalValue>
        <name>Call No Opportunity (Bad experience)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_No_Opportunity_Coverage_Issues</fullName>
        <field>Disposition__c</field>
        <literalValue>No Opportunity (Coverage Issues)</literalValue>
        <name>Call No Opportunity (Coverage Issues)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_No_Opportunity_Gone_to_competition</fullName>
        <field>Disposition__c</field>
        <literalValue>No Opportunity (Gone to competition)</literalValue>
        <name>Call No Opportunity (Gone to competition</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_No_Opportunity_Pricing_Cost_Cuttin</fullName>
        <field>Disposition__c</field>
        <literalValue>No Opportunity (Pricing/Cost Cutting)</literalValue>
        <name>Call No Opportunity (Pricing/Cost Cuttin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_No_Opportunity_Product_Offer_Lacki</fullName>
        <field>Disposition__c</field>
        <literalValue>No Opportunity (Product Offer Lacking – ie. BYOD, Industry Type, etc.)</literalValue>
        <name>Call No Opportunity (Product Offer Lacki</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_No_Opportunity_Recently_re_signed</fullName>
        <field>Disposition__c</field>
        <literalValue>Call No Opportunity (Recently re-signed)</literalValue>
        <name>Call No Opportunity (Recently re-signed)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_No_Opportunity_with_Rogers_No_Grow</fullName>
        <field>Disposition__c</field>
        <literalValue>No Opportunity (with Rogers/No Growth)</literalValue>
        <name>Call No Opportunity (with Rogers/No Grow</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Onboarding_not_completed</fullName>
        <field>Disposition__c</field>
        <literalValue>Onboarding not completed</literalValue>
        <name>Call Onboarding not completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Opportunity_Created</fullName>
        <field>Disposition__c</field>
        <literalValue>Opportunity Created</literalValue>
        <name>Call Opportunity Created</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Opportunity_Lost</fullName>
        <field>Disposition__c</field>
        <literalValue>Closed Opportunity as Lost</literalValue>
        <name>Call Opportunity Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Opportunity_Won</fullName>
        <field>Disposition__c</field>
        <literalValue>Closed Opportunity as Won</literalValue>
        <name>Call Opportunity Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Opportunity_created_in_previous_cam</fullName>
        <field>Disposition__c</field>
        <literalValue>Opportunity created in previous campaign</literalValue>
        <name>Call Opportunity created in previous cam</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Other</fullName>
        <field>Disposition__c</field>
        <literalValue>Other</literalValue>
        <name>Call Other</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Out_of_Scope_Employee_size_Ownersh</fullName>
        <field>Disposition__c</field>
        <literalValue>Out of Scope (employee size / ownership)</literalValue>
        <name>Call Out of Scope (Employee size/Ownersh</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Proposal_Review</fullName>
        <field>Disposition__c</field>
        <literalValue>Proposal Review</literalValue>
        <name>Call Proposal Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Call_Wants_one_bill</fullName>
        <field>Disposition__c</field>
        <literalValue>Wants one Bill</literalValue>
        <name>Call Wants one bill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_2nd_attempt_Subject_to_Disposition</fullName>
        <field>Disposition__c</field>
        <literalValue>2nd attempt</literalValue>
        <name>Copy 2nd attempt Subject to Disposition</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Final_Email_Sent_Subject_to_Disposi</fullName>
        <field>Disposition__c</field>
        <literalValue>Final Email Sent</literalValue>
        <name>Copy Final Email Sent Subject to Disposi</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_New_Subject_to_Disposition</fullName>
        <field>Disposition__c</field>
        <literalValue>New</literalValue>
        <name>Copy New Subject to Disposition</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Previous_Lost_to_Disposition</fullName>
        <field>Disposition__c</field>
        <literalValue>Previous Opportunity Lost</literalValue>
        <name>Copy Previous Lost Opport to Disposition</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Previous_Opp_Won_to_Disposition</fullName>
        <field>Disposition__c</field>
        <literalValue>Previous Opportunity Won</literalValue>
        <name>Copy Previous Opp Won to Disposition</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Customer_Assessment_Complete</fullName>
        <description>Field Status Change copy Subject to Disposition</description>
        <field>Disposition__c</field>
        <literalValue>Customer Assessment Complete</literalValue>
        <name>Customer Assessment Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Customer_no_longer_with_Rogers</fullName>
        <description>Field Status Change copy Subject to Disposition</description>
        <field>Disposition__c</field>
        <literalValue>Customer no longer with Rogers</literalValue>
        <name>Customer no longer with Rogers</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Line_commitment_info_is_incorrect</fullName>
        <description>Field Status Change copy Subject to Disposition</description>
        <field>Disposition__c</field>
        <literalValue>Line commitment info is incorrect</literalValue>
        <name>Line commitment info is incorrect</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Loads_or_pricing_increase_completed</fullName>
        <description>Field Status Change copy Subject to Disposition</description>
        <field>Disposition__c</field>
        <literalValue>Loads or pricing increase completed</literalValue>
        <name>Loads or pricing increase completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Moved_address_In_Footprint</fullName>
        <description>Field Status Change copy Subject to Disposition</description>
        <field>Disposition__c</field>
        <literalValue>Moved address - In Footprint</literalValue>
        <name>Moved address - In Footprint</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Moved_address_Not_in_Footprint</fullName>
        <field>Disposition__c</field>
        <literalValue>Moved address - Not in Footprint</literalValue>
        <name>Moved address - Not in Footprint</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Not_Interested</fullName>
        <description>Field Status Change copy Subject to Disposition</description>
        <field>Disposition__c</field>
        <literalValue>Not Interested</literalValue>
        <name>Not Interested</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_with_Rogers_future_Growth</fullName>
        <field>Disposition__c</field>
        <literalValue>Opportunity (with Rogers/future Growth)</literalValue>
        <name>Opportunity (with Rogers/future Growth)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RS4B_Lead_Sent</fullName>
        <description>Field Status Change copy Subject to Disposition</description>
        <field>Disposition__c</field>
        <literalValue>RS4B Lead Sent</literalValue>
        <name>RS4B Lead Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Registered</fullName>
        <field>Disposition__c</field>
        <literalValue>Registered</literalValue>
        <name>Registered</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Disposition_to_Lead_Previously_Se</fullName>
        <field>Disposition__c</field>
        <literalValue>Lead Previously Sent</literalValue>
        <name>Update Disposition to Lead Previously Se</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Disposition_to_Onboarding_Complet</fullName>
        <field>Disposition__c</field>
        <literalValue>Onboarding Completed</literalValue>
        <name>Update Disposition to Onboarding Complet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X1st_Call</fullName>
        <field>Disposition__c</field>
        <literalValue>1st attempt</literalValue>
        <name>1st Attempt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Already have card payment solution</fullName>
        <actions>
            <name>Already_have_card_payment_solution</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Already have card</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Attended</fullName>
        <actions>
            <name>Attended</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Attended</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Booked Meeting</fullName>
        <actions>
            <name>Booked_Meeting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Booked Meeting</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call 1st Attempt Subject to Disposition</fullName>
        <actions>
            <name>X1st_Call</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call 1st attempt</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call 2nd attempt Subject to Disposition</fullName>
        <actions>
            <name>Copy_2nd_attempt_Subject_to_Disposition</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call 2nd attempt</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call 3rd%2B attempt Subject to Disposition</fullName>
        <actions>
            <name>Call_3rd_Call</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call 3rd+ attempt</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Activity assigned to wrong contact</fullName>
        <actions>
            <name>Call_Activity_assigned_to_wrong_contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Activity assigned to wrong contact</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Activity assigned to wrong rep</fullName>
        <actions>
            <name>Call_Activity_assigned_to_wrong_rep</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Activity assigned to wrong rep</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Already in Renewal Process</fullName>
        <actions>
            <name>Call_Already_in_Renewal_Process</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Already in Renewal Process</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Can%27t reach Decision Maker</fullName>
        <actions>
            <name>Call_Can_t_reach_Decision_Maker</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Can&apos;t reach Decision Maker</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Company Out of Business%2FNon-Existent Subject to Disposition</fullName>
        <actions>
            <name>Call_Company_Out_of_Business_Non_Existen</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Company Out of Business/Non-Existent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Contact Emailed</fullName>
        <actions>
            <name>Call_Contact_Emailed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Contact Emailed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Contact Responded to Email</fullName>
        <actions>
            <name>Call_Contact_Responded_to_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Contact Responded to Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Credit Issues%2FCollections</fullName>
        <actions>
            <name>Call_Credit_Issues_Collections</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Credit Issues/Collections</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Final Email Sent Subject to Disposition</fullName>
        <actions>
            <name>Copy_Final_Email_Sent_Subject_to_Disposi</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Final Email Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Lead Previously Sent Subject to Disposition</fullName>
        <actions>
            <name>Update_Disposition_to_Lead_Previously_Se</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Lead Previously Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Needs Analysis</fullName>
        <actions>
            <name>Call_Needs_Analysis</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Needs Analysis</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call No Opportunity</fullName>
        <actions>
            <name>Call_No_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call No Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call No Opportunity %28Bad experience%29</fullName>
        <actions>
            <name>Call_No_Opportunity_Bad_experience</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call No Opportunity (Bad experience)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call No Opportunity %28Coverage Issues%29</fullName>
        <actions>
            <name>Call_No_Opportunity_Coverage_Issues</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call No Opportunity (Coverage Issues)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call No Opportunity %28Gone to competition%29</fullName>
        <actions>
            <name>Call_No_Opportunity_Gone_to_competition</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call No Opportunity (Gone to competition)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call No Opportunity %28Pricing%2FCost Cutting%29</fullName>
        <actions>
            <name>Call_No_Opportunity_Pricing_Cost_Cuttin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call No Opportunity (Pricing/Cost Cutting)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call No Opportunity %28Product Offer Lacking%29</fullName>
        <actions>
            <name>Call_No_Opportunity_Product_Offer_Lacki</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call No Opportunity (Product Offer Lacking)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call No Opportunity %28Recently re-signed%29</fullName>
        <actions>
            <name>Call_No_Opportunity_Recently_re_signed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call No Opportunity (Recently re-signed)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call No Opportunity %28with Rogers%2FNo Growth%29</fullName>
        <actions>
            <name>Call_No_Opportunity_with_Rogers_No_Grow</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call No Opportunity (with Rogers/No Growth)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Onboarding Completed Subject to Disposition</fullName>
        <actions>
            <name>Update_Disposition_to_Onboarding_Complet</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Copy Call Onboarding Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Onboarding not completed</fullName>
        <actions>
            <name>Call_Onboarding_not_completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Onboarding not completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Opportunity Created</fullName>
        <actions>
            <name>Call_Opportunity_Created</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Opportunity Created</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Opportunity Lost</fullName>
        <actions>
            <name>Call_Opportunity_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Opportunity Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Opportunity Won</fullName>
        <actions>
            <name>Call_Opportunity_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Opportunity Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Opportunity created in previous campaign Subject to Disposition</fullName>
        <actions>
            <name>Call_Opportunity_created_in_previous_cam</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Opportunity created in previous campaign</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Other</fullName>
        <actions>
            <name>Call_Other</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Out of Scope %28Employee size%2FOwnership%29</fullName>
        <actions>
            <name>Call_Out_of_Scope_Employee_size_Ownersh</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Out of Scope (Employee size/Ownership)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Previous Opportunity Lost Subject to Disposition</fullName>
        <actions>
            <name>Copy_Previous_Lost_to_Disposition</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Previous Opportunity Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Previous Opportunity Won Subject to Disposition</fullName>
        <actions>
            <name>Copy_Previous_Opp_Won_to_Disposition</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Previous Opportunity Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Proposal Review</fullName>
        <actions>
            <name>Call_Proposal_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Proposal Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Call Wants one bill</fullName>
        <actions>
            <name>Call_Wants_one_bill</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Wants one bill</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy New Subject to Disposition</fullName>
        <actions>
            <name>Copy_New_Subject_to_Disposition</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition,</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Opportunity %28with Rogers%2Ffuture Growth%29</fullName>
        <actions>
            <name>Opportunity_with_Rogers_future_Growth</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Opportunity (with Rogers/future Growth)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Customer Assessment Complete</fullName>
        <actions>
            <name>Customer_Assessment_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Customer Assessment Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Customer no longer with Rogers</fullName>
        <actions>
            <name>Customer_no_longer_with_Rogers</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Customer no longer with Rogers</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Line commitment info is incorrect</fullName>
        <actions>
            <name>Line_commitment_info_is_incorrect</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Line commitment info is incorrect</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Loads or pricing increase completed</fullName>
        <actions>
            <name>Loads_or_pricing_increase_completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Loads or pricing increase completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Moved address - In Footprint</fullName>
        <actions>
            <name>Moved_address_In_Footprint</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Moved address - In Footprint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Moved address - Not in Footprint</fullName>
        <actions>
            <name>Moved_address_Not_in_Footprint</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Moved address - Not in Footprint</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Not Interested</fullName>
        <actions>
            <name>Not_Interested</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Not Interested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RS4B Lead Sent</fullName>
        <actions>
            <name>RS4B_Lead_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call RS4B Lead Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Registered</fullName>
        <actions>
            <name>Registered</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call Registered</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Disposition__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Field Status Change copy Subject to Disposition</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
