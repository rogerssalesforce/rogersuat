<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Default_Total_Units</fullName>
        <description>Default Total Units to 1</description>
        <field>Quantity</field>
        <formula>1</formula>
        <name>Default Total Units</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Price_Update</fullName>
        <description>Sum of Revenues</description>
        <field>UnitPrice</field>
        <formula>(Revenue_Deployment_Renewal__c  +  Revenue_Monthly_or_One_Time__c  +  Revenue_Resellers__c)</formula>
        <name>Sales Price Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Access_Type</fullName>
        <description>update access type</description>
        <field>Access_Type1__c</field>
        <formula>Access_Type__c</formula>
        <name>Update Access Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Category</fullName>
        <description>copy the formula field Category to the text field Category</description>
        <field>Category1__c</field>
        <formula>Category__c</formula>
        <name>Update Category</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Charge_Type</fullName>
        <description>update the value from the formula field to text field.</description>
        <field>Charge_Type__c</field>
        <formula>Charge_Type1__c</formula>
        <name>Update Charge Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Product_Code</fullName>
        <description>Update the product code text field with the product code formula field that pulls information from product catalogue</description>
        <field>Product_Code1__c</field>
        <formula>Product_Code__c</formula>
        <name>Update Product Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Service_Type</fullName>
        <field>Service_Type__c</field>
        <formula>Service_Type__c</formula>
        <name>Update Service Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Sync Fields in Opp Prod</fullName>
        <actions>
            <name>Update_Access_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Category</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Charge_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Product_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Service_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Quantity</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <description>Sync all fields from formula fields to text fields in opportunity product section in order to be displayed into the opportunity product line item related list</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
