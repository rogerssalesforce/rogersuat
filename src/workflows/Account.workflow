<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Account_Owner_when_Account_Status_becomes_Unassigned</fullName>
        <description>Notify Account Owner when Account Status becomes Unassigned</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <fieldUpdates>
        <fullName>Auto_populate_Pricebook_with_Enterprise</fullName>
        <field>Pricebook__c</field>
        <literalValue>Enterprise Price book</literalValue>
        <name>Auto-populate Pricebook with &quot;Enterprise</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_populate_Pricebook_with_Interco</fullName>
        <field>Pricebook__c</field>
        <literalValue>Interco Price Book</literalValue>
        <name>Auto-populate Pricebook with &quot;Interco</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_ABS_Revenue</fullName>
        <description>Clear ABS Revenue when Restricted Account</description>
        <field>ABS_Revenue__c</field>
        <name>Clear ABS Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Annual_Company_Revenue</fullName>
        <description>Clear Annual Company Revenue when Restricted Account</description>
        <field>AnnualRevenue</field>
        <name>Clear Annual Company Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Credit_Limit</fullName>
        <description>Clear Credit Limit when Restricted Account</description>
        <field>Credit_Limit__c</field>
        <name>Clear Credit Limit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Postal_Code_Billing</fullName>
        <description>Clear Postal Code Billing when Restricted Account</description>
        <field>BillingPostalCode</field>
        <name>Clear Postal Code Billing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Postal_Code_Shipping</fullName>
        <description>Clear Postal Code Shipping</description>
        <field>ShippingPostalCode</field>
        <name>Clear Postal Code Shipping</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_type_change_to_Inactive_Account</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Inactive_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record type change to Inactive Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_type_change_to_Unassigned_Account</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Submitted_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record type change to Unassigned Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Entity_Code_Cable_Interne</fullName>
        <field>Billing_Entity_Code__c</field>
        <formula>&quot;CC33&quot;</formula>
        <name>Update Billing Entity Code Cable Interne</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Entity_Code_EoC</fullName>
        <description>Update the billing entity code with the default value</description>
        <field>Billing_Entity_Code_EoC__c</field>
        <formula>&quot;COR1&quot;</formula>
        <name>Update Billing Entity Code EoC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Submitted</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Submitted_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change Account Record Type for Inactive or Invalid Account</fullName>
        <actions>
            <name>Record_type_change_to_Inactive_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Invalid</value>
        </criteriaItems>
        <description>Change to Inactive Account record type when Account Status changed to Inactive or Invalid.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Account Record Type for Pending Account</fullName>
        <actions>
            <name>Update_Record_Type_to_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <description>Change the record type of the account once the status is changed to Pending from New</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Account Record Type for Unassigned</fullName>
        <actions>
            <name>Record_type_change_to_Unassigned_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Unassigned</value>
        </criteriaItems>
        <description>Change to Unassigned Account record type when Account Status changed to Unassigned .</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default Billing Entity</fullName>
        <actions>
            <name>Update_Billing_Entity_Code_Cable_Interne</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Billing_Entity_Code_EoC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Owner__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Assign automatically the billing entity number to any new account.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Pricebook - auto populate with %22Interco%22</fullName>
        <actions>
            <name>Auto_populate_Pricebook_with_Interco</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Interco_Account__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Auto-populates “Pricebook” field with “Interco Pricebook” if it is an interco account.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pricebook - default to %22Enterprise%22</fullName>
        <actions>
            <name>Auto_populate_Pricebook_with_Enterprise</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Interco_Account__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Carrier Account,Dealer Account</value>
        </criteriaItems>
        <description>Defaults “Pricebook” field to “Enterprise Price book” for non-Interco &amp; non-Carrier Accounts</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Restricted Account Fields</fullName>
        <actions>
            <name>Clear_ABS_Revenue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Annual_Company_Revenue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Credit_Limit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Postal_Code_Billing</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Postal_Code_Shipping</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Restricted__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Specific Account Fields need to be zeroed out for Restricted Account</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
