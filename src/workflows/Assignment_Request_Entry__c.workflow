<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Unique_Value_update</fullName>
        <field>Duplicate_Value__c</field>
        <formula>if( TEXT(Assignment_Request__r.Request_Type__c) ==&apos;Mixed&apos;, Account__c + MSD__c  + Shared_MSD__c + Assignment_Request__c,if( TEXT(Assignment_Request__r.Request_Type__c) ==&apos;Owner&apos;, Account__c + MSD__c  + Shared_MSD__c + Assignment_Request__c, if(TEXT(Assignment_Request__r.Request_Type__c) == &apos;District&apos;, Account__c + MSD__c  + Shared_MSD__c + Assignment_Request__c,if(TEXT(Assignment_Request__r.Request_Type__c) == &apos;MSD Member&apos;, MSD__c + Assignment_Request__c + TEXT(Action__c) + User__c + Territory_City__c  + Territory_NPA__c + Territory_Region__c ,if(TEXT(Assignment_Request__r.Request_Type__c) == &apos;Shared MSD Member&apos;, Shared_MSD__c + Assignment_Request__c + TEXT(Action__c) +User__c + Territory_City__c  + Territory_NPA__c + Territory_Region__c, if(TEXT(Assignment_Request__r.Request_Type__c) == &apos;Account Team&apos;, Account__c + Assignment_Request__c + TEXT(Action__c) +User__c, null ))))))</formula>
        <name>Unique Value update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Unique value generate for AR Entry</fullName>
        <actions>
            <name>Unique_Value_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
