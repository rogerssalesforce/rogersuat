<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Effective_Date_set_for_Non_Reason_Code_5</fullName>
        <description>Effective Date based on Rep Resignation chosen as the reason code.</description>
        <field>Effective_Date__c</field>
        <formula>IF(
NOT(ISPICKVAL
(Reason_Code__c , &quot;5 - Rep Turnover - LOA/Resignation&quot;)),
 NULL, 
DATE(2016,01,01)
 )</formula>
        <name>Effective Date set for Non Reason Code 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Last_Approval_Date_Time_Stamp</fullName>
        <field>Date_Time_of_Last_Approval__c</field>
        <formula>NOW()</formula>
        <name>Last Approval Date/Time Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Approver_Compensation</fullName>
        <field>Pending_Approver__c</field>
        <formula>&quot;Comp Approval Queue&quot;</formula>
        <name>Pending Approver: Compensation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Approver_Current_Approver_Owner</fullName>
        <description>For reporting purposes</description>
        <field>Pending_Approver__c</field>
        <formula>IF(AND( 
NOT(ISBLANK(Current_Approver__c )), 
ISPICKVAL( Current_Owner__r.Owner_Type__c ,&quot;Account&quot;)),  Current_Approver__r.FirstName &amp; &quot; &quot; &amp;  Current_Approver__r.LastName ,
IF(AND( 
ISPICKVAL( Current_Owner__r.Owner_Type__c ,&quot;District&quot;), 
NOT(ISBLANK(Current_Owner__c)) 
), Current_Owner__r.FirstName &amp; &quot; &quot; &amp;  Current_Owner__r.LastName, &quot;&quot;))</formula>
        <name>Pending Approver: Current Approver/Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Approver_Current_Channel</fullName>
        <field>Pending_Approver__c</field>
        <formula>IF(OR( 
CONTAINS(Current_Channel__c , &quot;Large&quot;),
CONTAINS(Current_Channel__c , &quot;Public Sector&quot;), 
CONTAINS(Current_Channel__c , &quot;Federal&quot;)), &quot;TAG Coord. Large/Public&quot;,
IF(CONTAINS(Current_Channel__c , &quot;Medium&quot;), &quot;TAG Coord. Medium&quot;,
IF(OR( 
CONTAINS(Current_Channel__c , &quot;VAR&quot;), 
CONTAINS(Current_Channel__c , &quot;MSA&quot;) 
), &quot;TAG Coord. VAR&quot;,
IF(CONTAINS(Current_Channel__c , &quot;Small&quot;), &quot;TAG Coord. Small&quot;,
IF(CONTAINS(Current_Channel__c , &quot;NIS&quot;), &quot;TAG Coord. NIS&quot;,
IF(CONTAINS(Current_Channel__c , &quot;Field Sales Cable&quot;), &quot;TAG Coord. Field Sales Cable&quot;,
IF(CONTAINS(Current_Channel__c , &quot;Dealer&quot;), &quot;TAG Coord. Dealer&quot;, 
IF(OR( 
CONTAINS(Current_Channel__c , &quot;M2M Reseller&quot;), 
CONTAINS(Current_Channel__c , &quot;ABS&quot;) 
), &quot;TAG Coord. M2M Reseller&quot;, 
IF(AND( 
NOT(ISBLANK(Current_Approver__c )), 
ISPICKVAL( Current_Owner__r.Owner_Type__c ,&quot;Account&quot;), New_Channel__c=Current_Channel__c), Current_Approver__r.FirstName &amp; &quot; &quot; &amp; Current_Approver__r.LastName , 
IF(AND( 
ISPICKVAL( Current_Owner__r.Owner_Type__c ,&quot;District&quot;), 
NOT(ISBLANK(Current_Owner__c)) , New_Channel__c=Current_Channel__c), Current_Owner__r.FirstName &amp; &quot; &quot; &amp; Current_Owner__r.LastName, 
IF(OR( 
CONTAINS(New_Channel__c , &quot;Large&quot;), 
CONTAINS(New_Channel__c , &quot;Public Sector&quot;), 
CONTAINS(New_Channel__c , &quot;Federal&quot;) 
), &quot;TAG Coord. Large/Public&quot;, 
IF(CONTAINS(New_Channel__c , &quot;Medium&quot;), &quot;TAG Coord. Medium&quot;, 
IF(OR( 
CONTAINS(New_Channel__c , &quot;VAR&quot;), 
CONTAINS(New_Channel__c , &quot;MSA&quot;)), &quot;TAG Coord. VAR&quot;, 
IF(CONTAINS(New_Channel__c , &quot;Small&quot;), &quot;TAG Coord. Small&quot;, 
IF(CONTAINS(New_Channel__c , &quot;NIS&quot;), &quot;TAG Coord. NIS&quot;, 
IF(CONTAINS(New_Channel__c , &quot;Field Sales Cable&quot;), &quot;TAG Coord. Field Sales Cable&quot;, 
IF(CONTAINS(New_Channel__c , &quot;Dealer&quot;), &quot;TAG Coord. Dealer&quot;, 
IF(OR( 
CONTAINS(New_Channel__c , &quot;M2M Reseller&quot;), 
CONTAINS(New_Channel__c , &quot;ABS&quot;)), &quot;TAG Coord. M2M Reseller&quot;, 
IF(AND( 
NOT(ISBLANK(Current_Approver__c )), 
ISPICKVAL( Current_Owner__r.Owner_Type__c ,&quot;Account&quot;)), Current_Approver__r.FirstName &amp; &quot; &quot; &amp; Current_Approver__r.LastName , 
IF(AND( 
ISPICKVAL( Current_Owner__r.Owner_Type__c ,&quot;District&quot;), 
NOT(ISBLANK(Current_Owner__c)) 
), Current_Owner__r.FirstName &amp; &quot; &quot; &amp; Current_Owner__r.LastName, 
IF(AND( 
NOT(ISBLANK(New_Approver__c )), 
New_Approver__c&lt;&gt;Current_Approver__c, 
ISPICKVAL( New_Owner__r.Owner_Type__c ,&quot;Account&quot;)), New_Approver__r.FirstName &amp; &quot; &quot; &amp; New_Approver__r.LastName , 
IF(AND( 
ISPICKVAL( New_Owner__r.Owner_Type__c ,&quot;District&quot;), 
NOT(ISBLANK(New_Owner__c)), 
Current_Owner__c&lt;&gt;New_Owner__c 
), New_Owner__r.FirstName &amp; &quot; &quot; &amp; New_Owner__r.LastName , &quot;&quot;))))))))))))))))))))))</formula>
        <name>Pending Approver: Current Channel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Approver_Member_Approver</fullName>
        <field>Pending_Approver__c</field>
        <formula>Member_Approver__r.FirstName &amp; &quot; &quot; &amp; Member_Approver__r.LastName</formula>
        <name>Pending Approver: Member Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Approver_Member_Channel</fullName>
        <field>Pending_Approver__c</field>
        <formula>IF(OR( 
CONTAINS(Member_Channel__c,&quot;Large&quot;), 
CONTAINS(Member_Channel__c,&quot;Public Sector&quot;), 
CONTAINS(Member_Channel__c,&quot;Federal&quot;), 
CONTAINS(Current_Channel__c,&quot;Large&quot;), 
CONTAINS(Current_Channel__c,&quot;Public Sector&quot;), 
CONTAINS(Current_Channel__c,&quot;Federal&quot;)),&quot;TAG Coord. Large/Public&quot;, 
IF(OR( 
CONTAINS(Member_Channel__c,&quot;Medium&quot;), 
CONTAINS(Current_Channel__c,&quot;Medium&quot;)),&quot;TAG Coord. Medium&quot;,
IF(OR( 
CONTAINS(Member_Channel__c,&quot;Small&quot;), 
CONTAINS(Current_Channel__c,&quot;Small&quot;)),&quot;TAG Coord. Small&quot;, 
IF(OR( 
CONTAINS(Member_Channel__c,&quot;Dealer&quot;), 
CONTAINS(Current_Channel__c,&quot;Dealer&quot;)),&quot;TAG Coord. Dealer&quot;,
IF(OR( 
CONTAINS(Member_Channel__c,&quot;M2M Reseller&quot;), 
CONTAINS(Current_Channel__c,&quot;M2M Reseller&quot;)), &quot;TAG Coord. M2M Reseller&quot;,
IF(OR( 
CONTAINS(Member_Channel__c,&quot;Field Sales Cable&quot;), 
CONTAINS(Current_Channel__c,&quot;Field Sales Cable&quot;)), &quot;TAG Coord. Field Sales Cable&quot;,
IF(OR( 
CONTAINS(Member_Channel__c,&quot;NIS&quot;), 
CONTAINS(Current_Channel__c,&quot;NIS&quot;)), &quot;TAG Coord. NIS&quot;,
IF(OR( 
CONTAINS(Member_Channel__c,&quot;VAR&quot;), 
CONTAINS(Current_Channel__c,&quot;VAR&quot;)),&quot;TAG Coord. VAR&quot;,&quot;&quot;))))))))</formula>
        <name>Pending Approver: Member Channel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Approver_New_Approver_Owner</fullName>
        <field>Pending_Approver__c</field>
        <formula>IF(AND(
NOT(ISBLANK(New_Approver__c )),
New_Approver__c&lt;&gt;Current_Approver__c,
ISPICKVAL( New_Owner__r.Owner_Type__c ,&quot;Account&quot;)),  New_Approver__r.FirstName &amp; &quot; &quot; &amp;  New_Approver__r.LastName ,
IF(AND( 
ISPICKVAL( New_Owner__r.Owner_Type__c ,&quot;District&quot;),
NOT(ISBLANK(New_Owner__c)),
Current_Owner__c&lt;&gt;New_Owner__c
), New_Owner__r.FirstName &amp; &quot; &quot; &amp;  New_Owner__r.LastName , &quot;&quot;))</formula>
        <name>Pending Approver: New Approver/Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Approver_New_Channel</fullName>
        <description>Used for reporting purposes</description>
        <field>Pending_Approver__c</field>
        <formula>IF(AND( 
NOT(ISBLANK(Current_Approver__c )), 
ISPICKVAL( Current_Owner__r.Owner_Type__c ,&quot;Account&quot;), New_Channel__c=Current_Channel__c),   Current_Approver__r.FirstName &amp; &quot; &quot; &amp;  Current_Approver__r.LastName  ,
IF(AND( 
ISPICKVAL( Current_Owner__r.Owner_Type__c ,&quot;District&quot;), 
NOT(ISBLANK(Current_Owner__c)) , New_Channel__c=Current_Channel__c),  Current_Owner__r.FirstName &amp; &quot; &quot; &amp;  Current_Owner__r.LastName,
IF(OR( 
CONTAINS(New_Channel__c , &quot;Large&quot;), 
CONTAINS(New_Channel__c , &quot;Public Sector&quot;), 
CONTAINS(New_Channel__c , &quot;Federal&quot;) 
), &quot;TAG Coord. Large/Public&quot;,
IF(CONTAINS(New_Channel__c , &quot;Medium&quot;), &quot;TAG Coord. Medium&quot;,
IF(OR( 
CONTAINS(New_Channel__c , &quot;VAR&quot;), 
CONTAINS(New_Channel__c , &quot;MSA&quot;)), &quot;TAG Coord. VAR&quot;, 
IF(CONTAINS(New_Channel__c , &quot;Small&quot;), &quot;TAG Coord. Small&quot;, 
IF(CONTAINS(New_Channel__c , &quot;NIS&quot;), &quot;TAG Coord. NIS&quot;, 
IF(CONTAINS(New_Channel__c , &quot;Field Sales Cable&quot;), &quot;TAG Coord. Field Sales Cable&quot;, 
IF(CONTAINS(New_Channel__c , &quot;Dealer&quot;), &quot;TAG Coord. Dealer&quot;, 
IF(OR( 
CONTAINS(New_Channel__c , &quot;M2M Reseller&quot;), 
CONTAINS(New_Channel__c , &quot;ABS&quot;)), &quot;TAG Coord. M2M Reseller&quot;, 
&quot;&quot;))))))))))</formula>
        <name>Pending Approver: New Channel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Approver_None</fullName>
        <description>Used for when the approval process has ended</description>
        <field>Pending_Approver__c</field>
        <name>Pending Approver: None</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Approver_TAG_Support</fullName>
        <field>Pending_Approver__c</field>
        <formula>&quot;Support&quot;</formula>
        <name>Pending Approver: TAG Support</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending_Approver_for_Account_Team</fullName>
        <field>Pending_Approver__c</field>
        <formula>IF(ISPICKVAL(Role__c, &quot;ABS Solution Specialist&quot;), &quot;ABS Solution Specialist&quot;,
IF(OR(ISPICKVAL(Role__c ,&quot;Inside Sales Rep - Large/Public&quot;),ISPICKVAL(Role__c ,&quot;Account Development Rep - Vodafone&quot;)), &quot;TAG Coord. Large/Public&quot;,
IF(ISPICKVAL(Role__c ,&quot;Inside Sales Rep - Medium&quot;), &quot;TAG Coord. Medium&quot;,
IF(ISPICKVAL(Role__c, &quot;Reseller Manager&quot;), &quot;TAG Coord. M2M Reseller&quot;,
IF(ISPICKVAL(Role__c,&quot;VAR Account Manager&quot;), &quot;TAG Coord. VAR and VAR Account Manager&quot;,
IF(ISPICKVAL(Role__c, &quot;VAR Account Manager - Wireline&quot;), &quot;TAG Coord. VAR and VAR Account Manager - Wireline&quot;,
IF(ISPICKVAL(Role__c,&quot;Wireline Specialist - Large/Public&quot;), &quot;TAG Coord. Large/Public and Wireline Specialist - Large/Public&quot;,
IF(ISPICKVAL(Role__c,&quot;Wireline Specialist - Medium&quot;), &quot;Wireline Specialist - Medium and TAG Coord. Medium&quot;,&quot;Comp Approval Queue&quot;))))))))</formula>
        <name>Pending Approver for Account Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Previous_Approver</fullName>
        <field>Previous_Approver__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>Previous Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RecallUpdate</fullName>
        <field>Status__c</field>
        <literalValue>Cancelled</literalValue>
        <name>RecallUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approved_Rejected_Date_to_NOW</fullName>
        <description>Auto-populates Approved/Rejected Date to NOW()</description>
        <field>Approved_Rejected_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Approved/Rejected Date to NOW()</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Effective_Date_to_TODAY</fullName>
        <description>Set Effective Date to Today&apos;s date when Assignment Request Item has been approved</description>
        <field>Effective_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Effective Date to TODAY()</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Approved</fullName>
        <description>Set Approval Status to Approved</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Rejected</fullName>
        <description>Set Status to Rejected when Assignment Request Item has been rejected</description>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_More_Info</fullName>
        <field>More_information_required__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck More Info</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ARI1</fullName>
        <actions>
            <name>Set_Approved_Rejected_Date_to_NOW</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Effective_Date_to_TODAY</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Status_to_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Assignment_Request_Item__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
