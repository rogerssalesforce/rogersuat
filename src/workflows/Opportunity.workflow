<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_for_Expected_Sign_Date1</fullName>
        <description>Email Alert for Expected Sign Date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Expected_Sign_Date_Alert</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_Closed_won_for_SSR_ADM</fullName>
        <ccEmails>SSRREQUESTS@rci.rogers.com</ccEmails>
        <ccEmails>rbs_soc@rci.rogers.com</ccEmails>
        <description>Email Notification -Closed won for SSR/ADM</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>RBS/Automated_Notification_for_SSR_ADM</template>
    </alerts>
    <alerts>
        <fullName>Missed_Est_Close_Date_Email</fullName>
        <description>Missed Est Close Date Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Missed_Close_Date_on_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_Account_Owner_on_Opp_Creation</fullName>
        <description>Notification to Account Owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RDC/Opp_Notification_to_Account_Owner</template>
    </alerts>
    <alerts>
        <fullName>Opp_Closed_Won_Notification_to_VP_Sales</fullName>
        <description>Opp Closed Won Notification to VP Sales</description>
        <protected>false</protected>
        <recipients>
            <recipient>LargeandPublicL3ID31</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>MediumL3ID21</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>SmallL3ID74</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RDC/Opp_Closed_Won_Notification_to_VP_Sales</template>
    </alerts>
    <alerts>
        <fullName>Wireline_Contract_Oppty_Closed_Won</fullName>
        <description>Create email alert to the Wireline Contract Team group when a new Wireline Oppty is marked as “Closed Won”</description>
        <protected>false</protected>
        <recipients>
            <recipient>Wireline_Contract_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Wireline_Contract_Oppty_Closed_Won_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Actual_Date_equal_today</fullName>
        <description>Update Actual Date to todays date for closed opportunity</description>
        <field>Actual_Closed_Date__c</field>
        <formula>Today()</formula>
        <name>Actual Date equal today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_1_to_close_Date_changed</fullName>
        <field>times_Close_Date_has_changed__c</field>
        <formula>times_Close_Date_has_changed__c + 1</formula>
        <name>Add 1 to #close Date changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Added_to_Account_with_Campaign_Source</fullName>
        <description>To flag that the Opportunity was created with an Account that already has an Opp with an existing Primary Campaign Source</description>
        <field>Added_to_Account_with_Campaign__c</field>
        <literalValue>1</literalValue>
        <name>Added to Account with Campaign Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Blank_Signed_Date</fullName>
        <field>Signed_Date__c</field>
        <name>Blank_Signed_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_UPDATE_Opp_Record_Type_Restricted</fullName>
        <description>Updates the Opportunity Record Type to &quot;Restricted - New&quot;</description>
        <field>RecordTypeId</field>
        <lookupValue>Rogers_EBU_Restricted_Open</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CPQ UPDATE Opp. Record Type - Restricted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_UPDATE_Opp_Record_Type_to_Closed</fullName>
        <description>Updates the Opportunity Record Type to &quot;Rogers EBU - Closed&quot;</description>
        <field>RecordTypeId</field>
        <lookupValue>Rogers_EBU_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CPQ UPDATE Opp. Record Type to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_UPDATE_Opp_Record_Type_to_Qualified</fullName>
        <description>Updates the Opportunity Record Type to &quot;Rogers EBU - Qualified&quot;</description>
        <field>RecordTypeId</field>
        <lookupValue>Rogers_EBU_Qualified</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CPQ UPDATE Opp. Record Type to Qualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Oppty_Record_Type_to_Wireless</fullName>
        <field>RecordTypeId</field>
        <lookupValue>New_Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Oppty Record Type to “Wireless –</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Record_Type_to_Closed_Opportunity</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type to Closed Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Record_Type_to_Data_Centre_Tran</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Data_Centre_Transactional</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type to Data Centre – Tran</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Record_Type_to_Mature_Opportunity</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Mature_Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type to Mature Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Record_Type_to_Wireline_New_Ent</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Wireline_New_Enterprise_Sale</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type to Wireline – New Ent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Date_equal_today</fullName>
        <description>Update close date to todays date for closed opportunity</description>
        <field>CloseDate</field>
        <formula>Today()</formula>
        <name>Close Date equal today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Complex_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Data_Centre_Complex</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Complex RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Create_Task_on_Next_Step_Date_reset</fullName>
        <field>Create_a_Task_with_Next_Step_Date__c</field>
        <literalValue>0</literalValue>
        <name>Create Task on Next Step Date reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Has_Campaign_Source_update</fullName>
        <field>Has_Campaign__c</field>
        <formula>1</formula>
        <name>Has Campaign Source update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Latest_From_Sales_Stage_Field_Update</fullName>
        <field>Most_Recent_From_Stage__c</field>
        <formula>TEXT (PRIORVALUE(StageName))</formula>
        <name>Latest From Sales Stage Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Latest_To_Sales_Stage_Field_Update</fullName>
        <field>Most_Recent_To_Stage__c</field>
        <formula>TEXT( StageName )</formula>
        <name>Latest To Sales Stage Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Upsell_Delta_MRR_when_Open</fullName>
        <field>Upsell_Delta_MRR__c</field>
        <formula>Estimated_MRR__c  -  Existing_MRR__c</formula>
        <name>Opp Upsell Delta MRR when Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Upsell_Delta_MRR_when_Won</fullName>
        <field>Upsell_Delta_MRR__c</field>
        <formula>Won_MRR__c  -  Existing_MRR__c</formula>
        <name>Opp Upsell Delta MRR when Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RFP_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Data_Centre_RFP</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RFP RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Transactional_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Data_Centre_Transactional</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Transactional RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UPDATE_Opp_Record_Type_to_Rest_Closed</fullName>
        <description>Updates the Opportunity Record Type to &quot;Rogers EBU - Restricted Closed&quot;</description>
        <field>RecordTypeId</field>
        <lookupValue>Rogers_EBU_Restricted_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>UPDATE Opp. Record Type to Rest. Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Added_to_Account_with_W2L</fullName>
        <field>Added_to_Account_with_W2L__c</field>
        <literalValue>1</literalValue>
        <name>Update Added to Account with W2L</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_EBU_Revenue_Data_Centre</fullName>
        <field>EBU_Revenue__c</field>
        <formula>Total_Contract_Revenue_TCV__c</formula>
        <name>Update EBU Revenue - Data Centre</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_EBU_Revenue_Wireless</fullName>
        <field>EBU_Revenue__c</field>
        <formula>Amount</formula>
        <name>Update EBU Revenue - Wireless</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_EBU_Revenue_Wireline</fullName>
        <field>EBU_Revenue__c</field>
        <formula>Estimated_Total_Contract_Value_TCV__c</formula>
        <name>Update EBU Revenue Wireline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Has_Campaign_Source_to_0</fullName>
        <field>Has_Campaign__c</field>
        <formula>0</formula>
        <name>Update Has Campaign to 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MRR_information</fullName>
        <description>Updates Est MRR information with the Est MRC information</description>
        <field>Estimated_MRR__c</field>
        <formula>Estimated_MRC1__c</formula>
        <name>Update MRR information</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_NP_OTC</fullName>
        <field>OTC_NP_Services__c</field>
        <formula>Primary_Quote__r.OTC_non_Professional_Services_Roll_up__c</formula>
        <name>Update NP OTC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_One_Time_Charge</fullName>
        <description>Update the estimated one time charge from the manual field with the quote information</description>
        <field>Estimated_One_Time_Charge__c</field>
        <formula>Estimated_One_Time_Charge1__c</formula>
        <name>Update One Time Charge</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_P_OTC</fullName>
        <field>OTC_P_Services__c</field>
        <formula>Primary_Quote__r.OTC_Professional_Services_Roll_up__c</formula>
        <name>Update P OTC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Signed_Date</fullName>
        <field>Signed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update_Signed_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Term</fullName>
        <description>Update the manual term with the term from the quote</description>
        <field>New_Term_Months__c</field>
        <formula>New_Term_Months1__c</formula>
        <name>Update Term</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_of_Cabs</fullName>
        <description>If there is a Primary Quote get the # of Cabinets from the Quote and if there is no Primary Quote, get it from the roll-up of Opportunity Data Centre records.</description>
        <field>of_Cabinets__c</field>
        <formula>IF(AND(NOT(ISBLANK(Primary_Quote__c)), Primary_Quote__r.Syncing__c), Primary_Quote__r.of_Cabinets__c,  of_Cabinets_Rel__c)</formula>
        <name>Update # of Cabs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>test</fullName>
        <field>NextStep</field>
        <formula>&quot;test&quot;</formula>
        <name>test</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>%23 Close Date Changes</fullName>
        <actions>
            <name>Add_1_to_close_Date_changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( CloseDate  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>%27Has Campaign Source%27 update</fullName>
        <actions>
            <name>Has_Campaign_Source_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.CampaignId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Data Centre</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>%27Has Campaign Source%27 update to 0</fullName>
        <actions>
            <name>Update_Has_Campaign_Source_to_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.CampaignId</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Data Centre</value>
        </criteriaItems>
        <description>Update Has Campaign Source to 0 if there&apos;s no campaign source.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Added to Account with Campaign</fullName>
        <actions>
            <name>Added_to_Account_with_Campaign_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Opps_with_Campaign_Source__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Data Centre</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>At Opp creation</fullName>
        <actions>
            <name>Notification_to_Account_Owner_on_Opp_Creation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Business_Unit__c, &quot;Data Centre&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>At any edit of Opp where no Primary Quote</fullName>
        <actions>
            <name>Update_of_Cabs</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Runs at any edit of Opportunity with no Primary Quote.</description>
        <formula>AND(ISBLANK(Primary_Quote__c),ISPICKVAL(Business_Unit__c,&quot;Data Centre&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>At any edit of Opp where stage %3C%3D 3</fullName>
        <actions>
            <name>Update_NP_OTC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_P_OTC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Term</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_of_Cabs</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>AND(NOT(ISBLANK(Primary_Quote__c)) &amp;&amp;  Primary_Quote__r.Syncing__c,ISPICKVAL(Business_Unit__c,&quot;Data Centre&quot;))</description>
        <formula>FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ Rogers EBU - Convert Qualified to Closed</fullName>
        <actions>
            <name>CPQ_UPDATE_Opp_Record_Type_to_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rogers EBU - Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <description>Updates the record type of an Opportunity to &quot;Rogers EBU - Closed&quot; once the Sales Stage is updated to &quot;Closed Won&quot; or &quot;Closed Lost&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CPQ Rogers EBU - Convert to Restricted</fullName>
        <actions>
            <name>CPQ_UPDATE_Opp_Record_Type_Restricted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Restricted__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rogers EBU - New</value>
        </criteriaItems>
        <description>Converts the Opportunity Record Type to &quot;Restricted - New&quot; if the Opportunity is created against an Account with the &quot;Restricted Account Flag?&quot; = Yes</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CPQ Rogers EBU - Qualify Opportunity</fullName>
        <actions>
            <name>CPQ_UPDATE_Opp_Record_Type_to_Qualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Qualify</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rogers EBU - New</value>
        </criteriaItems>
        <description>Updates the record type of an Opportunity to &quot;Rogers EBU - Qualified&quot; once the Sales Stage is updated to &quot;Qualify&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type to Closed Opportunity</fullName>
        <actions>
            <name>Change_Record_Type_to_Closed_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Wireless</value>
        </criteriaItems>
        <description>Change Record Type to Closed Opportunity when Stage value is changed to Close Win or Close Loss</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type to Mature Opportunity</fullName>
        <actions>
            <name>Change_Record_Type_to_Mature_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Qualify,Propose,Negotiate,Commit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Wireless</value>
        </criteriaItems>
        <description>Change Record Type to Mature Opportunity when Stage value is changed to Qualified (30%)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type to Wireless %E2%80%93 New Opportunity</fullName>
        <actions>
            <name>Change_Oppty_Record_Type_to_Wireless</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Wireless</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type to Wireline %E2%80%93 New Enterprise Sale</fullName>
        <actions>
            <name>Change_Record_Type_to_Wireline_New_Ent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Wireline</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Wireline - Carrier Contract Renewal,Wireline - Interco Opportunity,Wireline - New Carrier Sale</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Wireline - Enterprise Contract Renewal,Wireline - Enterprise Move,Wireline - New Channel Sale</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Wireline - Temp Solution Opportunity</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Copy Est MRC into Est MRR</fullName>
        <actions>
            <name>Update_MRR_information</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_One_Time_Charge</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Term</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.IsSyncing</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Wireline</value>
        </criteriaItems>
        <description>When Sales Rep Generates a quote and it&apos;s synchronized with the revenue information fields overwritting the existing content.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Create Task on Next Step Date</fullName>
        <actions>
            <name>Create_Task_on_Next_Step_Date_reset</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Activity_for_Opportunity_Next_Step_Date</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Create_a_Task_with_Next_Step_Date__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Next_Step_Date__c</field>
            <operation>greaterOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Wireless</value>
        </criteriaItems>
        <description>Creates a Task with the Next Step Date as the Due Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Expected Sign Date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Carrier Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Wireline</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Alert_for_Expected_Sign_Date1</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Latest From Sales Stage Field Update</fullName>
        <actions>
            <name>Latest_From_Sales_Stage_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(  ISNEW(),  ISCHANGED(  StageName  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Latest To Sales Stage Field Update</fullName>
        <actions>
            <name>Latest_To_Sales_Stage_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(  ISNEW(),  ISCHANGED(  StageName  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Make Actual Date equal Todayfor closed opportunity</fullName>
        <actions>
            <name>Actual_Date_equal_today</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Close_Date_equal_today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates Actual Date with Todays date if Opportunity stage equals Closed Won or Closed Lost</description>
        <formula>IF(( OR( ISPICKVAL(StageName, &quot;Closed Won&quot;),ISPICKVAL(StageName, &quot;Closed Lost&quot;))), true, false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Missed Est%2E Close Date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Channel__c</field>
            <operation>notEqual</operation>
            <value>Carrier</value>
        </criteriaItems>
        <description>Email reminder when Est Close Date on an Opportunity is missed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Missed_Est_Close_Date_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>New Opp on Account with W2L Opp</fullName>
        <actions>
            <name>Update_Added_to_Account_with_W2L</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Account_Other_W2L_Opp_Count__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Data Centre</value>
        </criteriaItems>
        <description>If the Opp is created on an Account that already has other Opp with Lead Source = W2L.
** This WF relies on the fact that Account W2L Opp Count formula field is not yet updated at the Creation of the current Opp, if the current Opp is also a W2L</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opp Upsell Delta MRR when Closed Won</fullName>
        <actions>
            <name>Opp_Upsell_Delta_MRR_when_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won,Renewal Win,Contracted,Win</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Wireline - New Enterprise Sale</value>
        </criteriaItems>
        <description>Upsell Delta MRR equals Won MRR - Existing MRR when Opp is Closed Won</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp Upsell Delta MRR when Open</fullName>
        <actions>
            <name>Opp_Upsell_Delta_MRR_when_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Won,Contracted,Renewal Win,Win</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Wireline - New Enterprise Sale</value>
        </criteriaItems>
        <description>Upsell Delta MRR equals Estimated MRR - Existing MRR before Opp is Closed</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp moved to ClosedWon</fullName>
        <actions>
            <name>Opp_Closed_Won_Notification_to_VP_Sales</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Check to see if an opportunity with MRR&gt;=500$ or OTC&gt;=5000$ has been marked as ClosedWon and the previous values for IsClosed and IsWon are false. Should prevent the rule from triggering more than once if a ClosedWon opportunity is edited.</description>
        <formula>AND(ISCHANGED(StageName ),  CONTAINS(Text(StageName) , &quot;Closed Won&quot;),  OR(   Expected_MRR1__c   &gt;= 500, Total_OTC__c &gt;= 5000),  $Profile.Name=&quot;Admin3 Integration User&quot;, ISPICKVAL(Business_Unit__c,&quot;Data Centre&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rogers EBU - Convert to Restricted Closed</fullName>
        <actions>
            <name>UPDATE_Opp_Record_Type_to_Rest_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rogers EBU - Restricted Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <description>Converts the Opportunity Record Type to &quot;Rogers EBU - Restricted Closed&quot; if the Opportunity is closing against an Account with the &quot;Restricted Account Flag?&quot; = Yes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update EBU Revenue - Data Centre</fullName>
        <actions>
            <name>Update_EBU_Revenue_Data_Centre</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Data Centre</value>
        </criteriaItems>
        <description>Ports TCR to the &quot;EBU Revenue&quot; field for Data Centre Opptys</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update EBU Revenue - Wireless</fullName>
        <actions>
            <name>Update_EBU_Revenue_Wireless</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Wireless</value>
        </criteriaItems>
        <description>Ports Amount to the &quot;EBU Revenue&quot; field for Data Centre Opptys</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update EBU Revenue - Wireline</fullName>
        <actions>
            <name>Update_EBU_Revenue_Wireline</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Wireline</value>
        </criteriaItems>
        <description>Ports TCV to the &quot;EBU Revenue&quot; field for Wireline Opptys</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Record Type - Complex</fullName>
        <actions>
            <name>Complex_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>Complex</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Record Type - RFP</fullName>
        <actions>
            <name>RFP_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>RFP</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Record Type - Transactional</fullName>
        <actions>
            <name>Transactional_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>Transactional</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Wireline Contract Oppty Closed Won Alert</fullName>
        <actions>
            <name>Wireline_Contract_Oppty_Closed_Won</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Business_Unit__c</field>
            <operation>equals</operation>
            <value>Wireline</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Activity_for_Opportunity_Next_Step_Date</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Opportunity.Next_Step_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Activity for Opportunity Next Step Date</subject>
    </tasks>
</Workflow>
