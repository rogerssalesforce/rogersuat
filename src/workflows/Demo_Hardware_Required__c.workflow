<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Demo_Case_Due_Date</fullName>
        <description>Demo Case - Due Date</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Demo_Case_Due_Date</template>
    </alerts>
    <alerts>
        <fullName>Demo_Case_Week_After_Due_Date</fullName>
        <description>Demo Case - Week After Due Date</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Demo_Case_Week_after_due_date</template>
    </alerts>
    <alerts>
        <fullName>Demo_Case_Week_Before_Due_Date</fullName>
        <description>Demo Case - Week Before Due Date</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Demo_Case_Week_before_due_date</template>
    </alerts>
    <rules>
        <fullName>Demo - Due Date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Demo Device Prime</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Complete,Cancelled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Demo_Hardware_Required__c.Date_Returned__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Demo Case
Emails for week before due date and on due date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Demo_Case_Week_After_Due_Date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Demo_Hardware_Required__c.Due_Date__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Demo_Case_Week_Before_Due_Date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Demo_Hardware_Required__c.Due_Date__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Demo_Case_Due_Date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Demo_Hardware_Required__c.Due_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Demo - Due Date %28test%29</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Demo Device Prime</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Demo_Hardware_Required__c.Date_Returned__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Demo Case
Emails for week before due date and on due date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Demo_Case_Due_Date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Demo_Hardware_Required__c.Due_Date__c</offsetFromField>
            <timeLength>-6</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
