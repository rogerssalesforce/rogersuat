<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SPR_Approved_Closed</fullName>
        <description>SPR Approved/Closed</description>
        <protected>false</protected>
        <recipients>
            <field>Global_Recipient_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Global_Recipient_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Global_Recipient_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Global_Recipient_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Global_Recipient_5__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Requested_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RDC/SPR_Closed_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Close_Date_Update</fullName>
        <description>Update Close Date to Today when Status goes Closed.</description>
        <field>Close_Date__c</field>
        <formula>Today()</formula>
        <name>Close Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Assigned_update</fullName>
        <description>Update Date Assigned to Today whenever the Owner of SPR changes.</description>
        <field>Date_Assigned__c</field>
        <formula>Today()</formula>
        <name>Date Assigned update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Close_Date_to_Blank</fullName>
        <field>Close_Date__c</field>
        <name>Update Close Date to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Global_Email_1</fullName>
        <field>Global_Recipient_1_Email__c</field>
        <formula>Global_Recipient_1__r.Email</formula>
        <name>Update Global Email 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Global_Email_2</fullName>
        <field>Global_Recipient_2_Email__c</field>
        <formula>Global_Recipient_2__r.Email</formula>
        <name>Update Global Email 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Global_Email_3</fullName>
        <field>Global_Recipient_3_Email__c</field>
        <formula>Global_Recipient_3__r.Email</formula>
        <name>Update Global Email 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Global_Email_4</fullName>
        <field>Global_Recipient_4_Email__c</field>
        <formula>Global_Recipient_4__r.Email</formula>
        <name>Update Global Email 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Global_Email_5</fullName>
        <field>Global_Recipient_5_Email__c</field>
        <formula>Global_Recipient_5__r.Email</formula>
        <name>Update Global Email 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_Email</fullName>
        <field>Owner_Email__c</field>
        <name>Update Owner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Requested_By_email</fullName>
        <field>Requested_By_Email__c</field>
        <formula>Requested_By__r.Email</formula>
        <name>Update Requested By email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Awaiting_Pricing_Analys</fullName>
        <field>Status__c</field>
        <literalValue>Awaiting Pricing Analyst Feedback</literalValue>
        <name>Update Status to Awaiting Pricing Analys</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>At SPR Creation%2FEdit</fullName>
        <actions>
            <name>Update_Global_Email_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Global_Email_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Global_Email_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Global_Email_4</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Global_Email_5</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Requested_By_email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Closed SPR Status Changes when New Note Added</fullName>
        <actions>
            <name>Update_Close_Date_to_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_to_Awaiting_Pricing_Analys</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Whenever a closed SPR gets a new note added, its status will go back to &quot;Awaiting Pricing Analyst Feedback&quot;.</description>
        <formula>AND (ISPICKVAL( Status__c , &quot;Closed&quot;),  ischanged (Note_Just_Added__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Newly Assigned SPR Own</fullName>
        <actions>
            <name>Update_Status_to_Awaiting_Pricing_Analys</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Changed Not Yet Assigned to a Pricing Analyst to Awaiting Pricing Analyst Feedback when SPR Owner assigned for first time.</description>
        <formula>AND  (    ISCHANGED(Owner_Name__c) ,     ISPICKVAL( Status__c , &quot;Not Yet Assigned to a Pricing Analyst&quot;) ,    PRIORVALUE(Owner_Name__c)  == &quot;SPR Queue&quot;,   Owner_Name__c &lt;&gt; &quot;SPR Queue&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SPR Approved%2FClosed</fullName>
        <actions>
            <name>SPR_Approved_Closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SPR__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Notify Requestor and all Global Recipients</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Close Date of SPR</fullName>
        <actions>
            <name>Close_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SPR__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Update Close Date to today whenever SPR status updates to Closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
