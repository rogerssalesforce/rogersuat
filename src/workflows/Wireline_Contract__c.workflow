<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contract_expiration_notification</fullName>
        <description>Contract expiration notification</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Contract_Expiration_Notic</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_for_Contract_Expiry_Date</fullName>
        <ccEmails>sfdc.support@rci.rogers.com</ccEmails>
        <description>Email Alert for Contract Expiry Date</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>Wireline Specialist - Large/Public</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Wireline Specialist - Medium</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>RBS/Wireline_Contract_Expiry_Alert</template>
    </alerts>
    <rules>
        <fullName>Alert the expiry contracts</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Wireline_Contract__c.Contract_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Alert_for_Contract_Expiry_Date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Wireline_Contract__c.Contract_End_Date__c</offsetFromField>
            <timeLength>-90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Send Contract Expiration Notice</fullName>
        <active>true</active>
        <formula>IF(ISPICKVAL(Status__c,&quot;Activated&quot;),TRUE,FALSE)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Contract_expiration_notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Wireline_Contract__c.Contract_End_Date__c</offsetFromField>
            <timeLength>-90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
