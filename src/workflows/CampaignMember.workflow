<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Marketing_Activity_Notification</fullName>
        <description>Marketing Activity Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Marketing_Activity_Notification_HTML</template>
    </alerts>
    <alerts>
        <fullName>Preferred_Call_Back_Email_Notification</fullName>
        <description>Preferred Call Back Email Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Preferred_Call_Date_notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Campaign_Member_Closed_field</fullName>
        <field>Campaign_Member_Closed__c</field>
        <literalValue>1</literalValue>
        <name>Update Campaign Member Closed? field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Campaign_Member_Up_field_to_False</fullName>
        <field>Campaign_Member_Updated__c</field>
        <literalValue>0</literalValue>
        <name>Update Campaign Member Up field to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Campaign_Member_Updated_field</fullName>
        <field>Campaign_Member_Updated__c</field>
        <literalValue>1</literalValue>
        <name>Update &quot;Campaign Member Updated?&quot; field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Campaign_Member_Updated_to_False</fullName>
        <field>Campaign_Member_Updated__c</field>
        <literalValue>0</literalValue>
        <name>Update Campaign Member Updated to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RPC_Contacted_field</fullName>
        <field>RPC_Contacted__c</field>
        <literalValue>1</literalValue>
        <name>Update RPC Contacted field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RPC_Contacted_field_to_False</fullName>
        <field>RPC_Contacted__c</field>
        <literalValue>0</literalValue>
        <name>Update RPC Contacted field to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Marketing Activity Notification</fullName>
        <actions>
            <name>Marketing_Activity_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Marketing Activity Notification</description>
        <formula>AND  (OR (ISNEW(),  ISCHANGED(Assigned_To__c)),  ISPICKVAL( Send_Email_Notification_to_Assigned_User__c , &quot;yes&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Preferred Call Date notification</fullName>
        <actions>
            <name>Preferred_Call_Back_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CampaignMember.Preferred_Call_Back_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Campaign Member has reached its preferred Call Back Date notification.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>CampaignMember.Preferred_Call_Back_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Campaign Member Closed to False</fullName>
        <actions>
            <name>Update_Campaign_Member_Updated_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>IF(ISPICKVAL(Status, &quot;New&quot;), True,  IF(ISPICKVAL(Status, &quot;Registered&quot;), True,  IF(ISPICKVAL(Status, &quot;Attended&quot;), True, False ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Campaign Member Closed to True</fullName>
        <actions>
            <name>Update_Campaign_Member_Closed_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to update Campaign Member Closed? field once Campaign Member Status is updated to a closed status.</description>
        <formula>IF(ISPICKVAL(Status, &quot;3rd+ attempt&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Account wrongly identified as Customer&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Already have card payment solution&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Already in Renewal Process&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Can&apos;t reach Decision Maker&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Company Out of Business/Non-Existent&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Credit Issues/Collections&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Customer Assessment Complete&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Customer no longer with Rogers&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Duplicate - Not Worked&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Final Email Sent&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Lead Previously Sent&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Leave it alone, they are still loading&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Line commitment info is incorrect&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Loads or pricing increase completed&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Moved address - Not in Footprint&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Bad experience)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Cash Only/No Need)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Coverage Issues)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Gone to competition)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Pricing/Cost Cutting)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Product Offer Lacking)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Product Too Complicated)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Recently re-signed)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Too busy-Not Interested)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (with Rogers/No Growth)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Not Interested&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Onboarding Completed&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Opportunity already created&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Opportunity created in previous campaign&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Opportunity Lost&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Opportunity Won&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Other&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Out of Scope (employee size / ownership)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Out of Scope (Employee size/Ownership)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Previous Opportunity Lost&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Previous Opportunity Won&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Registered&quot;), TRUE, IF(ISPICKVAL(Status, &quot;RS4B Lead Sent&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Wants one Bill&quot;), TRUE, FALSE ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Campaign Member Updated to False</fullName>
        <actions>
            <name>Update_Campaign_Member_Up_field_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(ISPICKVAL(Status, &quot;Added to Email Campaign&quot;), True, IF(ISPICKVAL(Status, &quot;Attended&quot;), True, IF(ISPICKVAL(Status, &quot;Email Sent through VR&quot;), True, IF(ISPICKVAL(Status, &quot;New&quot;), True, IF(ISPICKVAL(Status, &quot;Registered&quot;), True, False ) ) ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Campaign Member Updated to True</fullName>
        <actions>
            <name>Update_Campaign_Member_Updated_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(ISPICKVAL(Status, &quot;1st attempt&quot;), TRUE, IF(ISPICKVAL(Status, &quot;2nd attempt&quot;), TRUE, IF(ISPICKVAL(Status, &quot;3rd+ attempt&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Account wrongly identified as Customer&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Added to Email Campaign&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Already have card payment solution&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Already in Renewal Process&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Booked Meeting&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Can&apos;t reach Decision Maker&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Company Out of Business/Non-Existent&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Contact Emailed&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Contact Responded to Email&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Credit Issues/Collections&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Customer Assessment Complete&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Customer no longer with Rogers&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Duplicate - Not Worked&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Final Email Sent&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Follow up Email Sent&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Lead Previously Sent&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Leave it alone, they are still loading&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Line commitment info is incorrect&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Loads or pricing increase completed&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Moved address - In Footprint&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Moved address - Not in Footprint&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Needs Analysis&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Bad experience)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Cash Only/No Need)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Coverage Issues)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Gone to competition)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Pricing/Cost Cutting)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Product Offer Lacking)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Product Too Complicated)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Recently re-signed)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (Too busy-Not Interested)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;No Opportunity (with Rogers/No Growth)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Not Interested&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Onboarding Completed&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Onboarding not completed&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Opportunity&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Opportunity (Planning to self-serve)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Opportunity (with Rogers/future Growth)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Opportunity created&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Opportunity created in previous campaign&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Opportunity Lost&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Opportunity Won&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Other&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Out of Scope (employee size / ownership)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Out of Scope (Employee size/Ownership)&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Previous Opportunity Lost&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Previous Opportunity Won&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Proposal Review&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Registered&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Sales to action&quot;), TRUE, IF(ISPICKVAL(Status, &quot;RS4B Lead Sent&quot;), TRUE, IF(ISPICKVAL(Status, &quot;Wants one Bill&quot;), TRUE, FALSE ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update RPC Contacted field to False</fullName>
        <actions>
            <name>Update_RPC_Contacted_field_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(ISPICKVAL(Status, &quot;1st attempt&quot;), True, IF(ISPICKVAL(Status, &quot;2nd attempt&quot;), True, IF(ISPICKVAL(Status, &quot;3rd+ attempt&quot;), True, IF(ISPICKVAL(Status, &quot;Activity assigned to wrong contact&quot;), True, IF(ISPICKVAL(Status, &quot;Activity assigned to wrong rep&quot;), True, IF(ISPICKVAL(Status, &quot;Already in Renewal Process&quot;), True, IF(ISPICKVAL(Status, &quot;Can&apos;t reach Decision Maker&quot;), True, IF(ISPICKVAL(Status, &quot;Company Out of Business/Non-Existent&quot;), True, IF(ISPICKVAL(Status, &quot;Duplicate - Not Worked&quot;), True, IF(ISPICKVAL(Status, &quot;Final Email Sent&quot;), True, IF(ISPICKVAL(Status, &quot;Lead Previously Sent&quot;), True, False ) ) ) ) ) ) ) ) ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update RPC Contacted field to True</fullName>
        <actions>
            <name>Update_RPC_Contacted_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used in Campaign Members</description>
        <formula>IF(ISPICKVAL(Status, &quot;Booked Appointment&quot;), True, IF(ISPICKVAL(Status, &quot;Contact Responded to Email&quot;), True, IF(ISPICKVAL(Status, &quot;Contact Emailed&quot;), True, IF(ISPICKVAL(Status, &quot;Credit Issues/Collections&quot;), True,  IF(ISPICKVAL(Status, &quot;Follow up Email Sent&quot;), True, IF(ISPICKVAL(Status, &quot;Moved address - In Footprint&quot;), True, IF(ISPICKVAL(Status, &quot;Moved address - Not in Footprint&quot;), True, IF(ISPICKVAL(Status, &quot;Needs Analysis&quot;), True,  IF(ISPICKVAL(Status, &quot;No Opportunity&quot;), True,  IF(ISPICKVAL(Status, &quot;No Opportunity (Bad experience)&quot;), True,  IF(ISPICKVAL(Status, &quot;No Opportunity (Coverage Issues)&quot;), True,  IF(ISPICKVAL(Status, &quot;No Opportunity (Gone to competition)&quot;), True,  IF(ISPICKVAL(Status, &quot;No Opportunity (Pricing/Cost Cutting)&quot;), True,  IF(ISPICKVAL(Status, &quot;No Opportunity (Product Offer Lacking)&quot;), True,  IF(ISPICKVAL(Status, &quot;No Opportunity (Recently re-signed)&quot;), True,  IF(ISPICKVAL(Status, &quot;No Opportunity (with Rogers/No Growth)&quot;), True,  IF(ISPICKVAL(Status, &quot;Onboarding completed&quot;), True,  IF(ISPICKVAL(Status, &quot;Onboarding not completed&quot;), True,   IF(ISPICKVAL(Status, &quot;Opportunity (with Rogers/future Growth)&quot;), True,  IF(ISPICKVAL(Status, &quot;Opportunity created&quot;), True,  IF(ISPICKVAL(Status, &quot;Opportunity Lost&quot;), True,  IF(ISPICKVAL(Status, &quot;Opportunity Won&quot;), True,  IF(ISPICKVAL(Status, &quot;Opt In&quot;), True, IF(ISPICKVAL(Status, &quot;Opt Out&quot;), True, IF(ISPICKVAL(Status, &quot;Out of Scope (Employee size/Ownership)&quot;), True,  IF(ISPICKVAL(Status, &quot;Partial Opt In&quot;), True, IF(ISPICKVAL(Status, &quot;Previous Opportunity Lost&quot;), True, IF(ISPICKVAL(Status, &quot;Previous Opportunity Won&quot;), True, IF(ISPICKVAL(Status, &quot;Proposal Review&quot;), True, IF(ISPICKVAL(Status, &quot;RS4B Lead Sent&quot;), True,  IF(ISPICKVAL(Status, &quot;Wants one Bill&quot;), True,  False ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
