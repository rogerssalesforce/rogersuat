<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_EVP_team_for_IDV_request_full_fill</fullName>
        <description>Email to EVP  team for IDV request full fill</description>
        <protected>false</protected>
        <recipients>
            <field>EVP_Prime__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Various_Templates/IDV_EVPTeam_Notification</template>
    </alerts>
    <alerts>
        <fullName>IDV_Request_Completed</fullName>
        <description>IDV Request Completed</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Various_Templates/IDV_Account_Manager_Notification</template>
    </alerts>
    <alerts>
        <fullName>IDV_Request_Rejected</fullName>
        <description>IDV Request Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Various_Templates/IDV_Request_Rejected_Notification</template>
    </alerts>
    <alerts>
        <fullName>IDV_Request_With_Account_Manager</fullName>
        <description>IDV Request With Account Manager</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Various_Templates/IDV_Request_With_Account_Manager_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>IDV_Final_Approval</fullName>
        <field>Status__c</field>
        <literalValue>With Contracts Team</literalValue>
        <name>IDV Final Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IDV_Last_Approval_Date_Time</fullName>
        <field>Date_Time_of_Last_Approval__c</field>
        <formula>NOW()</formula>
        <name>IDV - Last Approval Date/Time Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IDV_Pending_For_Approval</fullName>
        <field>Status__c</field>
        <literalValue>With Director/SVP</literalValue>
        <name>IDV Pending For Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IDV_Request_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>IDV Request Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Previous_Approver</fullName>
        <field>Previous_Approver__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>Previous Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Not_require_Approval</fullName>
        <field>Status__c</field>
        <literalValue>With Contracts Team</literalValue>
        <name>Request Not require Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>IDV Approval Not Required</fullName>
        <actions>
            <name>Request_Not_require_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>IDV_Pricing_Request__c.Incremental_Share_Everything_Discount__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>IDV_Pricing_Request__c.Promotion__c</field>
            <operation>equals</operation>
            <value>None</value>
        </criteriaItems>
        <criteriaItems>
            <field>IDV_Pricing_Request__c.Conversion_Credit__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IDV Pricing Request Rejected</fullName>
        <actions>
            <name>IDV_Request_Rejected</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>IDV_Pricing_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Send email to Account Manager when status change to  Rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IDV Pricing Request With Account Manager</fullName>
        <actions>
            <name>IDV_Request_With_Account_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>IDV_Pricing_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>With Account Manager</value>
        </criteriaItems>
        <description>Send email to Account Manager when status change to With Account Manager</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IDV Send Email to Account Manager</fullName>
        <actions>
            <name>IDV_Request_Completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>IDV_Pricing_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>Send email to Account Manager when status change to Complete</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IDV Send Email to EVP Team</fullName>
        <actions>
            <name>Email_to_EVP_team_for_IDV_request_full_fill</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>IDV_Pricing_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>With EVP Team</value>
        </criteriaItems>
        <description>Notify  EVP  team when IDV request status change to &quot;With EVP Team &quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
