<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>RFP_Pre_Qualification_MIR_Email</fullName>
        <description>RFP Pre-Qualification MIR - Email</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Owner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Various_Templates/RFP_Pre_Qualification_More_Information_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>RFP_Pre_Qualification_ReSubmit_Email</fullName>
        <ccEmails>npc@rci.rogers.com</ccEmails>
        <description>RFP Pre-Qualification ReSubmit - Email</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Owner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Various_Templates/RFP_Pre_Qualification_Re_Submit_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>RFP_Pre_Qualification_Submit_Email</fullName>
        <ccEmails>npc@rci.rogers.com</ccEmails>
        <description>RFP Pre-Qualification Submit - Email</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Owner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Various_Templates/RFP_Pre_Qualification_New_Email_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>RFP_Flip_RT_MIR_To_Submitted</fullName>
        <field>RecordTypeId</field>
        <lookupValue>RFP_Submitted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RFP Flip RT MIR To Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RFP_Flip_RT_to_DD</fullName>
        <field>RecordTypeId</field>
        <lookupValue>RFP_Deal_Desk</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RFP Flip RT to DD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RFP_Flip_RT_to_DD_to_Bid</fullName>
        <field>RecordTypeId</field>
        <lookupValue>RFP_Submitted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RFP Flip RT to DD to Bid</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RFP_Flip_RT_to_MIR</fullName>
        <field>RecordTypeId</field>
        <lookupValue>RFP_More_Information</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RFP Flip RT to MIR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RFP_Flip_RT_to_Submitted</fullName>
        <field>RecordTypeId</field>
        <lookupValue>RFP_Submitted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RFP Flip RT to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RFP_Manager_Email_Fill</fullName>
        <field>Account_Owner_Manager_Email__c</field>
        <formula>Owner_Manager_Email__c</formula>
        <name>RFP Manager Email Fill</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RFP_Pre_Qualification_Closed_Records</fullName>
        <field>RecordTypeId</field>
        <lookupValue>RFP_Submitted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RFP Pre-Qualification - Closed Records</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RFP_Pre_Qualification_No_Bid_Date</fullName>
        <field>No_Bid_Decision_Date__c</field>
        <formula>Today()</formula>
        <name>RFP Pre-Qualification - No Bid Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RFP_Pre_Qualification_Submit_Date</fullName>
        <field>RFP_Pre_Qualification_Submission_Date__c</field>
        <formula>Today()</formula>
        <name>RFP Pre-Qualification - Submit Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>RFP Manager Email Fill</fullName>
        <actions>
            <name>RFP_Manager_Email_Fill</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.Status__c</field>
            <operation>equals</operation>
            <value>New,Pre-Qualification In Progress,More Information Required,Deal Desk Review,On Hold,Decided – Bid,RFP Response In Progress,Decided - No Bid (Closed),Cancelled by Customer (Closed),Completed – RFP Response Submitted (Closed),No Bid Overturned</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RFP Pre-Qualification - MIR</fullName>
        <actions>
            <name>RFP_Pre_Qualification_MIR_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.Status__c</field>
            <operation>equals</operation>
            <value>More Information Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>RFP Submitted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RFP Pre-Qualification - No Bid Date</fullName>
        <actions>
            <name>RFP_Pre_Qualification_No_Bid_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.Status__c</field>
            <operation>equals</operation>
            <value>Decided - No Bid (Closed)</value>
        </criteriaItems>
        <description>Update Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RFP Pre-Qualification - ReSubmit</fullName>
        <actions>
            <name>RFP_Pre_Qualification_ReSubmit_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.Status__c</field>
            <operation>equals</operation>
            <value>Pre-Qualification In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>RFP More Information</value>
        </criteriaItems>
        <description>Update Submission Date
Send Email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RFP Pre-Qualification - Submit</fullName>
        <actions>
            <name>RFP_Pre_Qualification_Submit_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>RFP_Pre_Qualification_Submit_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.Status__c</field>
            <operation>equals</operation>
            <value>Pre-Qualification In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>RFP New</value>
        </criteriaItems>
        <description>Update Submission Date
Send Email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RFP Pre-Qualification Closed</fullName>
        <actions>
            <name>RFP_Pre_Qualification_Closed_Records</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.Status__c</field>
            <operation>equals</operation>
            <value>Decided - No Bid (Closed),Cancelled by Customer (Closed),Completed – RFP Response Submitted (Closed)</value>
        </criteriaItems>
        <description>Update Record type to Submitted whenever the form is closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RFP Response Request- DD to Bid</fullName>
        <actions>
            <name>RFP_Flip_RT_to_DD_to_Bid</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.Status__c</field>
            <operation>equals</operation>
            <value>Decided – Bid,Decided - No Bid (Closed)</value>
        </criteriaItems>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>RFP Deal Desk</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RFP Response Request- MIR to Submitted</fullName>
        <actions>
            <name>RFP_Flip_RT_MIR_To_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.Status__c</field>
            <operation>equals</operation>
            <value>Pre-Qualification In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>RFP More Information</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RFP Response Request- More Information Required</fullName>
        <actions>
            <name>RFP_Flip_RT_to_MIR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.Status__c</field>
            <operation>equals</operation>
            <value>More Information Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>RFP Submitted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RFP Response Request- Submit to Bid Team</fullName>
        <actions>
            <name>RFP_Flip_RT_to_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.Status__c</field>
            <operation>equals</operation>
            <value>Pre-Qualification In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>RFP New</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RFP Response Request- Submit to DD</fullName>
        <actions>
            <name>RFP_Flip_RT_to_DD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.Status__c</field>
            <operation>equals</operation>
            <value>Deal Desk Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>RFP_Pre_Qualification__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>RFP Submitted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
