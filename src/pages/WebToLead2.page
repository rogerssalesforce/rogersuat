<!--/* 
* Name          : WebToLead2
* Author        : Shift CRM
* Description   : English web to lead form
*
* Maintenance History: 
* Date ------------ Name  ----  Version --- Remarks 
* 03/06/2014        Hermes      1.0         CR522: Made certain fields required
* 03/12/2014        Hermes      1.1         CR522: Phone made required
* 03/12/2014        Hermes      1.2         CR522: Javascript to copy address over
*/-->

<apex:page standardController="Lead"
           extensions="Web2LeadExtension"
           title="Contact Us" showHeader="false"
           standardStylesheets="true">
    <apex:includeScript value="{!$Resource.jQuery}" />
    <apex:includeScript value="{!$Resource.SimpleModal}" />
      <style>
  #simplemodal-overlay {background-color:#000; cursor:wait;}
  </style>
<script>
var j$ = jQuery.noConflict();

j$(document).ready(function() {
j$( "#submitButtons tr").children('td:gt(0)').remove();
j$( "#submitButtons tr td").css("text-align", "center");
});
function submitReferral(){
    var confirmed = document.getElementById('confirm').checked;
    
    if (confirmed)
        submitAF();
    else
        alert('You must accept the rules and regulations set by Rogers Business Solutions.');
        
}

<!--1.2-->
function toggleSameAddress() {
    var sameAddress = document.getElementById('j_id0:j_id37:j_id38:j_id39:sameAddress').checked;

    if(sameAddress) {
       document.getElementById('j_id0:j_id37:j_id38:j_id39:j_id59:pStreet').value = document.getElementById('j_id0:j_id37:j_id38:j_id39:hStreet').value;
       document.getElementById('j_id0:j_id37:j_id38:j_id39:j_id63:pCity').value = document.getElementById('j_id0:j_id37:j_id38:j_id39:hCity').value;
       document.getElementById('j_id0:j_id37:j_id38:j_id39:j_id67:pState').value = document.getElementById('j_id0:j_id37:j_id38:j_id39:hState').value;
       document.getElementById('j_id0:j_id37:j_id38:j_id39:j_id71:pPostal').value = document.getElementById('j_id0:j_id37:j_id38:j_id39:hPostal').value;
       document.getElementById('j_id0:j_id37:j_id38:j_id39:j_id75:pCountry').value = document.getElementById('j_id0:j_id37:j_id38:j_id39:hCountry').value;
    }
    else {
        document.getElementById('j_id0:j_id37:j_id38:j_id39:j_id59:pStreet').value = null;
        document.getElementById('j_id0:j_id37:j_id38:j_id39:j_id63:pCity').value = null;
        document.getElementById('j_id0:j_id37:j_id38:j_id39:j_id67:pState').value = null;
        document.getElementById('j_id0:j_id37:j_id38:j_id39:j_id71:pPostal').value = null;
        document.getElementById('j_id0:j_id37:j_id38:j_id39:j_id75:pCountry').value = null;
    }
}

function openModal(){
j$.modal('<iframe src="' + '{!$Resource.ReferralRules}' + '" height="450" width="830" style="border:0">', {
    closeHTML:"",
    containerCss:{
        backgroundColor:"#fff", 
        borderColor:"#fff", 
        height:450, 
        padding:0, 
        width:830
    },
    overlayClose:true
});
}

</script>

  <style>
    #simplemodal-overlay {background-color:#000;}
    #simplemodal-container {background-color:#333; border:8px solid #444; padding:12px;}
  
    .leadTab .secondaryPalette, .individualPalette .leadBlock .secondaryPalette {
    border-color:#CC0017;
    }
    
    body .bEditBlock .pbBody .pbSubheader, body .bWizardBlock .pbBody .pbSubheader {
    background-image:none;
    color: #EFEFEF;
    }
    
    .leadTab .tertiaryPalette, .individualPalette .leadBlock .tertiaryPalette, .layoutEdit .individualPalette .leadBlock .tertiaryPalette {
    background-color:#919191
    }
    
    .requiredLegend {
    color: #EFEFEF;
    }
    
    hr {
    color:#CC0017;
    }
    
    .subHeader{
        font-weight:bold;
    }
    
  </style>     
   <c:SiteHeader />  
  <apex:pageMessages />
   <apex:form >

      <apex:pageBlock title="Lead Referral"  mode="edit">        
        <apex:pageBlockSection title="Lead Information - Company" collapsible="false">
            <apex:inputField taborderhint="1" value="{!Lead.Company}"/> <apex:outputText value=""/>

        <apex:outputLabel styleClass="subHeader">Head Office Address</apex:outputLabel>
        <apex:outputLabel styleClass="subHeader">Primary Contact</apex:outputLabel>
                  <apex:inputField taborderhint="2" required="true" value="{!Lead.Street}" id="hStreet"/><apex:inputField taborderhint="7" value="{!Lead.Salutation}"/>
                  <apex:inputField taborderhint="3" required="true" value="{!Lead.City}" id="hCity"/><apex:inputField taborderhint="8" required="false" value="{!Lead.FirstName}"/>
                  <apex:inputField taborderhint="4" required="true" value="{!Lead.State}" id="hState"/><apex:inputField taborderhint="9" required="true" value="{!Lead.LastName}"/> <!-- 1.0 Last name is required -->
                  <apex:inputField taborderhint="5" required="true" value="{!Lead.PostalCode}" id="hPostal"/><apex:inputField taborderhint="10" required="false" value="{!Lead.Title}"/>
                  <apex:inputField taborderhint="6" required="true" value="{!Lead.Country}" id="hCountry"/><apex:inputField taborderhint="11" required="false" value="{!Lead.Email}"/>
                 <apex:outputText value=""/><apex:inputField taborderhint="12" required="true" value="{!Lead.Phone}"/>
                
        <apex:outputText value=""/><apex:outputText value=""/>
        <apex:outputLabel styleClass="subHeader">Site Office Address</apex:outputLabel><apex:inputField taborderhint="13" style="width:300px" value="{!Lead.Notes__c}"/>
        <apex:inputField taborderhint="14" value="{!Lead.Same_As_Head_Office__c}" id="sameAddress" onchange="javascript:toggleSameAddress();"/><apex:outputText value=""/>
        <!-- 1.0 - Address fields made required -->
        <apex:pageBlockSectionItem ><apex:outputLabel >Street</apex:outputLabel><apex:inputField taborderhint="15" required="true" value="{!Lead.Physical_Street__c}" id="pStreet"/></apex:pageBlockSectionItem><apex:outputText value=""/>
        <apex:pageBlockSectionItem ><apex:outputLabel >City</apex:outputLabel><apex:inputField taborderhint="16" required="true" value="{!Lead.Physical_City__c}" id="pCity"/></apex:pageBlockSectionItem><apex:outputText value=""/>
        <apex:pageBlockSectionItem ><apex:outputLabel >State/Province</apex:outputLabel><apex:inputField taborderhint="17" required="true" value="{!Lead.Physical_State_Province__c}" id="pState"/></apex:pageBlockSectionItem><apex:outputText value=""/>
        <apex:pageBlockSectionItem ><apex:outputLabel >Postal Code</apex:outputLabel><apex:inputField taborderhint="18" required="true" value="{!Lead.Physical_Postal_Code__c}" id="pPostal"/></apex:pageBlockSectionItem><apex:outputText value=""/>
        <apex:pageBlockSectionItem ><apex:outputLabel >Country</apex:outputLabel><apex:inputField taborderhint="19" required="true" value="{!Lead.Physical_Country__c}" id="pCountry"/></apex:pageBlockSectionItem><apex:outputText value=""/>
                 
        </apex:pageBlockSection>
        
        <apex:pageBlockSection title="Lead Information - Personal"
                               collapsible="false">
             <apex:outputLabel styleClass="subHeader">Personal Information</apex:outputLabel><apex:outputText value=""/>
         <apex:inputField value="{!Lead.Referral_Salutation__c}"/><apex:outputText value=""/>
         <apex:inputField required="true" value="{!Lead.Referral_Agent_First_Name__c}"/><apex:outputText value=""/>
         <apex:inputField required="true" value="{!Lead.Referral_Agent_Last_Name__c}"/><apex:outputText value=""/>
         <apex:inputField required="true" value="{!Lead.Referral_Agent_Phone__c}"/><apex:outputText value=""/>    
         <apex:inputField required="true" value="{!Lead.Referral_Agent_Email__c}"/><apex:outputText value=""/>
         <apex:inputField required="true" value="{!Lead.Referral_Agent_ID__c}"/><apex:outputText value=""/>
         <apex:inputField required="true" value="{!Lead.Lead_Rep_Type__c}"/><apex:outputText value=""/>
         <apex:inputField value="{!Lead.RBS_Sales_Rep_optional__c}"/><apex:outputText value=""/>
        </apex:pageBlockSection>
        <br/>
        <hr width="97%"/>
        <p style="text-align:center;"><input type="checkbox" id="confirm" name="confirm"/>By submitting this form, you agree to the <a href="#" onclick="javascript:openModal();">rules and regulations</a> set by Rogers Business Solutions.</p>
                     
        <div id="submitButtons">
         <apex:pageBlockSection columns="2">
                <apex:outputPanel style="text-align:center;width:70px;display:inline;" id="submitButton" onclick="javascript:submitReferral();" styleClass="btn" title="Submits the employee referral form.">Submit</apex:outputPanel>
            </apex:pageBlockSection>
            </div>

             <br/>
           </apex:pageBlock>
           
            <apex:actionFunction name="submitAF" action="{!saveLead}" />
   </apex:form>
     
   <c:SiteFooter_RBS />

</apex:page>