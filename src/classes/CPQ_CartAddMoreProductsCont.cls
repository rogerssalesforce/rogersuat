/**********************************************************************************************************
Class Name      : CPQ_CartAddMoreProductsCont
Description     : This class is used as a controller class for CPQ_CartAddMoreProducts
                  page.
Created By      : Deepika Rawat 
Created Date    : 29-Oct-15  
Modification Log:
-----------------------------------------------------------------------------------------------------------
Developer                   Date                   Description
-----------------------------------------------------------------------------------------------------------
Deepika Rawat             29-Oct-15           Original Version 
Deepika Rawat             19-Jan-16           Updated method addMoreProducts to allow SE to add 
                                              more products when Status is 'Soln Under Review'. TKT 165.                                   
**********************************************************************************************************/
public with sharing class CPQ_CartAddMoreProductsCont {
    public String sConfigRequestId {get;set;}
    public String sCartId {get;set;}
    public String sProposalId;
    public List<Apttus_Proposal__Proposal__c> lstProposal;
    Set<String> setAdminProfiles = new Set<String>();
    Set<String> setAEProfiles = new Set<String>();
    Set<String> setSEProfiles = new Set<String>();
    Set<String> setDDProfiles = new Set<String>();
    String sUserProfileName;
    String sflow;

    /*********************************************************************************
    Method Name    : CPQ_CartAddMoreProductsCont
    Description    : Constructor method
    Return Type    : 
    Parameter      : 
    *********************************************************************************/
    public CPQ_CartAddMoreProductsCont(){
        sCartId = ApexPages.currentPage().getParameters().get('id'); 
        sConfigRequestId = ApexPages.currentPage().getParameters().get('configrequestid');  
        sflow  = ApexPages.currentPage().getParameters().get('flow');
        Apttus_Config2__ProductConfiguration__c oProdConfig = [Select id,Apttus_Config2__BusinessObjectId__c from Apttus_Config2__ProductConfiguration__c where id =: sCartId];       
        sProposalId = oProdConfig.Apttus_Config2__BusinessObjectId__c;  
        lstProposal= [select id, RecordTypeId , Apttus_Proposal__Approval_Stage__c from Apttus_Proposal__Proposal__c where id=:sProposalId];
        sUserProfileName= [Select name from profile where id=:userinfo.getProfileId()].name;
        CPQ_Settings__c cpqSetting = CPQ_Settings__c.getOrgDefaults();
        List<String> lstAEProfiles = cpqSetting.RogersAE__c.Split(';');
        List<String> lstAEProfiles2 = cpqSetting.RogersAE_2__c.Split(';');
        List<String> lstSEProfiles = cpqSetting.RogersSE__c.Split(';');
        List<String> lstDDProfiles = cpqSetting.RogersDealDesk__c.Split(';');
        List<String> lstSysAdminProfiles = cpqSetting.System_Administrator__c.Split(';');
        setAEProfiles.addAll(lstAEProfiles);
        setAEProfiles.addAll(lstAEProfiles2);
        setSEProfiles.addAll(lstSEProfiles);
        setDDProfiles.addAll(lstDDProfiles);
        setAdminProfiles.addAll(lstSysAdminProfiles);
    }
    /*********************************************************************************
    Method Name    : addMoreProducts
    Description    : Based on Quote lifecycle displays Catalog page to user
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference addMoreProducts(){
        String sQStage = lstProposal[0].Apttus_Proposal__Approval_Stage__c;
        // Display Catalog page 1. to Profile AE, SE or DD if Status is 'Draft',
        // 2. to Profile DD and SE if Status is 'Soln under Review' or
        // 3. to Profile DD if Status is 'In Review' else display error message
        if(lstProposal.size()>0 && (setAdminProfiles.contains(sUserProfileName) || ((setAEProfiles.contains(sUserProfileName) ||setSEProfiles.contains(sUserProfileName) || setDDProfiles.contains(sUserProfileName)) && sQStage == 'Draft') || ((setDDProfiles.contains(sUserProfileName) || setSEProfiles.contains(sUserProfileName) )&& sQStage == 'Solution Under Review') || (setDDProfiles.contains(sUserProfileName) && sQStage == 'In Review'))){
           PageReference refreshPage = new PageReference(Site.getPathPrefix()+'/apex/apttus_config2__Cart?configRequestId='+sConfigRequestId+'&flow='+sflow+'&id='+sCartId+'#catalog');
           refreshPage.setRedirect(true);
           return refreshPage;
        }
        else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CPQ_addMoreProductsErrorMsg));
            return null;
        }  
    }
    /*********************************************************************************
    Method Name    : backToCart
    Description    : Returns back to Cart
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference backToCart(){
        PageReference refreshPage = new PageReference(Site.getPathPrefix()+'/apex/apttus_config2__Cart?configRequestId='+sConfigRequestId+'&flow='+sflow+'&id='+sCartId+'#cart');
        return refreshPage;
    }
}