/**********************************************************************************************************
Class Name      : CPQ_OpportunityProductHelper
Description     : This class is used as a helper class for CPQ_OpportunityProduct
Created By      : Deepika Rawat   
Created Date    : 24-Nov-2015 
Modification Log:
----------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
-----------------------------------------------------------------------------------------------------------  
Deepika Rawat            24-Nov-2015            Initial version. Added method 'calculateTCV' to calculate 
                                                expectec TCV on Opprtunity
************************************************************************************************************/
public with sharing class CPQ_OpportunityProductHelper{
    /*********************************************************************************
    Method Name    : calculateTCV
    Description    : This method calculates TCV values based on Quote line items
    Return Type    : Void
    Parameter      : List<OpportunityLineItem>
    *********************************************************************************/
    public static void  calculateTCV(List<OpportunityLineItem> lstOpplineItems){
        Set<Id> setOppIds = new Set<Id>();
        List<Opportunity> lstUpdateOppportunity = new List<Opportunity>();
        Map<Id, List<Apttus_Proposal__Proposal_Line_Item__c>> mapOppQuoteLineTem = new Map<Id, List<Apttus_Proposal__Proposal_Line_Item__c>>();
        List<RecordType> lstOppQualified = [Select Id,DeveloperName From RecordType  Where SobjectType = 'Opportunity' and DeveloperName ='Rogers_EBU_Qualified']; 
        //Get Opportunity Ids
        for(OpportunityLineItem oLineItemOpp : lstOpplineItems){
            setOppIds.add(oLineItemOpp.OpportunityID);
        }
        //Get Opportunity records
        Map<id, Opportunity> mapOpportunity = new Map<id, Opportunity>([select Unified_Comm_Collaboration_Expected__c, IoT_Applications_Expected__c, Data_Centre_Cloud_Expected__c, Wireless_Expected__c, Wireline_Fixed_Access_Expected__c, Security_Expected__c, Services_Expected__c from Opportunity where id in:setOppIds]);
        //Get related Primary quote
        Map<id, Apttus_Proposal__Proposal__c> mapPrimaryQuotes = new Map<id, Apttus_Proposal__Proposal__c>([select id from Apttus_Proposal__Proposal__c where Apttus_Proposal__Opportunity__c in :setOppIds and Apttus_Proposal__Primary__c=true ]);
        //Get Quote Line Items of the primary Quotes
        if(mapPrimaryQuotes!=null){
            List<Apttus_Proposal__Proposal_Line_Item__c> lstQouteLineItems = [select id, Apttus_QPConfig__PriceType__c, Apttus_QPConfig__NetPrice__c, Apttus_QPConfig__Term__c, Apttus_QPConfig__Quantity2__c, Apttus_Proposal__Proposal__c, Apttus_QPConfig__DerivedFromId__r.ProductPillar__c,  Apttus_QPConfig__DerivedFromId__r.IsCustomProduct__c, Apttus_Proposal__Proposal__r.Apttus_Proposal__Opportunity__c, Apttus_Proposal__Product__r.Family  from Apttus_Proposal__Proposal_Line_Item__c where Apttus_Proposal__Proposal__c in:mapPrimaryQuotes.keyset()];
            List<Apttus_Proposal__Proposal_Line_Item__c> lstItem;
            for(Apttus_Proposal__Proposal_Line_Item__c oLineitemQuote :lstQouteLineItems){
                Id oppId = Id.valueof(oLineitemQuote.Apttus_Proposal__Proposal__r.Apttus_Proposal__Opportunity__c);
                lstItem = mapOppQuoteLineTem.get(oppId);
                if(lstItem==null)
                    lstItem= new List<Apttus_Proposal__Proposal_Line_Item__c>();
                lstItem.add(oLineitemQuote);
                mapOppQuoteLineTem.put(oppId, lstItem);
            }
        }
        if(mapOppQuoteLineTem!=null){
            Decimal iUCCExpected;
            Decimal iIOTExpected;
            Decimal iDataCenterExpected;
            Decimal iWirelessExpected;
            Decimal iWirelineExpected;
            Decimal iSecurityExpected;
            Decimal iServicesExpected;
            //Loop through the mapOppQuoteLineTem and calculate TCV fields on Opp based on Quote Line Items on quote.
            for(Id oppID : mapOppQuoteLineTem.keyset()){
                iUCCExpected=0;
                iIOTExpected=0;
                iDataCenterExpected=0;
                iWirelessExpected=0;
                iWirelineExpected=0;
                iSecurityExpected=0;
                iServicesExpected=0;
                //Get list of quote Line ites related to Opportunity
                List<Apttus_Proposal__Proposal_Line_Item__c> lstQuoteLineItem = mapOppQuoteLineTem.get(oppID);
                //Get Opportunity record from Opportunity Id
                Opportunity oOpportunity = mapOpportunity.get(oppID);
                if(lstQuoteLineItem!=null && lstQuoteLineItem.size()>0){
                    for(Apttus_Proposal__Proposal_Line_Item__c item: lstQuoteLineItem){
                        //Calculate items of type Unified Comm. & Collaboration
                        if(item.Apttus_Proposal__Product__r.Family=='Unified Communication & Collaboration' ){
                            iUCCExpected = iUCCExpected + item.Apttus_QPConfig__NetPrice__c ;
                        }
                        //Calculate items of type IoT Applications
                        if(item.Apttus_Proposal__Product__r.Family=='IoT Applications' ){
                            iIOTExpected = iIOTExpected + item.Apttus_QPConfig__NetPrice__c;
                        }
                        //Calculate items of type Data Centre & Cloud
                        if(item.Apttus_Proposal__Product__r.Family=='Data Centres & Cloud'){
                            iDataCenterExpected = iDataCenterExpected + item.Apttus_QPConfig__NetPrice__c;
                        }
                        //Calculate items of type Wireless
                        if(item.Apttus_Proposal__Product__r.Family=='Enterprise Grade Networks - Wireless'){
                           iWirelessExpected = iWirelessExpected + item.Apttus_QPConfig__NetPrice__c;
                        }
                        //Calculate items of type Wireline / Fixed Access
                        if(item.Apttus_Proposal__Product__r.Family=='Enterprise Grade Networks - Wireline'){
                             iWirelineExpected = iWirelineExpected + item.Apttus_QPConfig__NetPrice__c ;
                        }
                        //Calculate items of type Security
                        if(item.Apttus_Proposal__Product__r.Family=='Security'){
                             iSecurityExpected = iSecurityExpected + item.Apttus_QPConfig__NetPrice__c;
                        }
                        //Calculate items of type Services
                        if(item.Apttus_Proposal__Product__r.Family=='Services' ){
                            iServicesExpected = iServicesExpected + item.Apttus_QPConfig__NetPrice__c;
                        }
                    }
                    
                    //Populate TCV fields on Opportunity
                    if(iUCCExpected!=0 || iIOTExpected!=0 || iDataCenterExpected!=0 || iWirelessExpected!=0 || iWirelineExpected!=0 || iSecurityExpected!=0 || iServicesExpected!=0){
                        oOpportunity.Unified_Comm_Collaboration_Expected__c = iUCCExpected;
                        oOpportunity.IoT_Applications_Expected__c = iIOTExpected;
                        oOpportunity.Data_Centre_Cloud_Expected__c = iDataCenterExpected;
                        oOpportunity.Wireless_Expected__c = iWirelessExpected;
                        oOpportunity.Wireline_Fixed_Access_Expected__c = iWirelineExpected;
                        oOpportunity.Security_Expected__c = iSecurityExpected;
                        oOpportunity.Services_Expected__c = iServicesExpected;
                        if(lstOppQualified!=null)
                            oOpportunity.RecordTypeId = lstOppQualified[0].id;
                        lstUpdateOppportunity.add(oOpportunity);
                    }
                } 
            }
        }
        //Update Opppotunity TCV Fields
        if(lstUpdateOppportunity!=null && lstUpdateOppportunity.size()>0){
            update lstUpdateOppportunity;
        }
    }
}