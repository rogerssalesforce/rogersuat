/*
 *
 * @ Name : APTSCU_changeLocationClass
 * @ Author : Sharanya 
 * @ CreatedDate : 11-Sept-2015
 * @ Description : To get selected location on click of change location button on catalogue and populate it on product configuration
 * @ UsedBy : VF - APTSCU_changeLocationVF
 * @ ForDemo : CDK Global and Telecom Demo
 ***************************************************************************************************************************************
 * @ ModifiedBy : 
 * @ ModifiedDate : 
 * @ ChangeDescription : 
 * @ ForDemo : 
 *
*/ 
public class APTSCU_changeLocationClass{
    public Apttus_Proposal__Proposal__c proposal {get;set;}
    public string proposalId {get;set;}
    public Apttus_Config2__ProductConfiguration__c config;
    //public Apttus_Config2__ProductConfiguration__c record {get; set;}
    public string proConfigId {get;set;}
    public Account acc;
    public String previousURL {get; set;}
    private ApexPages.StandardController controller {get; set;}
    //public list<Account> accList = new list<Account>();
    public list<Apttus_Config2__AccountLocation__c> locationsList {get;set;}
    public list<locationWrap> locationsWrapList {get;set;}
    
    public APTSCU_changeLocationClass(ApexPages.standardController controller){
        previousURL = ApexPages.CurrentPage().getHeaders().get('Referer');
        
        //previousURL  = ApexPages.currentPage().getHeaders().get('Host') + ApexPages.currentPage().getUrl();
        
        system.debug('@@@ previousURL  @@@'+ previousURL );
        integer startofConfigId = previousURL.indexOf('&id=');
        //system.debug('start of config id'+startofConfigId );
        string proCid= previousURL.substring(startofConfigId + 4 ,startofConfigId + 22 - 3);
        system.debug('start of config id:'+proCid);
        config = [SELECT id,Apttus_Config2__LocationId__c,Apttus_QPConfig__Proposald__c FROM Apttus_Config2__ProductConfiguration__c WHERE id=:proCid];
        locationsList = new list<Apttus_Config2__AccountLocation__c>();
        proposal = [SELECT id,Apttus_Proposal__Account__c FROM Apttus_Proposal__Proposal__c WHERE id=:config.Apttus_QPConfig__Proposald__c];
        system.debug('##proposal:'+proposal);
        locationsList = [SELECT id,Name,APTSCU_Account_Name__c,Apttus_Config2__Type__c,Apttus_Config2__Street__c,Apttus_Config2__City__c,Apttus_Config2__State__c,Apttus_Config2__Country__c from Apttus_Config2__AccountLocation__c WHERE Apttus_Config2__AccountId__c=:proposal.Apttus_Proposal__Account__c];
        //system.debug('**locationsList :'+locationsList);
        locationsWrapList = new list<locationWrap>();
        boolean hasSelectedVal = false;
        for(Apttus_Config2__AccountLocation__c loca : locationsList){
            
            
            
            if(loca.id==config.Apttus_Config2__LocationId__c){
                locationsWrapList.add(new locationWrap(loca,true));
                hasSelectedVal = true;
                
            }else{
                locationsWrapList.add(new locationWrap(loca,false));
            }
            
            
        }
        Apttus_Config2__AccountLocation__c noneLoc  = new Apttus_Config2__AccountLocation__c();
        noneLoc.Name='None';
        //noneLoc.id='a2xi0000000NKB1';
        noneLoc.Apttus_Config2__AccountId__c = proposal.Apttus_Proposal__Account__c;
        
        if(hasSelectedVal)
             locationsWrapList.add(new locationWrap(noneLoc,false));
        else
             locationsWrapList.add(new locationWrap(noneLoc,true));
        
        
        system.debug('@@ locationsWrapList @@'+locationsWrapList);
    } 
    
    public class locationWrap
    {
        public Apttus_Config2__AccountLocation__c location {get;set;}
        public Boolean selected {get;set;}
        
        public locationWrap(Apttus_Config2__AccountLocation__c loc,boolean selected)
        {
            this.location=loc;
            this.selected=selected;
            system.debug(location +'###' +loc);
            system.debug(location +' selected ### ' +selected);
            //system.debug();
        }
     }
     
     public PageReference Validate(){
     
         string locationid= ApexPages.CurrentPage().getParameters().get('selected');
         system.debug('@@@ locationid @@'+ locationid);
         if(locationid != null  && locationid !=''){
             config.Apttus_Config2__LocationId__c = locationid;
             //update config;
         }else{
             
             config.Apttus_Config2__LocationId__c = null;
             //update config;
         }
         update config;
         return null;//new PageReference(previousURL); 
     }
}