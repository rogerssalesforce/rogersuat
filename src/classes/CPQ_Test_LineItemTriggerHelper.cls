/*********************************************************************************
Class Name      : CPQ_Test_LineItemTriggerHelper
Description     : This class a test class for CPQ_LineItemTriggerHelper. 
Created By      : Deepika Rawat
Created Date    : 04-Nov-15  
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Deepika Rawat              24-Sep-15             Original version
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest (seeAllData=False) 
private class CPQ_Test_LineItemTriggerHelper{
    static User TestAEUser;
    static User TestDDUser;
    static Account oAccount;
    static product2 oProduct;
    static product2 oCustomIOTProduct;
    static Apttus_Config2__PriceListItem__c oCustomProductPriceListItem; 
    static Opportunity oOpportunity;
    static Apttus_Proposal__Proposal__c oQuote;
    static Attachment attachmentQuote;
    static Apttus_Config2__PriceList__c oPriceList;
    static Apttus_Config2__PriceListItem__c oPriceListItem;
    static ID prodConfigID;
    static Pricebook2 oPricebook;
    static Pricebook2 oPriceBookCustom;
    static List<Apttus_Config2__ProductConfiguration__c> lstProductConfig ;
    private static CPQ_Settings__c cs;
      /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testMethod void createTestData() {
        //Create custom Pricebook 
        oPriceBookCustom = CPQ_Test_Setup.newPriceBook('CPQ Temp PriceBook');
        insert oPriceBookCustom;
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        cs  = CPQ_Settings__c.getOrgDefaults();
        cs.DefaultOpportunityRecordType__c = 'Rogers EBU - New';
        upsert cs;
        //Create newPriceBook
        oPricebook = CPQ_Test_Setup.newPriceBook('Test Price Book');
        insert oPricebook;
        //Create Custom Product
        oCustomIOTProduct = CPQ_Test_Setup.newProduct();
        oCustomIOTProduct.ProductCode = 'IoTCustomProduct';
        oCustomIOTProduct.Name = 'IoT Custom Product';
        oCustomIOTProduct.Family = 'IoT Applications';
        insert oCustomIOTProduct;
        //Create PriceList
        oPriceList = CPQ_Test_Setup.newPriceList();
        insert oPriceList ;     
        // Create Product
        oProduct = CPQ_Test_Setup.newProduct();
        oProduct.Family = 'Wireless - Voice';
        insert oProduct;
        //Create Pricelist item
        oPriceListItem = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oProduct.ID);
        insert oPriceListItem;
        oCustomProductPriceListItem = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oCustomIOTProduct.ID);
        insert oCustomProductPriceListItem;
    }
     /*********************************************************************************
    Method Name    : testRedirectConfigureFlowDraft
    Description    : Test redirect flow as AE with Quote stage = draft
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testRedirectConfigureFlowDraft(){
        //Create AE test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');         
        
            createTestData();
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
            insert oQuote;
            //Product Configuration
            prodConfigID = CPQ_Test_Setup.newProdictConfiguration(oQuote);
            //Create line items
            CPQ_Test_Setup.createLineItem(prodConfigID,oProduct.id,oPriceListItem.id);
            oQuote.Apttus_Proposal__Approval_Stage__c = 'Draft';
            update oQuote;
            test.startTest();
            List<Apttus_Config2__LineItem__c > lstLineItem = [Select id from Apttus_Config2__LineItem__c  where Apttus_Config2__ConfigurationId__c =:prodConfigID ];
            Integer iLineItems = lstLineItem.size();
            if(lstLineItem!=null && lstLineItem.size()>0){
                delete lstLineItem[0];
                List<Apttus_Config2__LineItem__c > lstLineItem2 = new List<Apttus_Config2__LineItem__c >();
                lstLineItem2 = [Select id from Apttus_Config2__LineItem__c  where Apttus_Config2__ConfigurationId__c =:prodConfigID ];
                system.assert(lstLineItem2.size()< iLineItems);
            }    
            test.stopTest();
        
    }
     /*********************************************************************************
    Method Name    : testRedirectConfigureFlowSolnUnderReview
    Description    : Test redirect flow as AE with Quote stage = 'Solution Under Review'
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testRedirectConfigureFlowSolnUnderReview(){
        //Create AE test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');         
        system.runAs(TestAEUser){
            createTestData();
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
            insert oQuote;
            //Product Configuration
            prodConfigID = CPQ_Test_Setup.newProdictConfiguration(oQuote);
            //Create line items
            CPQ_Test_Setup.createLineItem(prodConfigID,oProduct.id,oPriceListItem.id);
            oQuote.Apttus_Proposal__Approval_Stage__c = 'Solution Under Review';
            update oQuote;
            test.startTest();
            List<Apttus_Config2__LineItem__c > lstLineItem = [Select id from Apttus_Config2__LineItem__c  where Apttus_Config2__ConfigurationId__c =:prodConfigID ];
            Integer iLineItems = lstLineItem.size();
            if(lstLineItem!=null && lstLineItem.size()>0){
                try{
                delete lstLineItem[0];
                }catch(exception e){
                
                List<Apttus_Config2__LineItem__c > lstLineItem2 = new List<Apttus_Config2__LineItem__c >();
                lstLineItem2 = [Select id from Apttus_Config2__LineItem__c  where Apttus_Config2__ConfigurationId__c =:prodConfigID ];
                system.assert(lstLineItem2.size() ==iLineItems);
                }
            }    
            test.stopTest();
        }
    }
     /*********************************************************************************
    Method Name    : testmakeFromCustomProductFlagFalse
    Description    : Test method makeFromCustomProductFlagFalse
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testmakeFromCustomProductFlagFalse(){
        //Create AE test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');         
        createTestData();
        //Create Account
        oAccount = CPQ_Test_Setup.newAccount('Test Account');
        insert oAccount;
        //Create Opportunity
        oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
        insert oOpportunity;
        //Create Quote
        oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
        insert oQuote;
        //Product Configuration
        prodConfigID = CPQ_Test_Setup.newProdictConfiguration(oQuote);
        //Create line items
        CPQ_Test_Setup.createLineItem(prodConfigID,oProduct.id,oPriceListItem.id);
        oQuote.Apttus_Proposal__Approval_Stage__c = 'Draft';
        update oQuote;
        String sConfigRequestId = [select id from Apttus_Config2__TempObject__c where Apttus_Config2__ConfigurationId__c=:prodConfigID].id;
        test.startTest();
        //First Test add Product
        ApexPages.currentPage().getParameters().put('id', prodConfigID);
        ApexPages.currentPage().getParameters().put('configrequestid', sConfigRequestId);
        ApexPages.currentPage().getParameters().put('sFlow', 'ngFlow');
        CPQ_AddCustomProductsController obj = new CPQ_AddCustomProductsController();
        obj.getOtherProducts();
        obj.getChargeType();
        obj.getCOPTeam();
        obj.oLineItem.ProductPillar__c='IoT Applications';
        obj.oLineItem.LineofBusiness__c='IoT / Applications';
        obj.oLineItem.CustomProductName__c='Test Prod Name';
        obj.oLineItem.Apttus_Config2__Quantity__c=1;
        obj.oLineItem.Apttus_Config2__BasePriceOverride__c=200;
        obj.addProduct();
        //Test edit
        List<Apttus_Config2__LineItem__c> lstOtherLineItems = [select id, CustomProductName__c from Apttus_Config2__LineItem__c where CustomProductName__c='Test Prod Name' ];
        ApexPages.currentPage().getParameters().put('id', prodConfigID);
        ApexPages.currentPage().getParameters().put('lineItemId', lstOtherLineItems[0].id);
        CPQ_AddCustomProductsController obj2 = new CPQ_AddCustomProductsController();
        obj2.oLineItem.CustomProductName__c='Test Prod Name2';
        obj2.addProduct();
        List<Apttus_Config2__LineItem__c> lstOtherLineItems2 = [select id, CustomProductName__c from Apttus_Config2__LineItem__c where CustomProductName__c='Test Prod Name2' ];
        System.Assert(lstOtherLineItems2!=null);
        test.stopTest();
    }
}