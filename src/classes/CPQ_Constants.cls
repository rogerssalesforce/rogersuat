/*********************************************************************************
Class Name      : CPQ_Constants
Description     : Constants class
Created By      : Alina Balan
Created Date    : 19-November-2015
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Alina Balan          18-November-2015             Original version
*********************************************************************************/

public with sharing class CPQ_Constants {
    //SitesController
    public static final String INT_TRUE_VALUE = '1';
    public static final String ASC_SORT_DIR = 'asc';
    public static final String DESC_SORT_DIR = 'desc';
    public static final String EDIT = 'edit';
    public static final String UTF8 = 'UTF-8';
    public static final String STRING_TRUE = 'true';
    public static final String STRING_FALSE = 'false';
    
    public static final String AGREEMENT_STATUS_CUSTOMER_SIGNATURES = 'Customer Signatures';
    public static final String AGREEMENT_STATUS_OTHER_PARTY_SIGNATURES = 'Other Party Signatures';
    public static final String AGREEMENT_STATUS_IN_SIGNATURES = 'In Signatures';
    public static final String AGREEMENT_STATUS_FULLY_SIGNED = 'Fully Signed';
    public static final String AGREEMENT_STATUS_CUSTOMER_SIGNATURES_RECEIVED = 'Customer Signatures Received';
    public static final String AGREEMENT_STATUS_INTERNAL_SIGNATURES = 'Internal Signatures';
    public static final String AGREEMENT_STATUS_READY_FOR_SIGNATURES = 'Ready for Signatures';
    public static final String QUOTE_STATUS_APPROVAL_REQUIRED = 'Approval Required';
}