/*Class Name :BIS_ActivitySummarySchedular.
 *Description : .
 *Created By : Rajiv Gangra.
 *Created Date :08/01/2014.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
global class BIS_ActivitySummarySchedular implements schedulable{

    global void execute(SchedulableContext ctx) {
        integer SFCheckPoint = Integer.ValueOf(Static_Data_Utilities__c.getInstance('BIS Activity Update Check').Value__c);
        //date dT1 = date.newInstance(system.Today().AddMonths(-SFCheckPoint+1).year(),system.Today().AddMonths(-SFCheckPoint+1).month(),1);
        BIS_ActivitySummary_batch TaskObj = new BIS_ActivitySummary_batch();
        TaskObj.dT1=date.newInstance(system.Today().AddMonths(-SFCheckPoint+1).year(),system.Today().AddMonths(-SFCheckPoint+1).month(),1);
        TaskObj.CronId = ctx.getTriggerId();
        Boolean delRec=False;
        if(Static_Data_Utilities__c.getInstance('Activity Summary Cleaned').Value__c=='True'){
            if(!Test.isRunningTest()){
                TaskObj.Query = 'select CreatedById,OwnerMonthType__c,RPC_Call__c,CreatedDate,Id,OwnerId,Owner.FirstName,Owner.LastName,LastModifiedDate,Activity_Type__c,Activity_Date__c,Deleted__c FROM BIS_Activity_Tracking__c where Activity_Date__c >=:dT1 AND Deleted__c=:delRec';
            }else{
                TaskObj.Query = 'select CreatedById,OwnerMonthType__c,RPC_Call__c,CreatedDate,Id,OwnerId,Owner.FirstName,Owner.LastName,LastModifiedDate,Activity_Type__c,Activity_Date__c,Deleted__c FROM BIS_Activity_Tracking__c LIMIT 50';
            }
        }else{
            if(!Test.isRunningTest()){
                TaskObj.Query = 'select CreatedById,OwnerMonthType__c,CreatedDate,Id,Start_of_Activity_Month__c,User_Channel__c  FROM Activity_Summary__c where Start_of_Activity_Month__c >=:dT1 AND isDeleted=:delRec AND User_Channel__c IN:newLstBU';
            }else{
                TaskObj.Query = 'select CreatedById,OwnerMonthType__c,CreatedDate,Id,Start_of_Activity_Month__c FROM Activity_Summary__c LIMIT 11';
            }
        }
        ID ActivitySummaryBatchProcessId = Database.executeBatch(TaskObj,200);
    }
}