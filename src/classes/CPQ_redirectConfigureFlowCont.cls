/***************************************************************************************
Class Name      : CPQ_redirectConfigureFlowCont
Description     : This class is used as a controller class for CPQ_redirectConfigureFlow
                  page.
Created By      : Deepika Rawat 
Created Date    : 29-Oct-15  
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Deepika Rawat             29-Oct-15              Original Version 
*****************************************************************************************/
public with sharing class CPQ_redirectConfigureFlowCont {
    public String sProposalId;
    public List<Apttus_Proposal__Proposal__c> lstProposal;
    Set<String> setAdminProfiles = new Set<String>();
    Set<String> setAEProfiles = new Set<String>();
    Set<String> setSEProfiles = new Set<String>();
    Set<String> setDDProfiles = new Set<String>();
    String sUserProfileName;
    /*********************************************************************************
    Method Name    : CPQ_redirectConfigureFlowCont
    Description    : Constructor method
    Return Type    : 
    Parameter      : 
    *********************************************************************************/
    public CPQ_redirectConfigureFlowCont(){
        sProposalId = ApexPages.currentPage().getParameters().get('id'); 
        lstProposal= [select id, RecordTypeId , Apttus_Proposal__Approval_Stage__c from Apttus_Proposal__Proposal__c where id=:sProposalId];
        sUserProfileName= [Select name from profile where id=:userinfo.getProfileId()].name;
        CPQ_Settings__c cpqSetting = CPQ_Settings__c.getOrgDefaults();
        List<String> lstAEProfiles = cpqSetting.RogersAE__c.Split(';');
        List<String> lstAEProfiles2 = cpqSetting.RogersAE_2__c.Split(';');
        List<String> lstSEProfiles = cpqSetting.RogersSE__c.Split(';');
        List<String> lstDDProfiles = cpqSetting.RogersDealDesk__c.Split(';');
        List<String> lstSysAdminProfiles = cpqSetting.System_Administrator__c.Split(';');
        setAEProfiles.addAll(lstAEProfiles);
        setAEProfiles.addAll(lstAEProfiles2);
        setSEProfiles.addAll(lstSEProfiles);
        setDDProfiles.addAll(lstDDProfiles);
        setAdminProfiles.addAll(lstSysAdminProfiles );
    }
     /*********************************************************************************
    Method Name    : openConfigureFlow
    Description    : This method opens Cart is appropriate Cart flow
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference openConfigureFlow(){
        String sQStage = lstProposal[0].Apttus_Proposal__Approval_Stage__c;
        // If Quote is in 'Draft' and proflie is DD, SE or AE open "ngFlow",
        // Else If Quote is in 'Soln Under Review' or 'Approval required' and proflie is DD, SE or AE open "repriceFlow",
        // Else If Quote is in 'In Review' and proflie is DD open "finalizeFlow"
        // Else open "onlyCloseFlow"
        if(sQStage == 'Draft' && (setAdminProfiles.contains(sUserProfileName) || setAEProfiles.contains(sUserProfileName) ||setSEProfiles.contains(sUserProfileName) || setDDProfiles.contains(sUserProfileName))){
            PageReference refreshPage = new PageReference(Site.getPathPrefix()+'/apex/Apttus_QPConfig__ProposalConfiguration?id='+sProposalId+'&flow=ngFlow');
            refreshPage.setRedirect(false);
            return refreshPage;
        }
        else if((sQStage == 'Solution Under Review' || sQStage == 'Approval Required') && (setAdminProfiles.contains(sUserProfileName) || setAEProfiles.contains(sUserProfileName) ||setSEProfiles.contains(sUserProfileName) || setDDProfiles.contains(sUserProfileName))){
            PageReference refreshPage = new PageReference(Site.getPathPrefix()+'/apex/Apttus_QPConfig__ProposalConfiguration?id='+sProposalId+'&flow=repriceFlow&useAdvancedApproval=true');
            refreshPage.setRedirect(false);
            return refreshPage;
        }
        else if(sQStage == 'In Review' && (setDDProfiles.contains(sUserProfileName) || setAdminProfiles.contains(sUserProfileName))){
            PageReference refreshPage = new PageReference(Site.getPathPrefix()+'/apex/Apttus_QPConfig__ProposalConfiguration?id='+sProposalId+'&flow=finalizeFlow');
            refreshPage.setRedirect(false);
            return refreshPage;
        }
        else{
            PageReference refreshPage = new PageReference(Site.getPathPrefix()+'/apex/Apttus_QPConfig__ProposalConfiguration?id='+sProposalId+'&flow=onlyCloseFlow');
            refreshPage.setRedirect(false);
            return refreshPage;
        }
    }
}