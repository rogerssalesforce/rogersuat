/*Class Name :SalesForecastRecalculationBatch.
 *Description : .
 *Created By : Deepika Rawat.
 *Created Date :5/11/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*/
global class SalesForecastDeletionBatch implements Database.Batchable<SObject>{
       public integer SFCheckPoint = Integer.ValueOf(Static_Data_Utilities__c.getInstance('Recalculation Period').Value__c);
       public date dT1=system.Today().addDays(-SFCheckPoint);
       global String Query;
       global Database.DeleteResult[] listDeleteResult;
       List<SalesForecast_Errors__c> errorList = new List<SalesForecast_Errors__c>();
       
    // End:Declarations----------------------------------------------------------------------------------------
   
     global Database.QueryLocator start(Database.BatchableContext BC){
        if(!Test.isRunningTest()){
        try{
            List<SalesForecast_Errors__c> errorRecordlist = [Select id , Error_Message__c, Job_Name__c, Status_Code__c, Record_Owner__c, Sales_Forecast__c, Sales_Measurement__c, Sales_Quota__c from SalesForecast_Errors__c];
            if(errorRecordlist.size()>0){
            delete errorRecordlist;
            }
            return database.getQueryLocator(query);
        }catch(exception ex){   
            return null;
        }
        }else{
            return database.getQueryLocator(query);
        }     
    }

     global void execute(Database.BatchableContext BC,List<SObject> Scope) {
        List<Sales_Forecast__c> lstDeleteSF = new list<Sales_Forecast__c>();
        for(SObject sO:Scope){
            Schema.Sobjecttype inspecttype=sO.Id.getSObjectType();
            if(String.Valueof(inspecttype) =='Sales_Forecast__c'){
                lstDeleteSF.add((Sales_Forecast__c)sO); 
            }
        }
        
        if(lstDeleteSF.size()>0){
            listDeleteResult = Database.delete(lstDeleteSF, false);
            for(integer i=0; i<lstDeleteSF.size(); i++){
                if(!(listDeleteResult[i].isSuccess())){
                     List<Database.Error> err = listDeleteResult[i].getErrors();
                    // Check if the error is related to trivial access level. Access levels equal or more permissive than the object's default access level are not allowed. 
                    //These sharing records are not required and thus an insert exception is acceptable. 
                    if(!(err[0].getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION &&  err[0].getMessage().contains('AccessLevel'))){
                        SalesForecast_Errors__c rec = new SalesForecast_Errors__c();
                        //rec.Record_Owner__c = listDeleteResult[i].OwnerId;
                        rec.Job_Name__c = 'Delete Sales Forecast records' ;
                        rec.Error_Message__c = err[0].getMessage();
                        rec.Sales_Forecast__c = listDeleteResult[i].id;
                        rec.Status_Code__c = String.Valueof(err[0].getStatusCode());
                        errorList.add(rec);                
                    }
                }
            }
        }

        if(errorList.size()>0){
                insert errorList;
        }             
     }
     
     global void finish(Database.BatchableContext BC){
        Static_Data_Utilities__c flagset = Static_Data_Utilities__c.getInstance('BatchFire');
        flagset.Value__c= 'false';
        update flagset;

        integer SFCheckPoint = Integer.ValueOf(Static_Data_Utilities__c.getInstance('Recalculation Period').Value__c);
        date dT1=system.Today().addDays(-SFCheckPoint);
        SalesForecastRecalculationBatch salesQuotaObj = new SalesForecastRecalculationBatch();
        if(!Test.isRunningTest()){
        salesQuotaObj.Query = 'select id,LastModifiedDate,Owner.FirstName,Owner.LastName,Quota_Family__c,Forecast_Month__c,OwnerID,Quantity__c,Revenue__c,OwnerMonthFamily__c FROM Sales_Quota__c';
        }
        else{
        salesQuotaObj.Query = 'select id,LastModifiedDate,Owner.FirstName,Owner.LastName,Quota_Family__c,Forecast_Month__c,OwnerID,Quantity__c,Revenue__c,OwnerMonthFamily__c FROM Sales_Quota__c limit 2';
        }
        ID salesQuotaBatchProcessId = Database.executeBatch(salesQuotaObj,200);
    }
}