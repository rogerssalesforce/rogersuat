/*Class Name  : TestClass_checkAccountUpdate .
 *Description : This is test class for trigger checkAccountUpdate.
 *Created By  : Deepika Rawat.
 *Created Date :29/01/2014.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
@isTest(SeeAllData = true)
private class TestClass_checkAccountUpdate{
    static testmethod void casemethod(){
        ID AccUpdateRecordTypeId = [select Id from recordType where name='Account Update' and sObjecttype ='Case'].id;
        Account acc= new Account();
        acc.BillingPostalCode = 'A1A 1A1';
        acc.Name = 'testAcc';
        insert acc;
        Case c = new Case();
        c.AccountId = acc.id;
        c.recordtypeId = AccUpdateRecordTypeId;

        Schema.DescribeFieldResult resultProgressReason= Case.In_Progress_Reason__c.getDescribe();        
        c.In_Progress_Reason__c = (String)(TestingUtil.getPicklistDefaultValue(resultProgressReason));
        c.Date__c = Date.today();
        /**/        
        System.debug('***In Progress Reason : ' + c.In_Progress_Reason__c);
        System.debug('***Date (custom): ' + c.Date__c);
        /**/        

        insert c;
        Case c2 = new Case();
        c2.AccountId = acc.id;

        c2.In_Progress_Reason__c = (String)(TestingUtil.getPicklistDefaultValue(resultProgressReason));
        c2.Date__c = Date.today();
        /**/        
        System.debug('***In Progress Reason 2: ' + c2.In_Progress_Reason__c);
        System.debug('***Date (custom) 2: ' + c2.Date__c);
        /**/        
       
        Test.starttest();       
        insert c2; 
        Test.stoptest();
    }
}