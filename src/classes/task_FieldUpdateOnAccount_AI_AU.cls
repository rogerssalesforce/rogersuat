/*Class Name  :task_FieldUpdateOnAccount_AI.
 *Description : This Class will support the logic for Task_FieldUpdateOnAccount_AI_AU Trigger after Insert and After Update.
 *Created By  : Rajiv Gangra.
 *Created Date :03/09/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
public class task_FieldUpdateOnAccount_AI_AU {

    /*@ Method Name: updateAccountInfo
      Description: This Method gets the list of Task and Assigns Field to related Account .
      Return Type: NA.
      Input Param: NA.   
    */
    public void updateAccountInfo(LIST<Task> tList) {
        List<Id> lstAccRelated= New List<Id>();
        List<Id> lstOppRelated= New List<Id>();
        List<Account> lstAccToUpdate= new List<Account>();
        List<Opportunity> lstOppToUpdate= new List<Opportunity>();
        Map<id,Account> mapOppAccount= new Map<id,Account>();
        list<User> uList=[select id,Name,Channel__c from User];
        Map<id,string> mapUserChannel= new Map<id,string>(); 
        for(User u:uList){
            mapUserChannel.put(u.id,u.Channel__c);
        }
        try{
            if(tList !=null && tList.size()!=0){
                for( Task t : tList){
                     Schema.Sobjecttype inspecttype;
                     if(t.WhatId !=null){
                         inspecttype=t.WhatId.getSObjectType();
                     }
                     if(inspecttype !=null){
                         if(String.Valueof(inspecttype) == 'Opportunity'){
                           lstOppRelated.add(t.WhatId);  
                         }else if(String.valueof(inspecttype) == 'Account'){
                           lstAccRelated.add(t.WhatId);
                         }
                     }
                
                }
                List<Opportunity> lstRelatedOpp= [select id,name,AccountId from Opportunity where Id IN:lstOppRelated];
                if(lstRelatedOpp !=null && lstRelatedOpp.size() !=0){
                    for(Opportunity o:lstRelatedOpp){
                        if(o.AccountId !=null || o.AccountId !=''){
                            lstAccRelated.add(o.AccountId);
                        }
                    }
                }
                List<Account> lstRelatedAcc= [select id,name,Last_Activity_Date__c,Last_Activity_Owner__c from Account where Id IN:lstAccRelated];
                if(lstRelatedAcc !=null && lstRelatedAcc.size() !=0){
                    for(Account a:lstRelatedAcc){
                     for(Opportunity o:lstRelatedOpp){
                         if(o.AccountId == a.id){
                             mapOppAccount.put(o.id,a);
                         }
                     }
                    }
                }
                
                for(Task t:tList){
                    DateTime dT = t.LastModifiedDate;
                    Date createdDate = date.newinstance(dT.year(), dT.month(), dT.day());
                    if(lstRelatedAcc !=null){
                        for(Account a:lstRelatedAcc){
                            if(t.WhatID==a.id){
                                a.Last_Activity_Date__c=createdDate;
                                a.Last_Activity_Owner__c=t.Ownerid;
                                a.Last_Activity_Channel__c=mapUserChannel.get(t.Ownerid);
                                lstAccToUpdate.add(a);
                            }
                        }
                    }
                    if(lstRelatedOpp !=null){
                        for(Opportunity o:lstRelatedOpp){
                            if(t.WhatID==o.id){
                                Account a=mapOppAccount.get(o.id);
                                if(a !=null){
                                    a.Last_Activity_Date__c=createdDate;
                                    a.Last_Activity_Owner__c=t.Ownerid;
                                    a.Last_Activity_Channel__c=mapUserChannel.get(t.Ownerid);
                                    lstAccToUpdate.add(a);
                                }
                            }
                        }
                    }
                }
                Update lstAccToUpdate;
            }
        }Catch(Exception ex){
            system.debug('Exception-------------------->'+ex);
        }
    }

}