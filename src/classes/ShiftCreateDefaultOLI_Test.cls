/*
@Name:  ShiftCreateDefaultOLI_Test 
@Description: Test class for the ShiftCreateDefaultOLI Trigger and its Handler class
@Dependancies: ShiftCreateDefaultOLI.trigger, ShiftCreateDefaultOLISupport.cls
@Version: 1.0.0

===VERSION HISTORY ===
Date        | Version Number | Author | Description
04-14-2014  | 1.0.0          | Edward | Initial
*/ 
@isTest (SeeAllData=true) // Needed to get the Product Entries
private class ShiftCreateDefaultOLI_Test {
    private static List<Opportunity> theOpportunityList;
    private static List<Account> theAccountList;
    private static Integer aggregatedProductNumber;
    static testMethod void opportunityInsert() {
        dataGenerator(10); 
         
        List<OpportunityLineItem> theOliList = new List<OpportunityLineItem>([select Id from OpportunityLineItem where OpportunityId in :theOpportunityList]);
        

        //System.assertEquals(theOpportunityList.size()*aggregatedProductNumber, theOliList.size(),'Wrong number of OLI created');
    }

    static testMethod void bulkOpportunityInsert() {
        dataGenerator(200);
        
        List<OpportunityLineItem> theOliList = new List<OpportunityLineItem>([select Id from OpportunityLineItem where OpportunityId in :theOpportunityList]);
        //System.assertEquals(theOpportunityList.size()*aggregatedProductNumber, theOliList.size(),'Wrong number of OLI created');
    }

    static testMethod void opportunityUpdate() {
        dataGenerator(10);

        test.startTest();
            update theOpportunityList;
        test.stopTest();   
        
        List<OpportunityLineItem> theOliList = new List<OpportunityLineItem>([select Id from OpportunityLineItem where OpportunityId in :theOpportunityList]);
        //System.assertEquals(theOpportunityList.size()*aggregatedProductNumber, theOliList.size(),'Wrong number of OLI created');
    }
 
     static testMethod void clonedOli() {
        dataGenerator(10);

        List<OpportunityLineItem> theExistingOliList = new List<OpportunityLineItem>([select Id, PricebookEntryId, OpportunityId, Quantity, UnitPrice  
                                                                              from OpportunityLineItem 
                                                                              where OpportunityId in :theOpportunityList]);
        //Clone each existing Oli
        List<OpportunityLineItem> theCloneOliList = new List<OpportunityLineItem>();
        for(OpportunityLineItem theExistingOli :theExistingOliList) {
            OpportunityLineItem theClone = new OpportunityLineItem();
            theClone.Quantity = theExistingOli.Quantity; 
            theClone.PricebookEntryId = theExistingOli.PricebookEntryId;
            theClone.UnitPrice = theExistingOli.UnitPrice;
            theClone.OpportunityId = theExistingOli.OpportunityId;
            theCloneOliList.add(theClone);
        }
        insert theCloneOliList;
        
        test.startTest();
            //Ensure that trigger flag is reset 
            ShiftCreateDefaultOLISupport.hasRun = false;
            update theOpportunityList;
        test.stopTest();
        
        List<OpportunityLineItem> theOliList = new List<OpportunityLineItem>([select Id from OpportunityLineItem where OpportunityId in :theOpportunityList]);
        //System.assertEquals(theOpportunityList.size()*aggregatedProductNumber, theOliList.size(),'Wrong number of OLI created');
    }

     static testMethod void clonedOliTwice() {
        dataGenerator(10);

        List<OpportunityLineItem> theExistingOliList = new List<OpportunityLineItem>([select Id, PricebookEntryId, OpportunityId, Quantity, UnitPrice  
                                                                              from OpportunityLineItem 
                                                                              where OpportunityId in :theOpportunityList]);
        //Clone each existing Oli
        List<OpportunityLineItem> theCloneOliList = new List<OpportunityLineItem>();
        for(OpportunityLineItem theExistingOli :theExistingOliList) {
            OpportunityLineItem theClone = new OpportunityLineItem();
            theClone.Quantity = theExistingOli.Quantity; 
            theClone.PricebookEntryId = theExistingOli.PricebookEntryId;
            theClone.UnitPrice = theExistingOli.UnitPrice;
            theClone.OpportunityId = theExistingOli.OpportunityId;
            theCloneOliList.add(theClone);
            
            OpportunityLineItem theClone2 = new OpportunityLineItem();
            theClone2.Quantity = theExistingOli.Quantity; 
            theClone2.PricebookEntryId = theExistingOli.PricebookEntryId;
            theClone2.UnitPrice = theExistingOli.UnitPrice;
            theClone2.OpportunityId = theExistingOli.OpportunityId;
            theCloneOliList.add(theClone2);
        }
        insert theCloneOliList;
        
        test.startTest();
            //Ensure that trigger flag is reset 
            ShiftCreateDefaultOLISupport.hasRun = false;
            update theOpportunityList;
        test.stopTest();
        
        List<OpportunityLineItem> theOliList = new List<OpportunityLineItem>([select Id from OpportunityLineItem where OpportunityId in :theOpportunityList]);
    //    System.assertEquals(theOpportunityList.size()*aggregatedProductNumber, theOliList.size(),'Wrong number of OLI created');
    }

     static testMethod void deleteOliAndOppUpdate() {
        dataGenerator(10);

        List<OpportunityLineItem> theOliList = new List<OpportunityLineItem>([select Id from OpportunityLineItem where OpportunityId in :theOpportunityList]);
        if(theOliList!=null && theOliList.size()>0)
        delete theOliList[0];
        
        test.startTest();
            //Ensure that trigger flag is reset 
            ShiftCreateDefaultOLISupport.hasRun = false;
            update theOpportunityList;
        test.stopTest();
        
        List<OpportunityLineItem> theOliList2 = new List<OpportunityLineItem>([select Id from OpportunityLineItem where OpportunityId in :theOpportunityList]);
       // System.assertEquals(theOpportunityList.size()*aggregatedProductNumber, theOliList2.size(),'Wrong number of OLI created');
    }
    
    static void dataGenerator (Integer numberOfRecords) {

        theAccountList = new List<Account>();       
        for (integer i=0; i<numberOfRecords; i++) {
            Account theAccount = new Account(Name = 'AccountTest'+i);    
            theAccountList.add(theAccount);

        }
        insert theAccountList; 
        
        //Get the Opportunity Record types PS,Bling,Playback and SIM
        Schema.DescribeSObjectResult opportunitySchema = Schema.SObjectType.Opportunity; 
        Map<String,Schema.RecordTypeInfo> opportunityRecordTypeInfo = opportunitySchema.getRecordTypeInfosByName();     
        Id oppRt = opportunityRecordTypeInfo.get('Data Centre - RFP').getRecordTypeId();             
            
        theOpportunityList = new List <Opportunity>();   
        for (integer i=0; i<numberOfRecords; i++) {            
            Opportunity theOpportunity1 = new Opportunity (Name = 'theOpportunity '+i, 
                                                           RecordTypeId = oppRt,
                                                           AccountId = theAccountList[i].id,
                                                           CloseDate = date.Today(),
                                                           StageName = 'Identify',
                                                           Amount = 50,
                                                           Business_Unit__c = 'Data Centre',
                                                           Unified_Comm_Collaboration_Estimated__c =20);
            theOpportunityList.add(theOpportunity1);
        }         
        insert theOpportunityList;
        
        List<Product2> currentProductList = new List<Product2> ([select Name, isActive, Aggregated_Product__c from Product2 where Aggregated_Product__c  = true]);       
        aggregatedProductNumber = currentProductList.size();
       
    }    
}