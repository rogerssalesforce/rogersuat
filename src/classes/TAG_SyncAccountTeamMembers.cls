/*
===============================================================================
 Class Name   : TAG_SyncAccountTeamMembers
===============================================================================
PURPOSE:    This class is used to sync AccountTeamMembers from Parent Account
            to it's child Accounts
              
Developer: Deepika Rawat
Date: 03/03/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
03/03/2015         Deepika Rawat               Original Version
===============================================================================*/
public class TAG_SyncAccountTeamMembers{

     /*******************************************************************************************************
     * @description: This method is used to create AccountTeammembers on child as in related Parent Account

     * @param: Set<Id>/ This is the Set of Child Account IDs, for which we need to create Account Team Members
     * @return type: void.
     ********************************************************************************************************/
    public void addTeamMembersTraversingFromChild(Set<Id> lstChildAccIDs){
        Set<Id> parentAccountIds = new Set<Id> ();
        Map<Id, Set<Id>> mapAccUserIdsToAdd = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> mapAccUserIdsToRemove = new Map<Id, Set<Id>>(); 
        Map<Id, List<AccountTeamMember>> mapParentTeamMembers = new Map<Id, List<AccountTeamMember>>();
        List<AccountTeamMember> createTeamMembers = new List<AccountTeamMember>();
        List<Account> lstChildAccounts = [Select id, ParentId from Account where id in : lstChildAccIDs];
        List<AccountTeamMember> lstAllChildExistingTeamMembers = [Select Id, UserId,TeamMemberRole,Account.Id,User.isActive from AccountTeamMember where Account.Id in :lstChildAccounts and User.isActive=true];
        system.debug('lstChildAccounts ***'+lstChildAccounts );
        //get list of parent Accounts for the Child Accounts passed to this method
        if(lstChildAccounts!=null && lstChildAccounts.size()>0){
            for(Account childAcc:lstChildAccounts){
                parentAccountIds.add(childAcc.ParentId);
            }
        }
        //Create a map of Parent Account and their related Account Team
        List<AccountTeamMember> lstAllTeamMembers = [Select Id, UserId,User.isActive,TeamMemberRole,Account.Id from AccountTeamMember where Account.Id in :parentAccountIds and User.isActive=true];
        List<AccountTeamMember> teamMembers;
        if(lstAllTeamMembers!=null && lstAllTeamMembers.size()>0){
            for(AccountTeamMember member:lstAllTeamMembers){
                teamMembers = mapParentTeamMembers.get(member.Account.Id);
                if(teamMembers==null)
                    teamMembers= new List<AccountTeamMember>();
                teamMembers.add(member);
                mapParentTeamMembers.put(member.Account.Id, teamMembers);
            }
        }
        //Create AccountTeammembers on child as in related Parent Account
        if(lstChildAccounts!=null && lstChildAccounts.size()>0){
            List<AccountTeamMember> existingTeamOnParent;
            AccountTeamMember newMem ;
            for(Account childAcc:lstChildAccounts){
                existingTeamOnParent = mapParentTeamMembers.get(childAcc.ParentId);
                if(existingTeamOnParent!=null && existingTeamOnParent.size()>0){
                    for(AccountTeamMember exitingMem : existingTeamOnParent){
                        newMem = new AccountTeamMember();
                        newMem.TeamMemberRole = exitingMem.TeamMemberRole;
                        newMem.AccountId = childAcc.Id;
                        newmem.UserId = exitingMem.UserId;
                        createTeamMembers.add(newmem);
                    }
                }
            }
        }
        if(lstAllChildExistingTeamMembers!=null && lstAllChildExistingTeamMembers.size()>0){
            try{Set<Id> userIds;
                for(AccountTeammember mem: lstAllChildExistingTeamMembers){
                    userIds = mapAccUserIdsToRemove.get(mem.Account.Id);
                    If(userIds==null)
                    userIds = new Set<Id>();
                    userIds.add(mem.UserId);
                    mapAccUserIdsToRemove.put(mem.Account.Id,userIds);
                }
                delete lstAllChildExistingTeamMembers;
                if(Test.isRunningTest())
            { Integer a=1/0;
            }
            }catch(exception e){
            system.debug('CLASS :syncAccountTeams, METHOD : addTeamfromParentToChild - Exception while creating AccountTeammembers ----: '+createTeamMembers);
            }
        }
        if(createTeamMembers.size()>0){
            insert createTeamMembers;
        }
        Set<Id> userIds ;
        for(AccountTeamMember teamMem : createTeamMembers){
            userIds = mapAccUserIdsToAdd.get(teamMem.AccountId);
            If(userIds==null)
            userIds = new Set<Id>();
            userIds.add(teamMem.UserId);
            mapAccUserIdsToAdd.put(teamMem.AccountId,userIds);
        }
        //Call updateAccountTeamAccess class to Give access from Users after updating AccountTeam
        updateAccountTeamAccess accessCls = new updateAccountTeamAccess();
        accessCls.giveAccesstoTeam(mapAccUserIdsToAdd);
        accessCls.removeAccessFromTeam(mapAccUserIdsToRemove);
    }
     /*******************************************************************************************************
     * @description: This method is used to create AccountTeammembers on child as in related Parent Account.

                     **Imp Note : This method first Deletes all the Team Members from Child Accounts and 
                                  recreates them as in Parent.

     * @param: Set<Id>/ This is the Set of Parent Account IDs
     * @return type: void.
     ********************************************************************************************************/
    public void addTeamMembersTraversingFromParent(Set<Id> lstPanentAccIDs){
        Map<Id, Set<Id>> mapAccUserIdsToAdd = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> mapAccUserIdsToRemove = new Map<Id, Set<Id>>(); 
        Map<Id, List<AccountTeamMember>> mapParentTeamMembers = new Map<Id, List<AccountTeamMember>>();
        List<AccountTeamMember> createTeamMembers = new List<AccountTeamMember>();
        List<Account> lstChildAccounts = [Select id, ParentId from Account where ParentId in : lstPanentAccIDs];
        List<AccountTeamMember> lstAllParentTeamMembers = [Select Id, UserId,User.isActive,TeamMemberRole,Account.Id from AccountTeamMember where Account.Id in :lstPanentAccIDs and User.isActive=true];
        List<AccountTeamMember> lstAllChildTeamMembers = [Select Id, UserId,User.isActive, TeamMemberRole,Account.Id from AccountTeamMember where Account.Id in :lstChildAccounts and User.isActive=true];
        //Create a map of Parent Account and their related Account Team
        List<AccountTeamMember> teamMembers;
        Set<Id> teamMemIds;
        if(lstAllParentTeamMembers!=null && lstAllParentTeamMembers.size()>0){
            for(AccountTeamMember member:lstAllParentTeamMembers){
                    teamMembers = mapParentTeamMembers.get(member.Account.Id);
                    if(teamMembers==null)
                        teamMembers= new List<AccountTeamMember>();
                    teamMembers.add(member);
                    mapParentTeamMembers.put(member.Account.Id, teamMembers);

                    teamMemIds = mapAccUserIdsToAdd.get(member.Account.Id);
                    If(teamMemIds==null)
                    teamMemIds = new Set<Id>();
                    teamMemIds.add(member.UserId);
                    mapAccUserIdsToAdd.put(member.Account.Id,teamMemIds);
                }
        }
        //Create AccountTeammembers on child as in related Parent Account
        
        if(lstChildAccounts!=null && lstChildAccounts.size()>0){
            AccountTeamMember newMem ;
            List<AccountTeamMember> existingTeamOnParent;
            for(Account childAcc:lstChildAccounts){
                existingTeamOnParent = mapParentTeamMembers.get(childAcc.ParentId);
                if(existingTeamOnParent!=null && existingTeamOnParent.size()>0){
                    for(AccountTeamMember exitingMem : existingTeamOnParent){
                        newMem = new AccountTeamMember();
                        newMem.TeamMemberRole = exitingMem.TeamMemberRole;
                        newMem.AccountId = childAcc.Id;
                        newmem.UserId = exitingMem.UserId;
                        createTeamMembers.add(newmem);
                    }
                }
            }
        }
        //delete existing AccountTeamMembers from Child Account
        if(lstAllChildTeamMembers.size()>0){
            try{
                Set<Id> userIds;
                for(AccountTeammember mem: lstAllChildTeamMembers){
                    userIds = mapAccUserIdsToRemove.get(mem.Account.Id);
                    If(userIds==null)
                    userIds = new Set<Id>();
                    userIds.add(mem.UserId);
                    mapAccUserIdsToRemove.put(mem.Account.Id,userIds);
                }
                delete lstAllChildTeamMembers;
                if(Test.isRunningTest())
            { Integer a=1/0;
            }
            }catch(exception e){
                system.debug('CLASS :syncAccountTeams, METHOD : addTeamfromParentToChild - Exception while deleting AccountTeammembers ----: '+lstAllChildTeamMembers);
            }
        }
        //create AccountTeamMembers on Child Account
        if(createTeamMembers.size()>0){
            insert createTeamMembers; 
        }
        Set<Id> userIds;
        for(AccountTeamMember teamMem : createTeamMembers){
            userIds = mapAccUserIdsToAdd.get(teamMem.AccountId);
            If(userIds==null)
            userIds = new Set<Id>();
            userIds.add(teamMem.UserId);
            mapAccUserIdsToAdd.put(teamMem.AccountId,userIds);
        }
        //Call updateAccountTeamAccess class to Give access from Users after updating AccountTeam
        updateAccountTeamAccess accessCls = new updateAccountTeamAccess();
        accessCls.giveAccesstoTeam(mapAccUserIdsToAdd);
        accessCls.removeAccessFromTeam(mapAccUserIdsToRemove);
    }
}