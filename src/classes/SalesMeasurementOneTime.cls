global class SalesMeasurementOneTime implements Database.Batchable<sObject>{

   global String Query;
   global map<string,Product_Type_List__c> prodTypelst = Product_Type_List__c.getAll();

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
       
        List<Id> oliIds = new List<Id>();        
        for(OpportunityLineItemSchedule olis : (List<OpportunityLineItemSchedule>)scope) {
            OliIds.add(olis.OpportunityLineItemId);
        }
        Map<id,OpportunityLineItem> oliMap = new Map<id,OpportunityLineItem>([select id,Product_Family__c, Data_of_Total_Revenue__c, One_Time_Value__c, Monthly_Value__c, Contract_term__c, OpportunityId,Quantity,of_Installments__c,ARPU__c,Opportunity.OwnerID,Opportunity.Account.Restricted__c,PricebookEntry.Product2ID from OpportunityLineItem where id in :OliIds]);
        Map<Id,OpportunityLineItem> mapLineItemScheduleIdLineItemObj = new Map<Id,OpportunityLineItem>();
        for(OpportunityLineItemSchedule olis : (List<OpportunityLineItemSchedule>)scope) {
            mapLineItemScheduleIdLineItemObj.put(olis.id, oliMap.get(olis.OpportunityLineItemId));
        }
        List<Sales_Measurement__c> lstRelatedSchedule=[Select Id,Name from Sales_Measurement__c where Opportunity_Line_Item_ID__c in :OliIds];
        delete lstRelatedSchedule;
        List<Sales_Measurement__c> lstSalesSchedule = new List<Sales_Measurement__c>();
        for(OpportunityLineItemSchedule oli:(List<OpportunityLineItemSchedule>)scope){
            String lineGrouping;
             for(Product_Type_List__c pT:Product_Type_List__c.getall().values()){
                if(pT.Product_Family__c==mapLineItemScheduleIdLineItemObj.get(oli.id).Product_Family__c){
                    lineGrouping=pT.Name;
                }
            }
            String prodFamily;
            if(prodTypelst!=null && lineGrouping!=null && prodTypelst.get(lineGrouping).Quota_Family__c!=null){
            prodFamily = prodTypelst.get(lineGrouping).Quota_Family__c;
            }else
            prodFamily='NA';
            Sales_Measurement__c salesMeasureSchedule= new Sales_Measurement__c();
            salesMeasureSchedule.Opportunity__c=mapLineItemScheduleIdLineItemObj.get(oli.id).OpportunityId;
            salesMeasureSchedule.Opportunity_Line_Item_ID__c=oli.OpportunityLineItemId;
            salesMeasureSchedule.Deployment_Date__c=oli.ScheduleDate;
            salesMeasureSchedule.OwnerID=mapLineItemScheduleIdLineItemObj.get(oli.id).Opportunity.OwnerID;
            salesMeasureSchedule.Comments__c=oli.description;
            salesMeasureSchedule.product__c=mapLineItemScheduleIdLineItemObj.get(oli.id).PricebookEntry.Product2ID;
            salesMeasureSchedule.Quota_Family__c=prodFamily;
            salesMeasureSchedule.Quantity__c=oli.Quantity;
            if( salesMeasureSchedule.Quota_Family__c !='NA' ){
                salesMeasureSchedule.Revenue__c=oli.Revenue;
            }else{
               Sales_Measurement__c salesMeasureSchedule1= new Sales_Measurement__c();
               salesMeasureSchedule1.Quota_Family__c=prodFamily;
               salesMeasureSchedule1.Opportunity__c=mapLineItemScheduleIdLineItemObj.get(oli.id).OpportunityId;
               salesMeasureSchedule1.Opportunity_Line_Item_ID__c=mapLineItemScheduleIdLineItemObj.get(oli.id).id;
               salesMeasureSchedule1.Deployment_Date__c=oli.ScheduleDate;
               salesMeasureSchedule1.OwnerID=mapLineItemScheduleIdLineItemObj.get(oli.id).Opportunity.OwnerID;
               salesMeasureSchedule1.Comments__c=oli.description;
               salesMeasureSchedule1.product__c=mapLineItemScheduleIdLineItemObj.get(oli.id).PricebookEntry.Product2ID;
               salesMeasureSchedule1.Quota_Family__c='Wireless - All Data';
               salesMeasureSchedule1.Quantity__c=oli.Quantity;
               salesMeasureSchedule.Quota_Family__c='Wireless - Voice';
               if(oli.Revenue !=null ){
                    if(mapLineItemScheduleIdLineItemObj.get(oli.id).Data_of_Total_Revenue__c!=null) {
                        salesMeasureSchedule1.Revenue__c=oli.Revenue *(mapLineItemScheduleIdLineItemObj.get(oli.id).Data_of_Total_Revenue__c/100);
                    } else {
                        salesMeasureSchedule1.Revenue__c=oli.Revenue *(0.5);
                    }
               }
               // Adding the same Product 2 times for once for Voice and 2nd time for Data
               lstSalesSchedule.add(salesMeasureSchedule1);
               if(oli.Revenue !=null ){
                    if(mapLineItemScheduleIdLineItemObj.get(oli.id).Data_of_Total_Revenue__c!=null) {
                        salesMeasureSchedule.Revenue__c=oli.Revenue *(1-(mapLineItemScheduleIdLineItemObj.get(oli.id).Data_of_Total_Revenue__c/100));   
                    } else {
                        salesMeasureSchedule.Revenue__c=oli.Revenue *(0.5);
                    }
                    
               }
                                
            }
            lstSalesSchedule.add(salesMeasureSchedule);
        }
        insert lstSalesSchedule;
   }

   global void finish(Database.BatchableContext BC){
   }
}