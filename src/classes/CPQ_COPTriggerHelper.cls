/*********************************************************************************
Class Name      : CPQ_COPTriggerHelper
Description     : This class is used as a helper class for COP Trigger 
Created By      : Aakanksha Patel
Created Date    : 23-Nov-2015
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Aakanksha Patel           23-Nov-2015         (US-0351) Updating the 'Latest Wireless COP' field on the Account
                                             
*********************************************************************************/
public with sharing class CPQ_COPTriggerHelper{
   
    private static final String CPQ_COP_STATUS_COMPLETED ='Completed';
    
    /*********************************************************************************
    Method Name    : updateWirelessCOPAccount
    Description    : 1) Updating the 'Latest Wireless COP' field on the Account
    Return Type    : Void
    Parameter      : List<COP__c>, Map<id,COP__c >
    Developer      : Aakanksha Patel
    *********************************************************************************/
    
    public static void  updateWirelessCOPAccount(LIST<COP__c > lstFromTrigger, Map<id,COP__c > mapFromTriggerOld)
    {    
        Id oAccID;  
        String strCopId;
        Account oAccUpdate;
        COP__c oCOPoldRec;
        
        Set<id> setAccIds = new Set<id>();
        Set<id> setNewCOPIds = new Set<id>();
        Set<Account> setAccountUpdate = new Set<Account>(); 
        LIST<Account> lstAccount = new LIST<Account>(); 
        List<COP__c> lstNewCOPIds = new List<COP__c>(); 
        Map<Id,Account> mapAccIdAccount = new Map<Id, Account>();
        Map<Id, Id> mapCopIdAccountId = new Map<Id, Id>();
        Map<Id, Id> mapNewCOPandOldCOPId = new Map<Id, Id>();
        
        List<RecordType> COPRecTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Contracts_Team_Wireless' AND SobjectType = 'COP__c'];
        
        //Iterating through the list of COP IDs from the trigger to get the Set of COP IDs in "Completed" status to be updated
        for(COP__c oCOPRec: lstFromTrigger)
        {
            oCOPoldRec = mapFromTriggerOld.get(oCOPRec.Id);
            //Check for the Wireless Record Type and the update in status to "Completed"
            if(oCOPRec.RecordTypeId == COPRecTypeId[0].id && oCOPRec.COP_Status__c!=null &&oCOPRec.COP_Status__c.equalsIgnorecase(CPQ_COP_STATUS_COMPLETED) && (oCOPoldRec.COP_Status__c == null || oCOPRec.COP_Status__c != oCOPoldRec.COP_Status__c))
            {
                setNewCOPIds.add(oCOPRec.id);
            }
        }
        
        lstNewCOPIds = [Select id,  Agreement__c, Agreement__r.Apttus__Account__c, Agreement__r.Apttus__Account__r.Latest_Wireless_COP__c, Agreement__r.Apttus__Account__r.Root_BAN__c, Root_BAN__c from COP__c where id in:setNewCOPIds];
         
        //Looping through set of COP Ids to be updated for creating Acc and COP map and for set of old COP IDs
        for(COP__c cop : lstNewCOPIds)
        {
            setAccIds.add(cop.Agreement__r.Apttus__Account__c);
            mapCopIdAccountId.put(cop.id, cop.Agreement__r.Apttus__Account__c);
        }
        lstAccount = [select id,Latest_Wireless_COP__c from Account where id in: setAccIds ];
        
        //creating map of AccountID and related Account to be updated
        for(Account accRec : lstAccount)
        {
            mapAccIdAccount.put(accRec.id, accRec);
        }
       
       //Iterating through new COP Ids for COP Line Items and Account Update 
        for(COP__c copRec: lstNewCOPIds)
        {
           
            oAccID = mapCopIdAccountId.get(copRec.id);
            oAccUpdate = mapAccIdAccount.get(oAccID);
            oAccUpdate.Latest_Wireless_COP__c = copRec.Id;
            oAccUpdate.Root_BAN__c = copRec.Root_BAN__c;
            setAccountUpdate.add(oAccUpdate);
        }
    
        //Update existing Account Record and Capturing Errors in Database 
        if(setAccountUpdate != null && setAccountUpdate.size() > 0)
        {
             LIST<Account> lstAccountUpdate = new LIST<Account>(setAccountUpdate); 
             update lstAccountUpdate;
        }
        
    }
    
}