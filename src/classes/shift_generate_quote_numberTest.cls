@isTest(seeAllData=true) 
private class shift_generate_quote_numberTest {

    static testMethod void myUnitTest() {
         
        // create account
        Recursion_blocker.flag = true;
        Account a = new Account(Name = 'test account', BillingPostalCode = 'A1A 1A1');
        insert a;
        
        // create oppotunity
        Opportunity o1 = new Opportunity(Name = 'test opportunity 1', AccountId = a.Id, CloseDate = system.today(), StageName = 'Open');
        o1.Unified_Comm_Collaboration_Estimated__c =20;
        insert o1;
        o1 = [select Opportunity_Number2__c from Opportunity where Id =: o1.Id];
        
        Opportunity o2 = new Opportunity(Name = 'test opportunity 2', AccountId = a.Id, CloseDate = system.today(), StageName = 'Open');
        o2.Unified_Comm_Collaboration_Estimated__c =20;
        insert o2;  
        o2 = [select Opportunity_Number2__c from Opportunity where Id =: o2.Id];      
        
        // create a quote
        Quote__c q1 = new Quote__c(Opportunity_Name__c = o1.Id);
        Recursion_blocker.flag = true;
        insert q1;
        
        // verify quote number
        q1 = [select Name,Opportunity_Name__c from Quote__c where Id =: q1.Id];
        system.debug(q1.Name);
        
        Test.startTest();
        // update quote moving quote to another opportunity
    //    q1.Opportunity_Name__c = o2.Id;
        Recursion_blocker.flag = true;
        update q1;
        
        // verify quote number after moving quote to another opportunity
        q1 = [select Name,Opportunity_Name__c from Quote__c where Id =: q1.Id];
        system.debug(q1.Name);
                
        // create second quote quote
        Quote__c q2 = new Quote__c(Opportunity_Name__c = o2.Id);
        Recursion_blocker.flag = true;
        insert q2;  
        
        // verify quote number for second quote
        q2 = [select Name,Opportunity_Name__c from Quote__c where Id =: q2.Id];
        system.debug(q2.Name);      
        
        // if quote number is deleted, then restore the old number
   //     q2.Name = null;
       Recursion_blocker.flag = true;
        update q2;
        
        // verify quote number for second quote
        q2 = [select Name from Quote__c where Id =: q2.Id];
        system.debug(q2.Name);    
         Test.stopTest();             
    }
}