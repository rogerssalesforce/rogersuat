/*
===============================================================================
Class Name : Test_TAG_WeeklyEmailScheduler
===============================================================================
PURPOSE: This is a test class for TAG_WeeklyEmailScheduler class

COMMENTS: 

Developer:Aakanksha Patel
Date: 16/3/2015


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
16/3/2015               Aakanksha               Created
===============================================================================

*/
@isTest(SeeAllData = false)
private class Test_TAG_WeeklyEmailScheduler{
 static testmethod void testMethod1(){
                
        Test.StartTest();
        TAG_WeeklyEmailScheduler obj= new TAG_WeeklyEmailScheduler();
        String sch = '0 0 23 * * ?';
        system.schedule('TAG_WeeklyEmailSchedulerTesting', sch, obj);
        System.assert(true);
        Test.stopTest();    

    }
}