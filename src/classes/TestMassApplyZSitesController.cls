/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 /*********************************************************************************
Class Name      : TestMassApplyZSitesController
Description     : Test class for MassApplyZSitesController
Created By      : 
Created Date    : 
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Alina Balan          25-November-2015            Refactoring
*********************************************************************************/
 
@isTest (seeAllData=False)
private class TestMassApplyZSitesController {
    private static Boolean withAssertions = true;
    private static Opportunity opp;
    private static Opportunity opp2;
    private static Account oAccount;
    private static ServiceableLocation__c oServiceableLocation1;
    private static ServiceableLocation__c oServiceableLocation2;
    private static ServiceableLocation__c oServiceableLocation3;
    private static ServiceableLocation__c oServiceableLocation4;
    private static Site__c oSite1;
    private static Site__c oSite2;
    private static Site__c oSite3;
    private static Site__c oSite4;
    private static Site__c oSite5;
    private static Site__c oSite6;
    private static Quote oQuote;
    private static Quote_Request__c qRequest;
    private static final String STREET_NUMBER = '1';
    private static final String CITY = 'Test City';
    private static final String ACCESS_TYPE_GROUP = 'NNI';
    
    public static void createTestData() {
        CPQ_Test_Setup.createSystemSettingRecord();
        //Create Account
        oAccount = CPQ_Test_Setup.newAccount('Test Account');
        insert oAccount;
        //create opportunity
        opp = CPQ_Test_Setup.newOpportunity(oAccount);
        insert opp;
        opp2 = CPQ_Test_Setup.newOpportunity(oAccount);
        insert opp2;
        //create quote
        oQuote = CPQ_Test_Setup.createQuote(opp);
        insert oQuote;
        //create serviceable location
        oServiceableLocation1 = CPQ_Test_Setup.createServiceablelocation(ACCESS_TYPE_GROUP, STREET_NUMBER, CITY);
        insert oServiceableLocation1;
        oServiceableLocation2 = CPQ_Test_Setup.createServiceablelocation(ACCESS_TYPE_GROUP, '2', CITY);
        insert oServiceableLocation2;
        oServiceableLocation3 = CPQ_Test_Setup.createServiceablelocation(ACCESS_TYPE_GROUP, '3', CITY);
        insert oServiceableLocation3;
        oServiceableLocation4 = CPQ_Test_Setup.createServiceablelocation(ACCESS_TYPE_GROUP, '4', CITY);
        insert oServiceableLocation4;
    }

    /*static {
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        
        
        
        Account a = new Account();
        a.name = 'Mass Apply Z Sites Test Act';
        a.Business_Segment__c = 'Alternate';
        //a.RecordTypeId = mapRTa.get('New Account');
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        a.parentId = null;
        a.Account_Status__c = 'Assigned';
        insert a;       
        accountId = a.Id;
        
        Opportunity opp = CPQ_Test_Setup.newOpportunity(a);
        //Opportunity opp = new Opportunity();
        opp.Estimated_MRR__c = 500;
        opp.Name = 'Mass Apply Z Sites Test Opp';
        opp.StageName = 'Suspect - Qualified';
        opp.Product_Category__c = 'Local';
        opp.Network__c = 'Cable';
        opp.Estimated_One_Time_Charge__c = 500;
        opp.New_Term_Months__c = 5;
        opp.AccountId = a.id;
        opp.Probability = 10;
        //opp.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        opp.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        opp.CloseDate = date.today();
        insert opp;
        oppId = opp.id;
        
        Opportunity opp2 = CPQ_Test_Setup.newOpportunity(a);
        //Opportunity opp2 = new Opportunity();
        opp2.Estimated_MRR__c = 500;
        opp2.Name = 'Mass Apply Z Sites Test Opp2';
        opp2.StageName = 'Suspect - Qualified';
        opp2.Product_Category__c = 'Local';
        opp2.Network__c = 'Cable';
        opp2.Estimated_One_Time_Charge__c = 500;
        opp2.New_Term_Months__c = 5;
        opp2.AccountId = a.id;
        opp2.Probability = 10;
        //opp2.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        opp2.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        opp2.CloseDate = date.today();
        insert opp2;
        opp2Id = opp2.id;
        
        Quote q = new Quote(Name='q1', actualTerm__c = '12', Term__c='12-Months', OpportunityId=oppId);
        insert q;
        quoteId = q.Id;
        
        ServiceableLocation__c sl1 = new ServiceableLocation__c();
        sl1.Name = '1Somewhere1AveCityON123113';
        sl1.Street_Name__c = 'Somewhere1';
        sl1.Street_Number__c  = '1';
        sl1.Street_Type__c  = 'Ave';
        sl1.City__c = 'City';
        sl1.Province_Code__c = 'ON';
        sl1.CLLI_Code__c = '123113';
        sl1.Access_Type_Group__c = 'NNI';
        insert sl1;
        sl1Id = sl1.id;
        
        ServiceableLocation__c sl2 = new ServiceableLocation__c();
        sl2.Name = '2Someplace1AveCityON223113';
        sl2.Street_Name__c = 'Someplace1';
        sl2.Street_Number__c  = '2';
        sl2.Street_Type__c  = 'Ave';
        sl2.City__c = 'City';
        sl2.Province_Code__c = 'ON';
        sl2.CLLI_Code__c = '223113';
        sl2.Access_Type_Group__c = 'NNI';
        insert sl2;
        sl2Id = sl2.id;
        
        ServiceableLocation__c sl3 = new ServiceableLocation__c();
        sl3.Name = '3Someplace1AveCityON223113';
        sl3.Street_Name__c = 'Someplace1';
        sl3.Street_Number__c  = '3';
        sl3.Street_Type__c  = 'Ave';
        sl3.City__c = 'City';
        sl3.Province_Code__c = 'ON';
        sl3.CLLI_Code__c = '223114';
        sl3.Access_Type_Group__c = 'CNI';
        insert sl3;
        sl3Id = sl3.id;
        
        ServiceableLocation__c sl4 = new ServiceableLocation__c();
        sl4.Name = '4Someplace1AveCityON223113';
        sl4.Street_Name__c = 'Someplace1';
        sl4.Suite_Floor__c  = 'A';
        sl4.Street_Number__c  = '4';
        sl4.Street_Type__c  = 'Ave';
        sl4.Street_Direction__c  = 'E';
        sl4.City__c = 'City';
        sl4.Province_Code__c = 'ON';
        sl4.CLLI_Code__c = '223115';
        sl4.Access_Type_Group__c = 'Fibre';
        insert sl4;
        sl4Id = sl4.id;
    }   */
    
    private static void setupSites(){
        oSite1 = CPQ_Test_Setup.createSite(opp, oServiceableLocation1, STREET_NUMBER, CITY);
        oSite1.Suite_Floor__c = '11a';
        oSite1.Street_Name__c = 'Somewhere1';
        oSite1.Street_Number__c  = '4';
        oSite1.City__c = 'City';
        oSite1.Postal_Code__c = 'A1A1A1';
        oSite1.Province_Code__c = 'ON';
        oSite1.Is_a_Z_Site__c = true;
        insert oSite1;
        oSite2 = CPQ_Test_Setup.createSite(opp, oServiceableLocation2, '5', CITY);
        oSite2.Suite_Floor__c = '11a';
        oSite2.Street_Name__c = 'Someplace2';
        oSite2.Street_Number__c  = '5';
        oSite2.City__c = 'City';
        oSite2.Postal_Code__c = 'A1A1A2';
        oSite2.Province_Code__c = 'ON';
        insert oSite2;
        oSite3 = CPQ_Test_Setup.createSite(opp, oServiceableLocation3, '3', CITY);
        oSite3.Suite_Floor__c = '11a';
        oSite3.Street_Name__c = 'Somewhere3';
        oSite3.Street_Number__c  = '5';
        oSite3.City__c = 'City';
        oSite3.Postal_Code__c = 'A1A1A3';
        oSite3.Province_Code__c = 'ON';
        oSite3.Is_a_Z_Site__c = true;
        insert oSite3;
        oSite4 = CPQ_Test_Setup.createSite(opp, oServiceableLocation4, '4', CITY);
        oSite4.Suite_Floor__c = '11a';
        oSite4.Street_Name__c = 'Someplace4';
        oSite4.Street_Number__c  = '5';
        oSite4.City__c = 'City';
        oSite4.Postal_Code__c = 'A1A1A4';
        oSite4.Province_Code__c = 'ON';
        insert oSite4;
        oSite5 = CPQ_Test_Setup.createSite(opp, null, '5', CITY);
        oSite5.Suite_Floor__c = '11a';
        oSite5.Street_Name__c = 'Somewhere5';
        oSite5.Street_Number__c  = '5';
        oSite5.City__c = 'City';
        oSite5.Postal_Code__c = 'A1A1A5';
        oSite5.Province_Code__c = 'AB';
        oSite5.Is_a_Z_Site__c = true;
        insert oSite5;
        oSite6 = CPQ_Test_Setup.createSite(opp, null, '6', CITY);
        oSite6.Suite_Floor__c = '11a';
        oSite6.Street_Name__c = 'Someplace6';
        oSite6.Street_Number__c  = '6';
        oSite6.City__c = 'City6';
        oSite6.Postal_Code__c = 'A1A1A6';
        oSite6.Province_Code__c = 'AB';
        insert oSite6;
    }

    private static void createQuoteRequest(){
        qRequest = CPQ_Test_Setup.newQuoteRequest(oAccount, opp, oQuote);
        insert qRequest;
    }

    static testMethod void testQuoteRequest(){
        createTestData();
        setupSites();
        createQuoteRequest();
       
        Test.StartTest();
        PageReference pageRef = new PageReference('/apex/massApplyZSite?retUrl=' + String.valueOf(opp.Id)+'&oppId=' + String.valueOf(opp.Id)+ '&qRequestId='+String.valueOf(qRequest.Id));
        Test.setCurrentPage(pageRef);
        MassApplyZSitesController controller = new MassApplyZSitesController();
        // 1, 3 and 5 are z Sites
        // 2, 4 and 6 are not z Sites.
        for (SiteWrapper s : controller.getProspectSites()){
            if ((s.Site.Id == oSite2.Id) || (s.Site.Id == oSite4.Id)){
                s.setIsSelected(true);
            }else{
                s.setIsSelected(false);
            }
        } 
        for (SiteWrapper s : controller.lstZSites){
            if (s.Site.Id == oSite1.Id){
                s.setIsSelected(true);
            }else{
                s.setIsSelected(false);
            }
        }  
        controller.updateZSites();
        if (withAssertions){
            System.assertEquals(controller.lstZSites.size(), 3);
            System.assertEquals(controller.getProspectSites().size(), 3);
        }
       
        for (SiteWrapper s : controller.getProspectSites()){
            if ((s.Site.Id == oSite2.Id) || (s.Site.Id == oSite4.Id)){
                s.setIsSelected(true);
            }else{
                s.setIsSelected(false);
            }
        } 
        for (SiteWrapper s : controller.lstZSites){
            if (s.Site.Id == oSite1.Id){
                s.setIsSelected(true);
            }else{
                s.setIsSelected(false);
            }
        }  
       controller.addSites();
       controller.loadQRequestsLocations();
       if (withAssertions){
            System.assertEquals(controller.lstZSites.size(), 3);
            System.assertEquals(controller.getProspectSites().size(), 3);
        }
       
       for (SiteWrapper s : controller.getProspectSites()){
            if ((s.Site.Id == oSite2.Id) || (s.Site.Id == oSite4.Id)){
                s.setIsSelected(true);
            }else{
                s.setIsSelected(false);
            }
        } 
        for (SiteWrapper s : controller.lstZSites){
            if (s.Site.Id == oSite1.Id){
                s.setIsSelected(true);
            }else{
                s.setIsSelected(false);
            }
        }  
       controller.removeSites();
       if (withAssertions){
            System.assertEquals(controller.lstZSites.size(), 3);
            System.assertEquals(controller.getProspectSites().size(), 3);
        }
       
       PageReference pgOpp = controller.returnToOpportunity();
       PageReference pgQR = controller.returnToQuoteRequest();
       if (withAssertions){
            System.assertNotEquals(pgOpp, null);
            System.assertNotEquals(pgQR, null);
        }
       Test.StopTest();
    }

    static testMethod void SearchTest() {
       
       createTestData();
       setupSites();
       createQuoteRequest();
       
       test.startTest();
       PageReference pageRef = new PageReference('/apex/massApplyZSite?retUrl=' + String.valueOf(opp.Id)+'&oppId=' + String.valueOf(opp.Id));
       Test.setCurrentPage(pageRef);
       MassApplyZSitesController controller = new MassApplyZSitesController();
       if (withAssertions){
        System.assertEquals(controller.lstZSites.size(), 3);
        System.assertEquals(controller.getProspectSites().size(), 3);
       }
        Apexpages.currentPage().getParameters().put('streetNumber', '');
        Apexpages.currentPage().getParameters().put('streetName', '');
        Apexpages.currentPage().getParameters().put('city', '');
        Apexpages.currentPage().getParameters().put('provinceCode', 'AB');
        controller.runSearch();
        if (withAssertions){
            Set<Id> test = new Set<Id>();
            boolean found = false;
            for (SiteWrapper s : controller.getProspectSites()){
                test.add(s.Site.Id);
            }
            System.assertEquals(test.size(), 1);
            System.assertEquals(test.contains(oSite6.Id), true);
        }
        Apexpages.currentPage().getParameters().put('streetNumber', '');
        Apexpages.currentPage().getParameters().put('streetName', 'Someplace');
        Apexpages.currentPage().getParameters().put('city', '');
        Apexpages.currentPage().getParameters().put('provinceCode', 'ON');
        controller.runSearch();
        if (withAssertions){
            Set<Id> test = new Set<Id>();
            boolean found = false;
            for (SiteWrapper s : controller.getProspectSites()){
                test.add(s.Site.Id);
            }
            System.assertEquals(test.size(), 2);
            System.assertEquals(test.contains(oSite2.Id), true);
            System.assertEquals(test.contains(oSite4.Id), true);
        }
        Apexpages.currentPage().getParameters().put('streetNumber', '');
        Apexpages.currentPage().getParameters().put('streetName', '');
        Apexpages.currentPage().getParameters().put('city', 'City');
        Apexpages.currentPage().getParameters().put('provinceCode', '');
        controller.runSearch();
        if (withAssertions){
            Set<Id> test = new Set<Id>();
            boolean found = false;
            for (SiteWrapper s : controller.getProspectSites()){
                test.add(s.Site.Id);
            }
            System.assertEquals(test.size(), 3);
        }
        Apexpages.currentPage().getParameters().put('streetNumber', '');
        Apexpages.currentPage().getParameters().put('streetName', '');
        Apexpages.currentPage().getParameters().put('city', 'City6');
        Apexpages.currentPage().getParameters().put('provinceCode', '');
        controller.runSearch();
        if (withAssertions){
            Set<Id> test = new Set<Id>();
            boolean found = false;
            for (SiteWrapper s : controller.getProspectSites()){
                test.add(s.Site.Id);
            }
            System.assertEquals(test.size(), 1);
            System.assertEquals(test.contains(oSite6.Id), true);
        }
        Apexpages.currentPage().getParameters().put('streetNumber', '5');
        Apexpages.currentPage().getParameters().put('streetName', '');
        Apexpages.currentPage().getParameters().put('city', '');
        Apexpages.currentPage().getParameters().put('provinceCode', '');
        controller.runSearch();
        if (withAssertions){
            Set<Id> test = new Set<Id>();
            boolean found = false;
            for (SiteWrapper s : controller.getProspectSites()){
                test.add(s.Site.Id);
            }
            System.assertEquals(test.size(), 2);
            System.assertEquals(test.contains(oSite2.Id), true);  
            System.assertEquals(test.contains(oSite4.Id), true);
        }
        controller.returnToOpportunity();
        Test.StopTest();
    }
    
    static testMethod void addZSitesTest() {
        createTestData();
        setupSites();
        Test.StartTest();
        PageReference pageRef = new PageReference('/apex/massApplyZSite?retURL=' + String.valueOf(opp.Id)+'&oppId=' + String.valueOf(opp.Id));
        Test.setCurrentPage(pageRef);
        MassApplyZSitesController controller = new MassApplyZSitesController();
        Apexpages.currentPage().getParameters().put('streetNumber', '');
        Apexpages.currentPage().getParameters().put('streetName', '');
        Apexpages.currentPage().getParameters().put('city', 'City');
        Apexpages.currentPage().getParameters().put('provinceCode', '');
        if (withAssertions){
            System.assertEquals(controller.lstZSites.size(), 3);
            System.assertEquals(controller.getProspectSites().size(), 3);
        }
        for (SiteWrapper s : controller.getProspectSites()){
            if ((s.Site.Id == oSite4.Id) || (s.Site.Id == oSite6.Id)){
                s.setIsSelected(true);
            }else{
                s.setIsSelected(false);
            }
        }  
        // Check what happens if no z sites are selected
        controller.runSearch();
        controller.updateZSites();
        Map<Id, Id> test1 = new Map<Id, Id>();
        for (SiteWrapper sw : controller.getProspectSites()){
            if (sw.Site.Z_Site__c != null){
                test1.put(sw.Site.Id, sw.Site.Z_Site__c);
            }
        }
        System.assertEquals(test1.size(), 0);
        // Now we have a z site selected.
        for (SiteWrapper z : controller.lstZSites){
            if (z.Site.Id == oSite1.Id){
                z.setIsSelected(true);
            }else{
                z.setIsSelected(false);
            }
        } 
        controller.updateZSites();
        test1 = new Map<Id, Id>();
        for (SiteWrapper sw : controller.getProspectSites()){
            if (sw.Site.Z_Site__c != null){
                test1.put(sw.Site.Id, sw.Site.Z_Site__c);
            }
        }
        // Clear the ZSites for all but site4
        for (SiteWrapper s : controller.getProspectSites()){
            if (s.Site.Id != oSite4.Id){
                s.setIsSelected(true);
            }else{
                s.setIsSelected(false);
            }
        }
        if (withAssertions){
            System.assertEquals(controller.lstZSites.size(), 3);
            System.assertEquals(controller.getProspectSites().size(), 3);
        }
        controller.removeZSites();
        test1 = new Map<Id, Id>();
        for (SiteWrapper sw : controller.getProspectSites()){
            if (sw.Site.Z_Site__c != null){
                test1.put(sw.Site.Id, sw.Site.Z_Site__c);
            }
        }
        System.assertEquals(test1.size(), 0);
        Test.StopTest();
    }
    
    static testMethod void noSitesTest() {
        createTestData();
        Test.StartTest();
        PageReference pageRef = New PageReference('/apex/massApplyZSite?retURL=' + String.valueOf(opp2.Id)+'&oppId=' + String.valueOf(opp2.Id)); 
        Test.setCurrentPage(pageRef);
        MassApplyZSitesController controller = new MassApplyZSitesController();
        if (withAssertions){
            System.assertEquals(controller.lstZSites.size(), 0);
            System.assertEquals(controller.getProspectSites().size(), 0);
        }
       Test.StopTest();
    }
}