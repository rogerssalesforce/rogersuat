/*********************************************************************************
Class Name      : CPQ_ReadyForReviewonCartController
Description     : This class is called on click of "Ready for Review" button on Cart
Created By      : Deepika Rawat
Created Date    : 13-Oct-2015
Modification Log:
---------------------------------------------------------------------------------------
Developer                   Date                   Description
--------------------------------------------------------------------------------------- 
Deepika Rawat            13-Oct-2015            Original Version
Deepika Rawat            28-Jan-2016            Changes to SE Status based on US-535
Joseph Toms               03-Feb-2016           Us-0442 Added validation to check line item exists
*********************************************************************************/
public with sharing class CPQ_ReadyForReviewonCartController {
    public String sCartId {get;set;}
    public String sProposalId;
    public String sFlow;
    public String sConfigRequestId;
    public String sProductConfigId;
    public List<Apttus_Proposal__Proposal__c> lstProposal;
    public List<Apttus_Config2__LineItem__c> lstLineItems;
    /*********************************************************************************
    Method Name    : CPQ_ReadyForReviewonCartController
    Description    : Constructor method
    Return Type    : 
    Parameter      :
    *********************************************************************************/
    public CPQ_ReadyForReviewonCartController (){
        sCartId = ApexPages.currentPage().getParameters().get('id');  
        sProductConfigId = ApexPages.currentPage().getParameters().get('id');
        sConfigRequestId = ApexPages.currentPage().getParameters().get('configRequestId');
        sFlow = ApexPages.currentPage().getParameters().get('flow');   
        Apttus_Config2__ProductConfiguration__c oProdConfig = [Select id,Apttus_Config2__BusinessObjectId__c from Apttus_Config2__ProductConfiguration__c where id =: sCartId]; 
        sProposalId = oProdConfig.Apttus_Config2__BusinessObjectId__c;
        
        System.Debug('CPQ_ReadyForReviewonCartController.updateQuote() - Limits.getQueries() CONSTRUCTOR ' + Limits.getQueries());
        System.Debug('CPQ_ReadyForReviewonCartController.updateQuote() - Limits.getLimitQueries() CONSTRUCTOR ' + Limits.getLimitQueries());
   }
   /*********************************************************************************
    Method Name    : updateQuote
    Description    : This method first finalizes the cart and then updates Quote on 
                        click of "Ready for Review" on Cart
    Return Type    : 
    Parameter      :
    *********************************************************************************/
    public PageReference updateQuote(){
        if(sProposalId!=null && sProposalId!=''){
            //Reprice Cart
            Apttus_CpqApi.CPQ.UpdatePriceRequestDO objUpdatePriceRequestDO = new Apttus_CpqApi.CPQ.UpdatePriceRequestDO();
            objUpdatePriceRequestDO.CartId = sCartId; 
            Apttus_CpqApi.CPQ.UpdatePriceResponseDO result = Apttus_CpqApi.CPQWebService.updatePriceForCart(objUpdatePriceRequestDO);
            //Finalize Cart
            CPQ_Settings__c cpqSetting = CPQ_Settings__c.getOrgDefaults();
            Apttus_CPQApi.CPQ.FinalizeCartRequestDO request = new Apttus_CPQApi.CPQ.FinalizeCartRequestDO();
            request.CartId = sCartId; 
            Apttus_CPQApi.CPQ.FinalizeCartResponseDO resp = Apttus_CPQApi.CPQWebService.finalizeCart(request); 
            //Update Quote
            Boolean bRequireTechReview = false;
            Boolean bDealDeskReview = false;
            Boolean bCustomProductPresent = false;
            Boolean bWirelessMobileProdPresent = false;
            Boolean bIOTApplicationsPresent = false;
            Boolean bUnifiedCommPresent = false;
            Boolean bWirelinePresent =false;
            Boolean bDataCentres = false;
            
            System.Debug('CPQ_ReadyForReviewonCartController.updateQuote() - Limits.getQueries() START ' + Limits.getQueries());
            System.Debug('CPQ_ReadyForReviewonCartController.updateQuote() - Limits.getLimitQueries() START ' + Limits.getLimitQueries());
            
            RecordType lockedRecType = [Select Id,DeveloperName From RecordType  Where SobjectType = 'Apttus_Proposal__Proposal__c' and DeveloperName =: cpqSetting.Proposal_Soln_Under_Review_RecordType__c ]; 
            List<Apttus_Config2__ProductConfiguration__c> lstProductConfig = [Select id, Apttus_QPConfig__Proposald__c, Apttus_Config2__Status__c from  Apttus_Config2__ProductConfiguration__c where Apttus_QPConfig__Proposald__c =:sProposalId and Apttus_Config2__Status__c !=:'Superseded' ];
            lstProposal= [select id, RecordTypeId , Apttus_Proposal__Approval_Stage__c, CSOW_required__c from Apttus_Proposal__Proposal__c where id=:sProposalId];
            lstLineItems = [select IsCustomProduct__c, CSOW_Required__c, Rogers_ProductFamily__c, COPTeam__c, Product_TechnicalReviewRequired__c, Apttus_Config2__ProductId__r.CSOW_Required__c, Apttus_Config2__OptionId__r.CSOW_Required__c, Apttus_Config2__ProductId__r.COR_Required__c, Apttus_Config2__LineType__c from Apttus_Config2__LineItem__c where Apttus_Config2__ConfigurationId__c =:lstProductConfig[0].id];  
            // if no line item added show error message 
            if(lstLineItems==null || lstLineItems.size()==0 ){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CPQ_ReadyForReviewNoLineItem));
                return null;
            }
            for(Apttus_Config2__LineItem__c item : lstLineItems){
                //Check if there is any product in the cart with Requires Technical Review marked as true or COR is required
                if(item.Product_TechnicalReviewRequired__c ==true || item.Apttus_Config2__ProductId__r.COR_Required__c ==true ){
                    bRequireTechReview = true;
                }
                // Check if any Custom Product is present
                if(item.IsCustomProduct__c == true){
                    bCustomProductPresent = true;
                }
                if(item.IsCustomProduct__c == true && item.COPTeam__c == 'Wireless Contract Team' ){
                    bDealDeskReview = true;
                }
                if(item.Rogers_ProductFamily__c!=null && item.Rogers_ProductFamily__c.containsIgnoreCase(cpqSetting.Wireless_Mobile__c)){
                    bWirelessMobileProdPresent=true;
                }
                 if(item.Rogers_ProductFamily__c!=null && item.Rogers_ProductFamily__c.containsIgnoreCase(cpqSetting.IOTApplications__c)){
                    bIOTApplicationsPresent=true;
                }
                 if(item.Rogers_ProductFamily__c!=null && item.Rogers_ProductFamily__c.containsIgnoreCase(cpqSetting.Unified_Comm_Collaboration__c)){
                    bUnifiedCommPresent = true;
                }
                if(item.Rogers_ProductFamily__c!=null && item.Rogers_ProductFamily__c.containsIgnoreCase(cpqSetting.Wireline__c)){
                    bWirelinePresent=true;
                }
                 if(item.Rogers_ProductFamily__c!=null && item.Rogers_ProductFamily__c.containsIgnoreCase(cpqSetting.Data_Centres__c)){
                    bDataCentres=true;
                }
                if(item.Apttus_Config2__ProductId__r.CSOW_Required__c && item.Apttus_Config2__LineType__c.equalsIgnoreCase('Product/Service')){
                    lstProposal[0].CSOW_required__c =true;
                }
                else if(item.Apttus_Config2__OptionId__r.CSOW_Required__c && item.Apttus_Config2__LineType__c.equalsIgnoreCase('Option')){
                    lstProposal[0].CSOW_required__c =true;    
                }
                else if(item.IsCustomProduct__c && item.CSOW_Required__c){
                    lstProposal[0].CSOW_required__c =true; 
                }
            }
           // Logic to populate Apttus_Proposal__Approval_Stage__c for US-169: Set Apttus_Proposal__Approval_Stage__c based on if custom products are added and product family
           if(bCustomProductPresent==false && bWirelessMobileProdPresent==true && (bWirelinePresent ==false && bUnifiedCommPresent == false && bDataCentres ==false && bIOTApplicationsPresent==false )){
                lstProposal[0].Apttus_Proposal__Approval_Stage__c = 'Approval Required';
            }
            else{   
                lstProposal[0].Apttus_Proposal__Approval_Stage__c = 'Solution Under Review';
            }          
            
            //Logic to populate SE_Status__c for US-169: Set SE_Status__c based on if custom products are added and product family
            /*if((bWirelessMobileProdPresent==true ) && (bWirelinePresent==false && bUnifiedCommPresent==false && bDataCentres==false && bIOTApplicationsPresent==false))
                lstProposal[0].SE_Status__c = 'Not Required';
            else
                lstProposal[0].SE_Status__c = 'Pending Review';
            */

            // Logic to populate SE_Status__c for US-535: Set SE_Status__c based on Technical Review Required field on product.
            if(bRequireTechReview == false)
                lstProposal[0].SE_Status__c = 'Not Required';
            else{
                lstProposal[0].SE_Status__c = 'Pending Review';   
                lstProposal[0].Apttus_Proposal__Approval_Stage__c = 'Solution Under Review';
            }
            //Set Deal_Desk_Status__c based on if custom products are added and product family
            if((bCustomProductPresent==true && ( bWirelessMobileProdPresent==true )) || (bDealDeskReview)){
                lstProposal[0].Deal_Desk_Status__c = 'Pending Review';
                lstProposal[0].Apttus_Proposal__Approval_Stage__c = 'Solution Under Review';
            }
            else{
                lstProposal[0].Deal_Desk_Status__c = 'Not Required';
            }
            lstProposal[0].RecordTypeId = lockedRecType.id;

            update lstProposal[0];
            Apttus_CPQApi.CPQ.FinalizeCartRequestDO request2 = new Apttus_CPQApi.CPQ.FinalizeCartRequestDO();
            request.CartId = sCartId; 
            Apttus_CPQApi.CPQ.FinalizeCartResponseDO resp2 = Apttus_CPQApi.CPQWebService.finalizeCart(request); 
        }    
        
        System.Debug('CPQ_ReadyForReviewonCartController.updateQuote() - Limits.getQueries() END ' + Limits.getQueries());
        System.Debug('CPQ_ReadyForReviewonCartController.updateQuote() - Limits.getLimitQueries() END ' + Limits.getLimitQueries());
        
        PageReference refreshPage = new PageReference('/'+sProposalId);
        return refreshPage;
    } 
    
       /*********************************************************************************
    Method Name    : backToQuoteCart
    Description    : Returns back to Cart
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference backToCart(){       
        pageReference pr = new PageReference(Site.getPathPrefix()+'/apex/Apttus_Config2__Cart?configRequestId='+sConfigRequestId+'&id='+sProductConfigId+'&flow='+sFlow+'&launchState=cart#/cart');
        pr.setRedirect(false);
        return pr;
       
    }
}