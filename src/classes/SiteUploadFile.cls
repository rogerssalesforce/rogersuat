public with sharing class SiteUploadFile {
    String[] filelines = new String[]{};
    String[] recordlines = new String[]{};
    List<Site__c> accstoupload;
    public Blob contentFile { get; set; }
    public String nameFile { get; set; }
    public String nameFiles { get; set; }
    public Integer rowCount { get; set; }
    public Integer colCount { get; set; }
    //public String test {get; set;}
    public String CSVReturn {get; set;}
    public String CSVReturn2 {get; set;}
    public String CSVReturn1 {get; set;}
    public String CSVReturn3 {get; set;}
    public id oppID {get; set;}
    public List<Site_Upload_Records> UploadRecords {get; set;}
    public Map<integer,string> parseMap = new Map<integer,string>();
    public Map<integer,integer> insertRecord = new Map<integer,integer>();
    //public String strDocumentLink {get; set;}
    //public String strUserManualLink {get; set;}
    String constDocumentName = 'Excel Template';
    String constUserManual = 'User Manual';
    String constFolder = 'Folder';
    integer constIMEIlenght = 8;
    
    //Error Label
    //String test = Label.<custom label name>;
    
    
    //Mapping inserted record for status
    public Map<integer,Site_Upload_Records> Siteupload = new Map<integer,Site_Upload_Records>();
    
    public Set<String> CSVHeader;
    
    Boolean blnGlobalError = false;
    
    //Constructor
    public SiteUploadFile ()
    {
    	oppId = ApexPages.currentPage().getParameters().get('RetURL');
        system.debug('TEST OPP ID ' + oppID);        
    }
    
    public Pagereference ReadFile()
    {
        blnGlobalError = false;
        
        //Check if user upload file
        if (contentFile == null){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, label.Invalid_File);
                ApexPages.addMessage(myMsg);
                //Exit Fatal Error Missing Header
                return null;
        }
        //End Check
        
        
        //Refresh Variable
        UploadRecords = new List<Site_Upload_Records>();
        parseMap = new Map<integer,string>();
        Siteupload = new Map<integer,Site_Upload_Records>();
        CSVHeader = new Set<String>();
        insertRecord =  new Map<integer,integer>();
        accstoupload = new List<Site__c>();
                
        //SL test start
                ///system.debug('SL-test: ReadFile: ' );
                //SL test end

        //Get All Field Specified in Site
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
                Schema.SObjectType ctype = gd.get('Site__c');
                
                Map<String, Schema.SobjectField> fmap = ctype.getDescribe().fields.getMap();
        
        //Get Field Assignment  
        List <Field_Assignment__c> lA = new List<Field_Assignment__c> ();
        Map <string,String> mapLA = new Map <String,String> ();
        lA = [select id,CSV_Field__c,Target_Field__c from Field_Assignment__c where Active__c = true AND Type__c = 'Prospect Sites'];
        
        
        Map <Integer,String> mapRow = new Map <Integer,String>();
        
        
        
        //Check if the fields specified in Field Assignment exist in Site Table
        String strCheckFieldError ='';
        for (Field_Assignment__c fassign : lA)
        {
                If (!(fmap.containsKey(fassign.Target_Field__c)))
                {
                        strCheckFieldError = strCheckFieldError + fassign.Target_Field__c + '\r\n';     
                }
        }
        if (strCheckFieldError != '')
        {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, label.Incorrect_Target_Field + '\r\n Error :'  + strCheckFieldError);
                ApexPages.addMessage(myMsg);
                //Exit Fatal Error Incorrect Target Field in Field Assignment
                return null;
        }
        //End Check
        
        system.debug('lA=' + lA);
        system.debug('fmap=' + fmap);
        
        //Check if all header specify in Field Assignment exist in CSV
        if(lA.size() > 0)
        {
                for(Field_Assignment__c f : lA)
                {
                        mapLA.put(f.CSV_Field__c,f.Target_Field__c);
                }
        }
        
        
        List<List<String>> parsedCSV = new List<List<String>>();
        rowCount = 0;
        colCount = 0;
        
        //Begin Parsing CSV File
        if (contentFile != null){
            String fileString = contentFile.toString();
            parsedCSV = parseCSV(fileString, false);
            rowCount = parsedCSV.size();
                
                nameFiles = contentFile.toString();
                recordlines = nameFiles.split('\r');
                
                //Add error and CSV Record to Map
                        Site_Upload_Records hh = new Site_Upload_Records();
                hh.CSV_File_Record = recordlines[0];
                hh.Record_Status = 'Status / Statut';
                ///system.debug('ADD Siteupload 1');
                Siteupload.put(0,hh);
                
                
                system.debug('recordlines=' + recordlines);
                
                
                //Build mapping for output CSV
                for (Integer i=0;i<recordlines.size();i++)
                {
                        parseMap.put(i,recordlines[i]);
                        
                }       
                
                
                system.debug('parseMap=' + parseMap);
                
                Integer i = 0;
                
                system.debug('parsedCSV=' + parsedCSV);
                system.debug('mapLA=' + mapLA);
                
                //get Header in CSV File
                if(rowCount > 0)
                {       
                        for (List <String> row : parsedCSV)
                        {
                                for(String r : row)
                                {
                                        if(mapLA.containsKey(r))
                                        {
                                                ///system.debug(' ROW R ' + r  + ' Value ' + mapLA.get(r));
                                                mapRow.put(i,mapLA.get(r));
                                        }
                                        
                                        //Get All CSV Header
                                        if(!(CSVHeader.contains(r)))
                                        {
                                                CSVHeader.add(r);
                                        }
                                        i++;
                                }
                                break;
                        }
                }
                
                system.debug('CSVHeader=' + CSVHeader);
                system.debug('mapRow=' + mapRow);
                
                Boolean blnHeaderError = false;
                List <String> MissingCSVHeader = new List <String>();
                
                //Check if all header specify in Field Assignment exist in CSV
                if(lA.size() > 0)
                {
                        for(Field_Assignment__c f : lA)
                        {
                                if(!(CSVHeader.contains(f.CSV_Field__c)))
                                {
                                        blnHeaderError = true;
                                        MissingCSVHeader.add(f.CSV_Field__c);   
                                }
                        }
                }
                
                // Check error specified in Field Assignment but not in CSV Header 
                if(blnHeaderError){
                        String strMissingHeader = '';
                        for(String str:MissingCSVHeader){
                                strMissingHeader = strMissingHeader + str + ';';
                        }
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, label.Missing_Header + strMissingHeader);
                        ApexPages.addMessage(myMsg);
                        //Exit Fatal Error Missing Header
                        return null;
                }
                
                
                ///system.debug('NUMBER of ParsedCSV '  + parsedCSV.size());
                string strError = '';
                i = 0;
                Integer x = 0;
                Integer y = 1;
                //Site_Error_Record__c Siteh = new Site_Error_Record__c();
                //Siteh.Error_Records__c = parseMap.get(0);
                //Siteh.Error_Desc__c = 'HEADER';
                //RecordErrors.add(Siteh);
                
                Site_Upload_Records hu = new Site_Upload_Records();
                
                strError = '';
                //Parse Upload file per Line
                try 
                {
	            	for (List<String> row : parsedCSV){
		                if(i > 0)
		                {       
		                        
		                        Site__c a = new Site__c();
		                        a.Opportunity__c = oppID;
		                        system.debug('ROW ' + i + ' - ' + row);
		                        for(String r: row)
		                        {
		                                
		                                if(mapRow.containsKey(x))
		                                        {
		                                        
		                                        system.debug('THis is the ID ' + mapRow.get(x) + ' and row value ' + row[x]);
		                                        
		                                        // Error Check before insert and mapped field
		                                        if(fmap.containsKey(mapRow.get(x)))
		                                        {
		                                                system.debug('fmap.containsKey(mapRow.get(x)) = ' + fmap.containsKey(mapRow.get(x)));
		                                                
		                                                if(fmap.get(mapRow.get(x)).getDescribe().getType()==Schema.DisplayType.Currency)
		                                                {
		                                                        system.debug('currency');
		                                                        //string cleanText = row[x].replace('$','');
		                                                        //cleanText = cleanText.replace('$','');
		                                                        ///cleanText = cleanText.trim();
		                                                        
		                                                        try
		                                                        {
		                                                                ///decimal dec = decimal.valueOf(cleanText);       
		                                                                a.put(mapRow.get(x),decimal.valueOf(row[x].trim().replace('$','')));
		                                                        }
		                                                        catch (Exception e)
		                                                        {
		                                                                strError = strError + label.Incorrect_Currency;
		                                                        }
		                                                        
		                                                }
		                                                if(fmap.get(mapRow.get(x)).getDescribe().getType()==Schema.DisplayType.String)
		                                                {
		                                                	    	system.debug('string');
		                                                	    	a.put(mapRow.get(x),row[x]);
		                                                }
		                                                if(fmap.get(mapRow.get(x)).getDescribe().getType()==Schema.DisplayType.TextArea)
		                                                {
		                                                        system.debug('text area');
		                                                        a.put(mapRow.get(x),row[x]);
		                                                        
		                                                }
		                                                if(fmap.get(mapRow.get(x)).getDescribe().getType()==Schema.DisplayType.Boolean)
		                                                {
		                                                	system.debug('boolean');
		                                                	try {
													            Boolean b = 'true'.equalsIgnoreCase(row[x]);
													            a.put(mapRow.get(x),b);
													        }catch(Exception e) {
													            System.debug('******** THERE WAS AN EXCEPTION: ' + e.getMessage());
													        }
		                                                        
		                                                        
		                                                }
		                                                if(fmap.get(mapRow.get(x)).getDescribe().getType()==Schema.DisplayType.Picklist)
		                                                {
		                                                        system.debug('picklist');
		                                                        a.put(mapRow.get(x),row[x]);
		                                                        
		                                                }
		                                                if(fmap.get(mapRow.get(x)).getDescribe().getType()==Schema.DisplayType.Date)
		                                                {
		                                                        //string cleanDate = row[x].replace('\','');
		                                                        //cleanDate = cleanDate.replace('\'','');
		                                                        ///string cleanDate = row[x].trim();
		                                                        
		                                                        system.debug('date');
		                                                        
		                                                        try{
		                                                                //Date myDate = date.valueOf(cleanDate);
		                                                                
		                                                                a.put(mapRow.get(x),date.valueOf(row[x].trim()));
		                                                        }
		                                                        catch (Exception e)
		                                                        {
		                                                                strError = strError + label.Invalid_Date + ';';
		                                                        }
		                                                }
		                                                if(fmap.get(mapRow.get(x)).getDescribe().getType()==Schema.DisplayType.Integer)
		                                                {
		                                                        
		                                                        system.debug('integer');
		                                                        ///string cleanText = row[x].trim();
		                                                        //cleanText = cleanText.trim();
		                                                        
		                                                        try
		                                                        {
		                                                                ///decimal dec = decimal.valueOf(cleanText);       
		                                                                a.put(mapRow.get(x),decimal.valueOf(row[x].trim()));
		                                                        }
		                                                        catch (Exception e)
		                                                        {
		                                                                strError = strError + label.Incorrect_Decimal + ';';
		                                                        }
		                                                        
		                                                }
		                                                /*if(fmap.get(mapRow.get(x)).getDescribe().getType()==Schema.DisplayType.Reference) {
		                                                  system.debug('id');
		                                                  try {
                                                                a.put(mapRow.get(x),Id.valueOf(row[x].trim()));
                                                          } catch (Exception e) {
                                                                strError = strError + label.Incorrect_Decimal + ';';
                                                          }
		                                                }*/
		                                                system.debug('*******************');
		                                                
		                                        }
		                                }
		                                x++;
		                                
		                        }
		                        x = 0;
		                        System.debug('SL-test: strError=' + strError); 
		                        
		                        if (strError != '')
		                        {
		                                //Add error and CSV Record to Map
		                                ///hu = new Site_Upload_Records();
		                                ///hu.CSV_File_Record = parseMap.get(i);
		                                ///hu.Record_Status = strError;
		                                ///system.debug('ADD Siteupload 2');
		                                Siteupload.put(i,new Site_Upload_Records(parseMap.get(i),strError));
		                                blnGlobalError = true;
		                                ///system.debug('\t' + i+ ' THIS ERROR before insert Add to List ') ;
		                                strError = '';
		                        }
		                        else
		                        {
		                                //Pass First Error Check and put it to List for Insert
		                                ///system.debug ( '\n ReadFile: add to accstoupload ' + a.MIN_number__c);
		                                insertRecord.put(y,i);
		                                accstoupload.add(a);
		                                y++;
		                        }
		                }
		                i++;    
	                
	            	}
                }
                catch (Exception e)
                {
                	ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,  label.Maximum_Size);
            		ApexPages.addMessage(errormsg);
                }
         }


                // SLtest:start 
                ///System.debug ( '\nSL-Test upload DHR before insert \n \n');
                // SLtest:end 
        
        system.debug('accstoupload=' + accstoupload);
        
        //Write to Database
        try{
                Site_Upload_Records hu = new Site_Upload_Records();
                String strError = '';
                ///system.debug('\n Execute INSERT  after try\n');
//            insert( accstoupload);
 
                        //All or nothing = false
            	Database.SaveResult[] MySaveResult = Database.Insert( accstoupload, false);
                
                ///system.debug('\n Execute INSERT after insert');
                system.debug('\t MySaveResult.size: ' + MySaveResult.size());
                
                integer i = 1;
                integer result = 0; 
                
                //Gather Return pointer from Database result for Success or Error
                for(Database.SaveResult sr:MySaveResult){
                        
                        result = insertRecord.get(i);
                        if(sr.isSuccess())
                        {
                                if(!(Siteupload.containsKey(result)))
                                {       
                                        //Add success and CSV Record to Map
                                       /// hu = new Site_Upload_Records();
                                        ///hu.CSV_File_Record = parseMap.get(result);
                                        ///hu.Record_Status = label.Success;
                                        ///system.debug('ADD Siteupload 3');
                                        Siteupload.put(result,new Site_Upload_Records(parseMap.get(result),label.Success));
                                        system.debug('\t' + result+ ' No ERROR  ') ;
                                }
                        } 
                        else 
                        {
                                Database.Error[] err = sr.getErrors();
                                strError = '';
                                for(Database.Error e : err)
                                {
                                        
                                        if(e.getMessage().contains('duplicate value found'))
                                        {
                                                strError = strError + label.Duplicate_Record + ';';
                                        }
                                        else if (e.getMessage().contains('Transaction date:'))
                                        {
                                                strError = strError + label.Invalid_Date + ';';
                                        }
                                        else
                                        {
                                                strError = strError + e.getMessage() + ';';
                                        }
                                        //strError = strError +  + ';';
                                        system.debug('\t'+ result+ ' ERROR MSG: ' + e.getMessage());
                                }
                                if(!(Siteupload.containsKey(result)))
                                {       
                                        ///hu = new Site_Upload_Records();
                                        //Add error and CSV Record to Map
                                        ///hu.CSV_File_Record = parseMap.get(result);
                                        ///hu.Record_Status = strError;
                                        ///system.debug('ADD Siteupload 4');
                                        Siteupload.put(result,new Site_Upload_Records( parseMap.get(result),strError));
                                        blnGlobalError = true;
                                        system.debug('\t' + result + ' THIS ERROR after insert Add to List ') ;
                                }
                                
                        }
                        i++;
                }

                system.debug('Execute INSERT - after FOR \n');
        }
        catch (Exception e)
        {
                system.debug ('\n Execute INSERT - catch, before for' );
                
                String strFatalError = '';
                for (Integer i = 0; i < e.getNumDml(); i++) {
                        strFatalError = strFatalError + e.getDMLMessage(i) + '\r\n';
                        
                    }
                                
                
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,  label.Fatal_Error + '\r\n' + strFatalError);
            ApexPages.addMessage(errormsg);
        }    
        finally
        {
        	try
        	{
                //Build mapping for output CSV
                for (Integer i=0;i<Siteupload.size();i++)
                {
                        if(Siteupload.containsKey(i))
                        
                        {
                                
                                UploadRecords.add(Siteupload.get(i));
                                
                        }
                }
                system.debug('LOAD UPLOADRECORDS ' + UploadRecords.size());
                CSVReturn = '';
                CSVReturn1 = '';
                CSVReturn2 = '';
                CSVReturn3 = '';
                integer count = 0;
                for(Site_Upload_Records g : UploadRecords)
                {
                        if(count < 1000)
                        {
                                CSVReturn = CSVReturn + g.CSV_File_Record + ',' +g.Record_Status + '\r\n';
                                
                        }
                        if(count >= 1000 && count < 2000)
                        {
                                CSVReturn1 = CSVReturn1 + g.CSV_File_Record + ',' +g.Record_Status + '\r\n';
                                
                        }
                        if(count >= 2000 && count < 3000)
                        {
                                CSVReturn2 = CSVReturn2 + g.CSV_File_Record + ',' +g.Record_Status + '\r\n';
                                
                        }
                        if(count >= 3000)
                        {
                                CSVReturn3 = CSVReturn3 + g.CSV_File_Record + ',' +g.Record_Status + '\r\n';
                                
                        }
                        
                        count++;
                }
                
                if(blnGlobalError)
                {
                        ApexPages.Message errormsg1 = new ApexPages.Message(ApexPages.severity.ERROR,label.Error_Upload);
                ApexPages.addMessage(errormsg1);
                }
                else
                {
                        ApexPages.Message SuccessMsg = new ApexPages.Message(ApexPages.severity.CONFIRM,label.Success);
                ApexPages.addMessage(SuccessMsg);
                }
        	}
        	catch (Exception e)
        	{
        		ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,  label.Maximum_Size);
            	ApexPages.addMessage(errormsg);
        	}
        }
        
        
        
        return null;
    }
    
    
       
        /*
        Credit to
        http://wiki.developerforce.com/index.php/Code_Samples#Parse_a_CSV_with_APEX
        */
            public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
                List<List<String>> allFields = new List<List<String>>();
           
                // replace instances where a double quote begins a field containing a comma
                // in this case you get a double quote followed by a doubled double quote
                // do this for beginning and end of a field
                contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
                // now replace all remaining double quotes - we do this so that we can reconstruct
                // fields with commas inside assuming they begin and end with a double quote
                contents = contents.replaceAll('""','DBLQT');
                // we are not attempting to handle fields with a newline inside of them
                // so, split on newline to get the spreadsheet rows
                List<String> lines = new List<String>();
                try {
                    //lines = contents.split('\n'); //correction: this only accomodates windows files
                    lines = contents.split('\r'); // using carriage return accomodates windows, unix, and mac files
                    //http://www.maxi-pedia.com/Line+termination+line+feed+versus+carriage+return+0d0a
                } catch (System.ListException e) {
                    System.debug('Limits exceeded?' + e.getMessage());
                }
                Integer num = 0;
                for(String line: lines) {
                    // check for blank CSV lines (only commas)
                    if (line.replaceAll(',','').trim().length() == 0) break;
                   
                    List<String> fields = line.split(','); 
                    List<String> cleanFields = new List<String>();
                    String compositeField;
                    Boolean makeCompositeField = false;
                    for(String field: fields) {
                        if (field.startsWith('"') && field.endsWith('"')) {
                            cleanFields.add(field.replaceAll('DBLQT','"'));
                        } else if (field.startsWith('"')) {
                            makeCompositeField = true;
                            compositeField = field;
                        } else if (field.endsWith('"')) {
                            //compositeField += ',' + field;
                            cleanFields.add((compositeField + ',' + field).replaceAll('DBLQT','"'));
                            makeCompositeField = false;
                        } else if (makeCompositeField) {
                            compositeField +=  ',' + field;
                        } else {
                            cleanFields.add(field.replaceAll('DBLQT','"'));
                        }
                    }
                   
                    allFields.add(cleanFields);
                }
                if (skipHeaders) allFields.remove(0);
                return allFields;      
            }
            
        public PageReference openError()
        {
                
                return page.SiteErrorCSV;
        }
        public PageReference openUploadFile()
        {
                
                return page.SiteUploadFile;
        }
        
        public PageReference openReport()
        {
               
                String strLink = '';
                
                PageReference p = new PageReference('');
                p.setRedirect(true);
                
                return p;
        }
       /* 
        public PageReference openUserManual()
        {
                    
                PageReference p = new PageReference('');
            p.setRedirect(true);

                        if (strUserManualLink != '')
                        {            
                p = new PageReference(strUserManualLink);
                }
            
            else
            {
                    p = null;
            }
            return p;
        }
        */
        public PageReference createNew()
        {
                //Get All Field Specified in Site
                PageReference p = new PageReference('');
                p.setRedirect(true);
                
                return p;
        }
        public PageReference back()
        {
        	PageReference p = new PageReference('/'+ oppId);
            p.setRedirect(true);
            
        	return p;
               
        }
        
    public void pageInitialization()
    {
    	
    }
    
    public void pageExtInitialization()
    {
        
        System.debug( 'SL-test: pageInitialization');
        String strDocId = '';
        
    }  
}