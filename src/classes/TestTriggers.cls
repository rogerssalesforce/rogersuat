/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
/*
========================================================================
Date                        Name                        Status
March 2015         Aakanksha Patel            Modified(For TAG)
========================================================================
*/
@isTest
private class TestTriggers {

    static testMethod void ContactTriggerTest() {
        //First, prepare 200 contacts for the test data
        Account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.BillingStreet = 'Street';
        a.Account_Status__c = 'Assigned';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        insert a;     
        
        Contact[] contactsToCreate = new Contact[]{};
        for(Integer x=0; x<200;x++){
            Contact ct = new Contact(AccountId=a.Id,lastname='testing',MailingCountry = 'CA',MailingState = 'AB',MailingPostalCode = 'A9A 9A9',firstname='apex', email=x+'test@test.com',Contact_type__c='Other');
            contactsToCreate.add(ct);
        }
        
        //Now insert data causing an contact trigger to fire. 
        Test.startTest();
        insert contactsToCreate;
        
        for (Contact x : contactsToCreate){
            x.Email = 'h' + x.Email;
            
        }
        
        update contactsToCreate;
        Test.stopTest();
    }
    
    static testMethod void AZTriggerTest() {
        //First, prepare 200 contacts for the test data
        Account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.BillingStreet = 'Street';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        a.ParentId = null;
        a.Account_Status__c = 'Assigned';
        insert a;     
        
        Opportunity o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.CloseDate = date.today();
        o.Unified_Comm_Collaboration_Estimated__c =20;
        insert o;
        
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Somewhere1';
        s1.Suite_Floor__c = '11a';
        s1.Street_Number__c  = '5';
        s1.City__c = 'Coty';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.CLLI_SWC__c = '123113';
        s1.Opportunity__c = o.id;
        insert s1;
        
        Site__c s2 = new Site__c();
        s2.Street_Name__c = 'Somewhere2';
        s2.Suite_Floor__c = '11a';
        s2.Street_Number__c  = '5';
        s2.City__c = 'Coty';
        s2.Is_a_Z_Site__c = true;
        s2.Postal_Code__c = 'A2A1A1';
        s2.Province_Code__c = 'ON';
        s2.CLLI_SWC__c = '123113';
        s2.Opportunity__c = o.id;
        insert s2;
        
        A_Z_Site__c azSite = new A_Z_Site__c();
        azSite.Site_A__c = s1.Id;
        azSite.Site_Z__c = s2.Id;
        INSERT azSite;
        
        Test.startTest();
        s2.Street_Name__c = 'Change';
        UPDATE s2;
                
        DELETE azSite;
        
        DELETE s2;
        DELETE s1;
        
        Test.stopTest();
    }
    
    static testMethod void ActivityTriggerTest() {
        Profile pEnterprise = [select id from profile where name='Rogers Enterprise Sales Manager'];
        
        User uEnterprise1 = new User(alias = 'stand1', email='standarduserEnterprise@testorg.com',
         emailencodingkey='UTF-8', lastname='Enterprise', languagelocalekey='en_US',
         localesidkey='en_US', profileid = pEnterprise.Id, 
         timezonesidkey='America/Los_Angeles', username='rbsEnterpriseUser@testorg.com');
         insert uEnterprise1;
         
        
        Task task1 = new Task();
        task1.ownerId = uEnterprise1.Id;
        task1.User__c = 'Test User';
        try{
            INSERT task1;
        }catch(Exception ex){
            System.debug('This should cause an error - as test User is not a real user.');
        }
    }
    
     static testMethod void LeadTriggerTest() {
        Lead lead1 = new Lead();
        lead1.Company = 'Lead Trigger Test';
        lead1.LeadSource = 'Test Lead Source';
        lead1.LastName = 'LastName';
        lead1.Status = 'Open';
        lead1.Phone = '416-555-7727'; 
        lead1.Lead_Type__c='Wireless';
     
        
        INSERT lead1;
        
        List<Lead> leadList =[SELECT id FROM Lead WHERE Id = :lead1.Id];
        UPDATE leadList; 
     }
}