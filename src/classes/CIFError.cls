public with sharing class CIFError {

	 public CIFError(ApexPages.StandardController controller) {
	 	
	 }

	public PageReference returnToCIF(){ 
          return new PageReference('/a0J/o');
    }
    
    public PageReference returnToOpportunity(){
 			return new PageReference('/006/o');
    }

 	static testMethod void cifErrorUnitTest() {
        ApexPages.StandardController sc = new ApexPages.standardController(new CIF__c());
        CIFError cifError_controller = new CIFError(sc) ;
        
        cifError_controller.returnToCIF();
        cifError_controller.returnToOpportunity();
 	}
}