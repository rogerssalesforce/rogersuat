/**
* Quote clone button controller
*/
public class QuoteCloning{
    private Quote__c quote { get; set; }
    private CloneUtil util = new CloneUtil();

    public QuoteCloning(ApexPages.StandardController stdController) {
        this.quote = (Quote__c)stdController.getRecord();
    }

    public Pagereference cloneQuote() {
        Quote__c newQuote;
        try {
            newQuote = util.cloneQuote(quote.Id);

            newQuote.Is_Cloned__c = FALSE;
            newQuote.Historical_admin3_Quote__c = null; 
            newQuote.Syncing__c = false; 
            
            database.update(newQuote);
        } catch (Exception e) {
            ApexPages.addMessages(e);
            return NULL;
        }
        return new PageReference('/'+newQuote.Id+'/e?retURL=%2F'+newQuote.Id);
    }
}