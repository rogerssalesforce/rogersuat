/*********************************************************************************
Class Name      : CPQ_BypassMECACDialogController
Description     : This class is related to VisualForce Page 'CPQ_BypassMECA'(US-034&035) 
Created By      : Ramiya Kandeepan
Created Date    : 18-Sept-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Ramiya Kandeepan            18-Sept-15             Initial Version
----------------------------------------------------------------------------------            
*********************************************************************************/

public with sharing class CPQ_BypassMECADialogController{

    private Apexpages.StandardController stdController;
    public String sURLid{get;set;}
    public Boolean bShouldRedirect{get;set;}
    public String sUrl{get;set;}
    public string sMyInput{get;set;}
    public String sUsers;
    Apttus__APTS_Agreement__c salesObjInst1;

     /*********************************************************************************
    Method Name    : CPQ_BypassMECADialogController
    Description    : This is a constructor class.
    Return Type    : null
    Parameter      : ApexPages.StandardController
    *********************************************************************************/
    public CPQ_BypassMECADialogController(ApexPages.StandardController apStdController){
        this.stdController = apStdController;
        sURLid = ApexPages.currentPage().getParameters().get('id');
        sUrl = URL.getSalesforceBaseUrl().toExternalForm()+ '/' + sURLid; 
        salesObjInst1 = [Select Id,Bypass_Reason__c,Bypass_MECA_requested__c,Legacy_Contract_Approval_Status__c,Owner.name from Apttus__APTS_Agreement__c where id = :sURLid];
        sMyInput = salesObjInst1.Bypass_Reason__c;  
    }
 
     /*********************************************************************************
    Method Name    : doCancel
    Description    : This method closes the dialog window and return to the record
    Return Type    : void
    Parameter      : ApexPages.StandardController
    *********************************************************************************/
    public  PageReference doCancel(){
        bShouldRedirect=true;
        return null;
    }
    
  /*********************************************************************************
    Method Name    : doSave
    Description    : This method is to save the customer input message and trigger the approval to contract team
    Return Type    : void
    Parameter      : ApexPages.StandardController
    *********************************************************************************/
    public PageReference doSave(){
       if(!String.isBlank(sMyInput)){
            salesObjInst1.Bypass_Reason__c = sMyInput;
            salesObjInst1.Bypass_MECA_requested__c = True;
            sUsers = salesObjInst1.Owner.name;
            update salesObjInst1;

            // create the new approval request to submit
             if([select count() from ProcessInstance where targetobjectid=:salesObjInst1.id] < 1){
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();           
                req.setComments('Submitted for approval.');
                req.setObjectId(salesObjInst1.Id);
    
                 // submit the approval request for processing
                Approval.ProcessResult result = Approval.process(req);
                }
    
                bShouldRedirect=true;
        
        }
        else if(String.isBlank(sMyInput)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, System.label.CPQ_Bypass_MECA_Invalid_Data_Message));
        }
    
        return null;
        
    }
   
}