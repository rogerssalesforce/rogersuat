/**************************************************************************************
Apex Class Name     : TAG_ApprovalCancellationScheduler
Version             :  
Created Date        : 2/19/2015
Function            : This is a Scheduling class for TAG_ApprovalCancellationScheduler batch Class
                       if its in pending status for more than 32 days.
Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Jayavardhan                  2/19/2015               Original Version  
*************************************************************************************/
global class TAG_ApprovalCancellationScheduler implements Schedulable {
   
   global void execute(SchedulableContext ctx) {        
     
     TAG_ApprovalAutoCancellation c = new TAG_ApprovalAutoCancellation();     
     Database.executeBatch(c);
   }   
}