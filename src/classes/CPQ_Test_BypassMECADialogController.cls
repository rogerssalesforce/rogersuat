/*********************************************************************************
Class Name      : CPQ_Test_BypassMECADialogController 
Description     : This class a test class for CPQ_BypassMECADialogController. 
Created By      : Ramiya Kandeepan
Created Date    : 24-Sep-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Ramiya Kandeepan            24-Sep-15             Original version
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest
public class CPQ_Test_BypassMECADialogController{
    static User TestAEUser;
    static Account oAccount;
    static Opportunity oOpportunity;
    static Apttus_Proposal__Proposal__c oQuote;
    static Apttus_Config2__PriceList__c oPriceList;
    static Apttus__APTS_Agreement__c oAgreement;
    private static CPQ_Settings__c csMapping;
    private static User testSUser;
    private static AccountTeamMember oAccountTeamMember;
    private static final Integer CPQ_AGREEMENTS_TOTAL_NUMBER = 200;
    private static final String CPQ_AGREEMENTS_PROFILES_SETTING_VALUE = 'EBU - Rogers Specialist|Read';
    private static final String CPQ_AGREEMENTS_PROFILES_SETTING_VALUE2 = 'EBU - Rogers Resellers Sales Rep|Read';
    private static final String CPQ_ACCOUNT_SPECIALIST = 'EBU - Rogers Specialist';
    private static final String CPQ_SYSTEM_ADMIN = 'System Administrator';
    private static final String CPQ_ADMIN_USERNAME = 'standarduser_test123@testorg.com';
    private static final String CPQ_SPECIALIST_USERNAME = 'specialistuser_test123@testorg.com';
    private static final String CPQ_STATUS_CATEGORY_IN_EFFECT = 'In Effect';
    private static final String CPQ_STATUS_ACTIVATED = 'Activated';
    private static final String CPQ_STATUS_CATEGORY_IN_SIGNATURES = 'In Signatures';
    private static final String CPQ_STATUS_FULLY_SIGNED = 'Fully Signed';
    private static final String CPQ_OPPORTUNITY_STAGE_CLOSED = 'Closed Won';
    private static final String CPQ_OPPORTUNITY_CLOSED_RECORDTYPE = 'Rogers EBU - Closed';
    private static final String CPQ_AGREEMENTS_LEGAL_NAME_RECORDTYPE = 'Rogers EBU Agreement - Legal Name Change';
    private static final String CPQ_STATUS_SIGNATURE_NOTREQUIRED = 'Signature Not Required';
    private static final String CPQ_STATUS_CATEGORY_IN_REQUEST ='Request';
    private static final String CPQ_STATUS_INTERNAL_SIGNATURE='Internal Signatures';
 /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testMethod void createTestData() {
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        
        //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');      
        system.runAs(TestAEUser){
       
        //Create PriceList
        oPriceList = CPQ_Test_Setup.newPriceList();
        insert oPriceList ;
        Apttus_Config2__PriceList__c pl =  [select id, name from  Apttus_Config2__PriceList__c where id =: oPriceList.id];
        System.assertEquals(oPriceList.Id, pl.id);
        
        //Create Account
        oAccount = CPQ_Test_Setup.newAccount('Test Account');
        insert oAccount;
        Account acc =  [select id, name from Account where id =: oAccount.id];
        System.AssertEquals('Test Account', oAccount.Name);
       
        //Create Opportunity
        oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
        insert oOpportunity;
        Opportunity opp =  [select id, name, Account.ID from Opportunity where id =: oOpportunity.id];
        System.AssertEquals(oAccount.Id, opp.Account.ID );
      
        //Create Quote
        oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
        insert oQuote;
        Apttus_Proposal__Proposal__c quote =  [select id, name, Apttus_Proposal__Opportunity__r.ID from Apttus_Proposal__Proposal__c where id =: oQuote.id];
        System.AssertEquals(oOpportunity.Id, quote.Apttus_Proposal__Opportunity__r.ID);
       
        //Create Agreement
        oAgreement= CPQ_Test_Setup.newAgreement(oAccount, oOpportunity,oQuote);
        insert oAgreement;
        Apttus__APTS_Agreement__c agr =  [select id, name, Apttus__Account__r.Id, Apttus__Related_Opportunity__r.ID from  Apttus__APTS_Agreement__c where id =: oAgreement.id];
        System.AssertEquals(oAccount.Id, agr.Apttus__Account__r.Id);
        
        //Inserting Custom setting CPQ_Settings__c
        csMapping  = CPQ_Settings__c.getOrgDefaults();
        csMapping.AgreementsProfiles__c = CPQ_AGREEMENTS_PROFILES_SETTING_VALUE;
        csMapping.AgreementsProfiles2__c = CPQ_AGREEMENTS_PROFILES_SETTING_VALUE2;
        csMapping.Agreement_Category_Status_to_Close_Opp__c=CPQ_STATUS_CATEGORY_IN_REQUEST+','+CPQ_STATUS_CATEGORY_IN_SIGNATURES;
        csMapping.Agreement_Status_to_Close_Opp__c=CPQ_STATUS_SIGNATURE_NOTREQUIRED+','+CPQ_STATUS_INTERNAL_SIGNATURE;
        upsert csMapping;
        }
    }
    
     /***************************************************************************************************
    Method Name    : testCPQ_BypassMECADialogController
    Description    : Method to test CPQ_BypassMECADialogController when bypassing the MECA document
    Return Type    : void  
    Parameter      : Nil              
    ******************************************************************************************************/ 
    static testMethod void testCPQ_BypassMECADialogController(){
        createTestData();
        system.runAs(TestAEUser){
        test.startTest(); 
        
        ApexPages.StandardController sc = new ApexPages.StandardController(oAgreement);
        PageReference pageRef = Page.CPQ_BypassMECA;
        Test.setCurrentPageReference(pageRef);
        
        // Add parameters to page URL
        pageref.getParameters().put('Id', oAgreement.Id);
        pageref.getParameters().put('BypassMECATextBox', 'test');
        
        CPQ_BypassMECADialogController cf = new CPQ_BypassMECADialogController(sc);
        cf.sMyInput = 'test';
        cf.doSave();
        cf.doCancel();
        
        // Verify that customer input message is updated successfully
        Apttus__APTS_Agreement__c[] agreements = [select id, Bypass_Reason__c from Apttus__APTS_Agreement__c where Id =: oAgreement.Id];
        System.assertEquals(cf.sMyInput, agreements[0].Bypass_Reason__c);
        
        Test.stopTest();
        }
    }  
}