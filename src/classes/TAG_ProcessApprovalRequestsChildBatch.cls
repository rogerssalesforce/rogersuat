/*Class Name :TAG_ProcessApprovalRequestsChildBatch. (with list of assignmentReqItems)
 *Description : 
 *Created By : Aakanksha Patel.
 *Created Date :07/05/2015.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/

global class TAG_ProcessApprovalRequestsChildBatch implements Database.Batchable<sObject>, Database.stateful
{
      /*   global String query ='select id,OwnerId,Name,Parent.District__c,ParentId,Parent.Cascade_changes_from_parent_to_child__c,Parent.OwnerId,Cascade_changes_from_parent_to_child__c,District__c,Account_Status__c,Account_Owner_to_MAL__c,Account_Team_to_MAL__c,Recalculate_Team__c from Account  WHERE  ParentId != Null and Parent.Cascade_changes_from_parent_to_child__c = TRUE  ';
  */  
       Global String query;
       Global Set<Id> setARIUpdate = new set<Id>();
       Global Set<Id> setAccountUpdate = new set<Id>();
       Global Set<Id> setARIfailedIdsnew = new set<Id>();
     //  Global Set<Id> setAccidsRecal = new set<Id>();
    
    global TAG_ProcessApprovalRequestsChildBatch(Set<Id> setAccountUpdate)
    {
        this.setAccountUpdate = setAccountUpdate;  // constructor copies in arg to local instance vbl
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
         return database.getQueryLocator('select id,OwnerId,Name,Parent.District__c,ParentId,Parent.Cascade_changes_from_parent_to_child__c,Parent.OwnerId,Cascade_changes_from_parent_to_child__c,District__c,Account_Status__c,Account_Owner_to_MAL__c,Account_Team_to_MAL__c,Recalculate_Team__c from Account  WHERE  ParentId != Null and Parent.Cascade_changes_from_parent_to_child__c = TRUE and ParentId in :setAccountUpdate' );
       
    }
   
    global void execute(Database.BatchableContext BC, List<Account> scope)
    {
          set<ID> AccountIdsNew    = new set<ID>();
          set<ID> setAllAccIdsNew = new set<ID>();
    
          set<ID> AccountIds    = new set<ID>();
          set<Id> setAccUserAdd = new set<ID>();
          set<ID> setAccUserRemove = new set<ID>();
          set<ID> setAllAccIds = new set<ID>();
                           
       //   set<Account> setAccountRecalMSDMem = new set<Account>();
          set<Account> setAccountAccOwner = new set<Account>();
          set<Account> setAccountDist = new set<Account>();
          set<Account> setAccountTeam = new set<Account>();
          set<Assignment_Request_Item__c> setAssignReq = new set<Assignment_Request_Item__c>();
          set<AccountTeamMember> setAccountTeamUpdate = new set<AccountTeamMember>();
          set<AccountTeamMember> setAccountTeamCreate = new set<AccountTeamMember>();
          set<AccountTeamMember> setAccountTeamDelete = new set<AccountTeamMember>();
          set<Account> accountUpdateSet = new set<Account>();
          List<Account> childAccounts = new List<Account>();
          
          Team_Assignment_Governance_Settings__c cs = Team_Assignment_Governance_Settings__c.getInstance();
          
          list<AccountTeamMember> listAccTeam = new list<AccountTeamMember>([Select Id,AccountId,Account.id,TeamMemberRole,UserId FROM AccountTeamMember where TeamMemberRole = 'Owner' AND AccountId in :AccountIds]);
          list<Account> listAccChild = new list<Account>([Select Id,OwnerId,Account_Team_to_MAL__c,Account_Owner_to_MAL__c,Recalculate_Team__c,ParentId from Account where ParentId in :AccountIds ]);    
          list<AccountTeamMember> listAccTeamDelete = new list<AccountTeamMember>([Select Id,AccountId,Account.id,TeamMemberRole,UserId FROM AccountTeamMember where  AccountId in :AccountIds]);
          
          List<AccountTeamMember> alist1 = new List<AccountTeamMember>();
          List<AccountTeamMember> alistnew1 = new List<AccountTeamMember>();
          List<Assignment_Request_item__c> ARIlist = new List<Assignment_Request_item__c>();
     
          List<AccountTeamMember> alistnew = new List<AccountTeamMember>();
          List<AccountTeamMember> alist = new List<AccountTeamMember>();
          List<Account> acclist = new List<Account>(); 
          List<Account> accChild = new List<Account>();
          List<AccountTeamMember> Childaccteam = new List<AccountTeamMember>();
          List<AccountTeamMember> accteam1 = new List<AccountTeamMember>();
          List<AccountTeamMember> Childaccteam1 = new List<AccountTeamMember>();
          List<Account> accChild1 = new List<Account>();
          List<Account> accChildAT = new List<Account>();
          Set<Id> AccTeamCIdsA = new Set<Id>();
          Set<Id> AccTeamPIdsA = new Set<Id>();
          Set<Id> AccDisCIdsA = new Set<Id>();
          Set<Id> AccDisCIdsR = new Set<Id>();
          Set<Id> AccDisPIdsA = new Set<Id>();
          Set<Id> AccDisPIdsR = new Set<Id>();
          Set<Id> AccOwnerCIdsA = new  Set<Id>();
          Set<Id> AccOwnerCIdsR = new Set<Id>();
          Set<Id> AccOwnerPIdsR = new Set<Id>();
          Set<Id> AccOwnerPIds1A = new Set<Id>();
          Set<Id> AccTeamPIdsR = new Set<Id>();
          Set<Id> AccTeamCIdsR = new Set<Id>();
          
          Account acc = new Account();
          Account acc1 = new Account();
          Account acc3 = new Account();
          Account acc4 = new Account();
          Account acc5 = new Account();
          Account acc1MD = new Account();
          AccountTeamMember atm;
          AccountTeamMember atmChild;
          AccountTeamMember atm1;
          AccountTeamMember Childatm1;
          AccountTeamMember atm2;
          AccountTeamMember Childatm2;
          
          List<Assignment_Request_item__c> asignmentReq = new List<Assignment_Request_item__c>();
                                       
         setAllAccIds.addAll(AccountIds);
         for(Account childIds: listAccChild)
         {
             setAllAccIds.add(childIds.Id);
         }
                  
             Map<Id, Set<Id>> mapAccUserIdsToAdd = new Map<Id, Set<Id>>(); 
             Map<Id, Set<Id>> mapAccUserIdsToRemove = new Map<Id, Set<Id>>();      
       
        Map<id,List<AccountTeamMember>> maTeam= new  Map<id,List<AccountTeamMember>>();  //Account and all its AccountTeamMembers with role condition
         for(AccountTeamMember m : listAccTeam)
         {
             alist= maTeam.get(m.AccountId);
             if(alist==null)
             {
                 alist=new List<AccountTeamMember>();
             }
             alist.add(m);
             maTeam.put(m.AccountId,alist);
         }
        
        
         
         map<id,id> mapChildParent = new map<id,id>();
         set<Id> setParentID = new set<Id>();
        for(Account accnt : scope)
          {
              AccountIdsNew.add(accnt.Id);
              mapChildParent.put(accnt.id,accnt.ParentId);
              setParentID.add(accnt.parentId);
              
          }
          System.debug('******SetParentIDS--->>' + setParentID);
          Map<id, Account> mapAccount = new Map<Id, Account>([select id,OwnerId,ParentId,District__c,Account_Status__c,Account_Owner_to_MAL__c,Account_Team_to_MAL__c,Recalculate_Team__c from Account where id in : AccountIdsNew]);
          setAllAccIdsNew.addAll(AccountIdsNew);
          List<AccountTeamMember> listAccTeam1 = new list<AccountTeamMember>([Select Id,AccountId,Account.id,TeamMemberRole,UserId FROM AccountTeamMember where TeamMemberRole = 'Owner' AND AccountId in :setAllAccIdsNew]);  
          list<AccountTeamMember> listAccTeamNew1 = new list<AccountTeamMember>([Select Id,AccountId,Account.id,TeamMemberRole,UserId FROM AccountTeamMember where AccountId in :setAllAccIdsNew]);  
         
         Map<id,List<AccountTeamMember>> maTeam1= new  Map<id,List<AccountTeamMember>>();  //Account and all its AccountTeamMembers with role condition
         for(AccountTeamMember m1 : listAccTeam1)
         {
             alist1= maTeam.get(m1.AccountId);
             if(alist1==null)
             {
                 alist1=new List<AccountTeamMember>();
             }
             alist1.add(m1);
             maTeam1.put(m1.AccountId,alist1);
         }
        
        Map<id,List<AccountTeamMember>> maTeamNew1= new  Map<id,List<AccountTeamMember>>();  //Account and all its AccountTeamMembers 
         for(AccountTeamMember mnew1 : listAccTeamNew1)
         {
             alistnew1= maTeamNew1.get(mnew1.AccountId);
             if(alistnew1==null)
             {
                 alistnew1=new List<AccountTeamMember>();
             }
             alistnew1.add(mnew1);
             maTeamNew1.put(mnew1.AccountId,alistnew1);
         }
        
        List<Assignment_Request_Item__c> listARI = new List<Assignment_Request_Item__c>([SELECT Id,Account__c,Role__c,Member__c,Member__r.Name,MSD__c,Shared_MSD__c,NPA__c,Territory_Region__c,Territory_City__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c WHERE  Status__c= 'Approved' and Account__c in: setParentID ]);
        
       // Map<id,Assignment_Request_Item__c> mapAssignReq = new map<id,Assignment_Request_Item__c>([SELECT Id,Account__c,Role__c,Member__c,Member__r.Name,MSD__c,Shared_MSD__c,NPA__c,Territory_Region__c,Territory_City__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c WHERE  Status__c= 'Approved' and Account__c in: setParentID ]);
        Map<id,List<Assignment_Request_item__c>> mapParentAsign = new Map<id,List<Assignment_Request_item__c>>();
        for(Assignment_Request_Item__c ari : listARI) // map of parent account and ARI
        {
            ARIlist = mapParentAsign.get(ari.Account__c);
            if(ARIlist==null)
            {
                ARIlist=new List<Assignment_Request_item__c>();
            }
            ARIlist.add(ari);
            mapParentAsign.put(ari.Account__c,ARIlist);
            setARIUpdate.add(ari.id);
        }
      
         Map<id,List<Assignment_Request_item__c>> mapChildAsign = new Map<id,List<Assignment_Request_item__c>>();
        for(Account a : scope) // map of child account and ARI(through Parent)
        {
            mapChildAsign.put(a.id,mapParentAsign.get(a.ParentId));
        }
         
         for(Account accScope : scope)
         {
            if(accScope.Parent.OwnerId !=null)
            {
                if(accScope.OwnerId != accScope.Parent.OwnerId || accScope.District__c != accScope.Parent.District__c)
                {
                    accScope.OwnerId = accScope.Parent.OwnerId;
                    accScope.Account_Owner_to_MAL__c = TRUE;
                 //   accScope.Recalculate_Team__c = TRUE;
                    
                    if(maTeam1.get(accScope.Id)!=null)
                    {
                        List<AccountTeamMember> accteam = maTeam1.get(accScope.Id);
                        if(accteam!=null)
                        {
                                for(integer i=0; i<accteam.size();i++)
                                {
                                    AccOwnerPIdsR = mapAccUserIdsToRemove.get(accteam[i].Account.Id);
                                    if(AccOwnerPIdsR==null)
                                         AccOwnerPIdsR = new Set<Id>();
                                    mapAccUserIdsToRemove.put(accteam[i].Account.Id,AccOwnerPIdsR);
                                    
                                    if(accteam[i].TeamMemberRole == cs.Default_Account_Owner_Role__c)                    
                                        setAccountTeamUpdate.add(accteam[i]);
                                }
                        }
                    }
                         
                         atm = new AccountTeamMember();
                         atm.UserId=accScope.Parent.OwnerId;
                         atm.AccountId=accScope.id;
                         atm.TeamMemberRole = cs.Default_Account_Owner_Role__c;
                         setAccountTeamCreate.add(atm);
                         
                         //AccOwnerPIds1A = mapAccUserIdsToAdd.get(atm.Account.Id);
                         AccOwnerPIds1A = mapAccUserIdsToAdd.get(atm.AccountId);
                         If(AccOwnerPIds1A ==null)
                             AccOwnerPIds1A = new Set<Id>();
                         AccOwnerPIds1A.add(atm.UserId);
                         //mapAccUserIdsToAdd.put(atm.Account.Id,AccOwnerPIds1A);
                         mapAccUserIdsToAdd.put(atm.AccountId,AccOwnerPIds1A);
                         setAccountAccOwner.add(accScope);
                    
               
               }
            }  

            
            if(mapChildAsign!=null)
            {
                if(mapChildAsign.get(accScope.id)!=null)
                {
                    asignmentReq = mapChildAsign.get(accScope.id);
                    for(integer i = 0; i<asignmentReq.size(); i++)
                    {
                        if(asignmentReq[i].Member__c!=null && asignmentReq[i].Role__c!=null )
                        {
                            if(asignmentReq[i].Action__c == System.Label.Transfer_Add)
                            {
                                     atm2 = new AccountTeamMember();
                                     atm2.AccountId=accScope.id;
                                     atm2.UserId=asignmentReq[i].Member__c;
                                     atm2.TeamMemberRole = asignmentReq[i].Role__c;
                                     setAccountTeamCreate.add(atm2);
                                     
                                     //AccTeamPIdsA = mapAccUserIdsToAdd.get(atm2.Account.Id);
                                     AccTeamPIdsA = mapAccUserIdsToAdd.get(atm2.AccountId);
                                       If(AccTeamPIdsA==null)
                                     AccTeamPIdsA = new Set<Id>();
                                     AccTeamPIdsA.add(atm2.UserId);
                                     //mapAccUserIdsToAdd.put(atm2.Account.Id,AccTeamPIdsA);
                                     mapAccUserIdsToAdd.put(atm2.AccountId,AccTeamPIdsA);
                            
                            }
                            else if(asignmentReq[i].Action__c == System.Label.Transfer_Remove)
                            {
                                if(maTeamNew1.get(accScope.id)!=null)
                                {
                                    accteam1 = maTeamNew1.get(accScope.id);
                                    if(accteam1!=null)
                                    {
                                        for(integer x2=0; x2<accteam1.size();x2++)
                                        {       
                                           if(accteam1[x2].UserId==asignmentReq[i].Member__c && accteam1[x2].TeamMemberRole==asignmentReq[i].Role__c)
                                           {
                                                AccTeamPIdsR = mapAccUserIdsToRemove.get(accteam1[x2].Account.Id);
                                                    if(AccTeamPIdsR==null)
                                                         AccTeamPIdsR = new Set<Id>();
                                                mapAccUserIdsToRemove.put(accteam1[x2].Account.Id,AccTeamPIdsR);                                  
                                               setAccountTeamDelete.add(accteam1[x2]);
                                           }
                                       }
                                    }   
                                }               
                            }
                        }else
                        {
                            setARIfailedIdsnew.add(asignmentReq[i].id);
                        }
                        setAssignReq.add(asignmentReq[i]);   
                    }  
                }
            }
       
         }
         update scope;
         
               if(setAccountTeamUpdate.size()!=NULL)
                {
                    try{
                        
                        List<AccountTeamMember> listAccountTeamUpdate = new List<AccountTeamMember>();
                        listAccountTeamUpdate.addAll(setAccountTeamUpdate);
                        set<AccountTeamMember> temp = new set<AccountTeamMember>(listAccountTeamUpdate);

                        listAccountTeamUpdate.clear();
                        listAccountTeamUpdate.addAll(temp);
                        Database.delete(listAccountTeamUpdate,false);
                        
                    }catch(exception e){
                        System.debug('TAF Batch :Error while deleting : setAccountTeamUpdate' +setAccountTeamUpdate);
                    }
                } 
                 if(setAccountTeamDelete.size()!=NULL)
                {   try{
                        
                        List<AccountTeamMember> listAccountTeamDelete = new List<AccountTeamMember>();
                        listAccountTeamDelete.addAll(setAccountTeamDelete);
                        set<AccountTeamMember> temp = new set<AccountTeamMember>(listAccountTeamDelete);

                        listAccountTeamDelete.clear();
                        listAccountTeamDelete.addAll(temp);
                        Database.delete(listAccountTeamDelete,false);
                        
                     }catch(exception e){
                         System.debug('TAF Batch :Error while deleting : setAccountTeamDelete' +setAccountTeamDelete);
                     }
                }
                
                if(setAccountAccOwner.size()!=NULL)
                {
                    
                    List<Account> listAccountAccOwner = new List<Account>();
                    listAccountAccOwner.addAll(setAccountAccOwner);
                    set<Account> temp = new set<Account>(listAccountAccOwner);

                    listAccountAccOwner.clear();
                    listAccountAccOwner .addAll(temp);
                    update listAccountAccOwner;
                }
                
                if(setAccountDist.size()!=NULL)
                {
                    List<Account> listAccountDist = new List<Account>();
                    listAccountDist.addAll(setAccountDist);
                    set<Account> temp = new set<Account>(listAccountDist);

                    listAccountDist.clear();
                    listAccountDist.addAll(temp);
                    update listAccountDist;
                    
                }
                
                if(setAccountTeam.size()!=NULL)
                {
                    List<Account> listAccountTeam = new List<Account>();
                    listAccountTeam.addAll(setAccountTeam);
                    set<Account> temp = new set<Account>(listAccountTeam);

                    listAccountTeam.clear();
                    listAccountTeam.addAll(temp);
                    update listAccountTeam;
                    
                }
                
                if(setAssignReq.size()!=NULL)
                {
                    List<Assignment_Request_Item__c> listAssignReq = new List<Assignment_Request_Item__c>();
                    listAssignReq.addAll(setAssignReq);
                    set<Assignment_Request_Item__c> temp = new set<Assignment_Request_Item__c>(listAssignReq);

                    listAssignReq.clear();
                    listAssignReq.addAll(temp);
                    update listAssignReq;
                    
                }
                
                
                if(accountUpdateSet.size()!=NULL)
                {
                    
                    List<Account> accountUpdateList = new List<Account>();
                    accountUpdateList.addAll(accountUpdateSet);
                    set<Account> temp = new set<Account>(accountUpdateList);

                    accountUpdateList.clear();
                    accountUpdateList.addAll(temp);
                    update accountUpdateList;
                    
                }
                
               
                if(setAccountTeamCreate.size()!=NULL)
                {
                    List<AccountTeamMember> listAccountTeamCreate1 = new List<AccountTeamMember>();
                    listAccountTeamCreate1.addAll(setAccountTeamCreate);
                    set<AccountTeamMember> temp = new set<AccountTeamMember>(listAccountTeamCreate1);

                    listAccountTeamCreate1.clear();
                    listAccountTeamCreate1.addAll(temp);
                    insert listAccountTeamCreate1;
                } 
                
         
        //Call updateAccountTeamAccess class to Give/Remove access from Users after updating AccountTeam
        updateAccountTeamAccess accessCls = new updateAccountTeamAccess();
        accessCls.removeAccessFromTeam(mapAccUserIdsToRemove);
        accessCls.giveAccesstoTeam(mapAccUserIdsToAdd);
       
         
    }     
         
       
    global void finish(Database.BatchableContext BC){
   
        List<Assignment_Request_Item__c> listAssignReq = new List<Assignment_Request_Item__c>([SELECT Id,Status__c FROM Assignment_Request_Item__c WHERE  Status__c= 'Approved' and Requested_Item_Type__c = 'Account Team' and (id in:setARIUpdate or Account__r.Cascade_changes_from_parent_to_child__c = TRUE)]);
        list<Assignment_Request_Item__c> AsgnUpdate = new list<Assignment_Request_Item__c>();
        if(listAssignReq.size()!=null)
        {
            for(Assignment_Request_Item__c asgn : listAssignReq)
            {
                if(setARIfailedIdsnew.contains(asgn.id))
                {
                    asgn.Status__c = 'Failed';
                }
                else
                {
                    asgn.Status__c = System.Label.Transfer_Processed;
                }
                AsgnUpdate.add(asgn);
            }
            if(AsgnUpdate.size()!=null)
             update AsgnUpdate;
    
        }
   
        
        //Call recalculateAccountTeam_batch after TAG_ProcessApprovalRequestsBatch is finished.
       Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
        Integer batchSize = Integer.ValueOf(TAGsettings.ReCalAccTeamSchBatchSize__c);
        
        if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < TAGsettings.Max_batch_run__c){ 
            TAG_ReCalculateAccountTeam_batch sc = new TAG_ReCalculateAccountTeam_batch(setAccountUpdate);
            ID batchprocessid = Database.executeBatch(sc,batchSize);
        } 
      
    }
}