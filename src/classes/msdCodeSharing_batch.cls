/*
===============================================================================
 Class Name   : msdCodeSharing_batch
===============================================================================
PURPOSE:    This batch class deletes all the MSD_code__share records with 
            rowcause = AccountTeamMember__c and inserts new MSD_Code share records
             for related Account's Owner and AccountTeamMembers
  

Developer: Deepika Rawat
Date: 09/03/2013

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
10/02/2013           Jacob Klay                  Bug fix
===============================================================================
*/

global class msdCodeSharing_batch implements Database.Batchable<SObject>{
    
    global String query = 'select id, Account__c FROM MSD_Code__c where Account__c!=null Order By Account__c';
    global Database.SaveResult[] listSaveResult;
    static BatchProcess_Admin__c admin = [Select email__c from BatchProcess_Admin__c limit 1];
    static OrgWideEmailAddress orgEmailId=[select id, Address, DisplayName from OrgWideEmailAddress limit 1] ;

    global Database.QueryLocator start(Database.BatchableContext BC){
        //List<ErrorRecords__c> errorRecordlist = [Select id,account_name__c , MSD_Code_Name__c, MSD_Code_Id__c, userId__c, user_name__c, Error_Message__c, statusCode__c from ErrorRecords__c where MSD_Snapshot_Id__c=''];
        //delete errorRecordlist;
        if(!Test.isRunningTest()){
        return Database.getQueryLocator(query);
        }
        else{
           return Database.getQueryLocator('select id, Account__c FROM MSD_Code__c where Account__c!=null Order By Account__c limit 10');
        }     
    }

    global void execute(Database.BatchableContext BC, List<MSD_Code__c > scope) {
        List<MSD_Code__share> listNewMsdCodeShare = new  List<MSD_Code__share>();
        List<ErrorRecords__c> errorList = new List<ErrorRecords__c>();
        Database.DeleteResult[] deleteShareRecList;
        List<Id> listAccIds = new List<Id>();
        List<Id> listMsdCodeIds = new List<Id>();
         Map<Id, List<id>> mapAccountUser = new Map<Id, List<id>>();
              
        for(MSD_Code__c msd:scope){
            listAccIds.add(msd.Account__c);
        }
         //Store all MSD Code Ids in listMsdCodeIds list        
        for(MSD_Code__c msdCodeObj : scope) {
            listMsdCodeIds.add(msdCodeObj.Id);
        }
         //All MSD codes to be deleted that are already in the system.
        List<MSD_Code__share> listOldMsdCodeShare = [select UserOrGroupId, ParentID, RowCause from MSD_Code__share where RowCause= 'AccountTeamMember__c' and ParentID in:listMsdCodeIds ];
        // Map of accountTeamMember as key and record as value
        Map<id, AccountTeamMember> mapAccountTeamMember = new Map<id, AccountTeamMember>([select id, accountId, account.ownerId, userId from AccountTeamMember where accountid in:listAccIds]);
   
        //Create a Map of Account id with list of User ids(Account Owner and AccountTeamMembers)
        for(Id atmId : mapAccountTeamMember.keySet()){
            AccountTeamMember accountTeamMemberObj = mapAccountTeamMember.get(atmId);
            //This is to map AccountTeamMembers
            if(mapAccountUser.containsKey(accountTeamMemberObj.accountId)) {
                List<Id> userIds = new List<Id>();
                userIds = mapAccountUser.get(accountTeamMemberObj.accountId);
                userIds.add((Id)accountTeamMemberObj.userId);
                mapAccountUser.put(accountTeamMemberObj.accountId, userIds);
            } 
            //This is to map Account Owner
            else {
                List<Id> userIds = new List<Id>();
                userIds.add((Id)accountTeamMemberObj.userId);
                userIds.add(accountTeamMemberObj.account.ownerId);
                mapAccountUser.put(accountTeamMemberObj.accountId, userIds);
            }  
        }

        // Create list of new MSD_Code__share records
        for(MSD_Code__c msdCodeObj : scope) {
            if(mapAccountUser.containsKey(msdCodeObj.account__c)){
                List<Id> userIds = mapAccountUser.get(msdCodeObj.account__c);
                for(Id userId : userIds) {
                    MSD_Code__share msdShare = new MSD_Code__share();
                    msdShare.ParentID = msdCodeObj.id;
                    msdShare.UserOrGroupId = userId;
                    msdShare.AccessLevel ='Read';  
                    msdShare.RowCause = Schema.MSD_Code__share.RowCause.AccountTeamMember__c;
                    listNewMsdCodeShare.add(msdShare);
                }
            }        
        }
        try {
           //Delete old MSD_Code__share records from the system
            if(listOldMsdCodeShare.size()>0){
                deleteShareRecList = Database.delete(listOldMsdCodeShare, false); 
            }

            //Insert share records for newly added AccountTeamMembers
            if(listNewMsdCodeShare.size()>0){
                listSaveResult = Database.insert(listNewMsdCodeShare, false);
            //This is to store the list of MSD_Code_share reocrds which failed to insert
                for(integer i=0; i<listNewMsdCodeShare.size(); i++){
                    if(!(listSaveResult[i].isSuccess())){
                        List<Database.Error> err = listSaveResult[i].getErrors();
                        // Check if the error is related to trivial access level. Access levels equal or more permissive than the object's default access level are not allowed. 
                        //These sharing records are not required and thus an insert exception is acceptable. 
                        if(!(err[0].getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION &&  err[0].getMessage().contains('AccessLevel'))){
                            ErrorRecords__c rec = new ErrorRecords__c();
                            rec.UserId__c = listNewMsdCodeShare[i].UserOrGroupId;
                            rec.MSD_Code_Id__c = listNewMsdCodeShare[i].ParentID ;
                            rec.Error_Message__c = err[0].getMessage();
                            rec.statusCode__c = String.Valueof(err[0].getStatusCode());
                            rec.Object_Name__c = 'MSDCode';
                            errorList.add(rec);                
                        }
                    }
                }
            } 
            if(errorList.size()>0){
                insert errorList;
                
                //**************JK DEBUG START**************
                string header = 'msdShare.ParentID(MSD Code Id), UserOrGroupId(UserId/OwnerId) , msdShare.AccessLevel, msdShare.RowCause \n';
                string finalstr = header ;
                for(MSD_Code__share rec: listNewMsdCodeShare )
                {
                       string recordString = '"'+rec.ParentID+'","'+rec.UserOrGroupId+'","'+rec.AccessLevel+'","'+rec.RowCause+'"\n';
                       finalstr = finalstr +recordString;
                }
                 
            }
        }catch(DmlException e) {
            //Send an email to the Apex job's submitter on failure.
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {admin.email__c}; 
            mail.setToAddresses(toAddresses); 
            mail.setSubject('Batch process to share MSD Code records Exception');
            mail.setPlainTextBody('Batch process to share MSD Code records threw the following exception: ' + e.getMessage());
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
   }

    global void finish(Database.BatchableContext BC){

        List<ErrorRecords__c> errorRecordlist = [Select id,account_name__c , MSD_Code_Name__c, MSD_Code_Id__c, userId__c, user_name__c, Error_Message__c, statusCode__c from ErrorRecords__c where MSD_Snapshot_Id__c='' and CreatedDate = TODAY];
        string header = 'Account Name, MSD Code Name , MSD Code Id, User Name, User Id, Error Message, Status Code \n';
        string finalstr = header ;
        for(ErrorRecords__c erObj: errorRecordlist )
        {
               string recordString = '"'+erObj.account_name__c+'","'+erObj.MSD_Code_Name__c+'","'+erObj.MSD_Code_Id__c+'","'+erObj.user_name__c+'","'+erObj.UserId__c+'","'+erObj.Error_Message__c+'","'+erObj.StatusCode__c+'"\n';
               finalstr = finalstr +recordString;
        }
        //Send Notification Email to Admin with Errors if any
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        string csvname= 'MSD Code Error Records.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {admin.email__c};
        String body = 'Hi,<br/><br/>Batch process to share MSD Code records with AccountTeamMembers has been completed. <br/><br/>'; 
        body += 'Please find the attached csv for error records that failed to get inserted in the database.';
        body += '<br/> <br/> Regards, <br/> Salesforce Team'; 
        email.setToAddresses(toAddresses); 
        email.setSubject('Batch Completion Report: MSD Code');        
        email.setSaveAsActivity(false);  
        email.setHtmlBody(body); 
        email.setOrgWideEmailAddressId(orgEmailId.id); 
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        
        //Execute MSD Snapshot batch
        msdSnapshotSharing_batch sc = new msdSnapshotSharing_batch();
        ID batchprocessid = Database.executeBatch(sc,200);
        system.debug('sc*************' +sc);
    }
}