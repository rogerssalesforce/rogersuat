/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestServiceableLocationWrapper {
	static ServiceableLocation__c sl;
	static Id site1Id;
	static Boolean withAssertions = true;
	
	static {
		sl = new ServiceableLocation__c();
	    sl.Street_Name__c = 'Somewhere';
	    sl.Street_Number__c  = '5';
	    sl.Street_Type__c  = 'Ave';
	    sl.City__c = 'Coty';
	    sl.Postal_Code__c = 'A1A1A1';
	    sl.Province_Code__c = 'ON';
	    sl.CLLI_Code__c = '123113';
	    sl.Access_Type_Group__c = 'NNI';
	    insert sl;
	    
	    Site__c s1 = new Site__c();
	    s1.Street_Name__c = 'Somewhere';
	    s1.Suite_Floor__c = '11a';
	    s1.Street_Number__c  = '5';
	    s1.City__c = 'Coty';
	    s1.Postal_Code__c = 'A1A1A1';
	    s1.Province_Code__c = 'ON';
	    s1.CLLI_SWC__c = '123113';
	    s1.ServiceableLocation__c = null;
	    insert s1;
	    site1Id = s1.Id;
	}

    static testMethod void serviceableLocationSelectedTest() {
       ServiceableLocationWrapper wrapper = new ServiceableLocationWrapper(sl);
       wrapper.setIsSelected(true);
       
		if (withAssertions){
			System.assertEquals(wrapper.getIsSelected(), true);
		}
          
    }
    
    static testMethod void serviceableLocationNotSelectedTest() {
       ServiceableLocationWrapper wrapper = new ServiceableLocationWrapper(sl);
       wrapper.setIsSelected(false);
       
		if (withAssertions){
			System.assertEquals(wrapper.getIsSelected(), false);
		}
    }
    
    static testMethod void serviceableLocationSiteTests() {
       ServiceableLocationWrapper wrapper = new ServiceableLocationWrapper(sl);
       
       if (withAssertions){
			System.assertEquals(wrapper.candidateSiteId, null);
		}
       
       wrapper.candidateSiteId = site1Id;
       
		if (withAssertions){
			System.assertEquals(wrapper.candidateSiteId, site1Id);
		}
    }
    
    static testMethod void serviceableLocationTest() {
       ServiceableLocationWrapper wrapper = new ServiceableLocationWrapper(sl);
       
       if (withAssertions){
			System.assertEquals(wrapper.getServiceableLocation(), sl);
		}
       
       
    }
}