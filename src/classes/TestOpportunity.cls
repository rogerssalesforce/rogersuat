/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts. opportunity
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestOpportunity {
    static Id oppId, accId, carrierAccId, carrierOppId;
    static Boolean withAssertions = true;
    static Map <string,Id> mapRTo, mapRTa;
    static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();         
    
       
    static {
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        
        
    }
    
    static testMethod void testTempServiceOppFails() {
        
      /*  
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;
        */
        Test.startTest();
        
        List<Account> lstAccountsToInsert = new List<Account>();
        List<Opportunity> lstOpportunityToInsert = new List<Opportunity>();
        
        Account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street'; a.Account_Status__c = 'Assigned';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        a.ParentId = null;
        //insert a; 
        lstAccountsToInsert.add(a);  
        //accId = a.Id;   
        
        Account a_carrier = new Account();
        a_carrier.name = 'Test Act Carrier';
        a_carrier.Business_Segment__c = 'Alternate';
        a_carrier.RecordTypeId = mapRTa.get('Carrier Account');
        a_carrier.BillingStreet = 'Test';
        a_carrier.BillingCity = 'Ontario';
        a_carrier.BillingCountry = 'CA';
        a_carrier.BillingPostalCode = 'A9A 9A9';
        a_carrier.BillingState = 'ON';
        a_carrier.ParentId = null;
        a_carrier.Account_Status__c = 'Assigned';
        //insert a_carrier;     
        lstAccountsToInsert.add(a_carrier);    
        //carrierAccId = a_carrier.Id;
        
        insert lstAccountsToInsert;
        accId = lstAccountsToInsert[0].Id;
        carrierAccId = lstAccountsToInsert[1].Id;   
        
        Opportunity o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.Unified_Comm_Collaboration_Estimated__c =20;
        o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.today();
        ShiftOpportunityTrigger_AU_AISupport.hasRun = false;
        lstOpportunityToInsert.add(o);
        //insert o;
        //oppId = o.id;
        
        Opportunity o3 = new Opportunity();
        o3.Estimated_MRR__c = 500;
        o3.Name = 'Test Opp';
        o3.StageName = 'Suspect - Qualified';
        o3.Product_Category__c = 'Local';
        o3.Network__c = 'Cable';
        o3.Estimated_One_Time_Charge__c = 500;
        o3.New_Term_Months__c = 5;
        o3.AccountId = a.id;
        o3.Unified_Comm_Collaboration_Estimated__c =20;
        o3.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o3.CloseDate = date.today();
        ShiftOpportunityTrigger_AU_AISupport.hasRun = false;
        lstOpportunityToInsert.add(o3);
        //insert o3;
        //id destId = o3.id;
        
        Opportunity o_carrier = new Opportunity();
        o_carrier.Estimated_MRR__c = 500;
        o_carrier.Name = 'Test Opp';
        o_carrier.StageName = 'Suspect - Qualified';
        o_carrier.Product_Category__c = 'Local';
        o_carrier.Network__c = 'Cable';
        o_carrier.Estimated_One_Time_Charge__c = 500;
        o_carrier.New_Term_Months__c = 5;
        o_carrier.Unified_Comm_Collaboration_Estimated__c =20;
        o_carrier.AccountId = a_carrier.id;
        o_carrier.RecordTypeId = mapRTo.get('Wireline - New Carrier Sale');
        o_carrier.CloseDate = date.today();
        ShiftOpportunityTrigger_AU_AISupport.hasRun = false;
        lstOpportunityToInsert.add(o_carrier);
        //insert o_carrier;
        //carrierOppId = o_carrier.id;
        
        Opportunity tempO = new Opportunity();
        tempO.Estimated_MRR__c = 500;
        tempO.Unified_Comm_Collaboration_Estimated__c =20;
        tempO.Name = 'Test Service Opp';
        tempO.StageName = 'Suspect - Qualified';
        tempO.Product_Category__c = 'Local';
        tempO.Network__c = 'Cable';
        tempO.Estimated_One_Time_Charge__c = 500;
        tempO.New_Term_Months__c = 5;
        tempO.AccountId = accId;
        tempO.RecordTypeId = mapRTo.get('Wireline - Temp Solution Opportunity');
        tempO.CloseDate = date.today();
        //try{
            ShiftOpportunityTrigger_AU_AISupport.hasRun = false;
            //insert tempO;
        /*}catch(Exception ex){
            // Should Fail without a parent opp
        }*/
        lstOpportunityToInsert.add(tempO);
        try {
            insert lstOpportunityToInsert;
        } catch(Exception ex){
            // Should Fail without a parent opp
        }
        
        oppId = lstOpportunityToInsert[0].id;
        id destId = lstOpportunityToInsert[1].id;
        carrierOppId = lstOpportunityToInsert[2].id;
        
        ServiceableLocation__c sl = new ServiceableLocation__c();
        sl.Street_Name__c = 'Somewhere';
        sl.Street_Number__c  = '5';
        sl.Street_Type__c  = 'Ave';
        sl.City__c = 'Coty';
        sl.Postal_Code__c = 'A1A1A1';
        sl.Province_Code__c = 'ON';
        sl.CLLI_Code__c = '123113';
        sl.Access_Type_Group__c = 'NNI';
        sl.Access_Type__c = 'Ethernet Eon';
        insert sl;
                
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Somewhere4';
        s1.Suite_Floor__c = '11a';
        s1.Street_Number__c  = '7';
        s1.City__c = 'Coty';
        s1.Postal_Code__c = 'A3A1A1';
        s1.Province_Code__c = 'ON';
        s1.CLLI_SWC__c = '123113';
        s1.ServiceableLocation__c = sl.Id;
        s1.Opportunity__c = oppId;
        s1.Is_a_Z_Site__c = true; 
        insert s1;
        
      /*  TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;
        */
        /*Account a_carrier = new Account();
        a_carrier.name = 'Test Act Carrier';
        a_carrier.Business_Segment__c = 'Alternate';
        a_carrier.RecordTypeId = mapRTa.get('Carrier Account');
        a_carrier.BillingStreet = 'Test';
        a_carrier.BillingCity = 'Ontario';
        a_carrier.BillingCountry = 'CA';
        a_carrier.BillingPostalCode = 'A9A 9A9';
        a_carrier.BillingState = 'ON';
        a_carrier.ParentId = null;
        a_carrier.Account_Status__c = 'Assigned';
        insert a_carrier;       
        carrierAccId = a_carrier.Id;*/
        
        /*Opportunity o_carrier = new Opportunity();
        o_carrier.Estimated_MRR__c = 500;
        o_carrier.Name = 'Test Opp';
        o_carrier.StageName = 'Suspect - Qualified';
        o_carrier.Product_Category__c = 'Local';
        o_carrier.Network__c = 'Cable';
        o_carrier.Estimated_One_Time_Charge__c = 500;
        o_carrier.New_Term_Months__c = 5;
        o_carrier.Unified_Comm_Collaboration_Estimated__c =20;
        o_carrier.AccountId = a_carrier.id;
        o_carrier.RecordTypeId = mapRTo.get('Wireline - New Carrier Sale');
        o_carrier.CloseDate = date.today();
        ShiftOpportunityTrigger_AU_AISupport.hasRun = false;
        insert o_carrier;
        carrierOppId = o_carrier.id;*/
        
        /*Opportunity tempO = new Opportunity();
        tempO.Estimated_MRR__c = 500;
        tempO.Unified_Comm_Collaboration_Estimated__c =20;
        tempO.Name = 'Test Service Opp';
        tempO.StageName = 'Suspect - Qualified';
        tempO.Product_Category__c = 'Local';
        tempO.Network__c = 'Cable';
        tempO.Estimated_One_Time_Charge__c = 500;
        tempO.New_Term_Months__c = 5;
        tempO.AccountId = accId;
        tempO.RecordTypeId = mapRTo.get('Wireline - Temp Solution Opportunity');
        tempO.CloseDate = date.today();
        try{
            ShiftOpportunityTrigger_AU_AISupport.hasRun = false;
            insert tempO;
        }catch(Exception ex){
            // Should Fail without a parent opp
        }*/
        
        /*tempO = new Opportunity();
        tempO.Estimated_MRR__c = 500;
        tempO.Name = 'Test Service Opp';
        tempO.StageName = 'Suspect - Qualified';
        tempO.Product_Category__c = 'Local';
        tempO.Unified_Comm_Collaboration_Estimated__c =20;
        tempO.Network__c = 'Cable';
        tempO.Estimated_One_Time_Charge__c = 500;
        tempO.New_Term_Months__c = 5;
        tempO.AccountId = accId;
        tempO.RecordTypeId = mapRTo.get('Wireline - Temp Solution Opportunity');
        tempO.CloseDate = date.today();
        tempO.Parent_Opportunity__c = carrierOppId;
        try{
            ShiftOpportunityTrigger_AU_AISupport.hasRun = false;
            insert tempO;
        }catch(Exception ex){
            // Should Fail without a parent opp
        }*/
        
        Utils.cloneSourceSites(oppId, destId);
        
        Test.stopTest();
        
    }
    
    static testMethod void testTempServiceOppPass() {
        
        Test.startTest();
     /*   TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;
       */ 
        Account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street'; a.Account_Status__c = 'Assigned';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        insert a;   
        accId = a.Id;   
        
        Opportunity o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.Unified_Comm_Collaboration_Estimated__c =20;
        o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.today();
        ShiftOpportunityTrigger_AU_AISupport.hasRun = false;
        insert o;
        oppId = o.id;
        
        ServiceableLocation__c sl = new ServiceableLocation__c();
        sl.Street_Name__c = 'Somewhere';
        sl.Street_Number__c  = '5';
        sl.Street_Type__c  = 'Ave';
        sl.City__c = 'Coty';
        sl.Postal_Code__c = 'A1A1A1';
        sl.Province_Code__c = 'ON';
        sl.CLLI_Code__c = '123113';
        sl.Access_Type_Group__c = 'NNI';
        sl.Access_Type__c = 'Ethernet Eon';
        insert sl;
                
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Somewhere4';
        s1.Suite_Floor__c = '11a';
        s1.Street_Number__c  = '7';
        s1.City__c = 'Coty';
        s1.Postal_Code__c = 'A3A1A1';
        s1.Province_Code__c = 'ON';
        s1.CLLI_SWC__c = '123113';
        s1.ServiceableLocation__c = sl.Id;
        s1.Opportunity__c = oppId;
        s1.Is_a_Z_Site__c = true; 
        insert s1;
        
        
        Account a_carrier = new Account();
        a_carrier.name = 'Test Act Carrier';
        a_carrier.Business_Segment__c = 'Alternate';
        a_carrier.RecordTypeId = mapRTa.get('Carrier Account');
        a_carrier.BillingStreet = 'Test';
        a_carrier.BillingCity = 'Ontario';
        a_carrier.BillingCountry = 'CA';
        a_carrier.BillingPostalCode = 'A9A 9A9';
        a_carrier.BillingState = 'ON';
        insert a_carrier;       
        carrierAccId = a_carrier.Id;
        
        Opportunity o_carrier = new Opportunity();
        o_carrier.Estimated_MRR__c = 500;
        o_carrier.Existing_MRR__c  =100;
        o_carrier.Name = 'Test Opp';
        o_carrier.StageName = 'Suspect - Qualified';
        o_carrier.Product_Category__c = 'Local';
        o_carrier.Network__c = 'Cable';
        o_carrier.Unified_Comm_Collaboration_Estimated__c =20;
        o_carrier.Estimated_One_Time_Charge__c = 500;
        o_carrier.New_Term_Months__c = 5;
        o_carrier.AccountId = a_carrier.id;
        o_carrier.RecordTypeId = mapRTo.get('Wireline - Carrier Contract Renewal');
        o_carrier.CloseDate = date.today();
        ShiftOpportunityTrigger_AU_AISupport.hasRun = false;
        insert o_carrier;
        carrierOppId = o_carrier.id;
        
        Opportunity tempO = new Opportunity();
        tempO.Estimated_MRR__c = 500;
        tempO.Name = 'Test Service Opp';
        tempO.Unified_Comm_Collaboration_Estimated__c =20;
        tempO.StageName = 'Suspect - Qualified';
        tempO.Product_Category__c = 'Local';
        tempO.Network__c = 'Cable';
        tempO.Estimated_One_Time_Charge__c = 500;
        tempO.New_Term_Months__c = 5;
        tempO.AccountId = accId;
        tempO.RecordTypeId = mapRTo.get('Wireline - Temp Solution Opportunity');
        tempO.CloseDate = date.today();
        tempO.Parent_Opportunity__c = oppId;
        ShiftOpportunityTrigger_AU_AISupport.hasRun = false;
        insert tempO;
        

        Test.stopTest();
    }
    
    static testMethod void testOpportunityUpdation() {
    
    Account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street'; a.Account_Status__c = 'Assigned';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        insert a;   
        
    Opportunity tempO = new Opportunity();
        tempO.Estimated_MRR__c = 500;
        tempO.Name = 'Test Service Opp';
        tempO.StageName = 'Suspect - Qualified';
        tempO.Product_Category__c = 'Local';
        tempO.Network__c = 'Cable';
        tempO.Estimated_One_Time_Charge__c = 500;
        tempO.Business_Unit__c='Wireline';
        tempO.New_Term_Months__c = 5;
        tempO.Unified_Comm_Collaboration_Estimated__c =20;
        tempO.AccountId = a.id;
        tempO.RecordTypeId = mapRTo.get('Wireline - Temp Solution Opportunity');
        tempO.CloseDate = date.today();
    ShiftOpportunityTrigger_AU_AISupport.hasRun = false;
        insert tempO;
        
        Pricebook2 stdpb = [SELECT Id FROM Pricebook2 WHERE IsStandard = true];
        PricebookEntry pb = [select Id From PricebookEntry where IsActive = true AND Pricebook2.id=:stdpb.id limit 1];
        
            OpportunityLineItem olis = new OpportunityLineItem(OpportunityId = tempO.Id,
                                                            Quantity=10.0,
                                                            TotalPrice = 100.0,
                                                            PricebookEntryId = pb.Id);
        insert olis;
        
         Quote q = new Quote(); 
         q.OpportunityId = tempO.id; 
         q.Name = 'Test Quote Opp Web Services'; 
         q.Pricebook2Id = stdpb.id; 
         q.Term__c = '3-Years';
         q.Allowable_Product_Information_Sheets__c = 'SIP Trunking;RBS Dedicated Internet;SIP and RDI Bundle';
         insert q;
         
        tempO.StageName = 'Closed Won';
        tempO.Business_Unit__c='Wireline';
        tempO.Mandatory_COP_Fields_Status__c='Submitted/Completed';
        tempO.Win_Reason__c = 'Customer Service';
        tempO.Expected_Billing_Date__c = date.today();
        update tempO;
        
        Site__c site = new Site__c(); 
        site.Suite_Floor__c = '11';
        site.Street_Name__c = 'Somewhere' ;
        site.Street_Number__c = '34';
        site.City__c = 'City'; 
        site.Postal_Code__c = 'A1A1A1'; 
        site.Province_Code__c = 'ON';
        site.CLLI_SWC__c = '123113'; 
        site.Opportunity__c = tempO.id; 
        insert site;
         
         Active_Site__c as1 = new Active_Site__c();
         as1.Account__c =  tempO.AccountId ;
         as1.Site__c = site.id;
         insert as1;
    
    }  
}