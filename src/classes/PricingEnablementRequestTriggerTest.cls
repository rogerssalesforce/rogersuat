@isTest(seeAllData = true)
public class PricingEnablementRequestTriggerTest {
    
    static testMethod Void PositiveTestCase(){
        
        account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        //a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Test';
        a.BillingCity = 'Ontario';
        a.BillingCountry = 'CA';
        a.BillingPostalCode = 'A9A 9A9';
        a.BillingState = 'ON';
        a.Account_Status__c = 'Assigned';
        insert a;       
        
        //List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        //Map <string,Id> mapRTo = new Map <string,id> ();
        
        opportunity o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        //o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.newInstance(2013,1,10);
        o.Unified_Comm_Collaboration_Estimated__c =20;
        insert o;
        
       // List <RecordType > lRtPCR = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Pricing_Enablement_Request__c']);
       // Map <string,Id> mapRTPCR = new Map <string,id> ();
        
        Pricing_Enablement_Request_Settings__c PERsettings = Pricing_Enablement_Request_Settings__c.getInstance();
        Pricing_Enablement_Request_Settings__c ContractsPrime = [Select Id, Name, Value__c, Field_API_Name__c FROM Pricing_Enablement_Request_Settings__c WHERE Field_API_Name__c = 'Contracts_Prime__c' LIMIT 1];
        
        Pricing_Enablement_Request__c PCR = new Pricing_Enablement_Request__c();
        PCR.Opportunity__c=o.id;
        //PCR.RecordTypeId=mapRTPCR.get('Approved Pricing Enablement Request');
        PCR.Final_Request__c=TRUE;
        PCR.Status__c='Approved';
        PCR.Contracts_Prime__c = ContractsPrime.Value__c;
        insert PCR;
        
        try{
            Pricing_Enablement_Request__c PCR2 = new Pricing_Enablement_Request__c();
            PCR2.Opportunity__c=o.id;
            //PCR.RecordTypeId=mapRTPCR.get('Approved Pricing Enablement Request');
            PCR2.Final_Request__c=TRUE;
            PCR2.Status__c='Approved';
            PCR2.Contracts_Prime__c = ContractsPrime.Value__c;
            insert PCR2;
        } catch (exception e) {
            
        }
        
    }
        
}