public class EditPricingRequestCont {

    public EditPricingRequestCont(ApexPages.StandardController controller) {
        reqId = ApexPages.currentPage().getParameters().get('id');
        List<Pricing_Enablement_Request__c> lstReq = [Select id, Opportunity__c from Pricing_Enablement_Request__c where id=:reqId];
        OppId=lstReq[0].Opportunity__c;
        Map<String, Object> myMap = new Map<String, Object>();
        myMap.put('Existing_Request_ID', reqId);
        my_interview = new Flow.Interview.New_Pricing_Enablement_Request(myMap);
    }

    public String OppId{get;set;}
    public String reqId{get;set;}
    public Flow.Interview.New_Pricing_Enablement_Request my_interview {get; set;}
   
    public pagereference returnToOpp(){
        PageReference pageRef = new PageReference('/'+OppId);
        return pageRef;
    }
    public pagereference returnToNewReq(){
        PageReference pageRef = new PageReference('/'+reqId);
        return pageRef;
    }
    

}