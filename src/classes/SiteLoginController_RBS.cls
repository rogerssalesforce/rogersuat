/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class SiteLoginController_RBS {
    global String username {get; set;}
    global String password {get; set;}

    global PageReference login() {
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        return Site.login(username, password, startUrl);
    }
    
     global SiteLoginController_RBS () {}
    
    @IsTest(SeeAllData=true) global static void testSiteLoginController_RBS () {
        // Instantiate a new controller with all parameters in the page
        SiteLoginController_RBS controller = new SiteLoginController_RBS ();
        controller.username = 'test@salesforce.com';
        controller.password = '123456'; 
                
        System.assertEquals(controller.login(),null);                           
    }    
}