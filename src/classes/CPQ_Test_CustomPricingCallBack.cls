/*********************************************************************************
Class Name      : CPQ_Test_CustomPricingCallBack
Description     : This is a test class for CPQ_CustomPricingCallBack. 
Created By      : Deepika Rawat
Created Date    : 02-Dec-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Deepika Rawat               02-Dec-15
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest(SeeAllData=false)
public class CPQ_Test_CustomPricingCallBack{
    static User TestAEUser;
    static Account oAccount;
    static product2 oProduct;
    static product2 oCustomIOTProduct;
    static Opportunity oOpportunity;
    static Apttus_Proposal__Proposal__c oQuote;
    static Attachment attachmentQuote;
    static Apttus_Config2__PriceList__c oPriceList;
    static Apttus_Config2__PriceListItem__c oPriceListItem; 
    static Apttus_Config2__PriceListItem__c oCustomProductPriceListItem; 
    static ID prodConfigID;
    static Pricebook2 oPricebook;
    static Pricebook2 oPriceBookCustom;
    private static CPQ_Settings__c cs;
    static List<Apttus_Config2__ProductConfiguration__c> lstProductConfig ;
     /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testMethod void createTestData() {
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        cs  = CPQ_Settings__c.getOrgDefaults();
        cs.DefaultOpportunityRecordType__c = 'Rogers EBU - New';
        upsert cs;
        //Create custom Pricebook 
            oPriceBookCustom = CPQ_Test_Setup.newPriceBook('CPQ Temp PriceBook');
            insert oPriceBookCustom;    
            //Create newPriceBook
            oPricebook = CPQ_Test_Setup.newPriceBook('Test Pricelist');
            insert oPricebook;
            //Create PriceList
            oPriceList = CPQ_Test_Setup.newPriceList();
            oPriceList.Apttus_Config2__Active__c=true;
            insert oPriceList ;
            // Create Product
            oProduct = CPQ_Test_Setup.newProduct();
            oProduct.Family = 'Wireless - Voice';
            insert oProduct;
            //Create Custom Product
            oCustomIOTProduct = CPQ_Test_Setup.newProduct();
            oCustomIOTProduct.ProductCode = 'IoTCustomProduct';
            oCustomIOTProduct.Name = 'IoT Custom Product';
            oCustomIOTProduct.Family = 'IoT Applications';
            insert oCustomIOTProduct;
            //Create Pricelist item
            oPriceListItem = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oProduct.ID);
            insert oPriceListItem;
            oCustomProductPriceListItem = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oCustomIOTProduct.ID);
            insert oCustomProductPriceListItem;
            //Create test user 
            TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');    
            system.runAs(TestAEUser){
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            oAccount.Price_List__c=oPriceList.id;
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            oAccount.Price_List__c=oPriceList.id;
            insert oOpportunity;
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList);
            insert oQuote;
            //Product Configuration
            prodConfigID = CPQ_Test_Setup.newProdictConfiguration(oQuote);
            //Create line items
            CPQ_Test_Setup.createLineItem(prodConfigID,oProduct.id,oPriceListItem.id);
            lstProductConfig = [Select id, Apttus_QPConfig__Proposald__c, Apttus_Config2__Status__c from  Apttus_Config2__ProductConfiguration__c where Apttus_QPConfig__Proposald__c =:oQuote.id ];
        }
    }
    /*********************************************************************************
    Method Name    : testRepriceAsAE
    Description    : Method to test reprice as Deal Desk User
    Return Type    : void  
    Parameter      : CartID, PriceListID, ProductID             
    *********************************************************************************/ 
    static testmethod void testRepriceAsAE(){
        createTestData();
        oQuote.Apttus_Proposal__Approval_Stage__c = 'Draft';
        update oQuote;
        String sConfigRequestId = [select id from Apttus_Config2__TempObject__c where Apttus_Config2__ConfigurationId__c=:prodConfigID].id;
        system.runAs(TestAEUser){
            test.startTest();
            ApexPages.currentPage().getParameters().put('id', prodConfigID);
            ApexPages.currentPage().getParameters().put('configrequestid', sConfigRequestId);
            ApexPages.currentPage().getParameters().put('sFlow', 'ngFlow');
            CPQ_AddCustomProductsController obj = new CPQ_AddCustomProductsController();
            obj.getOtherProducts();
            obj.getChargeType();
            obj.getCOPTeam();
            obj.oLineItem.ProductPillar__c='IoT Applications';
            obj.oLineItem.LineofBusiness__c='IoT / Applications';
            obj.oLineItem.CustomProductName__c='Test Prod Name';
            obj.oLineItem.Apttus_Config2__Quantity__c=1;
            obj.oLineItem.Apttus_Config2__BasePriceOverride__c=200;
            obj.addProduct();
            List<Apttus_Config2__LineItem__c> lstOtherLineItems = [select id, CustomProductName__c from Apttus_Config2__LineItem__c where CustomProductName__c='Test Prod Name' ];
            System.Assert(lstOtherLineItems!=null);
            test.stopTest();
        }
    } 
}