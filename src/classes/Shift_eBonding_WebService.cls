/*
    @Name: Shift_AccountRevenueRollups_Ext
    @Description: Webservice to be used to retreieve quote information from RBS
    @Dependancies: 
    @Version: 1.0.0 
    
    === VERSION HISTORY ===  
    | Version Number | Author      | Description
    | 1.0.0          | Ryan Morden |  Initial
*/ 

global with sharing class Shift_eBonding_WebService {
    
    //Init variables
    public static ServiceableLocation__c customerLocation;
    public static ServiceableLocation__c terminationLocation;
    public static Account requestorAccount;
    public static Product2 requestedMRCProduct;
    public static Product2 requestedNRCProduct;
    public static Map<Id, Double> priceMap = new Map<Id, Double>();
    public static Id pricebookId;
    public static String pricingType, pricebookName, carrierProspectRT, carrierRT;
    public static String gblCustomerName, gblCustomerAddress, gblTerminationAddress;
    public static String gblCustomerStreet, gblCustomerCity, gblCustomerProvince, gblCustomerCountry, gblTerminationStreet, gblTerminationCity, gblTerminationProvince, gblTerminationCountry;
    public static String gblProduct, gblProductBandwidth, gblAccess, gblContractTerm; 
    public static Map<String, Shift_Metro_Regions__c> metroRegionMap = Shift_Metro_Regions__c.GetAll();
    
    /*** webservice method to call ***/
    webservice static string getQuoteXML(String CustomerName, String CustomerStreet, String CustomerCity, String CustomerProvince, String CustomerCountry, 
                                            String TerminationStreet, String TerminationCity, String TerminationProvince, String TerminationCountry, 
                                            String Product, String ProductBandwidth, String Access, String ContractTerm) {       
    
        string xmlResult='';

        //Set variables, we set these after the field length check so they won't affect dml operations
        gblCustomerName = CustomerName;
        gblCustomerAddress = CustomerStreet + CustomerCity + CustomerProvince + CustomerCountry;
        gblCustomerAddress = gblCustomerAddress.replaceAll('\\s+', '');
        gblCustomerAddress = gblCustomerAddress.toUpperCase();
        gblTerminationAddress = TerminationStreet + TerminationCity + TerminationProvince + TerminationCountry;
        gblTerminationAddress = gblTerminationAddress.replaceAll('\\s+', '');
        gblTerminationAddress = gblTerminationAddress.toUpperCase();
        gblProduct = Product;
        gblProductBandwidth = ProductBandwidth;
        gblAccess = Access;
        gblContractTerm = ContractTerm; 

        gblCustomerStreet = CustomerStreet;
        gblCustomerCity = CustomerCity;
        gblCustomerProvince = CustomerProvince;
        gblCustomerCountry = CustomerCountry;
        gblTerminationStreet = TerminationStreet;
        gblTerminationCity = TerminationCity;
        gblTerminationProvince = TerminationProvince;
        gblTerminationCountry = TerminationCountry;

        //Check to make sure that the reqest is complete
        if (CustomerName==''||gblCustomerAddress==''||gblTerminationAddress==''
                ||Product==''||ProductBandwidth==''||Access==''||ContractTerm==''
                ||CustomerName==NULL||gblCustomerAddress==NULL||gblTerminationAddress==NULL
                ||Product==NULL||ProductBandwidth==NULL||Access==NULL||ContractTerm==NULL) {
        
            xmlResult = generateErrorXML(600);
            return xmlResult;           
        }         

        //Check parameter length (can't be longer than 255 chars)
        if (CustomerName.length()>255||gblCustomerAddress.length()>255||gblTerminationAddress.length()>255
                ||Product.length()>255||ProductBandwidth.length()>255||Access.length()>255||ContractTerm.length()>255) {
                    
            xmlResult = generateErrorXML(601);
            return xmlResult;                   
        }       
        
        //Query out the related account
        List<Account> tempAccList = new List<Account>([SELECT Id, Pricebook__c, RecordTypeId FROM Account WHERE eBonding_User__c = :UserInfo.getUserId()]);     
       system.debug('***listacc**'+tempAccList);
       system.debug('***listacc**'+tempAccList[0]);
       
        //If an account is not found, send back error message
        system.debug(tempAccList.size());
        if (!tempAccList.isEmpty()) {
            if (tempAccList.size() == 1) { //Must only be one linked account
                requestorAccount = tempAccList[0];
            } else {
                xmlResult = generateErrorXML(501);
                return xmlResult;               
            }
        } else {
            xmlResult = generateErrorXML(500);
            return xmlResult;
        }
        
        //Validate the Pricebook
        if (requestorAccount.Pricebook__c == NULL) {
            
            try {
                carrierRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Carrier Account').getRecordTypeId();
                carrierProspectRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Carrier Account').getRecordTypeId();
            } catch (Exception e) {
                xmlResult = generateErrorXML(503);
                return xmlResult;               
            }

            if (requestorAccount.RecordTypeId == carrierRT || requestorAccount.RecordTypeId == carrierProspectRT) {
                pricebookName = 'Carrier MEF2 PriceBook';   
            }

        } else {
            pricebookName = requestorAccount.Pricebook__c;
        }

        //If no pricebook throw error
        if (pricebookName == NULL || pricebookName == '') {
            xmlResult = generateErrorXML(502);
            return xmlResult;           
        }

        
        //Get the pricebookID
        List<Pricebook2> tempPricebook = new List<Pricebook2>([SELECT Id FROM Pricebook2 WHERE Name = 'EnterPrise PriceBook' LIMIT 1]);
        //If pricebook can not be found, send back error message
        system.debug('**********list*'+tempPricebook);
        System.debug('****name**'+pricebookName);
       
        if (!tempPricebook.isEmpty()) {
            pricebookId = tempPricebook[0].Id;
        } else {
            xmlResult = generateErrorXML(502);
            return xmlResult;
        }
        
        //Query out the customer Address - servicable location
        List<ServiceableLocation__c> tempCustLocList = new List<ServiceableLocation__c>([SELECT Id, Province_Code__c, City__c, Country__c FROM ServiceableLocation__c WHERE 
                                                                                            eBonding_Key__c = :gblCustomerAddress LIMIT 1]);
        //If a customer location is not found, send back error message
        if (!tempCustLocList.isEmpty()) {
            customerLocation = tempCustLocList[0];
        } else {
            xmlResult = generateErrorXML(100);
            return xmlResult;
        }
        
        //Query out the termination Address - servicable location
        List<ServiceableLocation__c> tempTermLocList = new List<ServiceableLocation__c>([SELECT Id, Province_Code__c, City__c, Country__c FROM ServiceableLocation__c WHERE 
                                                                                            eBonding_Key__c = :gblTerminationAddress LIMIT 1]); 
        //If a termination location is not found, send back error message
        if (!tempTermLocList.isEmpty()) {
            terminationLocation = tempTermLocList[0];
        } else {
            xmlResult = generateErrorXML(101);
            return xmlResult;
        }

        //Evaluate the pricing type, we will set to Inter by default
        pricingType = 'Inter';

        //Check to see if it meets conditions to be Intra pricing
        //Both addresses have the same Province
        if (tempCustLocList[0].Province_Code__c.toUpperCase() == tempTermLocList[0].Province_Code__c.toUpperCase()) { //check province
            if (tempCustLocList[0].Country__c.toUpperCase() == tempTermLocList[0].Country__c.toUpperCase()) { //check country
                pricingType = 'Intra';  
            }           

        }
        
        //Check to see if it meets the conditions to be Metro pricing
        //Both reside in the SAME metro region
        if (tempCustLocList[0].Province_Code__c.toUpperCase() == tempTermLocList[0].Province_Code__c.toUpperCase()) { //check province
            if (tempCustLocList[0].City__c.toUpperCase() == tempTermLocList[0].City__c.toUpperCase()) { //check city
                if (tempCustLocList[0].Country__c.toUpperCase() == tempTermLocList[0].Country__c.toUpperCase()) { //check countru
                    if (metroRegionMap.ContainsKey(tempCustLocList[0].City__c.toUpperCase())) { //check if metro
                        if (metroRegionMap.get(tempCustLocList[0].City__c).Province_Code__c.toUpperCase() == tempCustLocList[0].Province_Code__c.toUpperCase()) { //ensure same metro city
                            pricingType = 'Metro';
                        }
                    }
                }
            } 
        }


        //Query out the product
        List<Product2> tempProductList = new List<Product2>([SELECT Id, Service_Type__c, Access_Type__c, Service_Term__c, Region_Pricing__c 
                                                                FROM Product2 WHERE eBonding_Product_Name__c = :Product AND isActive=true]);    
        //If a termination location is not found, send back error message
        if (tempProductList.isEmpty()) {
            xmlResult = generateErrorXML(300);
            return xmlResult;           
        } 

        //Check to make sure that one Intra, Inter and Metro Product Exist
        Boolean hasMetro =  false;
        Boolean hasIntra = false;
        Boolean hasInter = false; 
        for (Product2 prod :tempProductList) {
            if (prod.Region_Pricing__c == 'Metro') {
                hasMetro = true;
            }
            if (prod.Region_Pricing__c == 'Intra') {
                hasIntra = true;
            }
            if (prod.Region_Pricing__c == 'Inter') {
                hasInter = true;
            }           
        }
        if (!hasMetro || !hasIntra  || !hasInter) { //If any equal false then we generate the error
            xmlResult = generateErrorXML(304);
            return xmlResult;               
        }

        //Query out the product
        List<Product2> tempProduct2List = new List<Product2>([SELECT Id, Service_Type__c, Access_Type__c, Service_Term__c, Charge_Type__c FROM Product2 
                                                                WHERE eBonding_Product_Name__c = :Product AND eBonding_Service_Type__c = :ProductBandwidth AND Access_Type__c = :Access
                                                                AND Service_Term__c = :ContractTerm AND (Charge_Type__c = 'MRC' OR Charge_Type__c = 'NRC')
                                                                AND isActive=true AND Region_Pricing__c = :pricingType]);   
        //Set the MRC and NRC products
        if (!tempProduct2List.isEmpty()) {
            
            if(tempProduct2List.size()>2) { //If more than two returned, should only have 1 product for each MRC and NRC
                xmlResult = generateErrorXML(303);
                return xmlResult;               
            } else {
                
                if (tempProduct2List[0].Charge_Type__c=='MRC') { //check the first product
                    requestedMRCProduct = tempProduct2List[0];
                } else {
                    requestedNRCProduct = tempProduct2List[0];
                }
                
                if (tempProduct2List.size()==2) { //check the 2nd product if it exist
                    if (tempProduct2List[1].Charge_Type__c=='MRC') {
                        if (requestedMRCProduct==NULL) {
                            requestedMRCProduct = tempProduct2List[1];
                        } else {
                            xmlResult = generateErrorXML(303);
                            return xmlResult;                           
                        }
                    } else if (tempProduct2List[1].Charge_Type__c=='NRC' ) {
                        if (requestedNRCProduct==NULL) {
                            requestedNRCProduct = tempProduct2List[1];
                        } else {
                            xmlResult = generateErrorXML(303);
                            return xmlResult;                               
                        }
                    }               
                }
            }
            
        } else {
            xmlResult = generateErrorXML(301);
            return xmlResult;               
        }   
        
        //if no product met criteria
        Set<Id> ProductIdSet = new Set<Id>();
        if (requestedMRCProduct!=NULL) { ProductIdSet.add(requestedMRCProduct.Id); }
        if (requestedNRCProduct!=NULL) { ProductIdSet.add(requestedNRCProduct.Id); }
        
        if (!ProductIdSet.isEmpty()) {
            List<PricebookEntry> tempPBE = new List<PricebookEntry>([SELECT Id, UnitPrice, Product2Id FROM PricebookEntry 
                                                                                    WHERE Product2Id = :ProductIdSet AND Pricebook2Id = :pricebookId AND isActive=true]);
            //If no pricebook entry for the product/pricebook
            if (!tempPBE.isEmpty()) {
                for (PricebookEntry pbe :tempPBE) {
                    priceMap.put(pbe.Product2Id, pbe.UnitPrice);    
                }
            } else {
                xmlResult = generateErrorXML(302);
                return xmlResult;               
            }               
        } else {
            xmlResult = generateErrorXML(301);
            return xmlResult;           
        }       
        
        xmlResult = generateSuccessXML();       
        return xmlResult;
    }
    

    /*** generate the xml response for a succesful request ***/
    public static string generateSuccessXML() {
        
        Xmlstreamwriter xmlW = new Xmlstreamwriter();
        xmlW.writeStartDocument('utf-8','1.0');
            xmlW.writeStartElement(null,'RBS_eBONDING', null);
            xmlW.writeAttribute(null,null,'version','1.0');
                    xmlW.writeStartElement(null,'QUOTE_INFORMATION',null);
                    xmlW.writeAttribute(null,null,'status_code','200');
    
                        xmlW.writeStartElement(null,'AVAILABILITY_OF_SERVICE',null);
                        xmlW.writeAttribute(null,null,'id','1');
                            xmlW.writeCharacters('true');
                        xmlW.writeEndElement(); //Close AVAILABILITY_OF_SERVICE
                        
                        if (requestedMRCProduct!=NULL) {
                            if(priceMap.containsKey(requestedMRCProduct.Id)) {                      
                                xmlW.writeStartElement(null,'MRC',null);
                                    xmlW.writeAttribute(null,null,'id','3');
                                    xmlW.writeCharacters(string.valueOf(priceMap.get(requestedMRCProduct.Id)));
                                xmlW.writeEndElement(); //Close MRC
                            }
                        }
                        
                        if (requestedNRCProduct!=NULL) {
                            if(priceMap.containsKey(requestedNRCProduct.Id)) {                      
                                xmlW.writeStartElement(null,'NRC',null);
                                    xmlW.writeAttribute(null,null,'id','3');
                                    xmlW.writeCharacters(string.valueOf(priceMap.get(requestedNRCProduct.Id)));
                                xmlW.writeEndElement(); //Close NRC
                            }
                        }                                               

                        xmlW.writeStartElement(null,'CONTRACT_TERM',null);
                            xmlW.writeAttribute(null,null,'id','4');
                            xmlW.writeCharacters(gblContractTerm);
                        xmlW.writeEndElement(); //Close CONTRACT_TERM

                        xmlW.writeStartElement(null,'ACCESS_TYPE',null);
                            xmlW.writeAttribute(null,null,'id','5');
                            xmlW.writeCharacters(gblAccess);
                        xmlW.writeEndElement(); //Close ACCESS_TYPE                                             
                                                
    
                xmlW.writeEndElement(); //Close QUOTE_INFORMATION
            xmlW.writeEndElement(); //Close HEADER
        xmlW.writeEndDocument(); //Close XML document
            
        String xmlStringxmlRes = xmlW.getXmlString();     
        xmlW.close();       
        
        logSuccessRecord(xmlStringxmlRes);
        system.debug('XML Result ::: '+xmlStringxmlRes);
        return xmlStringxmlRes;
    }
    
    /*** generate the xml response for a failed request ***/
    public static string generateErrorXML(integer errorCode) {
        Xmlstreamwriter xmlW = new Xmlstreamwriter();
        xmlW.writeStartDocument('utf-8','1.0');
            xmlW.writeStartElement(null,'RBS_eBONDING', null);
            xmlW.writeAttribute(null,null,'version','1.0');
                    xmlW.writeStartElement(null,'QUOTE_INFORMATION_ERROR',null);
                    xmlW.writeAttribute(null,null,'status_code','400');
    
                        xmlW.writeStartElement(null,'ERROR_INFORMATION',null);
                        xmlW.writeAttribute(null,null,'ERROR_CODE', string.valueOf(errorCode));
                            xmlW.writeCharacters(errorCodeMap.get(errorCode));
                        xmlW.writeEndElement(); //Close ERROR_INFORMATION
        
                xmlW.writeEndElement(); //Close QUOTE_INFORMATION_ERROR
            xmlW.writeEndElement(); //Close HEADER
        xmlW.writeEndDocument(); //Close XML document
            
        String xmlStringxmlRes = xmlW.getXmlString();     
        xmlW.close();       
        
        //call method to generate error log
        logErrorRecord(errorCode, xmlStringxmlRes);
        system.debug('XML Result ::: '+xmlStringxmlRes);
        return xmlStringxmlRes;
    }
   
   /*** Method to create a log record (eBonding_Request__c) for a SUCCESS request ***/
   public static void logSuccessRecord(String xmlStringxmlRes) {
        eBonding_Request__c log = new eBonding_Request__c();
        
        log.Customer_Name__c = gblCustomerName;
        log.Customer_Address__c = gblCustomerAddress;
        log.Termination_Address__c = gblTerminationAddress;
        log.Product_Name__c = gblProduct;
        log.Product_Bandwidth__c = gblProductBandwidth;
        log.Access__c = gblAccess;
        log.Contract_Term__c = gblContractTerm;
        log.XML_Response__c = xmlStringxmlRes;
        log.Request_Timestamp__c = System.Now();
        log.Requesting_User__c = UserInfo.getUserId();
        log.Pricebook__c = pricebookId;
        log.Requesting_Account__c = requestorAccount.Id;
        log.Customer_Street__c = gblCustomerStreet;
        log.Customer_City__c = gblCustomerCity;
        log.Customer_Province__c = gblCustomerProvince;
        log.Customer_Country__c = gblCustomerCountry;
        log.Termination_Street__c = gblTerminationStreet;
        log.Termination_City__c = gblTerminationCity;
        log.Termination_Province__c = gblTerminationProvince;
        log.Termination_Country__c = gblTerminationCountry;
        log.Pricing_Type__c = pricingType;
        
        if (requestedMRCProduct!=NULL) { 
            log.MRC_Product__c = requestedMRCProduct.Id; 
            if (priceMap.containsKey(requestedMRCProduct.Id)) {
                log.MRC__c = priceMap.get(requestedMRCProduct.Id);
            }   
        }
        if (requestedNRCProduct!=NULL) { 
            log.NRC_Product__c = requestedNRCProduct.Id; 
            if (priceMap.containsKey(requestedNRCProduct.Id)) {
                log.NRC__c = priceMap.get(requestedNRCProduct.Id);
            }   
        }       
        
        try {
            insert log;
        } catch (dmlException e) {
            System.debug('System Error creating success log: '+e);
        }
    }
    
    /*** Method to create a log record (eBonding_Request__c) for a ERROR request ***/
    public static void logErrorRecord(integer errorCode, String xmlStringxmlRes) {
        eBonding_Request__c log = new eBonding_Request__c();
        
        //error code 601 is more invalid parameters, these will not save to record
        if (errorCode!=601) {
            log.Customer_Name__c = gblCustomerName;
            log.Customer_Address__c = gblCustomerAddress;
            log.Termination_Address__c = gblTerminationAddress;
            log.Product_Name__c = gblProduct;
            log.Product_Bandwidth__c = gblProductBandwidth;
            log.Access__c = gblAccess;
            log.Contract_Term__c = gblContractTerm;
            log.Customer_Street__c = gblCustomerStreet;
            log.Customer_City__c = gblCustomerCity;
            log.Customer_Province__c = gblCustomerProvince;
            log.Customer_Country__c = gblCustomerCountry;
            log.Termination_Street__c = gblTerminationStreet;
            log.Termination_City__c = gblTerminationCity;
            log.Termination_Province__c = gblTerminationProvince;
            log.Termination_Country__c = gblTerminationCountry;
            log.Pricing_Type__c = pricingType;          
        }
        
        log.XML_Response__c = xmlStringxmlRes;
        log.Error_Code__c = errorCode;
        log.Error_Message__c = errorCodeMap.get(errorCode);
        log.Request_Timestamp__c = System.Now();
        log.Requesting_User__c = UserInfo.getUserId();
        
        try {
            insert log;
        } catch (dmlException e) {
            System.debug('System Error creating error log: '+e);
        }
        
    }   
    
    /*** error codes and their error messages ***/
    public static Map<Integer, String> errorCodeMap = new Map<Integer, String>{ 
            100 => 'The Customer Address is not available', 
            101 => 'The Termination Address is not available',
            300 => 'This product does not exist or is not available',
            301 => 'The product you have chosen does not support the combination of attributes specified (bandwidth, access type or contract term)',
            302 => 'This product is unavailable for your Pricebook',
            303 => 'The product and attribute selection has returned more than one MRC or NRC price',
            304 => 'The product you are requesting does not have prices established for all Pricing Regions (Metro, Intra, Inter). Please contact RBS to resolve this issue.',  
            500 => 'The request was made from an unknown source, please confirm with RBS that your user has access to a Pricebook',
            501 => 'The request user is availble for more than one account. Each user can only be assoicated to one account at a time',
            502 => 'No valid Pricebook provided for the Account',
            503 => 'Internal Error: Could not retrieve default Carrier Pricebook',
            600 => 'Incomplete request',
            601 => 'Invalid request, one of the parameters you have specified is longer than 255 characters'                                                    
    };      
}