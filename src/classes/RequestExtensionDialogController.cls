public without sharing class RequestExtensionDialogController {
	public id quoteId {get; set;}
	public Quote q {get; set;}
	public String redirectUrl {get; set;}
	
	public RequestExtensionDialogController(ApexPages.StandardController controller) {
		quoteId = controller.getId();
		q = [SELECT id, OpportunityId, Discount_Reasons__c, Requested_Expiry_Date__c, Extension_Reasons__c, Name, QuoteNumber__c, ExpirationDate, Extension_Requested_days__c, Competitor__c, Competitor_Match__c FROM Quote WHERE id = :quoteId]; 
		this.redirectUrl = '/' + quoteId; 
	}
	
	public void executeLogic() {
		q.requestExtension__c = true;
		
		Date tempDate = Date.today();
		tempDate = tempDate.addDays(Integer.valueOf(q.Extension_Requested_days__c));
		q.Requested_Expiry_Date__c = tempDate;
		
		UPDATE q;

		String errMessage = OpportunityWebServices.submitForApproval(quoteId);
		
		if (''.equals(errMessage))
			this.redirectUrl = '/' + quoteId; 
		else{
			q.requestExtension__c = false;
			UPDATE q;
			this.redirectUrl = '';
		} 
		
	}
}