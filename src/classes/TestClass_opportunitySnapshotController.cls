/*Class Name  : TestClass_opportunitySnapshotController.
 *Description : This is test class for opportunityProductCustomController.
 *Created By  : Deepika Rawat.
 *Created Date :11/15/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
 * Modified: Aakanksha Patel (March 2105: For TAG)
 
*/
@isTest(seeAllData=true)
private class TestClass_opportunitySnapshotController {
     
     private static Account accTestObj;
     private static Opportunity oppTestObj;
     private static user userRec4;
     private static void TestData(){
        Profile profile = [Select Id From Profile Where name = 'System Administrator' Limit 1];
        String randomUserName = String.valueOf(System.now().getTime()) + '@test.com';
        userRec4 = new User(LastName = 'Raj', FirstName='Jeev',Alias = 'aRoger4', Email='test999@Rogertest.com', Username='test999@Rogertest4.com', CommunityNickname = 'nickRog4', ProfileId = profile.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec4;
        accTestObj = new Account(Name='testAcc');
        accTestObj.BillingCountry= 'CA';
        accTestObj.BillingPostalCode = 'A9A 9A9';
        accTestObj.BillingState = 'MA';
        accTestObj.BillingCity='City';
        accTestObj.BillingStreet='Street';
        accTestObj.Account_Status__c= 'Assigned';
        accTestObj.ParentId = null;
        insert accTestObj;
        oppTestObj = new Opportunity();
        oppTestObj.AccountId=accTestObj.id;
        oppTestObj.Name='test';
        oppTestObj.StageName='New';
        oppTestObj.OwnerID=userRec4.id;
        oppTestObj.CloseDate=system.today();
        oppTestObj.Unified_Comm_Collaboration_Estimated__c =20;
        Insert oppTestObj;
     }
     static testmethod void TestMethod1(){
        test.startTest();
         TestData();
         ApexPages.currentPage().getParameters().put('id', accTestObj.id);
         ApexPages.StandardController aCrtl= new ApexPages.StandardController(accTestObj);
         OpportunitySnapshotController snapshotCtrl = new OpportunitySnapshotController(aCrtl);
         snapshotCtrl.stage = 'New'; 
         snapshotCtrl.channel = 'BIS';
         snapshotCtrl.ToDate=System.today();
         snapshotCtrl.datenameFinal=System.today();
         snapshotCtrl.getStageLst(); 
         snapshotCtrl.getChannelLst();
         snapshotCtrl.searchOpp();
        test.stopTest();
     }
     
}