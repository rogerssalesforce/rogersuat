/**************************************************************************************
Apex Class Name     : TAG_ApprovalAutoCancellation
Version             :  
Created Date        : 02/19/2015
Function            : This is a Batch class for rejecting an approval process
                       if its in pending status for more than 32 days.
Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Jayavardhan                  02/19/2015               Original Version  
*************************************************************************************/
global class TAG_ApprovalAutoCancellation implements Database.Batchable<sObject> {
    
    //Variables for Days,Custom Setting value
    Team_Assignment_Governance_Settings__c myCS2;
    Integer Days;
    Date dtApprovaldate;
    
    //Constructor
    global TAG_ApprovalAutoCancellation()
    {
        myCS2 = Team_Assignment_Governance_Settings__c.getInstance();
        Days= Integer.valueof(myCS2.Auto_Cancellation_Days__c);        
        dtApprovaldate = Date.Today()-Days;        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){

        String strQuery = 'select id, status,targetobjectId,createdDate from ProcessInstance where  processinstance.targetobject.type =\'Assignment_Request_Item__c\' and  status = \'Pending\' and createdDate<=:dtApprovaldate';
        system.debug('=========================>strQuery'+strQuery);        
        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<ProcessInstance>  lstProcessInstance = (List<ProcessInstance>)scope;
        List<id> assReqId = new List<id>();
        
        for(ProcessInstance procInstance:lstProcessInstance)
        {
            assReqId.add(procInstance.targetobjectId);
        }
        
        List<ProcessInstanceWorkitem> lstProcessInstanceWorkitem = [Select ProcessInstanceId,ProcessInstance.TargetObjectId,  OriginalActorId, IsDeleted, Id, CreatedDate, CreatedById, ActorId From ProcessInstanceWorkitem where ProcessInstance.TargetObjectId IN: assReqId];
        
        List<Approval.ProcessWorkitemRequest> approvalReqList=new List<Approval.ProcessWorkitemRequest>();
        Approval.ProcessWorkitemRequest pwr; 
        for(ProcessInstanceWorkitem rec:lstProcessInstanceWorkitem )
        {           
            pwr = new Approval.ProcessWorkitemRequest();
            pwr.setcomments('Approval overdue');
            pwr.setworkitemId(rec.Id);
            pwr.setaction('Reject');
            approvalReqList.add(pwr);            
        } 
        List<Approval.ProcessResult> resultList = Approval.process(approvalReqList);        
        // display if the request was successful
        for(Approval.ProcessResult result: resultList )
        {        
            System.debug('Submitted for approval successfully: '+result.isSuccess());      
        }   
        List<Assignment_Request_Item__c> lstRegItem = [select id,status__c from Assignment_Request_Item__c where id IN:assReqId];
        
        for(Assignment_Request_Item__c lstreq:lstRegItem ){          
            lstreq.status__c = 'Cancelled';            
        }   
        
        if(!lstRegItem.IsEmpty()){
            update lstRegItem;  
        }           
        
        
    }

    global void finish(Database.BatchableContext BC){
    }   
}