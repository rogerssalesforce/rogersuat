@isTest(SeeAllData = true)

 
 
private class Test_ScheduleDiscountApproval {

      private static testMethod void test1(){
      List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        List<Opportunity> listOpp = new List<Opportunity>();
        List<Quote> listQuote = new List<Quote>();
        
       Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta)
        {
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        List<Pricebook2> sps = [select id from Pricebook2 where Pricebook2.IsStandard = true];
        
        for (RecordType rto : lRto)
        {
            mapRTo.put(rto.Name,rto.id);  
        }
       
        List<Account> listAcc = new List<Account>();
        Account a11 = new Account();
        a11.name = 'Test Act';
        a11.Business_Segment__c = 'Alternate';
        a11.RecordTypeId = mapRTa.get('Carrier Account');
        a11.BillingStreet = 'Street';
        a11.BillingCity = 'Ontario';
        a11.BillingCountry = 'CA';
        a11.BillingPostalCode = 'A9A 9a9';
        a11.BillingState = 'ON';
        a11.ParentId=null;
        a11.Account_status__c='Assigned';
        listAcc.add(a11);

        Account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('Carrier Account');
        a.BillingStreet = 'Street';
        a.BillingCity = 'Ontario';
        a.BillingCountry = 'CA';
        a.BillingPostalCode = 'A9A 9a9';
        a.BillingState = 'ON';
        a.ParentId=null;
        a.Account_status__c='Assigned';
        //a.RecordType.Name = 'Wireless - New Opportunity';
        insert a;

        Pricebook2 pb = new Pricebook2();
        pb.IsActive = true;
        pb.Name = 'PB1';
        insert pb;

        Product2 p = new product2(name='unittest');
        p.Family = 'Data';
        insert p;
        Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = p.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert standardPrice;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = p.Id, UnitPrice = 1, isActive=true);
        insert pbe;

        List<Opportunity> listOpp11 = new List<Opportunity>();
        Opportunity o11 = new Opportunity();
        o11.Estimated_MRR__c = 500;
        o11.Name = 'Test Opp';
        o11.StageName = 'Suspect - Qualified';
        o11.Product_Category__c = 'Local';
        o11.Network__c = 'Cable';
        o11.Estimated_One_Time_Charge__c = 500;
        o11.New_Term_Months__c = 5;
        o11.AccountId = a11.id;
        o11.Unified_Comm_Collaboration_Estimated__c =20;
        o11.RecordTypeId = mapRTo.get('Wireline - New Enterprise Sale');
        o11.CloseDate = date.today();
        o11.Type='New';
   //     insert o;
        listOpp11.add(o11);

        Opportunity o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.Unified_Comm_Collaboration_Estimated__c =20;
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.RecordTypeId = mapRTo.get('Wireline - New Enterprise Sale');
        o.CloseDate = date.today();
        o.Type='New';
   //     insert o;
        listOpp.add(o);
        
        Opportunity OppDC = new Opportunity();
        OppDC.Estimated_MRR__c = 500;
        OppDC.Name = 'Test Opp';
        OppDC.Unified_Comm_Collaboration_Estimated__c =20;
        OppDC.StageName = 'Suspect - Qualified';
        OppDC.Product_Category__c = 'Local';
        OppDC.Network__c = 'Cable';
        OppDC.Estimated_One_Time_Charge__c = 500;
        OppDC.New_Term_Months__c = 5;
        OppDC.AccountId = a.id;
        OppDC.RecordTypeId = mapRTo.get('Data Centre - Transactional');
        OppDC.Pricebook2ID =  standardPB.id;
        OppDC.Type='New';
        OppDC.CloseDate = system.today().addDays(10);
        listOpp.add(OppDC);
        insert listOpp;
      insert listOpp11;       

       list<Quote> listQuote11 = new list<Quote>();
       Quote q11 = new Quote();
        q11.OpportunityId = listOpp[0].id;
        q11.Name = 'Test Quote';
        q11.Pricebook2Id = pb.id;
        q11.Term__c = '3-Years';
    //q.AccountRecordType__c = 'channel';
        q11.Approval_Escalation_Started__c = false;
        //q11.Discount = 1;
        //q11.Empty_List_Price__c = 2;
        //q11.Inconsistent_Pricing__c = 3;
        listQuote11.add(q11);  
       insert listQuote11;
       
        Quote q = new Quote();
        q.OpportunityId = listOpp[0].id;
        q.Name = 'Test Quote';
        q.Pricebook2Id = pb.id;
        q.Term__c = '3-Years';
    //q.AccountRecordType__c = 'channel';
        q.Approval_Escalation_Started__c = false;
        //q.Discount = 0;
        //q.Empty_List_Price__c = 0;
        //q.Inconsistent_Pricing__c = 0;
        listQuote.add(q);    

        Quote q2 = new Quote();
        q2.OpportunityId = listOpp[0].id;
        q2.Name = 'Test Quote2';
        q2.Pricebook2Id = standardPB.id;
        q2.Term__c = '3-Years';
        //q2.Discount = 4;
        //q2.Empty_List_Price__c = 5;
        //q2.Inconsistent_Pricing__c = 6;
//  q.AccountRecordType__c = 'carrier';
        listQuote.add(q2);
    
        Quote q3 = new Quote();
        q3.OpportunityId = listOpp[0].id;
        q3.Name = 'Test Quote2';
        q3.Pricebook2Id = pb.id;
        q3.Term__c = '3-Years';
        listQuote.add(q3); 
//  q.AccountRecordType__c = 'enterprise';      
        insert listQuote;
        
        listQuote[0].Approval_Escalation_Started__c = true;
        update listQuote;
            
        QuoteLineItem qli = new QuoteLineItem();
        qli.quoteid = listQuote[1].Id;
        qli.PricebookEntryId = standardPrice.id;
        qli.Discount = 10;
        qli.UnitPrice= 100;
        qli.quantity=1;

        insert qli;
        ScheduleDiscountApproval s2 = new ScheduleDiscountApproval ();
       ScheduleDiscountReportProcessing schdis = new ScheduleDiscountReportProcessing();
      
        Pending_Approval_Submission__c pend = new Pending_Approval_Submission__c();
        pend.Quote_to_Approve__c = listQuote[0].Id;
        pend.Next_Approver__c = listQuote[0].Manager_s_Id__c;
        INSERT pend;
      
        Pending_Approval_Submission__c pend2 = new Pending_Approval_Submission__c();
        pend2.Quote_to_Approve__c = listQuote[1].Id;
        pend2.Next_Approver__c = listQuote[1].Manager_s_Id__c;
        INSERT pend2;
        Pending_Approval_Submission__c pend3 = new Pending_Approval_Submission__c();
        pend3.Quote_to_Approve__c = listQuote[2].Id;
        pend3.Next_Approver__c = listQuote[2].Manager_s_Id__c;
        INSERT pend3;
        
       
        Pending_Approval_Submission__c pend311 = new Pending_Approval_Submission__c();
        pend311.Quote_to_Approve__c = listQuote11[0].Id;
        pend311.Next_Approver__c = listQuote11[0].Manager_s_Id__c;
        INSERT pend311;
        
        Test.startTest();
        String nextFireTime = '0 0 0 1 * ? *';
        system.schedule('Test Start me once', nextFireTime, s2);
        
        Test.stopTest(); 
    }
    
     
    }