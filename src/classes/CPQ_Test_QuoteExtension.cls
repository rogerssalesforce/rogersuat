/*********************************************************************************
Class Name      : CPQ_Test_QuoteExtension 
Description     : This class a test class for CPQ_Extension_Quote. 
Created By      : Alina Balan
Created Date    : 18-Dec-2015
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Alina Balan              18-Dec-2015             Original version
----------------------------------------------------------------------------------            
*********************************************************************************/

@isTest (seeAllData=False) 
public with sharing class CPQ_Test_QuoteExtension {
    private static User TestAEUser;
    private static Account oAccount;
    private static Opportunity oOpportunity;
    private static Apttus_Proposal__Proposal__c oQuote1;
    private static Apttus_Proposal__Proposal__c oQuote2;
    private static Apttus_Config2__PriceList__c oPriceList;
    private static final String CPQ_ACCOUNT_SPECIALIST = 'EBU - Rogers Specialist';
    private static final String CPQ_SPECIALIST_USERNAME = 'specialistuser_test123@testorg.com';
    private static final String CPQ_AE_PROFILE = 'EBU - Rogers Account Executive';
    private static final String CPQ_OBJECT_NAME = 'Test Name';
    private static final String ABANDON_PARAM = 'abandon';
    private static final String ID_PARAM = 'id';
    private static final String STRING_FALSE = 'false';
    private static final String STRING_TRUE = 'true';
    private static final String STAGE_ABANDONED = 'Abandoned';
    
    /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    :   
    Parameter      :               
    *********************************************************************************/ 
    private static void createTestData() {
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser(CPQ_AE_PROFILE);
        system.runAs(TestAEUser){
            //Create PriceList
            oPriceList = CPQ_Test_Setup.newPriceList();
            insert oPriceList ;
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount(CPQ_OBJECT_NAME);
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            //Create Quote
            oQuote1 = CPQ_Test_Setup.newQuote(oOpportunity, oPriceList );
            insert oQuote1;
            oQuote2 = CPQ_Test_Setup.newQuote(oOpportunity, oPriceList );
            insert oQuote2;
        }
    }
    
    /*********************************************************************************
    Method Name    : testAutoRun
    Description    : Method to test "AutoRun" method
    Return Type    :   
    Parameter      :               
    *********************************************************************************/ 
    static testmethod void testAutoRun(){
        createTestData();
        
        PageReference pageRef = Page.CPQ_SyncQuoteWithOpportunity;
        pageRef.getParameters().put(ID_PARAM, String.valueOf(oQuote1.Id));
        pageRef.getParameters().put(ABANDON_PARAM, String.valueOf(STRING_FALSE));
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdController = new ApexPages.StandardController(oQuote1);
        CPQ_Extension_Quote extensionQuoteController = new CPQ_Extension_Quote(stdController);

        system.runAs(TestAEUser){
            test.startTest();
            String sAbandonParameterValue = ApexPages.currentPage().getParameters().get(ABANDON_PARAM);
            String sIdParameterValue = ApexPages.currentPage().getParameters().get(ID_PARAM);
            system.assertEquals(sAbandonParameterValue, STRING_FALSE);
            system.assertEquals(sIdParameterValue, String.valueOf(oQuote1.Id));
            
            extensionQuoteController.autoRun();
            Apttus_Proposal__Proposal__c oProposalQuote = [SELECT Id, Synced_with_Opportunity__c FROM Apttus_Proposal__Proposal__c WHERE Id =: oQuote1.Id];
            system.assertEquals(oProposalQuote.Synced_with_Opportunity__c, true);
            test.stopTest();
        }
    }
    
    /*********************************************************************************
    Method Name    : testAutoRunAbandon
    Description    : Method to test "AutoRun" method, with abandon param set to true
    Return Type    :   
    Parameter      :               
    *********************************************************************************/ 
    static testmethod void testAutoRunAbandon(){
        createTestData();
        
        PageReference pageRef = Page.CPQ_SyncQuoteWithOpportunity;
        pageRef.getParameters().put(ID_PARAM, String.valueOf(oQuote1.Id));
        pageRef.getParameters().put(ABANDON_PARAM, String.valueOf(STRING_TRUE));
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdController = new ApexPages.StandardController(oQuote1);
        CPQ_Extension_Quote extensionQuoteController = new CPQ_Extension_Quote(stdController);

        system.runAs(TestAEUser){
            test.startTest();
            String sAbandonParameterValue = ApexPages.currentPage().getParameters().get(ABANDON_PARAM);
            String sIdParameterValue = ApexPages.currentPage().getParameters().get(ID_PARAM);
            system.assertEquals(sAbandonParameterValue, STRING_TRUE);
            system.assertEquals(sIdParameterValue, String.valueOf(oQuote1.Id));
            
            extensionQuoteController.autoRun();
            Apttus_Proposal__Proposal__c oProposalQuote = [SELECT Id, Apttus_Proposal__Approval_Stage__c FROM Apttus_Proposal__Proposal__c WHERE Id =: oQuote1.Id];
            system.assertEquals(oProposalQuote.Apttus_Proposal__Approval_Stage__c, STAGE_ABANDONED);
            test.stopTest();
        }
    } 
}