public without sharing class SiteManager {
public class SessionException extends Exception{}
    /* This method has been refactored to use the CLLI Code instead of the Postal Code for the concatanation of fields for the Name.
       This is because the exact match within process sites uses the Serviceable Location Name and Site Name to perform an exact match.
       Since the site most likely does not have a CLLI Code, we will need to obtain the CLLI Code from the Postal Code (if given)
       The trigger is to be handled so that we do not exceed the SOQL statements governor limit - this will be done by:
       
       1. Go through the newSites and obtain the postal Code in a list.
       2. Query the CLLIPostalCode table to obtain a list of records that match the postal codes from 1
       3. To improve performance - Create a Map where Postal Code is the key and CLLI Code is the value
       3. Traverse the List of sites and match the Postal Code if one exists to the records in 2 and set the CLLI Code in the Site Object
            A. If the Site currently has a CLLI Code this will overwrite it if a new one is found.
            B. If there is not a postal code then the CLLI Code will not be overwritten.
    */ 
    public static void handleSiteNameUpdate(List<Site__c> oldSites, List<Site__c> newSites){
        
        List<String> postalCodes = new List<String>();
        List<CLLIPostalCode__c> clliPostalCodes = new List<CLLIPostalCode__c>(); 
        Map<String, String> pcToCLLIMap = new Map<String, String>();
       
        for (Site__c s : newSites){
            if (!Utils.isEmpty(s.Postal_Code__c) && !Utils.listContains(postalCodes, s.Postal_Code__c))
                postalCodes.add(s.Postal_Code__c);
        }
        
        if (!postalCodes.isEmpty())
            clliPostalCodes = [SELECT Id, CLLI_Code__c, Postal_Code__c FROM CLLIPostalCode__c where Postal_Code__c IN :postalCodes];
        
        for (CLLIPostalCode__c entry : clliPostalCodes){
            pcToCLLIMap.put(entry.Postal_Code__c, entry.CLLI_Code__c);
        }
        
        for (Site__c s : newSites){
            if (!Utils.isEmpty(s.Postal_Code__c))
                s.CLLI_SWC__c = pcToCLLIMap.get(s.Postal_Code__c);
                            
            if ((Utils.isEmpty(s.Type__c))&&(s.ServiceableLocation__c!=null)){
                s.Type__c = 'Serviceable Location';
            }
            
            s.Name = Utils.removeNull(s.Suite_Floor__c) + Utils.removeNull(s.Street_Number__c) + Utils.removeNull(s.Street_Name__c) + Utils.removeNull(s.Street_Type__c) + Utils.removeNull(s.Street_Direction__c) + Utils.removeNull(s.City__c) + Utils.removeNull(s.Province_Code__c) + Utils.removeNull(s.CLLI_SWC__c) + ((s.Type__c=='CNI' || s.Type__c=='NNI') ? s.Type__c : '');

            s.Proposal_Display_Name__c = (Utils.isEmpty(s.Suite_Floor__c)?'':(s.Suite_Floor__c + ' ')) + (Utils.isEmpty(s.Street_Number__c)?'':(s.Street_Number__c + ' ')) + (Utils.isEmpty(s.Street_Name__c)?'':(s.Street_Name__c + ' ')) + (Utils.isEmpty(s.Street_Type__c)?'':(s.Street_Type__c )) + ((!Utils.isEmpty(s.Street_Direction__c)&&!Utils.isEmpty(s.Street_Type__c))?' ':'') + (Utils.isEmpty(s.Street_Direction__c)?'':(s.Street_Direction__c)) + (Utils.isEmpty(s.City__c)?'':(', ' + s.City__c + ' ')) + (Utils.isEmpty(s.Province_Code__c)?'':(s.Province_Code__c + ' ')) + (Utils.isEmpty(s.Postal_Code__c)?'':(s.Postal_Code__c + ' '));            
            s.Display_Name__c = s.Proposal_Display_Name__c + ((s.Type__c=='CNI' || s.Type__c=='NNI') ? ' - (' + s.Type__c  + (s.Is_a_Z_Site__c ? '-Z':'') + ')': (s.Is_a_Z_Site__c ? ' - (Z)':''));
        
        }
    }  
    
    /*
    public static void handleSiteOwnership(List<Site__c> newSites, Map<Id, Site__c> siteMap){
        
        List<Site__c> siteList = new List<Site__c>();
        Map<String, List<Site__c>> sMap = new Map<String, List<Site__c>>();
        
        Set<String> siteNames = new Set<String>();
        for (Site__c s : newSites){
            siteNames.add(s.Name);
        }
        
        try{
            siteList = [SELECT Id, Name, Opportunity__c FROM Site__c where Opportunity__r.isClosed = false AND Opportunity__r.LastActivityDate = LAST_N_DAYS:90 AND Name IN :siteNames];
        }catch(Exception ex){
        }
        
        for (Site__c s : siteList){
            if (sMap.get(s.Name)==NULL){
                List<Site__c> temp = new List<Site__c>();
                temp.add(s);
                sMap.put(s.Name, temp);
            }else{
                List<Site__c> temp = sMap.get(s.Name);
                temp.add(s);
                sMap.put(s.Name, temp);
            }
        }
        for(Site__c s : newSites){
            List<Site__c> sites = new List<Site__c>();
            sites = sMap.get(s.Name);

            if (sites != null && !sites.isEmpty()){
                for (Site__c a : sites){
                    if (a.Opportunity__c != s.Opportunity__c){
                        s.addError('You are attempting to add a Prospect that is currently part of another Active Opportunity! - ' + s.Proposal_Display_Name__c + ' was not added.');
                    }
                }
            }
            
            
        }
    } 
    
     public static void handleSiteCurrentlyOnQuote(List<Site__c> oldSites, List<Site__c> newSites){
        
        // Create old and new SiteMaps
        
        Map<Id, Site__c> oldSiteMap = new Map<Id, Site__c>();
        Map<Id, Site__c> newSiteMap = new Map<Id, Site__c>();
        
        if (oldSites!=null){
        	for(Site__c s1 : oldSites){
        		oldSiteMap.put(s1.Id, s1);
        	}
        }
         for(Site__c s2 : newSites){
        	newSiteMap.put(s2.Id, s2);
        }
        
        
        List<Quote_Site__c> qsList = new List<Quote_Site__c>();
        List<Site__c> siteList = new List<Site__c>();
        Map<String, List<Quote_Site__c>> sMap = new Map<String, List<Quote_Site__c>>();
        
        Set<Id> serviceableLocations = new Set<Id>();
     
       	Set<Id> siteIds = newSiteMap.keySet();
        
        
        for (Id sId : siteIds){
        	if (newSiteMap.get(sId).ServiceableLocation__c != NULL && (oldSiteMap.get(sId) == NULL || (newSiteMap.get(sId).ServiceableLocation__c != oldSiteMap.get(sId).ServiceableLocation__c)))
            	serviceableLocations.add(newSiteMap.get(sId).ServiceableLocation__c);// Change to use the Servicable Location ID as the map
        }
        
        System.debug('\n\n\n\n\nserviceableLocations: '+ serviceableLocations);
        
        try{// Change to use the Servicable Location ID as the map
            qsList = [SELECT Quote__r.ExpirationDate, Quote__r.QuoteNumber__c, Site__c, Site__r.Name, Site__r.ServiceableLocation__c FROM Quote_Site__c where Site__r.ServiceableLocation__c IN :serviceableLocations AND Quote__r.ExpirationDate >= today];
        
        }catch(Exception ex){
        }
        
        System.debug('\n\n\n\n\nqsList: '+ qsList);
        
        for (Quote_Site__c qs : qsList){
            if (sMap.get(qs.Site__r.ServiceableLocation__c)==NULL){
                List<Quote_Site__c> temp = new List<Quote_Site__c>();
                temp.add(qs);
                sMap.put(qs.Site__r.ServiceableLocation__c, temp);
            }else{
                List<Quote_Site__c> temp = sMap.get(qs.Site__r.ServiceableLocation__c);
                temp.add(qs);
                sMap.put(qs.Site__r.ServiceableLocation__c, temp);
            }
        }
        
        System.debug('\n\n\n\n\nsMap: '+ sMap);
        
        for(Site__c s : newSites){
            List<Quote_Site__c> sites = new List<Quote_Site__c>();
            sites = sMap.get(s.ServiceableLocation__c);

            if (sites != null && !sites.isEmpty()){
	            s.addError('You are attempting to add a Prospect that is currently part of a non-expired quote! - ' + s.Proposal_Display_Name__c + ' was not added because it is already on Quote Number: ' + sites[0].Quote__r.QuoteNumber__c);
            }
        }
    } 
    */
     public static void handleDuplicateSites(List<Site__c> newSites, Map<Id, Site__c> siteMap){
        List<Site__c> siteList = new List<Site__c>();
        Map<String, List<Site__c>> sMap = new Map<String, List<Site__c>>();
        
        Set<String> siteNames = new Set<String>();
        for (Site__c s : newSites){
            siteNames.add(s.Name);
        }
        
        try{
            siteList = [SELECT Id, Name, Opportunity__c FROM Site__c where Name IN :siteNames];
        }catch(Exception ex){
        }
              
        for (Site__c s : siteList){
            if (sMap.get(s.Name)==NULL){
                List<Site__c> temp = new List<Site__c>();
                temp.add(s);
                sMap.put(s.Name, temp);
            }else{
                List<Site__c> temp = sMap.get(s.Name);
                temp.add(s);
                sMap.put(s.Name, temp);
            }
        }
        for(Site__c s : newSites){
            List<Site__c> sites = new List<Site__c>();
            sites = sMap.get(s.Name);

            if (sites != null && !sites.isEmpty()){
                for (Site__c a : sites){
                    if (a.Opportunity__c == s.Opportunity__c){
                        s.addError('You are attempting to add a Prospect that is currently already added to this opportunity! - ' + s.Proposal_Display_Name__c + ' was not readded.');
                    }
                }
            }
            
            
        }
     }
}