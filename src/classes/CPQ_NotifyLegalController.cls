/*********************************************************************************
Class Name      : CPQ_NotifyLegalController
Description     : This class is related to VisualForce Page CPQ_NotifyLegalOfNonStandardClauses
Created By      : Ramiya Kandeepan
Created Date    : 04-Nov-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Ramiya Kandeepan            04-Nov-15             Initial Version
----------------------------------------------------------------------------------            
*********************************************************************************/

public with sharing class CPQ_NotifyLegalController{

    private Apexpages.StandardController stdController;
    public String sURLid{get;set;}
    public Boolean bShouldRedirect{get;set;}
    public String sUrl{get;set;}
    public string sMyInput{get;set;}
    public String sUsers;
    Apttus__APTS_Agreement__c oAgreementObj;
     
    PageReference nextPage;
    
    //Static Constant
    public static final String CPQ_LegalStatus_PendingReview = 'Pending Review';
    public static final String CPQ_ApttusStatus_InternalReview = 'Internal Review';
    public static final String CPQ_ApttusStatusCategory_InAuthoring = 'In Authoring';
    public static final String CPQ_Task_Description = 'Review non-standard clause';
    public static final String CPQ_Task_Priority = 'Normal';
    public static final String CPQ_Task_Status = 'Not Started';
    public static final String CPQ_Task_Subject = 'Task';
    public static final String CPQ_Task_SubjectType = 'Other';
    public static final Integer CPQ_Task_DueDuration = 2;
    public static final Integer CPQ_Task_ReminderDuration = 1;
    
     /*********************************************************************************
    Method Name    : CPQ_NotifyLegalController
    Description    : This is a constructor class.
    Return Type    : null
    Parameter      : ApexPages.StandardController
    *********************************************************************************/
    public CPQ_NotifyLegalController(ApexPages.StandardController apStdController){
        this.stdController = apStdController;
        sURLid = ApexPages.currentPage().getParameters().get('id');
        sUrl = URL.getSalesforceBaseUrl().toExternalForm()+ '/' + sURLid; 
        oAgreementObj = [Select Id,Legal_Status__c,CPQ_Customer_Non_Standard_Clause__c,Apttus__Requestor__c,Apttus__Status_Category__c,Owner.name,Apttus__Status__c from Apttus__APTS_Agreement__c where id = :sURLid];
        sMyInput = oAgreementObj.CPQ_Customer_Non_Standard_Clause__c; 
        nextPage = new PageReference('/' + oAgreementObj.Id);
        
    }
 
     /*********************************************************************************
    Method Name    : doCancel
    Description    : This method closes the dialog window and return to the record
    Return Type    : void
    Parameter      : ApexPages.StandardController
    *********************************************************************************/
    public  PageReference doCancel(){
        return nextPage; 
    }

  /*********************************************************************************
    Method Name    : doSave
    Description    : This method is to save the customer input of non standard clauses and save.
    Return Type    : void
    Parameter      : ApexPages.StandardController
    *********************************************************************************/
    public PageReference doSave(){
       if(!String.isBlank(sMyInput)){
           //[RK 13-Nov-15]: Updating Agreement object's status fields and customer clause when a customer request non-standard clauses
           oAgreementObj.CPQ_Customer_Non_Standard_Clause__c = sMyInput;
           oAgreementObj.Legal_Status__c = CPQ_LegalStatus_PendingReview;
           oAgreementObj.Apttus__Status__c = CPQ_ApttusStatus_InternalReview;
           oAgreementObj.Apttus__Status_Category__c = CPQ_ApttusStatusCategory_InAuthoring;
           sUsers = oAgreementObj.Owner.name;
           try{
               update oAgreementObj;
           }
           catch(DMLException e){
               ApexPages.addMessages(e); 
           }
           
            //[RK 13-Nov-15]: Custom settings for Legal queue
           CPQ_Settings__c cpqCS = CPQ_Settings__c.getOrgDefaults();
           system.debug('settings: ' +cpqCS.LegalQueueName__c);
           //[RK 13-Nov-15]: Querying legal queue id for memeber extraction
           LIST<Group> lstQueueId = [Select Id from Group where type='Queue' and Name =: cpqCS.LegalQueueName__c LIMIT 1];

           //[RK 13-Nov-15]: Finding list of group members belong to the queue from the queue id
           LIST<GroupMember> lstGroupmember = [Select UserOrGroupId From GroupMember where GroupId IN:lstQueueId];
           
           //[RK 13-Nov-15]: Looping through each of records to get memeber in the queue 
           Set<Id> setUserids = new Set<Id>();
           for(GroupMember oGrp: lstGroupmember){
               setUserids.add(oGrp.UserOrGroupId);
           }
           LIST<Task> lstTasks = new LIST<Task>();
           
           //[RK 13-Nov-15]: Querying user id for each memeber
           LIST<User> lstUser = [Select id from User where id in :setUserids and isActive=true];
           
           //[RK 13-Nov-15]: Creating tasks for each user in the queue
           for( User oUser : lstUser ){
               Task oNewTask = new Task();
               oNewTask.Description = CPQ_Task_Description;
               oNewTask.Priority = CPQ_Task_Priority;
               oNewTask.Status = CPQ_Task_Status;
               oNewTask.Subject = CPQ_Task_Subject;
               oNewTask.Subject_Type__c = CPQ_Task_SubjectType;
               oNewTask.IsReminderSet = true;
               oNewTask.ReminderDateTime = System.now()+CPQ_Task_ReminderDuration; 
               oNewTask.WhatId = oAgreementObj.Id; 
               oNewTask.OwnerId = oUser.Id;
               oNewTask.ActivityDate = System.today() + CPQ_Task_DueDuration;
               lstTasks.add(oNewTask);
           }
           
          //[RK 13-Nov-15]: inserting new tasks
           if(lstTasks.size() > 0){
               try{
                   insert lstTasks;
               }
               catch(DMLException e){
                   ApexPages.addMessages(e); 
               }
               
           }
               
           bShouldRedirect = true;
           return nextPage;             
        }
         //[RK 13-Nov-15]: Displaying error message for blank clauses
        else if(String.isBlank(sMyInput)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, System.label.CPQ_EngageLegalInvalidDataMSG));
        }
    
        return null;
        
    }
   
}