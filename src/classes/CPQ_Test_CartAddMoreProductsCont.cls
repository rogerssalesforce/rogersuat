/*********************************************************************************
Class Name      : CPQ_Test_CartAddMoreProductsCont c
Description     : This class a test class for CPQ_CartAddMoreProductsCont . 
Created By      : Deepika Rawat
Created Date    : 1-Nov-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Deepika Rawat              1-Nov-15              Original version
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest (seeAllData=False) 
private class CPQ_Test_CartAddMoreProductsCont{
    static User TestAEUser;
    static Account oAccount;
    static product2 oProduct;
    static Opportunity oOpportunity;
    static Apttus_Proposal__Proposal__c oQuote;
    static Attachment attachmentQuote;
    static Apttus_Config2__PriceList__c oPriceList;
    static Apttus_Config2__PriceListItem__c oPriceListItem;
    static ID prodConfigID;
    static Pricebook2 oPricebook;
    static Pricebook2 oPriceBookCustom;
    private static CPQ_Settings__c cs;
    static List<Apttus_Config2__ProductConfiguration__c> lstProductConfig ;
     /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testMethod void createTestData() {
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        cs  = CPQ_Settings__c.getOrgDefaults();
        cs.DefaultOpportunityRecordType__c = 'Rogers EBU - New';
        upsert cs;
        //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');      
        system.runAs(TestAEUser){
            //Create custom Pricebook 
            oPriceBookCustom = CPQ_Test_Setup.newPriceBook('CPQ Temp PriceBook');
            insert oPriceBookCustom;    
            //Create newPriceBook
            oPricebook = CPQ_Test_Setup.newPriceBook('Test Pricelist');
            insert oPricebook;
            //Create PriceList
            oPriceList = CPQ_Test_Setup.newPriceList();
            insert oPriceList ;
            // Create Product
            oProduct = CPQ_Test_Setup.newProduct();
            oProduct.Family = 'Wireless - Voice';
            insert oProduct;
            //Create Pricelist item
            oPriceListItem = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oProduct.ID);
            insert oPriceListItem;
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
            insert oQuote;
            //Product Configuration
            prodConfigID = CPQ_Test_Setup.newProdictConfiguration(oQuote);
            //Create line items
            CPQ_Test_Setup.createLineItem(prodConfigID,oProduct.id,oPriceListItem.id);
            lstProductConfig = [Select id, Apttus_QPConfig__Proposald__c, Apttus_Config2__Status__c from  Apttus_Config2__ProductConfiguration__c where Apttus_QPConfig__Proposald__c =:oQuote.id ];
        }
    }
    /*********************************************************************************
    Method Name    : testAddCartValid
    Description    : Test "Ready For Review" as AE with Saved Quote
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testAddCartValid(){
        createTestData();
        oQuote.Apttus_Proposal__Approval_Stage__c = 'Draft';
        update oQuote;
        system.runAs(TestAEUser){
            test.startTest();
            ApexPages.currentPage().getParameters().put('id', prodConfigID);
            ApexPages.currentPage().getParameters().put('configrequestid', prodConfigID);
            CPQ_CartAddMoreProductsCont obj = new CPQ_CartAddMoreProductsCont();
            PageReference  pg = obj.addMoreProducts();
            obj.backToCart();
            System.assert(pg.getUrl().containsIgnoreCase('catalog'));
            test.stopTest();
        }
    } 
    /*********************************************************************************
    Method Name    : testAddCartNotValid
    Description    : Test "Ready For Review" as AE with Saved Quote
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testAddCartNotValid(){
        createTestData();
        oQuote.Apttus_Proposal__Approval_Stage__c = 'Approved';
        update oQuote;
        system.runAs(TestAEUser){
            test.startTest();
            ApexPages.currentPage().getParameters().put('id', prodConfigID);
            ApexPages.currentPage().getParameters().put('configrequestid', prodConfigID);
            CPQ_CartAddMoreProductsCont obj = new CPQ_CartAddMoreProductsCont();
            obj.addMoreProducts();
            PageReference  pg =obj.backToCart();
            System.assert(pg.getUrl().containsIgnoreCase('cart'));
            test.stopTest();
        }
    } 
}