/*********************************************************************************
Class Name      : SitesController
Description     : This class is used as a extension class for add site page
Created By      : 
Created Date    : 
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Alina Balan          18-November-2015            Refactoring
*********************************************************************************/

global class SitesController implements ObjectPaginatorListener {
    public Boolean bFromOpportunity {get; set;}
    public Boolean bDuplicateCheck {get; set;}
    public Boolean bModifyServiceablLocation {get; set;}

    global ObjectPaginator paginator {get;private set;}
    // the collection of serviceablity to display
    global List<ServiceableLocationWrapper> lstServiceableLocations {get; private set;}
    
    private Opportunity oOpportunity {get; set;}
    private String sDuplicateMessage {get; set;}
    @TestVisible private Boolean bDisplayDuplicateMessage {get; set;}
    private Integer intDefaultAccessTypeGroup {get; set; }
    private String sDefaultATG {get; set; }
    // the soql without the order and limit
    @TestVisible private String sSoql {get;set;}
    private Id oppId;
    private List<String> lstClliCodes;
    private static final String SERVICEABLE_LOCATION = 'Serviceable Location'; 
    private static final String RET_URL = 'retURL';
    private static final String DUP_CHECK = 'dupCheck';
    private static final String NAME_FIELD = 'name';
    private static final Integer RECORDS_ON_PAGE = 20;
    private static final String IGNORED_FIELD_TEXT = 'Please%20Ignore%20Field';
    private static final String SUITE_MAP = 'suiteMap';
    private static final String SUITE_MAP_SPLITTER = '~';
    private static final String SUITE_ID_SPLITTER = '@';
    private static final String SITE_DUPLICATION_MESSAGE = 'This customer site has yet to be quoted for a Rogers service!';
    private static final String SITE_DUPLICATION_MESSAGE_SITE_QUOTED = 'This customer site has recently been quoted for Rogers services. If you would like to escalate or find out more information please contact your direct sales manager.\n';
    private static final String MATCHIMG_ACCOUNTS_TEXT = '<b>Matching Accounts: </b>';
    private static final String FIBRE = 'fibre';
    private static final String SERVICEABLE_LOCATION_NAME_PARAM = 'serviceableLocationName';
    private static final String SUITE_PARAM = 'suite';
    private static final String STREET_NUMBER_PARAM = 'streetNumber';
    private static final String STREET_NAME_PARAM = 'streetName';
    private static final String STREET_TYPE_PARAM = 'streetType';
    private static final String STREET_DIRECTION = 'streetDirection';
    private static final String CITY_PARAM = 'city';
    private static final String POSTAL_CODE_PARAM = 'postalCode';
    private static final String CNINNI_PARAM = 'cninni';
    private static final String ACCESS_TYPE_GROUPS_PARAM = 'accessTypeGroups';
    private static final String NETWORK_ACCESS_TYPE_PARAM = 'networkAccessTypes';
   
    //the current sort direction. defaults to asc
    public String sortDir {
        get  { if (sortDir == null) { sortDir = CPQ_Constants.ASC_SORT_DIR; } return sortDir;  }
        set;
    }
    // the current field to sort by. defaults to last name
    public String sortField {
        get  { if (sortField == null) {sortField = NAME_FIELD; } return sortField;  }
        set;
    }
    // format the soql for display on the visualforce page
    public String debugSoql {
        get { return sSoql + ' order by ' + sortField + ' ' + sortDir + ' limit ' + RECORDS_ON_PAGE; }
        set;
    }
   
    /*********************************************************************************
    Method Name    : SitesController
    Description    : Constructor method: init the controller and display some sample data when the page loads
    Return Type    : 
    Parameter      : ApexPages.StandardController
    *********************************************************************************/
    public SitesController(ApexPages.StandardController controller) {
        this.lstServiceableLocations = new List<ServiceableLocationWrapper>();
        oppId = ApexPages.currentPage().getParameters().get(RET_URL);
        String sDupCheck = ApexPages.currentPage().getParameters().get(DUP_CHECK);
        String sEditServiceable = ApexPages.currentPage().getParameters().get(CPQ_Constants.EDIT);
        sDuplicateMessage = '';
        // This will allow us to use the Serviceability functionality for duplicate Checking by setting this param in the URL 
        if (!Utils.isEmpty(sDupCheck) && sDupCheck.equals(CPQ_Constants.INT_TRUE_VALUE)) {
            bDuplicateCheck = true;
        } else {
            bDuplicateCheck = false;
        }
        // This will allow us to use the Serviceability functionality for duplicate Checking by setting this param in the URL 
        if (!Utils.isEmpty(sEditServiceable) && sEditServiceable .equals(CPQ_Constants.INT_TRUE_VALUE)) {
            bModifyServiceablLocation = true;
        } else {
            bModifyServiceablLocation = false;
        }
        //check for empty opportunity id
        if (!Utils.isEmpty(oppId)){
            bFromOpportunity = true;
            oOpportunity = [SELECT Id, Name, AccountId From Opportunity WHERE Id = :oppId];
        } else {
            bFromOpportunity = false;
        }
        // Obtain the default Access Type Group
        Schema.DescribeFieldResult fieldResult = ServiceableLocation__c.Access_Type_Group__c.getDescribe();
        List<Schema.PicklistEntry> lstPicklistEntries = fieldResult.getPicklistValues();
        sDefaultATG = '';
        //Iterating through all Access Type Group picklist entries 
        for(Schema.PicklistEntry picklistEntry : lstPicklistEntries) {
            //check for default value
            if (picklistEntry.isDefaultValue())
                sDefaultATG = picklistEntry.getLabel();
        }
        bDisplayDuplicateMessage = false;
        lstClliCodes = new List<String>();
        sSoql = 'SELECT Name, Suite_Floor__c, Street_Number__c, Street_Name__c, Street_Type__c, Street_Direction__c, Province_Code__c, CLLI_Code__c, Postal_Code__c, City__c, Access_Type__c,' + 
                'Access_Type_Group__c, CNINNI__c, Network_Access_Type__c, Build_Date__c, Display_Name__c FROM serviceableLocation__c';
        //check for empty default Access Type Group
        if (!Utils.isEmpty(sDefaultATG)) {
            sSoql += ' WHERE Access_Type_Group__c INCLUDES (\'' + sDefaultATG + '\')'; 
        }
        runQuery();
        List<ServiceableLocationWrapper> lstServiceableLocationWrappers = new List<ServiceableLocationWrapper>();
        //Iterating through all serviceable locations
        for (ServiceableLocationWrapper slw : lstServiceableLocations){
            lstServiceableLocationWrappers.add(slw);
        }
        paginator = new ObjectPaginator(RECORDS_ON_PAGE, this);
        paginator.setRecords(lstServiceableLocationWrappers);
    }
 
    /*********************************************************************************
    Method Name    : toggleSort
    Description    : toggles the sorting of query from asc<-->desc
    Return Type    : 
    Parameter      : 
    *********************************************************************************/
    public void toggleSort() {
        // simply toggle the direction
        sortDir = sortDir.equals(CPQ_Constants.ASC_SORT_DIR) ? CPQ_Constants.DESC_SORT_DIR : CPQ_Constants.ASC_SORT_DIR;
        // run the query again
        runQuery();
    }
 
    /*********************************************************************************
    Method Name    : runQuery
    Description    : runs the actual query
    Return Type    : 
    Parameter      : 
    *********************************************************************************/
    public void runQuery() {
        Set<Id> setCurrentlySelectedIds = new Set<Id>();
        //Iterating through all serviceable location wrappers
        for (ServiceableLocationWrapper serviceableLocation : lstServiceableLocations) {
            //check if the location is selected
            if (serviceableLocation.getIsSelected()){
                setCurrentlySelectedIds.add(serviceableLocation.getServiceableLocation().Id);
            }
        }
        lstServiceableLocations.clear();
        try {
            //Iterating through sorted serviceable locations
            for (ServiceableLocation__c serviceableLocation : Database.query(sSoql + ' order by ' + sortField + ' ' + sortDir + (sortDir == CPQ_Constants.ASC_SORT_DIR ? ' NULLS FIRST ' : ' NULLS LAST ') + ' limit 100')) {
                ServiceableLocationWrapper temp = new ServiceableLocationWrapper(serviceableLocation);
                //check if the set with selected serviceable location ids contains the current serviceable location id
                if (setCurrentlySelectedIds.contains(temp.getServiceableLocation().Id)) {
                    temp.setIsSelected(true);
                }
                lstServiceableLocations.add(temp);
            }
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }
 
    /*********************************************************************************
    Method Name    : runSearch
    Description    : runs the search with parameters passed via Javascript
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference runSearch() {
        String sPostalCode = Apexpages.currentPage().getParameters().get(POSTAL_CODE_PARAM);
        //sCninni = (!Utils.isEmpty(sCninni) && sCninni == CPQ_Constants.STRING_TRUE) ?' AND (CNINNI__C = \'NNI\' OR CNINNI__C =\'CNI\')':'';
        lstClliCodes.clear();
        //check if postal code is null or empty
        if (!Utils.isEmpty(sPostalCode)) {
            String sQryString = 'SELECT Id, Postal_Code__c, CLLI_Code__c FROM CLLIPostalCode__c where Postal_Code__c LIKE \'' + String.escapeSingleQuotes(sPostalCode)+ '%\' LIMIT 2500';
            try {    
                //Iterating through the postal codes
                for (CLLIPostalCode__c postalCode : Database.query(sQryString)) {
                    //check if postal codes list already contains the current code
                    if (!Utils.listContains(lstClliCodes, postalCode.CLLI_Code__c)) {
                        lstClliCodes.add(postalCode.CLLI_Code__c);
                    }
                }
            } catch(Exception ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            }
        }
        sSoql = buildServiceableLocationQuery(false);
        // run the query again
        runQuery();
        List<ServiceableLocationWrapper> lstAllServiceablelocationWrappers = new List<ServiceableLocationWrapper>();
        //iterating through serviceable locations
        for (ServiceableLocationWrapper slw : lstServiceableLocations) {
            lstAllServiceablelocationWrappers.add(slw);
        }
        //paginator = new ObjectPaginator(20, this);
        paginator.setRecords(lstAllServiceablelocationWrappers);
        return null;
    }
    
    /*********************************************************************************
    Method Name    : buildServiceableLocationQuery
    Description    : method used to build serviceable location query based on query string parameter
    Return Type    : String
    Parameter      : Boolean bIsForDelete: method is used for delete
    *********************************************************************************/
    private String buildServiceableLocationQuery(Boolean bIsForDelete) {
        String sQuery;
        String sServiceableLocationName = Apexpages.currentPage().getParameters().get(SERVICEABLE_LOCATION_NAME_PARAM);
        String sSuite = Apexpages.currentPage().getParameters().get(SUITE_PARAM);
        String sStreetNumber = Apexpages.currentPage().getParameters().get(STREET_NUMBER_PARAM);
        String sStreetName = Apexpages.currentPage().getParameters().get(STREET_NAME_PARAM);
        String sStreetType = Apexpages.currentPage().getParameters().get(STREET_TYPE_PARAM);
        String sStreetDirection = Apexpages.currentPage().getParameters().get(STREET_DIRECTION);
        String sCity = Apexpages.currentPage().getParameters().get(CITY_PARAM);
        String sPostalCode = Apexpages.currentPage().getParameters().get(POSTAL_CODE_PARAM);
        String sCninni = Apexpages.currentPage().getParameters().get(CNINNI_PARAM);
        String sAccessTypeGroup = Apexpages.currentPage().getParameters().get(ACCESS_TYPE_GROUPS_PARAM);
        String sNetworkAccessType = Apexpages.currentPage().getParameters().get(NETWORK_ACCESS_TYPE_PARAM);
        
        if (bIsForDelete) {
            sCninni = (!Utils.isEmpty(sCninni) && sCninni == CPQ_Constants.STRING_TRUE) ? ' AND (CNINNI__C = \'NNI\' OR CNINNI__C =\'CNI\')':' AND CNINNI__C = \'\'';
        } else {
            sCninni = (!Utils.isEmpty(sCninni) && sCninni == CPQ_Constants.STRING_TRUE) ?' AND (CNINNI__C = \'NNI\' OR CNINNI__C =\'CNI\')' : '';
        }
        
        sQuery = 'SELECT Name, Suite_Floor__c, Street_Number__c, Street_Name__c, Street_Type__c, Street_Direction__c, Province_Code__c,'
                + ' Postal_Code__c, CLLI_Code__c, City__c, Access_Type__c, Access_Type_Group__c, CNINNI__c, Network_Access_Type__c, Build_Date__c, Display_Name__c from serviceableLocation__c where Name!= null '
                + sCninni;
        //check if serviceable location name is empty
        if (!sServiceableLocationName.equals('')) {
            sQuery += ' and Name LIKE \'' + String.escapeSingleQuotes(sServiceableLocationName) + '%\'';
        }
        //check if suite is empty
        if (!sSuite.equals('')) {
            sQuery += ' and Suite_Floor__c LIKE \'' + String.escapeSingleQuotes(sSuite) + '%\'';
        }
        //check if street number is empty
        if (!sStreetNumber.equals('')) {
            sQuery += ' and Street_Number__c LIKE \'' + String.escapeSingleQuotes(sStreetNumber) + '%\'';
        }
        //check if street name is empty
        if (!sStreetName.equals('')) {
            sQuery += ' and Street_Name__c LIKE \'' + String.escapeSingleQuotes(sStreetName)+'%\'';
        }    
        //check if street type is empty
        if (!sStreetType.equals('')) {
            sQuery += ' and Street_Type__c LIKE \'' + String.escapeSingleQuotes(sStreetType) + '%\'';
        }
        //check if street direction is empty
        if (!sStreetDirection.equals('')) {
            sQuery += ' and Street_Direction__c LIKE \'' + String.escapeSingleQuotes(sStreetDirection) + '%\'';
        }
        //check if city is empty
        if (!sCity.equals('')) {
            sQuery += ' and City__c LIKE \'' + String.escapeSingleQuotes(sCity) + '%\'';
        }  
        //check if postal code is empty
        if (!Utils.isEmpty(sPostalCode)) {
            sQuery += ' and CLLI_Code__c IN :lstClliCodes';
        }
        //check if network access type is empty
        if (!Utils.isEmpty(sNetworkAccessType)) {
            sQuery += ' and Network_Access_Type__c = \'' + String.escapeSingleQuotes(sNetworkAccessType) + '\'';
        }
        //check if access type group is empty
        if (!sAccessTypeGroup.equals('')) {
            sQuery += ' and Access_Type_Group__c INCLUDES (\'' + String.escapeSingleQuotes(sAccessTypeGroup) + '\')';
        }    
        return sQuery;
    }
    
    /*********************************************************************************
    Method Name    : addNewSite
    Description    : add a new site
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference addNewSite() {
        // Old Sandbox
        // PageReference pr= new PageReference('/a0C/e?CF00NQ0000000leek=' + opp.Name +'&CF00NQ0000000leek_lkid='+opp.Id+'&retURL=%2F'+opp.Id+'&Name=Please%20Ignore%20Field');
        // Production
        SitesController_Setting__c csSitesController = SitesController_Setting__c.getInstance();
        String sOppName = EncodingUtil.urlEncode(oOpportunity.Name, CPQ_Constants.UTF8);
        PageReference pr = new PageReference('/' + csSitesController.Site__c + '/e?' + csSitesController.Site_Opportunity__c + '=' + sOppName +'&' + csSitesController.Site_Opportunity_lkid__c + 
                                            '=' + oOpportunity.Id + '&retURL=%2F' + oOpportunity.Id + '&Name=' + IGNORED_FIELD_TEXT);
        pr.setRedirect(true); 
        return pr;
  }
  
    /*********************************************************************************
    Method Name    : getAccessTypeGroups
    Description    : Obtains the Access Type Groups for the Select List and uses the order and default value specifies. The VF Page willl use jQuery to set the default value
    Return Type    : List<String>
    Parameter      : 
    *********************************************************************************/
    public List<String> getAccessTypeGroups() {
        List<String> lstAccessTypeGroupOptions = new List<String>();
        Integer i = 0;  
        Schema.DescribeFieldResult fieldResult = ServiceableLocation__c.Access_Type_Group__c.getDescribe();
        List<Schema.PicklistEntry> lstPicklistEntries = fieldResult.getPicklistValues();
        intDefaultAccessTypeGroup = lstPicklistEntries.size();    
        //iterating through all the Access Type Group picklist entries
        for(Schema.PicklistEntry picklistEntry : lstPicklistEntries) {
            //check if current picklist entry value is default
            if (picklistEntry.isDefaultValue()) {
                intDefaultAccessTypeGroup = i;
            }
            i++;    
            lstAccessTypeGroupOptions.add(picklistEntry.getLabel());
        }       
        return lstAccessTypeGroupOptions;
    }
    
    /*********************************************************************************
    Method Name    : getNetworkAccessTypes
    Description    : get all network access type options
    Return Type    : List<String>
    Parameter      : 
    *********************************************************************************/
    public List<String> getNetworkAccessTypes(){
        List<String> lstNetworkAccessTypeOptions = new List<String>();
        Schema.DescribeFieldResult fieldInfo = Schema.SObjectType.ServiceableLocation__c.fields.Network_Access_Type__c;
        List<Schema.PicklistEntry> lstPicklistValues = fieldInfo.getPicklistValues();
        //iterating through all network access type picklist entries values
        for (Schema.PicklistEntry picklistValue : lstPicklistValues) {
            lstNetworkAccessTypeOptions.add(picklistValue.getLabel());
        }
        List<String> lstOptions = new List<String>();
        //iterating through all network type options
        for(String network : lstNetworkAccessTypeOptions){
            lstOptions.add(network);
        }
        return lstOptions;
    }
 
    /*********************************************************************************
    Method Name    : addSites
    Description    : method used to add a site
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference addSites(){
        String sSuiteMap = Apexpages.currentPage().getParameters().get(SUITE_MAP);
        Map<String, String> mapSuiteId = new Map<String, String>();
        //check if suite map is empty
        if (!Utils.isEmpty(sSuiteMap)) {
            String[] suiteIds = sSuiteMap.split(SUITE_MAP_SPLITTER);
            //iterating through suites ids
            for (String suiteId : suiteIds){
                //check if suite id splitted by '@' contains 3 elements
                if (suiteId.split(SUITE_ID_SPLITTER).size() == 3){
                    mapSuiteId.put(suiteId.split(SUITE_ID_SPLITTER)[0], suiteId.split(SUITE_ID_SPLITTER)[1] + SUITE_ID_SPLITTER + suiteId.split(SUITE_ID_SPLITTER)[2]);
                }
            }
        }
        Boolean bError = false;
        String sDupList = '';
        Integer intCounter = 0;
        //iterating through all serviceable location wrappers
        for (ServiceableLocationWrapper serviceableLocation : lstServiceableLocations){
            //check if the current serviceable location is selected
            if (serviceableLocation.getIsSelected()) {
                Site__c oSite = new Site__c(ServiceableLocation__c = serviceableLocation.getServiceableLocation().Id,
                                            Name = serviceableLocation.getServiceableLocation().Name,                                              
                                            Suite_Floor__c = serviceableLocation.getServiceableLocation().Suite_Floor__c,
                                            Street_Number__c = serviceableLocation.getServiceableLocation().Street_Number__c,
                                            Street_Name__c = serviceableLocation.getServiceableLocation().Street_Name__c,
                                            Street_Type__c = serviceableLocation.getServiceableLocation().Street_Type__c,
                                            Province_Code__c = serviceableLocation.getServiceableLocation().Province_Code__c,
                                            Postal_Code__c = serviceableLocation.getServiceableLocation().Postal_Code__c,
                                            CLLI_SWC__c = serviceableLocation.getServiceableLocation().CLLI_Code__c,
                                            Street_Direction__c = serviceableLocation.getServiceableLocation().Street_Direction__c,
                                            City__c = serviceableLocation.getServiceableLocation().City__c,
                                            Access_Type__c = serviceableLocation.getServiceableLocation().Access_Type__c,
                                            Access_Type_Group__c = serviceableLocation.getServiceableLocation().Access_Type_Group__c,
                                            Opportunity__c = oOpportunity.Id,
                                            Account__c = oOpportunity.AccountId);
                System.debug('YYYYY: ' + Utils.get15CharId(serviceableLocation.getServiceableLocation().Id));                                                
                System.debug('YYYYY: ' + mapSuiteId);
                //check if the value appropriate for the current serviceable location id exists
                if (mapSuiteId.get(Utils.get15CharId(serviceableLocation.getServiceableLocation().Id)) != null){
                    String sTempSuite = mapSuiteId.get(Utils.get15CharId(serviceableLocation.getServiceableLocation().Id));
                    //check if the temporary suite splitted by '@' contains 2 elements
                    if (sTempSuite.split(SUITE_ID_SPLITTER).size() == 2) {
                        oSite.Suite_Floor__c = sTempSuite.split(SUITE_ID_SPLITTER)[0];
                        oSite.Suite_N_A__c = sTempSuite.split(SUITE_ID_SPLITTER)[1] == CPQ_Constants.STRING_TRUE;
                    }
                }
                try {
                    //check if cninni of the current serviceable location is empty
                    if (!Utils.isEmpty(serviceableLocation.getServiceableLocation().CNINNI__c)) {
                            oSite.Type__c = serviceableLocation.getServiceableLocation().CNINNI__c;
                    }  else {
                        oSite.Type__c = SERVICEABLE_LOCATION;
                    }
                    insert oSite;
                    serviceableLocation.setIsSelected(false);
                } catch(DMLException ex) {
                     // dupList += (counter++>0?', ':'') + serviceableLocation.getServiceableLocation().Name;
                     serviceableLocation.setIsSelected(false);
                     //     error = true;
                }
            }       
        }
        //   if (error){
        //        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'You have chosen a site that is already a prospect site for this Opportunity. Site' + (counter>1?'s ':' ') + dupList + (counter>1?' were':' was') +' not readded.'));
        //   }
        return null;
    }
   
    /*********************************************************************************
    Method Name    : returnToOpportunity
    Description    : method used to return to opportuntiy
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference returnToOpportunity(){
        PageReference oppPage = new ApexPages.StandardController(oopportunity).view();
        oppPage.setRedirect(true);
        return oppPage;
    }
    
    /*********************************************************************************
    Method Name    : handlePageChange
    Description    : populate lstServiceableLocations
    Return Type    : void
    Parameter      : 
    *********************************************************************************/
    global void handlePageChange(List<Object> lstNewPages){
        lstServiceableLocations.clear();
        //check if the list received as paramter is null
        if (lstNewPages != null){
            //iterating through all objects received as parameter
            for (Object serviceableLocation : lstNewPages){
                lstServiceableLocations.add((ServiceableLocationWrapper)serviceableLocation);
            }
        }
    }
    
    /*********************************************************************************
    Method Name    : handlePageChange
    Description    : method that checks for existing quotes
    Return Type    : void
    Parameter      : 
    *********************************************************************************/
    public PageReference checkExistingQuote() {
        String sSiteId = Apexpages.currentPage().getParameters().get('siteId');
        String sAccountName = Apexpages.currentPage().getParameters().get('accountName');    
        //check if site id is empty
        if (Utils.isEmpty(sSiteId)) {
            bDisplayDuplicateMessage = false;
            sDuplicateMessage = '';
            return null;
        }
        List<Quote_Site__c> lstQuoteSites = new List<Quote_Site__c>();
        try {
            lstQuoteSites = [SELECT quote__c, site__c, Site__r.ServiceableLocation__r.Access_Type_Group__c, Site__r.Account__r.Name FROM Quote_Site__c 
                             WHERE site__r.ServiceableLocation__c = :sSiteId AND Quote__r.ExpirationDate >= today AND Site__r.Opportunity__r.OwnerId != :UserInfo.getUserId()];
        } catch(Exception ex) {
            bDisplayDuplicateMessage = true;
            sDuplicateMessage = SITE_DUPLICATION_MESSAGE;
        }
        //check if quotes sites list has elements
        if (lstQuoteSites != null && lstQuoteSites.size() > 0) {
            Boolean bHasDup = false;
            Set<String> setMatchingAccounts = new Set<String>();
            //iterating through the sites
            for (Quote_Site__c qSite : lstQuoteSites) {
                //check if it's not a test
                if (!Test.isRunningTest()) {
                    //check access type group
                    if ((!Utils.isEmpty(qSite.site__r.ServiceableLocation__r.Access_Type_Group__c) && !qSite.Site__r.ServiceableLocation__r.Access_Type_Group__c.equalsIgnoreCase(FIBRE)) 
                         || (qSite.Site__r.ServiceableLocation__r.Access_Type_Group__c.toLowerCase().contains(FIBRE) && qSite.Site__r.Account__r.Name.toLowerCase().contains(sAccountName.toLowerCase()))) {
                        bDisplayDuplicateMessage = true;
                        bHasDup = true;
                        setMatchingAccounts.add(qSite.Site__r.Account__r.Name);
                    }
                }
            }
            //if duplicates exist
            if (!bHasDup){
                bDisplayDuplicateMessage = true;
                sDuplicateMessage = SITE_DUPLICATION_MESSAGE;
            } else {
                bDisplayDuplicateMessage = true;
                sDuplicateMessage = SITE_DUPLICATION_MESSAGE_SITE_QUOTED;
                sDuplicateMessage += MATCHIMG_ACCOUNTS_TEXT;
                //iterating through matching accounts set
                for (String acct : setMatchingAccounts) {
                    sDuplicateMessage += acct + '\n';
                }
                sDuplicateMessage = sDuplicateMessage.replace('\n', '<br/>');
            }
        } else {
            bDisplayDuplicateMessage = true;
            sDuplicateMessage = SITE_DUPLICATION_MESSAGE;
        }
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, sDuplicateMessage);
        ApexPages.addMessage(myMsg);
        return null;
    }
    
    /*********************************************************************************
    Method Name    : deleteServiceableLocations
    Description    : method used to delete serviceable locations
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference deleteServiceableLocations() {
        String sSiteIds = Apexpages.currentPage().getParameters().get('SiteIds');
        String sPostalCode = Apexpages.currentPage().getParameters().get('postalCode');
        List<String> lstIds = sSiteIds.split(SUITE_MAP_SPLITTER);
        try {
            List<ServiceableLocation__c> lstSitesToDelete = [SELECT Id, Name FROM ServiceableLocation__c WHERE id IN :lstIds];
            delete lstSitesToDelete;
            //runSearch();
        } catch(DMLException ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
        //sCninni = (!Utils.isEmpty(sCninni) && sCninni == CPQ_Constants.STRING_TRUE) ? '(CNINNI__C = \'NNI\' OR CNINNI__C =\'CNI\')':'CNINNI__C = \'\'';
        lstClliCodes.clear();
        //check if postal code is empty
        if (!Utils.isEmpty(sPostalCode)) {
            String sQryString = 'SELECT Id, Postal_Code__c, CLLI_Code__c FROM CLLIPostalCode__c where Postal_Code__c LIKE \'' + String.escapeSingleQuotes(sPostalCode)+ '%\' LIMIT 2500';
            try {    
                //iterating through all postal codes
                for (CLLIPostalCode__c postalCode : Database.query(sQryString)){
                    //check if codes list already contains current postal code
                    if (!Utils.listContains(lstClliCodes, postalCode.CLLI_Code__c))
                        lstClliCodes.add(postalCode.CLLI_Code__c);
                }
            } catch(Exception ex){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            }
        }
        sSoql = buildServiceableLocationQuery(true);
        // run the query again
         runQuery();
        List<ServiceableLocationWrapper> lstAllServiceableLocationWrappers = new List<ServiceableLocationWrapper>();
        //iterating through all serviceable location wrappers
        for (ServiceableLocationWrapper slw : lstServiceableLocations){
            lstAllServiceableLocationWrappers.add(slw);
        }
        //paginator = new ObjectPaginator(20, this);
        paginator.setRecords(lstAllServiceableLocationWrappers);
        return null;
    }   
    
    /*********************************************************************************
    Method Name    : editServiceableLocation
    Description    : method used to edit serviceable locations
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
     public PageReference editServiceableLocation(){
        String sSiteId = Apexpages.currentPage().getParameters().get('SiteId');
        String sIsNew = Apexpages.currentPage().getParameters().get('isNewSite');  
        SitesController_Setting__c cs = SitesController_Setting__c.getInstance(); 
        //check if site is not new
        if (Utils.isEmpty(sIsNew) || (sIsNew.equals(CPQ_Constants.STRING_FALSE) && Utils.isEmpty(sSiteId))){
            return null;
        }
        String sUrlString = '';
        //check if site is new
        if (sIsNew.equals(CPQ_Constants.STRING_TRUE)) {
            sUrlString = '/' + cs.Quote_Request__c + '/e?Name=' + IGNORED_FIELD_TEXT; //Please%20Ignore%20Field';//&retURL=%2Fapex%2FaddSite%3Fedit=1';
        } else {
            sUrlString = '/'+ sSiteId + '/e?retURL=' + sSiteId;//%2Fapex%2FaddSite%3Fedit=1';
        }       
        PageReference pr = new PageReference(sUrlString);
        pr.setRedirect(true); 
        return pr;
     }
}