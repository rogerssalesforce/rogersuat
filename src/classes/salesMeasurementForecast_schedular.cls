/*Class Name :salesQuotaForecast_schedular.
 *Description : .
 *Created By : Rajiv Gangra.
 *Created Date :30/09/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
global class salesMeasurementForecast_schedular implements schedulable{

    global void execute(SchedulableContext ctx) {
        
        integer msCheckPoint= Integer.ValueOf(Static_Data_Utilities__c.getInstance('Opportunity Update Checking Period').Value__c);
        dateTime dTmsCheckPoint =system.now().addDays(-msCheckPoint);
        //***Step 5.   Query the Sales Measurement object to get the all records belonging to the opportunities that are updated between (Today-7days) and Today, inclusively.
        salesQuotaForecast_batch salesMeasurementObj = new salesQuotaForecast_batch();
        if(!Test.isRunningTest()){
        salesMeasurementObj.Query = 'select id,LastModifiedDate,Quota_Family__c,Quantity__c,Revenue__c,OwnerMonthFamily__c,Deployment_Month__c,OwnerID,Deployment_Date__c,Opportunity__r.StageName,Opportunity__r.LastModifiedDate,Opportunity__c,Weighted_Revenue__c,Weighted_Quantity__c,Opportunity_Type__c FROM Sales_Measurement__c where Opportunity__r.LastModifiedDate >=:dTmsCheckPoint AND Opportunity_Type__c !='+'\'Renewal\'';
        }else{
        salesMeasurementObj.Query = 'select id,LastModifiedDate,Quota_Family__c,Quantity__c,Revenue__c,OwnerMonthFamily__c,Deployment_Month__c,OwnerID,Deployment_Date__c,Opportunity__r.StageName,Opportunity__r.LastModifiedDate,Opportunity__c,Weighted_Revenue__c,Weighted_Quantity__c FROM Sales_Measurement__c where Opportunity__r.LastModifiedDate >=:dTmsCheckPoint LIMIT 1';
        }
        ID salesMeasurementBatchProcessId = Database.executeBatch(salesMeasurementObj,100);

        
    }

}