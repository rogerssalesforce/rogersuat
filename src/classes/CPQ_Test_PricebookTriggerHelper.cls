/*********************************************************************************
Class Name      : CPQ_Test_PricebookTriggerHelper 
Description     : This class a test class for CPQ_PricebookTriggerHelper. 
Created By      : Alina Balan
Created Date    : 04-November-2015
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Alina Balan              04-November-2015          Original version
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest (seeAllData=False) 
public with sharing class CPQ_Test_PricebookTriggerHelper {
    private static Pricebook2 oPricebookTemp;
    private static Pricebook2 oPricebook;
    private static final String CPQ_SYSTEM_ADMIN = 'System Administrator';
    private static final String CPQ_ADMIN_USERNAME = 'standarduser_test123@testorg.com';
    private static final String CPQ_PRICEBOOK_NAME = 'CPQ Temp PriceBook';
    
    /*********************************************************************************
    Method Name    : testPreventDummyPricebookDeletion
    Description    : Method used to test preventDummyPricebookDeletion
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    private static testmethod void testPreventDummyPricebookDeletion(){
        createTestData();
        User testAdminUser = CPQ_Test_Setup.newUser(CPQ_SYSTEM_ADMIN, CPQ_ADMIN_USERNAME);
        insert testAdminUser;
        test.startTest();
        system.runAs(testAdminUser) {
            oPricebookTemp = [SELECT Id FROM pricebook2 WHERE Name =: CPQ_PRICEBOOK_NAME];
            oPricebook = [SELECT Id FROM pricebook2 WHERE Name =: CPQ_PRICEBOOK_NAME + '1'];
            system.assertNotEquals(oPricebookTemp, null);
            system.assertNotEquals(oPricebook, null);
            try {
                delete oPricebook;
                delete oPricebookTemp;
            } catch(Exception e) {}
            List<Pricebook2> lstPricebooksTemp = [SELECT Id FROM pricebook2 WHERE Name =: CPQ_PRICEBOOK_NAME];
            List<Pricebook2> lstPricebooks = [SELECT Id FROM pricebook2 WHERE Name =: CPQ_PRICEBOOK_NAME + '1'];
            system.assertEquals(lstPricebooksTemp.size(), 1);
            system.assertEquals(lstPricebooks.size(), 0);
        }
        test.stopTest();
    }
    
    /*********************************************************************************
    Method Name    : createTestData
    Description    : Method used to create data for test class
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    private static void createTestData() {
        oPricebookTemp = CPQ_Test_Setup.newPriceBook(CPQ_PRICEBOOK_NAME);
        insert oPricebookTemp;
        oPricebook = CPQ_Test_Setup.newPriceBook(CPQ_PRICEBOOK_NAME + '1');
        insert oPricebook;
        CPQ_Test_Setup.createSystemSettingRecord();
    }
}