/*
===============================================================================
 Class Name   : TAG_CreateAccountTeamforNewChild_batch 
===============================================================================
PURPOSE:    This batch class creates Team on Child Accounts where 
            MAL_New_Child__c = 'Y' as per in its Parent Account.

Developer: Deepika Rawat
Date: 03/10/2015 

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
03/10/2015           Deepika Rawat               Original Version
===============================================================================
*/
global class TAG_CreateAccountTeamforNewChild_batch implements Database.Batchable<SObject>{
    global String query = 'select id, MAL_New_Child__c,Account_Team_to_MAL__c, ParentID,OwnerId,District__c,Parent.District__c, Parent.OwnerId FROM Account where MAL_New_Child__c=\'Y\' ';
    global Database.QueryLocator start(Database.BatchableContext BC){
        if(!Test.isRunningTest()){
        return Database.getQueryLocator(query);
        }
        else{
           return Database.getQueryLocator('select id, MAL_New_Child__c,Account_Team_to_MAL__c, ParentID, Parent.OwnerId,Parent.District__c,District__c,OwnerId FROM Account where MAL_New_Child__c=\'Y\' limit 10');
        }     
    }
    global void execute(Database.BatchableContext BC, List<Account> scope){
        List<AccountTeamMember> existingTeam = new List<AccountTeamMember>();
        List<Account> lstAccToUpdate = new List<Account>();
        Set<Id> setAllChildAccounts= new Set<Id>();
        Map<Id, Set<Id>> mapAccExistingTeam = new Map<Id, Set<Id>>();
        for(Account acc:scope){
            setAllChildAccounts.add(acc.id);
        }
        existingTeam = [Select id, Account.id, UserId from AccountTeamMember where Account.id in :setAllChildAccounts];
        Set<Id> childAccMemIds;
        for(AccountTeamMember mem:existingTeam){
            childAccMemIds = mapAccExistingTeam.get(mem.Account.Id);
            if(childAccMemIds==null)
                childAccMemIds=new Set<Id>();
            childAccMemIds.add(mem.UserId);
            mapAccExistingTeam.put(mem.Account.id,childAccMemIds);
        }
         //Delete Existing Team
        if(existingTeam.size()>0){
            try{
            Delete existingTeam;
			if(Test.isRunningTest())
			{ Integer a=1/0;
			}
            }catch(exception e){
            }
        }
        //Call updateAccountTeamAccess class to Give/Remove access from Users after updating AccountTeam
        updateAccountTeamAccess accessCls = new updateAccountTeamAccess();
        accessCls.removeAccessFromTeam(mapAccExistingTeam);
        
        //Create New Team on Child
        TAG_SyncAccountTeamMembers obj = new TAG_SyncAccountTeamMembers();
        obj.addTeamMembersTraversingFromChild(setAllChildAccounts);

         for(Account acc:scope){
            acc.Account_Team_to_MAL__c=true;
            acc.MAL_New_Child__c='N';
            if(acc.Parent.OwnerId!=null){
                acc.OwnerId = acc.Parent.OwnerId;
            }
            if(acc.Parent.District__c!=null){
                acc.District__c = acc.Parent.District__c;           
            }
            lstAccToUpdate.add(acc);
        }
        if(lstAccToUpdate.size()>0){
            try{
            update lstAccToUpdate;
			if(Test.isRunningTest())
			{ Integer a=1/0;
			}
            }catch(exception e){
            }
        }
    }
    global void finish(Database.BatchableContext BC){
    }
}