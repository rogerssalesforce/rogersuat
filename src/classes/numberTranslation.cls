public class numberTranslation {
public decimal decNumber {get;set;}
public string strLanguage {get;set;}
public date dtValue {get;set;}


public string value;
public string dtDate;


public string getValue()
{
    if (decNumber != null) {
        if(strLanguage == 'fr') 
        {
            value = formatDouble(decNumber.setscale(2),true,'fr');
        }
        else
        {
            value = formatDouble(decNumber.setscale(2),true,'en');
        }
        return value;
    } else return null;
}

public string getdtDate()
{
    if (dtValue != null) {
        Datetime dtDateTime = dtValue;
        string strDate = '';
        if (strLanguage == 'fr')
        {
            strDate = dtDateTime.format('dd MMM yyyy');
            strDate = strDate.replace('Jan', 'janvier');
            strDate = strDate.replace('Feb', 'février');
            strDate = strDate.replace('Mar', 'mars');
            strDate = strDate.replace('Apr', 'avril');
            strDate = strDate.replace('May', 'mai');
            strDate = strDate.replace('Jun', 'juin');
            strDate = strDate.replace('Jul', 'juillet');
            strDate = strDate.replace('Aug', 'août');
            strDate = strDate.replace('Sep', ' septembre');
            strDate = strDate.replace('Oct', 'octobre');
            strDate = strDate.replace('Nov', 'novembre');
            strDate = strDate.replace('Dec', 'décembre');
            
        }
        else
        {
            strDate = dtDateTime.format('MMM dd, yyyy');
        }
        return strDate;
    } else return null;
}

public String formatDouble(Decimal myNumber,Boolean isCurrency,string lang){
	return formatDouble(myNumber,isCurrency,lang, 2);
         
}

public String formatDouble(Decimal myNumber,Boolean isCurrency,string lang, Integer decimalPlaces){

        String formattedString = '';
        Decimal myNumber1 = myNumber.setScale(decimalPlaces);
        String myNumberAsString = String.valueOf(myNumber1);
        //System.debug('TEST ' + myNumberAsString);
        
        if(myNumber>999999999){
            formattedString += myNumberAsString.substring(0,myNumberAsString.length()-12)+ ',' + myNumberAsString.substring(myNumberAsString.length()-12,myNumberAsString.length()-9) + ',' + myNumberAsString.substring(myNumberAsString.length()-9,myNumberAsString.length()-6) + ',' + myNumberAsString.substring(myNumberAsString.length()-6,myNumberAsString.length());
        } else if(myNumber>999999){
            formattedString += myNumberAsString.substring(0,myNumberAsString.length()-9)+ ',' + myNumberAsString.substring(myNumberAsString.length()-9,myNumberAsString.length()-6) + ',' + myNumberAsString.substring(myNumberAsString.length()-6,myNumberAsString.length());
        } else if(myNumber>999){
            formattedString += myNumberAsString.substring(0,myNumberAsString.length()-6)+ ',' + myNumberAsString.substring(myNumberAsString.length()-6,myNumberAsString.length());
        } else {
            formattedString += myNumberAsString;
        }
        
        if (isCurrency){
            if (lang == 'fr')
            {
                formattedString = formattedString.replace(',',' ');
                formattedString = formattedString.replace('.',',');
                formattedString = formattedString + ' $';
            }
            else
            {
                formattedString = '$ ' + formattedString;   
            }
            
        }
    return formattedString;
    }  
}