/*********************************************************************************
Class Name      : CPQ_ProposalReadyForReviewController
Description     : This class is called on click of "Ready for Review" button on Quote
Created By      : Deepika Rawat
Created Date    : 15-Oct-2015
Modification Log:
---------------------------------------------------------------------------------------
Developer                   Date                   Description
--------------------------------------------------------------------------------------- 
Deepika Rawat            15-Oct-2015            Original Version
*********************************************************************************/
public with sharing class CPQ_ProposalReadyForReviewController {
    public String sProposalId;
    public List<Apttus_Proposal__Proposal__c> lstProposal;
    public List<Apttus_Config2__LineItem__c> lstLineItems;
    public List<Apttus_Config2__ProductConfiguration__c> lstProductConfig ;

    /*********************************************************************************
    Method Name    : CPQ_ProposalReadyForReviewController
    Description    : Constructor method
    Return Type    : 
    Parameter      : ApexPages.StandardController
    *********************************************************************************/
    public CPQ_ProposalReadyForReviewController(ApexPages.StandardController controller) {
        sProposalId= System.currentPagereference().getParameters().get('Id');
    }

    /*********************************************************************************
    Method Name    : updatequote
    Description    : This method updates Quote on click of "Ready for Review"
    Return Type    : pagereference
    Parameter      :  None
    *********************************************************************************/
    public pagereference updatequote(){
        //Proceed if Proposal ID is present
        if(sProposalId!=null && sProposalId!=''){
            Boolean bCustomProductPresent = false;
            Boolean bWirelessMobileProdPresent = false;
            Boolean bIOTApplicationsPresent = false;
            Boolean bUnifiedCommPresent = false;
            Boolean bWirelinePresent =false;
            Boolean bDataCentres = false;
            CPQ_Settings__c cpqSetting = CPQ_Settings__c.getOrgDefaults();
            RecordType lockedRecType = [Select Id,DeveloperName From RecordType  Where SobjectType = 'Apttus_Proposal__Proposal__c' and DeveloperName =: cpqSetting.Proposal_Soln_Under_Review_RecordType__c]; 
            // Get list of Product Configuration with Status = "Saved"
            lstProductConfig = [Select id,  Apttus_QPConfig__Proposald__c, Apttus_Config2__Status__c from  Apttus_Config2__ProductConfiguration__c where Apttus_QPConfig__Proposald__c =:sProposalId and Apttus_Config2__Status__c =:'Saved' ];
            // Get Proposal ID
            lstProposal= [select id, RecordTypeId , Apttus_Proposal__Approval_Stage__c from Apttus_Proposal__Proposal__c where id=:sProposalId];
            // Get Proposal ID
            //Update Quote record if Shopping cart has been saved by AE; else display an error message
            if(lstProposal!=null && lstProposal.size()>0 && lstProductConfig!=null && lstProductConfig.size()>0 ){
                lstLineItems = [select IsCustomProduct__c, Rogers_ProductFamily__c from Apttus_Config2__LineItem__c where Apttus_Config2__ConfigurationId__c =:lstProductConfig[0].id];
                for(Apttus_Config2__LineItem__c item : lstLineItems){
                    // Check if any Custom Product is present
                    if(item.IsCustomProduct__c == true){
                        bCustomProductPresent = true;
                    }
                    if(item.Rogers_ProductFamily__c!=null && item.Rogers_ProductFamily__c.containsIgnoreCase(cpqSetting.Wireless_Mobile__c)){
                        bWirelessMobileProdPresent=true;
                    }
                     if(item.Rogers_ProductFamily__c!=null && item.Rogers_ProductFamily__c.containsIgnoreCase(cpqSetting.IOTApplications__c)){
                        bIOTApplicationsPresent=true;
                    }
                     if(item.Rogers_ProductFamily__c!=null && item.Rogers_ProductFamily__c.containsIgnoreCase(cpqSetting.Unified_Comm_Collaboration__c)){
                    bUnifiedCommPresent = true;
                    }
                    if(item.Rogers_ProductFamily__c!=null && item.Rogers_ProductFamily__c.containsIgnoreCase(cpqSetting.Wireline__c)){
                        bWirelinePresent=true;
                    }
                     if(item.Rogers_ProductFamily__c!=null && item.Rogers_ProductFamily__c.containsIgnoreCase(cpqSetting.Data_Centres__c)){
                        bDataCentres=true;
                    }
                }
                //Set Apttus_Proposal__Approval_Stage__c based on if custom products are added and product family
                if(bCustomProductPresent==false &&  bWirelessMobileProdPresent==true && (bWirelinePresent ==false && bUnifiedCommPresent == false && bDataCentres ==false && bIOTApplicationsPresent==false ))
                    lstProposal[0].Apttus_Proposal__Approval_Stage__c = 'Approval Required';
                else
                    lstProposal[0].Apttus_Proposal__Approval_Stage__c = 'Solution Under Review';
                //Set SE_Status__c based on if custom products are added and product family
                if(bWirelessMobileProdPresent==true && (bWirelinePresent ==false && bUnifiedCommPresent == false && bDataCentres ==false && bIOTApplicationsPresent==false))
                    lstProposal[0].SE_Status__c = 'Not Required';
                else
                    lstProposal[0].SE_Status__c = 'Pending Review';
                //Set Deal_Desk_Status__c based on if custom products are added and product family
                if(bCustomProductPresent==true && (bWirelessMobileProdPresent==true ))
                    lstProposal[0].Deal_Desk_Status__c = 'Pending Review';
                else
                    lstProposal[0].Deal_Desk_Status__c = 'Not Required';
                lstProposal[0].RecordTypeId = lockedRecType.id;
                update lstProposal[0];
                PageReference refreshPage = new PageReference('/' + lstProposal[0].Id);
                //Reprice Cart
                Apttus_CpqApi.CPQ.UpdatePriceRequestDO objUpdatePriceRequestDO = new Apttus_CpqApi.CPQ.UpdatePriceRequestDO();
                objUpdatePriceRequestDO.CartId = lstProductConfig[0].id; 
                Apttus_CpqApi.CPQ.UpdatePriceResponseDO result = Apttus_CpqApi.CPQWebService.updatePriceForCart(objUpdatePriceRequestDO);
                //Finalize the Cart
                Apttus_CPQApi.CPQ.FinalizeCartRequestDO request = new Apttus_CPQApi.CPQ.FinalizeCartRequestDO();
                request.CartId = lstProductConfig[0].id; 
                Apttus_CPQApi.CPQ.FinalizeCartResponseDO resp = Apttus_CPQApi.CPQWebService.finalizeCart(request); 
                return refreshPage ; 
            }
            else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CPQ_ReadyForReviewErrorMsg));
                return null;
            }
        }
        return null;
    }
    /*********************************************************************************
    Method Name    : backToQuote
    Description    : Returns back to Quote
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference backToQuote(){       
        PageReference refreshPage = new PageReference('/' + lstProposal[0].Id);
        return refreshPage ; 
    }
}