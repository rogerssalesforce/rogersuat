/*********************************************************************************
Class Name      : CPQ_SiteTriggerHelper
Description     : This class is used in Site__c trigger
Created By      : Alina Balan
Created Date    : 07-December-2015
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Alina Balan          07-December-2015            US-0088 Origianl version
*********************************************************************************/

public with sharing class CPQ_SiteTriggerHelper {
    
    /*********************************************************************************
    Method Name    : createAccountLocations
    Description    : Method used to create account locations list based on inserted sites list
    Return Type    : 
    Parameter      : List<Site__c> lstSites
    *********************************************************************************/
    public static void createAccountLocations(List<Site__c> lstSites) {
        List<Apttus_Config2__AccountLocation__c> lstAccountLocations = new List<Apttus_Config2__AccountLocation__c>();
        //iterating through all inserted sites list
        for (Site__c site : lstSites) {
            Apttus_Config2__AccountLocation__c oAccountLocation = createAccountLocation(site);
            if (oAccountLocation != null) {
                lstAccountLocations.add(oAccountLocation);
            }
        }
        //check if account locations list contains any elements
        if (lstAccountLocations.size() > 0) {
            insert lstAccountLocations;
        }
    }
    
    /*********************************************************************************
    Method Name    : updateAccountLocations
    Description    : Method used to update account locations list based on updated sites
    Return Type    : 
    Parameter      : Map<Id, Site__c> mapSites
    *********************************************************************************/
    public static void updateAccountLocations(Map<Id, Site__c> mapSites) {
        List<Apttus_Config2__AccountLocation__c> lstAccountLocations = [SELECT Id, Prospect_Site__c FROM Apttus_Config2__AccountLocation__c WHERE Prospect_Site__c IN: mapSites.keySet()];
        List<Apttus_Config2__AccountLocation__c> lstAccountLocationsToUpdate = new List<Apttus_Config2__AccountLocation__c>();
        Map<Id, List<Apttus_Config2__AccountLocation__c>> mapSiteAccountLocations = new Map<Id, List<Apttus_Config2__AccountLocation__c>>();
        //iterating through all sites map values
        for (Site__c site : mapSites.values()) {
            //iterating through all account locations list
            List<Apttus_Config2__AccountLocation__c> lstAccountLocationTemp;
            for (Apttus_Config2__AccountLocation__c accountLocation : lstAccountLocations ) {
                //check if site id is the same with account location site id
                if (site.Id != accountLocation.Prospect_Site__c) continue;
                //check if site account locations map contains the current site id
                if (mapSiteAccountLocations.get(site.Id) == null) {
                    lstAccountLocationTemp = new List<Apttus_Config2__AccountLocation__c>();
                } else {
                    lstAccountLocationTemp = mapSiteAccountLocations.get(site.Id);
                }
                lstAccountLocationTemp.add(accountLocation);
                mapSiteAccountLocations.put(site.Id, lstAccountLocationTemp );
            }
        }
        //iterating through all site account locations map keysets
        for (Id siteId : mapSiteAccountLocations.keySet()) {
            Site__c oSite = mapSites.get(siteId);
            List<Apttus_Config2__AccountLocation__c> lstAccountLocationAfterChanges = updateAccountLocationsForSite(oSite, mapSiteAccountLocations.get(siteId));
            lstAccountLocationsToUpdate.addAll(lstAccountLocationAfterChanges);
        }
        //check if account locations list contains any elements
        if (lstAccountLocationsToUpdate.size() > 0) {
            update lstAccountLocationsToUpdate;
        }
    }
    
    /*********************************************************************************
    Method Name    : deleteAccountLocations
    Description    : Method used to delete account locations list based on deleted sites
    Return Type    : 
    Parameter      : Map<Id, Site__c> mapSites
    *********************************************************************************/
    public static void deleteAccountLocations(Map<Id, Site__c> mapSites) {
        List<Apttus_Config2__AccountLocation__c> lstAccountLocations = [SELECT Id FROM Apttus_Config2__AccountLocation__c WHERE Prospect_Site__c IN: mapSites.keySet()];
        //check if account locations list contains any elements
        if (lstAccountLocations.size() > 0) {
            delete lstAccountLocations;
        }
    }
    
    /*********************************************************************************
    Method Name    : createAccountLocation
    Description    : Method used to create an account location based on site object
    Return Type    : Apttus_Config2__AccountLocation__c
    Parameter      : Site__c oSite
    *********************************************************************************/
    private static Apttus_Config2__AccountLocation__c createAccountLocation(Site__c oSite) {
        //check if account field on site object is null
        if (oSite.Account__c == null) return null;
        Apttus_Config2__AccountLocation__c oAccountLocation = new Apttus_Config2__AccountLocation__c();
        oAccountLocation = setAccountLocationDetails(oSite, oAccountLocation);
        return oAccountLocation;
    }
    
    /*********************************************************************************
    Method Name    : updateAccountLocationsForSite
    Description    : Method used to update account locations list based on site object
    Return Type    : List<Apttus_Config2__AccountLocation__c>
    Parameter      : Site__c oSite, List<Apttus_Config2__AccountLocation__c> lstAccountLocations
    *********************************************************************************/
    private static List<Apttus_Config2__AccountLocation__c> updateAccountLocationsForSite(Site__c oSite, List<Apttus_Config2__AccountLocation__c> lstAccountLocations) {
        List<Apttus_Config2__AccountLocation__c> lstAccountLocationsToUpdate = new List<Apttus_Config2__AccountLocation__c>();
        //iterating through all account locations list
        for(Apttus_Config2__AccountLocation__c accountLocation : lstAccountLocations) {
            //check if the account location site id equals with the site id given as parameter
            if (accountLocation.Prospect_Site__c != oSite.Id || oSite.Account__c == null) continue;
            accountLocation = setAccountLocationDetails(oSite, accountLocation);
            lstAccountLocationsToUpdate.add(accountLocation);
        }
        return lstAccountLocationsToUpdate;
    }
    
    /*********************************************************************************
    Method Name    : setAccountLocationDetails
    Description    : Method used to set account locations details
    Return Type    : Apttus_Config2__AccountLocation__c
    Parameter      : Site__c oSite, Apttus_Config2__AccountLocation__c oAccountLocation
    *********************************************************************************/
    private static Apttus_Config2__AccountLocation__c setAccountLocationDetails(Site__c oSite, Apttus_Config2__AccountLocation__c oAccountLocation) {
        oAccountLocation.Prospect_Site__c = oSite.Id;
        oAccountLocation.Opportunity__c = oSite.Opportunity__c;
        oAccountLocation.Apttus_Config2__AccountId__c = oSite.Account__c;
        oAccountLocation.Access_Type__c = oSite.Access_Type__c;
        oAccountLocation.Access_Type_Group__c = oSite.Access_Type_Group__c;
        oAccountLocation.Apttus_Config2__Street__c = oSite.Street_Name__c;
        oAccountLocation.Apttus_Config2__City__c = oSite.City__c;
        oAccountLocation.Apttus_Config2__State__c = oSite.Province_Code__c;
        oAccountLocation.Apttus_Config2__Country__c = oSite.Country__c;
        oAccountLocation.Name = oSite.Street_Number__c + ' ' + oSite.Street_Name__c + ', ' + oSite.City__c;
        return oAccountLocation;
    }
}