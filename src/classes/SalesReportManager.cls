/*
    Feb 24, 2015 Replace Opportunity.Actual_Sign_Date__c with Opportunity.CloseDate
*/
public with sharing class SalesReportManager {
//public static void updateApprovalHistory(List<Scheduled_Quote_Approval_History__c> schedQuotes){
    public static void updateSalesReport(){
        Map<Id, Opportunity> oppMap;
        if(!Test.isRunningTest()) { 
        oppMap = new Map<Id, Opportunity>([SELECT Id, Owner.Name, SyncedQuote.Id, SyncedQuote.QuoteNumber__c, Name, Account.Name, CloseDate FROM Opportunity WHERE IsWon = true and SyncedQuote.Id <> NULL AND CALENDAR_YEAR(CloseDate) = 2013]);
        }
        else{
            oppMap = new Map<Id, Opportunity>([SELECT Id, Owner.Name, SyncedQuote.Id, SyncedQuote.QuoteNumber__c, Name, Account.Name, CloseDate FROM Opportunity WHERE IsWon = true and SyncedQuote.Id <> NULL AND CALENDAR_YEAR(CloseDate) = 2013 limit 5]  );
        }

        
        List<Opportunity> oppList = oppMap.values();
        
        Set<Id> quoteIds = new Set<Id>();
        
        for (Opportunity o : oppList){
            quoteIds.add(o.SyncedQuote.Id);
        }
        system.debug('*****'+oppList);
        //List<QuoteLineItem> wonQuotes = [SELECT Id, Quote.OpportunityId, Site__r.ServiceableLocation__r.Access_Type__c, Charge_Type__c, TotalPrice, Service_Type__c, PricebookEntry.Product2.Access_Type__c FROM QuoteLineItem WHERE QuoteId IN :quoteIds];
        
        List<Access_Type_Sales__c> accessTypeRows = new List<Access_Type_Sales__c>();
        
        for (List<QuoteLineItem> qlis : [SELECT Id, Quote.OpportunityId, Site__r.ServiceableLocation__r.Access_Type__c, Charge_Type__c, TotalPrice, Service_Type__c,UPC_L1__c,UPC_L2__c,UPC_L3__c,UPC_L4__c,UPC_L5__c, PricebookEntry.Product2.Access_Type__c FROM QuoteLineItem WHERE QuoteId IN :quoteIds]){
            system.debug('******'+qlis);
            for (QuoteLineItem qli : qlis){
                Access_Type_Sales__c temp = new Access_Type_Sales__c();
                temp.Quote__c = qli.QuoteId;
                temp.Service_Description__c = qli.Service_Type__c;
                temp.UPC_L1__c = qli.UPC_L1__c;
                temp.UPC_L2__c = qli.UPC_L2__c;
                temp.UPC_L3__c = qli.UPC_L3__c;
                temp.UPC_L4__c = qli.UPC_L4__c;
                temp.UPC_L5__c = qli.UPC_L5__c;
                temp.Access_Type__c = (qli.Site__r.ServiceableLocation__r != null)? qli.Site__r.ServiceableLocation__r.Access_Type__c : qli.PricebookEntry.Product2.Access_Type__c;
                temp.MRR_Price__c = (qli.Charge_Type__c == 'MRC')?qli.TotalPrice : 0;
                temp.NRR_Price__c = (qli.Charge_Type__c == 'NRC')?qli.TotalPrice : 0;
                accessTypeRows.add(temp);
            }
        }
        
        INSERT accessTypeRows;
        
        
        
    }
}