/*
===============================================================================
 Class Name   : TAG_ReCalculateAccTeamOneParent_batch 
===============================================================================
PURPOSE:    This batch class updates AccountTeam after MSD and Shared MSD Owners 
            and Members have been updated, to ensure consistency.
  

Developer: Deepika Rawat
Date: 04/14/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
04/14/2015           Deepika Rawat               Original Version
===============================================================================
*/
global class TAG_ReCalculateAccTeamOneParent_batch implements Database.Batchable<SObject>{
    global Database.QueryLocator start(Database.BatchableContext BC){
        Id accId =ID.valueof(Label.Batch_AccountID);
        if(!Test.isRunningTest()){
            String query = 'select id, Recalculate_Team__c,Account_Team_to_MAL__c, ParentID,OwnerId FROM Account where Id =\''+accId+ '\' ';
            return Database.getQueryLocator(query);
        }
        else{
           return Database.getQueryLocator('select id, Recalculate_Team__c,Account_Team_to_MAL__c, ParentID,OwnerId FROM Account where Recalculate_Team__c=true limit 10');
        }     
    }
    global void execute(Database.BatchableContext BC, List<Account> scope){
        Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
        List<String> lstMSDRoles = TAGsettings.MSD_Recalcuate_Account_Team_Job__c.split(',');
        String msdOwnerRoleName = TAGsettings.Default_MSD_Owner_Role__c;
        String msdMewmberRoleName = TAGsettings.Default_MSD_Member_Role__c;
        String ownerRoleName =TAGsettings.Default_Account_Owner_Role__c;
        Set<Id> setAllAccounts = new Set<Id>();
        Map<Id, Set<Id>> mapAccMSDMember = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> mapAccMSDOwner = new Map<Id, Set<Id>>();
        List<Shared_MSD_Member__c> listSharedMSDMembers = new List<Shared_MSD_Member__c>();
        Set<Id> SharedMSDIds = new Set<Id>();
        Map<Shared_MSD_Accounts__c, Id> mapSASharedMSD = new Map<Shared_MSD_Accounts__c, Id>();
        Map<Id,List<Shared_MSD_Member__c>> mapSharedMSDMembers = new Map<Id,List<Shared_MSD_Member__c>>();
        List<AccountTeamMember> membersToAdd =new List<AccountTeamMember>();
        List<AccountTeamMember> ownerMemberToAdd =new List<AccountTeamMember>();
        Map<Id, Set<Id>> mapAccUserIdsToRemove = new Map<Id, Set<Id>>(); 
        
        for(Account acc:scope){
            setAllAccounts.add(acc.id);
        }
        //Get All MSD Codes
        Map<Id, MSD_Code__c> mapMSDCodes = new Map<Id, MSD_Code__c>([Select id, OwnerId,Account__c from MSD_Code__c where Account__c in :setAllAccounts]);
        system.debug('mapMSDCodes*****'+mapMSDCodes);
        //Get All Shared MSD Codes
        Map<Id, Shared_MSD_Accounts__c> mapSharedMSDCodes = new Map<id, Shared_MSD_Accounts__c>([Select id, Account__c,OwnerId, Shared_MSD_Code__r.OwnerId, Shared_MSD_Code__r.Id from Shared_MSD_Accounts__c where Account__c in :setAllAccounts]);
        //Add MSD OwnerIds into mapAccMSDOwner i.e. <AccountId, List of Related users.add<MSDCode.OwnerID>>
        if(mapMSDCodes!=null){
            Set<Id> msdCodeOwnerIds;
            for(MSD_Code__c msd:mapMSDCodes.values()){
                msdCodeOwnerIds = mapAccMSDOwner.get(msd.Account__c);
                if(msdCodeOwnerIds==null)
                    msdCodeOwnerIds=new Set<Id>();
                msdCodeOwnerIds.add(msd.OwnerId);
                mapAccMSDOwner.put(msd.Account__c,msdCodeOwnerIds); 
            }
        }
        //Add Shared MSD OwnerId into mapAccMSDOwner i.e. <AccountId, List of Related users.add<SharedMSDCode.OwnerID>
        //Create map mapSASharedMSD of <Shared_MSD_Accounts__c, Shared_MSD_Code__c>
        if(mapSharedMSDCodes!=null){
            Set<Id> sharedMSDOwnerIds;
            for(Shared_MSD_Accounts__c sharedMSD:mapSharedMSDCodes.values()){
                sharedMSDOwnerIds = mapAccMSDOwner.get(sharedMSD.Account__c);
                if(sharedMSDOwnerIds==null)
                    sharedMSDOwnerIds=new Set<Id>();
                sharedMSDOwnerIds.add(sharedMSD.Shared_MSD_Code__r.OwnerId);
                mapAccMSDOwner.put(sharedMSD.Account__c,sharedMSDOwnerIds); 
                SharedMSDIds.add(sharedMSD.Shared_MSD_Code__r.Id);
                mapSASharedMSD.put(sharedMSD, sharedMSD.Shared_MSD_Code__r.Id);
            }
        }
        //Create map mapSharedMSDMembers of <Shared_MSD_Code__c , List of Shared_MSD_Member__c>
        listSharedMSDMembers = [Select id, User__c,Shared_MSD_Code__c, Shared_MSD_Code__r.id from Shared_MSD_Member__c where Shared_MSD_Code__c in:SharedMSDIds];
        if(listSharedMSDMembers!=null && listSharedMSDMembers.size()>0){
            List<Shared_MSD_Member__c> sharedMSDMembers;
            for(Shared_MSD_Member__c memberMSD: listSharedMSDMembers){
                sharedMSDMembers = mapSharedMSDMembers.get(memberMSD.Shared_MSD_Code__r.id);
                if(sharedMSDMembers==null)
                    sharedMSDMembers=new List<Shared_MSD_Member__c>();
                sharedMSDMembers.add(memberMSD);
                mapSharedMSDMembers.put(memberMSD.Shared_MSD_Code__r.id,sharedMSDMembers); 
            }
        }
        //Add Shared MSD Member Ids into mapAccMSDMember i.e. <AccountId, List of Related users.add<Shared_MSD_Member__c.User__c>
        if(mapSharedMSDCodes!=null){
            Id sharedMsd;
            Set<Id> malMemberIds;
            for(Shared_MSD_Accounts__c sharedAccMSD:mapSharedMSDCodes.values()){
                sharedMsd = mapSASharedMSD.get(sharedAccMSD);
                if(sharedMsd!=null &&  mapSharedMSDMembers.get(sharedMsd)!=null){
                    for(Shared_MSD_Member__c mem : mapSharedMSDMembers.get(sharedMsd) ){
                        malMemberIds = mapAccMSDMember.get(sharedAccMSD.Account__c);
                        if(malMemberIds==null)
                            malMemberIds=new Set<Id>();
                        malMemberIds.add(mem.User__c);
                        mapAccMSDMember.put(sharedAccMSD.Account__c,malMemberIds); 
                    }  
                }
                
            }
        }
        List<MSD_Member__c> listMSDMembers = [Select id, User__c, MSD_Code__c, MSD_Code__r.id, MSD_Code__r.Account__c from MSD_Member__c where MSD_Code__c in :mapMSDCodes.keyset()];
        //Add MSD Member Ids into mapAccMSDMember i.e. <AccountId, List of Related users.add<MSD_Member__c.User__c>
        if(listMSDMembers!=null && listMSDMembers.size()>0){
            Set<Id> msdMemberIds ;
            for(MSD_Member__c memberMSD: listMSDMembers){
                msdMemberIds= mapAccMSDMember.get(memberMSD.MSD_Code__r.Account__c);
                if(msdMemberIds==null)
                    msdMemberIds=new Set<Id>();
                msdMemberIds.add(memberMSD.User__c);
                mapAccMSDMember.put(memberMSD.MSD_Code__r.Account__c,msdMemberIds); 
            }
        }
        system.debug('mapAccMSDMember******'+mapAccMSDMember);
        List<AccountTeamMember> existingAccMSDMembers = [Select Id, UserId, TeamMemberRole, Account.Id from AccountTeamMember where AccountId in: setAllAccounts and (TeamMemberRole in :lstMSDRoles)];
        List<AccountTeamMember> existingAccMembers = [Select Id, UserId,TeamMemberRole,Account.Id from AccountTeamMember where AccountId in: setAllAccounts and (TeamMemberRole NOT In : lstMSDRoles)];
        //Create map of Account and Owner type team members
        Map<Id,Set<Id>> mapAccMembers = new Map<Id,Set<Id>>();
        if(existingAccMembers!=null){
            Set<Id> accMemberIds;
            for(AccountTeamMember member : existingAccMembers){
                accMemberIds = mapAccMembers.get(member.Account.id);
                if(accMemberIds==null)
                    accMemberIds=new Set<Id>();
                accMemberIds.add(member.UserId);
                mapAccMembers.put(member.Account.id,accMemberIds);   
            }
        }
        for(Account acc:scope){
            AccountTeamMember newMem = new AccountTeamMember();
            newMem.TeamMemberRole = ownerRoleName;
            newMem.AccountId = acc.Id;
            newmem.UserId = acc.OwnerId;
            ownerMemberToAdd.add(newmem);
        }
        system.debug('****membersToAdd1******'+membersToAdd);
        AccountTeamMember newMem ;
        Set<Id> approvedOwnerMembers;
        Set<Id> existingOwnerMembers;
        Set<Id> approvedMembers;
        for(Id accId: setAllAccounts){
            approvedOwnerMembers = mapAccMSDOwner.get(accId);
            existingOwnerMembers = mapAccMembers.get(accId);
            
            if(approvedOwnerMembers!=null && approvedOwnerMembers.size()>0){
                for(Id approvedId : approvedOwnerMembers){
                    if(approvedId!=null){
                        if(!(existingOwnerMembers !=null && existingOwnerMembers.contains(approvedId))){
                            newMem = new AccountTeamMember();
                            newMem.TeamMemberRole = msdOwnerRoleName;
                            newMem.AccountId = accId;
                            newmem.UserId = approvedId;
                            membersToAdd.add(newmem);
                        }
                    }
                }
            }
        }
        system.debug('****membersToAdd2******'+membersToAdd);
        for(Id accId: setAllAccounts){
            approvedMembers = mapAccMSDMember.get(accId);
            existingOwnerMembers = mapAccMembers.get(accId);
            approvedOwnerMembers = mapAccMSDOwner.get(accId);
            if(approvedMembers!=null && approvedMembers.size()>0){
                for(Id approvedId : approvedMembers){
                    if(approvedId!=null){
                        if(!((existingOwnerMembers!=null && existingOwnerMembers.contains(approvedId)) || (approvedOwnerMembers!=null && approvedOwnerMembers.contains(approvedId)))){    
                            newMem = new AccountTeamMember();
                            newMem.TeamMemberRole = msdMewmberRoleName;
                            newMem.AccountId = accId;
                            newmem.UserId = approvedId;
                            membersToAdd.add(newmem);
                        }
                    }
                }
            }
        }
        system.debug('****membersToAdd3******'+membersToAdd);
        List<Account> accListToUpdate = new List<Account>();
        Map<Id, Set<Id>> mapAccUserIdsToAdd = new Map<Id, Set<Id>>(); 
        for(Account acc:scope){
            acc.Recalculate_Team__c = false;
            acc.Account_Team_to_MAL__c = true;
            accListToUpdate.add(acc);
        }
        Set<Id> userIds;
        //Delete Existing TeamMembers for selected accounts
        if(existingAccMSDMembers.size()>0){
            try{
                
                for(AccountTeammember mem: existingAccMSDMembers){
                    userIds = mapAccUserIdsToRemove.get(mem.Account.Id);
                    If(userIds==null)
                        userIds = new Set<Id>();
                    userIds.add(mem.UserId);
                    mapAccUserIdsToRemove.put(mem.Account.Id,userIds);
                }
                delete existingAccMSDMembers;
                if(Test.isRunningTest())
                { Integer a=1/0;
            }
            }catch(exception e){
            } 
        }
        system.debug('membersToAdd******'+membersToAdd);
        //Create New TeamMembers for selected accounts
        if(membersToAdd!=null && membersToAdd.size()>0){
            try{
                insert membersToAdd;
                if(Test.isRunningTest()){ 
                    Integer a=1/0;
                }
            }catch(exception e){
            }
        }
        if(ownerMemberToAdd!=null && ownerMemberToAdd.size()>0){
            insert ownerMemberToAdd;
        }
        List<AccountTeamMember> parentAccMSDMembers = [Select Id, UserId,TeamMemberRole,Account.Id from AccountTeamMember where  Account.Id in: setAllAccounts and TeamMemberRole in :lstMSDRoles ];
        List<AccountTeamMember> team;
        system.debug('parentAccMSDMembers******'+parentAccMSDMembers);
        Set<Id> teamMemIds;
        for(AccountTeamMember teamMem : parentAccMSDMembers){
            teamMemIds = mapAccUserIdsToAdd.get(teamMem.Account.Id);
            if(teamMemIds==null)
                teamMemIds = new Set<Id>();
            teamMemIds.add(teamMem.UserId);
            mapAccUserIdsToAdd.put(teamMem.Account.Id,teamMemIds);
        }
        
        //Call updateAccountTeamAccess class to Give/Remove access from Users after updating AccountTeam
        updateAccountTeamAccess accessCls = new updateAccountTeamAccess();
        accessCls.removeAccessFromTeam(mapAccUserIdsToRemove);
        accessCls.giveAccesstoTeam(mapAccUserIdsToAdd);
        Set<Account> setAcctoUpdate = new Set<Account>();
        //Update Recalculate_Team__c flag to false
        if(accListToUpdate.size()>0){
            setAcctoUpdate.addAll(accListToUpdate);
            accListToUpdate.clear();
            accListToUpdate.addAll(setAcctoUpdate);
            update accListToUpdate;
        }
    }
    global void finish(Database.BatchableContext BC){
         InitialLoadAccountTeamOneParent_batch sc = new InitialLoadAccountTeamOneParent_batch();
         ID batchprocessid = Database.executeBatch(sc,10);
    }
}