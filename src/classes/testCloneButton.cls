@isTest(seeAllData=true)
public class testCloneButton{
    static testmethod void testCloneOpportunity(){
        Config__c settings = Config__c.getInstance('Config');
                settings.OpportunityExclusion__c = 'ForecastCategory, Fiscal, Description';
                settings.OLIExclusion__c = '';
                settings.QuoteExclusion__c = null;
                settings.QLIExclusion__c = '';
        //with Quote && QLIs
        Opportunity op = new Opportunity(Name='test1',
                                        StageName = 'Duplicate',
                                        Unified_Comm_Collaboration_Estimated__c =20,
                                        CloseDate=Date.newInstance(2012, 6,14));
        insert op;
        Quote__c q = new Quote__c(Opportunity_Name__c = op.Id);
        q.Syncing__c = True;
        insert q;
        Product2 testProd2 = new Product2(Name = 'testName');
        insert testProd2;
        Quote_Line_Item__c qlis = new Quote_Line_Item__c(
                                    Name = 'test',
                                    Price__c = 1.0,
                                    Type__c = 'Custom',
                                    Quote__c = q.Id,
                                    Commission_Type__c = 'New Revenue',
                                    Product__c = testProd2.Id);
        insert qlis;
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(op);
        OpportunityCloning oppClon = new OpportunityCloning(sc);
        oppClon.cloneOpportunity();
        //with OLIs
        /*PricebookEntry pb = [select Id From PricebookEntry limit 1];
        OpportunityLineItem olis = new OpportunityLineItem(OpportunityId = op.Id,
                                                            Quantity=10.0,
                                                            TotalPrice = 100.0,
                                                            PricebookEntryId = pb.Id);
        insert olis;
        sc = new ApexPages.StandardController(op);
        oppClon = new OpportunityCloning(sc);
        oppClon.cloneOpportunity();
        op = new Opportunity();
        sc = new ApexPages.StandardController(op);
        oppClon = new OpportunityCloning(sc);
        oppClon.cloneOpportunity();
        Test.stopTest();*/
    }
    
    /////upd 06.21.2012
    private static testMethod void unitTest1() {
        Config__c settings = Config__c.getInstance('Config');
                settings.OpportunityExclusion__c = 'ForecastCategory, Fiscal, Description';
                settings.OLIExclusion__c = '';
                settings.QuoteExclusion__c = null;
                settings.QLIExclusion__c = '';
        //with Quote && QLIs
        Pricebook2 stdpb = [SELECT Id FROM Pricebook2 WHERE IsStandard = true];
        Opportunity op = new Opportunity(Name='test1',
                                        StageName = 'Duplicate',
                                        Unified_Comm_Collaboration_Estimated__c =20,
                                        Pricebook2Id = stdpb.id,
                                        CloseDate=Date.newInstance(2012, 6,14));
        insert op;
        
        Quote__c q = new Quote__c(
                                    Opportunity_Name__c = op.Id,
                                    Syncing__c = TRUE);
        insert q;
        
        Product2 testProd2 = new Product2(Name = 'testName');
        insert testProd2;
        
        
        PricebookEntry pb = [select Id From PricebookEntry where IsActive = true AND Pricebook2.id=:stdpb.id limit 1];
        OpportunityLineItem olis = new OpportunityLineItem(OpportunityId = op.Id,
                                                            Quantity=10.0,
                                                            TotalPrice = 100.0,
                                                            PricebookEntryId = pb.Id);
        insert olis;
        
        Quote_Line_Item__c qlis = new Quote_Line_Item__c(
                                    Name = 'test',
                                    Price__c = 1.0,
                                    Type__c = 'Custom',
                                    Quote__c = q.Id,
                                    Commission_Type__c = 'New Revenue',
                                    Product__c = testProd2.Id,
                                    RollupType__c = '123',
                                    RollupValue__c = 123,
                                    Service_Class__c = '123',
                                    Sub_Class__c = '123',
                                    Service_Group__c = '123',
                                    Family__c = '123');
        insert qlis;
        
        
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(op);
        OpportunityCloning oppClon = new OpportunityCloning(sc);
        oppClon.cloneOpportunity();
        Test.stopTest();
    }

    static testmethod void testCloneQoute(){
        Config__c settings = Config__c.getInstance('Config');
                settings.OpportunityExclusion__c = 'name,isClosed,closedate';
                settings.OLIExclusion__c = '';
                settings.QuoteExclusion__c = null;
                settings.QLIExclusion__c = '';
        Opportunity op = new Opportunity(
                                        Name='test1',
                                        StageName = 'Duplicate',
                                        Unified_Comm_Collaboration_Estimated__c =20,
                                        CloseDate=Date.newInstance(2012, 6,14));
        insert op;
        
        Quote__c q = new Quote__c(
                                        Opportunity_Name__c = op.Id,
                                        Syncing__c = TRUE);
        insert q;
        
        Product2 testProd2 = new Product2(Name = 'testName');
        insert testProd2;
        
        Quote_Line_Item__c qlis = new Quote_Line_Item__c(Name = 'test',
                                                        Price__c = 1.0,
                                                        Type__c = 'Custom',
                                                        Quote__c = q.Id,
                                                        Commission_Type__c = 'New Revenue',
                                                        Product__c = testProd2.Id,
                                                        RollupType__c = '123',
                                                        RollupValue__c = 123,
                                                        Service_Class__c = '123',
                                                        Sub_Class__c = '123',
                                                        Service_Group__c = '123',
                                                        Family__c = '123');
        insert qlis;
        
        Quote_Line_Item__c qlis2 = new Quote_Line_Item__c(Name = 'test',
                                                        Price__c = 1.0,
                                                        Type__c = 'Custom',
                                                        Quote__c = q.Id,
                                                        Commission_Type__c = 'New Revenue',
                                                        Product__c = testProd2.Id,
                                                        Parent_Quote_Line_Item__c = qlis.Id,
                                                        RollupType__c = '123',
                                                        RollupValue__c = 123,
                                                        Service_Class__c = '123',
                                                        Sub_Class__c = '123',
                                                        Service_Group__c = '123',
                                                        Family__c = '123');
        insert qlis2;
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(q);
        QuoteCloning quoteClon = new QuoteCloning(sc);
        quoteClon.cloneQuote();
        q = new Quote__c();
        sc = new ApexPages.StandardController(q);
        quoteClon = new QuoteCloning(sc);
        quoteClon.cloneQuote();
        Test.stopTest();
    }
}