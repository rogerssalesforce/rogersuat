/*
@Name:  ShiftCreateDefaultOLISupport 
@Description: Class with method to Create default Opportunity Line Items based on Aggregated Products
@Dependancies: 
@Version: 1.0.0

===VERSION HISTORY ===
Date        | Version Number | Author | Description
04-14-2014  | 1.0.0          | Jocsan | Initial
04-15-2014  | 1.0.1          | Edward | Add try/catch during Dml Operation
*/
public class ShiftCreateDefaultOLISupport {

    public static boolean hasRun = false;
    
    //Method to Create default Opportunity Line Items based on Aggregated Products
    public static void theCreateDefaultOLI(map<Id, Opportunity> theOpportunityMap) {
        List<ID> oppIDs = new List<ID>();
        List<ID> oppToUpdateIDs = new List<ID>();
        List<Opportunity> listOpp = new List<Opportunity>();
        List<Opportunity> listOppToUpdate = new List<Opportunity>();
        boolean proceed=false; 
         List<Pricebook2> dataCenterPB = [Select Id, Name from Pricebook2 where Name='Data Centre Price Book' ];
        for(Opportunity opp : theOpportunityMap.values()){
            if(opp.Business_Unit__c == 'Data Centre'){
                oppIDs.add(opp.id);
                system.debug('******'+opp.PriceBook2ID);
                system.debug('######'+dataCenterPB);
                proceed=true;
                if(dataCenterPB!=null && dataCenterPB.size()>0 && dataCenterPB[0].Id != opp.PriceBook2ID){
                    Opportunity oppNew = new Opportunity(Id=opp.id);
                    oppNew.PriceBook2ID= dataCenterPB[0].Id ;
                    listOppToUpdate.add(oppNew);
                }
            }
        }
        if(listOppToUpdate.size()>0){
            update listOppToUpdate;
        }
        
        listOpp = [Select id, Pricebook2ID from Opportunity where Id in : oppIDs ];

        
        ShiftCreateDefaultOLISupport.hasRun = true;
        // only process for RDC
        if(proceed){        
        // get all aggregated products
        
        Map<Id, PricebookEntry> thePBEList = new Map<Id, PricebookEntry>([
           select Id, Name, Product2.Family, PriceBook2Id from PricebookEntry 
            Where Name IN ('Business Resumption Total', 'Facilities Management Total','Connectivity Total','Cloud Total','Managed Services Total','Colocation Total') 
            AND PriceBook2.Name='Data Centre Price Book' AND Product2.Aggregated_Product__c = true 
            and Product2.Family != null and Product2.IsActive = true LIMIT 100]);
        system.debug(thePBEList);
                
        // create new OLI from PBE list
        list<OpportunityLineItem> theNewOLIList = new list<OpportunityLineItem>();
        map<Id, string> thePBEFamilyMap = new map<Id, string>(); // we need this to reference later as we are only setting the pbe.Id and that doesn't populate the family
        for (PricebookEntry pbe : thePBEList.values()) {
            OpportunityLineItem oli = new OpportunityLineItem();
            oli.UnitPrice = 0;
            oli.Quantity = 1;
            oli.PricebookEntryId = pbe.Id;
            oli.Line_Item_Product_Family__c = pbe.Product2.Family;
            
            thePBEFamilyMap.put(pbe.Id, pbe.Product2.Family);
            // oli.OpportunityId = nothing; // will do this later on
            
            theNewOLIList.add(oli);
        }
        system.debug(theNewOLIList);
        
        // get existing OLIs if any
        map<Id, set<string>> theOppProdMap = new map<Id, set<string>>();
        list<OpportunityLineItem> theExistingOLIList = new list<OpportunityLineItem>([
            select Id, PricebookEntry.Product2.Family, PricebookEntry.Product2.Aggregated_Product__c, OpportunityId 
                from OpportunityLineItem where OpportunityId in: oppIDs]);
        system.debug(theExistingOLIList);
        
        // populate opportunity to product family map. this map is used to only create missing ones if any.
        list<OpportunityLineItem> theOLIList2Delete = new list<OpportunityLineItem>();
        for (OpportunityLineItem oli : theExistingOLIList) {
            // delete any non-aggregated OLI
            if (!oli.PricebookEntry.Product2.Aggregated_Product__c) {
                theOLIList2Delete.add(oli);
                continue;
            }   
            
            if (!theOppProdMap.containsKey(oli.OpportunityId)) {
                set<string> temp = new set<string>();
                temp.add(oli.PricebookEntry.Product2.Family);
                theOppProdMap.put(oli.OpportunityId, temp);
            } else {
                // check for duplicate products based on family and add or remove accordingly
                if (!theOppProdMap.get(oli.OpportunityId).contains(oli.PricebookEntry.Product2.Family)) {
                    theOppProdMap.get(oli.OpportunityId).add(oli.PricebookEntry.Product2.Family);
                } else {
                    // ensure one product per family
                    theOLIList2Delete.add(oli);
                }
            }
        }
        system.debug(theOppProdMap);
               
        // create new OLI linked to opportunity records
        list<OpportunityLineItem> theOLIList2Insert = new list<OpportunityLineItem>();
        for (Opportunity o : listOpp) {
            // clone OLI list
            //list<OpportunityLineItem> temp = theNewOLIList.clone();
            
            // populate opportunity id field
            for (OpportunityLineItem oli : theNewOLIList) {
                 // check for o.Pricebook2ID == oli.PriceBook2Id" added by Deepika: 3/31/2015
                
                if (theOppProdMap.containsKey(o.Id)) {
                    system.debug(thePBEFamilyMap.get(oli.PricebookEntryId));
                    if (!theOppProdMap.get(o.Id).contains(thePBEFamilyMap.get(oli.PricebookEntryId)) ) {
                        // clone oli and set opportunity id
                        OpportunityLineItem temp = oli.clone();
                        temp.OpportunityId = o.Id;
                        
                        // add OLIs to list to insert
                        theOLIList2Insert.add(temp);                    
                    }
                } else {
                    // clone oli and set opportunity id
                    OpportunityLineItem temp = oli.clone();
                    temp.OpportunityId = o.Id;
                    
                    // add OLIs to list to insert
                    theOLIList2Insert.add(temp);
                }
                
            }
        }
        
        // insert OLIs
        system.debug(theOLIList2Insert);
        try{
            insert theOLIList2Insert;
            delete theOLIList2Delete;
        } catch (DMLexception e) {
            for (Opportunity theOpp: theOpportunityMap.values())
                theOpp.addError('There was an error during the creation of the OpportunityLineItems Error= '+e);
        }
       } 
    }           
}