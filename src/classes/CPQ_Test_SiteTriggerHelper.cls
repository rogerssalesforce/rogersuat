/*********************************************************************************
Class Name      : CPQ_Test_SiteTriggerHelper 
Description     : This class is used as a test class for CPQ_SiteTriggerHelper class
Created By      : Alina Balan
Created Date    : 23-Sep-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Alina Balan                07-December-2015       Original version
----------------------------------------------------------------------------------            
*********************************************************************************/

@isTest (seeAllData=False)
public with sharing class CPQ_Test_SiteTriggerHelper {
    private static User TestAEUser;
    private static Apttus_Config2__PriceList__c oPriceList;
    private static Account oAccount;
    private static Opportunity oOpportunity;
    private static Opportunity oOpportunity2;
    private static List<Site__c> lstSites;
    private static final String CPQ_TEST_CITY = 'Sity test';
    private static final String CPQ_TEST_STREET_NUMBER = '1';
    private static final Integer CPQ_TEST_RECORDS_NUMBER = 20;
    
    /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    :   
    Parameter      :              
    *********************************************************************************/
    private static void createTestData() {
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');
        system.runAs(TestAEUser) {
            //Create PriceList
            oPriceList = CPQ_Test_Setup.newPriceList();
            insert oPriceList ;
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            oOpportunity2 = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity2;
            //create sites
            lstSites = new List<Site__c>();
            for (Integer i = 0; i < CPQ_TEST_RECORDS_NUMBER; i++) {
                Site__c oSite = CPQ_Test_Setup.createSite(oOpportunity, null, CPQ_TEST_STREET_NUMBER + i, CPQ_TEST_CITY + i);
                lstSites.add(oSite);
            }
            insert lstSites;
        }
    }
    
    /*********************************************************************************
    Method Name    : testCopyPrimaryQuoteDoctoAgreement
    Description    : Method to test account location creation
    Return Type    :   
    Parameter      :               
    *********************************************************************************/ 
    static testmethod void testCreateAccountLocations(){
        createTestData();
        system.runAs(TestAEUser){
            test.startTest();
            Map<Id, Site__c> siteMap = new Map<Id, Site__c>(lstSites);
            List<Apttus_Config2__AccountLocation__c> lstAccountLocations = [SELECT Id FROM Apttus_Config2__AccountLocation__c WHERE Prospect_Site__c IN: siteMap.keySet()];
            system.assertEquals(lstAccountLocations.size(), CPQ_TEST_RECORDS_NUMBER);
            Test.stopTest();
        }
    }
    
    /*********************************************************************************
    Method Name    : testUpdateAccountLocations
    Description    : Method to test account location update
    Return Type    :   
    Parameter      :               
    *********************************************************************************/ 
    static testmethod void testUpdateAccountLocations(){
        createTestData();
        system.runAs(TestAEUser){
            test.startTest();
            for(Site__c site : lstSites) {
                site.Opportunity__c = oOpportunity2.Id;
            }
            update lstSites;
            List<Apttus_Config2__AccountLocation__c> lstAccountLocations = [SELECT Id FROM Apttus_Config2__AccountLocation__c WHERE Opportunity__c =: oOpportunity2.Id];
            system.assertEquals(lstAccountLocations.size(), CPQ_TEST_RECORDS_NUMBER);
            Test.stopTest();
        }
    }
    
    /*********************************************************************************
    Method Name    : testDeleteAccountLocations
    Description    : Method to test account location deletion
    Return Type    :   
    Parameter      :               
    *********************************************************************************/ 
    static testmethod void testDeleteAccountLocations(){
        createTestData();
        system.runAs(TestAEUser){
            test.startTest();
            List<Site__c> lstSitesToDelete = new List<Site__c>();
            List<Id> lstDeletedIds = new List<Id>();
            Integer intIndex = 0;
            for(Site__c site : lstSites) {
                lstSitesToDelete.add(site);
                lstDeletedIds.add(site.id);
                intIndex++;
                if (intIndex >= CPQ_TEST_RECORDS_NUMBER/2) break;
            }
            delete lstSitesToDelete;
            List<Apttus_Config2__AccountLocation__c> lstAccountLocations = [SELECT Id FROM Apttus_Config2__AccountLocation__c WHERE Prospect_Site__c IN: lstDeletedIds];
            system.assertEquals(lstAccountLocations.size(), 0);
            
            Map<Id, Site__c> siteMap = new Map<Id, Site__c>(lstSites);
            lstAccountLocations = [SELECT Id FROM Apttus_Config2__AccountLocation__c WHERE Prospect_Site__c IN: siteMap.keySet()];
            system.assertEquals(lstAccountLocations.size(), CPQ_TEST_RECORDS_NUMBER/2);
            Test.stopTest();
        }
    }
    
}