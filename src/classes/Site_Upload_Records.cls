public with sharing class Site_Upload_Records {
	//Wrapper Class
	
	public String CSV_File_Record {get; set;}
	public String Record_Status {get;set;}
	
	public Site_Upload_Records(string csv,string status)
	{
		CSV_File_Record = csv;
		Record_Status = status;
	}
	public Site_Upload_Records()
	{
		
	}
}