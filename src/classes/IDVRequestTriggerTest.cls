/*
Test class for IDVRequestTrigger

*/
@isTest (seeAllData=True) 
private class IDVRequestTriggerTest{

 private static Profile pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
 private static User userRec;
 private static User userContractTeam;
 private static User userEVPTeam;
 private static Account acc;
 private static Opportunity  opp;
 
  private static void setUpData(){
  
   userRec = new User(LastName = 'Mark O’Roger', Alias = 'alRoger1', Email='test@Rogertest.com', Username='test@Rogertest1.com', CommunityNickname = 'nickRog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec;
        
   userContractTeam = new User(LastName = 'Roy Nat', Alias = 'rnat1', Email='test1@Rogertest.com', Username='test1@Rogertest1.com', CommunityNickname = 'Rnat12', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userContractTeam;
        
   userEVPTeam = new User(LastName = 'Selena Sindu', Alias = 'ssindu1', Email='test2@Rogertest.com', Username='test2@Rogertest1.com', CommunityNickname = 'Ssindu12', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userEVPTeam;
        
        
        acc = new Account();
        acc.name = 'Test Act';
        acc.Business_Segment__c = 'Alternate';
        acc.BillingStreet = 'Test';
        acc.BillingCity = 'Ontario';
        acc.BillingCountry = 'CA';
        acc.BillingPostalCode = 'A9A 9A9';
        acc.BillingState = 'ON';
        acc.Account_Status__c = 'Assigned';
        insert acc;       
        
       
        opp = new Opportunity();
        opp.Estimated_MRR__c = 500;
        opp.Name = 'Test Opp';
        opp.StageName = 'Suspect - Qualified';
        opp.Product_Category__c = 'Local';
        opp.Network__c = 'Cable';
        opp.Estimated_One_Time_Charge__c = 500;
        opp.New_Term_Months__c = 5;
        opp.AccountId = acc.id;
        opp.CloseDate = date.newInstance(2013,1,10);
        opp.Unified_Comm_Collaboration_Estimated__c =20;
        insert opp;
  
  }
 
   static testmethod void testIDVRequest(){
    setUpData(); 
    
    IDV_Pricing_Request__c IDV0 = new IDV_Pricing_Request__c();
    
    IDV0.IDV_Request_Opportunity__c=opp.id;
    IDV0.Rogers_Sales_Representative__c=userRec.id;
    IDV0.Target_Number_Of_New_Individually_Liable__c=14;
    IDV0.Individually_Liable_MSD_Code__c='MSD1234';
    IDV0.Customers_with_Corporate_Email__c=90;
    IDV0.Corporate_Email_Domains__c='abc.marketing.ca';
    IDV0.Status__c='New';
     insert IDV0;
     
     IDV0.Status__c='With Contracts Team';
     IDV0.Contracts_Prime__c=userContractTeam.id;
     update IDV0;
     
     
     
     IDV0.Status__c='With EVP Team';
     IDV0.EVP_Prime__c=userEVPTeam.id;
     IDV0.Final_IDV_Request__c=true;
     IDV0.Date_Share_Everything_Discount_Loaded__c= date.today();
   
     update IDV0;
     
     
    IDV0.Status__c='Completed';
    IDV0.Date_Share_Everything_Discount_Loaded__c= date.today(); 
    IDV0.Date_Emp_Verification_Process_TurnedOn__c= date.today();
   
     update IDV0;
     
     
    
    }
}