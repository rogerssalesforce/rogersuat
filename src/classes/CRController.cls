/*
  Name         : CRController
  Purpose      : Serves as controller class for CR Join page
  Created Date : 4/2/2012*/
  
public class CRController{
    
    public Change_Request__c crInst{get;set;}
    public List<ComponentWrapper> componentList{get;set;}
    public Component__c newComp{get;set;}
    public List<Component__c> newCompList{get;set;}
    
    public CRController(ApexPages.StandardController controller){
        Change_Request__c currentCR = (Change_Request__c)controller.getRecord();
        crInst = [select Id,Work_Package__c from Change_Request__c where Id = :currentCR.Id];
        componentList = new List<ComponentWrapper>();
        newComp = new Component__c();
        Set<Id> existingCompIds = new Set<Id>();
        for(CR_Components_Join__c joinRec : [select Change_Request__c, Component__c from CR_Components_Join__c where Change_Request__c = :currentCR.Id]){
        	existingCompIds.add(joinRec.Component__c);	
        }
        for(Component__c cmp : [select Id,Name from Component__c where Work_Package__c = :crInst.Work_Package__c]){
            if(existingCompIds.contains(cmp.Id)){
            	componentList.add(new ComponentWrapper(cmp.Name,cmp.Id,true,true));
            }
            else{
            	componentList.add(new ComponentWrapper(cmp.Name,cmp.Id,false,false));
            }
        }
        newCompList = new List<Component__c>();
        newCompList.add(new Component__c());
        newCompList.add(new Component__c());
    }
    
    public class ComponentWrapper{
        public boolean selected{get;set;}
        public String name{get;set;}
        public Id compId{get;set;}
        public boolean created{get;set;}
        public componentWrapper(String name, Id compId, Boolean selected, Boolean created){
            this.name = name;
            this.compId = compId;
            this.selected = selected;
            this.created = created;
        }
    }
    
    public PageReference attach(){
        List<CR_Components_Join__c> crjRecords = new List<CR_Components_Join__c>();
        for(ComponentWrapper cw : componentList){
            if(cw.selected && !cw.created){
                crjRecords.add(new CR_Components_Join__c(Change_Request__c = crInst.Id, Component__c = cw.compId));
                cw.created = true;
            }
        }
        insert crjRecords;
        return null;
    }
    
    public PageReference createCompNJoin(){
    	try{
    		Integer compCount = 0;
    		List<Component__c> createComps = new List<Component__c>();
    		Map<Id,String> compMap = new Map<Id,String>();
            for(Component__c comp : newCompList){
            	if(comp.Name != NULL){
            		compCount++;
            		Component__c insertComp = new Component__c(Name = comp.Name, Work_Package__c = crInst.Work_Package__c, Last_Migrated_On__c = comp.Last_Migrated_On__c, Last_Migrated_To__c = comp.Last_Migrated_To__c);
            		createComps.add(insertComp);
            	}
            }
            if(!createComps.isEmpty()){
	            insert createComps;
	            List<CR_Components_Join__c> createCRJoin = new List<CR_Components_Join__c>();
	            for(Component__c comp : createComps){
	            	CR_Components_Join__c newCRJoin = new CR_Components_Join__c(Change_Request__c = crInst.Id, Component__c = comp.Id);
	            	compMap.put(comp.Id,comp.Name);
                	createCRJoin.add(newCRJoin);
	            }
	            insert createCRJoin;
	            for(CR_Components_Join__c crComps : createCRJoin){
	            	if(compMap.containsKey(crComps.Component__c)){
	            		componentList.add(new ComponentWrapper(compMap.get(crComps.Component__c),crComps.Id,true,true));
	            	}	
	            }	            
	            return null;
            }
            if(compCount == 0){
            	Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter Name and WorkPackage for the Components you create'));
                return null;
            }
            /*if(newComp.Name != NULL && newComp.Work_Package__c != NULL){
                Component__c insertComp = new Component__c();
                insertComp.Name = newComp.Name;
                insertComp.Work_Package__c = newComp.Work_Package__c;
                if(newComp.Last_Migrated_On__c != NULL){
                	insertComp.Last_Migrated_On__c = newComp.Last_Migrated_On__c;
                	newComp.Last_Migrated_On__c = NULL;
                }
                if(newComp.Last_Migrated_To__c != NULL){
                	insertComp.Last_Migrated_To__c = newComp.Last_Migrated_To__c;                	
                }
                if(newComp.Description__c != NULL){
                	insertComp.Description__c = newComp.Description__c;                	
                }
                if(newComp.Dependency_Details__c != NULL){
                	insertComp.Dependency_Details__c = newComp.Dependency_Details__c;                	
                }
                insert insertComp;
                CR_Components_Join__c newCRJoin = new CR_Components_Join__c(Change_Request__c = crInst.Id, Component__c = insertComp.Id);
                insert newCRJoin;
                componentList.add(new ComponentWrapper(insertComp.Name,insertComp.Id,true,true));
                return null;
            }
            else{              
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a Name and WorkPackage for the Component you create'));
                return null;
            }*/
        }
        catch(System.Exception ex){
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An unexpected exception occured. Please contact your System Admin'));
            System.debug('###Exception : '+ ex.getMessage());
            return null;    
        }
        return null;
    }
    
    public PageReference addRows(){
    	newCompList.add(new Component__c());
    	return null;
    }
}