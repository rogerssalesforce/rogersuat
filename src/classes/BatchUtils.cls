/*
===============================================================================
Class Name   : BatchUtils 
===============================================================================
PURPOSE:    Utility methods for batch jobs
  

CHANGE HISTORY
===============================================================================
DATE              NAME                        DESC
MM/DD/YYYY
01/29/2014        Michael Appleton            Created
===============================================================================
*/
public with sharing class BatchUtils {
    
    /**
    * Obtain a default value from the Organization-Wide Email Address list.   This is done 
    * by looking for a given default value in the List of OrgWideEmailAddress 
    * objects currently defined in the environment.  
    * 
    * If the given default value is not found, the last entry in the list will be used.
    * 
    * defaultOWE - the String representing the default value for the Organization-Wide Email Address
    *
    * @returns Id
  **/
  public static Id getDefaultOrgWideEmailAddr(String defaultOWE){
      
        List<OrgWideEmailAddress> listOwe = [select id, Address, DisplayName from OrgWideEmailAddress];
      
        Id currentAddressId;
        for(OrgWideEmailAddress orgwide : listOWE){
            currentAddressId = orgwide.id;
            
            if(orgwide.Address.equals(defaultOWE)){
                break;
            }
            
        }
        listOwe = null;
          
        return currentAddressId;
  }

}