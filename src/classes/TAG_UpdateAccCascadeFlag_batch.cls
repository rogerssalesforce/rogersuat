/*
===============================================================================
 Class Name   : TAG_UpdateAccCascadeFlag_batch
===============================================================================
PURPOSE:    This batch class updates AccountTeam after MSD and Shared MSD Owners 
            and Members have been updated, to ensure consistency.
  

Developer:  Aakanksha Patel  
Date: 15/06/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
15/06/2015           Aakanksha Patel             Original Version
===============================================================================
*/
global class TAG_UpdateAccCascadeFlag_batch implements Database.Batchable<SObject>, Database.stateful{
    
    Global String query;
    Global Set<Id> setAccountUpdate = new set<Id>();
    
    global TAG_UpdateAccCascadeFlag_batch(Set<Id> setAccountUpdate)
    {
        this.setAccountUpdate = setAccountUpdate;  // constructor copies in arg to local instance vbl
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        if(!Test.isRunningTest()){
        return Database.getQueryLocator('select id, Cascade_Team_to_Child__c, OwnerId, Owner.isActive FROM Account where Cascade_Team_to_Child__c=true and ID in : setAccountUpdate');
        }
        else{
           return Database.getQueryLocator('select id, Cascade_Team_to_Child__c, OwnerId, Owner.isActive FROM Account where Cascade_Team_to_Child__c=true and ID in : setAccountUpdate limit 10');
        }     
    }

    global void execute(Database.BatchableContext BC, List<Account> scope){
     //   system.debug('scope*****'+scope);
      
      List<Account> lstToUpdate = new list<Account>();
      
      for(Account Acc : scope)
      {
            Acc.Cascade_Team_to_Child__c = false;
            lstToUpdate.add(Acc);
      }
        
        if(lstToUpdate.size()>0)
        {
            update lstToUpdate;
        }
        
        
    }
    global void finish(Database.BatchableContext BC){       
     
    }
}