/*********************************************************************************
Class Name      : CPQ_Test_AgreementLine_Trigger 
Description     : This class a test class for CPQ_AgreementLine trigger. 
Created By      : Deepika Rawat
Created Date    : 24-Sep-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Akshay Ahluwalia               21-Oct-15             Original version
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest (seeAllData=False) 
private class CPQ_Test_AgreementLine_Trigger{
   static User TestAEUser;
    static Account oAccount;
    static Opportunity oOpportunity;
    static Apttus_Proposal__Proposal__c oQuote;
    static Attachment attachmentQuote;
    static Apttus_Config2__PriceList__c oPriceList;
    static product2 oProduct;
    static Apttus__APTS_Agreement__c oAgreement;
    static List<Apttus__APTS_Agreement__c> lstAgreement;

    /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testMethod void createTestData() {
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');      
        system.runAs(TestAEUser){
            //Create PriceList
            oPriceList = CPQ_Test_Setup.newPriceList();
            insert oPriceList ;
            Apttus__DocAssemblyRuleset__c oDocRuleset = new Apttus__DocAssemblyRuleset__c();
            oDocRuleset.Apttus__Active__c = true;
            oDocRuleset.Apttus__Sequence__c = 1;
            oDocRuleset.name = 'Test Ruleset';
            Insert oDocRuleset;
            
            CPQ_ProductSchedule__c oProductSchedule = new CPQ_ProductSchedule__c();
            oProductSchedule.Doc_RuleSet__c = oDocRuleset.id;
            oProductSchedule.ProductScheduleName__c = 'Test Product Schedule';
            insert oProductSchedule;
            
            //Create Product 
            oProduct = CPQ_Test_Setup.newProduct();
            oProduct.ProductSchedule__c = oProductSchedule.id;
            oProduct.CSOW_required__c = true;
            insert oProduct;
            
            System.assert(oProduct.ProductSchedule__c != null);
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
            insert oQuote;
            //Create attachment for Quote
            attachmentQuote = CPQ_Test_Setup.newAttachment(oQuote.id);
            insert attachmentQuote;
            //Create Agreement
            oAgreement= CPQ_Test_Setup.newAgreement(oAccount, oOpportunity,oQuote);
            //Create List of Agreements
            lstAgreement= CPQ_Test_Setup.newlstAgreement(oAccount, oOpportunity,oQuote, 300);
        }
    }

    
    /*********************************************************************************
    Method Name    : testProductSchedule
    Description    : Method to test testProductSchedule defaulting in agreement line Trigger
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testProductSchedule(){
        createTestData();
        oQuote.Primary_Quote_Document_ID__c = attachmentQuote.id;
        update oQuote;
        system.runAs(TestAEUser){
            test.startTest();
            lstAgreement= CPQ_Test_Setup.newlstAgreement(oAccount, oOpportunity,oQuote, 300);
            insert lstAgreement;
            for(Apttus__APTS_Agreement__c agreement : lstAgreement){
                agreement.Apttus__Contract_End_Date__c = system.today().addDays(365);
                agreement.Language__c = 'English';
            }
            List<Apttus__AgreementLineItem__c> lstAgreementLines = new List<Apttus__AgreementLineItem__c>();
            lstAgreementLines = CPQ_Test_Setup.newlstAgreementLine(lstAgreement[0], 300);
            
            for(Apttus__AgreementLineItem__c agreementLine : lstAgreementLines){
                agreementLine.Apttus__ProductId__c = oProduct.id;
                
            }
            insert lstAgreementLines;
            
            list<Id> ListagreementLineIds = new List<Id>();
            set<Id> setagreementIds = new Set<Id>();
            for(Apttus__AgreementLineItem__c agreementLine : lstAgreementLines){
                ListagreementLineIds.add(agreementLine.id);
                setagreementIds.add(agreementLine.Apttus__AgreementId__c );
            }
            
            List<Apttus__AgreementLineItem__c> LstInsertedAgreementLines = [select id, Apttus__ProductId__c, Prod_Schedule__c, InclProdSchdl__c from Apttus__AgreementLineItem__c where id in : ListagreementLineIds ];
            // This integer counter is Used to test if one and Only one Agreement Line items has the Product Schedule Inculded Flag as Yes.
            Integer iProdSceduleIncludedCount = 0;
            for(Apttus__AgreementLineItem__c agreementLine : LstInsertedAgreementLines){
                system.assert(agreementLine.id != null);
                system.assert(agreementLine.Apttus__ProductId__c != null);
                system.assert(agreementLine.Prod_Schedule__c != null);
                If(agreementLine.InclProdSchdl__c == 'Yes'){
                    iProdSceduleIncludedCount ++;
                }
                
                
            }
           
            // CHecking if only one agreement Line has the Product Schedule Included Flag as Yes
            system.assert(iProdSceduleIncludedCount == 1);
            List<Apttus__APTS_Agreement__c> listUpdatedAgreement = [select id, ProdSchdlsIncl__c from Apttus__APTS_Agreement__c where id in : setagreementIds];
            for(Apttus__APTS_Agreement__c updatedAgreement : listUpdatedAgreement ){
                system.assert(updatedAgreement .ProdSchdlsIncl__c == 'Test Product Schedule');
            }
            Test.stopTest();
        }
    }  
}