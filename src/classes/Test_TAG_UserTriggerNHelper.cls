/*
===============================================================================
 Class Name   : Test_TAG_UserTriggerNHelper
===============================================================================
PURPOSE:    This is a Test class for UserTrigger and userTriggerHelper.
              
Developer: Aakanksha Patel
Date: 30/04/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
30/04/2015           Aakanksha               Original Version
===============================================================================*/
@isTest (SeeAllData= true)
private class Test_TAG_UserTriggerNHelper{
     private static Account ParentAcc;
     private static  Assignment_Request__c  AccountAR;
     private static  Assignment_Request_Item__c AccountARI;
     private static  Assignment_Request_Item__c AccountARI1;
     
  //   private static OrgWideEmailAddress orgEmailId=[select id, Address, DisplayName from OrgWideEmailAddress limit 1] ;
  //   private static OrgWideEmailAddress orgw;
     private static Profile pEmp = [Select Id from Profile where Name Like '%System Administrator%' limit 1];
     private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
     private static User userRec;
     private static User userRec2;
    
     //create Test Data
     private static void setUpData(){        
        userRec = new User(LastName = 'Mark O’BrienRoger', Owner_Type__c = 'District',Channel__c = 'Channel_test', Assignment_Approver__c = UserInfo.getUserId() ,Alias = 'alRoger', Email='test@Rogertest.com', Username='test66@Rogertest.com', CommunityNickname = 'nickRoger666', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec;
        userRec2 = new User(LastName = 'Mark O’BrienRoger2',isActive=true,Owner_Type__c ='Account',Assignment_Approver__c = userRec.id ,Channel__c = 'Channel_test' , Alias = 'aRoger', Email='test@Rogertest.com', Username='test67@Rogertest2.com', CommunityNickname = 'nickRoger667', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec2;
        
        ParentAcc= new Account(Name='ParentAcc');
        ParentAcc.Account_Status__c = 'Pending';
        ParentAcc.ParentID= null;
        ParentAcc.Initial_Account_Executive__c = userRec2.id;
        ParentAcc.OwnerId = userRec2.Id;
        ParentAcc.BillingPostalCode= 'A1A 1A1';
        insert ParentAcc;
        
        AccountAR = new Assignment_Request__c();
        AccountAR.OwnerID = userRec2.id;
        insert AccountAR ;
        
        AccountARI= new Assignment_Request_Item__c();
        AccountARI.Account__c = ParentAcc.Id;
        AccountARI.New_Owner__c = userRec2.id;
        AccountARI.Business_Case__c = 'Business_Trigger';
        AccountARI.Reason_code__c = '4 - New Account Creation';
        AccountARI.Assignment_Request__c = AccountAR.Id;
        AccountARI.New_Approver__c = userRec2.Assignment_Approver__c;
        AccountARI.New_Channel__c = 'Channel_test';
        AccountARI.Requested_Item_Type__c='Account Owner';
        AccountARI.Status__c ='Submitted';
        insert AccountARI;
        
        AccountARI1= new Assignment_Request_Item__c();
        AccountARI1.Account__c = ParentAcc.Id;
        AccountARI1.New_Owner__c = userRec2.id;
        AccountARI1.Business_Case__c = 'Business_Trigger';
        AccountARI1.Reason_code__c = '4 - New Account Creation';
        AccountARI1.Assignment_Request__c = AccountAR.Id;
        AccountARI1.New_Approver__c = userRec2.Assignment_Approver__c;
        AccountARI1.New_Channel__c = 'Channel_test';
        AccountARI1.Requested_Item_Type__c='Account Owner';
        AccountARI1.Status__c ='Approved';
        insert AccountARI1;
    }
    /*******************************************************************************************************
    * @description: This is a positive test method to test the User trigger
    *******************************************************************************************************/
    static testmethod void testUserTrigger(){
        setUpData();
        System.RunAs(userRec){
            Test.startTest();
            userRec2.isActive = false;
            update userRec2;
            Test.stopTest();
        }
     }
}