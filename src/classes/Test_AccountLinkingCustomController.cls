@isTest (seeAllData=true) 
private class Test_AccountLinkingCustomController{
    private static Account ParentAcc;
    private static Account ChildAcc;
    private static Account ChildAcc1;
    private static Account ChildAcc2;
    private static Account ChildAcc3;
    private static Case caseTestObj;
    private static Mass_Update_Details__c mObj;
    static ID caseDelinkRecordTypeId = [select Id from recordType where name='Account link' and sObjecttype ='Case'].id;
    static User userRec = [select id, ManagerId  from User where id=:UserInfo.getUserId()];
    static User userAdmn = [select id, ManagerId  from User where isActive = true limit 1];
    static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
    private static void setUpData(){
       
        ParentAcc= new Account(Name='AccountLinking');
        ParentAcc.BillingCountry= 'CA';
        ParentAcc.BillingPostalCode = 'A9A 9A9';
        ParentAcc.BillingState = 'MA';
        ParentAcc.BillingCity='City';
        ParentAcc.BillingStreet='Street';
        ParentAcc.Account_Status__c = 'Assigned';
        ParentAcc.OwnerId = userRec.id;
        insert ParentAcc;
        
        ChildAcc= new Account(Name='testAccAccountLinking');
        ChildAcc.BillingCountry= 'CA';
        ChildAcc.BillingPostalCode = 'A9A 9A9';
        ChildAcc.BillingState = 'MA';
        ChildAcc.BillingCity='City';
        ChildAcc.BillingStreet='Street';
        ChildAcc.Account_Status__c = 'Assigned';
        ChildAcc.OwnerId = userRec.id;
        insert ChildAcc;
        
        ChildAcc1= new Account(Name='FirstchildAcc');
        ChildAcc1.BillingCountry= 'CA';
        ChildAcc1.BillingPostalCode = 'A9A 9A9';
        ChildAcc1.BillingState = 'MA';
        ChildAcc1.BillingCity='City';
        ChildAcc1.BillingStreet='Street';
        ChildAcc1.Account_Status__c = 'Assigned';
        ChildAcc.OwnerId = userRec.id;
        insert ChildAcc1;
        
        ChildAcc2= new Account(Name='SecondChildAcc');
        ChildAcc2.BillingCountry= 'CA';
        ChildAcc2.BillingPostalCode = 'A9A 9A9';
        ChildAcc2.BillingState = 'MA';
        ChildAcc2.BillingCity='City';
        ChildAcc2.BillingStreet='Street';
        ChildAcc2.Account_Status__c = 'Assigned';
        ChildAcc2.OwnerId = userRec.id;
        insert ChildAcc2;
        
        ChildAcc3= new Account(Name='ThirdChildAcc');
        ChildAcc3.BillingCountry= 'CA';
        ChildAcc3.BillingPostalCode = 'A9A 9A9';
        ChildAcc3.BillingState = 'MA';
        ChildAcc3.BillingCity='City';
        ChildAcc3.BillingStreet='Street';
        ChildAcc3.Account_Status__c = 'Assigned';
        ChildAcc3.OwnerId = userRec.id;
        insert ChildAcc3;
        
        
        caseTestObj = new Case();
        
        Schema.DescribeFieldResult resultProgressReason= Case.In_Progress_Reason__c.getDescribe();        
        caseTestObj.In_Progress_Reason__c = (String)(TestingUtil.getPicklistDefaultValue(resultProgressReason));
        caseTestObj.Date__c = Date.today();
        /**/        
        System.debug('***setUpData: In Progress Reason : ' + caseTestObj.In_Progress_Reason__c);
        System.debug('***setUpData: Date (custom): ' + caseTestObj.Date__c);
        /**/        
                
    }
     static testmethod void testCaseInsertNew(){
        setUpData();  
        caseTestObj.RecordTypeId = caseDelinkRecordTypeId;
        caseTestObj.Status = 'In Progress';
        insert caseTestObj;
        Approval.ProcessSubmitRequest req1;
        Approval.ProcessResult result;
        req1 = new Approval.ProcessSubmitRequest();
        req1.SetComments('Comments');
        req1.setObjectId(caseTestObj.id);
        result = Approval.process(req1);
       
       
       caseTestObj.Status = 'Pending';
        test.startTest();
        
        Apexpages.currentPage().getParameters().put('loc','500');
        Apexpages.currentPage().getParameters().put('Type','New');
        ApexPages.StandardController controller = new ApexPages.StandardController(caseTestObj); 
        AccountLinkingCustomController cls = new AccountLinkingCustomController(controller );
        cls.getSortDirection();
        cls.sortExpression ='name';
        cls.Previous();
        cls.Beginning();
        cls.Next();
        cls.getDisablePrevious();
        cls.getDisableNext();
        cls.getTotal_size();
        cls.getTotal_size();
        test.stopTest();   
         
     }
     static testmethod void testCaseInsertWithAccountNew(){
        setUpData();        
        test.startTest();
        Apexpages.currentPage().getParameters().put('loc','500');
        Apexpages.currentPage().getParameters().put('Type','New');
        Apexpages.currentPage().getParameters().put('parentAcc',ParentAcc.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(caseTestObj); 
        AccountLinkingCustomController cls = new AccountLinkingCustomController(controller ); 
        AccountLinkingCustomController.accRelCheck ack= new AccountLinkingCustomController.accRelCheck();
        ack.a=ChildAcc1;
        ack.acheck=True;
        cls.accCheckWrap.Add(ack);
        
        AccountLinkingCustomController.accRelCheck ack1= new AccountLinkingCustomController.accRelCheck();
        ack1.a=ChildAcc2;
        ack1.acheck=True;
        cls.accCheckWrap.Add(ack1);
        cls.addTolst();
        
        ack1.a=ChildAcc2;
        ack1.acheck=true;
        cls.accSelectedCheckWrap.Add(ack1);
        cls.removeItemfromlst();
        
        cls.addTolst();
        cls.saveLst();
        cls.cancel();
        test.stopTest();   
         
     }
     
     static testmethod void testCaseInsertWithAccountNewSubmit(){
        setUpData();        
        test.startTest();
        Apexpages.currentPage().getParameters().put('loc','500');
        Apexpages.currentPage().getParameters().put('Type','New');
        Apexpages.currentPage().getParameters().put('parentAcc',ParentAcc.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(caseTestObj); 
        AccountLinkingCustomController cls = new AccountLinkingCustomController(controller ); 
        AccountLinkingCustomController.accRelCheck ack= new AccountLinkingCustomController.accRelCheck();
        ack.a=ChildAcc1;
        ack.acheck=True;
        cls.accCheckWrap.Add(ack);
        cls.addTolst();
        cls.saveAndSubmitLst();
        test.stopTest();   
         
     }
     
     static testmethod void testCaseInsertDetail(){
        setUpData();        
        test.startTest();
        
        /**/        
        System.debug('***testCaseInsertDetail: In Progress Reason : ' + caseTestObj.In_Progress_Reason__c);
        System.debug('***testCaseInsertDetail: Date (custom): ' + caseTestObj .Date__c);
        /**/        
        
        caseTestObj.AccountID =ParentAcc.id;
        caseTestObj.Mass_Update_Parent_Account__c = ParentAcc.id;
        caseTestObj.RecordTypeId = caseDelinkRecordTypeId;
        caseTestObj.Account_Owner__c=userRec.id;
        caseTestObj.Parent_Account_Owner__c = userRec.id;
        caseTestObj.Parent_Account_Owner_Manager__c=userRec.ManagerID;
        insert caseTestObj;  
        
        mObj = new Mass_Update_Details__c();
        mObj.Case__c = caseTestObj.id;
        mObj.Child_Account__c = ChildAcc.id;
        insert mObj;

        Apexpages.currentPage().getParameters().put('loc','500');
        Apexpages.currentPage().getParameters().put('Type','New');
        Apexpages.currentPage().getParameters().put('parentAcc',ParentAcc.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(caseTestObj); 
        AccountLinkingCustomController cls = new AccountLinkingCustomController(controller ); 
        AccountLinkingCustomController.accRelCheck ack= new AccountLinkingCustomController.accRelCheck();
        ack.a=ChildAcc1;
        ack.acheck=True;
        cls.accCheckWrap.Add(ack);
        cls.addTolst();
        cls.saveAndSubmitLst();
        cls.ApproveRequest();
        cls.EditDetails();
        cls.SubmitforApprovalDetails();
        cls.OpenAccount();
        cls.newSearchAccounts();
        test.stopTest();   
         
     }
     static testmethod void testCaseInsertEdit(){
        setUpData();        
        test.startTest();
        caseTestObj.AccountID =ParentAcc.id;
        caseTestObj.Mass_Update_Parent_Account__c = ParentAcc.id;
        caseTestObj.RecordTypeId = caseDelinkRecordTypeId;
        insert caseTestObj;  
        
        mObj = new Mass_Update_Details__c();
        mObj.Case__c = caseTestObj.id;
        mObj.Child_Account__c = ChildAcc.id;
        insert mObj;
          
        Apexpages.currentPage().getParameters().put('loc','500');
        Apexpages.currentPage().getParameters().put('Type','Edit');
        Apexpages.currentPage().getParameters().put('ID',caseTestObj.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(caseTestObj); 
       AccountLinkingCustomController cls = new AccountLinkingCustomController(controller ); 
        test.stopTest();   
         
     }
     static testmethod void testCaseInsertDetail2(){
        setUpData();        
        test.startTest();
        caseTestObj.AccountID =ParentAcc.id;
        caseTestObj.Mass_Update_Parent_Account__c = ParentAcc.id;
        caseTestObj.RecordTypeId = caseDelinkRecordTypeId;
        insert caseTestObj;  
        
        mObj = new Mass_Update_Details__c();
        mObj.Case__c = caseTestObj.id;
        mObj.Child_Account__c = ChildAcc.id;
        insert mObj;
          
        Apexpages.currentPage().getParameters().put('loc','500');
        Apexpages.currentPage().getParameters().put('Type','Detail');
        Apexpages.currentPage().getParameters().put('ID',caseTestObj.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(caseTestObj); 
        AccountLinkingCustomController cls = new AccountLinkingCustomController(controller ); 
        cls.deleteCase();

        test.stopTest();   
         
     }

 }