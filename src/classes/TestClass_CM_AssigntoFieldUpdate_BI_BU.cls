/*Class Name  : TestClass_CM_AssigntoFieldUpdate_BI.
 *Description : This is test class for CampaignMember_AssigntoFieldUpdate_BI.
 *Created By  : Rajiv Gangra.
 *Created Date :29/08/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
* Modified : Aakanksha Patel(March 2015) : For TAG
*/
@isTest
private class TestClass_CM_AssigntoFieldUpdate_BI_BU {
    private static Account accTestObj;
    private static Contact conTestObj;
    private static Lead leadTestObj;
    private static CampaignMember cmmTestObj;
    private static Campaign cmpTestObj;
    private static user objUser;
     private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();  
   // Create data for testing purpose
    private static void setUpData(){
        Profile profile = [Select Id From Profile Where name = 'System Administrator' Limit 1];
        objUser = [SELECT Id,IsActive,ProfileId FROM User WHERE ProfileId =: profile.id AND IsActive = True limit 1];


 TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;


       accTestObj = new Account(Name='testAcc');
       accTestObj.BillingCity='City';
        accTestObj.BillingStreet='Street';
         accTestObj.BillingCountry= 'CA';
        accTestObj.BillingPostalCode = 'A9A 9A9';
        accTestObj.BillingState = 'MA';
       accTestObj.Account_Status__c= 'Assigned';
        accTestObj.ParentId = null;
       insert accTestObj;

       conTestObj = new Contact(FirstName='testCon',LastName='test',MailingCountry = 'CA',MailingState = 'AB',MailingPostalCode = 'A9A 9A9', Accountid=accTestObj.id, Phone='123', Contact_type__c ='other');
       
       leadTestObj = new Lead(FirstName='testLead',LastName='test',Company=accTestObj.id, Status='Open',Lead_Type__c='Wireless');
       
       cmpTestObj = new Campaign(Name='teastCmp');
       insert cmpTestObj;
       
       cmmTestObj = new CampaignMember(); 
       cmmTestObj.CampaignID=cmpTestObj.id;
       
       Schema.DescribeFieldResult fieldResult = CampaignMember.Status.getDescribe();
       List<Schema.PicklistEntry> pickListValues = fieldResult.getPicklistValues();
       if(pickListValues.size() > 0) {
            cmmTestObj.Status = pickListValues[0].getValue();
       }
             

    }
    static testmethod void myUnitWithLeadTest(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            insert leadTestObj;
            cmmTestObj.leadId=leadTestObj.id;
            insert cmmTestObj;
            String updatedFieldValue=[select id,Assigned_To__c from CampaignMember where Id=:cmmTestObj.id].Assigned_To__c;
            //system.assertequals(updatedFieldValue,objUser.id);
            Test.stoptest();
        }
    }
    static testmethod void myUnitWithContactTest(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            insert conTestObj;
            cmmTestObj.contactId=conTestObj.id;
            insert cmmTestObj;
            Test.stoptest();
        }
    } 
    static testmethod void myUnitWithContactWithAssigneToTest(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            insert conTestObj;
            cmmTestObj.contactId=conTestObj.id;
            cmmTestObj.Assigned_To__c=objUser.id;
            insert cmmTestObj;
            CampaignMember updatedFieldValue=[select id,Assigned_To__c,Uploaded_ID_Error__c,Uploaded_Assigned_To_ID__c from CampaignMember where Id=:cmmTestObj.id];
           // system.assertequals(updatedFieldValue.Uploaded_Assigned_To_ID__c,objUser.id);
           // system.assertequals(updatedFieldValue.Uploaded_ID_Error__c,false);
            cmmTestObj.Assigned_To__c=null;
            update cmmTestObj;
            CampaignMember updatedFieldValue1=[select id,Assigned_To__c,Uploaded_ID_Error__c,Uploaded_Assigned_To_ID__c from CampaignMember where Id=:cmmTestObj.id];
           // system.assertequals(updatedFieldValue1.Uploaded_Assigned_To_ID__c,null);
//system.assertequals(updatedFieldValue1.Uploaded_ID_Error__c,false);
            Test.stoptest();
        }
    } 
    static testmethod void myUnitWithContactAndAssignIDTest(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            insert conTestObj;
            cmmTestObj.contactId=conTestObj.id;
            cmmTestObj.Uploaded_Assigned_To_ID__c=objUser.id;
            insert cmmTestObj;
            CampaignMember updatedFieldValue=[select id,Assigned_To__c,Uploaded_ID_Error__c from CampaignMember where Id=:cmmTestObj.id];
            //system.assertequals(updatedFieldValue.Assigned_To__c,objUser.id);
            //system.assertequals(updatedFieldValue.Uploaded_ID_Error__c,false);
            Test.stoptest();
        }
    }
    static testmethod void myUnitNegAssignIDTest(){
        setUpData();
        insert conTestObj;
        System.runAs(objUser) {
            Test.starttest();
            cmmTestObj.contactId=conTestObj.id;
            cmmTestObj.Uploaded_Assigned_To_ID__c='123456789123456';
            insert cmmTestObj;
            CampaignMember updatedFieldValue=[select id,Assigned_To__c,Uploaded_ID_Error__c from CampaignMember where Id=:cmmTestObj.id];
            //system.assertnotequals(updatedFieldValue.Assigned_To__c,objUser.id);
            //system.assertequals(updatedFieldValue.Uploaded_ID_Error__c,true);
            Test.stoptest();
        }
    }    
}