/*Class Name :AccountLinkingCustomController.
 *Description : Controller Class for AccountLinkingTemplate, Contains all the Methods used in tha Page.
 *Created By : Rajiv Gangra.
 *Created Date :05/12/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
public class AccountLinkingCustomController {
    public boolean IsNew{get;set;}
    public boolean IsDetail{get;set;}
    public boolean IsEdit{get;set;}
    public boolean EnableEdit{get;set;}
    public boolean EnableApproval{get;set;}
    public Account accRecord{get;set;}
    public static string searchString{get;set;}
    public static List<Account> lstNotRelatedAcccounts{get;set;}
    public List<accRelCheck>   accCheckWrap{get;set;}
    public List<accRelCheck>   accSelectedCheckWrap{get;set;}
    public List<accRelCheck>   accSelectedCheckWrapDetails{get;set;}
    public Case cObj{get;set;}
    public List<Case> listCaseObj{get; set;}//
    public List<string> dispButton{get;set;}
    public set<id> lstIdSelected{get;set;}
    public set<id> lstIdtoRemove{get;set;}
    public String location{get;set;}
    public String accID{get;set;}
    public Boolean dispRelatedAcc{get;set;}
    public String totalSearchResults{get;set;}
    public Boolean serachButtonActive{get;set;}
    public Boolean block1disp{get;set;}
    public Boolean block2disp{get;set;}
    public boolean showDeleteonDetail{get;set;}// To show delete button on certain conditions
    public boolean shwSubmitForApproval{get;set;}//Show Submit for Approval button to Case Owner, Sys admin and CRM admin
    public Boolean noRelatedAccDisp{get;set;}
    public List<Mass_Update_Details__c> lstMassUpdate;
    public String ActiveMSDParent{get;set;}
    public Account cObjParentAcc{get;set;}
    //Number of records to Skip
    private integer counterEnd;
    //Limit to Be set
    private integer counterBegin;
    //pages crossed
    public boolean shwSubmitForApprovalMsg{get;set;}//Show Submit for Approval button to Case Owner, Sys admin and CRM admin
    public String profileId = UserInfo.getProfileId();    
    public Profile profile = [select name from profile where id= :profileId];
    public String formType{get;set;}
    public String relatedCase{get;set;}
    public ID caseDlinkRecordTypeId = [select Id from recordType where name='Account link' and sObjecttype ='Case'].id;
    public String sortDirection = 'ASC';
    public String sortExp = 'name';
    public String selectedAccountID {get;set;}
    public boolean AccLimitExceed {get;set;}
    
    private boolean caseExists() {
        List<Case> cases = [select id from Case where Id=:relatedCase];
        if (cases.size() > 0) {
            return true;
        }
        return false;
    }
    
    public String sortExpression{
        get{ return sortExp; }
        set{ //if the column is clicked on then switch between Ascending and Descending modes
            if (value == sortExp)
                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
            else
                sortDirection = 'ASC';
                sortExp = value;
        }
    }
    public String getSortDirection(){
        //if column is not selected 
        if (sortExpression == null || sortExpression == '')
            return 'ASC';
        else
            return sortDirection;
    }
    public void setSortDirection(String value){  
        sortDirection = value;
    }
        
    public string sortFullExp = sortExpression  + ' ' + sortDirection;
    public AccountLinkingCustomController(ApexPages.StandardController controller) {
            accRecord = new Account();
            serachButtonActive=true;
            accCheckWrap= new List<accRelCheck>();
            accSelectedCheckWrap= new List<accRelCheck>();
            dispButton= new list<string>();
            lstIdSelected= new set<id>();
            cObj = new Case();
            noRelatedAccDisp=false;
            location= ApexPages.currentPage().getParameters().get('loc');
            formType= ApexPages.currentPage().getParameters().get('type');
            total_Pages=0;
            relatedCase= ApexPages.currentPage().getParameters().get('ID');
            boolean error = false;
            //Decide the form type to display
            if(formType=='New'){
                IsNew=true;
            }else{
                IsNew=False;
            }

            if(relatedCase!='' && relatedCase!= null && relatedCase.startswith('500')){      
                try {
                    listCaseObj = [select id, Mass_Update_Parent_Account__c,Notes__c,subject, CreatedById, CaseNumber,OwnerID, status from Case where isDeleted = false and id=:relatedCase];
                }
                catch (System.QueryException e) {
                    error = true;  
                }
                if (listCaseObj.size() < 1) {
                    error = true; 
                }
                
            }
            if(!error) {
         
                if(formType=='detail'){
                    accSelectedCheckWrapDetails= new List<accRelCheck>();
                    set<id> lstAccInUpdateStage= new set<id>();
                    relatedCase= ApexPages.currentPage().getParameters().get('ID');
                    location=relatedCase;
                    if(relatedCase !=null && relatedCase !=''){
                        cObj=[select id,OwnerId,Account.Name,Parent_Account_Owner__r.Name, subject, CaseNumber, CreatedBy.Name, Status,Notes__c,Account_Owner_Manager__r.Name, Account.OwnerID, Mass_Update_Parent_Account__c,Mass_Update_Parent_Account__r.id, Mass_Update_Parent_Account__r.Customer_Status__c, Mass_Update_Parent_Account__r.ownerId from Case where Id=:relatedCase];
                        IsDetail=true;
                        if(cObj.Mass_Update_Parent_Account__c !=null ){
                            cObjParentAcc = [select Owner.Channel__c,Customer_Status__c, (SELECT MSD_Code_Status__c FROM MSD_Codes__r where MSD_Code_Status__c='Active' limit 1) from Account where id =: cObj.Mass_Update_Parent_Account__r.id];
                            if(cObjParentAcc.MSD_Codes__r.size()>0){
                                ActiveMSDParent = 'Yes';
                            }
                            else
                                ActiveMSDParent = 'No';
                        }
                        // Get Mass_Update_Details__c Related to List of Account Queried
                        list<Mass_Update_Details__c> lstMUDRel=[select id,Name,Child_Account__c,Case__r.status,Case__r.CaseNumber from Mass_Update_Details__c where Case__c=:relatedCase];
                        for(Mass_Update_Details__c MUD:lstMUDRel){
                            lstAccInUpdateStage.add(MUD.Child_Account__c); 
                        }
                        if(profile.name=='System Administrator' && cObj.Status!='New'  ){
                                showDeleteonDetail = true;
                        }
                    
                        if(cObj.Status=='New' && (profile.name=='System Administrator' || profile.name=='EBU - Rogers CRM Admin' || cObj.OwnerId == UserInfo.getUserId())){
                            EnableEdit=true; 
                            shwSubmitForApproval=true;
                        }
                        else if(cObj.Status=='In Progress' && (profile.name=='System Administrator' || profile.name=='EBU - Rogers CRM Admin')){
                            EnableEdit=true; 
                        }
                        
                        //Get Approver Details
                        try{
                            ProcessInstanceWorkitem PIW=[SELECT ActorId,Actor.Type,CreatedById,CreatedDate,Id,IsDeleted,OriginalActorId,ProcessInstance.TargetObjectId,SystemModstamp,ProcessInstance.Status FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =: cObj.id];
                                if(PIW.ProcessInstance.Status=='Pending'){
                                    List<User> uList=[SELECT Id,Profile.name,Name FROM User where Profile.name='System Administrator' OR Profile.name='EBU - Rogers CRM Admin'];
                                    set<string> allowedUserList= new set<String>();
                                    for(user u:uList){
                                        allowedUserList.add(u.id);
                                    }
                                    if((PIW.Actor.Type !='Queue' && UserInfo.getUserId()==PIW.ActorId) || allowedUserList.contains(UserInfo.getUserId())){
                                       EnableApproval=true; 
                                    }else if(PIW.Actor.Type =='Queue'){
                                    // If queue then get the list of members related to queue
                                       set<id> lstQueueMembers= new set<id>();
                                       list<GroupMember> gm=[SELECT GroupId,Id,SystemModstamp,UserOrGroupId FROM GroupMember WHERE GroupId =:PIW.ActorId];
                                       for(GroupMember gItem:gm){
                                           lstQueueMembers.add(gItem.UserOrGroupId);
                                       }
                                       // Either member should be part of queue /System Admin / EBU - Rogers CRM Admin
                                       if(allowedUserList.contains(UserInfo.getUserId()) || lstQueueMembers.contains(UserInfo.getUserId())){
                                           EnableApproval=True;
                                       }else{
                                           EnableApproval=False;
                                       } 
                                    }else{
                                        EnableApproval=False;
                                    }
                                }
                        }catch(exception ex){
                        }
    
                    }
                    if(lstAccInUpdateStage !=null && lstAccInUpdateStage.size()>0){
                        List<Account> lstRelatedAcc=[SELECT Id,Name,Account_Status__c,Internal_Duns__c,ParentID,Trade_Name__c,Owner.Name,Owner.Channel__c,Customer_Status__c,Parent.Internal_Duns__c, (SELECT MSD_Code_Status__c FROM MSD_Codes__r where MSD_Code_Status__c='Active' limit 1) FROM Account where id IN:lstAccInUpdateStage];
                        for(account a:lstRelatedAcc){
                            accRelCheck ack = new accRelCheck();
                            ack.a=a;
                            ack.acheck=False;
                            if(a.MSD_Codes__r.size()>0){
                                ack.activeMSD='Yes';
                            }else{
                                ack.activeMSD='No';
                            }
                            accSelectedCheckWrapDetails.add(ack);
                        }
                    }
                }else{
                    IsDetail=False;
                }
                // if edit
                if(formType=='Edit'){
                    accSelectedCheckWrapDetails= new List<accRelCheck>();
                    set<id> lstAccInUpdateStage= new set<id>();
                    string relatedCase= ApexPages.currentPage().getParameters().get('ID');
                    location=relatedCase;
                    if(relatedCase !=null && relatedCase !=''){
                        IsEdit=True;
                        serachButtonActive=false;
                        cObj=[select id,Account.Name,Parent_Account_Owner__r.Name,subject,CaseNumber,CreatedBy.Name,Status,Notes__c,Account_Owner_Manager__r.Name,Account.OwnerID,Mass_Update_Parent_Account__c from Case where Id=:relatedCase];
                        accRecord.ParentID=cObj.Mass_Update_Parent_Account__c;
                        // Get Mass_Update_Details__c Related to List of Account Queried
                        list<Mass_Update_Details__c> lstMUDRel=[select id,Name,Child_Account__c,Case__r.status,Case__r.CaseNumber from Mass_Update_Details__c where Case__c=:relatedCase];
                        for(Mass_Update_Details__c MUD:lstMUDRel){
                            lstAccInUpdateStage.add(MUD.Child_Account__c); 
                        }
                        if(lstAccInUpdateStage !=null && lstAccInUpdateStage.size()>0){
                            block2disp=true;
                            List<Account> lstRelatedAcc=[SELECT Id,Name,Account_Status__c,Internal_Duns__c,ParentID,Trade_Name__c,Owner.Name,Owner.Channel__c,Customer_Status__c,Parent.Internal_Duns__c, (SELECT MSD_Code_Status__c FROM MSD_Codes__r where MSD_Code_Status__c='Active' limit 1) FROM Account where id IN:lstAccInUpdateStage];
                            for(account a:lstRelatedAcc){
                                accRelCheck ack = new accRelCheck();
                                ack.a=a;
                                ack.acheck=False;
                                if(a.MSD_Codes__r.size()>0){
                                    ack.activeMSD='Yes';
                                }else{
                                    ack.activeMSD='No';
                                }
                                accSelectedCheckWrap.add(ack);
                            }
                        }
                    }
                    
                }else{
                    IsEdit=False;
                }
                if(ApexPages.currentPage().getParameters().get('parentAcc') !=null && IsDetail==False && IsEdit==False){
                    accRecord.ParentId = ApexPages.currentPage().getParameters().get('parentAcc');
                    searchActive();
                    if(serachButtonActive==False){
                        SearchAccounts();
                    }
                }
                //Allow Submit For Approval button display when linking account directly from creating new Case
                else if(formType=='NewNoParentAcct'){
                    showSubmitForApproval(true);
                }
                
                //Set conditions for is Edit case
                if(IsEdit==true){
                    block1disp=true;   
                    dispRelatedAcc=true; 
                }
                
            }
    }
    public void searchActive() {
        try{
        Account a = [select id, Account_status__c from Account where id=:accRecord.ParentID];
        if(a.Account_status__c == 'Pending' || a.Account_status__c == 'New'){
        ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,label.This_Account_is_Pending));
        }
        //Allow Submit For Approval button display when linking account directly from creating new Case
        else if(formType=='NewNoParentAcct'){
            showSubmitForApproval(true);
        }
        else{
        block1disp=false;
        if(accRecord.ParentId!=null || accRecord.ParentId!=''){
            serachButtonActive=false;
        }
        }
        }catch(exception ex){
        }
    }
    
    /*
    * Control display of block containing Submit For Approval button for New Account Linking Cases as needed.
    */
    private void showSubmitForApproval(boolean isDisplayed){
            block1disp = isDisplayed;
            IsNew=isDisplayed;
    }
    
    public PageReference newSearchAccounts(){
        counter=0;  
        total_size=0; //used to show user the total size of the list
        total_Pages=0;
        current_Page=0;
        AccLimitExceed=False;
        SearchAccounts();
        return null;
    }
    public PageReference SearchAccounts() {
    if(!caseExists() && !IsNew) {
        return new PageReference('/apex/RecordDeleted');
    }
        accCheckWrap.clear();
        try{
            if( accRecord.ParentID !=null){
                if(searchString ==null ){
                    searchString ='';
                }
               // Adding Id's That should not be displayed in the current list
                lstIdSelected.add(accRecord.ParentID);
                List<Case> cRelAcc=[select id,RecordType.Name,Mass_Update_Parent_Account__c,Status from Case where Mass_Update_Parent_Account__c=:accRecord.ParentID AND RecordType.Name=:'Account Link' AND (status ='New' OR Status ='In Progress')];
                if(cRelAcc.size()>0 && formType=='New'){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,label.Pending_Account_Related));
                }else{
                    total_size=Database.countQuery('SELECT Count() FROM Account WHERE Name LIKE '+ '\''+'%' + searchString + '%'+ '\''+' AND ParentID !='+ '\''+accRecord.ParentID+ '\''+' AND id NOT IN :lstIdSelected LIMIT 2001');
                    if(total_size>2000){
                        total_size=2000; 
                        pageTotalRecord_Size='2000+';
                        AccLimitExceed=true;   
                    }else{
                        pageTotalRecord_Size=string.Valueof(total_size);
                    }
                    if(PageList_Size !=null){
                        list_size=Integer.ValueOf(PageList_Size);
                    }
                    set<id> lstAccid= new set<id>();
                    sortFullExp = sortExpression  + ' ' + sortDirection;
                    if(counter <=2000){
                    lstNotRelatedAcccounts=Database.query('SELECT Id,Account_Status__c,Name,Internal_Duns__c,ParentID,Trade_Name__c,Owner.Name,Owner.Channel__c,Parent.Name,Customer_Status__c,Parent.Internal_Duns__c,(SELECT MSD_Code_Status__c FROM MSD_Codes__r where MSD_Code_Status__c=\'Active\' limit 1) FROM Account WHERE (Name LIKE '+ '\''+'%' + searchString + '%'+ '\''+' OR Internal_Duns__c LIKE '+ '\''+'%' + searchString + '%'+ '\''+' OR Trade_Name__c LIKE '+ '\''+'%' + searchString + '%'+ '\''+') AND id NOT IN :lstIdSelected Order By '+ sortFullExp+' limit :list_size offset :counter' );
                    for(Account a:lstNotRelatedAcccounts){
                        lstAccid.add(a.id);    
                    }
                    total_Pages=(total_size/list_size)+1;
                    current_Page=(Counter/list_size)+1;
                    // Get Mass_Update_Details__c Related to List of Account Queried
                    set<id> lstAccInUpdateStage= new set<id>();
                    Map<id,string> mapMUDCase= new Map<id,string>();
                    list<Mass_Update_Details__c> lstMUDRel=[select id,Name,Child_Account__c,Case__r.status,Case__r.CaseNumber from Mass_Update_Details__c where Child_Account__c IN:lstAccid AND Case__r.status!='approved' AND Case__r.status!='rejected'];
                    for(Mass_Update_Details__c MUD:lstMUDRel){
                        lstAccInUpdateStage.add(MUD.Child_Account__c); 
                        mapMUDCase.put(MUD.Child_Account__c,MUD.Case__r.CaseNumber);   
                    }
                    if(lstNotRelatedAcccounts !=null && lstNotRelatedAcccounts.size()!=0){
                        block1disp=true;
                        for(account a:lstNotRelatedAcccounts){
                            accRelCheck ack = new accRelCheck();
                            ack.a=a;
                            ack.acheck=False;
                            if(a.MSD_Codes__r.size()>0){
                                ack.activeMSD='Yes';
                            }else{
                                ack.activeMSD='No';
                            }
                            if(a.ParentID ==accRecord.ParentID){
                                ack.Notes=Label.Already_a_children_account;
                            }else if(a.ParentID !=null){
                                if(a.Parent.Internal_Duns__c!=null){
                                   ack.Notes=Label.Link_to_DUNS+ ' '+ a.Parent.Internal_Duns__c+Label.Please_delink;

                                }
                                else{
                                    ack.Notes=Label.Linked_to_Account + '' +a.Parent.Name+Label.Please_delink; 
                                }
                             
                            }else if(lstAccInUpdateStage.Contains(a.id)){
                                ack.Notes=Label.Case+ ' '+ mapMUDCase.get(a.id)+' '+Label.is_pending_with_this_account;
                            }else if(a.Account_Status__c =='Pending' || a.Account_Status__c =='New'){
                                ack.Notes=Label.Pending_account;
                            }
                            accCheckWrap.add(ack);
                        }
                    }
                    }else{
                        ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Please redefine your search'));
                    }
                }
                
            }else if(accRecord.ParentID ==null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,Label.Please_select_a_Valid_Account));
            }
        }catch(exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,label.The_Following_Error_was_Found+ex));
        }
        return null;
    }
    /**
    Add Items to the List
    */  
    public PageReference addTolst(){
    if(!caseExists() && !IsNew) {
        return new PageReference('/apex/RecordDeleted');
    }
        for(accRelCheck ac:accCheckWrap){
            if(ac.acheck == true){
                ac.acheck=False;
                accSelectedCheckWrap.add(ac);
                lstIdSelected.add(ac.a.id);
                block2disp=true;
            }
        }
        SearchAccounts();
        return null;
    }
    /**
    remove Items from List
    */  
    public PageReference removeItemfromlst(){
    if(!caseExists() && !IsNew) {
        return new PageReference('/apex/RecordDeleted');
    }
        List<accRelCheck> accSelectedCheckWrapnew = new List<accRelCheck>();
        for(accRelCheck ac:accSelectedCheckWrap){
            if(ac.acheck == true){
                //TODO remove Item form the list
                ac.acheck=False;
                lstIdSelected.remove(ac.a.id);
            }else {
                accSelectedCheckWrapnew.add(ac);
            }
        }
        accSelectedCheckWrap.clear();
        accSelectedCheckWrap.addall(accSelectedCheckWrapnew);
        if(accSelectedCheckWrap.size() <1){
            block2disp=false;
        }
        SearchAccounts();
        return null;
    }
    /**
    Save the Current Set of data in and Upadate current Account with Case 
    */
    public pagereference saveLst(){
    if(!caseExists() && !IsNew) {
        return new PageReference('/apex/RecordDeleted');
    }
        if(formType=='Edit'){
            list<Mass_Update_Details__c> lstMUDRel=[select id,Name,Child_Account__c,Case__r.status,Case__r.CaseNumber from Mass_Update_Details__c where Case__c=:cObj.id];
            delete lstMUDRel;
        }
        list<Account> updateAccount= new list<Account>();
        if(accSelectedCheckWrap !=null && accSelectedCheckWrap.size()!=0){
            Account aParentRec= [select id,Account_Status__c,OwnerID,Customer_Status__c,Owner.Channel__c,Owner.Name, Owner.ManagerID,DunsNumber,Trade_Name__c, name,Owner.Manager.isActive from Account where id =: accRecord.ParentID];
            
            cObj.AccountID = accRecord.ParentID;
            cObj.Mass_Update_Parent_Account__c = accRecord.ParentID;
            cObj.RecordTypeID=caseDlinkRecordTypeId;
            if(aParentRec.Owner.Manager.isActive==true){
                cObj.Account_Owner_Manager__c=aParentRec.Owner.ManagerID;
            }
            cObj.Account_Owner__c=aParentRec.OwnerID;
            cObj.Parent_Account_Owner__c = aParentRec.OwnerID;
            cObj.Parent_Account_Owner_Manager__c= aParentRec.Owner.ManagerID;
            cObj.OwnerID=UserInfo.GetUserID();
            upsert cObj;
            lstMassUpdate = new List<Mass_Update_Details__c>();
            for(accRelCheck ac:accSelectedCheckWrap){
                    Mass_Update_Details__c mObj = new Mass_Update_Details__c();
                    mObj.Case__c = cObj.id;
                    mObj.Child_Account__c = ac.a.id;
                    lstMassUpdate.add(mObj); 
            }
            if(lstMassUpdate.size()>0){
                try{
                    insert lstMassUpdate;
                    cObj.subject = Label.Account_Link_request +aParentRec.Name+ ' + '+ String.valueof(lstMassUpdate.size()) +Label.children;
                    update cObj; 
                }catch(exception e){
                }  
            }
        }else{
             ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,Label.No_Accounts_Selected));   
             return null;
        }
        if(cObj.id!=null){
            location='/'+cObj.id;
        }
        Pagereference page = new Pagereference('/'+location);
        return page ;
    }  
    
    /**
    Save the Current Set of data in and Upadate current Account with Case and Sumbit for Approval 
    */
    public pagereference saveAndSubmitLst(){
    if(!caseExists() && !IsNew) {
        return new PageReference('/apex/RecordDeleted');
    }
        if(formType=='Edit'){
            list<Mass_Update_Details__c> lstMUDRel=[select id,Name,Child_Account__c,Case__r.status,Case__r.CaseNumber from Mass_Update_Details__c where Case__c=:relatedCase];
            delete lstMUDRel;
        }
        list<Account> updateAccount= new list<Account>();
        if(accSelectedCheckWrap !=null && accSelectedCheckWrap.size()!=0){
            Account aParentRec= [select id, OwnerID,Customer_Status__c,Owner.Channel__c,Owner.Name, Owner.ManagerID,DunsNumber,Trade_Name__c, name,Owner.Manager.isActive from Account where id =: accRecord.ParentID];
             AssignmentRule AR = new AssignmentRule(); 
             AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1]; 
               //Creating the DMLOptions for "Assign using active assignment rules" checkbox 
                Database.DMLOptions dmlOpts = new Database.DMLOptions(); 
                dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id; 
                cObj.setOptions(dmlOpts); 
            cObj.AccountID = accRecord.ParentID;
            cObj.Mass_Update_Parent_Account__c = accRecord.ParentID;
            cObj.RecordTypeID=caseDlinkRecordTypeId;
            if(aParentRec.Owner.Manager.isActive==true){
                cObj.Account_Owner_Manager__c=aParentRec.Owner.ManagerID;
            }
            cObj.Account_Owner__c=aParentRec.OwnerID;
            cObj.Parent_Account_Owner__c = aParentRec.OwnerID;
            cObj.Parent_Account_Owner_Manager__c= aParentRec.Owner.ManagerID;
            upsert cObj;
            lstMassUpdate = new List<Mass_Update_Details__c>();
            for(accRelCheck ac:accSelectedCheckWrap){
                    Mass_Update_Details__c mObj = new Mass_Update_Details__c();
                    mObj.Case__c = cObj.id;
                    mObj.Child_Account__c = ac.a.id;
                    lstMassUpdate.add(mObj); 
            }
            if(lstMassUpdate.size()>0){
                try{
                    insert lstMassUpdate; 
                    cObj.subject = Label.Account_Link_request +aParentRec.Name+ ' + '+ String.valueof(lstMassUpdate.size()) +Label.children;
                    update cObj; 
                    Approval.ProcessSubmitRequest req1 = 
                    new Approval.ProcessSubmitRequest();
                    req1.setComments(Label.Submitting_request_for_approval);
                    req1.setObjectId(cObj.id);
                    // Submit the approval request for the account
                    Approval.ProcessResult result = Approval.process(req1);
                    shwSubmitForApprovalMsg = true;
                }catch(exception e){
                }  
            }
            // SearchAccounts();
         }else{
             ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'No Accounts Selected'));   
             return null;
         }
        if(cObj.id!=null){
            location='/'+cObj.id;
        }
        Pagereference page = new Pagereference('/'+location);
       return page ;
       //return null;
    }
    /**
    Cancel function
    */
    public pagereference cancel(){
        if(location ==null){
            location='/001/o';
        }
        Pagereference page = new Pagereference('/'+location);
        return page ;
    } 
    
    /**
    Approve function
    */
    public pagereference ApproveRequest(){
    if(!caseExists() && !IsNew) {
        return new PageReference('/apex/RecordDeleted');
    }
        Approval.ProcessWorkitemRequest req2 = 
        new Approval.ProcessWorkitemRequest();
        req2.setComments('Approving request.');
        req2.setAction('Approve');

        ProcessInstanceWorkitem PIW=[SELECT ActorId,Actor.Type,CreatedById,CreatedDate,Id,IsDeleted,OriginalActorId,ProcessInstance.TargetObjectId,SystemModstamp FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =: cObj.id];
        req2.setWorkitemId(PIW.id);
        // Submit the request for approval  
        Approval.ProcessResult result2 =  Approval.process(req2);
        Pagereference page = new Pagereference('/'+cObj.id);
        return page ;
    }
    
    /**
    Edit function
    */
    public pagereference RejectRequest(){
    if(!caseExists() && !IsNew) {
        return new PageReference('/apex/RecordDeleted');
    }
        Approval.ProcessWorkitemRequest req2 = 
            new Approval.ProcessWorkitemRequest();
        req2.setComments('Rejecting request.');
        req2.setAction('Reject');
        ProcessInstanceWorkitem PIW=[SELECT ActorId,Actor.Type,CreatedById,CreatedDate,Id,IsDeleted,OriginalActorId,ProcessInstance.TargetObjectId,SystemModstamp FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =: cObj.id];
        req2.setWorkitemId(PIW.id);
        Approval.ProcessResult result2 =  Approval.process(req2);
        Pagereference page = new Pagereference('/'+cObj.id);
        return page ;
    } 
    
    /**
    Edit function
    */
    public pagereference EditDetails(){
    if(!caseExists() && !IsNew) {
        return new PageReference('/apex/RecordDeleted');
    }
        Pagereference page = new Pagereference('/apex/AccountLinkingTemplate?ID='+cObj.id+'&loc='+cObj.id+'&parentAcc='+cObj.Mass_Update_Parent_Account__c+'&Type=Edit');
        page.setRedirect(true);
        return page;
    }
    
    /**
    submitForApprovalDetails function
    */
    public pagereference submitForApprovalDetails(){
    if(!caseExists() && !IsNew) {
        return new PageReference('/apex/RecordDeleted');
    }
        
        try{
            Approval.ProcessSubmitRequest req1 = 
            new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(cObj.id);
            // Submit the approval request for the account
            Approval.ProcessResult result = Approval.process(req1);
            shwSubmitForApprovalMsg = true;
        }catch(exception ex){
        }
        Pagereference page = new Pagereference('/apex/AccountLinkingTemplate?ID='+cObj.id+'&loc='+cObj.id+'&parentAcc='+cObj.Mass_Update_Parent_Account__c+'&Type=Detail');
        page.setRedirect(true);
        //return page;
        return null;
    }
      
    /**
        *wrapper
     */  
      public pagereference openAccount(){
        Pagereference page = new Pagereference('/'+selectedAccountID);
        return page ;
    }  
     public class accRelCheck{
         //setup appropriate variables
         public account a{get;set;}
         public Boolean acheck{get;set;}
         public String Notes{get;set;}
         public String activeMSD{get;set;}
     }
      public pagereference deleteCase(){
        if(!caseExists() && !IsNew) {
            return new PageReference('/apex/RecordDeleted');
        }
        if(cObj!= null){
        delete cObj;
        Pagereference page = new Pagereference('/'+500);
        return page ;
        }
        return null;
    }  
 /* Code for Pagenation :Start*/
   private integer counter=0;  //keeps track of the offset
   private integer list_size=20; //sets the page size or number of rows
   public integer total_size; //used to show user the total size of the list
   public integer total_Pages{get;Set;}
   public integer current_Page{get;Set;}
   public string PageList_Size{get;set;}
   public string pageTotalRecord_Size{get;set;}
 
   public PageReference Beginning() { //user clicked beginning
      counter = 0;
      SearchAccounts();
      return null;
   }
 
   public PageReference Previous() { //user clicked previous button
      counter -= list_size;
      SearchAccounts();
      return null;
   }
 
   public PageReference Next() { //user clicked next button
      counter += list_size;
      SearchAccounts();
      return null;
   }
 
   public PageReference End() { //user clicked end
      counter = total_size - math.mod(total_size, list_size);
      SearchAccounts();
      return null;
   }
 
   public Boolean getDisablePrevious() {
      //this will disable the previous and beginning buttons
      if (counter>0) return false; else return true;
   }
 
   public Boolean getDisableNext() { //this will disable the next and end buttons
      if (counter + list_size <= total_size) return false; else return true;
   }
 
   public Integer getTotal_size() {
      return total_size;
   }
 
   public Integer getPageNumber() {
      return counter/list_size + 1;
   }
 
   public Integer getTotalPages() {
      if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;
      } else {
         return (total_size/list_size);
      }
   }
/* Code for Pagenation :End*/
}