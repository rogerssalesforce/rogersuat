/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestSiteManager {
    static Id carrierOppId, oppId, sl1Id, sl2Id, site1Id, site2Id, site3Id, site4Id, site5Id, site6Id, site7Id; 
    static Id carrier_priceEntry1id, priceEntry1id, carrier_priceEntry2_1id, priceEntry2_1id, priceEntry2id, quote1Id, quote2Id, quote3Id;
    static ServiceableLocation__c sl2;
    static Opportunity o, o_carrier;
    static Product2 p, pInstall;
    static Boolean withAssertions = true;
    static Product2 carrierTestProduct;
    
    static {
        CLLIPostalCode__c pcToCLLI = new CLLIPostalCode__c(Postal_Code__c = 'M1C 3R2', CLLI_Code__c = '12345678');
        insert pcToCLLI;    
        
        
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        
        List <RecordType > lRtp = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Product2']);
        Map <string,Id> mapRTp = new Map <string,id> ();
        
        for (RecordType rtp : lRtp){
            mapRTp.put(rtp.Name,rtp.id);  
        }
       
        Account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street'; a.Account_Status__c = 'Assigned';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        insert a;       
        
        o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.Unified_Comm_Collaboration_Estimated__c =20;
        o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.today();
        insert o;
        oppId = o.id;
        
        Account a_carrier = new Account();
        a_carrier.name = 'Test Act2';
        a_carrier.Business_Segment__c = 'Alternate';
        a_carrier.RecordTypeId = mapRTa.get('Carrier Account');
        a_carrier.BillingStreet = 'Test';
        a_carrier.BillingCity = 'Ontario';
        a_carrier.BillingCountry = 'CA';
        a_carrier.BillingPostalCode = 'A9A 9A9';
        a_carrier.BillingState = 'ON';
        a_carrier.Account_Status__c = 'Assigned';
        insert a_carrier;       
        
        o_carrier = new Opportunity();
        o_carrier.Estimated_MRR__c = 500;
        o_carrier.Name = 'Test Opp';
        o_carrier.Unified_Comm_Collaboration_Estimated__c =20;
        o_carrier.StageName = 'Suspect - Qualified';
        o_carrier.Product_Category__c = 'Local';
        o_carrier.Network__c = 'Cable';
        o_carrier.Estimated_One_Time_Charge__c = 500;
        o_carrier.New_Term_Months__c = 5;
        o_carrier.AccountId = a_carrier.id;
        o_carrier.RecordTypeId = mapRTo.get('Wireline - New Carrier Sale');
        o_carrier.CloseDate = date.today();
        insert o_carrier;
        carrierOppId = o_carrier.id;
        
        
        Pricebook2 sp = new Pricebook2();
        sp = [select id from Pricebook2 where Name = :'Enterprise PriceBook'];
        
        Pricebook2 carrier_sp = new Pricebook2();
        carrier_sp = [select id from Pricebook2 where Name = :'Carrier PriceBook'];
        
        Pricebook2 sp1 = new Pricebook2();
        sp1 = [select id from Pricebook2 where isStandard = true];
        
        Quote q = new Quote(Name='q1', Term__c='3-Years', OpportunityId=oppId, Pricebook2Id = sp.id);
        q.ExpirationDate = Date.today()-1;
        insert q;
        quote1Id = q.Id;
        
        Quote q2 = new Quote(Name='q2', Term__c='3-Years', OpportunityId=oppId, Pricebook2Id = sp.id);
        q2.ExpirationDate = Date.today();
        insert q2;
        quote2Id = q2.Id;
        
        Quote q3 = new Quote(Name='q3', Term__c='3-Years', OpportunityId=carrierOppId, Pricebook2Id = carrier_sp.id);
        q3.ExpirationDate = Date.today();
        insert q3;
        quote3Id = q3.Id;
        
        ServiceableLocation__c sl = new ServiceableLocation__c();
        sl.Street_Name__c = 'Somewhere';
        sl.Street_Number__c  = '5';
        sl.Street_Type__c  = 'Ave';
        sl.City__c = 'Coty';
        sl.Postal_Code__c = 'A1A1A1';
        sl.Province_Code__c = 'ON';
        sl.CLLI_Code__c = '123113';
        sl.Access_Type_Group__c = 'NNI';
        sl.Access_Type__c = 'Ethernet Eon';
        insert sl;
        sl1Id = sl.Id;
        
        sl2 = new ServiceableLocation__c();
        sl2.Street_Name__c = 'Somewhere';
        sl2.Street_Number__c  = '5';
        sl2.Street_Type__c  = 'Ave';
        sl2.City__c = 'Coty';
        sl2.Postal_Code__c = 'A1A1A1';
        sl2.Province_Code__c = 'ON';
        sl2.CLLI_Code__c = '123113';
        sl2.Access_Type_Group__c = 'Fibre';
        sl2.Access_Type__c = 'ETHERNET EON;ETHERNET CAP';
        insert sl2;
        sl2Id = sl2.Id;
       
        
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Somewhere1';
        s1.Suite_Floor__c = '11a';
        s1.Street_Number__c  = '5';
        s1.City__c = 'Coty';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.CLLI_SWC__c = '123113';
        s1.ServiceableLocation__c = sl1Id;
        s1.Opportunity__c = oppId;
        insert s1;
        site1Id = s1.Id;
        
        Site__c s2 = new Site__c();
        s2.Street_Name__c = 'Somewhere2';
        s2.Suite_Floor__c = '11a';
        s2.Street_Number__c  = '5';
        s2.City__c = 'Coty';
        s2.Postal_Code__c = 'A2A1A1';
        s2.Province_Code__c = 'ON';
        s2.CLLI_SWC__c = '123113';
        s2.ServiceableLocation__c = sl2Id;
        s2.Opportunity__c = oppId;
        insert s2;
        site2Id = s2.Id;
        
        // This site is Serviceable
        Site__c s3 = new Site__c();
        s3.Street_Name__c = 'Somewhere3';
        s3.Suite_Floor__c = '11a';
        s3.Street_Number__c  = '5';
        s3.City__c = 'Coty';
        s3.Postal_Code__c = 'A3A1A1';
        s3.Province_Code__c = 'ON';
        s3.CLLI_SWC__c = '123113';
        s3.ServiceableLocation__c = sl.Id;
        s3.Opportunity__c = oppId;
        insert s3;
        site3Id = s3.Id;
        
        
        Site__c s4 = new Site__c();
        s4.Street_Name__c = 'Somewhere4';
        s4.Suite_Floor__c = '11a';
        s4.Street_Number__c  = '7';
        s4.City__c = 'Coty';
        s4.Postal_Code__c = 'A3A1A1';
        s4.Province_Code__c = 'ON';
        s4.CLLI_SWC__c = '123113';
        s4.ServiceableLocation__c = sl.Id;
        s4.Opportunity__c = carrierOppId;
        s4.Is_a_Z_Site__c = true; 
        insert s4;
        site4Id = s4.Id;
        
        Site__c s5 = new Site__c();
        s5.Street_Name__c = 'Somewhere5';
        s5.Suite_Floor__c = '11a';
        s5.Street_Number__c  = '8';
        s5.City__c = 'Coty';
        s5.Postal_Code__c = 'A3A1A1';
        s5.Province_Code__c = 'ON';
        s5.CLLI_SWC__c = '123113';
        s5.ServiceableLocation__c = sl2.Id;
        s5.Opportunity__c = carrierOppId; 
        s5.Z_Site__c = site4Id;
        insert s5;
        site5Id = s5.Id;
        
        Site__c s6 = new Site__c();
        s6.Street_Name__c = 'Somewhere6';
        s6.Suite_Floor__c = '11a';
        s6.Street_Number__c  = '9';
        s6.City__c = 'Coty';
        s6.Postal_Code__c = 'A3A1A1';
        s6.Province_Code__c = 'ON';
        s6.CLLI_SWC__c = '123113';
        s6.ServiceableLocation__c = sl2.Id;
        s6.Opportunity__c = carrierOppId; 
        insert s6;
        site6Id = s6.Id;
        
        Site__c s7 = new Site__c();
        s7.Street_Name__c = 'Somewhere7';
        s7.Suite_Floor__c = '11a';
        s7.Street_Number__c  = '10';
        s7.City__c = 'Coty';
        s7.Postal_Code__c = 'A3A1A1';
        s7.Province_Code__c = 'ON';
        s7.CLLI_SWC__c = '123113';
        s7.ServiceableLocation__c = sl2.Id;
        s7.Opportunity__c = carrierOppId; 
        s7.Is_a_Z_Site__c = true;
        insert s7;
        site7Id = s7.Id;
        
        Quote_Site__c qs = new Quote_Site__c();
        qs.Site__c = site1Id;
        qs.Quote__c = quote1Id;
        insert qs;
        
        qs = new Quote_Site__c();
        qs.Site__c = site2Id;
        qs.Quote__c = quote1Id;
        insert qs;
        
        qs = new Quote_Site__c();
        qs.Site__c = site3Id;
        qs.Quote__c = quote1Id;
        insert qs;
        
        
        /* Carrier */
        qs = new Quote_Site__c();
        qs.Site__c = site4Id;
        qs.Quote__c = quote3Id;
        insert qs;
        
                // Install Product
        pInstall  = new Product2();
        pInstall.IsActive = true;
        pInstall.Name = 'Fibre';
        pInstall.Charge_Type__c = 'NRC';
        pInstall.Access_Type__c = 'Ethernet EON';
        pInstall.Access_Type_Group__c = 'Fibre';
        pInstall.Category__c = 'Install';
        pInstall.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
        pInstall.RecordTypeId = mapRTp.get('Enterprise Products');
        pInstall.Service_Term__c = '3-Years';
        pInstall.Mark_Up_Factor__c = 1.0;
        pInstall.Start_Date__c = Date.today();
        pInstall.End_Date__c = Date.today()+1;
        insert pInstall;
        
        //Product 1
        p  = new Product2();
        p.IsActive = true;
        p.Name = 'Fibre';
        p.Charge_Type__c = 'NRC';
        p.Access_Type__c = 'Ethernet EON';
        p.Access_Type_Group__c = 'Fibre';
        p.Category__c = 'Access';
        p.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
        p.RecordTypeId = mapRTp.get('Enterprise Products');
        p.Service_Term__c = '3-Years';
        p.Product_Install_Link__c = pInstall.Id;
        p.Mark_Up_Factor__c = 1.0;
        p.Start_Date__c = Date.today();
        p.End_Date__c = Date.today()+1;
        insert p;
        

        
        PricebookEntry peStandard = new PricebookEntry();
        peStandard.Pricebook2Id = sp1.id;
        peStandard.UnitPrice = 60;
        peStandard.Product2Id = p.id;
        peStandard.IsActive = true;
        peStandard.UseStandardPrice = false;
        
        insert peStandard;
        
        PricebookEntry pe1 = new PricebookEntry();
        pe1.Pricebook2Id = sp.id;
        pe1.UnitPrice = 60;
        pe1.Product2Id = p.id;
        pe1.IsActive = true;
        pe1.UseStandardPrice = false;
        
        insert pe1;
        priceEntry1id = pe1.Id;
        
        peStandard = new PricebookEntry();
        peStandard.Pricebook2Id = sp1.id;
        peStandard.UnitPrice = 60;
        peStandard.Product2Id = pInstall.id;
        peStandard.IsActive = true;
        peStandard.UseStandardPrice = false;
        
        insert peStandard;
        
        pe1 = new PricebookEntry();
        pe1.Pricebook2Id = sp.id;
        pe1.UnitPrice = 60;
        pe1.Product2Id = pInstall.id;
        pe1.IsActive = true;
        pe1.UseStandardPrice = false;
        
        insert pe1;
        
        //Product 2
        Product2 p1  = new Product2();
        p1.IsActive = true;
        p1.Name = 'ProductMe1';
        p1.Charge_Type__c = 'MRC';
        p1.Mark_Up_Factor__c = 1.0;
        p1.Start_Date__c = Date.today();
        p1.End_Date__c = Date.today()+1;
        insert p1;
        
        PricebookEntry pe21Standard = new PricebookEntry();
        pe21Standard.Pricebook2Id = sp1.id;
        pe21Standard.UnitPrice = 60;
        pe21Standard.Product2Id = p1.id;
        pe21Standard.IsActive = true;
        pe21Standard.UseStandardPrice = false;
        
        insert pe21Standard;
        
        PricebookEntry pe21 = new PricebookEntry();
        pe21.Pricebook2Id = sp.id;
        pe21.UnitPrice = 60;
        pe21.Product2Id = p1.id;
        pe21.IsActive = true;
        pe21.UseStandardPrice = false;
        
        insert pe21;
        priceEntry2_1id = pe21.Id;
        
        
        
        // Carrier 
        Product2 carrierProduct  = new Product2();
        carrierProduct.IsActive = true;
        carrierProduct.Name = 'Fibre';
        carrierProduct.Charge_Type__c = 'NRC';
        carrierProduct.Access_Type__c = 'Ethernet EON';
        carrierProduct.Access_Type_Group__c = 'Fibre';
        carrierProduct.Category__c = 'Access';
        carrierProduct.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
        carrierProduct.RecordTypeId = mapRTp.get('Carrier Wholesale Products');
        carrierProduct.Service_Term__c = '3-Years';
        carrierProduct.Mark_Up_Factor__c = 1.0;
        carrierProduct.Start_Date__c = Date.today();
        carrierProduct.End_Date__c = Date.today()+1;
        insert carrierProduct;
        
        carrierTestProduct = carrierProduct;
        
        PricebookEntry carrier_peStandard = new PricebookEntry();
        carrier_peStandard.Pricebook2Id = sp1.id;
        carrier_peStandard.UnitPrice = 60;
        carrier_peStandard.Product2Id = carrierProduct.id;
        carrier_peStandard.IsActive = true;
        carrier_peStandard.UseStandardPrice = false;
        
        insert carrier_peStandard;
        
        PricebookEntry carrier_pe1 = new PricebookEntry();
        carrier_pe1.Pricebook2Id = carrier_sp.id;
        carrier_pe1.UnitPrice = 60;
        carrier_pe1.Product2Id = carrierProduct.id;
        carrier_pe1.IsActive = true;
        carrier_pe1.UseStandardPrice = false;
        
        insert carrier_pe1;
        carrier_priceEntry1id = carrier_pe1.Id;
        
        //Product 2
        Product2 carrier_p1  = new Product2();
        carrier_p1.IsActive = true;
        carrier_p1.Name = 'ProductMe1';
        carrier_p1.Charge_Type__c = 'MRC';
        carrier_p1.Mark_Up_Factor__c = 1.0;
        carrier_p1.Start_Date__c = Date.today();
        carrier_p1.End_Date__c = Date.today()+1;
        insert carrier_p1;
        
        PricebookEntry carrier_pe21Standard = new PricebookEntry();
        carrier_pe21Standard.Pricebook2Id = sp1.id;
        carrier_pe21Standard.UnitPrice = 60;
        carrier_pe21Standard.Product2Id = carrier_p1.id;
        carrier_pe21Standard.IsActive = true;
        carrier_pe21Standard.UseStandardPrice = false;
        
        insert carrier_pe21Standard;
        
        PricebookEntry carrier_pe21 = new PricebookEntry();
        carrier_pe21.Pricebook2Id = carrier_sp.id;
        carrier_pe21.UnitPrice = 60;
        carrier_pe21.Product2Id = carrier_p1.id;
        carrier_pe21.IsActive = true;
        carrier_pe21.UseStandardPrice = false;
        
        insert carrier_pe21;
        carrier_priceEntry2_1id = carrier_pe21.Id;
    }
    
    static testMethod void siteWithPostalCodeTest() {
        Site__c siteWithPC = new Site__c(Name='Test', Suite_Floor__c = '11a', Street_Number__c = '123', Street_Name__c = 'Sesame', Street_Type__c = 'Ave', City__c='Toronto', Province_Code__c='ON', Postal_Code__c = 'M1C 3R2');
        insert siteWithPC;
    }
    
    static testMethod void siteNoPostalCodeTest() {
        Site__c siteNoPC = new Site__c(Name='Test', Suite_Floor__c = '11a', Street_Number__c = '123', Street_Name__c = 'Sesame', Street_Type__c = 'Ave', City__c='Toronto', Province_Code__c='ON');
        insert siteNoPC;
    }
    
    static testMethod void CLLICodeMatchTest() {
        Site__c siteWithPC = new Site__c(Name='Test', Suite_Floor__c = '11a', Street_Number__c = '123', Street_Name__c = 'Sesame', Street_Type__c = 'Ave', City__c='Toronto', Province_Code__c='ON', Postal_Code__c = 'M1C 3R2');
        insert siteWithPC;
    }
    
    static testMethod void noCLLICodeMatchTest() {
        Site__c siteWithPC = new Site__c(Name='Test', Suite_Floor__c = '11a', Street_Number__c = '123', Street_Name__c = 'Sesame', Street_Type__c = 'Ave', City__c='Toronto', Province_Code__c='ON', Postal_Code__c = 'Z1Z 3Z2');
        insert siteWithPC;
    }
    
    static testMethod void testInsertSiteAlreadyOnQuote(){
        Integer sitesBeforeSize = 0;
       
        List<Site__c> sitesBefore = [Select Id, Name, Street_Name__c FROM Site__c where Street_Name__c = 'Somewhere4' AND Street_Number__c = '7' AND ServiceableLocation__c = :sl2.Id];
                System.debug('\n\n\n\n\nSitesBefore: ' + sitesBefore);
        if (sitesBefore!= null)
            sitesBeforeSize = sitesBefore.size();
        
        Site__c s8;
        try{
        s8 = new Site__c();
        s8.Street_Name__c = 'Somewhere4';
        s8.Suite_Floor__c = '11a';
        s8.Street_Number__c  = '7';
        s8.City__c = 'Coty';
        s8.Postal_Code__c = 'A3A1A1';
        s8.Province_Code__c = 'ON';
        s8.CLLI_SWC__c = '123113';
        s8.ServiceableLocation__c = sl2.Id;
        s8.Opportunity__c = oppId; 
        s8.Is_a_Z_Site__c = true;
        insert s8;
        }catch(Exception ex){
            System.debug('\n\n\n\n\nTest testInsertSiteAlreadyOnQuote: ' + ex.getMessage());
        }
        List<Site__c> sitesAfter = [Select Id, Name, Street_Name__c FROM Site__c where Street_Name__c = 'Somewhere4' AND Street_Number__c = '7' AND ServiceableLocation__c = :sl2.Id];
                System.debug('\n\n\n\n\nSitesBefore: ' + sitesBefore);
        //System.assertEquals(sitesAfter.size() - sitesBeforeSize, 0);
        
    }
    
    static testMethod void testInsertSiteOnExpiredQuote(){
        Integer sitesBeforeSize = 0;
       
        List<Site__c> sitesBefore = [Select Id, Name, Street_Name__c FROM Site__c where Street_Name__c = 'Somewhere4' AND Street_Number__c = '7' AND ServiceableLocation__c = :sl2.Id];
        
        if (sitesBefore!= null)
            sitesBeforeSize = sitesBefore.size();
        
        Quote qTest = [SELECT Id, ExpirationDate From Quote WHERE Id = :quote3Id];
        
        System.debug('\n\n\n\n\nTest Expiration Date: ' + qTest.ExpirationDate);
        
        qTest.ExpirationDate = Date.today() - 1;
        UPDATE qTest;       
        System.debug('\n\n\n\n\nTest Expiration Date: ' + qTest.ExpirationDate);

        
        Site__c s8;
        try{
        s8 = new Site__c();
        s8.Street_Name__c = 'Somewhere4';
        s8.Suite_Floor__c = '11a';
        s8.Street_Number__c  = '7';
        s8.City__c = 'Coty';
        s8.Postal_Code__c = 'A3A1A1';
        s8.Province_Code__c = 'ON';
        s8.CLLI_SWC__c = '123113';
        s8.ServiceableLocation__c = sl2.Id;
        s8.Opportunity__c = oppId; 
        s8.Is_a_Z_Site__c = true;
        insert s8;
        }catch(Exception ex){
            System.debug('\n\n\n\n\nTest testInsertSiteOnExpiredQuote: ' + ex.getMessage());
        }
        List<Site__c> sitesAfter = [Select Id, Name, Street_Name__c FROM Site__c where Street_Name__c = 'Somewhere4' AND Street_Number__c = '7' AND ServiceableLocation__c = :sl2.Id];
        
        System.debug('\n\n\n\n\nSitesAfter: ' + sitesAfter);
        
        System.assertEquals(sitesAfter.size() - sitesBeforeSize, 1);
        
    }
    
    
    
     static testMethod void testSiteOkay(){
        Integer sitesBeforeSize = 0;
       
        List<Site__c> sitesBefore = [Select Id, Name, Street_Name__c FROM Site__c where Street_Name__c = 'Somewhere1' AND Street_Number__c = '5' AND ServiceableLocation__c = :sl2.Id];
        
        if (sitesBefore!= null)
            sitesBeforeSize = sitesBefore.size();
        
        Site__c s8;
        try{
            s8 = new Site__c();
            s8.Street_Name__c = 'Somewhere1';
            s8.Suite_Floor__c = '11a';
            s8.Street_Number__c  = '5';
            s8.City__c = 'Coty';
            s8.Postal_Code__c = 'A2A1A1';
            s8.Province_Code__c = 'ON';
            s8.CLLI_SWC__c = '123113';
            s8.ServiceableLocation__c = sl2Id;
            s8.Opportunity__c = carrierOppId;
            insert s8;
        }catch(Exception ex){
            System.debug('\n\n\n\n\nTest Site Okay' + ex.getMessage());
        }
        List<Site__c> sitesAfter = [Select Id, Name, Street_Name__c FROM Site__c where Street_Name__c = 'Somewhere1' AND Street_Number__c = '5' AND ServiceableLocation__c = :sl2.Id];
        System.assertEquals(sitesAfter.size() - sitesBeforeSize, 1);
        update s8;
        System.assertEquals(sitesAfter.size() - sitesBeforeSize, 1);
    }
    
    static testMethod void testDuplicateSiteOnOpportunity(){
        Integer sitesBeforeSize = 0;
       
        List<Site__c> sitesBefore = [Select Id, Name, Street_Name__c FROM Site__c where Street_Name__c = 'Somewhere4' AND Street_Number__c = '7' AND ServiceableLocation__c = :sl2.Id];
        
        if (sitesBefore!= null)
            sitesBeforeSize = sitesBefore.size();
        Site__c s8;
        try{
        s8 = new Site__c();
        s8.Suite_Floor__c = '11a';
        s8.Street_Name__c = 'Somewhere4';
        s8.Street_Number__c  = '7';
        s8.City__c = 'Coty';
        s8.Postal_Code__c = 'A3A1A1';
        s8.Province_Code__c = 'ON';
        s8.CLLI_SWC__c = '123113';
        s8.ServiceableLocation__c = sl2.Id;
        s8.Opportunity__c = carrierOppId; 
        s8.Is_a_Z_Site__c = true;
        insert s8;
        }catch(Exception ex){
            System.debug('\n\n\n\n\n\ntestDuplicateSiteOnOpportunity: ' + ex.getMessage());
        }
        List<Site__c> sitesAfter = [Select Id, Name, Street_Name__c FROM Site__c where Street_Name__c = 'Somewhere4' AND Street_Number__c = '7' AND ServiceableLocation__c = :sl2.Id];
    }
}