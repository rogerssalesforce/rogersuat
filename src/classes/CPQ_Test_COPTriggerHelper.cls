/*********************************************************************************
Class Name      : CPQ_Test_COPTriggerHelper 
Description     : This class a test class for CPQ_AgreementTriggerHelper. 
Created By      : Aakanksha Patel
Created Date    : 25-Nov-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Aakanksha Patel            25-Nov-2015           Method "testAccountUpdateWithCOPId"
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest (seeAllData=False) 
private class CPQ_Test_COPTriggerHelper{
    static User TestAEUser;
    static Account oAccount;
    static Opportunity oOpportunity;
    static Apttus_Proposal__Proposal__c oQuote;
    static Attachment attachmentQuote;
    static Apttus_Config2__PriceList__c oPriceList;
    static Product2 oProduct;
    static Apttus__APTS_Agreement__c oAgreement;
    static Apttus__APTS_Agreement__c oAgreementNew;
    static List<Apttus__AgreementLineItem__c> lstAgreementLineItem;
    static COP__c oCOP;
    static COP__c oCOPnew;
    static COP_Line_Item__c oCOPLineItem;
    
    private static User testSUser;
    private static CPQ_Settings__c csMapping;
  
    private static final String CPQ_AGREEMENTS_PROFILES_SETTING_VALUE = 'EBU - Rogers Specialist|Read';
    private static final String CPQ_AGREEMENTS_PROFILES_SETTING_VALUE2 = 'EBU - Rogers Resellers Sales Rep|Read';
    private static final String CPQ_ACCOUNT_SPECIALIST = 'EBU - Rogers Specialist';
    private static final String CPQ_SYSTEM_ADMIN = 'System Administrator';
    private static final String CPQ_ADMIN_USERNAME = 'standarduser_test123@testorg.com';
    private static final String CPQ_SPECIALIST_USERNAME = 'specialistuser_test123@testorg.com';
    private static final String CPQ_COP_STATUS_COMPLETED = 'Completed';
    private static final String CPQ_STATUS_CATEGORY_IN_REQUEST ='Request';
    private static final String CPQ_STATUS_SIGNATURE_NOTREQUIRED = 'Signature Not Required';
    private static final String CPQ_STATUS_CATEGORY_IN_SIGNATURES = 'In Signatures';
    private static final String CPQ_STATUS_INTERNAL_SIGNATURE='Internal Signatures';
   
    /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testMethod void createTestData() {
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');      
        testSUser = CPQ_Test_Setup.newUser(CPQ_ACCOUNT_SPECIALIST, CPQ_SPECIALIST_USERNAME);
        insert testSUser;
        system.runAs(TestAEUser){
            
            //Create PriceList
            oPriceList = CPQ_Test_Setup.newPriceList();
            insert oPriceList ;
            
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
           
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
            insert oQuote;
            //Create attachment for Quote
            attachmentQuote = CPQ_Test_Setup.newAttachment(oQuote.id);
            insert attachmentQuote;
    
            //Create Agreement
            oAgreement= CPQ_Test_Setup.newAgreement(oAccount, oOpportunity,oQuote);
            
            //Create Agreement
            oAgreementNew = CPQ_Test_Setup.newAgreement(oAccount, oOpportunity,oQuote);
        
            //Inserting Custom setting CPQ_Settings__c
           //Inserting Custom setting CPQ_Settings__c
            csMapping  = CPQ_Settings__c.getOrgDefaults();
            csMapping.AgreementsProfiles__c = CPQ_AGREEMENTS_PROFILES_SETTING_VALUE;
            csMapping.AgreementsProfiles2__c = CPQ_AGREEMENTS_PROFILES_SETTING_VALUE2;
            csMapping.Agreement_Category_Status_to_Close_Opp__c=CPQ_STATUS_CATEGORY_IN_REQUEST+','+CPQ_STATUS_CATEGORY_IN_SIGNATURES;
            csMapping.Agreement_Status_to_Close_Opp__c=CPQ_STATUS_SIGNATURE_NOTREQUIRED+','+CPQ_STATUS_INTERNAL_SIGNATURE;
            upsert csMapping;
            
            insert oAgreement;
            insert oAgreementNew;
        
        }
    }

    /*********************************************************************************************************
    Method Name    : testAccountUpdateWithCOPId
    Description    : Method for Testing the "Wireless COP" field update on Account when a COP related to Acc is "Completed"
    Return Type    : void  
    Parameter      : Nil              
    **********************************************************************************************************/ 
    static testmethod void testAccountUpdateWithCOPId()
    {
        createTestData();
        oQuote.Primary_Quote_Document_ID__c = attachmentQuote.id;
        update oQuote;
        system.runAs(TestAEUser)
        {
            test.startTest();
            
            //RecordType for the COP record
            RecordType oRecTypeCOP = [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.IsActive, r.Id, r.DeveloperName From RecordType r where r.DeveloperName = 'Contracts_Team_Wireless' and r.SobjectType = 'COP__c'];
            
            //COP Creation
            oCOP = CPQ_Test_Setup.newCOP(oAgreement);
            oCOP.RecordTypeID  = oRecTypeCOP.id;
            oCOP.COP_Status__c = CPQ_COP_STATUS_COMPLETED;
            insert oCOP;
            
            //RecordType for the COP Line Item record
            RecordType oRecTypeCOPLI = [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.IsActive, r.Id, r.DeveloperName From RecordType r where r.DeveloperName = 'Contracts_Team_Wireless' and r.SobjectType = 'COP_Line_Item__c'];
            
            //COP Line Item Creation  with Wireless RecordType
            oCOPLineItem = CPQ_Test_Setup.newCOPLineItem(oCOP);
            oCOPLineItem.RecordTypeID  = oRecTypeCOPLI.id;
            insert oCOPLineItem;
            
            //Account Update for the Wireless ID field
            oAccount.Latest_Wireless_COP__c = oCOP.id;
            update oAccount;
            
            //Assertion to check if the Account has been updated with the COP Salesforce ID
            List<Account> lstAccount = [select id, Latest_Wireless_COP__c from Account where Id=:oAccount.id ];
            system.assert(lstAccount[0].Latest_Wireless_COP__c == oCOP.id);
          
            //COP Creation
            oCOPnew = CPQ_Test_Setup.newCOP(oAgreementNew);
            oCOPnew.RecordTypeID  = oRecTypeCOP.id;
            insert oCOPnew;
             
            //Updating the Status of COP
            oCOPnew.COP_Status__c = CPQ_COP_STATUS_COMPLETED;
            update oCOPnew;
             
            //Assertion to check if the Account has been updated with the new COP ID
            lstAccount = [select id, Latest_Wireless_COP__c from Account where Id=:oAccount.id ];
            system.assert(lstAccount[0].Latest_Wireless_COP__c == oCOPnew.id);
            
        }
    }
    
     /*********************************************************************************************************
    Method Name    : testBulkAccountUpdateWithCOPId
    Description    : Method for Testing the "Wireless COP" field update on Account when a COP related to Acc is "Completed"
    Return Type    : void  
    Parameter      : Nil              
    **********************************************************************************************************/ 
    static testmethod void testBulkAccountUpdateWithCOPId()
    {
        createTestData();
        oQuote.Primary_Quote_Document_ID__c = attachmentQuote.id;
        update oQuote;
        system.runAs(TestAEUser)
        {
            test.startTest();
            
            Account oAccBulk;
            Apttus__APTS_Agreement__c oAgreementBulk;
            COP__c oCOPBulk;
            COP_Line_Item__c oCOPLIBulk;
            List<Account> lstAccount = new List<Account>();
            List<Apttus__APTS_Agreement__c> lstAgreement = new List<Apttus__APTS_Agreement__c>();
            List<COP__c> lstCOP = new List<COP__c>();
            List<COP_Line_Item__c> lstCOPLI = new List<COP_Line_Item__c>();
            
             //RecordType for the COP record
            RecordType oRecTypeCOP = [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.IsActive, r.Id, r.DeveloperName From RecordType r where r.DeveloperName = 'Contracts_Team_Wireless' and r.SobjectType = 'COP__c'];
            
            //RecordType for the COP Line Item record
            RecordType oRecTypeCOPLI = [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.IsActive, r.Id, r.DeveloperName From RecordType r where r.DeveloperName = 'Contracts_Team_Wireless' and r.SobjectType = 'COP_Line_Item__c'];
          
            //Creating Accounts for bulkification
            for(integer i=0; i<100; i++)
            {
                oAccBulk = CPQ_Test_Setup.newAccount('Test Account[i]');
                lstAccount.add(oAccBulk);
            }
            insert lstAccount;
            
            //Creating Agreements for each of the Accounts
            for(Account acc: lstAccount)
            {
                oAgreementBulk= CPQ_Test_Setup.newAgreement(acc, oOpportunity,oQuote);
                lstAgreement.add(oAgreementBulk);
            }
            insert lstAgreement;
            
            //Creating COP records for each of the Agreement records
            for(Apttus__APTS_Agreement__c agreement: lstAgreement)
            {
                oCOPBulk = CPQ_Test_Setup.newCOP(agreement);
                oCOPBulk.RecordTypeID  = oRecTypeCOP.id;
                oCOPBulk.COP_Status__c = CPQ_COP_STATUS_COMPLETED;
                lstCOP.add(oCOPBulk);
            }
            insert lstCOP;
            
            //Creating COP Line Items  for each of the COP records
            for(COP__c cop: lstCOP)
            {
                oCOPLIBulk = CPQ_Test_Setup.newCOPLineItem(cop);
                oCOPLIBulk.RecordTypeID  = oRecTypeCOPLI.id;
                lstCOPLI.add(oCOPLIBulk);
            }
            insert lstCOPLI;
    
            //Account Update for the Wireless ID field
            for(Account accUpdate: lstAccount)
            {
                accUpdate.Latest_Wireless_COP__c = lstCOP[0].id;
            }
            update lstAccount;
            
            //Assertion to check if the Account from the list has been updated with the COP Salesforce ID
            for(Account accRec : [select id, Latest_Wireless_COP__c from Account where Id=:lstAccount ])
                system.assert(accRec.Latest_Wireless_COP__c == lstCOP[0].id);
        }
    }
  
}