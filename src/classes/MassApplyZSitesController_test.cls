@istest(seealldata=true)
public class MassApplyZSitesController_test {
/*    public static Opportunity opp;
    public static Id oppId;
    public static Id qrId;
    public static Quote q;
    public A_Z_Site__c AZS = new A_Z_Site__c();
    
    public static void dataGenerator () {
        Account theAccount = new Account(Name = 'AccountTest',NumberOfEmployees=5,phone='111111111',Account_Status__c='Assigned');    

        insert theAccount; 
        Schema.DescribeSObjectResult opportunitySchema = Schema.SObjectType.Opportunity; 
 
        Id oppRt = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Wireless - New Opportunity').getRecordTypeId(); 
            
        opp = new Opportunity(Name = 'theOpportunityx',  RecordTypeId = oppRt,
                                                           AccountId = theAccount.id,
                                                           CloseDate = date.Today(),
                                                           StageName = 'Identify',
                                                           Amount = 50, ForecastCategoryName='Pipeline',Type='New' );
        Pricebook2 sp = new Pricebook2();
        sp= [select id from Pricebook2 where Name = :'Enterprise PriceBook'];

        Profile pNSA = [select id from profile where name='EBU - Rogers Specialist'];
        User uNSAManager = new User(alias = 'stand3', email='standarduserEnterprise3@testorg.com',
         emailencodingkey='UTF-8', lastname='Enterprise3', languagelocalekey='en_US',
         localesidkey='en_US', profileid = pNSA.Id, 
         timezonesidkey='America/Los_Angeles', username='rbsEnterprise3User@testorg.com');
        insert uNSAManager;
        User uNSA = new User(alias = 'stand2', email='standarduserEnterprise2@testorg.com',
         emailencodingkey='UTF-8', lastname='Enterprise2', languagelocalekey='en_US',
         localesidkey='en_US', profileid = pNSA.Id, ManagerId = uNSAManager.Id,
         timezonesidkey='America/Los_Angeles', username='rbsEnterprise2User@testorg.com');
        insert uNSA;
        System.debug('oppbeforeinsert'+opp);
        insert opp;
        System.debug('oppinserted'+opp);
        oppId=opp.Id;
        q = new Quote(Name='q1', actualTerm__c = '12', Term__c='12-Months', OpportunityId=oppId, Pricebook2Id = sp.id);
        insert q;
        qrId = OpportunityWebServices.createQuoteRequest('' + q.Id, '' + uNSA.Id);
    }
	static testMethod void testloadQRequestsLocations() {
       dataGenerator();
       PageReference pageRef = New PageReference('/apex/MassApplyZSite?retURL=' + String.valueOf(oppId)); 
       Test.setCurrentPage(pageRef);       
       MassApplyZSitesController controller = new MassApplyZSitesController();
	   Controller.loadQRequestsLocations();          
	}
	static testMethod void testrunSearch() {
        dataGenerator();
        PageReference pageRef = New PageReference('/apex/MassApplyZSite?retURL=' + String.valueOf(oppId));
        pageRef.getParameters().put('streetNumber','1');
        pageRef.getParameters().put('streetName','Albert');
        pageRef.getParameters().put('city','Waterloo');
        pageRef.getParameters().put('provinceCode','ON');
       Test.setCurrentPage(pageRef);       
       MassApplyZSitesController controller = new MassApplyZSitesController();
	   Controller.runSearch();         
	}
	static testMethod void testupdateZSites() {
        dataGenerator();
        PageReference pageRef = New PageReference('/apex/MassApplyZSite?retURL=' + String.valueOf(oppId)); 
       Test.setCurrentPage(pageRef);       
       MassApplyZSitesController controller = new MassApplyZSitesController();
	   Controller.updateZSites();         
	}
	static testMethod void testremoveZSites() {
        dataGenerator();
        PageReference pageRef = New PageReference('/apex/MassApplyZSite?retURL=' + String.valueOf(oppId)); 
       Test.setCurrentPage(pageRef);       
       MassApplyZSitesController controller = new MassApplyZSitesController();
	   Controller.removeZSites();         
	}
	static testMethod void testreturnToOpportunity() {
	dataGenerator();
       PageReference pageRef = new PageReference('/apex/massApplyZSite?retUrl=' + String.valueOf(oppId)+'&oppId=' + String.valueOf(oppId)+ '&qRequestId='+String.valueOf(qrId)); 
       Test.setCurrentPage(pageRef);       
       MassApplyZSitesController controller = new MassApplyZSitesController();
	   Controller.returnToOpportunity();         
	}
	static testMethod void testreturnToQuoteRequest() {
       dataGenerator();
       PageReference pageRef = new PageReference('/apex/massApplyZSite?retUrl=' + String.valueOf(oppId)+'&oppId=' + String.valueOf(oppId)+ '&qRequestId='+String.valueOf(qrId));
       pageRef.getParameters().put('RetURL', ''+qrId);
       Test.setCurrentPage(pageRef);       
       MassApplyZSitesController controller = new MassApplyZSitesController();
	   Controller.returnToQuoteRequest();         
	}
	static testMethod void testaddSites() {
        dataGenerator();
        PageReference pageRef = New PageReference('/apex/MassApplyZSite?retURL=' + String.valueOf(oppId)); 
       Test.setCurrentPage(pageRef);       
       MassApplyZSitesController controller = new MassApplyZSitesController();
	   Controller.addSites();         
	}
	static testMethod void testremoveSites() {
        dataGenerator();
        PageReference pageRef = New PageReference('/apex/MassApplyZSite?retURL=' + String.valueOf(oppId)); 
       Test.setCurrentPage(pageRef);       
       MassApplyZSitesController controller = new MassApplyZSitesController();
	   Controller.removeSites();         
	}*/
}