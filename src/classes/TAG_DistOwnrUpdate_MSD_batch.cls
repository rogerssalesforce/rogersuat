/*
===============================================================================
 Class Name   : TAG_DistOwnrUpdate_MSD_batch 
===============================================================================
PURPOSE:  (This Class will support the logic for District Owner before Update Trigger.)  
            Get list of all MSD Codes for a District Owner and 
            update them with owner and set a flag which is used for MAL integration.
  

Developer: Aakanksha Patel
Date: 06/29/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
06/29/2015           Aakanksha Patel             Original Version
===============================================================================
*/
global class TAG_DistOwnrUpdate_MSD_batch implements Database.Batchable<SObject>, Database.stateful{
    
    Global String query;
    Global Map<ID,District__c> mapIdDistrict;
    Global set<ID> accountIds = new set<ID>();
        
    global TAG_DistOwnrUpdate_MSD_batch(Map<ID,District__c> mapIdDistrict,set<ID> accountIds)
    {
        this.mapIdDistrict = mapIdDistrict; 
        this.accountIds = accountIds;           // constructor copies in arg to local instance vbl
    }
      
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        set<Id>  tempSet = new Set<Id>();
        tempSet = mapIdDistrict.keyset();
       
       if(!Test.isRunningTest()){
        return Database.getQueryLocator('Select Id,OwnerId, MSD_to_MAL__c,District__c,Account__r.Recalculate_Team__c,District__r.District_Owner__c From MSD_Code__c where District__c IN: tempSet');
        }
        else{
           return Database.getQueryLocator('Select Id,OwnerId, MSD_to_MAL__c,District__c,Account__r.Recalculate_Team__c,District__r.District_Owner__c From MSD_Code__c where District__c IN : tempset limit 10');
        }     
    }
    
    global void execute(Database.BatchableContext BC, List<MSD_Code__c> scope)
    {
        Account acc1 = new Account();
         
        Set<Account> setaccountUpdate = new Set<Account>();
        Set<MSD_Code__c> setMsdOwnerUpdate = new Set<MSD_Code__c>();
        map<id, Account> mapmsdAccount = new map<id,Account>();
        
        
        for(MSD_Code__c msd : scope ) //map of MSD codes and its account
        {
             mapmsdAccount.put(msd.id,msd.Account__r);
        }
       
        for(MSD_Code__c msdRec: scope) 
        {
            if(msdRec.District__r.District_Owner__c!=null)
            {
                msdRec.OwnerId = msdRec.District__r.District_Owner__c;
            }
            msdRec.MSD_to_MAL__c = TRUE;
            if(mapmsdAccount.get(msdRec.id)!=null)
            {
                acc1 = mapmsdAccount.get(msdRec.id);
                acc1.Recalculate_Team__c = TRUE;                
                setaccountUpdate.add(acc1);
            }
            setMsdOwnerUpdate.add(msdRec);
            
        }
        update scope;
        
        if(setaccountUpdate.size()!=NULL)
        {
            List<Account> listAccountUpdate = new List<Account>();
            listAccountUpdate.addAll(setaccountUpdate);
            set<Account> temp = new set<Account>(listAccountUpdate);

            listAccountUpdate.clear();
            listAccountUpdate.addAll(temp);
            update listAccountUpdate;
        }
        if(setMsdOwnerUpdate.size()!=NULL)
        {
            List<MSD_Code__c> listMSDUpdate = new List<MSD_Code__c>();
            listMSDUpdate.addAll(setMsdOwnerUpdate);
            set<MSD_Code__c> temp = new set<MSD_Code__c>(listMSDUpdate);
           
            listMSDUpdate.clear();
            listMSDUpdate.addAll(temp);
            update listMSDUpdate;
        }
    
    }
    
    global void finish(Database.BatchableContext BC)
    {       
         Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
         Integer batchSize = Integer.ValueOf(TAGsettings.DistrictOwnerUpdateBatch_size__c);
        
         TAG_DistOwnrUpdate_SharedMSD_batch sc1 = new TAG_DistOwnrUpdate_SharedMSD_batch(mapIdDistrict,accountIds);
         ID batchprocessid = Database.executeBatch(sc1,batchSize);
    }
}