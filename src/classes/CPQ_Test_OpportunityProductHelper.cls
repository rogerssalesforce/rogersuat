/*********************************************************************************
Class Name      : CPQ_Test_OpportunityProductHelper
Description     : This class a test class for CPQ_OpportunityProductHelper. 
Created By      : Deepika Rawat
Created Date    : 25-Nov-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Deepika Rawat             25-Nov-15             Original version
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest (seeAllData=False) 
private class CPQ_Test_OpportunityProductHelper{
    static User TestAEUser;
    static Account oAccount;
    static product2 oProduct;
    static product2 oProductWireless;
    static product2 oProductWireline;
    static product2 oProductUCC;
    static product2 oProductService;
    static product2 oProductSecurity;
    static product2 oProductIoT;
    static product2 oProductDataCenter;
    static Opportunity oOpportunity;
    static Apttus_Proposal__Proposal__c oQuote;
    static Attachment attachmentQuote;
    static Apttus_Config2__PriceList__c oPriceList;
    static Apttus_Config2__PriceListItem__c oPriceListItem1;
    static Apttus_Config2__PriceListItem__c oPriceListItem2;
    static Apttus_Config2__PriceListItem__c oPriceListItem3;
    static Apttus_Config2__PriceListItem__c oPriceListItem4;
    static Apttus_Config2__PriceListItem__c oPriceListItem5;
    static Apttus_Config2__PriceListItem__c oPriceListItem6;
    static Apttus_Config2__PriceListItem__c oPriceListItem7;
    static ID prodConfigID;
    static Pricebook2 oPricebook;
    static Pricebook2 oPriceBookCustom;
    static List<Apttus_Config2__ProductConfiguration__c> lstProductConfig ;
    private static CPQ_Settings__c cs;
    static Id pricebookId = Test.getStandardPricebookId();
    static PricebookEntry pbEntry;
     /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testMethod void createTestData() {
         //Create custom Pricebook 
        oPriceBookCustom = CPQ_Test_Setup.newPriceBook('CPQ Temp PriceBook');
        insert oPriceBookCustom;
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        cs  = CPQ_Settings__c.getOrgDefaults();
        cs.DefaultOpportunityRecordType__c = 'Rogers EBU - New';
        upsert cs;
        //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');      
        system.runAs(TestAEUser){
            //Create newPriceBook
            oPricebook = CPQ_Test_Setup.newPriceBook('Test pricebook');
            insert oPricebook;
            //Create PriceList
            oPriceList = CPQ_Test_Setup.newPriceList();
            insert oPriceList ;
            // Create Products
            oProductWireline = CPQ_Test_Setup.newProduct();
            oProductWireline.Family = 'Enterprise Grade Networks - Wireline';
            insert oProductWireline;
            oProductUCC = CPQ_Test_Setup.newProduct();
            oProductUCC.Family = 'Unified Communication & Collaboration';
            insert oProductUCC;
            oProductService = CPQ_Test_Setup.newProduct();
            oProductService.Family = 'Services';
            insert oProductService;
            oProductSecurity = CPQ_Test_Setup.newProduct();
            oProductSecurity.Family = 'Security';
            insert oProductSecurity;
            oProductDataCenter = CPQ_Test_Setup.newProduct();
            oProductDataCenter.Family = 'Data Centres & Cloud';
            insert oProductDataCenter;
            oProductWireless = CPQ_Test_Setup.newProduct();
            oProductWireless.Family = 'Enterprise Grade Networks - Wireless';
            insert oProductWireless;
            oProductIoT = CPQ_Test_Setup.newProduct();
            oProductIoT.Family = 'IoT Applications';
            insert oProductIoT;
            //Create your pricebook entry
            pbEntry = new PricebookEntry(
                Pricebook2Id = pricebookId,
                Product2Id = oProductWireline.Id,
                UnitPrice = 100.00,
                IsActive = true
            );
            insert pbEntry;
            //Create Pricelist item
            oPriceListItem1 = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oProductWireline.ID);
            insert oPriceListItem1;
            oPriceListItem2 = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oProductUCC.ID);
            insert oPriceListItem2;
            oPriceListItem3 = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oProductService.ID);
            insert oPriceListItem3;
            oPriceListItem4 = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oProductSecurity.ID);
            insert oPriceListItem4;
            oPriceListItem5 = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oProductDataCenter.ID);
            insert oPriceListItem5;
            oPriceListItem6 = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oProductWireless.ID);
            insert oPriceListItem6;
            oPriceListItem7 = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oProductIoT.ID);
            insert oPriceListItem7;
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            oOpportunity.Pricebook2Id = pricebookId;
            insert oOpportunity;
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
            oQuote.Apttus_Proposal__Primary__c=true;
            insert oQuote;
            //Product Configuration
            prodConfigID = CPQ_Test_Setup.newProdictConfiguration(oQuote);
            //Create line items for all product types
            CPQ_Test_Setup.createLineItem(prodConfigID,oProductWireline.id,oPriceListItem1.id);
            CPQ_Test_Setup.createLineItem(prodConfigID,oProductUCC.id,oPriceListItem2.id);
            CPQ_Test_Setup.createLineItem(prodConfigID,oProductService.id,oPriceListItem3.id);
            CPQ_Test_Setup.createLineItem(prodConfigID,oProductSecurity.id,oPriceListItem4.id);
            CPQ_Test_Setup.createLineItem(prodConfigID,oProductDataCenter.id,oPriceListItem5.id);
            CPQ_Test_Setup.createLineItem(prodConfigID,oProductWireless.id,oPriceListItem6.id);
            CPQ_Test_Setup.createLineItem(prodConfigID,oProductIoT.id,oPriceListItem7.id);
        }
    }
    /*********************************************************************************
    Method Name    : testCalculateTCVOneTime
    Description    : Method to test testCalculate Functionality
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testCalculateTCVOneTime(){
        createTestData();
        system.runAs(TestAEUser){
            test.startTest();
            //Finalize Cart
            Apttus_CPQApi.CPQ.FinalizeCartRequestDO request = new Apttus_CPQApi.CPQ.FinalizeCartRequestDO();
            request.CartId = prodConfigID; 
            Apttus_CPQApi.CPQ.FinalizeCartResponseDO resp = Apttus_CPQApi.CPQWebService.finalizeCart(request);
            List<Apttus_Proposal__Proposal_Line_Item__c> lstQouteLineItems = [select id, Apttus_QPConfig__PriceType__c, Apttus_QPConfig__ExtendedPrice__c, Apttus_QPConfig__Term__c, Apttus_QPConfig__Quantity2__c, Apttus_Proposal__Proposal__c, Apttus_Proposal__Proposal__r.Apttus_Proposal__Opportunity__c, Apttus_Proposal__Product__r.Family  from Apttus_Proposal__Proposal_Line_Item__c where Apttus_Proposal__Proposal__c =:oQuote.id];
            List<Apttus_Proposal__Proposal_Line_Item__c> lstUpdateQouteLineItems = new List<Apttus_Proposal__Proposal_Line_Item__c>();
            if(lstQouteLineItems!=null && lstQouteLineItems.size()>0){
                for(Apttus_Proposal__Proposal_Line_Item__c item :lstQouteLineItems){
                    item.Apttus_QPConfig__NetPrice__c= 10;
                    lstUpdateQouteLineItems.add(item);
                }
            }
            update lstUpdateQouteLineItems;            
            //Create Opportunity Line Item
            OpportunityLineItem lineItem = new OpportunityLineItem();
            lineItem.PricebookEntryId=pbEntry.Id;
            lineItem.OpportunityID=oOpportunity.id;
            lineItem.Quantity =1;
            lineItem.TotalPrice = lineItem.Quantity * pbEntry.UnitPrice;
            insert lineItem;
            List<Opportunity> lstOpportunity= [Select IoT_Applications_Expected__c, Unified_Comm_Collaboration_Expected__c, Data_Centre_Cloud_Expected__c, Wireless_Expected__c, Wireline_Fixed_Access_Expected__c, Security_Expected__c, Services_Expected__c from Opportunity where id=:oOpportunity.id ];
            //Chech TCV calculated values on Opportunity
            System.assert(lstOpportunity[0].IoT_Applications_Expected__c ==10);
            System.assert(lstOpportunity[0].Unified_Comm_Collaboration_Expected__c ==10);
            System.assert(lstOpportunity[0].Data_Centre_Cloud_Expected__c ==10);
            System.assert(lstOpportunity[0].Wireless_Expected__c ==10);
            System.assert(lstOpportunity[0].Wireline_Fixed_Access_Expected__c ==10);
            System.assert(lstOpportunity[0].Security_Expected__c ==10);
            System.assert(lstOpportunity[0].Services_Expected__c ==10);
            test.stopTest();
        }
    }
}