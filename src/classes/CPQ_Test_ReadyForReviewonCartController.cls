/*********************************************************************************
Class Name      : CPQ_Test_ReadyForReviewonCartController 
Description     : This class a test class for CPQ_ProposalReadyForReviewController. 
Created By      : Deepika Rawat
Created Date    : 1-Nov-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Deepika Rawat              1-Nov-15              Original version
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest (seeAllData=False) 
private class CPQ_Test_ReadyForReviewonCartController{
    static User TestAEUser;
    static Account oAccount;
    static product2 oProduct;
    static product2 oProductWireless;
    static product2 oProductIoT;
    static Opportunity oOpportunity;
    static Apttus_Proposal__Proposal__c oQuote;
    static Attachment attachmentQuote;
    static Apttus_Config2__PriceList__c oPriceList;
    static Apttus_Config2__PriceListItem__c oPriceListItem;
    static Apttus_Config2__PriceListItem__c oPriceListItem2;
    static Apttus_Config2__PriceListItem__c oPriceListItem3;
    static ID prodConfigID;
    static Pricebook2 oPricebook;
    static Pricebook2 oPriceBookCustom;
    static List<Apttus_Config2__ProductConfiguration__c> lstProductConfig ;
    private static CPQ_Settings__c cs;
     /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testMethod void createTestData() {
         //Create custom Pricebook 
        oPriceBookCustom = CPQ_Test_Setup.newPriceBook('CPQ Temp PriceBook');
        insert oPriceBookCustom;
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        cs  = CPQ_Settings__c.getOrgDefaults();
        cs.DefaultOpportunityRecordType__c = 'Rogers EBU - New';
        upsert cs;
        //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');      
        system.runAs(TestAEUser){
            //Create newPriceBook
            oPricebook = CPQ_Test_Setup.newPriceBook('Test pricebook');
            insert oPricebook;
            //Create PriceList
            oPriceList = CPQ_Test_Setup.newPriceList();
            insert oPriceList ;
            // Create Products
            oProduct = CPQ_Test_Setup.newProduct();
            oProduct.Family = 'Wireless - Voice';
            insert oProduct;
            oProductWireless = CPQ_Test_Setup.newProduct();
            oProductWireless.Family = 'Enterprise Grade Networks - Wireless';
            insert oProductWireless;
            oProductIoT = CPQ_Test_Setup.newProduct();
            oProductIoT.Family = 'IoT';
            insert oProductIoT;
            //Create Pricelist item
            oPriceListItem = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oProduct.ID);
            insert oPriceListItem;
            oPriceListItem2 = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oProductWireless.ID);
            insert oPriceListItem2;
            oPriceListItem3 = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oProductIoT.ID);
            insert oPriceListItem3;
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
            insert oQuote;
            //Product Configuration
            prodConfigID = CPQ_Test_Setup.newProdictConfiguration(oQuote);
           // lstProductConfig = [Select id, Apttus_QPConfig__Proposald__c, Apttus_Config2__Status__c from  Apttus_Config2__ProductConfiguration__c where Apttus_QPConfig__Proposald__c =:oQuote.id ];
        }
    }
    /*********************************************************************************
    Method Name    : testReadyForReviewSavedConfig
    Description    : Test "Ready For Review" as AE with Saved Quote
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testReadyForReviewSavedConfig(){
        createTestData();
        //Create line items
        CPQ_Test_Setup.createLineItem(prodConfigID,oProduct.id,oPriceListItem.id);
        system.runAs(TestAEUser){
            test.startTest();
            ApexPages.currentPage().getParameters().put('id', prodConfigID);
            CPQ_ReadyForReviewonCartController obj = new CPQ_ReadyForReviewonCartController();
            obj.updateQuote();
            test.stopTest();
            List<Apttus_Proposal__Proposal__c> lstTestProposal= [select id, RecordTypeId , Apttus_Proposal__Approval_Stage__c from Apttus_Proposal__Proposal__c where id=:oQuote.id];
            List<Apttus_Config2__ProductConfiguration__c> lstTestProductConfig = [Select id, Apttus_QPConfig__Proposald__c, Apttus_Config2__Status__c from  Apttus_Config2__ProductConfiguration__c where Apttus_QPConfig__Proposald__c =:oQuote.id ];
            System.assert(lstTestProposal[0].Apttus_Proposal__Approval_Stage__c == 'Solution Under Review');
            System.assert(lstTestProductConfig[0].Apttus_Config2__Status__c == 'Finalized');
        }
    }
    /*********************************************************************************
    Method Name    : testReadyForReviewWithWireLess
    Description    : Test "Ready For Review" as AE with Saved Quote
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testReadyForReviewWithWireLess(){
        createTestData();
        //Create line items
        CPQ_Test_Setup.createLineItem(prodConfigID,oProductWireless.id,oPriceListItem2.id);
        system.runAs(TestAEUser){
            test.startTest();
            ApexPages.currentPage().getParameters().put('id', prodConfigID);
            CPQ_ReadyForReviewonCartController obj = new CPQ_ReadyForReviewonCartController();
            obj.updateQuote();
            test.stopTest();
            List<Apttus_Proposal__Proposal__c> lstTestProposal= [select id, RecordTypeId , Apttus_Proposal__Approval_Stage__c from Apttus_Proposal__Proposal__c where id=:oQuote.id];
            List<Apttus_Config2__ProductConfiguration__c> lstTestProductConfig = [Select id, Apttus_QPConfig__Proposald__c, Apttus_Config2__Status__c from  Apttus_Config2__ProductConfiguration__c where Apttus_QPConfig__Proposald__c =:oQuote.id ];
            System.assert(lstTestProposal[0].Apttus_Proposal__Approval_Stage__c == 'Approval Required');
            System.assert(lstTestProductConfig[0].Apttus_Config2__Status__c == 'Finalized');
        }
    }
    /*********************************************************************************
    Method Name    : testReadyForReviewWithIoT
    Description    : Test "Ready For Review" as AE with Saved Quote
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testReadyForReviewWithIoT(){
        createTestData();
        //Create line items
        CPQ_Test_Setup.createLineItem(prodConfigID,oProductIoT.id,oPriceListItem3.id);
        system.runAs(TestAEUser){
            test.startTest();
            ApexPages.currentPage().getParameters().put('id', prodConfigID);
            CPQ_ReadyForReviewonCartController obj = new CPQ_ReadyForReviewonCartController();
            obj.updateQuote();
            test.stopTest();
            List<Apttus_Proposal__Proposal__c> lstTestProposal= [select id, RecordTypeId , Apttus_Proposal__Approval_Stage__c from Apttus_Proposal__Proposal__c where id=:oQuote.id];
            List<Apttus_Config2__ProductConfiguration__c> lstTestProductConfig = [Select id, Apttus_QPConfig__Proposald__c, Apttus_Config2__Status__c from  Apttus_Config2__ProductConfiguration__c where Apttus_QPConfig__Proposald__c =:oQuote.id ];
            System.assert(lstTestProposal[0].Apttus_Proposal__Approval_Stage__c == 'Solution Under Review');
            System.assert(lstTestProductConfig[0].Apttus_Config2__Status__c == 'Finalized');
        }
    }
}