/**********************************************************************************
Class Name      : CPQ_AgreementTriggerHelper
Description     : This class is used as a helper class for Agreement Trigger 
Created By      : Metali Telang
Created Date    : 21-Aug-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Mitali Telang            21-Aug-2015            Created class for Agreement Trigger
Aakanksha Patel          26-Aug-2015            Method for Updating 'Has Meca' field on Account.
                         09-Oct-2015            Method for Case Creation after approval of MECA Amendment Agreement
Deepika Rawat            31-Aug-2015            Method for copying Primary Quote Document from quote to Agreement.
Ramiya Kandeepan         29-Sept-2015           Method for updating owner when bypassing MECA approval process
Akshay Ahluwalia         24-Sept-2015           Method for updating RecordType on Agreement  
Alina Balan              30-October-2015        Added "setSharingsRules", "getProfilesAndAccess" methods, used for sharing agreements, US-0364
Alina Balan              03-November-2015       Added "closeOpportunities" method, used for closing an opportunity, US-0292
Joseph Toms              01-Dec-2015            Modified closeOpportunities" method, used for closing an opportunity, US-0292
Alina Balan              17-December-2015       Added "setESignatureAgreementStatuses", US-0436, US-0503
Akshay Ahluwalia         18-December-2015       Modified UpdateRecordType on Agreement Method, US-122
Alina Balan              08-January-2016        Removed "getProfilesAndAccess" method, added "setSharingsRulesBasedOnOpportunityTeam" and "userIsOnAccountTeam" methods, US-0507
*********************************************************************************/
public class CPQ_AgreementTriggerHelper{
    private static final String CPQ_SETTINGS_AGREEMENTS_PROFILES_SPLITTER = '\\|';
    private static final String CPQ_SETTINGS_AGREEMENTS_STATUS_SPLITTER = ',';
    private static final String CPQ_STATUS_CATEGORY_IN_EFFECT = 'In Effect';
    private static final String CPQ_STATUS_ACTIVATED = 'Activated';
    private static final String CPQ_OPPORTUNITY_STAGE_CLOSED = 'Closed Won';
    private static final String CPQ_STATUS_SIGNATURE_NOTREQUIRED = 'Signature Not Required';
    private static final String CPQ_STATUS_CUSTOMER_SIGN_RECEIVED = 'Customer Signatures Received';
    private static final String CPQ_STATUS_CATEGORY_IN_SIGNATURE = 'In Signatures';
    private static final String CPQ_STATUS_CATEGORY_IN_REQUEST ='Request';
    private static final String CPQ_STATUS_INTERNAL_SIGNATURE='Internal Signatures';
    
    /*********************************************************************************
    Method Name    : createDocumentOnAgreement
    Description    : Method for copying Primary Quote Document from quote to Agreement.
    Return Type    : Void
    Parameter      : List<Apttus__APTS_Agreement__c>
    *********************************************************************************/
    public static void  createDocumentOnAgreement(List<Apttus__APTS_Agreement__c> lstAgreement){
        List<id> lstProposalIDs = new List<id>();
        List<Attachment> lstAttachments = new List<Attachment>();
        Map<String, Attachment> mapAttachment = new Map<String, Attachment>();
        List<Attachment> lstNewAttachments = new List<Attachment>();
        Map<id,String> mapQuoteDocId = new Map<id,String>();
        List<Apttus_Proposal__Proposal__c> lstProposal = new List<Apttus_Proposal__Proposal__c>();
        // Create list of related Proposal IDs: lstProposalIDs
        for(Apttus__APTS_Agreement__c agreement : lstAgreement){
            if(agreement.Apttus_QPComply__RelatedProposalId__c!=null){
                lstProposalIDs.add(agreement.Apttus_QPComply__RelatedProposalId__c);
            }
        }
        lstProposal = [Select Primary_Quote_Document_ID__c from Apttus_Proposal__Proposal__c where id in:lstProposalIDs];
        // Create map of Proposal and it's related Primary_Quote_Document_ID__c, if Primary_Quote_Document_ID__c is not null
        for(Apttus_Proposal__Proposal__c proposal:lstProposal){
            if(proposal.Primary_Quote_Document_ID__c!=null && proposal.Primary_Quote_Document_ID__c!='')
            mapQuoteDocId.put(proposal.id,proposal.Primary_Quote_Document_ID__c);
        }
        lstAttachments = [SELECT Id, Name, ParentId, CreatedDate, Body FROM Attachment WHERE Id in:mapQuoteDocId.Values() ORDER BY createddate desc];

        for(Attachment doc : lstAttachments){
            mapAttachment.put(String.valueOf(doc.id), doc);
        }
        system.debug('lstAgreement******'+lstAgreement);
        system.debug('mapAttachment******'+mapAttachment);
        //Create list of documents to be pushed on Agreement records
        for(Apttus__APTS_Agreement__c agreement : lstAgreement){
            String sDocId = mapQuoteDocId.get(agreement.Apttus_QPComply__RelatedProposalId__c);
            system.debug('sDocId******'+sDocId);
            Attachment cloneDoc = mapAttachment.get(sDocId);
            system.debug('cloneDoc******'+cloneDoc);
            //if there is existing Primary doc on Quote create new attachment record to be created on Agreement
            if(cloneDoc!=null){
                Attachment newDoc = cloneDoc.clone();
                newDoc.ParentId = agreement.id;
                newDoc.Body = cloneDoc.Body;
                lstNewAttachments.add(newDoc);
            }
        }
        system.debug('lstNewAttachments******'+lstNewAttachments);
        //Insert created attechment records.
        if(lstNewAttachments.size()>0){
            insert lstNewAttachments;
        }
     }
    
    /*********************************************************************************
    Method Name    : updateAccountOnAgreementUpdate
    Description    : Update Account Field Has MECA, when Agreement is Activated.
    Return Type    : Void
    Parameter      : List<Apttus__APTS_Agreement__c>, Map<Id, Apttus__APTS_Agreement__c>
    Developer      : Akshay Ahluwalia 
    Date           : 29-Sept-2015
    
    Modified       : Aakanksha Patel (US-0055)
    *********************************************************************************/
    
     public static void updateAccountOnAgreementUpdate(List<Apttus__APTS_Agreement__c> lstNewAgreements, Map<Id, Apttus__APTS_Agreement__c> oldMap){
        List<Id> lstAccountIds =  new List<Id>();
        Map<Id, sObject> mapAccountIdAgreementMap = new map<Id, sObject>();
        List<RecordType> lstMecaAgreementRecordId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Rogers_EBU_MECA' AND SobjectType = 'Apttus__APTS_Agreement__c'];
        
        //Iterating through list of Agreements updated
        for(Apttus__APTS_Agreement__c agreement : lstNewAgreements)
        {
            //Check for the Required Conditions and null checks
            if(agreement.Apttus__Status_Category__c != null && agreement.Apttus__Status_Category__c == CPQ_STATUS_CATEGORY_IN_SIGNATURE && agreement.Apttus__Status__c != null && agreement.Apttus__Status__c == CPQ_STATUS_CUSTOMER_SIGN_RECEIVED && agreement.Apttus__Account__c != null){
              
              //Checking for Recordtype
              if(agreement.recordTypeId == lstMecaAgreementRecordId[0].id) 
                {
                    lstAccountIds.add(agreement.Apttus__Account__c);
                    mapAccountIdAgreementMap.put(agreement.Apttus__Account__c, agreement);
                    
                }
            }
        }
        List<Account> lstAccountsToBeUpdated = [select id, has_MECA__c from Account where Id in : lstAccountIds];
        
        //Updating the Has Meca Field on the Related Accounts
        for(Account acc : lstAccountsToBeUpdated)
        {
            acc.has_MECA__c = true;
        }
        //Updating the Accounts for corresponding Agreements
        if(lstAccountsToBeUpdated != null && lstAccountsToBeUpdated.size() > 0)
        {
            update lstAccountsToBeUpdated;
        }
    }
     /*********************************************************************************
    Method Name    : updateRecordTypeOnInsert
    Description    : Method for updating the Record type of the agreement based on has Meca 
                     Flag on Account
    Return Type    : Void
    Parameter      : List<Apttus__APTS_Agreement__c>
    Developer      : Akshay Ahluwalia
    Date           : 29-Sept-2015
    *********************************************************************************/
    
    public static void updateRecordTypeOnInsert(List<Apttus__APTS_Agreement__c> newAgreements){
        list<Id> lstAgreementOptyIds = new List<Id>();
        Map<Id, String> mapOpptyIdPriceList = new Map<id, String>(); 
        
        //Iterating agreement to get Opportunity Ids 
        for(Apttus__APTS_Agreement__c agreement : newAgreements){
            lstAgreementOptyIds.add(agreement.Apttus__Related_Opportunity__c);
        }
        List<opportunity> lstRelatedOptyList = [select Id, name, accountId, Price_List__c from opportunity where id in :lstAgreementOptyIds];
        //Iterating over Opportunity to get Oppty Price list Map
        for(Opportunity oppty : lstRelatedOptyList){
            mapOpptyIdPriceList.put(oppty.id, oppty.Price_List__c);
        }
        List<Id> lstAgreementAccountIds = new list<Id>();
        //Itereating the Agreements to get Account Ids
        for(Apttus__APTS_Agreement__c agreement : newAgreements){
            //Checking if Account Exists for the Agreement
            if(agreement.Apttus__Account__c != null){
                lstAgreementAccountIds.add(agreement.Apttus__Account__c);    
            }
            //if Account does not exist checking for the opportunity Account.
            else{
                for(opportunity opty : lstRelatedOptyList){
                    lstAgreementAccountIds.add(opty.accountId);
                    if(agreement.Apttus__Related_Opportunity__c == opty.id){
                        agreement.Apttus__Account__c  = opty.accountId;
                    }
                }
            }
        }
        //Quering of account record to get MECA Flag.
        List<Account> lstAgreementAccounts = [select id, name, Has_MECA__c, RecordType.Name, Interco_Account__c from Account where Id  in : lstAgreementAccountIds];
      /*  Id MecaAgreementRecordId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Rogers EBU - MECA').getRecordTypeId();
        Id IDVAgreementRecordId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Rogers EBU - IDV').getRecordTypeId();
        Id SimplifiedAgreementRecordId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Rogers EBU - Simplified').getRecordTypeId();
        Id CarrierAgreementRecordId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Rogers WBU - Carrier').getRecordTypeId();        
        Id MecaAmendmentRecordTypId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Rogers EBU - MECA Amendment').getRecordTypeId();
        Id LegalNameRecordTypId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Rogers EBU Agreement - Legal Name Change').getRecordTypeId();
    */  
    
        //Modified by Aakanksha for querying the RecordTypes by developer name instead of getting by label
        List<RecordType> MecaAgreementRecordId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Rogers_EBU_MECA' AND SobjectType = 'Apttus__APTS_Agreement__c'];
        List<RecordType> IDVAgreementRecordId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Rogers_EBU_IDV' AND SobjectType = 'Apttus__APTS_Agreement__c'];
        List<RecordType> SimplifiedAgreementRecordId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Rogers_EBU_Simplified' AND SobjectType = 'Apttus__APTS_Agreement__c'];
        List<RecordType> CarrierAgreementRecordId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Rogers_WBU_Carrier' AND SobjectType = 'Apttus__APTS_Agreement__c'];
        List<RecordType> MecaAmendmentRecordTypId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Rogers_EBU_MECA_Amendment' AND SobjectType = 'Apttus__APTS_Agreement__c'];
        List<RecordType> LegalNameRecordTypId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Rogers_EBU_Agreement_Legal_Name_Change' AND SobjectType = 'Apttus__APTS_Agreement__c'];
        
        
        //Iterating Account records to Assign recordtype Id based on Account 
        for(Account account : lstAgreementAccounts){
            //Iterating Agreement records to Assign recordtypeId based on Account
            for(Apttus__APTS_Agreement__c agreement : newAgreements ){
                //Updating Record type Based on price list
                if(agreement.Apttus__Account__c == account.Id){
                    //Condition to not Modify the Record type if Legal Name change Record type exists
                    if(agreement.recordTypeId != LegalNameRecordTypId[0].id)
                    {   
                        //Condition to Check carrier Account to update for Carrier Record Type 
                        if(account.RecordType.Name.equalsIgnoreCase('Carrier Account')){
                            agreement.recordTypeId = CarrierAgreementRecordId[0].id;
                        }
                        //Defaulting Simplified Record type for Interco Account
                        else if(account.Interco_Account__c == true){
                            agreement.recordTypeId = SimplifiedAgreementRecordId[0].id;
                            agreement.Apttus__Status__c = CPQ_STATUS_SIGNATURE_NOTREQUIRED ;
                            agreement.Apttus__Status_Category__c = CPQ_STATUS_CATEGORY_IN_REQUEST;
                        
                        }
                        //Defaulting IDV record type for IDV price List
                        else if(agreement.Apttus__Related_Opportunity__c != null && mapOpptyIdPriceList.get(agreement.Apttus__Related_Opportunity__c) != null && mapOpptyIdPriceList.get(agreement.Apttus__Related_Opportunity__c).equalsIgnoreCase('IDV')){
                            agreement.recordTypeId = IDVAgreementRecordId[0].id;
                        }
                        //Defaulting PED Record type for PED price list.
                        else if((agreement.Apttus__Related_Opportunity__c != null && mapOpptyIdPriceList.get(agreement.Apttus__Related_Opportunity__c) != null && mapOpptyIdPriceList.get(agreement.Apttus__Related_Opportunity__c).equalsIgnoreCase('PED'))){
                            agreement.recordTypeId = SimplifiedAgreementRecordId[0].id;
                            agreement.Apttus__Status__c = CPQ_STATUS_SIGNATURE_NOTREQUIRED ;
                            agreement.Apttus__Status_Category__c = CPQ_STATUS_CATEGORY_IN_REQUEST;
                        }
                        //Defaulting MECA Amendment Record type    
                        else if(account.Has_MECA__c){
                            agreement.recordTypeId = MecaAmendmentRecordTypId[0].id;
                        }
                        //Defaulting MECA Record type
                        else{
                            agreement.recordTypeId = MecaAgreementRecordId[0].id;
                        }
                    }
                }
            }
        
        }
        
    
    }
        
  
    /*********************************************************************************
    Method Name    : createCaseRecAgreement
    Description    : Method for creating a new case for legal name change for a MECA Amemndment Agreement type.
    Return Type    : Void
    Parameter      : List<Apttus__APTS_Agreement__c>
    Developer      : Aakanksha Patel
    *********************************************************************************/
    
    public static void  createCaseRecAgreement(LIST<Apttus__APTS_Agreement__c> listFromTrigger)
    {                   
        Set<ID> setApprovedAgreementIDs = new Set<ID>();
        Set<ID> setAccAgreementIds = new Set<ID>();
        Map<Id, sObject> mapCaseIdAgreementInsert = new map<Id, sObject>();
        Map<Id, sObject> mapCaseIdAgreementUpdate = new map<Id, sObject>();
        
        //Iterating to get Agreement Ids
        for(Apttus__APTS_Agreement__c agreement : listFromTrigger)
        {
            setApprovedAgreementIDs.add(agreement.Id);
            
            if(agreement.Apttus__Account__c!=Null)
                setAccAgreementIds.add(agreement.Apttus__Account__c);
        }
       
        List<RecordType> caseRecTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Account_Update' AND SobjectType = 'Case'];
        LIST<Apttus__APTS_Agreement__c> lstAgreements= [select id,Apttus__Account__c ,Apttus__Status__c, Apttus_Approval__Approval_Status__c,Apttus__Status_Category__c, RecordType.Name,NewLegalName__c,OldLegalName__c,Requestor_ID__c, OwnerId, RecordType.DeveloperName from Apttus__APTS_Agreement__c where RecordType.DeveloperName='Rogers_EBU_Agreement_Legal_Name_Change'  AND Id in: setApprovedAgreementIDs]; 
         
        LIST<Case> lstCase = new LIST<Case>(); //List for inserting new Cases
        LIST<Case> lstCaseUpdate = new LIST<Case>(); //List for Updating present Cases
        
        List<Case> lstAccRelatedCases = new List<Case>([Select id,status,AccountID,RecordTypeid from Case where RecordTypeid =: caseRecTypeId[0].id and AccountID in: setAccAgreementIds and status= 'new' ]); //List of all related existing cases to be updated if the condition meets
        Map<Id,List<Case>> mapAccIDCase = new Map<Id,List<Case>>();
        List<Case> lstC = new List<Case>();
        
        //Account and all its Cases with 'Account Update' as RecordType
        for(Case c:lstAccRelatedCases)  
        {
            lstC = mapAccIDCase.get(c.AccountID);
            if(lstC == null)
            {   
                lstC = new List<Case>();
            }
            lstC.add(c);
            mapAccIDCase.put(c.AccountID,lstC);
        }
        
        //Iterating over new Approved agreements to create a case for each of them
        for(Apttus__APTS_Agreement__c apts :lstAgreements)
        {
             //Null check for the required Agreement fields
             if(apts.NewLegalName__c!= null || apts.OldLegalName__c!= null || apts.Apttus__Account__c != null)
             {
                 //Checking for the required Status. Status Category and RecordType of the Agreement
                 if(apts.Apttus__Status_Category__c == 'In Effect' && apts.Apttus__Status__c == 'Activated' && apts.RecordType.DeveloperName == 'Rogers_EBU_Agreement_Legal_Name_Change' && apts.Apttus__Account__c != null && apts.Apttus__Status_Category__c != null && apts.Apttus__Status__c != null) 
                 {
                        if(mapAccIDCase.get(apts.Apttus__Account__c)!=null)
                        {
                             //updating new company name for existing case record
                             List<Case> lstCaseExist = mapAccIDCase.get(apts.Apttus__Account__c);
                             if(lstCaseExist[0]!=null  || lstCaseExist[0].AccountID!=null)
                             {
                                lstCaseExist[0].New_Company_Name__c = apts.NewLegalName__c;
                                lstCaseExist[0].Origin= null;
                             }
                             lstCaseUpdate.add(lstCaseExist[0]);
                        
                             //Map for the AddErrorsToRecord call while updating existing Case
                              mapCaseIdAgreementUpdate.put(lstCaseExist[0].id,apts);
                            
                        }
                        else
                        {
                              //New Case is being created
                              Case oCase = new Case();
                              oCase.New_Company_Name__c = apts.NewLegalName__c;
                              oCase.AccountID = apts.Apttus__Account__c;
                              oCase.Origin= null;
                              oCase.RecordTypeid = caseRecTypeId[0].id;
                              lstCase.add(oCase);
                              
                              //Map for the AddErrorsToRecord call while inserting new Case
                              mapCaseIdAgreementInsert.put(oCase.id,apts);
                             
                        }
                }
             }
        }
       
        
        //Inserting the Case for corresponding Agreements with Record type as 'Account Update' for a legal name change
        if(lstCase != null && lstCase.size() > 0)
        {
           //Inserting the Case Record and Capturing Errors in Database 
            list<Database.SaveResult> lstResultsInsert = Database.Insert(lstCase, false);
        }
        
        //Update existing Case Record for corresponding Agreements  with the New Company Name 
        if(lstCaseUpdate != null && lstCaseUpdate.size() > 0)
        {
            //Updating the Case Record and Capturing Errors in Database 
            update lstCaseUpdate;
        }
        
    }
    
     /*********************************************************************************
     Method Name    : setSharingsRules
     Description    : Method used to share agreements with the users that are a part of Account Team, for Roger Specialist profiles
     Return Type    : void
     Parameter      : 1. List<Apttus__APTS_Agreement__c> lstAgreements : list of newly inserted agreements
     *********************************************************************************/
    public static void setSharingsRules(List<Apttus__APTS_Agreement__c> lstAgreements) {
        Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
        Set<Id> setAccountIds = new Set<Id>();
        List<AccountTeamMember> lstAccountMembers;
        Map<Id, List<AccountTeamMember>> mapAccountTeams = new Map<Id, List<AccountTeamMember>>();
        List<Apttus__APTS_Agreement__Share> lstAgreementSharesToInsert = new List<Apttus__APTS_Agreement__Share>();
        List<Profile> lstProfiles = new List<Profile>();
        Set<Id> setUserIds = new Set<Id>();
        Map<Id, User> mapUsers;
        //iterating through all proposals and get all accounts ids
        for(Apttus__APTS_Agreement__c agreement : lstAgreements){
            setAccountIds.add(agreement.Apttus__Account__c);
        }
        lstAccountMembers = [SELECT AccountId, UserId FROM AccountTeamMember WHERE AccountId IN: setAccountIds];
        //iterating through all account team members and get all users ids and build the account team members map
        for (AccountTeamMember accountTeamMember : lstAccountMembers) {
            setUserIds.add(accountTeamMember.UserId);
            List<AccountTeamMember> lstAccountTeamMemberTemp = mapAccountTeams.get(accountTeamMember.AccountId);
            //check the accounte team members list for the current account 
            if (lstAccountTeamMemberTemp == null) {
                lstAccountTeamMemberTemp = new List<AccountTeamMember>();
            }
            lstAccountTeamMemberTemp.add(accountTeamMember);  
            mapAccountTeams.put(accountTeamMember.AccountId, lstAccountTeamMemberTemp );   
        }
        mapUsers = new Map<Id, User>([SELECT Id, Profile.name FROM user WHERE Id IN: setUserIds]);
        //for each agreement, check conditions and insert share record
        for (Apttus__APTS_Agreement__c agreement : lstAgreements) {
            List<AccountTeamMember> lstAccountTeamMemberForAgreement = mapAccountTeams.get(agreement.Apttus__Account__c);
            //check if the agreement account has any account team member
            if (lstAccountTeamMemberForAgreement != null && lstAccountTeamMemberForAgreement.size() > 0 && mapUsers != null) {
                //iterateing throungh all the team members
                for(AccountTeamMember accountTeamMember : lstAccountTeamMemberForAgreement) {
                    //check if the mapUsers contains the current account Team Member
                    if (mapUsers.get(accountTeamMember.UserId) == null) continue;
                    //if the user from account team member is not the owner
                    if (agreement.OwnerId != accountTeamMember.UserId) {
                        Apttus__APTS_Agreement__Share oAgreementShare = new Apttus__APTS_Agreement__Share();
                        oAgreementShare.ParentID = agreement.Id;
                        oAgreementShare.UserOrGroupId = accountTeamMember.UserId;
                        oAgreementShare.AccessLevel = TAGsettings.Team_Custom_Object_Access__c;
                        //oAgreementShare.RowCause = Schema.Apttus__APTS_Agreement__Share.RowCause.AccountTeamMember__c;
                        lstAgreementSharesToInsert.add(oAgreementShare);  
                    }
                }
            }
        }
        //insert shared records
        if (lstAgreementSharesToInsert.size()> 0) {
            insert lstAgreementSharesToInsert;
        }
    }
    
    /*********************************************************************************
     Method Name    : setSharingsRulesBasedOnOpportunityTeam
     Description    : Method used to share agreements with the users that are a part of Opportunity Team, for Roger Specialist profiles
     Return Type    : void
     Parameter      : 1. List<Apttus__APTS_Agreement__c> lstAgreements : list of newly inserted agreements
     *********************************************************************************/
    /*public static void setSharingsRulesBasedOnOpportunityTeam(List<Apttus__APTS_Agreement__c> lstAgreements) {
        Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
        Set<Id> setOpportunityIds = new Set<Id>();
        Set<Id> setAccountIds = new Set<Id>();
        List<OpportunityTeamMember> lstOpportunityMembers;
        List<AccountTeamMember> lstAccountMembers;
        Map<Id, List<OpportunityTeamMember>> mapOpportunityTeams = new Map<Id, List<OpportunityTeamMember>>();
        List<Apttus__APTS_Agreement__Share> lstAgreementSharesToInsert = new List<Apttus__APTS_Agreement__Share>();
        List<Profile> lstProfiles = new List<Profile>();
        Set<Id> setUserIds = new Set<Id>();
        Map<Id, User> mapUsers;
        //iterating through all proposals and get all opportunity ids
        for(Apttus__APTS_Agreement__c agreement : lstAgreements){
            setOpportunityIds.add(agreement.Apttus__Related_Opportunity__c);
            setAccountIds.add(agreement.Apttus__Account__c);
        }
        lstOpportunityMembers = [SELECT OpportunityId, UserId FROM OpportunityTeamMember WHERE OpportunityId IN: setOpportunityIds];
        lstAccountMembers = [SELECT AccountId, UserId FROM AccountTeamMember WHERE AccountId IN: setAccountIds];
        //iterating through all account team members and get all users ids and build the account team members map
        for (OpportunityTeamMember opportunityTeamMember : lstOpportunityMembers) {
            setUserIds.add(opportunityTeamMember.UserId);
            List<OpportunityTeamMember> lstOpportunityTeamMemberTemp = mapOpportunityTeams.get(opportunityTeamMember.OpportunityId);
            //check the accounte team members list for the current account 
            if (lstOpportunityTeamMemberTemp == null) {
                lstOpportunityTeamMemberTemp = new List<OpportunityTeamMember>();
            }
            lstOpportunityTeamMemberTemp.add(opportunityTeamMember);  
            mapOpportunityTeams.put(opportunityTeamMember.OpportunityId, lstOpportunityTeamMemberTemp );
        }
        mapUsers = new Map<Id, User>([SELECT Id, Profile.name FROM User WHERE Id IN: setUserIds]);
        //for each agreement, check conditions and insert share record
        Boolean oppTeamMemberIsOnAccountTeam;
        for (Apttus__APTS_Agreement__c agreement : lstAgreements) {
            List<OpportunityTeamMember> lstOpportunityTeamMemberForAgreement = mapOpportunityTeams.get(agreement.Apttus__Related_Opportunity__c);
            //check if the agreement account has any account team member
            if (lstOpportunityTeamMemberForAgreement != null && lstOpportunityTeamMemberForAgreement.size() > 0 && mapUsers != null) {
                //iterateing throungh all the team members
                for(OpportunityTeamMember opportunityTeamMember : lstOpportunityTeamMemberForAgreement) {
                    //check if the mapUsers contains the current opp Team Member
                    if (mapUsers.get(opportunityTeamMember.UserId) == null) continue;
                    //check if the user from opportunity team member is not the agreement owner AND is not the account owner AND is not in the account team AND is not hte opportunity owner
                    oppTeamMemberIsOnAccountTeam = userIsOnAccountTeam(lstAccountMembers, agreement.Apttus__Account__c, opportunityTeamMember.UserId);
                    if (agreement.OwnerId != opportunityTeamMember.UserId && agreement.Apttus__Account__r.OwnerId != opportunityTeamMember.UserId && !oppTeamMemberIsOnAccountTeam && 
                       agreement.Apttus__Related_Opportunity__r.OwnerId != opportunityTeamMember.UserId) {
                        Apttus__APTS_Agreement__Share oAgreementShare = new Apttus__APTS_Agreement__Share();
                        oAgreementShare.ParentID = agreement.Id;
                        oAgreementShare.UserOrGroupId = opportunityTeamMember.UserId;
                        oAgreementShare.AccessLevel = TAGsettings.Team_Custom_Object_Access__c;
                        lstAgreementSharesToInsert.add(oAgreementShare);  
                    }
                }
            }
        }
        //insert shared records
        if (lstAgreementSharesToInsert.size()> 0) {
            insert lstAgreementSharesToInsert;
        }
    }*/
    
     /*********************************************************************************
     Method Name    : userIsOnAccountTeam
     Description    : Method used to check if a user is a part of account team, for a given account
     Return Type    : Boolean
     Parameter      : List<AccountTeamMember> lstAccountMembers, Id accountId, Id oppTeamMemberId
     *********************************************************************************/
    public static Boolean userIsOnAccountTeam(List<AccountTeamMember> lstAccountMembers, Id accountId, Id oppTeamMemberId) {
        //iterating through the list of account members
        for (AccountTeamMember accountTeamMember : lstAccountMembers) {
            //check the record with the given account and user ids
            if (accountTeamMember.AccountId == accountId && accountTeamMember.UserId == oppTeamMemberId) return true;
        }
        return false;
    }
    
    /*********************************************************************************
     Method Name    : closeOpportunity
     Description    : Method used to close opportunities for signed agreements
     Return Type    : void
     Parameter      : 1. List<Apttus__APTS_Agreement__c> lstAgreements : list of newly inserted/updated agreements
     *********************************************************************************/
     public static void closeOpportunities(List<Apttus__APTS_Agreement__c> lstAgreements, Map<Id, Apttus__APTS_Agreement__c> oldMap) {
        List<Id> lstOpportunityIds = new List<Id>();
        //get status and category staus from custom settings
        CPQ_Settings__c cs = CPQ_Settings__c.getInstance();
        List<String>  lstCategoryStatus = new List<String>();
        List<String>  lstStatus = new List<String>();
        if(cs.Agreement_Category_Status_to_Close_Opp__c != null && cs.Agreement_Status_to_Close_Opp__c != null){
            lstCategoryStatus = cs.Agreement_Category_Status_to_Close_Opp__c.split(CPQ_SETTINGS_AGREEMENTS_STATUS_SPLITTER);
            lstStatus = cs.Agreement_Status_to_Close_Opp__c.split(CPQ_SETTINGS_AGREEMENTS_STATUS_SPLITTER);
        }
        Set<String> setCategoryStatus = new Set<String>(lstCategoryStatus);
        Set<String> setStatus = new Set<String>(lstStatus);
        //for each agreement, check if status category and status changed
        for (Apttus__APTS_Agreement__c agreement : lstAgreements) {
            Boolean bAgreementWasSigned = false;
            //for insert do not check old map values
            if (oldMap != null) {
                //if the status and status category were changed, update action
                if(agreement.Apttus__Status_Category__c != null && setCategoryStatus.contains(agreement.Apttus__Status_Category__c) 
                            && (!agreement.Apttus__Status_Category__c.equalsIgnoreCase(oldMap.get(agreement.Id).Apttus__Status_Category__c) || oldMap == null)
                            && agreement.Apttus__Status__c != null && setStatus.contains(agreement.Apttus__Status__c)
                            && (!agreement.Apttus__Status__c.equalsIgnoreCase(oldMap.get(agreement.Id).Apttus__Status__c) || oldMap == null)) {
                    bAgreementWasSigned = true;
                 // for Agreement in "Request" status category and status in "Signature Not Required"                       
                } else if(agreement.Apttus__Status_Category__c != null && setCategoryStatus.contains(agreement.Apttus__Status_Category__c) 
                            && agreement.Apttus__Status__c != null && setStatus.contains(agreement.Apttus__Status__c)){
                    bAgreementWasSigned = true; 
                }
            } else if(agreement.Apttus__Status_Category__c != null && setCategoryStatus.contains(agreement.Apttus__Status_Category__c)
                     && agreement.Apttus__Status__c != null && setStatus.contains(agreement.Apttus__Status__c)) {
                    bAgreementWasSigned = true;
            }
            //check opportunity status category and status
            if (bAgreementWasSigned) {
                //check for a not null opportunity id
                if (agreement.Apttus__Related_Opportunity__c == null) continue;
                    lstOpportunityIds.add(agreement.Apttus__Related_Opportunity__c);
            }
        }
        List<Opportunity> lstOpportunities = [SELECT Id, StageName FROM Opportunity WHERE Id IN: lstOpportunityIds];

        List<RecordType> opportunityClosedRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Rogers_EBU_Closed' AND SobjectType = 'Opportunity'];
        
        //for every opportunity related, update the status and record type
        for (Opportunity opportunity : lstOpportunities) {
            if (opportunity.StageName == CPQ_OPPORTUNITY_STAGE_CLOSED) continue;
            opportunity.StageName = CPQ_OPPORTUNITY_STAGE_CLOSED;
            opportunity.CloseDate = Date.today();
            opportunity.RecordTypeId = opportunityClosedRecordTypeId[0].id;
         }
        //update opportunities list
        if (lstOpportunities.size()> 0) {
            update lstOpportunities;
        }
    }
    
    /*********************************************************************************
    Method Name    : setESignatureAgreementStatuses
    Description    : Method used to set the right status when a doc is sent for eSignature
    Return Type    : 
    Parameter      : List<Apttus__APTS_Agreement__c> lstAgreements, Map<Id, Apttus__APTS_Agreement__c> mapOldAgreements
    *********************************************************************************/
    public static void setESignatureAgreementStatuses(List<Apttus__APTS_Agreement__c> lstAgreements, Map<Id, Apttus__APTS_Agreement__c> mapOldAgreements) {
        //iterating through all updated agreements 
        for (Apttus__APTS_Agreement__c agreement : lstAgreements) {
            //if the actual status was changed to "other party signatures" and "fully signed" checkbox is not checked
            if (mapOldAgreements.get(agreement.Id).Apttus__Status__c != CPQ_Constants.AGREEMENT_STATUS_OTHER_PARTY_SIGNATURES 
                && agreement.Apttus__Status__c == CPQ_Constants.AGREEMENT_STATUS_OTHER_PARTY_SIGNATURES && !agreement.Fully_Signed__c) {
                agreement.Apttus__Status__c = CPQ_Constants.AGREEMENT_STATUS_CUSTOMER_SIGNATURES;
                agreement.Apttus__Status_Category__c = CPQ_Constants.AGREEMENT_STATUS_IN_SIGNATURES;
            }
            //if the actual status was changed to "fully signed" and "fully signed" checkbox is not checked
            if (mapOldAgreements.get(agreement.Id).Apttus__Status__c != CPQ_Constants.AGREEMENT_STATUS_FULLY_SIGNED && agreement.Apttus__Status__c == CPQ_Constants.AGREEMENT_STATUS_FULLY_SIGNED 
                && !agreement.Fully_Signed__c) {
                agreement.Fully_Signed__c = true;
                agreement.Apttus__Status__c = CPQ_Constants.AGREEMENT_STATUS_CUSTOMER_SIGNATURES_RECEIVED;
                agreement.Apttus__Status_Category__c = CPQ_Constants.AGREEMENT_STATUS_IN_SIGNATURES;
                agreement.Customer_Signed_Date__c = system.today();
            }
            //if the actual status was changed to "other party signatures" and "fully signed" checkbox is checked
            if (mapOldAgreements.get(agreement.Id).Apttus__Status__c != CPQ_Constants.AGREEMENT_STATUS_OTHER_PARTY_SIGNATURES && agreement.Apttus__Status__c == CPQ_Constants.AGREEMENT_STATUS_OTHER_PARTY_SIGNATURES 
                && agreement.Fully_Signed__c) {
                agreement.Apttus__Status__c = CPQ_Constants.AGREEMENT_STATUS_INTERNAL_SIGNATURES;
            }
            //if the status changes from "Internal Signatures" to "Fully Signed"
            if (mapOldAgreements.get(agreement.Id).Apttus__Status__c == CPQ_Constants.AGREEMENT_STATUS_INTERNAL_SIGNATURES && agreement.Apttus__Status__c == CPQ_Constants.AGREEMENT_STATUS_FULLY_SIGNED
                && agreement.Fully_Signed__c) {
                agreement.Rogers_Signed_Date__c = system.today();
            }
            //When I change the status from Ready for Signatures to Customer Signatures Received
            if (mapOldAgreements.get(agreement.Id).Apttus__Status__c == CPQ_Constants.AGREEMENT_STATUS_READY_FOR_SIGNATURES && agreement.Apttus__Status__c == CPQ_Constants.AGREEMENT_STATUS_CUSTOMER_SIGNATURES_RECEIVED) {
                agreement.Fully_Signed__c = true;
                agreement.Customer_Signed_Date__c = system.today();
            }
         }
    }
    
}