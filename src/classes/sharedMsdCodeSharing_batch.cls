/*
Class: sharedMsdCodeSharing_batch
For each Shared MSD record; read the related Shared MSD Accounts to get a list of Accounts that
are associated with this Shared MSD code and grant all Account Team members with Read Only
access to the record.

Written by: Jacob Klay, Rogers Communications
Date: 26-Nov-2014 09:00 EST

***Revisions***
===============================================================================
DATE                 NAME                        DESC
MM/DD/YYYY
11/26/2014           Jacob Klay                  Created
02/06/2015           Michael Appleton            Fix for multiple OrgWideEmail
02/25/2015           Michael Appleton            Fix ErrorRecords query error
===============================================================================

*/

global class sharedMsdCodeSharing_batch implements Database.Batchable<SObject>{
    
    global String query = 'SELECT Id, Shared_MSD_Code__c, Account__c, Account__r.OwnerId, Shared_MSD_Code__r.OwnerId FROM Shared_MSD_Accounts__c';
    global Database.SaveResult[] listSharedMsdCodeSaveResult;
    global Database.SaveResult[] listSharedMsdAccountsSaveResult;

    static BatchProcess_Admin__c admin = [Select email__c from BatchProcess_Admin__c limit 1];

    global Database.QueryLocator start(Database.BatchableContext BC){
        if(!Test.isRunningTest()){
        return Database.getQueryLocator(query);
        }
        else{
           return Database.getQueryLocator('SELECT Id, Shared_MSD_Code__c, Account__c, Account__r.OwnerId, Shared_MSD_Code__r.OwnerId FROM Shared_MSD_Accounts__c LIMIT 10');
        }     
    }
    
    global void execute(Database.BatchableContext BC, List<Shared_MSD_Accounts__c> scope) {
        List<ErrorRecords__c> errorList = new List<ErrorRecords__c>();
        Map<Id, Set<id>> sharedMsdCodesToUsers = new Map<Id, Set<Id>>();
        Map<Id, Set<id>> sharedMsdAccountsToUsers = new Map<Id, Set<Id>>();
        //Enble sharedMSDCodeOwners everywhere if you would like to include Owner as a Team Memeber for Shared MSD Codes
        //Map<Id, Id> sharedMSDCodeOwners = new Map<Id, Id>();
        //System.debug('Shared MSD Accounts: ' + scope);
        Set<Id> accountIds = new Set<Id>();
        for(Shared_MSD_Accounts__c element:scope) {
            accountIds.add((Id)(element.Account__c));
            //sharedMSDCodeOwners.put((Id)element.Shared_MSD_Code__c, (Id)element.Shared_MSD_Code__r.OwnerId);
        }
        //System.debug('Account IDs: ' + accountIds);
        List<AccountTeamMember> accountTeamMembers = [SELECT AccountId, UserId FROM AccountTeamMember WHERE AccountId in: accountIds];
        //System.debug('Account Team Members: ' + accountTeamMembers);
        for(Integer i = 0; i < scope.size(); i++) {
            Id acctId = scope[i].Account__c;
            Id sharedMsdAccountId = scope[i].Id;
            Id sharedMsdCodeId = scope[i].Shared_MSD_Code__c;
            Id sharedMsdAccountOwner = scope[i].Account__r.OwnerId; 
            Set<id> sharedMsdCodeUsersId = new Set<id>();
            Set<id> sharedMsdAccountsUsersId = new Set<id>();
            //sharedMsdCodeUsersId.add(sharedMSDCodeOwners.get(scope[i].Shared_MSD_Code__c));
            for(Integer j = 0; j < accountTeamMembers.size(); j++) {
                if(accountTeamMembers[j].AccountId == acctId) {
                    sharedMsdCodeUsersId.add(accountTeamMembers[j].UserId);
                    //accountTeamMembers.remove(j);
                }
            }
            sharedMsdAccountsUsersId.addAll(sharedMsdCodeUsersId);
            sharedMsdAccountsUsersId.add(sharedMsdAccountOwner);
            //Shared MSD Account to users
            sharedMsdAccountsToUsers.put(sharedMsdAccountId,sharedMsdAccountsUsersId);
            //Shared MSD Code to users
            if(!sharedMsdCodesToUsers.containsKey(sharedMsdCodeId)) {
                sharedMsdCodesToUsers.put(sharedMsdCodeId,sharedMsdCodeUsersId);
            } else {
                Set<Id> IdSet = sharedMsdCodesToUsers.get(sharedMsdCodeId);
                IdSet.addAll(sharedMsdCodeUsersId);
                sharedMsdCodesToUsers.put(sharedMsdCodeId,IdSet);
            }
        }
        //System.debug('Shared MSD Codes to Users Map: ' + sharedMsdCodesToUsers);
        //System.debug('Shared MSD Accounts to Users Map: ' + sharedMsdAccountsToUsers);

        List<Shared_MSD_Accounts__Share> listNewMsdAccountShare = new  List<Shared_MSD_Accounts__Share>();
        for(Id SharedMsdAccountId:sharedMsdAccountsToUsers.KeySet()) {
            Set<Id> usersId = sharedMsdAccountsToUsers.get(SharedMsdAccountId);
            for(Id userId:UsersId) {
                Shared_MSD_Accounts__Share msdAccountShare = new Shared_MSD_Accounts__Share();
                msdAccountShare.ParentID = SharedMsdAccountId;
                msdAccountShare.UserOrGroupId = userId;
                msdAccountShare.AccessLevel ='Read';  
                msdAccountShare.RowCause = Schema.MSD_Code__share.RowCause.AccountTeamMember__c;
                listNewMsdAccountShare.add(msdAccountShare);
            }
        }

        List<Shared_MSD_Code__Share> listNewMsdCodeShare = new  List<Shared_MSD_Code__Share>();
        for(Id sharedMsdCodeId:sharedMsdCodesToUsers.KeySet()) {
            Set<Id> usersId = sharedMsdCodesToUsers.get(sharedMsdCodeId);
            for(Id userId:UsersId) {
                Shared_MSD_Code__Share msdShare = new Shared_MSD_Code__Share();
                msdShare.ParentID = sharedMsdCodeId;
                msdShare.UserOrGroupId = userId;
                msdShare.AccessLevel ='Read';  
                msdShare.RowCause = Schema.MSD_Code__share.RowCause.AccountTeamMember__c;
                listNewMsdCodeShare.add(msdShare);
            }
        }
        //System.debug('Shared MSD Codes Sharing Records:' + listNewMsdCodeShare);
        //System.debug('Shared MSD Accounts Sharing Records:' + listNewMsdAccountShare);
        //Database.insert(listNewMsdCodeShare, false);
        //Database.insert(listNewMsdAccountShare, false);       
        try {
           //Delete old share records from the system
           /*
           Set<Id> sharedMsdCodeIds = sharedMsdCodesToUsers.KeySet();
           Set<Id> sharedMsdAccountIds = sharedMsdAccountsToUsers.KeySet();
            if(sharedMsdCodeIds.size()>0){
                List<Shared_MSD_Code__Share> listOldSharedMsdCodeShare = [select UserOrGroupId, ParentID, RowCause from Shared_MSD_Code__Share where RowCause= 'AccountTeamMember__c' and ParentID in:sharedMsdCodeIds ];
                Database.delete(listOldSharedMsdCodeShare, false); 
            }
            if(sharedMsdAccountIds.size()>0){
                List<Shared_MSD_Accounts__Share> listOldSharedMsdAccountsShare = [select UserOrGroupId, ParentID, RowCause from Shared_MSD_Accounts__Share where RowCause= 'AccountTeamMember__c' and ParentID in:sharedMsdAccountIds ];
                Database.delete(listOldSharedMsdAccountsShare, false); 
            }
      */
            //insert share records
            if(listNewMsdCodeShare.size()>0){
                listSharedMsdCodeSaveResult = Database.insert(listNewMsdCodeShare, false);
            //This is to store the list of Shared_MSD_Code_share reocrds which failed to insert
                for(integer i=0; i<listNewMsdCodeShare.size(); i++){
                    if(!(listSharedMsdCodeSaveResult[i].isSuccess())){
                        List<Database.Error> err = listSharedMsdCodeSaveResult[i].getErrors();
                        // Check if the error is related to trivial access level. Access levels equal or more permissive than the object's default access level are not allowed. 
                        //These sharing records are not required and thus an insert exception is acceptable. 
                        if(!(err[0].getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION &&  err[0].getMessage().contains('AccessLevel'))){
                            ErrorRecords__c rec = new ErrorRecords__c();
                            rec.UserId__c = listNewMsdCodeShare[i].UserOrGroupId;
                            rec.Shared_MSD_Code_Id__c = listNewMsdCodeShare[i].ParentID ;
                            rec.Error_Message__c = err[0].getMessage();
                            rec.statusCode__c = String.Valueof(err[0].getStatusCode());
                            rec.Object_Name__c = 'SharedMSDCode';
                            errorList.add(rec);                
                        }
                    }
                }
                System.debug('#####Total Error Records: ' + errorList.size());
            }
            if(listNewMsdAccountShare.size()>0){
                listSharedMsdAccountsSaveResult = Database.insert(listNewMsdAccountShare, false);
            //This is to store the list of Shared_MSD_Code_share reocrds which failed to insert
                for(integer i=0; i<listNewMsdAccountShare.size(); i++){
                    if(!(listSharedMsdAccountsSaveResult[i].isSuccess())){
                        List<Database.Error> err = listSharedMsdAccountsSaveResult[i].getErrors();
                        // Check if the error is related to trivial access level. Access levels equal or more permissive than the object's default access level are not allowed. 
                        //These sharing records are not required and thus an insert exception is acceptable. 
                        if(!(err[0].getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION &&  err[0].getMessage().contains('AccessLevel'))){
                            ErrorRecords__c rec = new ErrorRecords__c();
                            rec.UserId__c = listNewMsdAccountShare[i].UserOrGroupId;
                            rec.Shared_MSD_Account_Id__c = listNewMsdAccountShare[i].ParentID ;
                            rec.Error_Message__c = err[0].getMessage();
                            rec.statusCode__c = String.Valueof(err[0].getStatusCode());
                            rec.Object_Name__c = 'SharedMSDAccount';
                            errorList.add(rec);                
                        }
                    }
                }
                //System.debug('#####Total Error Records: ' + errorList.size());
            }

            if(errorList.size()>0){
                insert errorList;
            }
        }catch(DmlException e) {
            //Send an email to the Apex job's submitter on failure.
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {admin.email__c}; 
            mail.setToAddresses(toAddresses); 
            mail.setSubject('Batch process to share Shared MSD Code records Exception');
            mail.setPlainTextBody('Batch process to share Shared MSD Code records threw the following exception: ' + e.getMessage());
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
   }
   
    global void finish(Database.BatchableContext BC){

        List<ErrorRecords__c> errorRecordlist = [Select id, Object_Name__c, account_name__c , Shared_MSD_Code_Name__c, Shared_MSD_Code_Id__c, userId__c, user_name__c, Error_Message__c, statusCode__c from ErrorRecords__c where CreatedDate = TODAY and  Object_Name__c in ('SharedMSDAccount','SharedMSDCode') and isDeleted = false];
        string header = 'Object Name, Account Name, Shared MSD Code Name , Shared MSD Code Id, User Name, User Id, Error Message, Status Code \n';
        string finalstr = header;
        for(ErrorRecords__c erObj: errorRecordlist )
        {
               string recordString = '"'+erObj.Object_Name__c+'","'+erObj.account_name__c+'","'+erObj.Shared_MSD_Code_Name__c+'","'+erObj.Shared_MSD_Code_Id__c+'","'+erObj.user_name__c+'","'+erObj.UserId__c+'","'+erObj.Error_Message__c+'","'+erObj.StatusCode__c+'"\n';
               finalstr = finalstr+recordString;
        }
        //Send Notification Email to Admin with Errors if any
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        string csvname= 'Shared MSD Code Error Records.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {admin.email__c};
        String body = 'Hi,<br/><br/>Batch process to share Shared MSD Code records with AccountTeamMembers has been completed. <br/><br/>'; 
        body += 'Please find the attached csv for error records that failed to get inserted in the database.';
        body += '<br/> <br/> Regards, <br/> Rogers Salesforce IT Team'; 
        email.setToAddresses(toAddresses); 
        email.setSubject('Batch Completion Report: Shared MSD Code');        
        email.setSaveAsActivity(false);  
        email.setHtmlBody(body); 

        email.setOrgWideEmailAddressId(BatchUtils.getDefaultOrgWideEmailAddr(Label.Default_OrgWideEmailAddress)); 
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }

}