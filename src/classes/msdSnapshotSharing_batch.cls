/*
===============================================================================
 Class Name   : msdSnapshotSharing_batch
===============================================================================
PURPOSE:    This batch class deltets all the MSD__share records with 
            rowcause = AccountTeamMember__c and inserts new MSD_Code share records
            for related Account's Owner and AccountTeamMembers
  

Developer: Deepika Rawat
Date: 09/03/2013

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
09/03/2013          Deepika                     Created
===============================================================================
*/
global class msdSnapshotSharing_batch implements Database.Batchable<SObject>{
    global String query = 'select id, AccountId__c FROM MSD__c where AccountId__c!=null Order By AccountId__c';
    global Database.SaveResult[] saveResultList; 
    static BatchProcess_Admin__c admin = [Select email__c from BatchProcess_Admin__c limit 1];
    static OrgWideEmailAddress orgEmailId=[select id, Address, DisplayName from OrgWideEmailAddress limit 1] ;

    global Database.QueryLocator start(Database.BatchableContext BC){
        //List<ErrorRecords__c> errorRecordlist = [Select id,account_name__c , MSD_Snapshot_Name__c, MSD_Snapshot_Id__c, userId__c, user_name__c, Error_Message__c, statusCode__c from ErrorRecords__c where MSD_Code_Id__c=''];
        //delete errorRecordlist;
        if(!Test.isRunningTest()){
        return Database.getQueryLocator(query);
        }
        else{
           return Database.getQueryLocator('select id, AccountId__c FROM MSD__c where AccountId__c!=null Order By AccountId__c limit 10');
        }     
        
    }

    global void execute(Database.BatchableContext BC, List<MSD__c > scope) {
        List<ErrorRecords__c> errorList = new List<ErrorRecords__c>();
        List<MSD__share> listNewMsdShare = new  List<MSD__share>();
        Database.DeleteResult[] listDeleteShareRec;
        Map<Id, List<id>> mapAccountUser = new Map<Id, List<id>>();
        List<Id> listMsdIds = new List<Id>(); 
        List<Id> listAccIds = new List<Id>();      
        
        for(MSD__c msd:scope){
            listAccIds.add(msd.AccountId__c);
        }
        //Store all MSD Snapshot Ids in listMsdIds list
        for(MSD__c msdRec : scope) {
            listMsdIds.add(msdRec.Id);
        }
        Map<id, AccountTeamMember> mapAccountTeamMember = new Map<id, AccountTeamMember>([select id, accountId, account.ownerId, userId from AccountTeamMember where accountId in:listAccIds ]);
        List<MSD__share> listOldMsdShare = [select UserOrGroupId, ParentID, RowCause from MSD__share where RowCause= 'AccountTeamMember__c' and ParentID in:listMsdIds];
        
        //Create a Map of Account id with list of User ids(Account Owner and AccountTeamMembers)
        for(Id atmId : mapAccountTeamMember.keySet()){
            AccountTeamMember atmRec = mapAccountTeamMember.get(atmId);
            //This is to map AccountTeamMembers
            if(mapAccountUser.containsKey(atmRec.accountId)) {
                List<Id> userIds = new List<Id>();
                userIds = mapAccountUser.get(atmRec.accountId);
                userIds.add((Id)atmRec.userId);
                mapAccountUser.put(atmRec.accountId, userIds);
            } 
            //This is to map Account Owner
            else {
                List<Id> userIds = new List<Id>();
                userIds.add((Id)atmRec.userId);
                userIds.add(atmRec.account.ownerId);
                mapAccountUser.put(atmRec.accountId, userIds);
            }
        }

        // Create list of new MSD__share records
        for(MSD__c msdRec : scope) {
            if(mapAccountUser.containsKey(msdRec.AccountId__c)){
                List<Id> userIds = mapAccountUser.get(msdRec.AccountId__c);
                for(Id userId : userIds) {
                    MSD__share msdShare = new MSD__share();
                    msdShare.ParentID = msdRec.id;
                    msdShare.UserOrGroupId = userId;
                    msdShare.AccessLevel ='Read';  
                    msdShare.RowCause = Schema.MSD__share.RowCause.AccountTeamMember__c;
                    listNewMsdShare.add(msdShare);
                }
            }        
        }
         try {
          //Delete old MSD__share records from the system
            if(listOldMsdShare.size()>0){
                listDeleteShareRec = Database.delete(listOldMsdShare, false); 
            }

             //Insert share records for newly added AccountTeamMembers
            if(listNewMsdShare.size()>0){
                saveResultList = Database.insert(listNewMsdShare, false);
                //This is to store the list of MSD_share reocrds which failed to insert
                for(integer i=0; i<listNewMsdShare.size(); i++){
                    if(!(saveResultList[i].isSuccess())){
                        List<Database.Error> err = saveResultList[i].getErrors();
                        // Check if the error is related to trivial access level. Access levels equal or more permissive than the object's default access level are not allowed. 
                        //These sharing records are not required and thus an insert exception is acceptable. 
                        if(!(err[0].getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION &&  err[0].getMessage().contains('AccessLevel'))){
                            ErrorRecords__c rec = new ErrorRecords__c();
                            rec.UserId__c= listNewMsdShare[i].UserOrGroupId;
                            rec.MSD_Snapshot_Id__c = listNewMsdShare[i].ParentID ;
                            rec.Error_Message__c= err[0].getMessage();
                            rec.statusCode__c= String.Valueof(err[0].getStatusCode() );
                            rec.Object_Name__c='MSDSnapshot';
                            errorList.add(rec);                
                        }
                    }
                }
            }
            if(errorList.size()>0){
                insert errorList;
            }
        }catch(DmlException e) {
           // Send an email to the Apex job's submitter on failure.
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
             String[] toAddresses = new String[] {admin.email__c}; 
            mail.setToAddresses(toAddresses); 
            mail.setSubject('Batch process to share MSD Snapshot records Exception');
            mail.setPlainTextBody('Batch process to share MSD Snapshot records threw the following exception: ' + e.getMessage());
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }    
    }

    global void finish(Database.BatchableContext BC){
        //Send email to systemAdmin on process completion with list of records which
        //failed to insert, if any.

        List<ErrorRecords__c> errorRecordlist = [Select id,account_name__c , MSD_Snapshot_Name__c, MSD_Snapshot_Id__c, userId__c, user_name__c, Error_Message__c, statusCode__c from ErrorRecords__c where MSD_Code_Id__c='' and CreatedDate = TODAY];
        string header = 'Account Name, MSD Snapshot Name , MSD Snapshot Id, User Name, User Id, Error Message, Status Code \n';
        string finalstr = header ;
        for(ErrorRecords__c erObj: errorRecordlist )
        {
               string recordString = '"'+erObj.account_name__c+'","'+erObj.MSD_Snapshot_Name__c+'","'+erObj.MSD_Snapshot_Id__c+'","'+erObj.User_Name__c+'","'+erObj.userId__c+'","'+erObj.Error_Message__c+'","'+erObj.StatusCode__c+'"\n';
               finalstr = finalstr +recordString;
        }
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        string csvname= 'MSD Snapshot Error Records.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {admin.email__c};
        String body = 'Hi,<br/><br/>Batch process to share MSD Snapshot records with AccountTeamMembers has been completed. <br/><br/>'; 
        body += 'Please find the attached csv for error records that failed to get inserted in the database.';
        body += '<br/> <br/> Regards, <br/> Salesforce Team'; 
        email.setToAddresses(toAddresses); 
        email.setSubject('Batch Completion Report: MSD Snapshot');        
        email.setSaveAsActivity(false);  
        email.setHtmlBody(body);  
        email.setOrgWideEmailAddressId(orgEmailId.id); 
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }
}