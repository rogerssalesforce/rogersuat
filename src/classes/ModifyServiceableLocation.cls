public with sharing class ModifyServiceableLocation{

   public ModifyServiceableLocation(ApexPages.StandardController controller) {
     
   }
   
   public ModifyServiceableLocation() {
     
   }

  public PageReference addSite(){ 
          String hostname = ApexPages.currentPage().getHeaders().get('Host');
          String idLink = '?edit=1';
          String absolutePath = 'https://' + hostname + '/apex/addSite'  + idLink;
          PageReference nextPage = new PageReference(absolutePath);
  
          return nextPage;
    }
}