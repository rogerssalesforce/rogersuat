/* ********************************************************************
 * Name : MassApplyZSitesController
 * Description : Controller. Allows user to link Z Sites to Site A
 * Modification Log
  =====================================================================
 * Ver    Date          Author               Modification
 ----------------------------------------------------------------------
 * 1.0    -/--/2011     Kevin DesLauriers    Initial Version  
 * 1.1    5/29/2012     Kevin DesLauriers    Allow Site A to have multiple Z Sites
 * 1.2    6/25/2012		Kevin DesLauriers	 Reuse for Quote Request Process
  25-November-2015      Alina Balan          Refactoring
 **********************************************************************/

public with sharing class MassApplyZSitesController {
    public Boolean bRenderSiteTable {get; set;}
    public Boolean bRenderSiteMessage {get; set;}
    public List<SiteWrapper> lstZSites {get; set;}
    public Boolean bRenderZSiteMessage {get; set;}
    public Boolean bRenderZSiteTable {get; set;}
    //public Map<String, Site__c> mapProspectSites {get; set;}
    public Boolean bFromQuoteRequest {get; set;}
    // the collection of serviceablity to display
    public List<SiteWrapper> lstProspectSites = new List<SiteWrapper>();
    private Quote_Request__c oQuoteRequest {get; set;}
    private Opportunity oOpportunity {get; set;}
    private Id oppId;
    private Id returnURL;
    private Id qRequestId;
    // These maps have the site id as the key and the QRLocation as the value.
    private Map<Id, Location_A__c> mapLocationA = new Map<Id, Location_A__c>();
    private Map<Id, Location_Z__c> mapLocationZ = new Map<Id, Location_Z__c>();
    private Set<Id> setQRLocation = new Set<Id>();
    // the soql without the order and limit
    private String soql {get;set;}
    
    public List<SiteWrapper> getProspectSites(){
        return this.lstProspectSites;
    }
    
    /*********************************************************************************
    Method Name    : MassApplyZSitesController
    Description    : Constructor method
    Return Type    : 
    Parameter      : 
    *********************************************************************************/
    public MassApplyZSitesController() {
        try {
            returnURL = ApexPages.currentPage().getParameters().get('retUrl');
        } catch (Exception ex) {  
            // In the case that the user uses the application incorrectly and selects a site before hitting process Sites
            returnURL = ApexPages.currentPage().getParameters().get('retUrl').substring(1);
        }
        try {
            qRequestId = ApexPages.currentPage().getParameters().get('qRequestId');
        } catch(Exception ex) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
            ApexPages.addMessage(myMsg);
        }
        bFromQuoteRequest = !Utils.isEmpty(qRequestId);
        //check if the request comes from quote 
        if (bFromQuoteRequest) {
            oQuoteRequest = [SELECT Id From Quote_Request__c WHERE Id = :qRequestId];
            loadQRequestsLocations();
        }
        try {
            oppId = ApexPages.currentPage().getParameters().get('oppId');
        } catch (Exception ex){  // In the case that the user uses the application incorrectly and selects a site before hitting process Sites
            oppId = ApexPages.currentPage().getParameters().get('oppId').substring(1);
        }
        try  {        
            oOpportunity = [SELECT Id From Opportunity WHERE Id = :oppId];
        } catch(Exception ex) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
            ApexPages.addMessage(myMsg);
        }
        soql = 'SELECT s.Id , s.Name, s.Suite_Floor__C, s.Street_Number__c, s.Street_Name__c, s.Street_Type__c, s.Street_Direction__c, s.City__c, s.Postal_Code__c, s.Display_Name__c, s.Province_Code__c, ' + 
               's.ServiceableLocation__r.Id, s.ServiceableLocation__r.Name, s.Type__c, s.Z_Sites__c FROM Site__c s WHERE Opportunity__c = : oppId AND Is_a_Z_Site__c = false ORDER BY CreatedDate';
        // Obtain any prospective sites
        loadProspectSites();  
         // Obtain any prospective sites
        loadZSites(oppId);
        //check if prospect sites list has elements
        if (lstProspectSites.size() == 0) {
            bRenderSiteTable = false;
        } else {
            bRenderSiteTable = true;
        }
        bRenderSiteMessage = !bRenderSiteTable;
        //check if z-sites list has elements
        if (lstZSites.size() == 0) {
            bRenderZSiteTable = false;
        } else {
            bRenderZSiteTable = true;
        }
        bRenderZSiteMessage = !bRenderZSiteTable;
    }
    
    /*********************************************************************************
    Method Name    : loadZSites
    Description    : method used to load z-sites
    Return Type    : 
    Parameter      : Id opportunityId
    *********************************************************************************/
    private void loadZSites(Id opportunityId) {
        try {
            //check if z-sites list has elements
            if (lstZSites == null) {
                lstZSites= new List<SiteWrapper>();
                //iterating through a-z sites
                for(Site__c s : [SELECT s.Id , s.Name, s.Suite_Floor__C, s.Street_Number__c
                                        , s.Street_Name__c
                                        , s.Street_Type__c
                                        , s.Street_Direction__c
                                        , s.City__c
                                        , s.Postal_Code__c
                                        , s.Province_Code__c
                                        , s.ServiceableLocation__r.Id
                                        , s.ServiceableLocation__r.Name, s.Type__c
                                        , s.Display_Name__c,  s.Z_Sites__c
                                         FROM Site__c s
                                         WHERE Opportunity__c = : opportunityId
                                         AND s.Is_a_Z_Site__c = true 
                                         order BY CreatedDate]) {
                    lstZSites.add(new SiteWrapper(s, false, setQRLocation.contains(s.Id)));
                }
            }
        }
        catch(DMLException e) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
        }
    }
    
    /*********************************************************************************
    Method Name    : loadProspectSites
    Description    : method used to load all prospect sites
    Return Type    : 
    Parameter      : 
    *********************************************************************************/
    public void loadProspectSites() {
        Set<Id> setCurrentlySelected = new Set<Id>();
        //iterating through all prospect sites
        for (SiteWrapper site : lstProspectSites) {
            //check if the current site is selected
            if (site.getIsSelected()) {
                setCurrentlySelected.add(site.Site.Id);
            }
         }
         lstProspectSites.clear();
         try {
            //iterating through all sites
            for(Site__c s : Database.query(soql)) {
                SiteWrapper temp = new SiteWrapper(s, false, setQRLocation.contains(s.Id));
                //if the selected sites set contains the current site id
                if (setCurrentlySelected.contains(temp.Site.Id)) {
                    temp.setIsSelected(true);
                }
                lstProspectSites.add(temp);
            }
        }
        catch(DMLException e) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
        }
    }
    
    /*********************************************************************************
    Method Name    : loadQRequestsLocations
    Description    : method used to load quote request locations
    Return Type    : 
    Parameter      : 
    *********************************************************************************/
     public void loadQRequestsLocations() {
        List<Location_Z__c> lstZQRLocations = new List<Location_Z__c>();
        List<Location_A__c> lstAQRLocations = new List<Location_A__c>();
        try {
            lstZQRLocations = [SELECT Id, Prospect_Site__c FROM Location_Z__c WHERE Quote_Request__c = :qRequestId];                  
            lstAQRLocations = [SELECT Id, Prospect_Site__c FROM Location_A__c WHERE Quote_Request__c = :qRequestId];
        } catch(DMLException e) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
        }
        //iterating through all z locations
        for (Location_Z__c z : lstZQRLocations){
            setQRLocation.add(z.Prospect_Site__c);
            mapLocationZ.put(z.Prospect_Site__c, z);
        }
        //iterating through all a locations
        for (Location_A__c a : lstAQRLocations){
            setQRLocation.add(a.Prospect_Site__c);
            mapLocationA.put(a.Prospect_Site__c, a);
        }
    }
    
    /*********************************************************************************
    Method Name    : runSearch
    Description    : method used to search sites
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference runSearch() {
        String sStreetNumber = Apexpages.currentPage().getParameters().get('streetNumber');
        String sStreetName = Apexpages.currentPage().getParameters().get('streetName');
        String sCity = Apexpages.currentPage().getParameters().get('city');
        String sProvinceCode = Apexpages.currentPage().getParameters().get('provinceCode');
        soql = 'SELECT s.Id , s.Name, s.Suite_Floor__C, s.Street_Number__c, s.Street_Name__c, s.Street_Type__c, s.Street_Direction__c, s.City__c, s.Postal_Code__c, s.Display_Name__c, s.Province_Code__c, ' + 
               's.ServiceableLocation__r.Id, s.ServiceableLocation__r.Name, s.Type__c, s.Z_Site__r.Display_Name__c, s.Z_Sites__c FROM Site__c s WHERE Opportunity__c = : oppId AND Is_a_Z_site__c = false';
        //check if street number is empty
        if (!sStreetNumber.equals('')) {
            soql += ' and s.Street_Number__c LIKE \''+String.escapeSingleQuotes(sStreetNumber)+'%\'';
        }
        //check if street name is empty
        if (!sStreetName.equals('')) {
        soql += ' and s.Street_Name__c LIKE \''+String.escapeSingleQuotes(sStreetName)+'%\'';    
        }
        //check if province code is empty
        if (!sProvinceCode.equals('')) {
            soql += ' and s.Province_Code__c LIKE \''+String.escapeSingleQuotes(sProvinceCode)+'%\'';
        }
        //check if city is empty
        if (!sCity.equals('')) {
            soql += ' and s.City__c LIKE \''+String.escapeSingleQuotes(sCity)+'%\'';  
        }
        soql += ' order by CreatedDate';
         // run the query again
        loadProspectSites();
        return null;
    }
      
    /*********************************************************************************
    Method Name    : updateZSites
    Description    : method used to update z-sites
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    /*
         Refactored: May 30/2012 - KD - Allow for selection of many ZSites 
         This method now creates AZ SIte Junction Objects that did not previously 
         exist.
    */   
    public PageReference updateZSites() {
        List<Id> lstZSiteIds = new List<Id>();
        List<Id> lstASiteIds = new List<Id>();
        List<A_Z_Site__c> lstAzSites = new List<A_Z_Site__c>();
        Map<Id, Set<Id>> mapAzSites = new Map<Id, Set<Id>>();
        List<A_Z_Site__c> lstNewAZConnections = new List<A_Z_Site__c>();
        //iterating through all z-sites
        for (SiteWrapper s : lstZSites) {
            //if the current site is selected
            if (s.getIsSelected()) {
                lstZSiteIds.add(s.Site.Id);            
                s.setIsSelected(false);
            }
        }
        //iterating through all prospect sites
        for (SiteWrapper s : lstProspectSites){
            //if the current site is selected
            if (s.getIsSelected()) {
                lstASiteIds.add(s.Site.Id);
                s.setIsSelected(false);
            }
        }
        try {
            lstAzSites = [SELECT Site_A__c, Site_Z__c FROM A_Z_Site__c WHERE Site_A__c IN :lstASiteIds];
            //iterating through all a-z-sites
            for (A_Z_Site__c az : lstAzSites) {
                Set<Id> setZSite = mapAzSites.get(az.Site_A__c);
                //check if the set obtained from a-z sites map is not null
                if (setZSite == null) {
                    setZSite = new Set<Id>();
                }
                setZSite.add(az.Site_Z__c);
                mapAzSites.put(az.Site_A__c, setZSite);
            }
        } catch(Exception ex) {
            // There are no site AZ junction objects for this site A
        }
        //iterating through all a-sites ids
        for (Id a : lstASiteIds) {
            Set<Id> setZSite = mapAzSites.get(a);
            //iterating through all z-sites ids
            for (Id z : lstZSiteIds) {
                //check if z-sites set is null or z-sites set does not contain the current z-site id
                if (setZSite == null || !setZSite.contains(z))
                lstNewAZConnections.add(new A_Z_Site__c(Site_A__c = a, Site_Z__C = z));
            }
        }
        insert lstNewAZConnections;
        loadProspectSites();        
        return null;
    }
    
    /*********************************************************************************
    Method Name    : removeZSites
    Description    : method used to remove z-sites
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    /*
         Refactored: May 30/2012 - KD - Allow for removal of all ZSites. 
         This method now removes the AZ SIte Junction Objects that exist for the A Site.
    */ 
    public PageReference removeZSites() {
        String sRemoveAll = Apexpages.currentPage().getParameters().get('allSites');
        Boolean bClearAll = !Utils.isEmpty(sRemoveAll) && sRemoveAll == 'true';
        List<Site__c> lstUpdateSites = new List<Site__c>();
        List<Id> lstASiteIds = new List<Id>();
        //iterating through all prospect sites
        for (SiteWrapper s : lstProspectSites) {
            //check if the current site is selected
            if (s.getIsSelected()){
                s.Site.Z_Site__c = null;
                lstUpdateSites.add(s.Site);
                s.setIsSelected(false);
                lstASiteIds.add(s.Site.Id);
            }
        }
        List<A_Z_Site__c> lstAzSites; 
        try {
            lstAzSites = [SELECT Site_A__c, Site_Z__c FROM A_Z_Site__c WHERE Site_A__c IN :lstASiteIds];
            //check if all sites should be deleted
            if (bClearAll) {
                delete lstAzSites;
            } else {
                List<A_Z_Site__c> lstSelectedAZSites = new List<A_Z_Site__c>();
                List<Id> lstZSiteIds = new List<Id>();
                //iterating through all z-sites
                for (SiteWrapper s : lstZSites){
                    //check if the current site is selected
                    if (s.getIsSelected()){
                        lstZSiteIds.add(s.Site.Id);
                        s.setIsSelected(false);
                    }
                }
                // n^3 but we should not have a long list of these so it should be relatively quick 
                //iterating through all a-sites ids
                for (Id a : lstASiteIds) {
                    //iterating through z-sites ids
                    for (Id z : lstZSiteIds) {
                        //iterating through a-z-sites
                        for (A_Z_Site__c az : lstAzSites) {
                            //check if the current a-z-site has the current a-site id and z-site id
                            if ((az.Site_A__c == a) && (az.Site_Z__c == z)) {
                                lstSelectedAZSites.add(az);
                            }
                        }
                    }
                }
                delete lstSelectedAZSites;
            }
        }catch(Exception ex){
            // There are no site AZ junction objects for this site A
        }
        loadProspectSites();
        clearZSites();
        return null;
    }
    
    /*********************************************************************************
    Method Name    : returnToOpportunity
    Description    : method used to go back to opportunity
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference returnToOpportunity() {
        PageReference oppPage = new ApexPages.StandardController(oOpportunity).view();
        oppPage.setRedirect(true);
        return oppPage;
    }
    
    /*********************************************************************************
    Method Name    : returnToQuoteRequest
    Description    : method used to go back to quote request
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference returnToQuoteRequest() {
        PageReference qRequestPage = new ApexPages.StandardController(oQuoteRequest).view();
        qRequestPage.setRedirect(true);
        return qRequestPage;
    }
    
    /*********************************************************************************
    Method Name    : addSites
    Description    : method used to add sites
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference addSites() {
        List<Location_A__c> lstAQRSites = new List<Location_A__c>();
        List<Location_Z__c> lstZQRSites = new List<Location_Z__c>();
        //iterating through al z-sites 
        for (SiteWrapper s : lstZSites) {
            //check if the current site is selected
            if (s.getIsSelected()) {
                //check if the qr location set already contains current site id
                if (!setQRLocation.contains(s.Site.Id)) {
                    Location_Z__c oLocationZ = new Location_Z__c(Name = s.Site.Display_Name__c, Prospect_Site__c = s.Site.Id, Quote_Request__c = qRequestId);
                    lstZQRSites.add(oLocationZ);
                    setQRLocation.add(s.Site.Id);
                    mapLocationZ.put(s.Site.Id, oLocationZ);
                }
                s.setOnQuoteRequest(true);         
                s.setIsSelected(false);
            }
        }
        //iterating through all prospect sites
        for (SiteWrapper s : lstProspectSites) {
            //check if the current site is selected
            if (s.getIsSelected()) {
                //check if the qr location set already contains current site id
                if (!setQRLocation.contains(s.Site.Id)){
                    Location_A__c oLocationA = new Location_A__c(Name = s.Site.Display_Name__c, Prospect_Site__c = s.Site.Id, Quote_Request__c = qRequestId);
                    lstAQRSites.add(oLocationA);
                    setQRLocation.add(s.Site.Id);
                    mapLocationA.put(s.Site.Id, oLocationA);
                }
                s.setOnQuoteRequest(true);
                s.setIsSelected(false);
            }
        }
        insert lstZQRSites;
        insert lstAQRSites;
        return null;
    }
   
    /*********************************************************************************
    Method Name    : removeSites
    Description    : method used to remove sites
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference removeSites() {
        List<Location_A__c> lstAQRSites = new List<Location_A__c>();
        List<Location_Z__c> lstZQRSites = new List<Location_Z__c>();
        //iterating through all prospect sites
        for (SiteWrapper s : lstZSites) {
            //check if the current site is selected
            if (s.getIsSelected()) {
                //check if the qr location set already contains current site id
                if (setQRLocation.contains(s.Site.Id)) {
                    lstZQRSites.add(mapLocationZ.get(s.Site.Id));
                    setQRLocation.remove(s.Site.Id);
                    mapLocationZ.remove(s.Site.Id);
                }
                s.setOnQuoteRequest(false);         
                s.setIsSelected(false);
            }
        }
        //iterating through all prospect sites
        for (SiteWrapper s : lstProspectSites) {
            //check if the current site is selected
            if (s.getIsSelected()) {
                //check if the qr location set already contains current site id
                if (setQRLocation.contains(s.Site.Id)) {
                    lstAQRSites.add(mapLocationA.get(s.Site.Id));
                    setQRLocation.remove(s.Site.Id);
                    mapLocationA.remove(s.Site.Id);
                }
                s.setOnQuoteRequest(false);
                s.setIsSelected(false);
            }
        }
        //check if z qr sites is empty
        if (!lstZQRSites.isEmpty()) {
            delete lstZQRSites;
        }
        //check if a qr sites is empty
        if (!lstAQRSites.isEmpty()) {
            delete lstAQRSites;
        }
        return null;
    }
    
    /*********************************************************************************
    Method Name    : clearZSites
    Description    : method used to deselect all z-sites
    Return Type    : 
    Parameter      : 
    *********************************************************************************/
    private void clearZSites() {
        //iterating through all z-sites
        for (SiteWrapper s : lstZSites) {
            s.setIsSelected(false);
        }
    }    
}