public with sharing class DiscountApprovalManager {
	private static final Decimal MILLISEC_PER_MINUTE = 60000.00;
	
	public static void updateApprovalHistory(List<Scheduled_Quote_Approval_History__c> schedQuotes){
		
		Set<Id> quoteIds = new Set<Id>();
		for (Scheduled_Quote_Approval_History__c sq : schedQuotes){
			quoteIds.add(sq.quote__c);
		}
		
		

		List<ProcessInstance> approvals = [SELECT Id,  
									Status, 
									TargetObjectId, 
										(SELECT Id, 
											StepStatus, 
											ActorId,
											Actor.Name, 
											OriginalActor.Name, 
											CreatedDate 
											FROM StepsAndWorkitems Order By CreatedDate ASC) 
									FROM ProcessInstance 
									WHERE TargetObjectId IN :quoteIds Order By LastModifiedDate DESC];
		
		
		
		Map<Id, List<ProcessInstanceHistory>> processItemMap = new Map<Id, List<ProcessInstanceHistory>>(); 
		Map<Id, ProcessInstance> processInsanceMap = new Map<Id, ProcessInstance>();
		
		
		
		for (ProcessInstance approval : approvals){
			processItemMap.put(approval.Id, approval.StepsAndWorkitems);
			processInsanceMap.put(approval.Id, approval);
		}
		
		Map<Id, Discount_Approval_History__c> discountApprovalMap = new Map<Id, Discount_Approval_History__c>();
		
		
		List<Discount_Approval_History__c> discApprovalHistories = new List<Discount_Approval_History__c>(); 
		
		try{
			discApprovalHistories = [SELECT Current_Status__c,
									 ProcessInstanceId__c, 
									 Product_Manager__c, 
									 Quote__c, 
									 Id, 
									 Sales_Manager__c, 
									 Submitted_By__c, 
									 Time_Waiting_For_Product_Manager_Hours__c, 
									 Time_Waiting_for_Sales_Manager_Hours__c, 
									 Time_in_Approval_Process_Hours__c 
									 FROM Discount_Approval_History__c 
									 WHERE ProcessInstanceId__c IN :processItemMap.keySet()];
		}catch(Exception ex){
			
		}			
		
		for (Discount_Approval_History__c discountApproval : discApprovalHistories){
			discountApprovalMap.put(discountApproval.ProcessInstanceId__c, discountApproval);
			
		}
		
		
		// We need to go through each of these and perform the processing.
		for (Scheduled_Quote_Approval_History__c schedQuote : schedQuotes){
			// Let's find out when the Quote was submitted
			DateTime startTime;
			List<ProcessInstanceHistory> processItems = processItemMap.get(schedQuote.ProcessInstanceId__c);
			for (ProcessInstanceHistory step : processItems){
				if (step.StepStatus.equals('Started')){	
						startTime = step.CreatedDate;	
				}	
			}						
			
			
			Discount_Approval_History__c discApprovalHistory = discountApprovalMap.get(schedQuote.ProcessInstanceId__c);
			
			if (!schedQuote.isCarrier__c){
				if (discApprovalHistory == null){	// we will need to create one
					discApprovalHistory = new Discount_Approval_History__c();
					discountApprovalMap.put(schedQuote.ProcessInstanceId__c, discApprovalHistory);
					for (ProcessInstanceHistory step : processItems){
						if (step.StepStatus.equals('Started')){
							discApprovalHistory.Submitted_By__c = step.ActorId;
							discApprovalHistory.Time_Submitted__c = startTime;
							discApprovalHistory.ProcessInstanceId__c = schedQuote.processInstanceId__c;
							discApprovalHistory.Quote__c = schedQuote.quote__c;
							discApprovalHistory.Current_Status__c = 'Sales Manager - Pending';
						}
					}
				}else{
					if (schedQuote.Approval_History__c.equals('Sales Manager Approved')){
					 	for (ProcessInstanceHistory step : processItems){
					 		if (step.StepStatus.equals('Approved')){
					 			discApprovalHistory.Sales_Manager__c = step.ActorId;
					 			DateTime stepTime = step.CreatedDate;
					 			discApprovalHistory.Time_Waiting_for_Sales_Manager_Hours__c = (stepTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
					 			discApprovalHistory.Time_Approved_Rejected_By_Sales_Manager__c = stepTime;
					 			break;
					 		}
					 	}
					 	discApprovalHistory.Current_Status__c = 'Product Manager - Pending';
					 }else if (schedQuote.Approval_History__c.equals('Final Approved')){
					 	// if there are two Approved Entries then the last one is because it went through the product managers
					 	if (Utils.isEmpty(discApprovalHistory.Sales_Manager__c)){	// We have not set the Sales Manager - must have been the Sales Manager
					 		for (ProcessInstanceHistory step : processItems){
						 		if (step.StepStatus.equals('Approved')){
						 			discApprovalHistory.Sales_Manager__c = step.ActorId;
						 			DateTime stepTime = step.CreatedDate;
						 			discApprovalHistory.Time_Waiting_for_Sales_Manager_Hours__c = (stepTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
						 			discApprovalHistory.Time_in_Approval_Process_Hours__c = discApprovalHistory.Time_Waiting_for_Sales_Manager_Hours__c;
						 			discApprovalHistory.Time_Approved_Rejected_By_Sales_Manager__c = stepTime;
						 			discApprovalHistory.Time_Final_Approved_Rejected__c = stepTime;
						 			break;
						 		}
						 	}
					 	}else{		// must be two and the product Manager also approved it.
					 		Integer numApproved = 0;
					 		for (ProcessInstanceHistory step : processItems){
						 		if (step.StepStatus.equals('Approved')){
						 			numApproved++;
						 			if (numApproved==2){
							 			discApprovalHistory.Product_Manager__c = step.ActorId;
							 			DateTime stepTime = step.CreatedDate;
							 			discApprovalHistory.Time_in_Approval_Process_Hours__c = (stepTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;						 			
							 			discApprovalHistory.Time_Waiting_for_Product_Manager_Hours__c = discApprovalHistory.Time_in_Approval_Process_Hours__c 
							 																				- discApprovalHistory.Time_Waiting_for_Sales_Manager_Hours__c;
							 			discApprovalHistory.Time_Approved_Rejected_Product_Manager__c = stepTime;
							 			discApprovalHistory.Time_Final_Approved_Rejected__c = stepTime;
							 			break;
						 			}															
						 			
						 		}
						 	}
					 	}
					 	
					 	discApprovalHistory.Current_Status__c = 'Completed - Approved';
					 }else if (schedQuote.Approval_History__c.equals('Final Rejection')){
					 	if (Utils.isEmpty(discApprovalHistory.Sales_Manager__c)){	// We have not set the Sales Manager - must have been the Sales Manager
					 		for (ProcessInstanceHistory step : processItems){
						 		if (step.StepStatus.equals('Rejected')){
						 			discApprovalHistory.Sales_Manager__c = step.ActorId;
						 			DateTime stepTime = step.CreatedDate;
						 			discApprovalHistory.Time_Waiting_for_Sales_Manager_Hours__c = (stepTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
						 			discApprovalHistory.Time_in_Approval_Process_Hours__c = discApprovalHistory.Time_Waiting_for_Sales_Manager_Hours__c;
						 			discApprovalHistory.Time_Approved_Rejected_By_Sales_Manager__c = stepTime;
						 			discApprovalHistory.Time_Final_Approved_Rejected__c = stepTime;
						 			break;
						 		}
						 	}
					 	}else{		// Product Manager rejected it
					 		for (ProcessInstanceHistory step : processItems){
						 		if (step.StepStatus.equals('Rejected')){
						 			discApprovalHistory.Product_Manager__c = step.ActorId;
						 			DateTime stepTime = step.CreatedDate;
						 			discApprovalHistory.Time_in_Approval_Process_Hours__c = (stepTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;						 			
						 			discApprovalHistory.Time_Waiting_for_Product_Manager_Hours__c = discApprovalHistory.Time_in_Approval_Process_Hours__c 
						 																				- discApprovalHistory.Time_Waiting_for_Sales_Manager_Hours__c;
						 			discApprovalHistory.Time_Approved_Rejected_Product_Manager__c = stepTime;
							 		discApprovalHistory.Time_Final_Approved_Rejected__c = stepTime;
						 			break;
						 		}
						 	}
					 	}
					 	
					 	discApprovalHistory.Current_Status__c = 'Completed - Rejected';
					 }else if (schedQuote.Approval_History__c.equals('Recalled')){
					 	for (ProcessInstanceHistory step : processItems){
							if (step.StepStatus.equals('Recalled')){
							 	discApprovalHistory.Current_Status__c = 'Recalled';
							 	DateTime stepTime = step.CreatedDate;
							 	discApprovalHistory.Time_Recalled__c = stepTime;
								break;
							}
					 	}
					 }				
				}			
			}else{  // It is Carrier so they can skip the sales step
				if (discApprovalHistory == null){	// we will need to create one
					discApprovalHistory = new Discount_Approval_History__c();
					discountApprovalMap.put(schedQuote.ProcessInstanceId__c, discApprovalHistory);
					for (ProcessInstanceHistory step : processItems){
						if (step.StepStatus.equals('Started')){
							discApprovalHistory.Submitted_By__c = step.ActorId;
							discApprovalHistory.Time_Submitted__c = startTime;
							discApprovalHistory.ProcessInstanceId__c = schedQuote.processInstanceId__c;
							discApprovalHistory.Quote__c = schedQuote.quote__c;
							if (schedQuote.skipSalesManager__c)
								discApprovalHistory.Current_Status__c = 'Product Manager - Pending';
							else
								discApprovalHistory.Current_Status__c = 'Sales Manager - Pending';
						}
					}
				}else{
					
					if (schedQuote.Approval_History__c.contains('Approved') || schedQuote.Approval_History__c.contains('Rejection')){	
						for (ProcessInstanceHistory step : processItems){
					 		if (step.StepStatus.equals('Approved') || step.StepStatus.equals('Rejected')){
					 			
					 			DateTime stepTime = step.CreatedDate;
					 			discApprovalHistory.Time_Final_Approved_Rejected__c = stepTime;
					 			if (schedQuote.skipSalesManager__c){
									discApprovalHistory.Product_Manager__c = step.ActorId;
									discApprovalHistory.Time_Waiting_for_Product_Manager_Hours__c = (stepTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
						 			discApprovalHistory.Time_in_Approval_Process_Hours__c = discApprovalHistory.Time_Waiting_for_Product_Manager_Hours__c;
						 			discApprovalHistory.Time_Approved_Rejected_Product_Manager__c = stepTime;
					 			}else{
					 				discApprovalHistory.Sales_Manager__c = step.ActorId;
					 				discApprovalHistory.Time_Waiting_for_Sales_Manager_Hours__c = (stepTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
					 				discApprovalHistory.Time_in_Approval_Process_Hours__c = discApprovalHistory.Time_Waiting_for_Sales_Manager_Hours__c;
						 			discApprovalHistory.Time_Approved_Rejected_By_Sales_Manager__c = stepTime;
					 			}
					 			
					 			break;				 		
					 		}
						}
						
						if (schedQuote.Approval_History__c.contains('Final Rejection'))
				 			discApprovalHistory.Current_Status__c = 'Completed - Rejected';
					 	else
							discApprovalHistory.Current_Status__c = 'Completed - Approved';
					}else if (schedQuote.Approval_History__c.equals('Recalled')){
					 	for (ProcessInstanceHistory step : processItems){
							if (step.StepStatus.equals('Recalled')){
							 	discApprovalHistory.Current_Status__c = 'Recalled';
							 	DateTime stepTime = step.CreatedDate;
							 	discApprovalHistory.Time_Recalled__c = stepTime;
								break;
							}
					 	}
					 }	
				}
			}
		}		
		UPSERT discountApprovalMap.values();			
		
		DELETE schedQuotes;							
	}
}