public without sharing class GenerateContractExtension {
	
	public Id idContract {get; set;}
    public String sContractId {get; set;}
    public Wireline_Contract__c aContract {get; set;}
	public Map<String, Boolean> productData {get; set;}

	public GenerateContractExtension(ApexPages.StandardController controller) {
		sContractId = ApexPages.currentPage().getParameters().get('id');
        
        if (!Utils.isEmpty(sContractId)){
            idContract = sContractId.substring(0);
        }else
        	return;
    
    /*
    Account
    ActivatedBy
    BillingAddress
    CompanySigned
    Owner
    RecordType
     CreatedBy
     CustomerSigned
     LastModifiedBy
     ShippingAddress
    */
    
    	aContract = [SELECT id, Account_Name__r.Name, Account_Name__r.Owner.Name, Activated_Date__c, Company_Signed_Date__c, Contract_End_Date__c, Name, Contract_Number__c, Contract_Start_Date__c, Contract_Term_months__c, Customer_Signed_Date__c, Customer_Signed_Title__c, Description__c, Owner_Expiration_Notice__c, Special_Terms__c, Status__c,
    	ANI_Serv_in_Lieu_of_Min_Commitment__c, Business_Downturn__c, Chronic_Outage__c, Contract_Type__c, Co_terminous_Services__c, Credit_Rounding__c, Documents_to_be_included__c, Exclusivity_in_lieu_of_min_commitment__c, Fast_COR__c, Free_Short_Calls__c, Install_Date__c, Month_Month_Renewal_at_Incr_Rates__c, MRC__c, 
		NRC__c, Price_type__c, Product_Data__c, Product_Voice__c, Rate_Review_Competitive__c, Rate_Review_Internal__c, Rate_Review_Market__c,  Remining__c, Renewal_Type__c, Service_Lease__c, Supplier_Name__c, Technology_Upgrade__c, Total_Contract_Value__c, Total_Remining_Contract_Value__c, Trial_Period__c, Waived_Charges_Other__c, 
		Waived_DAL_Charges__c, Waived_DAL_Charges_based_on_utilization__c, Waived_Installs__c, Work_Category__c, Channel__c, Customer_Type__c, Renewal_Notification_Period__c, Renewal_Notification_Period_Other__c, Termination_Notification_Period__c, Termination_Notification_Period_Other__c, Termination_Option__c, Old_Rate__c, 
		New_Rate__c, Minimum_Commitments__c, Standard_Contract__c, Customer_Sign_Off__c  
    	
    	  FROM Wireline_Contract__c WHERE Id = :idContract];		    	  
	}
	
	
}