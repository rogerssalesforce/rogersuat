/*********************************************************************************
Class Name      : CPQ_CreateLegalNameChangeController
Description     : Controller class for creating a new Legal Name Change Amendment  
Created By      : Akshay Ahluwalia 
Created Date    : 24-Sept-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Akshay Ahluwalia         24-Sept-2015           Updating the Restricted Flag 
Aakanksha Patel          22-Oct-2015            Method for Creating a new Legal Name Change Amendment  
Alina Balan              03-Feb-2016            Added "isRunningTest" verification
*********************************************************************************/

public class CPQ_CreateLegalNameChangeController
{
    public Id accountID {get;set;}
    public boolean restrictedAccount {get;set;}
    @TestVisible private String sResBody = '';
    
    //Constructor method
    public CPQ_CreateLegalNameChangeController()
    {
        accountID = ApexPages.currentPage().getParameters().get('Id');
    }
    
    /*********************************************************************************
    Method Name    : CreateLegalNameChangeAgreementRecord
    Description    : Method for Creating a new Legal Name Change Amendment  
    Developer      : Aakanksha Patel
    Date           : 22-Oct-2015
    *********************************************************************************/
    
    public PageReference CreateLegalNameChangeAgreementRecord()
    {
        accountID = ApexPages.currentPage().getParameters().get('Id');
        //Custom Setting for the AgreementID Prefix
        CPQ_Settings__c cpq_setting = CPQ_Settings__c.getInstance();
        
        List<RecordType> LegalNameChangeRecordTypId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Rogers_EBU_Agreement_Legal_Name_Change' AND SobjectType = 'Apttus__APTS_Agreement__c'];
        
        //Null check on Account
        if(accountID != null)
        {
            Account oAccount = [select id,name, Restricted__c from account where id =:accountID ];
            //Check if the related Account is Restricted and updating the flag
            if(oAccount.Restricted__c!=null)
            if(oAccount.Restricted__c.equalsIgnoreCase('Yes'))
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CPQ_AccountRestrictedError));
                restrictedAccount = true;
                return Null;
            }
            
            HttpRequest req = new HttpRequest();
            req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
            req.setHeader('Content-Type', 'application/json');
            String toolingendpoint = 'https://rogersb2b--ddev2.cs25.my.salesforce.com/services/data/v33.0/tooling/';
            //query for custom fields
            toolingendpoint += 'query/?q=select+id,DeveloperName,FullName+from+CustomField+where+DeveloperName+in(\'Status_Category\',\'Status\',\'Account\')';
            req.setEndpoint(toolingendpoint);
            req.setMethod('GET');
            Http h = new Http();
            if (!Test.isRunningTest()) {
                HttpResponse res = h.send(req);
                sResBody = res.getBody();
            }
            system.debug('sResBody = ' + sResBody);
            CPQ_ToolingWrapperClass fieldWrapper = CPQ_ToolingWrapperClass.parse(sResBody);  
            String StatusFieldId;
            String StatusCategoryFieldId;
            String AccountFieldId;    
            if(fieldWrapper !=null && fieldWrapper.records != null){
               for(CPQ_ToolingWrapperClass.Records record : fieldWrapper.records){
                   if(record.fullname == 'APTS_Agreement__c.Apttus__Status__c'){
                      system.debug('found Status : '+ record.id);
                      StatusFieldId = record.id.left(15); 
                   }
                   else if(record.fullname == 'APTS_Agreement__c.Apttus__Status_Category__c'){
                       system.debug('found Status Category: '+ record.id);                   
                       StatusCategoryFieldId = record.id.left(15);
                   }
                   else if(record.fullname == 'APTS_Agreement__c.Apttus__Account__c'){
                       system.debug('found Account : '+ record.id);                   
                        AccountFieldId = record.id.left(15);   
                   }
               }
                
            }
            
            //For creating new Amendment Record
            PageReference pg = New PageReference('/'+cpq_setting.AgreementIDPrefix__c+'/e?&RecordType='+LegalNameChangeRecordTypId[0].id+'&Name='+Label.CPQ_LegalNameChangeAmendment+'&nooverride=1&'+cpq_setting.LegalNameChangeFlagAgreementID__c+'=1&'+cpq_setting.AgreementOldLegalNameFieldID__c+'='+oAccount.name+'&'+AccountFieldId +'='+oAccount.name+'&'+StatusFieldId+'='+Label.CPQ_AgreementRequestApproval+'&'+StatusCategoryFieldId+'='+Label.CPQ_AgreementRequest+'&retURL=%2F'+oAccount.id);
            return pg;
        }
        else
        {
            return null;
        }
    }
}