/*
===============================================================================
Class Name : Test_TAG_CreateAccTeamNewChild_schedular
===============================================================================
PURPOSE: This is a test class for TAG_CreateAccTeamforNewChild_schedular class

COMMENTS: 

Developer: Aakanksha Patel
Date: 12/3/2015


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
12/3/2015               Aakanksha Patel        Created
===============================================================================

*/
@isTest(SeeAllData = false)
private class Test_TAG_CreateAccTeamNewChild_schedular{
 static testmethod void testMethod1(){
                
        Test.StartTest();
        TAG_CreateAccTeamforNewChild_schedular obj= new TAG_CreateAccTeamforNewChild_schedular ();
        String sch = '0 0 23 * * ?';
        system.schedule('TAG_CreateAccTeamNewChildTesting', sch, obj);
        System.assert(true);
        Test.stopTest();    

    }
}