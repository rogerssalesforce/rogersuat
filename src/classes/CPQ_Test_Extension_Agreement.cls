/*********************************************************************************
Class Name      : CPQ_Test_Extension_Agreement 
Description     : This class a test class for CPQ_Extension_Agreement. 
Created By      : Joseph Toms
Created Date    : 28-Nov-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Joseph Toms              12-Nov-15              Original version
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest
public with sharing class CPQ_Test_Extension_Agreement {
static User TestAEUser;
    static Account oAccount;
    static Opportunity oOpportunity;
    static Apttus_Proposal__Proposal__c oQuote;
    static Apttus_Config2__PriceList__c oPriceList;
    static Apttus__APTS_Agreement__c oAgreement;
    private static CPQ_Settings__c CS;
    private static final String CPQ_STATUS_CATEGORY_IN_SIGNATURES = 'In Signatures';
    private static final String CPQ_STATUS_SIGNATURE_NOTREQUIRED = 'Signature Not Required';
    private static final String CPQ_STATUS_CATEGORY_IN_REQUEST ='Request';
    private static final String CPQ_STATUS_INTERNAL_SIGNATURE='Internal Signatures';
   
     /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testMethod void createTestData() {
         //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        cs  = CPQ_Settings__c.getOrgDefaults();
        cs.Apttus_eSignature_URL__c = '/apex/Apttus_CMDSign__CMDocuSignCreateEnvelope?id=';
        cs.Agreement_Category_Status_to_Close_Opp__c=CPQ_STATUS_CATEGORY_IN_REQUEST+','+CPQ_STATUS_CATEGORY_IN_SIGNATURES;
        cs.Agreement_Status_to_Close_Opp__c=CPQ_STATUS_SIGNATURE_NOTREQUIRED+','+CPQ_STATUS_INTERNAL_SIGNATURE;
           
        upsert cs;
         //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');      
        system.runAs(TestAEUser){
       
        //Create PriceList
        oPriceList = CPQ_Test_Setup.newPriceList();
        insert oPriceList ;
        Apttus_Config2__PriceList__c pl =  [select id, name from  Apttus_Config2__PriceList__c where id =: oPriceList.id];
        System.assertEquals(oPriceList.Id, pl.id);
        
        //Create Account
        oAccount = CPQ_Test_Setup.newAccount('Test Account');
        insert oAccount;
        Account acc =  [select id, name from Account where id =: oAccount.id];
        System.AssertEquals('Test Account', oAccount.Name);
       
        //Create Opportunity
        oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
        insert oOpportunity;
        Opportunity opp =  [select id, name, Account.ID from Opportunity where id =: oOpportunity.id];
        System.AssertEquals(oAccount.Id, opp.Account.ID );
      
        //Create Quote
        oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
        insert oQuote;
        Apttus_Proposal__Proposal__c quote =  [select id, name, Apttus_Proposal__Opportunity__r.ID from Apttus_Proposal__Proposal__c where id =: oQuote.id];
        System.AssertEquals(oOpportunity.Id, quote.Apttus_Proposal__Opportunity__r.ID);
       
        //Create Agreement
        oAgreement= CPQ_Test_Setup.newAgreement(oAccount, oOpportunity,oQuote);
        insert oAgreement;
        Apttus__APTS_Agreement__c agr =  [select id, name, Apttus__Account__r.Id, Apttus__Related_Opportunity__r.ID from  Apttus__APTS_Agreement__c where id =: oAgreement.id];
        System.AssertEquals(oAccount.Id, agr.Apttus__Account__r.Id);
        }
    }
    
   /***************************************************************************************************
    Method Name    : testCPQ_Extension_Agreement
    Description    : Method to test CPQ_Extension_Agreement 
    Return Type    : void  
    Parameter      : Nil              
    ******************************************************************************************************/ 
    static testMethod void testCPQ_Extension_Agreement(){
        
        createTestData();
        system.runAs(TestAEUser){
        
        test.startTest(); 
                
        ApexPages.StandardController sc = new ApexPages.StandardController(oAgreement);
        PageReference pageRef = Page.CPQ_WinReasonEntry;
        Test.setCurrentPageReference(pageRef);        
        pageref.getParameters().put('Id', oAgreement.Id);        
        CPQ_Extension_Agreement eAgreement= new CPQ_Extension_Agreement(sc);
        eAgreement.oAgreement.Apttus__Related_Opportunity__r.Win_Reason__c='Price';
        eAgreement.oAgreement.Apttus__Related_Opportunity__r.Win_Loss_Description__c='Test Description';
        // test  save method
        eAgreement.saveWinReason();        
        Apttus__APTS_Agreement__c[] oAgreements = [SELECT Id,  Apttus__Related_Opportunity__r.Win_Loss_Description__c, Apttus__Related_Opportunity__r.Win_Reason__c
                                                     FROM Apttus__APTS_Agreement__c WHERE Id =: oAgreement.Id];        
        System.assertEquals(eAgreement.oAgreement.Apttus__Related_Opportunity__r.Win_Reason__c, oAgreements[0].Apttus__Related_Opportunity__r.Win_Reason__c);
        System.assertEquals(eAgreement.oAgreement.Apttus__Related_Opportunity__r.Win_Loss_Description__c, oAgreements[0].Apttus__Related_Opportunity__r.Win_Loss_Description__c);
        //test getRedirectLink() method
        System.assert(eAgreement.getRedirectLink().contains(cs.Apttus_eSignature_URL__c)); 
        //test backToAgreement()  method    
        System.assert(eAgreement.backToAgreement().getUrl()!=null);
      
        Test.stopTest();
    }
    
}
}