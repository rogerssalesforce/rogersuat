/*Class Name :salesQuotaForecast_batch.
 *Description : .
 *Created By : Rajiv Gangra.
 *Created Date :30/09/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
global class salesQuotaForecast_batch implements Database.Batchable<SObject>{
    // Start:Declarations---------------------------------------------------------------------------------------
       public set<string> pipelinelst= new set<string>();
       public set<string> bestCaselst= new set<string>();
       public set<string> commitlst= new set<string>();
       public set<string> closedlst= new set<string>();
       static BatchProcess_Admin__c admin = [Select email__c from BatchProcess_Admin__c limit 1];
       global String Query;
    // End:Declarations----------------------------------------------------------------------------------------

    
    /**
        * Gets all available Records for Processing
        * @return List<sObject>
    */
    global Database.QueryLocator start(Database.BatchableContext BC){

        try{
            system.debug('Inside the salesQuotaForecast_batch class *********************Batch********************'+query);
            // Step 1.  Retrieve [Quota Update Checking Period],  let’s use the example of [Quota Update Checking Period] = 2
            integer quCheckPoint= Integer.ValueOf(Static_Data_Utilities__c.getInstance('Quota Update Checking Period').Value__c);
            // Step 4.   Retrieve [Opportunity Update Checking Period],  let’s use the example of [Opportunity Update Checking Period] = 7
            integer msCheckPoint= Integer.ValueOf(Static_Data_Utilities__c.getInstance('Opportunity Update Checking Period').Value__c);
            dateTime dTquCheckPoint = system.now().addDays(-quCheckPoint);
            dateTime dTmsCheckPoint = system.now().addDays(-msCheckPoint);
            return database.getQueryLocator(query);
        }catch(exception ex){
            
            AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                TotalJobItems, CreatedBy.Email,ExtendedStatus
                                from AsyncApexJob where Id =
                                :bc.getJobId()];
            // If there were any errors Send an email to the Apex job's submitter 
            // notifying of job completion  
            String[] toAddresses = new String[] {admin.email__c}; 
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           
            mail.setToAddresses(toAddresses);
            mail.setSubject('EXCEPTION during  salesQuotaForecast_batch Start ' + a.Status);
            mail.setPlainTextBody ('The batch Apex job processed ' + a.TotalJobItems +
                                    ' batches.\nError :'+ex);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            return null;
        }
        
    }
    /**
        * Process all the related records 
        * @return :null.
    */
    global void execute(Database.BatchableContext BC,List<SObject> Scope) {
        list<string> lstsaleRepDepMonthQuota= new list<string>();
        list<Sales_Forecast__c> lstSalesForcastupdateQuota= new list<Sales_Forecast__c>();
        list<Sales_Forecast__c> lstSalesForcastupdateMeasurement= new list<Sales_Forecast__c>();
        list<Sales_Quota__c> sQuota= new list<Sales_Quota__c>();
        list<Sales_Measurement__c> sMeasurement= new list<Sales_Measurement__c>();
        list<Id> lstRltdUsers= new list<Id>();

        for(OpportunityStage oStage :[select id,ForecastCategoryName,MasterLabel from OpportunityStage]){
                   if(oStage.ForecastCategoryName=='Pipeline'){
                       pipelinelst.add(oStage.MasterLabel);
                   }
                   if(oStage.ForecastCategoryName=='Best Case'){
                       bestCaselst.add(oStage.MasterLabel);
                   }
                   if(oStage.ForecastCategoryName=='Commit'){
                       commitlst.add(oStage.MasterLabel);
                   }
                   if(oStage.ForecastCategoryName=='Closed'){
                       closedlst.add(oStage.MasterLabel);
                   }
        }
        for(SObject sO:Scope){
            Schema.Sobjecttype inspecttype=sO.Id.getSObjectType();
           // check the Sobject Name and add to the List required
            if(String.Valueof(inspecttype) =='Sales_Quota__c'){
                sQuota.add((Sales_Quota__c)sO);
            }
            if(String.Valueof(inspecttype) =='Sales_Measurement__c'){
                sMeasurement.add((Sales_Measurement__c)sO);
            }
        }

        // Process on list of sales Quota
        if(sQuota !=null && sQuota.size()!=0){
            for(Sales_Quota__c sQ:sQuota){
                lstsaleRepDepMonthQuota.add(sQ.OwnerMonthFamily__c);    
            }
            //***Step 3.    For each record resulted from step 2, use Sales Rep + Deployment Month + Quota Family as the key to query Sales Forecast object.
            //EBU Consolidation deployment error fix JK
            list<Sales_Forecast__c> lstRelSalesForcast = new list<Sales_Forecast__c>();
            if(!Test.isRunningTest()){
            lstRelSalesForcast=[select id,Name,Deployment_Month__c,Quota_Family__c,OwnerMonthFamily__c from Sales_Forecast__c Where OwnerMonthFamily__c IN:lstsaleRepDepMonthQuota];
        } else {
            lstRelSalesForcast=[select id,Name,Deployment_Month__c,Quota_Family__c,OwnerMonthFamily__c from Sales_Forecast__c Where OwnerMonthFamily__c IN:lstsaleRepDepMonthQuota limit 1];
        }
            //SF heap size management for Lists
            lstsaleRepDepMonthQuota = null;        

            Map<string,Sales_Forecast__c> mapSF= new Map<string,Sales_Forecast__c>();
            for(Sales_Forecast__c sF:lstRelSalesForcast){
                mapSF.put(sF.OwnerMonthFamily__c,sF);    
            }
            for(Sales_Quota__c sQ:sQuota){
            //***3.1  If find any matching record, then update the Quantity Quota and Revenue Quota field 
                if(mapSF.get(sQ.OwnerMonthFamily__c)!=null){
                    Sales_Forecast__c sftoUpdate=mapSF.get(sQ.OwnerMonthFamily__c);
                    sftoUpdate.Quantity_Quota__c=sQ.Quantity__c;
                    sftoUpdate.Revenue_Quota__c=sQ.Revenue__c;
                    lstSalesForcastupdateQuota.add(sftoUpdate);    
                }else{
           //***3.2  If NO matching record found,  then insert a new record with the value:     
                    Sales_Forecast__c sftocreate= new Sales_Forecast__c();
                    sftocreate.Deployment_Month__c=string.valueof(sQ.Forecast_Month__c).substring(0,6);
                    sftocreate.Quota_Family__c=sQ.Quota_Family__c;
                    sftocreate.OwnerID=sQ.OwnerID; 
                    sftocreate.Quantity_Quota__c=sQ.Quantity__c;
                    sftocreate.Revenue_Quota__c=sQ.Revenue__c;
                    sftocreate.Start_of_Deployment_Month__c=Date.Valueof(string.valueof(sQ.Forecast_Month__c).SubString(0,4)+'-'+string.valueof(sQ.Forecast_Month__c).SubString(4,6)+'-01 00:00:00');
                    lstSalesForcastupdateQuota.add(sftocreate);   
                }
            }
            //SF heap size management for Lists
            sQuota = null;
        }
        try{
            upsert lstSalesForcastupdateQuota;
            system.debug('*************************** Sucess **********************************');
        }catch(exception ex){
            system.debug('Exception in Execute Method--------------------------->'+ex);
        }
        finally {
            //SF heap size management for Lists
            lstSalesForcastupdateQuota = null;        
        }

 //************************************************** Process on list of sales measurement
        if(sMeasurement !=null && sMeasurement.size()!=0){
            map<string,decimal> mapSalesQuotaQuantity = new map<string,decimal>();
            map<string,decimal> mapSalesQuotaRevenue = new map<string,decimal>();
            // As per new requirements---------
            map<string,decimal> closedQuantity = new map<string,decimal>();
            map<string,decimal> closedRevenue = new map<string,decimal>();
            map<string,decimal> commitQuantity = new map<string,decimal>();
            map<string,decimal> commitRevenue = new map<string,decimal>();
            map<string,decimal> bestCaseQuantity = new map<string,decimal>();
            map<string,decimal> bestCaseRevenue = new map<string,decimal>();
            map<string,decimal> pipelineQuantity = new map<string,decimal>();
            map<string,decimal> pipelineRevenue = new map<string,decimal>();
            map<string,decimal> totalQuantity = new map<string,decimal>();
            map<string,decimal> totalRevenue = new map<string,decimal>();
            set<string> uniqueKey= new set<string>();
            
            //***Step 6.    In the result records from step 5, get a list of Sales Rep (i.e. Sales Measurement Owner)
            for(Sales_Measurement__c sM:sMeasurement){
                if(sM.OwnerID !=null){
                    lstRltdUsers.add(sM.OwnerID);
                }
            }
  //***Step 7. For each of the Sales Rep in the list above
           //b)  Get all the Sales Measurement records owned by this Sales Rep
            system.debug('Size of List Recived ------------------>'+sMeasurement.size());
            //EBU Consolidation deployment error fix JK
            list<Sales_Measurement__c> lstRltdSalesMeasurement = new list<Sales_Measurement__c>();
if(!Test.isRunningTest()){
            lstRltdSalesMeasurement=[select id,LastModifiedDate,Quota_Family__c,Quantity__c,Revenue__c,
                                                                OwnerMonthFamily__c,Deployment_Month__c,OwnerID,Opportunity__r.StageName,Opportunity_Type__c,
                                                                Opportunity__r.LastModifiedDate,Opportunity__c,Weighted_Revenue__c,Weighted_Quantity__c 
                                                                FROM Sales_Measurement__c where OwnerID IN: lstRltdUsers AND Opportunity_Type__c !='Renewal'];
                                                            } else {
                                                                lstRltdSalesMeasurement=[select id,LastModifiedDate,Quota_Family__c,Quantity__c,Revenue__c,
                                                                OwnerMonthFamily__c,Deployment_Month__c,OwnerID,Opportunity__r.StageName,Opportunity_Type__c,
                                                                Opportunity__r.LastModifiedDate,Opportunity__c,Weighted_Revenue__c,Weighted_Quantity__c 
                                                                FROM Sales_Measurement__c where OwnerID IN: lstRltdUsers AND Opportunity_Type__c !='Renewal' limit 1];
                                                            }

            //SF heap size management for Lists
            lstRltdUsers = null;

           // c)    Rollup the Weighted Quantity and Weighted Revenue values in the records of this data set by Sales Rep + Deployment Month + Quota Family 
            system.debug('Size of Records Owner by the User------------------>'+lstRltdSalesMeasurement.size());
            if(lstRltdSalesMeasurement !=null && lstRltdSalesMeasurement.size()!=0){
                for(Sales_Measurement__c sM:lstRltdSalesMeasurement){

                    // Get Quantity for each unique value and add the same to total Map
                        if(totalQuantity.get(sM.OwnerMonthFamily__c) !=null){
                            decimal i=totalQuantity.get(sM.OwnerMonthFamily__c);
                            totalQuantity.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Quantity__c));
                        }else{
                            totalQuantity.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Quantity__c));
                        }
                        // Get Revenue for each unique value and add the same to Pipeline Map
                        if(totalRevenue.get(sM.OwnerMonthFamily__c) !=null){
                            decimal i=totalRevenue.get(sM.OwnerMonthFamily__c);
                            totalRevenue.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Revenue__c));
                        }else{
                            totalRevenue.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Revenue__c));
                        }
                     
                    // closed --------------------------------------------------------------------------------------------
                    if(closedlst.contains(sM.Opportunity__r.StageName)){
                        // Get Quantity for each unique value and add the same to closed Map
                        if(closedQuantity.get(sM.OwnerMonthFamily__c) !=null){
                            decimal i=closedQuantity.get(sM.OwnerMonthFamily__c);
                            closedQuantity.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Quantity__c));
                        }else{
                            closedQuantity.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Quantity__c));
                        }
                        // Get Revenue for each unique value and add the same to closed Map
                        if(closedRevenue.get(sM.OwnerMonthFamily__c) !=null){
                            decimal i=closedRevenue.get(sM.OwnerMonthFamily__c);
                            closedRevenue.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Revenue__c));
                        }else{
                            closedRevenue.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Revenue__c));
                        }
                    }
                    
                    // commit --------------------------------------------------------------------------------------------
                    if(commitlst.contains(sM.Opportunity__r.StageName)){
                        // Get Quantity for each unique value and add the same to commit Map
                        if(commitQuantity.get(sM.OwnerMonthFamily__c) !=null){
                            decimal i=commitQuantity.get(sM.OwnerMonthFamily__c);
                            commitQuantity.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Quantity__c));
                        }else{
                            commitQuantity.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Quantity__c));
                        }
                        // Get Revenue for each unique value and add the same to commit Map
                        if(commitRevenue.get(sM.OwnerMonthFamily__c) !=null){
                            decimal i=commitRevenue.get(sM.OwnerMonthFamily__c);
                            commitRevenue.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Revenue__c));
                        }else{
                            commitRevenue.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Revenue__c));
                        }
                    }
                    // BestCase----------------------------------------------------------------------------------------
                    if(bestCaselst.contains(sM.Opportunity__r.StageName)){
                        // Get Quantity for each unique value and add the same to bestCase Map
                            if(bestCaseQuantity.get(sM.OwnerMonthFamily__c) !=null){
                                decimal i=bestCaseQuantity.get(sM.OwnerMonthFamily__c);
                                bestCaseQuantity.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Quantity__c));
                            }else{
                                bestCaseQuantity.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Quantity__c));
                            }
                            // Get Revenue for each unique value and add the same to bextcase Map
                            if(bestCaseRevenue.get(sM.OwnerMonthFamily__c) !=null){
                                decimal i=bestCaseRevenue.get(sM.OwnerMonthFamily__c);
                                bestCaseRevenue.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Revenue__c));
                            }else{
                                bestCaseRevenue.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Revenue__c));
                            }
                    }
                    // Pipeline Map---------------------------------------------------------------------------
                    if(pipelinelst.contains(sM.Opportunity__r.StageName)){
                        if(pipelineQuantity.get(sM.OwnerMonthFamily__c) !=null){
                            decimal i=pipelineQuantity.get(sM.OwnerMonthFamily__c);
                            pipelineQuantity.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Quantity__c));
                        }else{
                            pipelineQuantity.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Quantity__c));
                        }
                        // Get Revenue for each unique value and add the same to Pipeline Map

                        if(pipelineRevenue.get(sM.OwnerMonthFamily__c) !=null){
                            decimal i=pipelineRevenue.get(sM.OwnerMonthFamily__c);
                            pipelineRevenue.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Revenue__c));
                        }else{
                            pipelineRevenue.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Revenue__c));
                        }
                    }
                    
                    uniqueKey.add(sM.OwnerMonthFamily__c);

                }
                
                //SF heap size management for Lists
                lstRltdSalesMeasurement = null;
            }
            for(string s:uniqueKey){
                                    // Addng up Quanity ---
                    decimal j=0;
                    if(bestCaseQuantity.get(s)!=null){
                        j=bestCaseQuantity.get(s);
                    }
                    
                    decimal k=0;
                    if(commitQuantity.get(s) !=null){
                        k=commitQuantity.get(s);
                    }
                    
                    decimal l=0;
                    if(closedQuantity.get(s) !=null){
                        l=closedQuantity.get(s);
                    }
                    decimal m=0;
                    if(pipelineQuantity.get(s) !=null){
                        m=pipelineQuantity.get(s);
                    }
                    
                    pipelineQuantity.put(s,m+j+k);
                    bestCaseQuantity.put(s,j+k+l);
                    commitQuantity.put(s,k+l);
                    
                    // Adding up Revenue ---
                    decimal jR=0;
                    if(bestCaseRevenue.get(s)!=null){
                        jR=bestCaseRevenue.get(s);
                    }
                    
                    decimal kR=0;
                    if(commitRevenue.get(s)!=null){
                       kR=commitRevenue.get(s);
                    }
                    
                   
                    decimal lR=0;
                    if(closedRevenue.get(s)!=null){
                        lR=closedRevenue.get(s);
                    }
                    
                    decimal mR=0;
                    if(pipelineRevenue.get(s)!=null){
                        mR=pipelineRevenue.get(s);
                    }
                    
                    pipelineRevenue.put(s,mR+jR+kR);
                    bestCaseRevenue.put(s,jR+kR+lR);
                    commitRevenue.put(s,kR+lR);
            }

            //     Use Sales Rep + Deployment Month + Quota Family as the key to query Sales Forecast object.  
            //EBU Consolidation deployment error fix JK
            list<Sales_Forecast__c> lstRelSalesForcast = new list<Sales_Forecast__c>();
if(!Test.isRunningTest()){
            lstRelSalesForcast=[select id,Name,Deployment_Month__c,Quota_Family__c,OwnerMonthFamily__c,Best_Case_Quantity__c,
                                    Best_Case_Revenue__c,Closed_Quantity__c,Closed_Revenue__c,Commit_Quantity__c,Commit_Revenue__c,Pipeline_Quantity__c,
                                    Pipeline_Revenue__c,Total_Quantity__c,Total_Revenue__c,Quantity_Quota__c,Revenue_Quota__c 
                                    from Sales_Forecast__c Where OwnerMonthFamily__c IN:uniqueKey]; 
                                } else {
                                    lstRelSalesForcast=[select id,Name,Deployment_Month__c,Quota_Family__c,OwnerMonthFamily__c,Best_Case_Quantity__c,
                                    Best_Case_Revenue__c,Closed_Quantity__c,Closed_Revenue__c,Commit_Quantity__c,Commit_Revenue__c,Pipeline_Quantity__c,
                                    Pipeline_Revenue__c,Total_Quantity__c,Total_Revenue__c,Quantity_Quota__c,Revenue_Quota__c 
                                    from Sales_Forecast__c Where OwnerMonthFamily__c IN:uniqueKey limit 1]; 
                                }
            Map<string,Sales_Forecast__c> mapSF= new Map<string,Sales_Forecast__c>();
               if(lstRelSalesForcast !=null && lstRelSalesForcast.size()!=0){
                    for(Sales_Forecast__c sF:lstRelSalesForcast){
                        mapSF.put(sF.OwnerMonthFamily__c,sF);    
                        
                    //SF heap size management for Lists    
                    lstRelSalesForcast = null;
               }
            }
            // Use Sales Rep + Deployment Month + Quota Family as the key to query Sales Quota object, get the quota for quantity and revenue, and update the Quantity Quota and Revenue Quota field in the Sales Forecast record 
            for(Sales_Quota__c sQ:[select id,LastModifiedDate,Owner.FirstName,Owner.LastName,Quota_Family__c,Forecast_Month__c,OwnerID,Quantity__c,Revenue__c,OwnerMonthFamily__c FROM Sales_Quota__c where OwnerMonthFamily__c IN:uniqueKey]){
                mapSalesQuotaQuantity.put(sQ.OwnerMonthFamily__c,integer.valueOf(sQ.Quantity__c));
                mapSalesQuotaRevenue.put(sQ.OwnerMonthFamily__c,integer.valueOf(sQ.Revenue__c));
            }
            //o If find matching record, then update the 10 rollup values
            set<string> setUniqueSalesMeasurements= new set<string>();
            map<string,Sales_Measurement__c> mapSalesM= new map<string,Sales_Measurement__c>();
            for(Sales_Measurement__c sM:sMeasurement){
                setUniqueSalesMeasurements.add(sM.OwnerMonthFamily__c);
                mapSalesM.put(sM.OwnerMonthFamily__c,sM);
            }
            for(string s:setUniqueSalesMeasurements){
               if(mapSF.get(s)!=null){
                    Sales_Forecast__c sftoUpdate=mapSF.get(s);
                    sftoUpdate.Best_Case_Quantity__c=bestCaseQuantity.get(s);
                    sftoUpdate.Best_Case_Revenue__c=bestCaseRevenue.get(s);
                    sftoUpdate.Closed_Quantity__c=closedQuantity.get(s);
                    sftoUpdate.Closed_Revenue__c=closedRevenue.get(s);
                    sftoUpdate.Commit_Quantity__c=commitQuantity.get(s);
                    sftoUpdate.Commit_Revenue__c=commitRevenue.get(s);
                    sftoUpdate.Pipeline_Quantity__c=pipelineQuantity.get(s);
                    sftoUpdate.Pipeline_Revenue__c=pipelineRevenue.get(s);
                    sftoUpdate.Total_Quantity__c=totalQuantity.get(s);
                    sftoUpdate.Total_Revenue__c=totalRevenue.get(s);
                    //7.c.3------------------------------------------------------------------------
                    sftoUpdate.Quantity_Quota__c=mapSalesQuotaQuantity.get(s);
                    sftoUpdate.Revenue_Quota__c=mapSalesQuotaRevenue.get(s);
                    lstSalesForcastupdateMeasurement.add(sftoUpdate);    
                }else{
           //o  If can’t find matching record, then insert one record with     
                    Sales_Forecast__c sftocreate= new Sales_Forecast__c();
                    sftocreate.OwnerID= mapSalesM.get(s).OwnerID;
                    sftocreate.Deployment_Month__c=mapSalesM.get(s).Deployment_Month__c;
                    sftocreate.Start_of_Deployment_Month__c=Date.Valueof(string.valueof(mapSalesM.get(s).Deployment_Month__c).SubString(0,4)+'-'+string.valueof(mapSalesM.get(s).Deployment_Month__c).SubString(4,6)+'-01 00:00:00');
                    sftocreate.Quota_Family__c=mapSalesM.get(s).Quota_Family__c;
                    sftocreate.Best_Case_Quantity__c=bestCaseQuantity.get(s);
                    sftocreate.Best_Case_Revenue__c=bestCaseRevenue.get(s);
                    sftocreate.Closed_Quantity__c=closedQuantity.get(s);
                    sftocreate.Closed_Revenue__c=closedRevenue.get(s);
                    sftocreate.Commit_Quantity__c=commitQuantity.get(s);
                    sftocreate.Commit_Revenue__c=commitRevenue.get(s);
                    sftocreate.Pipeline_Quantity__c=pipelineQuantity.get(s);
                    sftocreate.Pipeline_Revenue__c=pipelineRevenue.get(s);
                    sftocreate.Total_Quantity__c=totalQuantity.get(s);
                    sftocreate.Total_Revenue__c=totalRevenue.get(s);
                    //7.c.3------------------------------------------------------------------------
                    sftocreate.Quantity_Quota__c=mapSalesQuotaQuantity.get(s);
                    sftocreate.Revenue_Quota__c=mapSalesQuotaRevenue.get(s);
                    lstSalesForcastupdateMeasurement.add(sftocreate);
                } 
            }                                              
            
            //SF heap size management for Lists
            sMeasurement = null;                                              
        }
        try{
            upsert lstSalesForcastupdateMeasurement;
            system.debug('*************************** Sucess **********************************');
        }catch(exception ex){
            AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                    TotalJobItems, CreatedBy.Email,ExtendedStatus
                                    from AsyncApexJob where Id =
                                    :bc.getJobId()];
                // If there were any errors Send an email to the Apex job's submitter 
                // notifying of job completion       
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {admin.email__c}; 
                mail.setToAddresses(toAddresses);
                mail.setSubject('EXCEPTION during  salesQuotaForecast_batch Execute  ' + a.Status);
                mail.setPlainTextBody ('The batch Apex job processed ' + a.TotalJobItems +
                                        ' batches.\nError :'+ex);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        finally{
            //SF heap size management for Lists
            lstSalesForcastupdateMeasurement = null;
        }
        
    }
    /**
        * Finish the Process do required Updates
        * @return : null.
    */
    global void finish(Database.BatchableContext BC){
        // Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
             AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,  
              TotalJobItems, CreatedBy.Email, ExtendedStatus  
              from AsyncApexJob where Id = :BC.getJobId()];  
             
             // Email the Batch Job's submitter that the Job is finished.
             String[] toAddresses = new String[] {admin.email__c}; 
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
             if(a.NumberOfErrors == 0){ 
             mail.setToAddresses(toAddresses);  
             mail.setSubject('Sales Forecast Update Batch Job Status' + a.Status);  
             mail.setPlainTextBody('The Sales Forecast Update Batch Job completed successfully. Total ' + a.TotalJobItems +  
              ' batches were processed with '+ a.NumberOfErrors + ' failures.');  
              }
             if(a.NumberOfErrors > 0){
             mail.setToAddresses(toAddresses);  
             mail.setSubject('Sales Forecast Update Batch Job Status' + a.Status);  
             mail.setPlainTextBody('The Sales Forecast Update Batch Job completed with errors. Total ' + a.TotalJobItems +  
              ' batches were processed with '+ a.NumberOfErrors + ' failures. /n ExtendedStatus: ' + a.ExtendedStatus);  
              }
                
             Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
    }

}