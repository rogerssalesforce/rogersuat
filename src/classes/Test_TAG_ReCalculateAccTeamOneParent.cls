/**************************************************************************************
Apex Class Name     : Test_TAG_ReCalculateAccTeamOneParent
Version             : 1.0 
Created Date        : 04/13/2015
Function            : This is the Test Class for 1. InitialLoadAccountTeamOneParent_batch
Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deepika Rawat             04/13/2015              Original Version
*************************************************************************************/
@isTest
private class Test_TAG_ReCalculateAccTeamOneParent
{  
     private static Account ParentAcc;
    private static Account ParentAcc2;
    private static Account ParentAcc3;
    private static Account ChildAcc1;
    private static Profile pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
    private static User userRec;
    private static User userRec2;
    private static User userRec3;
    private static  MSD_Code__c msdCode; 
    private static Shared_MSD_Code__c sharedMSD;
    private static Shared_MSD_Accounts__c sharedAccMSD;
    private static List<MSD_Member__c> msdMems;
    private static List<Shared_MSD_Member__c> sharedMSDMem;
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
    private static Static_Data_Utilities__c staticData_PageSize = new Static_Data_Utilities__c ();
    private static Static_Data_Utilities__c staticData_Limit = new Static_Data_Utilities__c ();

    /*
    Description : This method is used to create Test Data. 
    Parameters  : None
    Return Type : void
    */
    private static void setUpData()
    {       
         //Data for Custom Settings
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        TAG_CS.Team_Account_Access__c='Read';
        TAG_CS.Team_Custom_Object_Access__c='Read';
        TAG_CS.MSD_Recalcuate_Account_Team_Job__c ='MSD Owner,Owner,MSD Member';
        TAG_CS.Default_Account_Owner_Role__c='Owner';
        TAG_CS.Default_MSD_Member_Role__c='MSD Member';
        TAG_CS.Default_MSD_Owner_Role__c='MSD Owner';
        insert TAG_CS;
        staticData_PageSize.Name = 'AccountTransfer_PageSize';
        staticData_PageSize.Value__c ='1';
        insert staticData_PageSize;
        staticData_Limit.Name='AccountTransfer_RecordsLimit';
        staticData_Limit.Value__c='2';
        insert staticData_Limit;
        
        system.debug('pEmp***'+pEmp);

         userRec = new User(LastName = 'Mark O’Roger', Alias = 'alRoger1', Email='test@Rogertest.com', Username='test@Rogertest1.com', CommunityNickname = 'nickRog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec;
        userRec2 = new User(LastName = 'Mark O’Roger2', Alias = 'aRoger2', Email='test@Rogertest.com', Username='test@Rogertest2.com', CommunityNickname = 'nickRog2', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',  Assignment_Approver__c = userRec.id,Channel__c='Business Segment',Owner_Type__c='District');
        insert userRec2;

         userRec3 = new User(LastName = 'Mark O’Roger3', Alias = 'aRoger3', Email='test@Rogertest.com', Username='test@Rogertest3.com', CommunityNickname = 'nickRog3', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Assignment_Approver__c = userRec2.id,Channel__c='Business Segment');
        insert userRec3;

        //Account 1
        ParentAcc= new Account();
        ParentAcc.Name = 'ParentAcc';
        ParentAcc.Account_Status__c = 'Assigned';
        ParentAcc.ParentID= null;
        ParentAcc.OwnerId= userRec.id;
        ParentAcc.Recalculate_Team__c=true;
        ParentAcc.Account_Team_to_MAL__c=false;
        ParentAcc.BillingPostalCode ='A1A 1A1';
        insert ParentAcc;
        //child of ParentAcc
        ChildAcc1= new Account();
        ChildAcc1.Name = 'ChildAcc1';
        ChildAcc1.Account_Status__c = 'Assigned';
        ChildAcc1.ParentID= ParentAcc.Id;
        ChildAcc1.OwnerId= userRec.id;
        ChildAcc1.Account_Team_to_MAL__c=false;
        ChildAcc1.BillingPostalCode ='A1A 1A1';
        insert ChildAcc1;


        msdCode = new MSD_Code__c();
        msdCode.OwnerId = userRec.id ;
        msdCode.Account__c = ParentAcc.id ;
        msdCode.MSD_Code_External__c = '789';
        insert msdCode;

        sharedMSD = new Shared_MSD_Code__c();
        sharedMSD.OwnerId = userRec.id ;
        sharedMSD.Name = 'SharedMSD1';
        sharedMSD.MSD_Code_External__c = '12345';
        insert sharedMSD;

        sharedAccMSD = new Shared_MSD_Accounts__c();
        sharedAccMSD.Account__c=ParentAcc.id;
        sharedAccMSD.Shared_MSD_Code__c=sharedMSD.id;
        insert sharedAccMSD;

        MSD_Member__c mem1= new MSD_Member__c();  
        mem1.MSD_Code__c=msdCode.id;
        mem1.User__c=userRec2.id;
        insert mem1;
        Shared_MSD_Member__c mem2= new Shared_MSD_Member__c();
        mem2.Shared_MSD_Code__c=sharedMSD.id;
        mem2.User__c=userRec3.id;
        insert mem2;
        
        AccountTeamMember newMem3 = new AccountTeamMember();
        newMem3.TeamMemberRole = 'Owner';
        newMem3.AccountId = ParentAcc.Id;
        newMem3.UserId = ParentAcc.OwnerId; 
        insert newMem3;
        
        AccountTeamMember newMem = new AccountTeamMember();
        newMem.TeamMemberRole = 'SDC';
        newMem.AccountId = ChildAcc1.Id;
        newMem.UserId = ChildAcc1.OwnerId; 
        insert newMem;
    }
   
     /*
     Description : This is is a method to test the Batch for MSD District and MSD Owner Type
     Parameters  : None
     Return Type : void
     */
   
    private static testMethod void Test_InitialLoadBatch()
    {
        setUpData();
        Test.startTest();
         Database.BatchableContext scc;
            TAG_ReCalculateAccTeamOneParent_batch sc = new TAG_ReCalculateAccTeamOneParent_batch();
            sc.start(scc);
            List<Account> scope = ([select id, Recalculate_Team__c,Account_Team_to_MAL__c, ParentID,OwnerId FROM Account where Id =:ParentAcc.Id]);
            sc.execute(scc,scope);
            sc.finish(scc);
        Test.stopTest();     
    }
    
}