@isTest(SeeAllData = true)
private class Test_DiscountApprovalManager{
    private static Opportunity testOpp;
    private static Quote__c testQuote;
    private static Quote_Line_Item__c testQuoteLineItem;
    private static Product2 testProd2;
    private static list<PricebookEntry> thePBEList;
     static testmethod void vanillaTest(){
         DiscountApprovalManager obj = new DiscountApprovalManager();
        thePBEList = new list<PricebookEntry>([select Id, Product2.Family from PricebookEntry where Product2.Aggregated_Product__c = true and Product2.Family != null and Product2.IsActive = true]);
        
        List<ProcessInstance> approvals = [Select id,  Status,  TargetObjectId, (SELECT Id, 
                                            StepStatus, 
                                            ActorId,
                                            Actor.Name, 
                                            OriginalActor.Name, 
                                            CreatedDate 
                                            FROM StepsAndWorkitems Order By CreatedDate ASC) 
                                    FROM ProcessInstance 
                                    limit 1];
        
        testOpp = new Opportunity(Name = 'testOpp',
        StageName = 'testOpp',
        CloseDate = Date.today().addDays(2),Unified_Comm_Collaboration_Estimated__c =20);
        insert testOpp;
        
        testQuote = new Quote__c(Opportunity_Name__c = testOpp.Id);
        insert testQuote;
        List<Scheduled_Quote_Approval_History__c> listHistory = new List<Scheduled_Quote_Approval_History__c>();
         if(approvals!=null && approvals.size()>0){
              
         Scheduled_Quote_Approval_History__c h1 = new Scheduled_Quote_Approval_History__c ();
         h1.Approval_History__c='Test';
         h1.Quote__c=testQuote.id;
         h1.Approver_Comment__c ='Approve';
         h1.ProcessInstanceId__c=String.valueof(approvals[0].id);
         //insert h1;
         listHistory.add(h1);
         }
         //DiscountApprovalManager.updateApprovalHistory(listHistory);
     }
}