public class ContractRedirect {

    private ApexPages.StandardController controller;
    public String retURL {get; set;}
    public String saveNewURL {get; set;}
    public String rType {get; set;}
    public String cancelURL {get; set;}
    public String ent {get; set;}
    public String confirmationToken {get; set;}
    public String contractID {get; set;}
    public String contactID {get; set;}
    public String accountID {get; set;}
    public String accID {get; set;}
    Team_Assignment_Governance_Settings__c cs = Team_Assignment_Governance_Settings__c.getInstance();

    public ContractRedirect(ApexPages.StandardController controller) {
         this.controller = controller;

         retURL = ApexPages.currentPage().getParameters().get('retURL');
         rType = ApexPages.currentPage().getParameters().get('RecordType');
         cancelURL = ApexPages.currentPage().getParameters().get('cancelURL');
         ent = ApexPages.currentPage().getParameters().get('ent');
         confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
         saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
         contractID = ApexPages.currentPage().getParameters().get('def_Contract_id');
         contactID = ApexPages.currentPage().getParameters().get('def_contact_id');
         accID = ApexPages.currentPage().getParameters().get('def_account_id');
         accountID = ApexPages.currentPage().getParameters().get('accid');
         
    }

    public PageReference redirect(){
        
        List <RecordType > lRtc = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Wireline_Contract__c']);
        Map <string,Id> mapRTc = new Map <string,id> ();
        
        for (RecordType rtc : lRtc){
            mapRTc.put(rtc.Name,rtc.id);  
        }

        PageReference returnURL;

        // Redirect if Record Type corresponds to custom VisualForce page
        // Channel Prospect Opportunity appends 3D at the start of the record type here but not in the Edit
        if(!Utils.isEmpty(rType) && ('' + mapRTc.get('Enterprise Contract Renewal')).contains(rType)) {
              returnURL = new PageReference('/apex/ContractRenewalEditPage');
        } else {
              returnURL = new PageReference(cs.Wireline_Contract_URL__c);
        }

        returnURL.getParameters().put('retURL', retURL);
        returnURL.getParameters().put('RecordTypeId', rType);
        returnURL.getParameters().put('cancelURL', cancelURL);
        returnURL.getParameters().put('ent', ent);
        returnURL.getParameters().put('_CONFIRMATIONTOKEN', confirmationToken);
        returnURL.getParameters().put('save_new_url', saveNewURL);
        returnURL.getParameters().put('nooverride', '1');

        if(contractID != null){
            returnURL.getParameters().put('def_Contract_id', contractID);
        }
        
        if(accountID != null){
            returnURL.getParameters().put('accid', AccountID);
        }
        
        if(accID != null){
            returnURL.getParameters().put('def_Account_id', accID);
        }
        

        returnURL.setRedirect(true);
        return returnURL;

    }
}