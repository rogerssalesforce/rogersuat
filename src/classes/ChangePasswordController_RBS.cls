/**
 * An apex page controller that exposes the change password functionality
 */
public with sharing class ChangePasswordController_RBS {
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}        
    
    public PageReference changePassword() {
        return Site.changePassword(newPassword, verifyNewPassword, oldpassword);    
    }     
    
     public ChangePasswordController_RBS() {}
    
     @IsTest(SeeAllData=true) public static void testChangePasswordController_RBS() {
        // Instantiate a new controller with all parameters in the page
        ChangePasswordController_RBS controller = new ChangePasswordController_RBS();
        controller.oldPassword = '123456';
        controller.newPassword = 'qwerty1'; 
        controller.verifyNewPassword = 'qwerty1';                
        
        System.assertEquals(controller.changePassword(),null);                           
    }    
}