/*********************************************************************************
Class Name      : CPQ_Extension_Agreement
Description     : This class is related to VisualForce Page 'CPQ_WinReasonEntry'(US-0426) 
Created By      : Alina Balan
Created Date    : 10-November-2015
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Alina Balan            10-November-2015            Initial Version(US-0426) 
Joseph Toms            28-Nov-2015                 Modified for (US-0426) 
----------------------------------------------------------------------------------            
*********************************************************************************/
public with sharing class CPQ_Extension_Agreement {
public Apttus__APTS_Agreement__c oAgreement {get; set;}
public Opportunity oOpportunity;
  
public CPQ_Extension_Agreement(Apexpages.StandardController stdController) {
    this.oAgreement = (Apttus__APTS_Agreement__c)stdController.getRecord();
    oAgreement = [SELECT Id, Apttus__Status_Category__c, Apttus__Status__c, Apttus__Related_Opportunity__r.Win_Loss_Description__c, Apttus__Related_Opportunity__r.Win_Reason__c, Apttus__Related_Opportunity__r.StageName, Name 
                 FROM Apttus__APTS_Agreement__c WHERE Id =: oAgreement.Id];
    this.oOpportunity = [SELECT Id FROM Opportunity WHERE Id =: oAgreement.Apttus__Related_Opportunity__c];
 
}  
/*********************************************************************************
Method Name    : getRedirectLink()
Description    : This method get the esignature URL to redirect after saving win reason on Opportunity
Return Type    : String
Parameter      : Nil
*********************************************************************************/  
public String getRedirectLink() {
   
    String sRedirectLink ;
    Id profileId = userinfo.getProfileId();
    List<Profile> lstProfiles = [SELECT UserType  FROM Profile WHERE Id =: profileId];  
    String sProfileUserType = lstProfiles[0].UserType;
    CPQ_Settings__c cs = CPQ_Settings__c.getInstance();
    if (sProfileUserType == 'Standard') {
        sRedirectLink = cs.Apttus_eSignature_URL__c + oAgreement.Id;
    } else {
        sRedirectLink = cs.PartnerPortalDealerPrefix__c + cs.Apttus_eSignature_URL__c+ oAgreement.Id;
    } 
return sRedirectLink;
}
/*********************************************************************************
Method Name    : saveWinReason
Description    : This method save win reason before sending agreemen tfo signature 
Return Type    : void
Parameter      : Nil
*********************************************************************************/      
public void saveWinReason() {
     //update opportunity with win reasons and description
    oOpportunity.Win_Loss_Description__c = oAgreement.Apttus__Related_Opportunity__r.Win_Loss_Description__c;
    oOpportunity.Win_Reason__c = oAgreement.Apttus__Related_Opportunity__r.Win_Reason__c; 
    if(String.isBlank(oOpportunity.Win_Loss_Description__c) || String.isBlank(oOpportunity.Win_Reason__c)){
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, System.label.CPQ_Win_ReasonAndDesc_Required));
    }   
    try{
        update oOpportunity;
    }
    catch(DMLException e){
        ApexPages.addMessages(e); 
    }       
}
 /*********************************************************************************
Method Name    : backToAgreement()
Description    : This method retun back to Agreement from VF page CPQ_WinReasonEntry
Return Type    : PageReference
Parameter      : Nil
*********************************************************************************/         
public  PageReference backToAgreement(){
    return new PageReference('/' + oAgreement.Id); 
}
}