/*
    Jan 23, 2015. Paul Saini. Replace Opportunity.Lost_Comments__c & Reason_Lost__c with Opportunity.Win_Loss_Description__c.
    
    As per comments from MP: The code needs to instead reference the field [Win_Loss_Description__c]
    Feb 23, 2015 Removed Signed_Date__c as it is no longer required.
    Feb 24, 2015 Replace Opportunity.Actual_Sign_Date with Opportunity.CloseDate
*/

global with sharing class Utils {

    public static Integer FRENCH = 1;
    public static Integer ENGLISH = 0;
    
    public static String removeNull(String str){
        return (str==null?'':str);
    }
        
    public static String removeNullsAndSpaces(String str){
        str = Utils.removeNull(str);
        return str.replaceAll( '\\s+', '');
    }
    
    public static Boolean isEmpty(String str){
        return ((str==null || str.trim() == (''))?true:false); 
    }
    
    public static Boolean isEmpty(Decimal dec){
        return (dec == null?true:false); 
    }
    
    public static Boolean listContains(List<String> collection, String value){ 
        if (collection==null)
            return false;
        
        for (String current : collection){
            if (current == value)
                return true;
        }
        return false;
    }
    
    public static Boolean containsNull(List<String> collection){
        if (collection == null)
            return false;
        
        for (String current : collection){
            if (current == null)
                return true;
        }
        return false;
    }
    
    public static String removeHTML(String value){
        return value.replace('<br>','').replace('<br/>','');
    }
    
    public static String convertToHTML(String value){
        return value.replace('\n','<br/>');
    }
    
    public static String getMonth(Integer month, Integer language){
        if (language == ENGLISH){
            if (month == 1)
                return 'Jan';
            else if (month == 2)
                return 'Feb';
            else if (month == 3)
                return 'Mar';
            else if (month == 4)
                return 'Apr';
            else if (month == 5)
                return 'May';
            else if (month == 6)
                return 'Jun';
            else if (month == 7)
                return 'Jul';
            else if (month == 8)
                return 'Aug';
            else if (month == 9)
                return 'Sep';
            else if (month == 10)
                return 'Oct';
            else if (month == 11)
                return 'Nov';
            else if (month == 12)
                return 'Dec';
            else
                return '';
        }else if (language == FRENCH){
            if (month == 1)
                return 'janv.';
            else if (month == 2)
                return 'févr.';
            else if (month == 3)
                return 'mars';
            else if (month == 4)
                return 'avril';
            else if (month == 5)
                return 'mai';
            else if (month == 6)
                return 'juin';
            else if (month == 7)
                return 'juil.';
            else if (month == 8)
                return 'août';
            else if (month == 9)
                return 'sept.';
            else if (month == 10)
                return 'oct.';
            else if (month == 11)
                return 'nov.';
            else if (month == 12)
                return 'déc.';
            else
                return '';
        }
        return '';
    }
    
    public static String translateDate(String dateString, Integer fromLanguage, Integer toLanguage){
        if (fromLanguage == ENGLISH && toLanguage == FRENCH){
            return dateString.replaceAll('months', 'mois').replaceAll('month', 'mois').replaceAll('Months', 'Mois').replaceAll('Month', 'Mois');
        }
        
        return dateString;
    
    }
    
    public static String formatDate(Date dateValue, Integer language){
        if (dateValue == null) 
            return ''; 
        
        String sMonth = Utils.getMonth(dateValue.month(), language);
       
        String sDate = string.valueOf(dateValue.day());
        if (sDate.length() < 2){ 
            sDate = '0' + sDate;
        }
        
       String expDate = sMonth + ' ' + sDate + ', ' + dateValue.year() ;
       
       return expDate;
    }
    
    public static List<String> parseList(String listValue){
        List<String> resultList = new List<String>();
        
        if (Utils.isEmpty(listValue))
            return resultList; 
        
        String[] tempList = listValue.split('\n');
        
        for (String row : tempList){
            if(row.split(' ').size() == 2)
                resultList.add(row.split(' ')[1]);
        }
        
        return resultList;
    }
    
    public static Boolean validateNumericList(List<String> values, Integer maxValue){
        Boolean result = true;
        
        if (values != null && values.size() > 0){
            for (String value : values){
                try{
                    Integer temp = Integer.valueOf(value);
                    if (temp>maxValue)
                        result = false;
                }catch(Exception ex){
                    result = false;
                }
            }
        }
            
        return result;
    }
    
    public static List<String> removeSpacesFromList(List<String> values){
        if (values == null){
            return null;
        }
        
        List<String> cleanedValues = new List<String>();
        
        for (String s : values){
            cleanedValues.add(s.trim());
        }       
            
        return cleanedValues;
    }
    
    public static void sendEmail(String messageBody, String messageSubject, String[] toAddresses){
        
        if (toAddresses == null || toAddresses.size() == 0 || messageBody == null)
            return;

        Messaging.SingleEmailMessage outbound = new Messaging.SingleEmailMessage();
        outbound.toAddresses = toAddresses;
        outbound.setSubject(messageSubject);
        outbound.setHtmlBody(messageBody);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {outbound}); 
    }
    
    public static void sendEmail(EmailTemplate template, String[] toAddresses, Id targetId){
        
        if (template == null || toAddresses.size() == 0 || targetId == null)
            return;

        Messaging.SingleEmailMessage outbound = new Messaging.SingleEmailMessage();
        outbound.toAddresses = toAddresses;
        outbound.setTemplateId(template.Id);
        //outbound.setWhatId(targetId);
        outbound.setTargetObjectId('00530000004s0Ez');
        outbound.setSaveAsActivity(false);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {outbound}); 
    }
    
    public static String generate18CharId(String id){
        
    // This method will take a 15 character ID and return its 18 character ID:
        
     if (id == null){ 
          return null;
     }
     if (id.length() != 15) {
          return null;
     }
     string suffix = '';
     integer flags;
        
     for (integer i = 0; i < 3; i++) {
          flags = 0;
          for (integer j = 0; j < 5; j++) {
               string c = id.substring(i * 5 + j,i * 5 + j + 1);
               //Only add to flags if c is an uppercase letter:
               if (c.toUpperCase().equals(c) && c >= 'A' && c <= 'Z') {
                    flags = flags + (1 << j);
               }
          }
          
          if (flags <= 25) {
               suffix = suffix + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.substring(flags,flags+1); 
          }else if(flags > 25 && flags<=30 && !Test.isRunningTest()){
               suffix = suffix + '012345'.substring(flags-25,flags-24);
          }
     }
     return id + suffix;
    }
    
    public static String display(Decimal str){
        if (str != null)
            return str + '';
        else 
            return '';
    }
    
    public static String makeMultiPicklist(List<String> values){
        String result = '';
        
        if (values == null || values.isEmpty())
            return result;
        
        if (values.size() == 1)
            return values.get(0);
        else{
            for (String v : values){
                result += v + ';';
            }
        }
        
        return result.substring(0, result.length()-1); 
        
    }
    
    public static String parseList(List<String> values, String deliminator){
        String result = '';
        
        if (values == null || values.isEmpty())
            return result;
        
        if (values.size() == 1)
            return values.get(0);
        else{
            for (String v : values){
                result += v + deliminator + ' ';
            }
        }
                
        return result.substring(0, result.length() - 2);
        
    }
    
    public static boolean cloneSourceSites(Id sourceOppId, Id destinationOppId){
        // check currently site on the newOpp (the clone) which were created during opp insertion:
        //List<Site__c> existingSiteList = [select Id, Name, UniqueKey__c
        //  from Site__c where Opportunity__c = : newOpp.Id ];
        if (sourceOppId == null || destinationOppId == null){
            return false;
        }
        
        List<Site__c> sourceSites; // Sites of sourceOpp
            
        Opportunity sourceOpp = new Opportunity();  
        try {
            sourceOpp = [SELECT Id, Name
                , AccountId, Account.Total_MRR__c
                , Account_Record_Type__c
                , CloseDate
                , Amount
                , Assigned_Campaign__c
                , Assigned_Telemarketing__c
                , Build_Cost__c
                , CampaignId
                , Channel_Sales_Rep__c                
                , Close_Month__c
                , Contact__c
                , Contract_Number__c
                , COR_Comments__c
                , COR__c
                //, CreatedById
                //, CreatedDate
            //  , Customer_Facing_COP__c
                , Customer_Order_Reference__c
                , Description
                , Description_Multiple_networks__c
                , EncryptedCOPId__c
                , Estimated_MRC1__c
                , Estimated_MRR__c
                , Estimated_One_Time_Charge1__c
                , Estimated_One_Time_Charge__c
                , Estimated_Total_Contract_Value_TCV1__c
                , Estimated_Total_Contract_Value_TCV__c
                , Existing_MRR__c
                , ExpectedRevenue
                , Expected_Billing_Date__c
                , First_Bill_Date__c
                , Fiscal
                , FiscalQuarter
                , FiscalYear
                , ForecastCategory
                , ForecastCategoryName
                , HasOpportunityLineItem
                //, IsClosed
                //, IsDeleted
                , IsPrivate
                //, IsWon
                //, LastActivityDate
                //, LastModifiedById
                //, LastModifiedDate
                , LeadSource
                , Win_Loss_Description__c
                , Network__c
                , Net_Change_MRC__c
                , Net_Change_MRR__c
                , New_MRR__c
                , New_Term_Months1__c
                , New_Term_Months__c
                , NextStep
                , No_of_Sites__c
                , Number_of_Circuits__c
                , One_Time_Charge1__c
                , One_Time_Charge__c
                , Opportunity_Number__c
                , Opp_Age__c
                , Order_Submission_Date__c
                , Order_Tracker__c
                , OwnerId
                , PartnerAccountId
                , Pre_Approved_Pricing__c
                , Pricebook2Id
                , Pricing__c
                , Probability
                , Product_Category__c
                , Product__c
                , Proposal__c
                , Provider__c
                , Qualify_by__c
                //, Win_Loss_Description__c
                , RecordTypeId
                , Renewal_Notification_Period__c
                , Renewal_TCV__c
                , Renewal_Type__c
                , Service_Requested_Date__c
                //, Signed_Date__c
                , Site_A_City__c
                , Site_A_Postal_Zip_Code__c
                , Site_A_Street_Address_2__c
                , Site_A_Street_Address__c
                , Site_Z_City__c
                , Site_Z_Postal_Zip_Code__c
                , Site_Z_Street_Address_2__c
                , Site_Z_Street_Address__c
                , StageName
    //          , SyncedQuoteId
                , SystemModstamp
                , Term_Months1__c
                , Term_Months__c
                , TotalOpportunityQuantity
                , Type
                , Upsell_Delta_MRC1__c
                , Upsell_Delta_MRR__c
                , Won_MRC1__c
                , Won_MRR__c
                , Won_Total_Contract_Value_TCV1__c
                , Won_Total_Contract_Value_TCV__c 
                , ( SELECT Access_Class__c
                        , Access_Provider__c, Access_Type_Group__c, Access_Type__c
                        , Account__c
                        , Approved__c
                        , City__c
                        , CLLI_SWC__c
                        , Country__c
                        , CreatedById, CreatedDate
                        , Display_Name__c
                        , Id
                        //, IsDeleted
                        , Is_a_Z_Site__c
                        , LastModifiedById, LastModifiedDate
                        , LocationKey_del__c
                        , Name
                        //, Notes__c
                        , Opportunity__c
                        //, OwnerId
                        , Postal_Code__c
                        , Proposal_Display_Name__c
                        , Province_Code__c
                        , ServiceableLocation__c
                        , Serviceable__c
                        , Services__c
                        , Service_Address__c
                        , Street_Direction__c
                        , Street_Name__c, Street_Number__c, Street_Type__c
                        , Suite_Floor__c
                        , SystemModstamp
                        , Type__c
                        //, UniqueKey__c // should be calculated on insert
                        , Z_Site__c // this will not be used again
                        , Suite_N_A__c
                        
                    FROM Sites__r )
                FROM Opportunity WHERE Id = : sourceOppId];
        } catch (Exception ex) {
            return false;
        }
        
        Map<Id, Site__c> existingIdToSiteMap = new Map<Id, Site__c>([
            select Id, Name, UniqueKey__c
            from Site__c where Opportunity__c = : destinationOppId ]);
        
        //Set<String> existingUniqueKeys = new Set<String>();
        Set<String> existingSiteNames = new Set<String>();
        
        Map<String, Site__c> newSiteNameToObjectMap = new Map<String, Site__c>();
        for ( Site__c savedSite : existingIdToSiteMap.values() )
        {
            //existingUniqueKeys.add(savedSite.UniqueKey__c);
            existingSiteNames.add(savedSite.Name);
            newSiteNameToObjectMap.put(savedSite.Name, savedSite);
        }
        
        // sourceSiteIdToObjectMap will be used in 'cloning' the A_Z_Site records
        Map<Id, Site__c> sourceSiteIdToObjectMap = new Map<Id, Site__c>();
        
        sourceSites = sourceOpp.Sites__r;
        List<Site__c> newSites = new List<Site__c> ();
        
        // clone all non existing sites, A or Z will be put on the same list
        for (Site__c srcSite : sourceSites)
        {
            sourceSiteIdToObjectMap.put(srcSite.Id, srcSite);
            /*
            String thisUniqueKey = srcSite.Suite_Floor__c 
                + ' ' + srcSite.Street_Number__c + ' ' + srcSite.Street_Name__c 
                + ' ' + srcSite.Street_Type__c + ' ' + srcSite.Street_Direction__c 
                + ' ' + srcSIte.City__c + ' ' + srcSite.Province_Code__c 
                + ' ' + srcSite.Postal_Code__c + ' ' + srcSite.opportunity__c;
            // f (! existingUniqueKeys.contains(thisUniqueKey))
            */
            
            if (! existingSiteNames.contains(srcSite.Name))
            {   
                Site__c newSite = srcSite.clone(false, false);
                newSite.Opportunity__c = destinationOppId;
                newSiteNameToObjectMap.put(newSite.Name, newSite);
                
                newSites.add(newSite);
            }
        }
        
        insert newSites;
        
        Set<Id> siteIds = new Set<Id>(); // existing & new Site__c.Id
        siteIds.addAll(existingIdToSiteMap.keySet());
        for (Site__c newSite : newSites) {
            siteIds.add(newSite.Id);
        }
        
        // get the A_Z Site Mapping
        List<A_Z_Site__c> existingAZSiteList = [select Id, Site_A__c, Site_Z__c
            from A_Z_Site__c 
            where Site_A__c in : siteIds 
                and IsDeleted = false
            ];
        Set<String> existingAZIdPairSet = new Set<String>();
        for (A_Z_Site__c existingAZ : existingAZSiteList){
            String existingPair = existingAZ.Site_A__c;
            existingPair += existingAZ.Site_Z__c;
            existingAZIdPairSet.add(existingPair);
        }
            
        List<A_Z_Site__c> sourceAZSiteList = [select Id, Site_A__c, Site_Z__c
            from A_Z_Site__c 
            where Site_A__c in : sourceSiteIdToObjectMap.keySet() 
                and IsDeleted = false
            ];
        
        
        List<A_Z_Site__c> newAZSiteList = new List<A_Z_Site__c>(); // new map of Site_A and Site_Z
    
        // create Z SIte
        for ( A_Z_Site__c azSite : sourceAZSiteList) {
            Id newASiteId = newSiteNameToObjectMap.get(sourceSiteIdToObjectMap.get(azSite.Site_A__c).Name).Id;
            //System.assertNotEquals( null, newASiteId);
            Id newZSiteId = newSiteNameToObjectMap.get(sourceSiteIdToObjectMap.get(azSite.Site_Z__c).Name).Id;
            
                            
            String newPair = newASiteId;
            newPair += newZSiteId;
            if (!existingAZIdPairSet.contains(newPair))
            { 
                A_Z_Site__c newAZSite = new A_Z_Site__c ( Site_A__c = newASiteId
                                                    , Site_Z__c = newZSiteId );
                newAZSiteList.add(newAZSite);
            }
        }
        insert newAZSiteList;
        
        return true;
    }
    
    public static String get15CharId(String chat18Id){
        return chat18Id.substring(0,15);
    }
    
    public static String cleanSpaces(String val){
        if (val == null)
            return null;
        
        return val.replaceAll('\\s+', ' ');
    }
    
    webservice static String getPicklistValues(String ObjectApi_name,String Field_name){ 
        List<String> lstPickvals=new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); 
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); 
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); 
        List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); 
        for (Schema.PicklistEntry a : pick_list_values) { 
            lstPickvals.add(a.getValue());
        }

        return Utils.makeMultiPicklist(lstPickvals);
    }
    
    /* Originally from */
    /* http://salesforcesource.blogspot.ca/2010/01/utilizing-apex-pattern-and-matcher.html */
    public static Boolean isValidEmail(String str){
        String emailRegex = '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
        Pattern MyPattern = Pattern.compile(emailRegex);

        // Then instantiate a new Matcher object "MyMatcher"
        Matcher MyMatcher = MyPattern.matcher(str);

        return MyMatcher.matches();
    }
   
}