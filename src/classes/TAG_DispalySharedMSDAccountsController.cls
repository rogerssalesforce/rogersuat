/*
===============================================================================
 Class Name   : TAG_DispalySharedMSDAccountsController
===============================================================================
PURPOSE:    This class a controller class for TAG_DispalySharedMSDAccounts page.
            It displays Accounts for the Shared MSD whose ID is in the URL.
  
Developer: Deepika Rawat
Date: 01/28/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
01/28/2015           Deepika Rawat               Original Version
===============================================================================
*/
public class TAG_DispalySharedMSDAccountsController {
    public List<Account> lstAllAccount{get;set;}
    public List<Shared_MSD_Accounts__c> lstSharedMSDAcc{get;set;}
    public Shared_MSD_Code__c sharedMSDCode{get;set;} 
    public String sMSDID;
    public String noOfChilds{get;set;}
    //This is used to set limit on the number of child Accounts displayed on the Account Hierarchy pop up
    static final Integer Accountlimit=10;
    
     /*******************************************************************************************************
     * @description: This method is controller method. It reads the smsdid parameter from URL and fetches its 
                     it's related accounts
     * @param: none
     * @return type: none.
     ********************************************************************************************************/
    public TAG_DispalySharedMSDAccountsController (){
        lstAllAccount = new List<Account>();
        lstSharedMSDAcc = new List<Shared_MSD_Accounts__c>();
        sharedMSDCode = new Shared_MSD_Code__c();
        String sMSDID= ApexPages.currentPage().getParameters().get('smsdid');
        if(sMSDID!=null && sMSDID!=''){
            lstSharedMSDAcc= [select Id, Account__c, Account__r.Name, Account__r.id,Account__r.Internal_DUNS__c , Shared_MSD_Code__r.Id from Shared_MSD_Accounts__c where Shared_MSD_Code__r.Id =:sMSDID and Account__c!=null ORDER BY Account__r.Internal_DUNS__c limit :Accountlimit];
            sharedMSDCode= [select Name from Shared_MSD_Code__c where ID =:sMSDID];
        }
        if(lstSharedMSDAcc!=null && lstSharedMSDAcc.size()>0){
            if(lstSharedMSDAcc.size() >10){
                noOfChilds = String.valueof(lstSharedMSDAcc.size()) + '+ ';
            }
            else
                 noOfChilds = String.valueof(lstSharedMSDAcc.size());
        }
        else{
            noOfChilds ='0';
        }
    }
}