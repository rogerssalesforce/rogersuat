/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.  
 */
@isTest
public with sharing class TestCloneQuoteExtension {
    static final Integer ACCESS_INSTALL = 1;
    static final Integer SIP_TRUNKING = 2;
    static final Integer MIN_OVERAGE = 3;
    static final Integer MIN_MAX = 4;
    static final Integer BUNDLE = 5;
    static final Integer MIN_MAX_INSTALL = 6;
    
    static Id oppId, sl1Id, sl2Id, site1Id, site2Id, site3Id; 
    static Id priceEntry1id, priceEntry2_1id, priceEntry2id, quote1Id, pMinId, pMaxId;
    static Opportunity o;
    static Quote q;
    static Product2 p, pInstall, carrierTestProduct, pSIP, pOverage, pMin, pMax;
    static Pricebook2 sp1;
    static Map <string,Id> mapRTa, mapRTo, mapRTp;
    
    
    static {
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        
        List <RecordType > lRtp = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Product2']);
        mapRTp = new Map <string,id> ();
        
        for (RecordType rtp : lRtp){
            mapRTp.put(rtp.Name,rtp.id);  
        }
        
        
        sp1 = new Pricebook2();
        sp1 = [select id from Pricebook2 where isStandard = true];
              
        ServiceableLocation__c sl = new ServiceableLocation__c();
        sl.Street_Name__c = 'Somewhere';
        sl.Street_Number__c  = '5';
        sl.Street_Type__c  = 'Ave';
        sl.City__c = 'Coty';
        sl.Postal_Code__c = 'A1A1A1';
        sl.Province_Code__c = 'ON';
        sl.CLLI_Code__c = '123113';
        sl.Access_Type_Group__c = 'NNI';
        sl.Access_Type__c = 'Ethernet Eon';
        insert sl;
        sl1Id = sl.Id;
        
        ServiceableLocation__c sl2 = new ServiceableLocation__c();
        sl2.Street_Name__c = 'Somewhere';
        sl2.Street_Number__c  = '5';
        sl2.Street_Type__c  = 'Ave';
        sl2.City__c = 'Coty';
        sl2.Postal_Code__c = 'A1A1A1';
        sl2.Province_Code__c = 'ON';
        sl2.CLLI_Code__c = '123113';
        sl2.Access_Type_Group__c = 'Fibre';
        sl2.Access_Type__c = 'ETHERNET EON;ETHERNET CAP';
        insert sl2;
        sl2Id = sl2.Id;
        
    }
    
    @isTest static void testCloneQuote1() {
        addEnterpriseInformation(ACCESS_INSTALL);
        
        PageReference pageRef = New PageReference('/apex/CloneQuote?oppId=' + String.valueOf(oppId) + '&quoteId=' + quote1Id + '&quoteName=' + q.Name); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(q);
        CloneQuoteExtension controller = new CloneQuoteExtension(sc);
        
        Test.startTest();
        controller.actualterm = '13';
        controller.quoteName = 'Quote Test 1';
        controller.cloneQuote();
        Quote testQuote = [SELECT Id, Name, TotalPrice FROM Quote WHERE Id = :controller.clonedQuote.id];
        List<QuoteLineItem> lineItems = [SELECT Id, TotalPrice FROM QuoteLineItem WHERE quoteId = :testQuote.id];
       lineItems = testQuote.quoteLineItems;
        controller.cloneQuote();
        
        //System.assertEquals(testQuote.TotalPrice, 110);
       // System.assertEquals(lineItems.size(), 2);
        Test.stopTest();
    }
    
    @isTest static void testCloneQuoteBasic() {
        addEnterpriseInformation(ACCESS_INSTALL);
        
        PageReference pageRef = New PageReference('/apex/CloneQuote?oppId=' + String.valueOf(oppId) + '&quoteId=' + quote1Id + '&quoteName=' + q.Name); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(q);
        CloneQuoteExtension controller = new CloneQuoteExtension(sc);
        
        Test.startTest();
        controller.returnToQuote();
        String test1 = controller.ExpirationDateAsString;
        String test2 = controller.currentDateAsString;
        Test.stopTest();
    }
    
    @isTest static void testCloneQuoteBasic2() {
        addEnterpriseInformation(ACCESS_INSTALL);
        Account a = new Account();
        a.name = 'TespQuote Wizard123967857555';
        a.Business_Segment__c = 'Carrier';
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street Wizard';
         a.BillingCity = 'Ontario';
        a.BillingCountry = 'CA';
        a.BillingPostalCode = 'A9A 9A9';
        a.BillingState = 'ON';
        a.Account_Status__c= 'Assigned';
        a.ParentId=null;
        insert a;       
        
        o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'TeRogerTesting';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.Unified_Comm_Collaboration_Estimated__c =20;
        o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.today();
        insert o;
        oppId = o.id;
        
        PageReference pageRef = New PageReference('/apex/CloneQuote?oppId=' + String.valueOf(oppId) + '&quoteId=' + quote1Id); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(q);
        CloneQuoteExtension controller = new CloneQuoteExtension(sc);
        
        Test.startTest();
        controller.pricebookId = null;
        controller.q = null;
        controller.actualterm = '13';
        controller.quoteName = 'Quote Test 1';
        controller.cloneQuote();
        controller.returnToQuote();
        controller.getPriceBookId();
        controller.setPricingTerm();
        String test1 = controller.ExpirationDateAsString;
        String test2 = controller.currentDateAsString;
        Test.stopTest();
    }
    
    private static void addEnterpriseInformation(Integer testCase){
        Account a = new Account();
        a.name = 'TeTestQuote Wizard345';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street Wizard';
        a.BillingCity = 'Ontario';
        a.BillingCountry = 'CA';
        a.BillingPostalCode = 'A9A 9A9';
        a.BillingState = 'ON';
        a.Account_Status__c= 'Assigned';
        a.ParentId=null;
        insert a;       
        
        o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'TesRogtOpp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.Unified_Comm_Collaboration_Estimated__c =20;
        o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.today();
        insert o;
        oppId = o.id;
        
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Some1where4';
        s1.Suite_Floor__c = '11a';
        s1.Street_Number__c  = '7';
        s1.City__c = 'Coty';
        s1.Postal_Code__c = 'A3A1A1';
        s1.Province_Code__c = 'ON';
        s1.CLLI_SWC__c = '123113';
        s1.ServiceableLocation__c = sl1Id;
        s1.Opportunity__c = oppId;
        s1.Is_a_Z_Site__c = true; 
        insert s1;
        site1Id = s1.Id;
        
        Site__c s2 = new Site__c();
        s2.Street_Name__c = 'Some1where5';
        s2.Suite_Floor__c = '11a';
        s2.Street_Number__c  = '8';
        s2.City__c = 'Coty';
        s2.Postal_Code__c = 'A3A1A1';
        s2.Province_Code__c = 'ON';
        s2.CLLI_SWC__c = '123113';
        s2.ServiceableLocation__c = sl2Id;
        s2.Opportunity__c = oppId; 
        insert s2;
        site2Id = s2.Id;
        
        Site__c s3 = new Site__c();
        s3.Street_Name__c = 'Some1where6';
        s3.Suite_Floor__c = '11a';
        s3.Street_Number__c  = '9';
        s3.City__c = 'Coty';
        s3.Postal_Code__c = 'A3A1A1';
        s3.Province_Code__c = 'ON';
        s3.CLLI_SWC__c = '123113';
        s3.ServiceableLocation__c = sl2Id;
        s3.Opportunity__c = oppId; 
        s3.Z_Site__c = s1.Id;
        insert s3;
        site3Id = s3.Id;
        
      
        
        Pricebook2 sp = new Pricebook2();
        sp = [select id from Pricebook2 where Name = :'Enterprise PriceBook'];
        
        q = new Quote(Name='q1', actualTerm__c='12', Term__c='12-Months', OpportunityId=oppId, Pricebook2Id = sp.id);
        insert q;
        quote1Id = q.Id;
        
        Quote_Site__c qs = new Quote_Site__c();
        qs.Site__c = site1Id;
        qs.Quote__c = quote1Id;
        insert qs;
        
        qs = new Quote_Site__c();
        qs.Site__c = site2Id;
        qs.Quote__c = quote1Id;
        insert qs;
        
        qs = new Quote_Site__c();
        qs.Site__c = site3Id;
        qs.Quote__c = quote1Id;
        insert qs;
        
        if (testCase == ACCESS_INSTALL){
            // Install Product
            pInstall  = new Product2();
            pInstall.IsActive = true;
            pInstall.Name = 'Fibre';
            pInstall.Charge_Type__c = 'NRC';
            pInstall.Access_Type__c = 'Ethernet EON';
            pInstall.Access_Type_Group__c = 'Fibre';
            pInstall.Category__c = 'Install';
            pInstall.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
            pInstall.RecordTypeId = mapRTp.get('Enterprise Products');
            pInstall.Service_Term__c = '12-Months';
            pInstall.Mark_Up_Factor__c = 1.0;
            pInstall.Start_Date__c = Date.today();
            pInstall.End_Date__c = Date.today()+1;
            insert pInstall;
            
            //Product 1
            p  = new Product2();
            p.IsActive = true;
            p.Name = 'Fibre';
            p.Charge_Type__c = 'NRC';
            p.Access_Type__c = 'Ethernet EON';
            p.Access_Type_Group__c = 'Fibre';
            p.Category__c = 'Access';
            p.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
            p.RecordTypeId = mapRTp.get('Enterprise Products');
            p.Service_Term__c = '12-Months';
            p.Product_Install_Link__c = pInstall.Id;
            p.Mark_Up_Factor__c = 1.0;
            p.Start_Date__c = Date.today();
            p.End_Date__c = Date.today()+1;
            insert p;
            
            PricebookEntry peStandard = new PricebookEntry();
            peStandard.Pricebook2Id = sp1.id;
            peStandard.UnitPrice = 60;
            peStandard.Product2Id = p.id;
            peStandard.IsActive = true;
            peStandard.UseStandardPrice = false;
            
            insert peStandard;
            
            PricebookEntry pe1 = new PricebookEntry();
            pe1.Pricebook2Id = sp.id;
            pe1.UnitPrice = 60;
            pe1.Product2Id = p.id;
            pe1.IsActive = true;
            pe1.UseStandardPrice = false;
            
            insert pe1;
            priceEntry1id = pe1.Id;
            
            peStandard = new PricebookEntry();
            peStandard.Pricebook2Id = sp1.id;
            peStandard.UnitPrice = 60;
            peStandard.Product2Id = pInstall.id;
            peStandard.IsActive = true;
            peStandard.UseStandardPrice = false;
            
            insert peStandard;
            
            pe1 = new PricebookEntry();
            pe1.Pricebook2Id = sp.id;
            pe1.UnitPrice = 60;
            pe1.Product2Id = pInstall.id;
            pe1.IsActive = true;
            pe1.UseStandardPrice = false;
            
            insert pe1;
            
            //Product 2
            Product2 p1  = new Product2();
            p1.IsActive = true;
            p1.Name = 'ProductMe1';
            p1.Charge_Type__c = 'MRC';
            p1.Mark_Up_Factor__c = 1.0;
            p1.Start_Date__c = Date.today();
            p1.End_Date__c = Date.today()+1;
            insert p1;
            
            PricebookEntry pe21Standard = new PricebookEntry();
            pe21Standard.Pricebook2Id = sp1.id;
            pe21Standard.UnitPrice = 60;
            pe21Standard.Product2Id = p1.id;
            pe21Standard.IsActive = true;
            pe21Standard.UseStandardPrice = false;
            
            insert pe21Standard;
            
            PricebookEntry pe21 = new PricebookEntry();
            pe21.Pricebook2Id = sp.id;
            pe21.UnitPrice = 60;
            pe21.Product2Id = p1.id;
            pe21.IsActive = true;
            pe21.UseStandardPrice = false;
            
            insert pe21;
            priceEntry2_1id = pe21.Id;
        }else if (testCase == SIP_TRUNKING){
            // SIP Product Sheet
            pSIP  = new Product2();
            pSIP.IsActive = true;
            pSIP.Name = 'Fibre';
            pSIP.Charge_Type__c = 'NRC';
            pSIP.Access_Type__c = 'Ethernet EON';
            pSIP.Access_Type_Group__c = 'Fibre';
            pSIP.Category__c = 'Access';
            pSIP.Service_Type__c = 'SIP Trunking - Test';
            pSIP.RecordTypeId = mapRTp.get('Enterprise Products');
            pSIP.Service_Term__c = '12-Months';
            pSIP.Mark_Up_Factor__c = 1.0;
            pSIP.Start_Date__c = Date.today();
            pSIP.Product_Sheet__c = 'SIP Trunking';
            pSIP.End_Date__c = Date.today()+1;
            insert pSIP;
                   
            PricebookEntry peStandard = new PricebookEntry();
            peStandard.Pricebook2Id = sp1.id;
            peStandard.UnitPrice = 60;
            peStandard.Product2Id = pSIP.id;
            peStandard.IsActive = true;
            peStandard.UseStandardPrice = false;
            
            insert peStandard;
            
            PricebookEntry pe1 = new PricebookEntry();
            pe1.Pricebook2Id = sp.id;
            pe1.UnitPrice = 60;
            pe1.Product2Id = pSIP.id;
            pe1.IsActive = true;
            pe1.UseStandardPrice = false;
            
            insert pe1;
            
        }else if (testCase == MIN_OVERAGE){
            // Overage Product
            pOverage  = new Product2();
            pOverage.IsActive = true;
            pOverage.Name = 'Fibre';
            pOverage.Charge_Type__c = 'OC';
            pOverage.Access_Type__c = 'Ethernet EON';
            pOverage.Access_Type_Group__c = 'Fibre';
            pOverage.Category__c = 'Overage';
            pOverage.Service_Type__c = '10 Mbps - Ethernet Fibre Network Overage - Test';
            pOverage.RecordTypeId = mapRTp.get('Enterprise Products');
            pOverage.Service_Term__c = '12-Months';
            pOverage.Mark_Up_Factor__c = 1.0;
            pOverage.Start_Date__c = Date.today();
            pOverage.End_Date__c = Date.today()+1;
            insert pOverage;
            
            //Product 1
            p  = new Product2();
            p.IsActive = true;
            p.Name = 'Fibre';
            p.Charge_Type__c = 'NRC';
            p.Access_Type__c = 'Ethernet EON';
            p.Access_Type_Group__c = 'Fibre';
            p.Category__c = 'Access';
            p.Service_Type__c = '10 Mbps - Ethernet Fibre Network Min - Test';
            p.RecordTypeId = mapRTp.get('Enterprise Products');
            p.Service_Term__c = '12-Months';
            p.Product_Overage_Link__c = pOverage.Id;
            p.Mark_Up_Factor__c = 1.0;
            p.Start_Date__c = Date.today();
            p.End_Date__c = Date.today()+1;
            insert p;
            
            PricebookEntry peStandard = new PricebookEntry();
            peStandard.Pricebook2Id = sp1.id;
            peStandard.UnitPrice = 60;
            peStandard.Product2Id = p.id;
            peStandard.IsActive = true;
            peStandard.UseStandardPrice = false;
            
            insert peStandard;
            
            PricebookEntry pe1 = new PricebookEntry();
            pe1.Pricebook2Id = sp.id;
            pe1.UnitPrice = 60;
            pe1.Product2Id = p.id;
            pe1.IsActive = true;
            pe1.UseStandardPrice = false;
            
            insert pe1;
            priceEntry1id = pe1.Id;
            
            peStandard = new PricebookEntry();
            peStandard.Pricebook2Id = sp1.id;
            peStandard.UnitPrice = 60;
            peStandard.Product2Id = pOverage.id;
            peStandard.IsActive = true;
            peStandard.UseStandardPrice = false;
            
            insert peStandard;
            
            pe1 = new PricebookEntry();
            pe1.Pricebook2Id = sp.id;
            pe1.UnitPrice = 60;
            pe1.Product2Id = pOverage.id;
            pe1.IsActive = true;
            pe1.UseStandardPrice = false;
            
            insert pe1;
            
            //Product 2
            Product2 p1  = new Product2();
            p1.IsActive = true;
            p1.Name = 'ProductMe1';
            p1.Charge_Type__c = 'MRC';
            p1.Mark_Up_Factor__c = 1.0;
            p1.Start_Date__c = Date.today();
            p1.End_Date__c = Date.today()+1;
            insert p1;
            
            PricebookEntry pe21Standard = new PricebookEntry();
            pe21Standard.Pricebook2Id = sp1.id;
            pe21Standard.UnitPrice = 60;
            pe21Standard.Product2Id = p1.id;
            pe21Standard.IsActive = true;
            pe21Standard.UseStandardPrice = false;
            
            insert pe21Standard;
            
            PricebookEntry pe21 = new PricebookEntry();
            pe21.Pricebook2Id = sp.id;
            pe21.UnitPrice = 60;
            pe21.Product2Id = p1.id;
            pe21.IsActive = true;
            pe21.UseStandardPrice = false;
            
            insert pe21;
            priceEntry2_1id = pe21.Id;
            
        }else if (testCase == MIN_MAX){
            // Min Product Product
            pMin  = new Product2();
            pMin.IsActive = true;
            pMin.Name = 'Fibre';
            pMin.Charge_Type__c = 'NRC';
            pMin.Access_Type__c = 'Ethernet EON';
            pMin.Access_Type_Group__c = 'Fibre';
            pMin.Category__c = 'Overage';
            pMin.Service_Type__c = '10 Mbps - Ethernet Fibre Network Min - Test';
            pMin.RecordTypeId = mapRTp.get('Enterprise Products');
            pMin.Service_Term__c = '12-Months';
            pMin.Mark_Up_Factor__c = 1.0;
            pMin.Start_Date__c = Date.today();
            pMin.End_Date__c = Date.today()+1;
            pMin.isMinSpeed__c = true;
            insert pMin;
            pMinId = pMin.Id;
            
            //Product 1
            p  = new Product2();
            p.IsActive = true;
            p.Name = 'Fibre';
            p.Charge_Type__c = 'PMO';
            p.Access_Type__c = 'Ethernet EON';
            p.Access_Type_Group__c = 'Fibre';
            p.Category__c = 'Access';
            p.Service_Type__c = '10 Mbps - Ethernet Fibre Network Max - Test';
            p.RecordTypeId = mapRTp.get('Enterprise Products');
            p.Service_Term__c = '12-Months';
            p.Mark_Up_Factor__c = 1.0;
            p.Start_Date__c = Date.today();
            p.End_Date__c = Date.today()+1;
            p.isMaxSpeed__c = true;
            p.Parent__c = pMin.Id;
            insert p;
            pMaxId = p.Id;
            
            PricebookEntry peStandard = new PricebookEntry();
            peStandard.Pricebook2Id = sp1.id;
            peStandard.UnitPrice = 60;
            peStandard.Product2Id = p.id;
            peStandard.IsActive = true;
            peStandard.UseStandardPrice = false;
            
            insert peStandard;
            
            PricebookEntry pe1 = new PricebookEntry();
            pe1.Pricebook2Id = sp.id;
            pe1.UnitPrice = 60;
            pe1.Product2Id = p.id;
            pe1.IsActive = true;
            pe1.UseStandardPrice = false;
            
            insert pe1;
            priceEntry1id = pe1.Id;
            
            peStandard = new PricebookEntry();
            peStandard.Pricebook2Id = sp1.id;
            peStandard.UnitPrice = 60;
            peStandard.Product2Id = pMin.id;
            peStandard.IsActive = true;
            peStandard.UseStandardPrice = false;
            
            insert peStandard;
            
            pe1 = new PricebookEntry();
            pe1.Pricebook2Id = sp.id;
            pe1.UnitPrice = 60;
            pe1.Product2Id = pMin.id;
            pe1.IsActive = true;
            pe1.UseStandardPrice = false;
            
            insert pe1;
        }else if (testCase == BUNDLE){
            // Install Product
            Product2 pBundleParent  = new Product2();
            pBundleParent.IsActive = true;
            pBundleParent.Name = 'Fibre';
            pBundleParent.Charge_Type__c = 'MRC';
            pBundleParent.Access_Type__c = 'Ethernet EON';
            pBundleParent.Access_Type_Group__c = 'Fibre';
            pBundleParent.Category__c = 'Promo';
            pBundleParent.Service_Type__c = '10 Mbps - Ethernet Fibre Network Promo Parent - Test';
            pBundleParent.RecordTypeId = mapRTp.get('Enterprise Products');
            pBundleParent.Service_Term__c = '12-Months';
            pBundleParent.Mark_Up_Factor__c = 1.0;
            pBundleParent.Start_Date__c = Date.today();
            pBundleParent.End_Date__c = Date.today()+1;
            pBundleParent.Is_a_Bundle__c = true;
            pBundleParent.Product_Sheet__c = 'SIP and RDI Bundle';
            
            insert pBundleParent;
            
            // Install Product
            pInstall  = new Product2();
            pInstall.IsActive = true;
            pInstall.Name = 'Fibre';
            pInstall.Charge_Type__c = 'NRC';
            pInstall.Access_Type__c = 'Ethernet EON';
            pInstall.Access_Type_Group__c = 'Fibre';
            pInstall.Category__c = 'Promo';
            pInstall.Service_Type__c = '10 Mbps - Ethernet Fibre Network Install Bundle - Test';
            pInstall.RecordTypeId = mapRTp.get('Enterprise Products');
            pInstall.Service_Term__c = '12-Months';
            pInstall.Mark_Up_Factor__c = 1.0;
            pInstall.Start_Date__c = Date.today();
            pInstall.End_Date__c = Date.today()+1;
            pInstall.Is_a_Bundle__c = true;
            pInstall.Is_Promo_Install__c = true;
            pInstall.Parent_Promo__c = pBundleParent.Id;
            insert pInstall;
            
            //Product 1
            p  = new Product2();
            p.IsActive = true;
            p.Name = 'Fibre';
            p.Charge_Type__c = 'PMO';
            p.Access_Type__c = 'Ethernet EON';
            p.Access_Type_Group__c = 'Fibre';
            p.Category__c = 'Promo';
            p.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access Bundle - Test';
            p.RecordTypeId = mapRTp.get('Enterprise Products');
            p.Service_Term__c = '12-Months';
            p.Mark_Up_Factor__c = 1.0;
            p.Start_Date__c = Date.today();
            p.End_Date__c = Date.today()+1;
            p.Is_a_Bundle__c = true;
            p.Parent_Promo__c = pBundleParent.Id;
            insert p;
            
            
            PricebookEntry peStandard = new PricebookEntry();
            peStandard.Pricebook2Id = sp1.id;
            peStandard.UnitPrice = 60;
            peStandard.Product2Id = p.id;
            peStandard.IsActive = true;
            peStandard.UseStandardPrice = false;
            
            insert peStandard;
            
            PricebookEntry pe1 = new PricebookEntry();
            pe1.Pricebook2Id = sp.id;
            pe1.UnitPrice = 60;
            pe1.Product2Id = p.id;
            pe1.IsActive = true;
            pe1.UseStandardPrice = false;
            
            insert pe1;
            priceEntry1id = pe1.Id;
            
            peStandard = new PricebookEntry();
            peStandard.Pricebook2Id = sp1.id;
            peStandard.UnitPrice = 60;
            peStandard.Product2Id = pBundleParent.id;
            peStandard.IsActive = true;
            peStandard.UseStandardPrice = false;
            
            insert peStandard;
            
            pe1 = new PricebookEntry();
            pe1.Pricebook2Id = sp.id;
            pe1.UnitPrice = 60;
            pe1.Product2Id = pBundleParent.id;
            pe1.IsActive = true;
            pe1.UseStandardPrice = false;
            
            insert pe1;
            
            peStandard = new PricebookEntry();
            peStandard.Pricebook2Id = sp1.id;
            peStandard.UnitPrice = 60;
            peStandard.Product2Id = pInstall.id;
            peStandard.IsActive = true;
            peStandard.UseStandardPrice = false;
            
            insert peStandard;
            
            pe1 = new PricebookEntry();
            pe1.Pricebook2Id = sp.id;
            pe1.UnitPrice = 60;
            pe1.Product2Id = pInstall.id;
            pe1.IsActive = true;
            pe1.UseStandardPrice = false;
            
            insert pe1;
        } else if (testCase == MIN_MAX_INSTALL) { // Install Product on a min item
             // Install Product
            pInstall  = new Product2();
            pInstall.IsActive = true;
            pInstall.Name = 'Fibre';
            pInstall.Charge_Type__c = 'NRC';
            pInstall.Access_Type__c = 'Ethernet EON';
            pInstall.Access_Type_Group__c = 'Fibre';
            pInstall.Category__c = 'Install';
            pInstall.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
            pInstall.RecordTypeId = mapRTp.get('Enterprise Products');
            pInstall.Service_Term__c = '12-Months';
            pInstall.Mark_Up_Factor__c = 1.0;
            pInstall.Start_Date__c = Date.today();
            pInstall.End_Date__c = Date.today()+1;
            insert pInstall;
            
            PricebookEntry peStandard = new PricebookEntry();
            peStandard.Pricebook2Id = sp1.id;
            peStandard.UnitPrice = 60;
            peStandard.Product2Id = pInstall.id;
            peStandard.IsActive = true;
            peStandard.UseStandardPrice = false;
            
            insert peStandard;
            
            PricebookEntry pe1 = new PricebookEntry();
            pe1.Pricebook2Id = sp.id;
            pe1.UnitPrice = 60;
            pe1.Product2Id = pInstall.id;
            pe1.IsActive = true;
            pe1.UseStandardPrice = false;
            
            insert pe1;
            
            pMin  = new Product2();
            pMin.IsActive = true;
            pMin.Name = 'Fibre';
            pMin.Charge_Type__c = 'NRC';
            pMin.Access_Type__c = 'Ethernet EON';
            pMin.Access_Type_Group__c = 'Fibre';
            pMin.Category__c = 'Overage';
            pMin.Service_Type__c = '10 Mbps - Ethernet Fibre Network Min - Test';
            pMin.RecordTypeId = mapRTp.get('Enterprise Products');
            pMin.Service_Term__c = '12-Months';
            pMin.Mark_Up_Factor__c = 1.0;
            pMin.Start_Date__c = Date.today();
            pMin.End_Date__c = Date.today()+1;
            pMin.isMinSpeed__c = true;
            pMin.Product_Install_Link__c = pInstall.Id;
            insert pMin;
            pMinId = pMin.Id;
            
            //Product 1
            p  = new Product2();
            p.IsActive = true;
            p.Name = 'Fibre';
            p.Charge_Type__c = 'PMO';
            p.Access_Type__c = 'Ethernet EON';
            p.Access_Type_Group__c = 'Fibre';
            p.Category__c = 'Access';
            p.Service_Type__c = '10 Mbps - Ethernet Fibre Network Max - Test';
            p.RecordTypeId = mapRTp.get('Enterprise Products');
            p.Service_Term__c = '12-Months';
            p.Mark_Up_Factor__c = 1.0;
            p.Start_Date__c = Date.today();
            p.End_Date__c = Date.today()+1;
            p.isMaxSpeed__c = true;
            p.Parent__c = pMin.Id;
            insert p;
            pMaxId = p.Id;
            
            peStandard = new PricebookEntry();
            peStandard.Pricebook2Id = sp1.id;
            peStandard.UnitPrice = 60;
            peStandard.Product2Id = p.id;
            peStandard.IsActive = true;
            peStandard.UseStandardPrice = false;
            
            insert peStandard;
            
            pe1 = new PricebookEntry();
            pe1.Pricebook2Id = sp.id;
            pe1.UnitPrice = 60;
            pe1.Product2Id = p.id;
            pe1.IsActive = true;
            pe1.UseStandardPrice = false;
            
            insert pe1;
            priceEntry1id = pe1.Id;
            
            peStandard = new PricebookEntry();
            peStandard.Pricebook2Id = sp1.id;
            peStandard.UnitPrice = 60;
            peStandard.Product2Id = pMin.id;
            peStandard.IsActive = true;
            peStandard.UseStandardPrice = false;
            
            insert peStandard;
            
            pe1 = new PricebookEntry();
            pe1.Pricebook2Id = sp.id;
            pe1.UnitPrice = 60;
            pe1.Product2Id = pMin.id;
            pe1.IsActive = true;
            pe1.UseStandardPrice = false;
            
            insert pe1;
        }
    }
}