/*Class Name  : TestClass_oppProductCustomController.
 *Description : This is test class for opportunityProductCustomController.
 *Created By  : Rajiv Gangra.
 *Created Date :11/09/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
@isTest(seeAllData=true)
private class TestClass_oppProductDisplayController {
    private static Account accTestObj;
    private static Opportunity oppTestObj;
    private static OpportunityLineItem  oliTestObj;
    private static OpportunityLineItem  oliTestObjNeg;
    private static Case caseTestObj;
    private static user objUser;
    private static Product2 p2;
    private static Pricebook2 stdpb;
    private static PricebookEntry pbe;
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
    private static CPQ_Settings__c CPQ_cs = CPQ_Settings__c.getInstance();
    
    private static Product_Type_List__c custSetting = [SELECT id, Name, Form_Type__c, Order__c, Product_Category__c, Product_Family__c, Schedule_Type__c from Product_Type_List__c limit 1];
    private static void setUpData(){
       
        CPQ_cs.DefaultOpportunityPB__c = 'CPQ Temp PriceBook';
        CPQ_cs.DefaultOpportunityRecordType__c = 'Rogers EBU - New';
        insert CPQ_cs;
        
        accTestObj = new Account(Name='testAcc');
        accTestObj.BillingCountry= 'CA';
        accTestObj.BillingPostalCode = 'A9A 9A9';
        accTestObj.BillingState = 'MA';
        accTestObj.BillingCity='City';
        accTestObj.BillingStreet='Street';
        insert accTestObj;
        
      
        //p2 = [SELECT Id FROM Product2 where Name = 'INTERNET' LIMIT 1];
        p2 = new Product2(Name='Test Product',isActive=true,CanUseQuantitySchedule=true,CanUseRevenueSchedule=true,QuantityScheduleType='Divide',RevenueScheduleType='Repeat', NumberOfRevenueInstallments=5,NumberOfQuantityInstallments=5,QuantityInstallmentPeriod='Daily',RevenueInstallmentPeriod='Daily');
        insert p2;
        stdpb = [SELECT Id FROM Pricebook2 WHERE IsStandard = true];
        pbe = new PricebookEntry(Pricebook2Id = stdpb.Id, Product2Id = p2.Id, UnitPrice = 1, isActive = true);
        insert pbe;
              
        oppTestObj = new Opportunity();
        oppTestObj.AccountId=accTestObj.id;
        oppTestObj.Name='test';
        oppTestObj.StageName='New';
        oppTestObj.CloseDate=system.today();
        oppTestObj.Unified_Comm_Collaboration_Estimated__c =20;
        oppTestObj.PriceBook2Id =stdpb.id;
        Insert oppTestObj;      
              
        oliTestObj= new OpportunityLineItem();
        oliTestObj.ARPU__c=10;
        oliTestObj.Start_Date__c=system.today();
        oliTestObj.Quantity=10;
        oliTestObj.Installment_Period__c='Monthly';
        oliTestObj.of_Installments__c=10;
        oliTestObj.Monthly_Value__c=100;
        oliTestObj.Minimum_Contract_Value__c=100;
        oliTestObj.PricebookEntryId=pbe.id;
        oliTestObj.OpportunityId=oppTestObj.id;
        oliTestObj.TotalPrice=0;
        oliTestObj.Product_Family__c='Cable - TV';
        insert oliTestObj;
        oliTestObjNeg= new OpportunityLineItem();
       
        caseTestObj= new case(accountID=accTestObj.id);
    }
    static testmethod void myUnitWithCaseTestDeployment(){
        setUpData();
        PageReference pageRef = Page.OpportunityProductTemplate;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', oliTestObj.id);
        ApexPages.StandardController sCrtl= new ApexPages.StandardController(oliTestObj);
        //System.runAs(objUser) {
        Test.starttest();
        opportunityProductDisplayController oProdCtrl = new opportunityProductDisplayController(sCrtl); 
        oProdCtrl.editOLI();
        oProdCtrl.resetAll();

        oProdCtrl.DeleteAll();
        oProdCtrl.DeleteOppProduct();        
        
        Test.stoptest();
        //}
    }
    static testmethod void testmethod1(){
        setUpData();
        PageReference pageRef = Page.OpportunityProductTemplate;
        Test.setCurrentPage(pageRef);
        oliTestobj.Product_Family__c = 'Monthly/One Time Form';
        custSetting.Product_Family__c = 'Monthly/One Time Form';
        custSetting.Form_Type__c= 'Monthly/One Time Form';
        update oliTestobj;
        update custsetting;
        ApexPages.currentPage().getParameters().put('id', oliTestObj.id);
        ApexPages.StandardController sCrtl= new ApexPages.StandardController(oliTestObj);
        //System.runAs(objUser) {
        Test.starttest();
        opportunityProductDisplayController oProdCtrl = new opportunityProductDisplayController(sCrtl); 
        oProdCtrl.getHasErrorMessages();        
        oProdCtrl.resetAll();        
        Test.stoptest();
        //}
    }
    
    static testmethod void testmethod2(){
        setUpData();
        PageReference pageRef = Page.OpportunityProductTemplate;
        Test.setCurrentPage(pageRef);
        oliTestobj.Product_Family__c = 'Reseller Form';
        custSetting.Product_Family__c = 'Reseller Form';
        custSetting.Form_Type__c= 'Reseller Form';
        update oliTestobj;
        update custsetting;
        ApexPages.currentPage().getParameters().put('id', oliTestObj.id);
        ApexPages.StandardController sCrtl= new ApexPages.StandardController(oliTestObj);
        //System.runAs(objUser) {
        Test.starttest();
        opportunityProductDisplayController oProdCtrl = new opportunityProductDisplayController(sCrtl);         
        oProdCtrl.resetAll();        
        Test.stoptest();
        //}
    }
    
    static testmethod void testmethod3(){
        setUpData();
        PageReference pageRef = Page.OpportunityProductTemplate;
        Test.setCurrentPage(pageRef);
        oliTestobj.Product_Family__c = 'Wireless - Voice & Data';
        custSetting.Product_Family__c = 'Wireless - Voice & Data';
        custSetting.Form_Type__c= 'Reseller Form';
        update oliTestobj;
        update custsetting;
        ApexPages.currentPage().getParameters().put('id', oliTestObj.id);
        ApexPages.StandardController sCrtl= new ApexPages.StandardController(oliTestObj);
        //System.runAs(objUser) {
        Test.starttest();
        opportunityProductDisplayController oProdCtrl = new opportunityProductDisplayController(sCrtl);         
        oProdCtrl.resetAll();        
        Test.stoptest();
        //}
    }
     
}