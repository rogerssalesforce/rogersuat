/*********************************************************************************************************
Class Name      : CPQ_CustomPricingCallBack
Description     : Class for updating the price of standard Products(US-0172) 
Created By      : Mitali Telang
Created Date    : 23-Sep-15
Modification Log:
----------------------------------------------------------------------------------------------------------- 
Developer                   Date                   Description
Mitali Telang           23-Sep-15             Original version
Deepika Rawat           30-Nov-15             Updated method beforePricing(). 
                                              Modified logic to add markups for custom products.
-----------------------------------------------------------------------------------------------------------            
***********************************************************************************************************/
global with sharing class CPQ_CustomPricingCallBack implements Apttus_Config2.CustomClass.IPricingCallback2{
    private Apttus_Config2.ProductConfiguration cart = null;
    private Apttus_Config2.CustomClass.PricingMode mode = null;
    private List<CPQ_CustomRepriceSetting__c> lstCustomPricing;
    List<Apttus_Config2__LineItem__c> cusTomProdLineItems; 
    
    /*********************************************************************************
    Method Name    : afterPricing
    Return Type    : Void  
    Parameter      : Apttus_Config2.ProductConfiguration.LineItemColl             
    *********************************************************************************/ 
    global void afterPricing(Apttus_Config2.ProductConfiguration.LineItemColl COLineItem){
        system.debug('!!! afterPricing');
        system.debug('!!! afterPricing - mode = ' + mode);
        system.debug('COLineItem = ' + COLineItem);
        system.debug('CPQ_LineItemTriggerHelper.bFirstRun = ' + CPQ_LineItemTriggerHelper.bFirstRun);
        
        if (String.valueOf(mode) != 'BASEPRICE') return;
        if(COLineItem == null) {
            return;         
        }
        if (!CPQ_LineItemTriggerHelper.bFirstRun) return;
        
        List<Id> lstLineItemsIds = new List<Id>();
        for (Apttus_Config2.LineItem lineItem : COLineItem.getAllLineItems()) {
            Apttus_Config2__LineItem__c lineItemSO = lineItem.getLineItemSO();
            lstLineItemsIds.add(lineItemSO.Id);
        }
        List<Apttus_Config2__LineItem__c> lstLineItems = [SELECT Id, Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__r.Apttus_Proposal__Opportunity__c FROM Apttus_Config2__LineItem__c
                                                          WHERE Id IN: lstLineItemsIds];
        
        List<Id> lstOpportunitiesIds = new List<Id>(); 
        Map<Id, Id> mapLineItemOpportunity = new Map<Id, Id>();
        for (Apttus_Config2__LineItem__c lineItem : lstLineItems) {
            lstOpportunitiesIds.add(lineItem.Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__r.Apttus_Proposal__Opportunity__c);
            mapLineItemOpportunity.put(lineItem.Id, lineItem.Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__r.Apttus_Proposal__Opportunity__c);
        }
        
        List<Apttus_Config2__AccountLocation__c> lstAccountLocations = [SELECT Id, Opportunity__c FROM Apttus_Config2__AccountLocation__c WHERE Opportunity__c IN: lstOpportunitiesIds]; 
        Map<Id, List<Apttus_Config2__AccountLocation__c>> mapOpportunityAccountLocations = new Map<Id, List<Apttus_Config2__AccountLocation__c>>();
        List<Apttus_Config2__AccountLocation__c> lstTempAccountLocations;
        for (Apttus_Config2__AccountLocation__c accountLocation : lstAccountLocations) {
            lstTempAccountLocations = mapOpportunityAccountLocations.get(accountLocation.Opportunity__c);
            if (lstTempAccountLocations == null) {
                lstTempAccountLocations = new List<Apttus_Config2__AccountLocation__c>();
            }
            lstTempAccountLocations.add(accountLocation);
            mapOpportunityAccountLocations.put(accountLocation.Opportunity__c, lstTempAccountLocations);
        }
        List<Apttus_Config2__AccountLocation__c> lstAccountLocationsForItem;
        
        
        system.debug('lstLineItems = ' + lstLineItems);
        system.debug('mapLineItemOpportunity = ' + mapLineItemOpportunity);
        system.debug('mapOpportunityAccountLocations = ' + mapOpportunityAccountLocations);

        
        List<Apttus_Config2__LineItem__c> lstLineItemsToInsert = new List<Apttus_Config2__LineItem__c>();
        Apttus_Config2.LineItem lineItemFirst = COLineItem.getAllLineItems()[0];
        Apttus_Config2__LineItem__c lineItemSOFirst = lineItemFirst.getLineItemSO();
        List<Apttus_Config2__LineItem__c> lstAllLineItems = [SELECT Id, Apttus_Config2__LineNumber__c FROM Apttus_Config2__LineItem__c 
                                                            WHERE Apttus_Config2__ConfigurationId__c =: lineItemSOFirst.Apttus_Config2__ConfigurationId__c ORDER BY Apttus_Config2__LineNumber__c desc];
        Decimal dCurrentLineItemsNumber = 0;
        if (lstAllLineItems != null && lstAllLineItems.size() > 0) {
            dCurrentLineItemsNumber = lstAllLineItems[0].Apttus_Config2__LineNumber__c;
        }
        system.debug('dCurrentLineItemsNumber = ' + dCurrentLineItemsNumber);
        
        Integer index = 0;
        for (Apttus_Config2.LineItem lineItem : COLineItem.getAllLineItems()) {
            index++;
            Apttus_Config2__LineItem__c lineItemSO = lineItem.getLineItemSO();
            
            lstAccountLocationsForItem = mapOpportunityAccountLocations.get(mapLineItemOpportunity.get(lineItemSO.Id));
            system.debug('lstAccountLocationsForItem = ' + lstAccountLocationsForItem);
            
            system.debug('lineItemSO = ' + lineItemSO);
            system.debug('lineItem = ' + lineItem);
            
            if (lstAccountLocationsForItem != null) {
                for (Apttus_Config2__AccountLocation__c accountLocation : lstAccountLocationsForItem) {
                    Apttus_Config2__LineItem__c oLineItem = new Apttus_Config2__LineItem__c();
                    oLineItem = lineItemSO.clone(false, false, false, false);
                    oLineItem.Apttus_Config2__LineNumber__c = dCurrentLineItemsNumber + index;
                    oLineItem.Apttus_Config2__PrimaryLineNumber__c = dCurrentLineItemsNumber + index;
                    //oLineItem.Apttus_Config2__LineSequence__c = numberOfRecords + index;
                    lstLineItemsToInsert.add(oLineItem);
                }
            }
        }
        system.debug('lstLineItemsToInsert = ' + lstLineItemsToInsert);
        
        CPQ_LineItemTriggerHelper.bFirstRun = false;
        if (lstLineItemsToInsert.size() > 0) {
            insert lstLineItemsToInsert;
        }
        lstLineItemsToInsert.clear();
    }
    
    /*********************************************************************************
    Method Name    : afterPricingLineItem
    Return Type    : Void  
    Parameter      : Apttus_Config2.ProductConfiguration.LineItemColl, Apttus_Config2.LineItem            
    *********************************************************************************/ 
    global void afterPricingLineItem(Apttus_Config2.ProductConfiguration.LineItemColl COLineItem, Apttus_Config2.LineItem lineItemMO){
    }
    
    /*********************************************************************************
    Method Name    : beforePricing
    Description    : Method for updating the price of standard Products
    Return Type    : Void  
    Parameter      : Apttus_Config2.ProductConfiguration.LineItemColl             
    *********************************************************************************/ 
    global void beforePricing(Apttus_Config2.ProductConfiguration.LineItemColl COLineItem){
        cusTomProdLineItems = new List<Apttus_Config2__LineItem__c>();
        //null check for the Line Items
        if(COLineItem == null){
            return;         
        }

        //Get default Adjustments for Custom products based on Product Family
        lstCustomPricing = new List<CPQ_CustomRepriceSetting__c>();
        lstCustomPricing = CPQ_CustomRepriceSetting__c.getall().values();
        Map<String, CPQ_CustomRepriceSetting__c> mapCustomPrice = new Map<String, CPQ_CustomRepriceSetting__c>();
        set<String> setProdPillarCustpmPrice = new Set<String>();
        // Iterating through CPQ_CustomRepriceSetting__c to create a map for related Product Family
        for(CPQ_CustomRepriceSetting__c cp: lstCustomPricing){
            mapCustomPrice.put(cp.ProductFamily__c+cp.Type_of_Product__c, cp);
        }
        setProdPillarCustpmPrice.addAll(mapCustomPrice.keySet());
        //Get Profile Information
        Set<String> setDDProfiles = new Set<String>();
        CPQ_Settings__c cpqSetting = CPQ_Settings__c.getOrgDefaults();
        List<String> lstDealDeskProfiles = cpqSetting.RogersDealDesk__c.Split(';');
        setDDProfiles.addAll(lstDealDeskProfiles);
        Id profileId=userinfo.getProfileId();
        String sProfileName=[Select Id,Name from Profile where Id=:profileId].Name;
        //Iterating through Line Items
        
        for (Apttus_Config2.LineItem lineItem : COLineItem.getAllLineItems()) {
            Apttus_Config2__LineItem__c lineItemSO = lineItem.getLineItemSO();
            //Check if the set contains the required Product Family
            if(setProdPillarCustpmPrice.contains(lineItemSO.ProductPillar__c+lineItemSO.TypeOfProduct__c)){ 
                //Check if Base price is greater than 0 and if user profile is allowed to updated adjustment on produuct
                if( lineItemSO.Apttus_Config2__BasePriceOverride__c > 0 && lineItemSO.From_Custom_Product_Form__c ==true &&((!setDDProfiles.contains(sProfileName) ) || (setDDProfiles.contains(sProfileName) && lineItemSO.Apttus_Config2__AdjustmentAmount__c==null))){  
                    cusTomProdLineItems.add(lineItemSO);
                    CPQ_CustomRepriceSetting__c cp = new CPQ_CustomRepriceSetting__c();
                    cp = mapCustomPrice.get(lineItemSO.ProductPillar__c+lineItemSO.TypeOfProduct__c);
                    lineItemSO.Apttus_Config2__AdjustmentAmount__c = Decimal.valueOf(cp.Adjustment_Amount__c);
                    lineItemSO.Apttus_Config2__AdjustmentType__c = cp.Adjustment_Type__c;                    
                }
               
            }
            //For Custom Products calculate and populate fields
            if( lineItemSO.IsCustomProduct__c==true ){
                lineItemSO.Apttus_Config2__NetUnitPrice__c = lineItemSO.Apttus_Config2__BasePriceOverride__c;
                lineItemSO.Apttus_Config2__BaseExtendedPrice__c = lineItemSO.Apttus_Config2__BasePriceOverride__c * lineItemSO.Apttus_Config2__Quantity__c;
                lineItemSO.Apttus_Config2__ExtendedPrice__c = lineItemSO.Apttus_Config2__BasePriceOverride__c *  lineItemSO.Apttus_Config2__Quantity__c;
                lineItemSO.Apttus_Config2__NetPrice__c = lineItemSO.Apttus_Config2__BasePriceOverride__c *  lineItemSO.Apttus_Config2__Quantity__c;
                lineItemSO.Apttus_Config2__AdjustmentAmount__c = lineItemSO.Apttus_Config2__AdjustmentAmount__c;
                lineItemSO.Apttus_Config2__AdjustmentType__c = lineItemSO.Apttus_Config2__AdjustmentType__c; 
            }
        }
    }  
    /*********************************************************************************
    Method Name    : beforePricingLineItem
    Return Type    : Void  
    Parameter      : Apttus_Config2.ProductConfiguration.LineItemColl, Apttus_Config2.LineItem            
    *********************************************************************************/ 
    global void beforePricingLineItem(Apttus_Config2.ProductConfiguration.LineItemColl listLineItem, Apttus_Config2.LineItem lineItemMO){
    }
    /*********************************************************************************
    Method Name    : finish
    Return Type    : Void  
    Parameter      : nil            
    *********************************************************************************/ 
    global void finish(){
        
    }  
    /*********************************************************************************
    Method Name    : setMode
    Return Type    : Void  
    Parameter      : Apttus_Config2.CustomClass.PricingMode            
    *********************************************************************************/ 
    global void setMode(Apttus_Config2.CustomClass.PricingMode mode){
        this.mode = mode;
    }
    
    /*********************************************************************************
    Method Name    : start
    Return Type    : Void  
    Parameter      : Apttus_Config2.ProductConfiguration           
    *********************************************************************************/ 
    global void start(Apttus_Config2.ProductConfiguration pc){
        system.debug('pc = ' + pc);
    } 
}