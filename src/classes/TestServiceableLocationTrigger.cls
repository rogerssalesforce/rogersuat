/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestServiceableLocationTrigger {

    static testMethod void isCNITest() {
        ServiceableLocation__c s = new ServiceableLocation__c(Name='Test', Street_Number__c='123', Street_Name__c='Sesame', Street_Type__c='Ave', City__c='Toronto', Access_Type_Group__c = 'CNI', Province_Code__c='ON', CLLI_Code__c='12345678');
        insert s;
    }
    
    static testMethod void isNeitherNNI_CNITest() {
        ServiceableLocation__c s = new ServiceableLocation__c(Name='Test', Street_Number__c='123', Street_Name__c='Sesame', Street_Type__c='Ave', City__c='Toronto', Access_Type_Group__c = 'FIBRE', Province_Code__c='ON', CLLI_Code__c='12345678');
        insert s;
    }
    
     static testMethod void noAccessTypeGroupTest() {
        ServiceableLocation__c s = new ServiceableLocation__c(Name='Test', Street_Number__c='123', Street_Name__c='Sesame', Street_Type__c='Ave', City__c='Toronto', Province_Code__c='ON', CLLI_Code__c='12345678');
        insert s;
    }
    
    static testMethod void isNNITest() {
        ServiceableLocation__c s = new ServiceableLocation__c(Name='Test', Street_Number__c='123', Street_Name__c='Sesame', Street_Type__c='Ave', City__c='Toronto', Access_Type_Group__c = 'NNI', Province_Code__c='ON', CLLI_Code__c='12345678');
        insert s;
    }
}