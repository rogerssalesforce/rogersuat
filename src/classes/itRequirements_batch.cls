/*
===============================================================================
 Class Name   : itRequirements_batch
===============================================================================
Scan all IT Requirements records and submit the results using an Excel file.
Include:
-All work items with active and on-hold statuses.
-All work items with cancelled and closed that have an
actual closed date within the last week. 

Jacob Klay, 11/11/2014

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
===============================================================================
*/

global class itRequirements_batch implements Database.Batchable<SObject>{
    
    global String query =
        'SELECT Name,Type__c,Status__c,Summary__c,Progress__c,Due_Date__c,BA_Prime__r.Name,Development_Prime__r.Name,Start_Date__c,Actual_Closed_Date__c FROM IT_Requirement__c WHERE (Actual_Closed_Date__c >= LAST_WEEK OR Actual_Closed_Date__c = null) AND isDeleted = false ORDER BY Due_Date__c ASC';
    
    List<String> toAddressesList = new List<String>();
    global String[] toAddresses;
    String header = '';
    blob csvBlob = null;
    
    static OrgWideEmailAddress orgEmailId=[select id, Address, DisplayName from OrgWideEmailAddress limit 1] ;

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<IT_Requirement__c> scope) {
        List<String> toAddressesList = new List<String>();
        //String[] toAddresses;
        //String header = '';
        //blob csvBlob = null;
        List<IT_Requirement__c> listItRequirementsActive = new  List<IT_Requirement__c>();
        List<IT_Requirement__c> listItRequirementsClosed = new  List<IT_Requirement__c>();
        List<IT_Requirement__c> reqsDueThisWeek = [SELECT Name FROM IT_Requirement__c WHERE Due_Date__c = THIS_WEEK AND isDeleted = false];     
        
        //if(!Test.isRunningTest()){
        List<UserRole> userRoles = [SELECT Name,(SELECT Email FROM UserRole.Users) FROM UserRole WHERE UserRole.Name = 'Rogers IT'];
        for (UserRole userRole:userRoles) {
            List<User> users = userRole.getSObjects('Users');
            for(User user:users) {
                toAddressesList.add(user.Email);
            }
        }
        toAddresses = new String[toAddressesList.size()];
        for (Integer i = 0; i < toAddressesList.size(); i++) {
            toAddresses[i] = toAddressesList.get(i);
        }
        //}
              
        for(IT_Requirement__c element:scope){
            if(element.Actual_Closed_Date__c == null) {
                listItRequirementsActive.add(element);
            } else {
                listItRequirementsClosed.add(element);
            }
        }
        
        header = 'Report Date: ' +  System.today().format() + '\n\n';
        header += 'Active Items \n';
        header += 'Title,Type,Status,Summary,Progress(%),Due Date,BA Prime,Dev Prime,Start Date \n';
        if (!listItRequirementsActive.isEmpty()) {
            for(IT_Requirement__c element: listItRequirementsActive) {
                String recordString = '"'+element.Name+'","'+element.Type__c+'","'+element.Status__c+'","'+element.Summary__c+'","'+element.Progress__c+
                '","'+element.Due_date__c.format()+'","'+element.BA_Prime__r.Name+'","'+element.Development_Prime__r.Name+'","'+element.Start_Date__c.format()+'"\n';
                header += recordString;
            }
            header += '\n';
        } else {
            header += 'There are currenctly no active items \n';
        }
        header += 'Closed and Canceled Items \n';
        header += 'Title,Type,Status,Summary,Due Date,Actual Closed Date,BA Prime,Dev Prime \n';
        if (!listItRequirementsClosed.isEmpty()) {
            for(IT_Requirement__c element: listItRequirementsClosed) {
                String recordString = '"'+element.Name+'","'+element.Type__c+'","'+element.Status__c+'","'+element.Summary__c+'","'+element.Due_date__c.format()+
                '","'+element.Actual_closed_date__c.format()+'","'+element.BA_Prime__r.Name+'","'+element.Development_Prime__r.Name+'"\n';
                header += recordString;
            }
            header += '\n';
        } else {
            header += 'There are currenctly no closed or canaceled items \n';
        }
        header += 'Total Active: '+listItRequirementsActive.size()+'\n';
        header += 'Total Closed/Canceled: '+listItRequirementsClosed.size()+'\n';
        header += 'Total Due This Week: '+reqsDueThisWeek.size()+'\n';
        
        
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        csvBlob = Blob.valueOf(header);
        string csvname= 'SFDC Status Report.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        String body = 'Hi,<br/><br/>Please find attached the SFDC status report. <br/><br/>'; 
        body += '<br/> <br/> Best regards, <br/> Rogers IT Team'; 
        email.setToAddresses(toAddresses); 
        email.setSubject('SFDC Status Report ' + System.today().format());        
        email.setSaveAsActivity(false);  
        email.setHtmlBody(body); 
        email.setOrgWideEmailAddressId(orgEmailId.id); 
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
   }

    global void finish(Database.BatchableContext BC){
        header = null;
        csvBlob = null;
    }
}