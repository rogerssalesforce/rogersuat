@isTest
public class CaseTrigger_PopulateFormulaFieldsTest{
    
     private static Account accTestObj;
     private static Opportunity oppTestObj;
     private static Case caseTest;
    
     public static testMethod void caseTriggerTest(){
         List<User> uList = [SELECT id, Name, Manager.Email, Email,isActive from User Where Profile.Name like '%System Administrator%' and isActive = true];
         
         uList[0].ManagerId = uList[2].id;
         update uList[0];
         
         Team_Assignment_Governance_Settings__c TAGcs = new Team_Assignment_Governance_Settings__c (Unassigned_User__c = 'Unassigned User', Unassigned_Status__c = 'Unassigned', Business_case__c = 'This is a new account.', New_Acct_Reason_Code__c = '4 - New Account Creation');
         insert TAGcs;
        CPQ_Settings__c CPQ_cs= new CPQ_Settings__c();  
        CPQ_cs.DefaultOpportunityPB__c ='CPQ Temp PriceBook';
        CPQ_cs.DefaultOpportunityRecordType__c = 'Rogers EBU - New';
        insert CPQ_cs;
         
         accTestObj = new Account(Name='testAcc');
         accTestObj.BillingCountry= 'CA';
         accTestObj.BillingPostalCode = 'A9A 9A9';
         accTestObj.BillingState = 'MA';
         accTestObj.BillingCity='City';
         accTestObj.BillingStreet='Street';
         insert accTestObj;
        
         oppTestObj = new Opportunity();
         oppTestObj.AccountId=accTestObj.id;
         oppTestObj.Name='test';
         oppTestObj.StageName='New';
         oppTestObj.Unified_Comm_Collaboration_Estimated__c =20;
         oppTestObj.CloseDate=system.today();
         oppTestObj.OwnerId = uList[0].id;
         Insert oppTestObj;
         
         caseTest = new Case();
         caseTest.Opportunity__c = oppTestObj.id;
         insert caseTest;         
     }
}