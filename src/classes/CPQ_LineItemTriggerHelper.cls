/***************************************************************************************
Class Name      : CPQ_LineItemTriggerHelper
Description     : This class is helper class for CPQ_LineItemTrigger 
                  page.
Created By      : Deepika Rawat 
Created Date    : 04-Nov-15  
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Deepika Rawat             04-Nov-15               Original Version 
Deepika Rawat             04-Dec-15               Added methods restoreBasePriceOverride
                                                  and makeFromCustomProductFlagFalse   
*****************************************************************************************/
public with sharing class CPQ_LineItemTriggerHelper{
    public static Boolean bFirstRun = true;
    
    /************************************************************************************
    Method Name    : restrictCartLineItemDeletion
    Description    : This method restricts user from deleting line items based on Quote 
                        approval stage and User's profile
    Return Type    : void
    Parameter      : List<Apttus_Config2__LineItem__c>
    *************************************************************************************/
    public static void restrictCartLineItemDeletion(List<Apttus_Config2__LineItem__c> lstLineItems){
        Set<String> setAEProfiles = new Set<String>();
        Set<String> setSEProfiles = new Set<String>();
        Set<String> setDDProfiles = new Set<String>();
        Set<String> setAdminProfiles = new Set<String>();
        CPQ_Settings__c cpqSetting = CPQ_Settings__c.getOrgDefaults();
        List<String> lstAEProfiles = cpqSetting.RogersAE__c.Split(';');
        List<String> lstAEProfiles2 = cpqSetting.RogersAE_2__c.Split(';');
        List<String> lstSEProfiles = cpqSetting.RogersSE__c.Split(';');
        List<String> lstDDProfiles = cpqSetting.RogersDealDesk__c.Split(';');
        List<String> lstAdminProfiles = cpqSetting.System_Administrator__c.Split(';');
        setAEProfiles.addAll(lstAEProfiles);
        setAEProfiles.addAll(lstAEProfiles2);
        setSEProfiles.addAll(lstSEProfiles);
        setDDProfiles.addAll(lstDDProfiles);
        setAdminProfiles.addAll(lstAdminProfiles);
        Set<Id> setProdConfigIds = new Set<Id>();
        //Get the Product Configuration Ids
        for(Apttus_Config2__LineItem__c item : lstLineItems){
            setProdConfigIds.add(item.Apttus_Config2__ConfigurationId__c);
        }
        Map<id, Apttus_Config2__ProductConfiguration__c> mapProdConfig = new Map<id, Apttus_Config2__ProductConfiguration__c>([select id, Apttus_QPConfig__Proposald__r.Apttus_Proposal__Approval_Stage__c from Apttus_Config2__ProductConfiguration__c where id in : setProdConfigIds]);
        String sUserProfileName= [Select name from profile where id=:userinfo.getProfileId()].name;
        //Check all the line items requested to be deleted and throw error where user is not allowed to delete any any line
        for(Apttus_Config2__LineItem__c item : lstLineItems){
            Apttus_Config2__ProductConfiguration__c oProdConfig = mapProdConfig.get(item.Apttus_Config2__ConfigurationId__c);
            String sQStage = oProdConfig.Apttus_QPConfig__Proposald__r.Apttus_Proposal__Approval_Stage__c;
             // Restrict user from removing cart items 1. if Profile AE, SE if Status is 'Soln under Review' ,
            // 2. if Profile AE, SE if Status is 'In Review'
            if(!(setAdminProfiles.contains(sUserProfileName) || ((setAEProfiles.contains(sUserProfileName) ||setSEProfiles.contains(sUserProfileName) || setDDProfiles.contains(sUserProfileName)) && sQStage == 'Draft') || (setDDProfiles.contains(sUserProfileName) && sQStage == 'Solution Under Review') || (setDDProfiles.contains(sUserProfileName) && sQStage == 'In Review'))){
                item.addError(Label.CPQ_removeLineItemErrorMsg);
            }
        }
    }
    /************************************************************************************
    Method Name    : restoreBasePriceOverride
    Description    : This method populates is BasePrice is null
    Return Type    : void
    Parameter      : Map<id, Apttus_Config2__LineItem__c>,  Map<id,Apttus_Config2__LineItem__c>
    *************************************************************************************/
    public static void restoreBasePriceOverride(Map<id, Apttus_Config2__LineItem__c> mapNewLineItems, Map<id,Apttus_Config2__LineItem__c> mapOldLineItems){
        Apttus_Config2__LineItem__c oldLineItem;
        //Ensure that BasePriceOverride is not equal to Null
        for(Apttus_Config2__LineItem__c newLineItem :mapNewLineItems.values()){
            oldLineItem = mapOldLineItems.get(newLineItem.id);
            if(oldLineItem!=null && newLineItem.Apttus_Config2__BasePriceOverride__c== null && oldLineItem.Apttus_Config2__BasePriceOverride__c!=null && oldLineItem.Apttus_Config2__BasePriceOverride__c>=0 ){
                newLineItem.Apttus_Config2__BasePriceOverride__c = oldLineItem.Apttus_Config2__BasePriceOverride__c;
            }
        }
    }
    /************************************************************************************
    Method Name    : makeFromCustomProductFlagFalse
    Description    : This method unchecks From_Custom_Product_Form__c field on LineItem
    Return Type    : void
    Parameter      : List<Apttus_Config2__LineItem__c>
    *************************************************************************************/
    public static void makeFromCustomProductFlagFalse(List<Apttus_Config2__LineItem__c> lstLineItems){
        List<Apttus_Config2__LineItem__c>  updateLineItem = new List<Apttus_Config2__LineItem__c>();
        List<CPQ_CustomRepriceSetting__c> lstCustomPricing = new List<CPQ_CustomRepriceSetting__c>();
        lstCustomPricing = CPQ_CustomRepriceSetting__c.getall().values();
        system.debug('lstLineItems******'+lstLineItems);
        Map<String, CPQ_CustomRepriceSetting__c> mapCustomPrice = new Map<String, CPQ_CustomRepriceSetting__c>();
        set<String> setProdPillarCustpmPrice = new Set<String>();
        Set<Id> lineItemIds = new Set<Id>();
        //Create set of all lineitem ids where From_Custom_Product_Form__c = true
        for(Apttus_Config2__LineItem__c lineItem : lstLineItems){
            if(lineItem.From_Custom_Product_Form__c == true){
                lineItemIds.add(lineItem.id); 
            }
        }
        system.debug('lineItemIds******'+lineItemIds);
        List<Apttus_Config2__LineItem__c>  lstLineItem = [select From_Custom_Product_Form__c, ProductPillar__c, TypeOfProduct__c, Apttus_Config2__AdjustmentAmount__c from Apttus_Config2__LineItem__c where id in:lineItemIds];
        system.debug('lstLineItem******'+lstLineItem);
        // Iterating through CPQ_CustomRepriceSetting__c to create a map for related Product Family
        for(CPQ_CustomRepriceSetting__c cp: lstCustomPricing){
            mapCustomPrice.put(cp.ProductFamily__c+cp.Type_of_Product__c, cp);
        }
        system.debug('mapCustomPrice******'+mapCustomPrice);
        setProdPillarCustpmPrice.addAll(mapCustomPrice.keySet());
        system.debug('setProdPillarCustpmPrice******'+setProdPillarCustpmPrice);
        //Iterating through Line Items
        for (Apttus_Config2__LineItem__c lineItem : lstLineItem) {
            //Check if the set contains the required Product Family
            if(setProdPillarCustpmPrice.contains(lineItem.ProductPillar__c+lineItem.TypeOfProduct__c) && lineItem.Apttus_Config2__AdjustmentAmount__c !=null && lineItem.From_Custom_Product_Form__c==true){ 
                lineItem.From_Custom_Product_Form__c =false;
                updateLineItem.add(lineItem);
            }
        }
        system.debug('updateLineItem******'+updateLineItem);
        //Update LineItems
        if(updateLineItem.size()>0){
            update updateLineItem;
        }
    }
}