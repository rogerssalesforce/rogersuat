/*
===============================================================================
Class Name : Test_TAG_DailyEmailScheduler 
===============================================================================
PURPOSE: This is a test class for TAG_DailyEmailScheduler  class

COMMENTS: 

Developer:Aakanksha Patel
Date: 16/3/2015


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
16/3/2015               Aakanksha             Created
===============================================================================

*/
@isTest(SeeAllData = false)
private class Test_TAG_DailyEmailScheduler{
 static testmethod void testMethod1(){
                
        Test.StartTest();
        Tag_DataInstantiation obj2 = new Tag_DataInstantiation();
        obj2.populateDataSharedMSDMember143();
        TAG_DailyEmailScheduler obj= new TAG_DailyEmailScheduler();
        String sch = '0 0 23 * * ?';
        system.schedule('TAG_DailyEmailSchedulerTesting', sch, obj);
        System.assert(true);
        Test.stopTest();    

    }
}