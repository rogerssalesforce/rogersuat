/*********************************************************************************
Class Name      : CPQ_SelectPrimaryQuoteDoccontoller 
Description     : This class is a controller for CPQ_selectPrimaryQuoteDoc page. 
Created By      : Deepika Rawat
Created Date    : 10-Sep-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                       Date                   Description
Deepika Rawat                  10-Sep-15             Original version
----------------------------------------------------------------------------------            
*********************************************************************************/
public with sharing class CPQ_SelectPrimaryQuoteDoccontoller {
    public string sDocSelected {get;set;}
    public Apttus_Proposal__Proposal__c oQuote {get;set;}
    public Boolean bRefreshPage {get; set;}
    public List<Attachment> lstAttachments{get;set;}
    public List<Attachment> lstPrimaryDoc {get;set;}
    private map<string, string> taxGroupMap = new map<string, string> ();

    /*********************************************************************************
    Method Name    : CPQ_SelectPrimaryQuoteDoccontoller
    Description    : This is a constructor class.
    Return Type    : null
    Parameter      : ApexPages.StandardController
    *********************************************************************************/
    public CPQ_SelectPrimaryQuoteDoccontoller(ApexPages.StandardController stdController) {
        oQuote = [Select Primary_Quote_Document_ID__c from Apttus_Proposal__Proposal__c where id=:stdController.getId()];
        lstAttachments = new List<Attachment>();
        lstPrimaryDoc = [Select Name from Attachment where id=:oQuote.Primary_Quote_Document_ID__c];
        //Create a list of all attachment related to a Quote : lstAttachments. If a document is already selected as Primary quote document do not add it in the list. 
        if(lstPrimaryDoc!=null&& lstPrimaryDoc.size()>0){
            lstAttachments = [select Name, Id from Attachment where ParentId=:stdController.getId() and id!=: lstPrimaryDoc order by createdDate];
        }
        else
            lstAttachments = [select Name, Id from Attachment where ParentId=:stdController.getId()  order by createdDate];
    }

    /*********************************************************************************
    Method Name    : getDocs
    Description    : This method create SelectOptions with Attached quote documents.
    Return Type    : List<SelectOption>
    Parameter      : null
    *********************************************************************************/  
    public List<SelectOption> getDocs() {
        List<SelectOption> options = new List<SelectOption>();        
        //If there is already a Document selected as Primary quote document put it as first selected option.
        if(lstPrimaryDoc!=null&& lstPrimaryDoc.size()>0){
            options.add(new SelectOption(lstPrimaryDoc[0].id, lstPrimaryDoc[0].Name));
        }
        options.add(new SelectOption(Label.CPQ_None, Label.CPQ_None));
        if(lstAttachments.size()>0){
            for(Attachment documentRec : lstAttachments){
                options.add(new SelectOption(documentRec.id, documentRec.Name));
            }
        }
        return options;  
    }

    /*********************************************************************************
    Method Name    : updateDoc
    Description    : This method updates field Primary_Quote_Document_ID__c on quote with the selected Quote document.
    Return Type    : void
    Parameter      : null
    *********************************************************************************/ 
    public void updateDoc(){
        bRefreshPage = true;
        //Update Primary_Quote_Document_ID__c with selected document. If user seleted "None" update with null.
        if(sDocSelected!=Label.CPQ_None)
            oQuote.Primary_Quote_Document_ID__c = sDocSelected;        
        else
            oQuote.Primary_Quote_Document_ID__c = null;   
        update oQuote;
    }
}