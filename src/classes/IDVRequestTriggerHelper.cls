/*
Helper calss for IDV trigger.
Below code does following 
1. Send email to contract team
2. Share recrords to Contract/EVP team
2. Delete share records once request completed
*/
public with sharing class  IDVRequestTriggerHelper{

public static boolean isStatusChanged(String  oldStatus ,String newStatus){
  if(oldStatus ==null || newStatus ==null) 
  return false;
  
  return ! oldStatus.equals(newStatus);
} 

/*
Send email to contract team .
*/
public void sendEmailToContractTeam(set<id> idvReqIds ){

  List<IDV_Pricing_Request__c>  idvReqs = new List<IDV_Pricing_Request__c>();  
              idvReqs = [Select Name,Company_Name__c,Customers_with_Corporate_Email__c,Approval_Threshold_formula__c,Base_Share_Everything_Discount__c,Case_Owner__c,Company_Incentive__c,Contracts_Prime__c,
                        Conversion_Credit__c,Corporate_BAN__c,Corporate_Email_Domains__c,Corporately_Paid_MSD_Code__c,Date_Time_of_Last_Approval__c,Date_Emp_Verification_Process_TurnedOn__c,Date_Share_Everything_Discount_Loaded__c,Dealer_Address__c,Dealer_Company_Name__c,
                        Dealer_Reps_Email_Address__c,Dealer_Rep_s_First_Last_Name__c,Dealer_Rep_Phone_Number__c,Discount_Applicability__c,EVP_Prime__c,Final_IDV_Request__c,Format_of_Employee_Number__c,Incremental_Share_Everything_Discount__c,Individually_Liable_MSD_Code__c,Individually_Liable_Program_Name__c,
                        IDV_Request_Opportunity__c,Previous_Approver__c,Promotion__c,Rogers_Sales_Director__c,Rogers_Sales_Representative__c,SVP_Approver__c,Target_Number_Of_New_Individually_Liable__c,
                        Total_Share_Everything_Discount__c,VP_Pricing__c,Status__c ,Signing_Authority_email__c,Signing_Authority_Name__c,Signing_Authority_telephone__c,Duns__c,
                        Address__c,  Name__c ,No_of_Employee__c ,Channel__c,Region__c from IDV_Pricing_Request__c where id in :idvReqIds];
                        
                        
                          for(IDV_Pricing_Request__c IDV4 :idvReqs){
                          
                          string header = 'Request No,  Company Name, % of Customers with a Corporate Email,Approval Threshold ,Base Share Everything Discount %,Case Owner,Company Incentive %,Contracts Prime,'+
                                'Conversion Credit $,Corporate BAN,Corporate Email Domain(s),Corporately Paid MSD Code,Date/Time of Last Approval,Date Employee Verification Turned On,Date Share Everything Discount Loaded,'+
                                'Dealer Address,Dealer Company Name,Dealer Rep’s Email Address,Dealer Rep’s First/Last Name,Dealer Rep’s Phone Number,Discount Applicability,EVP Prime,Final IDV Request,'+
                                'Format of Employee Number,Incremental Share Everything Discount %,Individually Liable MSD Code,Individually Liable Program Name,Opportunity,Previous Approver,Promotion,'+
                                'Rogers Sales Director,Rogers Sales Representative,Rogers Sales VP,Target Number Of New Indiv. Liable Lines,Total Share Everything Discount %,VP Sales Operations,Status,Signing Authority Email,Signing Authority Name,Signing Authority Telephone,Duns#,Address,Name,No of Employees,Channel,Region  \n';
                 string finalstr = header ;
           string idvRecordString ='"'+IDV4.Name+'","'+IDV4.Company_Name__c+'","'+IDV4.Customers_with_Corporate_Email__c+'","'+IDV4.Approval_Threshold_formula__c+'","'+IDV4.Base_Share_Everything_Discount__c+'",'+
                                    '"'+IDV4.Case_Owner__c+'","'+IDV4.Company_Incentive__c+'","'+IDV4.Contracts_Prime__c+'","'+IDV4.Conversion_Credit__c+'","'+IDV4.Corporate_BAN__c+'","'+IDV4.Corporate_Email_Domains__c+'","'+IDV4.Corporately_Paid_MSD_Code__c+'",'+
                                    '"'+IDV4.Date_Time_of_Last_Approval__c+'","'+IDV4.Date_Emp_Verification_Process_TurnedOn__c+'","'+IDV4.Date_Share_Everything_Discount_Loaded__c+'","'+IDV4.Dealer_Address__c+'","'+IDV4.Dealer_Company_Name__c+'",'+
                                    '"'+IDV4.Dealer_Reps_Email_Address__c+'","'+IDV4.Dealer_Rep_s_First_Last_Name__c+'","'+IDV4.Dealer_Rep_Phone_Number__c+'","'+IDV4.Discount_Applicability__c+'","'+IDV4.EVP_Prime__c+'",'+
                                    '"'+IDV4.Final_IDV_Request__c+'","'+IDV4.Format_of_Employee_Number__c+'","'+IDV4.Incremental_Share_Everything_Discount__c+'","'+IDV4.Individually_Liable_MSD_Code__c+'","'+IDV4.Individually_Liable_Program_Name__c+'",'+
                                    '"'+IDV4.IDV_Request_Opportunity__c+'","'+IDV4.Previous_Approver__c+'","'+IDV4.Promotion__c+'","'+IDV4.Rogers_Sales_Director__c+'","'+IDV4.Rogers_Sales_Representative__c+'","'+IDV4.SVP_Approver__c+'",'+
                                   '"'+IDV4.Target_Number_Of_New_Individually_Liable__c+'","'+IDV4.Total_Share_Everything_Discount__c+'","'+IDV4.VP_Pricing__c+'","'+IDV4.Status__c+'",'+
                                    '"'+IDV4.Signing_Authority_email__c+'","'+IDV4.Signing_Authority_Name__c+'","'+IDV4.Signing_Authority_telephone__c+'",'+
                                   '"'+IDV4.Duns__c+'","'+IDV4.Address__c+'","'+IDV4.Name__c+'","'+IDV4.No_of_Employee__c+'","'+IDV4.Channel__c+'","'+IDV4.Region__c +'"\n' ;
                                   
                 finalstr = finalstr +idvRecordString;
                 Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
                 
                 blob csvBlob = Blob.valueOf(finalstr);
                 string csvname= 'IDV_'+IDV4.Name+'.csv';
                 csvAttc.setFileName(csvname);
                 csvAttc.setBody(csvBlob);
                Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
                
             

                
                String[] toAddresses = new String[] {IDV4.Contracts_Prime__c}; 
                String body = 'Hello Sir/Madam, <br/><br/>';
                body  += 'Your  attention  has been requested for the Request:'+ IDV4.Name+' <br/><br/>';        
                
                body +=  'Individually Liable Program Name = '+IDV4.Individually_Liable_Program_Name__c+' <br/><br/>'; 
                body += 'Company Name = '+IDV4.Company_Name__c +' <br/><br/>';
                body +=  'Number of Lines = '+IDV4.Target_Number_Of_New_Individually_Liable__c+' <br/><br/>'; 
                body +=  'Discount Applicability = '+IDV4.Discount_Applicability__c+' <br/><br/>'; 
                body +=  'Promotion = '+IDV4.Promotion__c+' <br/><br/>'; 
                body +=  'Conversion Credit $ = '+IDV4.Conversion_Credit__c+' <br/><br/>'; 
                body +=  'Company Incentive % = '+IDV4.Company_Incentive__c+' <br/><br/>'; 
                body +=  'Incremental Share Everything Discount % = '+IDV4.Incremental_Share_Everything_Discount__c+' <br/><br/>'; 
                body +=  'Total Share Everything Discount = '+IDV4.Total_Share_Everything_Discount__c+' <br/><br/>'; 
                body +=  'Individually Liable MSD Code = '+IDV4.Individually_Liable_MSD_Code__c+' <br/><br/>'; 
                body +=  'Your attention required for below pending tasks <br/><br/>'; 
                body +=  '&nbsp;&nbsp; a. Load the Share Everything Discount and any other promotions/incentives/credits <br/><br/>'; 
                body +=  '&nbsp;&nbsp; b. Update the “Date Share Everything Discount Loaded” field within the IDV Pricing Request <br/><br/>'; 
                body +=  '&nbsp;&nbsp; c. Generate the promotional letter(s) in the Rogers Contract Tool  <br/><br/>'; 
                body +=  '&nbsp;&nbsp; d. Update the "Status" field within the IDV Pricing Request to "With EVP Team" <br/><br/>';               
              
                body += 'Click below  link for more details : <br/><br/>';
                body += URL.getSalesforceBaseUrl().toExternalForm() +'/'+IDV4.id+' <br/><br/>';
                 
                body += 'Please find the attached csv for IDV request details .<br/><br/>';    

                
                body += '<br/> <br/> Regards, <br/> Salesforce Team'; 
                email.setToAddresses(toAddresses); 
                email.setSubject('Individually Liable (IDV) Pricing Requests - Action Required ('+IDV4.Company_Name__c+')');        
                email.setSaveAsActivity(false);  
                email.setHtmlBody(body); 
               // email.setFrOrgWideEmailAddressId(orgEmailId.id);
                email.setSenderDisplayName (IDV4.Case_Owner__c);
                email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});  
                          
                          }
   

}


public void shareRecords(set<id> idvReqIds,boolean isContractTeam){


        List<IDV_Pricing_Request__c>  idvReqs = new List<IDV_Pricing_Request__c>([SELECT Id,EVP_Prime__c,Contracts_Prime__c FROM IDV_Pricing_Request__c WHERE id in : idvReqIds]);
        List<IDV_Pricing_Request__Share>  idvShareRecords = new List <IDV_Pricing_Request__Share>();
        
            for(IDV_Pricing_Request__c idv :idvReqs){
            IDV_Pricing_Request__Share  iDVShare = new IDV_Pricing_Request__Share();
               
                iDVShare.ParentId  =  idv.id  ; 
                if(isContractTeam) 
                 iDVShare.UserOrGroupId =idv.Contracts_Prime__c;   
                 else    
                 iDVShare.UserOrGroupId =idv.EVP_Prime__c;
                iDVShare.AccessLevel='Edit';
                idvShareRecords.add(iDVShare);
                } 
                
                if(idvShareRecords.size()>0)
                
                try{
                
                insert  idvShareRecords; 
                
                }Catch(Exception e){
                  system.debug('Error share record mostly becuase of user may be the record owner ');
                }            
}

public void deleteShare(set<id> parentIds,set<id> userIds){

 List<IDV_Pricing_Request__Share> shareRecords = new List<IDV_Pricing_Request__Share>([SELECT Id FROM IDV_Pricing_Request__Share  where ParentId in :parentIds and UserOrGroupId in :userIds ]);
           system.debug('Share records to delete '+shareRecords.size());
         if(shareRecords.size()>0){
         
          try{
         delete shareRecords;
           }Catch(Exception e){
                  system.debug('Error delete share record mostly becuase of user may be the record owner ');
                }   
         }
}




}