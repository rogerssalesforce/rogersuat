/*
===============================================================================
 Class Name   : itRequirements_batch_test
===============================================================================

Jacob Klay, 11/11/2014

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
===============================================================================
*/
@isTest (SeeAllData = true)
private class itRequirements_batch_test{
    static IT_Requirement__c req;
    static User aUser = [select id, user.profile.name, user.email from User where user.profile.name= 'System Administrator' and isActive = true limit 1];
 
    static testmethod void testMethod1(){
        System.runAs(aUser) {
            req = new IT_Requirement__c();
            req.Name='Test Requirement';
            req.OwnerId = aUser.id;
            req.Type__c = 'Enhancement';
            req.Status__c = 'In Progress';
            req.Summary__c = 'JK did this!';
            req.Progress__c = 0.25;
            req.Due_Date__c = System.Date.today().addDays(2);
            req.BA_Prime__c = aUser.id;
            req.Development_Prime__c = aUser.id;
            req.Start_Date__c = System.Date.today();
            //Optional
            //req.Actual_Closed_Date__c = ;
            insert req;
        }
        System.runAs(aUser) {
            Database.BatchableContext context;
            itRequirements_batch apexBatch = new itRequirements_batch();           
            Test.StartTest();
            apexBatch.Query ='SELECT Name,Type__c,Status__c,Summary__c,Progress__c,Due_Date__c,BA_Prime__r.Name,Development_Prime__r.Name,Start_Date__c,Actual_Closed_Date__c FROM IT_Requirement__c WHERE name =\''+ req.Name+ '\' ';
            //apexBatch.toAddresses = new List<String> { aUser.email };
            apexBatch.start(context);
            List<IT_Requirement__c> scope = [SELECT Name,Type__c,Status__c,Summary__c,Progress__c,Due_Date__c,BA_Prime__r.Name,Development_Prime__r.Name,Start_Date__c,Actual_Closed_Date__c FROM IT_Requirement__c WHERE name =: req.Name];
            apexBatch.execute(context,scope);
            apexBatch.finish(context);
            Test.StopTest();
        }
    }       
}