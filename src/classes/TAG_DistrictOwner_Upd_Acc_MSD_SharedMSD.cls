/*Class Name  : TAG_DistrictOwner_Upd_Acc_MSD_SharedMSD 
*Description : This Class will support the logic for District Owner before Update Trigger.
*Created By  : Jayavardhan.
*Created Date :27/01/2015.
* Modification Log :
------------------------------------------------------------------------------------------------------------------------------
                Developer         Version       Date        Description
                Jayavardhan         1.0       27/01/2015    Get list of all Account,MSD,Shared MSD for a District Owner and 
                                                            update them with owner and set a flag which is used for MAL integration 
------------------------------------------------------------------------------------------------------------------------------
*
*/
Public Class TAG_DistrictOwner_Upd_Acc_MSD_SharedMSD  {
    
    Public Static void UpdatedDistrictOwner(Map<ID,District__c> mapIdDistrict){       
        //get all Account asociated to set of District Owners
        List<Account> accountUpdateList = new List<Account>();
        set<ID> accountIds = new set<ID>();
        List<Account> accountOwnerUpdates = [select id,OwnerId,District__c,Account_Owner_to_MAL__c,District__r.District_Owner__c from Account where District__c IN : mapIdDistrict.keyset()];
        
        if(!accountOwnerUpdates.IsEmpty()){
            for(Account accRec:accountOwnerUpdates){
                if(accRec.District__r.District_Owner__c!=null){
                    accRec.OwnerId =accRec.District__r.District_Owner__c;
                }
                accRec.Account_Owner_to_MAL__c = TRUE;
                accRec.Recalculate_Team__c = TRUE;                
                accountIds.add(accRec.Id);
            }
        }
        system.debug('==========>accountOwnerUpdates.size'+accountOwnerUpdates.size());
        if(!accountOwnerUpdates.IsEmpty()){
            accountUpdateList.addAll(accountOwnerUpdates);            
        }
        
        //get all MSD Codes asociated to set of District Owners
        List<MSD_Code__c> msdOwnerUpdates = [Select Id,OwnerId, MSD_to_MAL__c,District__c,Account__r.Recalculate_Team__c,District__r.District_Owner__c From MSD_Code__c where District__c IN: mapIdDistrict.keyset()];
        
        
        
        if(!msdOwnerUpdates.isEmpty()){
            for(MSD_Code__c msdRec:msdOwnerUpdates){       
                if(msdRec.District__r.District_Owner__c!=null){
                    msdRec.OwnerId = msdRec.District__r.District_Owner__c;
                }
                msdRec.MSD_to_MAL__c = TRUE;
                msdRec.Account__r.Recalculate_Team__c = TRUE;                
            }
        }   
        
        if(!msdOwnerUpdates.isEmpty()){
            update msdOwnerUpdates;
            system.debug('==========>msdOwnerUpdates'+msdOwnerUpdates);
        }
        
        //get all Shared MSD Codes asociated to set of District Owners
        List<Shared_MSD_Code__c> sharedmsdOwnerUpdates =[Select Shared_MSD_to_MAL__c, OwnerId, Id, District__r.District_Owner__c, District__c From Shared_MSD_Code__c where District__c IN: mapIdDistrict.keyset()];
        
        
        if(!sharedmsdOwnerUpdates.isEmpty()){
            for(Shared_MSD_Code__c sharedmsdRec:sharedmsdOwnerUpdates){ 
                if(sharedmsdRec.District__r.District_Owner__c!=null){
                    sharedmsdRec.OwnerId = sharedmsdRec.District__r.District_Owner__c;
                }
                sharedmsdRec.Shared_MSD_to_MAL__c = TRUE;
                
            }
        }
        system.debug('==========>sharedmsdOwnerUpdates'+sharedmsdOwnerUpdates.size());
        if(!sharedmsdOwnerUpdates.isEmpty()){
            update sharedmsdOwnerUpdates;
            system.debug('==========>sharedmsdOwnerUpdates.size'+sharedmsdOwnerUpdates.size());
        }
        
        //get list of accounts associated with Shared MSD Code object in order to set the Recalculate_Team__c boolean flag to True      
        if(!sharedmsdOwnerUpdates.IsEmpty()){     
            for(Shared_MSD_Accounts__c lstMSDAccount :[select id,Account__c,Account__r.Id,Account__r.Recalculate_Team__c,Shared_MSD_Code__c from Shared_MSD_Accounts__c where Shared_MSD_Code__c IN:sharedmsdOwnerUpdates]){
               
               if(!accountIds.contains(lstMSDAccount.Account__c)){
                lstMSDAccount.Account__r.Recalculate_Team__c = TRUE;
                accountUpdateList.add(lstMSDAccount.Account__r); 
               }    
            }
        }
        /*
        if(!accountUpdateList.isEmpty()){         
            update accountUpdateList;            
        }
        */
        Set<Account> setAcctoUpdate = new Set<Account>();
        if(!accountUpdateList.isEmpty()){
            setAcctoUpdate.addAll(accountUpdateList);
            accountUpdateList.clear();
            accountUpdateList.addAll(setAcctoUpdate);
            update accountUpdateList;
        }
        
    }
}