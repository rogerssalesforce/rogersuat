/**********************************************************************************
Class Name    : CPQ_createCOPandCOPLineItemsCont
Description     : Controller Class for vf page "CPQ_createCOPandCOPLineItems" 
                  Class to create COP and COP line Items For respective Agreement line Items. (US - 0233)
                  Copying the COP Line Items of an existing Wireless COP record if it exists for the related account (US - 0350)
Created By      : Aakanksha Patel
Created Date    : 19-Nov-15 
Modification Log:
---------------------------------------------------------------------------------- 
Developer               Date                   Description
----------------------------------------------------------------------------------            
Aakanksha Patel        19-Nov-15              Initial Version
Akshay Ahluwalia       06-Jan-16              Added Fields to be defaulted on COP Creation
**********************************************************************************/
public with sharing class CPQ_createCOPandCOPLineItemsCont{

    public Id oldCOPId;
    public Id wirelessCOPId;
    public COP_Line_Item__c oCopLINew;
    public Set<id> setOldCOPIds = new Set<id>();
    public Map<Id, Id> mapNewCOPandOldCOPId = new Map<Id, Id>();
    public List<COP_Line_Item__c> lstCOPLineItemsOld = new List<COP_Line_Item__c>();
    public String sAgreementId;
    public COP__c sCOPRec;
    public COP_Line_Item__c sCOPLineItemRec;
    public List<Apttus__APTS_Agreement__c> lstAgreement;
    public List<Apttus__AgreementLineItem__c> lstAgreementLineItems;
    public List<COP_Line_Item__c> lstCOPLineItems = new List<COP_Line_Item__c>();
    public Map <String, String> mapCOPTeamRecType = new Map <String, String> ();
    public Map<String, ID> mapRecNameAndCOPid = new Map<String, ID>();
    public Map<String, ID> mapRecNameAndIdCOPLI = new Map<String, ID>();
    public Map<String, COP__c> mapCOPTeamCOP = new  Map<String, COP__c>();
    public List<CPQ_COPTeamValues__c> CPQ_COPsetting = CPQ_COPTeamValues__c.getAll().values();
     
    /*********************************************************************************
    Method Name    : CPQ_createCOPandCOPLineItemsCont
    Description    : Constructor method
    Return Type    : 
    Parameter      : 
    *********************************************************************************/
    public CPQ_createCOPandCOPLineItemsCont() 
    {
        sAgreementId = ApexPages.currentPage().getParameters().get('id'); 
         
        //Iterating through the Custom Setting values for mapping of "COP Team" and "RecordType" of COP
        for(CPQ_COPTeamValues__c cpqSetting: CPQ_COPsetting)
        {
            mapCOPTeamRecType.put(cpqSetting.COP_Team__c, cpqSetting.Record_Type_Developer_Name__c);
        }
          
        //List of Agreement Line Items for which COp and COP Line Items will be created
        lstAgreementLineItems= [select id,Name, Apttus__AgreementId__c, IsCustomProduct__c, Apttus__AgreementId__r.Apttus__Account__r.Latest_Wireless_COP__c , Apttus__ProductId__r.COP_Team__c, Apttus_CMConfig__BillingFrequency__c,Apttus__ExtendedPrice__c, Apttus_CMConfig__LineType__c, Apttus__ListPrice__c, Apttus_CMConfig__PrimaryLineNumber__c,Apttus__ProductId__r.Name, Apttus__ProductId__c,Apttus__ProductId__r.ProductCode, Apttus__Quantity__c, Apttus__AgreementId__r.Apttus__Account__r.Root_BAN__c, rd_Party_Services__c, COPTeam__c, CSOW_Required__c, CustomProductName__c, TypeOfProduct__c, Expiration_Date__c, LineofBusiness__c, Presentable_on_Quote__c, ProspectSite__c, RelatedProduct__c, OverageRate__c, RevenueType__c, FulfilmentType__c, Eligible_Device_s__c, DataCenter__c from Apttus__AgreementLineItem__c where Apttus__AgreementId__c=:sAgreementId  AND Apttus__ProductId__r.COP_Team__c IN :mapCOPTeamRecType.keySet()];
     
        List<RecordType> lstRecType = [SELECT Id,DeveloperName FROM RecordType WHERE SobjectType = 'COP__c' AND DeveloperName in : mapCOPTeamRecType.values()];
        List<RecordType> lstRecTypeCOPLI = [SELECT Id,DeveloperName FROM RecordType WHERE SobjectType = 'COP_Line_Item__c' AND DeveloperName in : mapCOPTeamRecType.values()];
        
        //Creating map of Record Type of COP(from Custom Setting) and COP RecType ID
        for(RecordType rec: lstRecType)
        {
            mapRecNameAndCOPid.put(rec.DeveloperName, rec.id);
        }
        //Creating map of Record Type of COP Line Items(from Custom Setting) and COP Line Item RecType ID 
        for(RecordType recCOPLI: lstRecTypeCOPLI)
        {
            mapRecNameAndIdCOPLI.put(recCOPLI.DeveloperName, recCOPLI.id);
        }
    }

    /*********************************************************************************
    Method Name    : createCOPRec
    Description    : This method creates COP record
    Return Type    : COP__c
    Parameter      : String,ID,String
    *********************************************************************************/
    public COP__c createCOPRec(String COPTeam, ID AgreementId, String RootBAN )
    {
        COP__c COPrec;
        ID RecType;
        RecType = mapRecNameAndCOPid.get(mapCOPTeamRecType.get(COPTeam));
        lstAgreement = [select id, Apttus__Account__c, Apttus__Account__r.Voice_Data_Line_CAP__c,  Apttus__Account__r.Data_Only_Line_CAP__c, Apttus__Account__r.Credit_Limit2__c from Apttus__APTS_Agreement__c where id = :AgreementId];
        
        COPrec = new COP__c();
        for(Apttus__APTS_Agreement__c agreementRec : lstAgreement ){
            COPrec.Agreement__c = AgreementId;
            COPrec.RecordTypeID = RecType;
            COPrec.COP_Status__c = 'Pending';
            COPrec.Company_Name__c = agreementRec.Apttus__Account__c;
            COPrec.Voice_Data_Line_CAP__c = agreementRec.Apttus__Account__r.Voice_Data_Line_CAP__c;
            COPrec.Data_Only_Line_CAP__c = agreementRec.Apttus__Account__r.Data_Only_Line_CAP__c;
            COPrec.Credit_Limit__c = agreementRec.Apttus__Account__r.Credit_Limit2__c;
            if(RootBAN!=null){
                COPrec.Root_BAN__c = RootBAN;
                }
            }
        return COPrec;
        
    }
  
    /*********************************************************************************
    Method Name    : InsertCOPRecordsNew
    Description    : This method inserts COP record and creates a map of COPTEam and COP Rec
    Return Type    : 
    *********************************************************************************/
    public void InsertCOPRecordsNew()
    {
        //Check for the existance of Agreement and Agreement Line Items
        if(sAgreementId != null && lstAgreementLineItems.size() > 0)
        {
            //Looping through the Agreement Line Items
            for(Apttus__AgreementLineItem__c agreementLI: lstAgreementLineItems)
            {
                if(mapCOPTeamCOP.get(agreementLI.Apttus__ProductId__r.COP_Team__c)==null)
                {
                    mapCOPTeamCOP.put(agreementLI.Apttus__ProductId__r.COP_Team__c,createCOPRec(agreementLI.Apttus__ProductId__r.COP_Team__c, sAgreementId, agreementLI.Apttus__AgreementId__r.Apttus__Account__r.Root_BAN__c));
                }
            }
            //Exception Handling if COP records are not created
            try
            {
                if(mapCOPTeamCOP.values()!=null)
                    insert mapCOPTeamCOP.values();
            }
            catch(Exception e)
            {
                  ApexPages.Message ErrorMsg = new ApexPages.Message(ApexPages.Severity.FATAL,Label.CPQ_ErrorCreatingCOPandCOPLI); 
                  ApexPages.addMessage(ErrorMsg); 
            }
            
        }
    }
  
  /*********************************************************************************
    Method Name    : createCOP
    Description    : 1) This method creates COP record and COP Line Items for the respective Agreement (US - 0233)
                     2) Copying the COP Line Items of an existing Wireless COP record if it exists for the related account (US - 0350)
    Return Type    : 
    Parameter      : nil
    *********************************************************************************/
    public PageReference createCOP()
    {
        //Check for the existance of Agreement and Agreement Line Items
        if(sAgreementId != null && lstAgreementLineItems.size() > 0)
        {
            //Calling the method to create the COP Records
            InsertCOPRecordsNew();
            
            //Looping through the Agreement Line Items
            for(Apttus__AgreementLineItem__c agreementLI: lstAgreementLineItems)
            {   
                    sCOPLineItemRec = new COP_Line_Item__c();
                    //Assigning COP Line Items to the Respective COP Records
                    if(mapCOPTeamCOP.get(agreementLI.Apttus__ProductId__r.COP_Team__c)!=null)
                    {
                        sCOPRec = mapCOPTeamCOP.get(agreementLI.Apttus__ProductId__r.COP_Team__c);
                        if(sCOPRec!=null)
                            sCOPLineItemRec.COP__c = sCOPRec.id;
                    }
                    
                    //Populating the COP Line Item Field values
                    sCOPLineItemRec.RecordTypeID = mapRecNameAndIdCOPLI.get(mapCOPTeamRecType.get(agreementLI.Apttus__ProductId__r.COP_Team__c));
                    sCOPLineItemRec.Billing_Frequency__c = agreementLI.Apttus_CMConfig__BillingFrequency__c;
                    sCOPLineItemRec.Extended_Price__c = agreementLI.Apttus__ExtendedPrice__c;
                    sCOPLineItemRec.Line_Type__c = agreementLI.Apttus_CMConfig__LineType__c;
                    sCOPLineItemRec.List_Price__c = agreementLI.Apttus__ListPrice__c;
                    sCOPLineItemRec.Primary_Line_Number__c = agreementLI.Apttus_CMConfig__PrimaryLineNumber__c;
                    sCOPLineItemRec.Product_Name__c = agreementLI.Apttus__ProductId__c;
                    sCOPLineItemRec.Product_ID__c = agreementLI.Apttus__ProductId__c;
                    sCOPLineItemRec.SOC_Code__c = agreementLI.Apttus__ProductId__r.ProductCode;
                    sCOPLineItemRec.Quantity__c = agreementLI.Apttus__Quantity__c;
                    sCOPLineItemRec.Is_Custom_Product__c = agreementLI.IsCustomProduct__c;
                    sCOPLineItemRec.rd_Party_Services__c = agreementLI.rd_Party_Services__c;
                    sCOPLineItemRec.Presentable_on_Quote__c = agreementLI.Presentable_on_Quote__c;
                    sCOPLineItemRec.COPTeam__c = agreementLI.COPTeam__c;
                    sCOPLineItemRec.CSOW_Required__c= agreementLI.CSOW_Required__c;
                    sCOPLineItemRec.CustomProductName__c= agreementLI.CustomProductName__c;
                    sCOPLineItemRec.LineofBusiness__c= agreementLI.LineofBusiness__c;
                    sCOPLineItemRec.ProspectSite__c= agreementLI.ProspectSite__c;
                    sCOPLineItemRec.RelatedProduct__c= agreementLI.RelatedProduct__c;
                    sCOPLineItemRec.RevenueType__c= agreementLI.RevenueType__c;
                    sCOPLineItemRec.FulfilmentType__c = agreementLI.FulfilmentType__c;
                    sCOPLineItemRec.Eligible_Device_s__c = agreementLI.Eligible_Device_s__c;
                    sCOPLineItemRec.Data_Centre__c = agreementLI.DataCenter__c;
                    lstCOPLineItems.add(sCOPLineItemRec);
            }
            
            List<RecordType> COPRecTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Contracts_Team_Wireless' AND SobjectType = 'COP__c'];
            List<RecordType> COPLIRecTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Contracts_Team_Wireless' AND SobjectType = 'COP_Line_Item__c'];
            
            //Null check if COp records are being created.
            if(mapCOPTeamCOP.values()!=null)
            {
                //Iterating through the COP records to check if wireless COP record exists
                for(COP__c oCOPRec: mapCOPTeamCOP.values())
                {
                    //Check if a new wireless COP record is being created and if a Wireless COp already exists 
                    if(oCOPRec.RecordTypeId == COPRecTypeId[0].id)
                    {
                        if(lstAgreementLineItems[0]!=null)
                        if(lstAgreementLineItems[0].Apttus__AgreementId__r.Apttus__Account__r.Latest_Wireless_COP__c!=null)
                        {
                        oldCOPId = lstAgreementLineItems[0].Apttus__AgreementId__r.Apttus__Account__r.Latest_Wireless_COP__c;
                        setOldCOPIds.add(oldCOPId);
                        mapNewCOPandOldCOPId.put(oCOPRec.id,oldCOPId);
                        wirelessCOPId = oCOPRec.id;
                        }
                    }
                }
            }
            
            //If Wireless COP exists for the related account then copy the COP line Items 
            if(wirelessCOPId!=null)
            {
                lstCOPLineItemsOld = [Select id, COP__c,Name, Account_Name__c, Billing_Frequency__c,Billing_Start_Date__c, Billing_Term__c, Data_Centre__c, Discount__c, Extended_Price__c, Is_Custom_Product__c, Line_Commitment__c, Line_Type__c, Quantity__c, List_Price__c, MSD_Code__c, MSD_Placeholder_BAN__c, MSD_Type__c, Overage_Rate__c, Primary_Line_Number__c, Product_Name__c, Product_ID__c, Quote_ID__c, Revenue_Type__c, Term__c, Unit_of_Measure__c, SOC_Code__c from COP_Line_Item__c where COP__c in: setOldCOPIds];
                
                //Copying the COP Line Items from Old COP to New COP record
                for(COP_Line_Item__c copLIRec : lstCOPLineItemsOld)
                {
                    oCopLINew = new COP_Line_Item__c();
                    oCopLINew = copLIRec.clone(false, false, false, false);
                    oCopLINew.COP__c = wirelessCOPId;
                    oCopLINew.RecordTypeId = COPLIRecTypeId[0].id;
                    lstCOPLineItems.add(oCopLINew);
                }
            }
            
            if(lstCOPLineItems.size()>0)
            {   //Exception Handling if COP Line Item records are not created
                try
                {
                    if(lstCOPLineItems!=null)
                        insert lstCOPLineItems;
                }
                catch(Exception e)
                {
                    ApexPages.Message ErrorMsg = new ApexPages.Message(ApexPages.Severity.FATAL,Label.CPQ_ErrorCreatingCOPandCOPLI); 
                    ApexPages.addMessage(ErrorMsg); 
                }
            }
        }
        PageReference pf = new PageReference('/'+sAgreementId );
        return pf;
    }
    
    /*********************************************************************************
    Method Name    : backToAgreement
    Description    : Returns back to Agreement
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference backToAgreement(){
        PageReference refreshPage = new PageReference(Site.getPathPrefix()+'&id='+sAgreementId);
        return refreshPage;
    }
}