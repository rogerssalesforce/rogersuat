/*
On Update service Information
1.  Persist the Service Information - serviceInfo 
2.  Remove any QuoteRequestService__c records that are linked to the serviceInfo
3.  Get the Selected Services - and create/persist the QuoteRequestService__c.
*/

public without sharing class LocationsController {

    public List<Location_A__c> aLocations {get; set;}
    public List<Location_Z__c> zLocations {get; set;}
    public List<Site__c> aSites {get; set;}
    public List<Site__c> zSites {get; set;}
    public Map<Id, List<ServiceWrapper>> siteA_QLIMap {get; set;}
    public Map<Id, List<ServiceWrapper>> siteZ_QLIMap {get; set;}
  public List<QuoteRequestService__c> qServices {get; set;}
    public List<ServiceWrapper> servicesA {get; set;}
    public List<ServiceWrapper> servicesZ {get; set;}
    public String quoteRequestId {get; set;}
  public Boolean allowServiceSelection {get; set;}
  public ServiceRequestInfo__c serviceInfo {get; set;}
  public Quote_Request__c qRequest {get; set;}
  public Boolean displayCircuitId {get; set;}

    public LocationsController (ApexPages.StandardController stdController) {
        quoteRequestId = stdController.getId();
        siteZ_QLIMap = new Map<Id, List<ServiceWrapper>>();
        siteA_QLIMap = new Map<Id, List<ServiceWrapper>>();
        
        allowServiceSelection = true;
        User currentUser = [SELECT Id, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
        
        if (!(currentUser.Profile.Name.startsWith('Network Design Consultant') || currentUser.Profile.Name.equals('System Administrator') || currentUser.Profile.Name.equals('Serviceability Team'))){
          allowServiceSelection = false;
        }
        
        
        serviceInfo = new ServiceRequestInfo__c();
        
        
        try{
          aLocations = [SELECT id, Prospect_Site__c, Prospect_Site__r.Display_Name__c FROM Location_A__c WHERE Quote_Request__c = :quoteRequestId];
        }catch(Exception ex){
          aLocations = new List<Location_A__c>();
        }
        
        try{
          zLocations = [SELECT id, Prospect_Site__c, Prospect_Site__r.Display_Name__c FROM Location_Z__c WHERE Quote_Request__c = :quoteRequestId];
        }catch(Exception ex){
          zLocations = new List<Location_Z__c>();
        }
        
        qRequest = [SELECT id 
          ,Quote__c
          ,Request_Type__c 
          ,Quoting_Stage__c
          ,(select Id, Name, Service__c, Service__r.Id, Quote_Request__c, ServiceRequestInfo__c from QuoteRequestServices__r)
           FROM Quote_Request__c WHERE id = :quoteRequestId];
        
        if (currentUser.Profile.Name.startsWith('Network Design Consultant') && ((qRequest.Quoting_Stage__c == 'Closed by Quote Desk') || (qRequest.Quoting_Stage__c == 'Submitted by SE'))){
          allowServiceSelection = false;
        }
        
        if (qRequest.Quoting_Stage__c == 'Closed by Quote Desk'){
          allowServiceSelection = false;
        }
        
        if (qRequest.Request_Type__c != null && (qRequest.Request_Type__c.equals('Renewal') || qRequest.Request_Type__c.equals('MAC-D') || qRequest.Request_Type__c.equals('Upgrade')))
      displayCircuitId = true;
    else
      displayCircuitId = false;

        prepareServiceMaps();
    }
    
    public PageReference updateCurrentService() {
      String serviceId = Apexpages.currentPage().getParameters().get('service');
      Id srId = null;
      
      for(QuoteRequestService__c qs : this.qServices) {
          if(qs.Service__c == serviceId) {
              srId = qs.ServiceRequestInfo__c;
          }
      }
      System.debug('Found it: ' + srId);
      if (srId != null){
        serviceInfo = [SELECT Access__c,
         Circuit_ID__c,
         CoS__c,
         CreatedById,
         CreatedDate,
         IsDeleted,
         Diversity__c,
         EVC__c,
         EVC_Size__c,
         EVPL_EPL__c,
         LastModifiedById,
         LastModifiedDate,
         MTU_Size__c,
         NNI__c,
         NPA__c,
         NPA_NXX__c,
         NXX__c,
         EVC_Size_Other__c,
         Other_EVC_Size__c,
         OwnerId,
         Port__c,
         Port_Size__c,
         Port_Type__c,
         Product_Type__c,
         Id,
         Other_Cos__c,
         Notes__c,
         Name
          FROM ServiceRequestInfo__c WHERE id = :srId];
      } else {
        serviceInfo = new ServiceRequestInfo__c();
      }
     
       prepareServiceMaps();
     
      return null;
      
    }
    
    public void prepareServiceMaps(){
      
      qRequest = [SELECT id 
          ,Quote__c
          ,Quoting_Stage__c
          ,(select Id, Name, Service__c, Service__r.Id, Quote_Request__c, ServiceRequestInfo__c from QuoteRequestServices__r)
           FROM Quote_Request__c WHERE id = :quoteRequestId];
      
      qServices = new List<QuoteRequestService__c>();
        
        if (qRequest != null && qRequest.QuoteRequestServices__r != null){
            qServices = qRequest.QuoteRequestServices__r;
        }
        
        System.debug('qServices now: ' + qServices);
        
        siteZ_QLIMap = new Map<Id, List<ServiceWrapper>>();
        siteA_QLIMap = new Map<Id, List<ServiceWrapper>>();
          
        
        Set<Id> siteAIds = new Set<Id>();
        for (Location_A__c a : aLocations){
            siteAIds.add(a.Prospect_Site__c);
        }
        
        aSites = [SELECT id, Display_Name__c FROM Site__c WHERE Id IN :siteAIds];
        // init the map
        for (Site__c s : aSites){
            siteA_QLIMap.put(s.Id, new List<ServiceWrapper>());
        }
        
        
        Set<Id> siteZIds = new Set<Id>();
        for (Location_Z__c z : zLocations){
            siteZIds.add(z.Prospect_Site__c);
        }
        
        zSites = [SELECT id, Display_Name__c FROM Site__c WHERE Id IN :siteZIds];
        // init the map
        for (Site__c s : zSites){
            siteZ_QLIMap.put(s.Id, new List<ServiceWrapper>());
        }
        
        List<QuoteLineItem> qliAs = [SELECT id, Site__c, Site__r.Display_Name__c, PriceBookEntry.Product2.Access_Type__c, PriceBookEntry.Product2.Access_Type_Group__c, PriceBookEntry.Name, Service_Type__c FROM QuoteLineItem WHERE Site__c IN :siteAIds AND QuoteId = :qRequest.Quote__c];
        List<QuoteLineItem> qliZs = [SELECT id, Site__c, Site__r.Display_Name__c, PriceBookEntry.Product2.Access_Type__c, PriceBookEntry.Product2.Access_Type_Group__c, PriceBookEntry.Name, Service_Type__c FROM QuoteLineItem WHERE Site__c IN :siteZIds AND QuoteId = :qRequest.Quote__c];
        
        
        // Need to traverse the aLineItems and place them in the wrapper
        servicesA = new List<ServiceWrapper>();
        
        for(QuoteLineItem qli : qliAs) {
           Boolean selected = false;
           Boolean isActive = false;
            if (qServices!= null){
                for (QuoteRequestService__c qs : qServices){
                    if (qs.Service__r.Id == qli.Id){
                        selected = true;
                        if (qs.ServiceRequestInfo__c == serviceInfo.Id)
                          isActive = true;
                    }
                }
            }
            Boolean isDisabled = (qli.PriceBookEntry.Product2.Access_Type__c == null) || !(qli.PriceBookEntry.Product2.Access_Type__c).toLowerCase().contains('off net') || !allowServiceSelection;
            servicesA.add(new ServiceWrapper(qli, selected, isDisabled, isActive));
        }
        
        
        // Need to traverse the zLineItems and place them in the wrapper
         servicesZ = new List<ServiceWrapper>();
          
        for(QuoteLineItem qli : qliZs) {
           Boolean selected = false;
           Boolean isActive = false;
            if (qServices!= null){
                for (QuoteRequestService__c qs : qServices){
                    if (qs.Service__r.Id == qli.Id){
                        selected = true;
                        if (qs.ServiceRequestInfo__c == serviceInfo.Id)
                          isActive = true;
                        
                    }
                        
                }
            }
            
            Boolean isDisabled = (qli.PriceBookEntry.Product2.Access_Type__c == null) || !(qli.PriceBookEntry.Product2.Access_Type__c).toLowerCase().contains('off net') || !allowServiceSelection;
            servicesZ.add(new ServiceWrapper(qli, selected, isDisabled, isActive));
        }
        
        for (ServiceWrapper sA : servicesA){
            List<ServiceWrapper> lineItems = siteA_QLIMap.get(sA.qli.Site__c);
            if (lineItems == null){
                lineItems = new List<ServiceWrapper>();
            }
            
            lineItems.add(sA);
            siteA_QLIMap.put(sA.qli.Site__c, lineItems);
        }
        
        for (ServiceWrapper sZ : servicesZ){
            List<ServiceWrapper> lineItems = siteZ_QLIMap.get(sZ.qli.Site__c);
            if (lineItems == null){
                lineItems = new List<ServiceWrapper>();
            }
            
            lineItems.add(sZ);
            siteZ_QLIMap.put(sZ.qli.Site__c, lineItems);
        }
        
        System.debug('siteZ_QLIMap: ' + siteZ_QLIMap);
        System.debug('siteA_QLIMap: ' + siteA_QLIMap);
    }
    
    public PageReference processSelectedServices() {
        
        try{
          /*
            1.  Persist the Service Information - serviceInfo 
        2.  Remove any QuoteRequestService__c records that are linked to the serviceInfo
        3.  Get the Selected Services - and create/persist the QuoteRequestService__c.
          */
          
          /* Let's obtain which rows are currently selected - if there are none then we delete this serviceInfo */
          List<QuoteLineItem> selectedServices= new List<QuoteLineItem>();
            
            for(ServiceWrapper sw : this.servicesA) {
                if(sw.isActive) {
                    selectedServices.add(sw.qli);
                }
            }
            
            for(ServiceWrapper sw : this.servicesZ) {
                if(sw.isActive) {
                    selectedServices.add(sw.qli);
                }
            }
          
          /* If there are none we will delete the serviceInfo */
          if (selectedServices.size() > 0)
            UPSERT serviceInfo;
          else 
            DELETE serviceInfo;
          
          if (serviceInfo.Id != null){
            /* Delete the existing services that are liked to the info object*/
            List<QuoteRequestService__c> expiredQServices = [SELECT Id FROM QuoteRequestService__c WHERE Quote_Request__c = :quoteRequestId AND ServiceRequestInfo__c = :serviceInfo.Id];
            DELETE expiredQServices;
            
            /* Set the objects and persist them */
            this.qServices = new List<QuoteRequestService__c>();
          
            
            
              for(QuoteLineItem qli : selectedServices){
                  QuoteRequestService__c qs = new QuoteRequestService__c();
                  qs.Quote_Request__c = quoteRequestId;
                  qs.Service__c = qli.Id;
                  qs.ServiceRequestInfo__c = serviceInfo.Id;
                
                this.qServices.add(qs);
              }
              
              INSERT qServices;
              
              
          } 
          
          prepareServiceMaps();  
        }catch(Exception ex){
          
        }
        
        return null;
    }
    
    
    public class ServiceWrapper{
        public QuoteLineItem qli {get; set;}
      public Boolean hasInfo;  
      public Boolean isDisabled;
      public Boolean isActive;       
        
        public void sethasInfo(Boolean b) { this.hasInfo = b; }
      public Boolean gethasInfo() { return hasInfo; }
      
      public void setIsDisabled(Boolean b) { this.isDisabled = b; }
      public Boolean getIsDisabled() { return isDisabled; }
        
        public void setIsActive(Boolean b) { this.isActive = b; }
      public Boolean getIsActive() { return isActive; }
        
        public ServiceWrapper() {
            // constructor
        }
        
        public ServiceWrapper(QuoteLineItem qli) { 
          this.qli = qli;
          this.hasInfo = false;
          this.isActive = false; 
          this.isDisabled = false;
      }
      /*
      public ServiceWrapper(QuoteLineItem qli, Boolean hasInfo, Boolean isActive) {
          this.qli = qli;
          this.hasInfo = hasInfo;
          this.isActive = isActive;
          this.isDisabled = false;
      }
      */
      
      /* hasInfo and isCurrent */
      public ServiceWrapper(QuoteLineItem qli, Boolean hasInfo, Boolean isDisabled, Boolean isActive) {
          this.qli = qli;
          this.hasInfo = hasInfo;
          this.isActive = isActive;
          this.isDisabled = isDisabled;
      }
        
    }
}