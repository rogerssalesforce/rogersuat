public with sharing class ServiceableLocationWrapper {
 	public Id candidateSiteId {get; set;}
 	ServiceableLocation__c s;
    Boolean isSelected;
    
    
    public ServiceableLocationWrapper(ServiceableLocation__c s) {
    	this.s = s;
        isSelected = false;
    }
        
    public ServiceableLocation__c getServiceableLocation() { return s; }
    public Boolean getIsSelected() { return isSelected; }
    public void setIsSelected(Boolean b) { this.isselected = b; }
}