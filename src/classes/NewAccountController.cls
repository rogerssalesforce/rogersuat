public class NewAccountController {

    public NewAccountController(ApexPages.StandardController controller) {
        this.controller = controller;
    }
    
    public PageReference save(){
    	if (duplicateCheck()){
    		String hostname = ApexPages.currentPage().getHeaders().get('Host');
    		String absolutePath = 'https://' + hostname + '/apex/duplicateAccount';
    		PageReference helpDeskPage = new PageReference(absolutePath);
    		helpDeskPage.setRedirect(true);
    		return helpDeskPage;
    	}
    	
    	return controller.save();
    }
    
    private boolean duplicateCheck(){
    	boolean duplicate = true;
    	
    	List<Account> accounts = new List<Account>();
    	Account acc = (Account)controller.getRecord();
        
        System.debug('\n\n\n\n\nAccount: ' + acc);
        try{	
    		accounts = [SELECT Id, Name, BillingPostalCode FROM Account WHERE ((BillingPostalCode <> NULL AND BillingPostalCode = :acc.BillingPostalCode) OR Name = :acc.Name) AND Id != :acc.Id];
        }catch(Exception ex){
        	System.debug('We have a Duplicate');
        }
    	
    	System.debug('\n\n\n\n\naccounts: ' +accounts);
    	
    	if (accounts == null || accounts.size() == 0)
    		duplicate = false;
    	
    	return duplicate;
    	
    }
    
    private ApexPages.StandardController controller;
}