/*********************************************************************************
Class Name      : CPQ_PricebookTriggerHelper
Description     : This class is used as a helper class for Pricebook2 Trigger 
Created By      : Alina Balan
Created Date    : 04-November-2015
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Alina Balan            04-November-2015            Created class for Pricebook2 Trigger
*********************************************************************************/
public with sharing class CPQ_PricebookTriggerHelper {
    
     /*********************************************************************************
     Method Name    : preventDummyPricebookDeletion
     Description    : Method used to prevent deletion of the default price book record (CPQ Temp PriceBook)
     Return Type    : void
     Parameter      : 1. List<Pricebook2> lstPricebooks 
     *********************************************************************************/
    public static void preventDummyPricebookDeletion(List<Pricebook2> lstPricebooks) {
        CPQ_Settings__c csCustomSetting = CPQ_Settings__c.getInstance();
        for (Pricebook2 pricebook : lstPricebooks) {
            if (pricebook.Name.equalsIgnorecase(csCustomSetting.DefaultOpportunityPB__c)) {
                pricebook.addError(Label.CPQ_DefaultPricebookDeletionError);
            }
        }
    }
}