/*********************************************************************************
Class Name      : CPQ_OpportunityTriggerHelper
Description     : This class is used as a helper class for Opportunity Trigger 
Created By      : Aakanksha Patel
Created Date    : 19-Oct-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Aakanksha Patel          19-Oct-15            Method to associate a Temporary PriceBook every time a new Opportunity is created.(US-0115)
Joseph Toms              20-Nov-2015         US-0381  Check primary contact exist for Opportunity before changing stage to Qualify 

*********************************************************************************/
public with sharing class CPQ_OpportunityTriggerHelper{

private static final String OPP_STAGE_QUALIFY ='Qualify';
   
   /*********************************************************************************
    Description    : To associate a Temp PriceBook every time a new Opportunity is created.(US-0115)
    Developer      : Aakanksha Patel
    Date           : 19th Oct 2015
    *********************************************************************************/
  
     public static void  assignPricebookToOpp(List<Opportunity> lstOpportunity)
     {
        //For using Custom Setting field value
        CPQ_Settings__c cs = CPQ_Settings__c.getInstance();
      
        //Id OppRecordId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('EBU - Unified Opportunity').getRecordTypeId();
        Id OppRecordId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(cs.DefaultOpportunityRecordType__c).getRecordTypeId();
        List<Pricebook2> lstPriceBook =  [select id,Name from Pricebook2 where Name =: cs.DefaultOpportunityPB__c];
        
          //Assigning a default pricebook to newly created opportunities
            for(Opportunity o: lstOpportunity)
            {   //check for the required Record Type
                if(o.recordTypeId == OppRecordId) 
                {   //Checking if Default Pricebook  and Opportunity.Pricebook field  is empty
                    if( String.isBlank(o.PriceBook2Id) && (!lstPriceBook.isEmpty()))
                    { 
                         o.PriceBook2Id = lstPriceBook[0].id;
                    }
                }
            }
    }
    
    /*********************************************************************************
    Description    : Get primary contact for Opportunities  
    Developer      : Joseph Toms
    Date           : 20-Nov-2015
    *********************************************************************************/
    private  static  Map<String , OpportunityContactRole> getPrimaryContacts(List<Id> lstOpportunityIds){
        Map<String,OpportunityContactRole> mapOpportunityContactRole =  new Map<String, OpportunityContactRole>();   
        //get primary contact role for each opportunity and addt toMap<String , OpportunityContactRole>    
        List< OpportunityContactRole> lstOpportunityContactRole = new List< OpportunityContactRole>([select OpportunityId, ContactID from OpportunityContactRole where OpportunityId in :lstOpportunityIds and IsPrimary = true]);                   
        for(OpportunityContactRole oOpportunityContactRole:lstOpportunityContactRole){
            mapOpportunityContactRole.put(oOpportunityContactRole.OpportunityId,oOpportunityContactRole);                  
        }
        return mapOpportunityContactRole;   
    }
    
    /*********************************************************************************
    Description    : To check  primary contact exists before changing Opportunity Stage to Qualify
    Developer      : Joseph Toms
    Date           : 20-Nov-2015
    *********************************************************************************/
    public static void validateOpportunityStageQualify(List<Opportunity> lstOpportunity){       
        //List to store all opportunity ids 
        List<Id> lstOpportunityIds = new List<Id>(); 
        //get all opportunity ids to use in SOQL          
        for(Opportunity oOpportunity: lstOpportunity){
            lstOpportunityIds.add(oOpportunity.Id);
        }
        //get primary contact for all opportunity             
        Map<String, OpportunityContactRole> mapOpportunityContactRole = getPrimaryContacts(lstOpportunityIds);  
        //loop thru each opportunity check stage is Qualify. If stage is Qualify, check primary role exist.   
        for(Opportunity oOpportunity: lstOpportunity){ 
            //check stage is Qualify
            if(oOpportunity.StageName.equals(OPP_STAGE_QUALIFY)){ 
                //if no primary contact show error     
                if(mapOpportunityContactRole.get(oOpportunity.id)==null )
                  oOpportunity.addError(Label.CPQ_NoPrimary_Contact); 
            }           
        }             
    }   
}