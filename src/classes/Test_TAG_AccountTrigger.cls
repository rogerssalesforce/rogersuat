/*
===============================================================================
 Class Name   : Test_AccountTrigger
===============================================================================
PURPOSE:    This is a Test class for AccountTrigger.
              
Developer: Aakanksha Patel
Date: 12/03/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
12/03/2015           Aakanksha               Original Version
===============================================================================*/
@isTest (seeAllData=False) 
private class Test_TAG_AccountTrigger{
     private static Account ParentAcc;
     private static Account childAcc;
     private static District__c Dist;
     private static Account ParentAccDist;
     private static  Assignment_Request__c  AccountAR;
     private static  Assignment_Request_Item__c AccountARI;
     private static  Assignment_Request_Item__c AccountARI1;
     private static  Assignment_Request_Item__c AccountDistARI;
     private static List<AccountTeamMember> parentAccTeam;
     private static Profile pEmp = [Select Id from Profile where Name Like '%Rogers%' limit 1];
     private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
     private static User userRec;
     private static User userRec2;
     //create Test Data
     private static void setUpData(){
        TAG_CS.Team_Custom_Object_Access__c='Read';
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Unassigned_Status__c = 'Unassigned';
        insert TAG_CS;
        userRec = new User(LastName = 'Mark O’BrienRoger',Channel__c = 'Channel_test' ,Alias = 'alRoger', Email='test@Rogertest.com', Username='test66@Rogertest.com', CommunityNickname = 'nickRoger666', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec;
        userRec2 = new User(LastName = 'Mark O’BrienRoger2',Owner_Type__c ='Account',Assignment_Approver__c = userRec.id ,Channel__c = 'Channel_test' , Alias = 'aRoger', Email='test@Rogertest.com', Username='test67@Rogertest2.com', CommunityNickname = 'nickRoger667', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec2;
        
        ParentAcc= new Account(Name='ParentAcc');
        ParentAcc.Account_Status__c = 'Pending';
        ParentAcc.ParentID= null;
        ParentAcc.Initial_Account_Executive__c = userRec2.id;
        ParentAcc.OwnerId = userRec2.Id;
        ParentAcc.BillingPostalCode= 'A1A 1A1'; 
        insert ParentAcc;
        
        AccountAR = new Assignment_Request__c();
        AccountAR.OwnerID = userRec2.id;
        insert AccountAR ;
        
        AccountARI= new Assignment_Request_Item__c();
        AccountARI.Account__c = ParentAcc.Id;
        AccountARI.New_Owner__c = userRec2.id;
        AccountARI.Business_Case__c = 'Business_Trigger';
        AccountARI.Reason_code__c = '4 - New Account Creation';
        AccountARI.Assignment_Request__c = AccountAR.Id;
        AccountARI.New_Approver__c = userRec2.Assignment_Approver__c;
        AccountARI.New_Channel__c = 'Channel_test';
        AccountARI.Requested_Item_Type__c='Account Owner';
        insert AccountARI;
        
        //For the condition where parent field is not blank
        childAcc= new Account(Name='childAcc');
        childAcc.Account_Status__c = 'Pending';
        childAcc.ParentID= ParentAcc.id;
        childAcc.Initial_Account_Executive__c = userRec2.id;
        childAcc.OwnerId = userRec2.Id;
        childAcc.BillingPostalCode= 'A1A 1A1';
        insert childAcc;
        
        AccountARI1= new Assignment_Request_Item__c();
        AccountARI1.Account__c = childAcc.Id;
        AccountARI1.New_Owner__c = userRec2.id;
        AccountARI1.Business_Case__c = 'Business_Trigger';
        AccountARI1.Reason_code__c = '4 - New Account Creation';
        AccountARI1.Assignment_Request__c = AccountAR.Id;
        AccountARI1.New_Approver__c = userRec2.Assignment_Approver__c;
        AccountARI1.New_Channel__c = 'Channel_test';
        AccountARI1.Requested_Item_Type__c='Account Owner';
        insert AccountARI1;
        
        // For condition when Initial_Account_Executive__c is null
        Dist = new District__c();
        Dist.OwnerId=userrec2.id;
        
        ParentAccDist= new Account(Name='ParentAcc');
        ParentAccDist.Account_Status__c = 'Pending';
        ParentAccDist.ParentID= null;
        ParentAccDist.Initial_District__c = Dist.id;
        ParentAccDist.BillingPostalCode= 'A1A 1A1';
        insert ParentAccDist;
              
        AccountDistARI= new Assignment_Request_Item__c();
        AccountDistARI.Account__c = ParentAccDist.Id;
        AccountDistARI.New_Owner__c = Dist.OwnerID;
        AccountDistARI.Business_Case__c = 'Business_Trigger';
        AccountDistARI.Reason_code__c = '4 - New Account Creation';
        AccountDistARI.Assignment_Request__c = AccountAR.Id;
        AccountDistARI.New_Approver__c = userRec2.Assignment_Approver__c;
        AccountDistARI.New_Channel__c = 'Channel_test';
        AccountDistARI.New_District__c = Dist.id;
        AccountDistARI.Requested_Item_Type__c='Account Owner';
        insert AccountDistARI; 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with Assignment Request gets created for the particular account
    *******************************************************************************************************/
    static testmethod void testAccTrigger(){
        setUpData(); 
        test.startTest();
        ParentAcc.Account_Status__c = 'Unassigned';
        childAcc.Account_Status__c = 'Unassigned';
        ParentAccDist.Account_Status__c = 'Unassigned';
        update ParentAcc;
        update ParentAccDist;
        update childAcc;
        delete ParentAcc;
        undelete ParentAcc;
        test.stopTest();
    }    
}