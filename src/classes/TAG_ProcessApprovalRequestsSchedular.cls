/*Class Name :TAG_ProcessApprovalRequestsSchedular.
 *Description : 
 *Created By : Aakanksha Patel.
 *Created Date :23/02/2015.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/

global class TAG_ProcessApprovalRequestsSchedular implements schedulable{

    global void execute(SchedulableContext sch) {
       Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
       
        list<AsyncApexJob> templist = new list<AsyncApexJob>([SELECT ApexClassId FROM AsyncApexJob WHERE JobType='BatchApex' AND ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND (Name = 'TAG_ProcessApprovalRequestsBatch' OR Name = 'TAG_ProcessApprovalRequestsChildBatch' OR  Name = 'TAG_ReCalculateAccountTeam_batch' OR  Name = 'TAG_giveAccessToNewTeam_batch' OR Name='TAG_ReCalAccTeamOnChild_new_batch' OR Name='TAG_UpdateAccCascadeFlag_batch')) AND (Status = 'Processing' OR Status = 'Preparing') ]);
        
      if ([SELECT COUNT() FROM AsyncApexJob WHERE JobType='BatchApex'  AND (Status = 'Processing' OR Status = 'Preparing')] < TAGsettings.Max_batch_run__c){ 
       
            if(templist == null || templist.size() == 0 )
            {
               Integer batchSize = Integer.ValueOf(TAGsettings.ProAppReqSchBatchSize__c);
               TAG_ProcessApprovalRequestsBatch ProcessAReq = new TAG_ProcessApprovalRequestsBatch();
               ID batchprocessid = Database.executeBatch(ProcessAReq,batchSize);
            }
        } 
    }
}