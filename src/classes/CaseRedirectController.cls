public class CaseRedirectController {
    private String RecordTypeId;
    private String RecordTypeName;
    public String accId;
    public String oppId;
    public String oppName;
    public boolean redirect{get; set;}
    public CaseRedirectController(ApexPages.StandardController controller){
        RecordTypeId = ApexPages.currentPage().getParameters().get('RecordType');
        RecordTypeName = '';
        accId=ApexPages.currentPage().getParameters().get('cas4_lkid');
        oppId=ApexPages.currentPage().getParameters().get('CF00Ni0000006oHO8_lkid');
        oppName=ApexPages.currentPage().getParameters().get('CF00Ni0000006oHO8');
        for(RecordType rt:[select id,name from recordtype where id = :recordtypeid])
            RecordTypeName = rt.name;
    } 
    public pagereference doRedirect(){
        PageReference page; 
        if(RecordTypeName=='Account Delink'){//for Account Delink record type go to AccountDelinking vf page
            page = new PageReference('/apex/AccountDeLinkingTemplate?loc=500'); 
            return page;
        }
        else if(RecordTypeName=='Account Link'){//for Account Link record type go to AccountLinking vf page
            page = new PageReference('/apex/AccountLinkingTemplate?loc=500&Type=NewNoParentAcct'); 
            return page;
        }
        else{//for all the other record types redirect to the standard new page
            System.debug('#####Other record types#####');
            String strURL = '/500/e?RecordType='+recordtypeid+'&nooverride=1';
            if(accId!='' && accId!=null){
                strURL = strURL +'&cas4_lkid='+accId;
                System.debug('#####1:' + strURL + '#####');
            }
            if(oppId!='' && oppId!=null){
                strURL = strURL +'&CF00Ni0000006oHO8_lkid='+oppId;
                System.debug('#####2:' + strURL + '#####');
            }
            if(oppName!='' && oppName!=null){
                strURL = strURL +'&CF00Ni0000006oHO8='+EncodingUtil.urlEncode(oppName, 'UTF-8');
                System.debug('#####3:' + strURL + '#####');
            }
            
            page = new PageReference(strURL); 
            return page;
        }
    }
}