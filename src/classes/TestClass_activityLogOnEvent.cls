/*Class Name  : TestClass_activityLogOnEvent.
 *Description : This is test class for trigger activityLogOnEvent.
 *Created By  : Deepika Rawat.
 *Created Date :28/01/2014.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
@isTest(SeeAllData = true)
private class TestClass_activityLogOnEvent{
    private static Account accTestObj;
     private static user objUser;
    static testmethod void myUnitWithCaseTest(){
        accTestObj = new Account(Name='testAcc');
        accTestObj.BillingPostalCode ='A1A 1A1';
        insert accTestObj;
        Event e = new event();
        e.Subject='Call';
        e.ActivityDate = system.today();
        e.StartDateTime = system.now();
        e.EndDateTime = system.now();
        Test.starttest();       
        insert e;
        e.Subject='Email';
        update e;
        delete e;
        Test.stoptest();
    
    }
}