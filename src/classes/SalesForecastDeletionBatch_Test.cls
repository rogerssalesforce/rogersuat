/*
===============================================================================
Class Name :SalesForecastDeletionBatch_Test
===============================================================================
PURPOSE: This is a test class for SalesForecastDeletionBatch class

COMMENTS: 

Developer: Deepika Rawat
Date: 11/11/2013


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
11/11/2013               Deepika               Created
===============================================================================

*/
@isTest (seealldata = false) 
private class SalesForecastDeletionBatch_Test{
private static Static_Data_Utilities__c CS= new Static_Data_Utilities__c(); 
    private static Static_Data_Utilities__c CS4= new Static_Data_Utilities__c();   
    private static Static_Data_Utilities__c CS3= new Static_Data_Utilities__c();   
    private static BatchProcess_Admin__c CS1= new BatchProcess_Admin__c();   
    private static Static_Data_Utilities__c CS2 = new Static_Data_Utilities__c(); 

     static Sales_Forecast__c forecast;
    static List<User> listUser = [select id, user.profile.name from User where user.profile.name= 'System Administrator' and isActive = true limit 2];
    
 static testmethod void testMethod1(){
        Sales_Quota__c quota = new Sales_Quota__c();
        quota.Forecast_Month__c =201310;
        quota.Ownerid =listUser[1].id;
        quota.Quantity__c= 2;
        quota.Quota_Family__c= 'ABS - Software';
        quota.Revenue__c = 100.00;
        
        insert quota; 
 
        CS.Name = 'Quota Update Checking Period';
        CS.Value__c = '1'; 
        insert CS;
        CS3.Name = 'Recalculation Period';
        CS3.Value__c= '120';
        insert CS3;
         
        
        CS2.Name = 'Opportunity Update Checking Period';
        CS2.Value__c = '1'; 
        insert CS2;
        
        CS1.Name = 'SysAdm1';
        CS1.Email__c = 'sfdc.support@rci.rogers.com'; 
        insert CS1;
        
        CS4.Name = 'BatchFire';
        CS4.Value__c = 'true'; 
        insert CS4;

        forecast= new Sales_Forecast__c();
        forecast.Start_of_Deployment_Month__c=system.today();
        insert forecast;  
         
        Test.StartTest();
        SalesForecastDeletionBatch sc = new SalesForecastDeletionBatch();                
        sc.Query ='select id , Start_of_Deployment_Month__c from Sales_Forecast__c  WHERE id =\''+ forecast.id+ '\' ';
        ID salesForecastBatchProcessId = Database.executeBatch(sc,200);
        Test.StopTest();
    }
}