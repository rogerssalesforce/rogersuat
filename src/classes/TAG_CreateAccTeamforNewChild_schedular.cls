/*
===============================================================================
 Class Name   : TAG_CreateAccTeamforNewChild_schedular
===============================================================================
PURPOSE:    This schedular class runs the TAG_CreateAccountTeamforNewChild_batch 
            with a batch of 200 records. 

Developer: Deepika Rawat
Date: 01/25/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
01/25/2015          Deepika                     Original Version
===============================================================================
*/
global class TAG_CreateAccTeamforNewChild_schedular implements schedulable{

    global void execute(SchedulableContext ctx) {
        TAG_CreateAccountTeamforNewChild_batch sc = new TAG_CreateAccountTeamforNewChild_batch ();
        ID batchprocessid = Database.executeBatch(sc,200);
        system.debug('sc*************' +sc);
    }

}