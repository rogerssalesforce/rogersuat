/**************************************************************************************
Apex Class Name     : TAG_LandingPageController
Version             :  
Created Date        : 4/8/2015
Function            : This is a controller class for TAG_LandingPage page.
Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deepika                    4/8/2015               Original Version  
*************************************************************************************/
public with sharing class TAG_LandingPageController{
      public List<PermissionSetAssignment> lstPermissionSet;
      public boolean hasAccessonBulk{get;set;}
      public Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
      
      public TAG_LandingPageController(){
          List<string> bulkPermissionSetNames = new list<string>();
          bulkPermissionSetNames = TAGsettings.Allowed_Bulk_Access__c.split(',');

          lstPermissionSet = new list<PermissionSetAssignment>([SELECT Id, PermissionSet.Name,AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name in: bulkPermissionSetNames and AssigneeId = :Userinfo.getUserId()]);

          if(lstPermissionSet!=null && lstPermissionSet.size()>0){
            hasAccessonBulk = true;
          }
      }
      

}