/*
===============================================================================
Class Name :SalesForecastRecalculationBatch_Test
===============================================================================
PURPOSE: This is a test class for SalesForecastRecalculationBatch class

COMMENTS: 

Developer: Deepika Rawat
Date: 11/11/2013


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
11/11/2013               Deepika               Created
===============================================================================

*/
@isTest (SeeAllData = true)
private class SalesForecastRecalculationBatch_Test{
    private static Sales_Quota__c quota;
    private static MSD_Code__c msd; 
    private static Account accTestObj;
    private static OpportunityLineItem  oliTestObj;
    private static Opportunity oppTestObj;
    private static Sales_Measurement__c smTest;
    private static Sales_Measurement__c smTest2;
    private static User thisUser;
    private static User u;
    static List<User> listUser = [select id, user.profile.name from User where user.profile.name= 'System Administrator' and isActive = true limit 2];
    static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
    static testmethod void testData(){

        thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        Profile p = [select id from profile where name='System Administrator' ];
        UserRole r = [Select id from userrole where DeveloperName='CRMSupportID2'];
        u = new User(alias = 'TestUser', email='standarduser@testorg.com.rogers', 
            emailencodingkey='UTF-8', lastname='TestUser', 
            languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
            timezonesidkey='America/Los_Angeles', 
            username='standarduser@testorg.com.Rogers');
        //insert AccountaccTestObj
        accTestObj = new Account();
        accTestObj.Name='testAcc';
        accTestObj.BillingCountry= 'CA';
        accTestObj.BillingPostalCode = 'A9A 9A9';
        accTestObj.BillingState = 'MA';
        accTestObj.BillingCity='City';
        accTestObj.BillingStreet='Street';
        insert accTestObj;
        //insert Opportunity oppTestObj
        oppTestObj = new Opportunity(Name='testAcc');
        oppTestObj.StageName='Defined';
        oppTestObj.CloseDate=system.today();
        oppTestObj.AccountID=accTestObj.id;
        oppTestObj.ownerid =listUser[0].id;
        oppTestObj.Pricebook2Id=[select id from Pricebook2 where isStandard = true LIMIT 1].id;
        oppTestObj.Unified_Comm_Collaboration_Estimated__c =20;
        insert oppTestObj;
        //insert Opportunity line item oliTestObj
        oliTestObj= new OpportunityLineItem();
        oliTestObj.ARPU__c=10;
        oliTestObj.Start_Date__c=system.today();
        oliTestObj.Quantity=10;
        oliTestObj.Installment_Period__c='Monthly';
        oliTestObj.of_Installments__c=10;
        oliTestObj.Monthly_Value__c=100;
        oliTestObj.Minimum_Contract_Value__c=100;
        oliTestObj.OpportunityId=oppTestObj.id;         
        oliTestObj.PricebookEntryId=[select id,Name from PricebookEntry where isactive = True AND Pricebook2.id=:oppTestObj.Pricebook2Id LIMIT 1].id;
        oliTestObj.TotalPrice=0;
        oliTestObj.Product_Family__c='Cable - TV';
        insert oliTestObj;
        }
        
    
    }
    static testmethod void recalculateSFfromQuota(){
        testData();
        System.runAs(u) { 
            quota = new Sales_Quota__c();
            quota.Forecast_Month__c =201310;
            quota.Ownerid =listUser[1].id;
            quota.Quantity__c= 2;
            quota.Quantity__c= 2;
            quota.Revenue__c = 100.00;
            insert quota; 
         }
        // System.runAs ( thisUser ) {
        //    listUser[1].isActive = false;
        //    update listUser[1];
        //}     
        Test.StartTest();
        SalesForecastRecalculationBatch sc = new SalesForecastRecalculationBatch();                
        sc.Query ='select id,LastModifiedDate,Owner.FirstName,Owner.LastName,Quota_Family__c,Forecast_Month__c,OwnerID,Quantity__c,Revenue__c,OwnerMonthFamily__c FROM Sales_Quota__c WHERE id =\''+ quota.id+ '\' ';
        ID salesQuotaBatchProcessId = Database.executeBatch(sc,200);
        Test.StopTest();
    }
    static testmethod void recalculateSFfrmMeasurement(){
        testData();
        System.runAs(u) { 
            //Update Opp to Closed Won
            oppTestObj.StageName='Closed Won';
            oppTestObj.Win_Reason__c = 'TestReason';
            update oppTestObj;
            //insert salesmeasurement for oliTestObj
            smTest= new Sales_Measurement__c ();
            smTest.Ownerid = listUser[1].id;
            smTest.Opportunity__c = oppTestObj.id;
            smTest.Opportunity_Line_Item_ID__c = oliTestObj.id;
            smTest.Deployment_Date__c = system.today();
            smTest.Quota_Family__c = 'ABS Connectivity';
            insert smTest; 
            //insert salesmeasurement for oliTestObj
            smTest2= new Sales_Measurement__c ();
            smTest2.Ownerid = listUser[1].id;
            smTest2.Opportunity__c = oppTestObj.id;
            smTest2.Opportunity_Line_Item_ID__c = oliTestObj.id;
            smTest2.Deployment_Date__c = system.today();
            smTest2.Quota_Family__c = 'ABS Connectivity';
            insert smTest2;   
        }
        // System.runAs ( thisUser ) {
        //    listUser[1].isActive = false;
        //    update listUser[1];
        //}   
        Test.StartTest();
        
        SalesForecastRecalculationBatch sc = new SalesForecastRecalculationBatch();                
        sc.Query ='select id, LastModifiedDate, Quota_Family__c, Quantity__c, Revenue__c, OwnerMonthFamily__c, Deployment_Month__c, OwnerID, Deployment_Date__c, Opportunity__r.StageName, Opportunity__r.LastModifiedDate, Opportunity__c, Weighted_Revenue__c, Weighted_Quantity__c FROM Sales_Measurement__c  WHERE id =\''+ smTest.id+ '\' or id =\''+ smTest2.id+ '\' ';
        ID salesQuotaBatchProcessId = Database.executeBatch(sc,200);
        Test.StopTest();
    }
    static testmethod void recalculateSFfromMeasurementCommit(){
        testData();
        System.runAs(u) { 
            oppTestObj.StageName='Committed';
            update oppTestObj;
            //insert salesmeasurement for oliTestObj
            smTest= new Sales_Measurement__c ();
            smTest.Ownerid = listUser[1].id;
            smTest.Opportunity__c = oppTestObj.id;
            smTest.Opportunity_Line_Item_ID__c = oliTestObj.id;
            smTest.Deployment_Date__c = system.today();
            smTest.Quota_Family__c = 'ABS Connectivity';
            insert smTest;    
        }
        // System.runAs ( thisUser ) {
        //    listUser[1].isActive = false;
        //    update listUser[1];
        //}   
        Test.StartTest();
        SalesForecastRecalculationBatch sc = new SalesForecastRecalculationBatch();                
        sc.Query ='select id, LastModifiedDate, Quota_Family__c, Quantity__c, Revenue__c, OwnerMonthFamily__c, Deployment_Month__c, OwnerID, Deployment_Date__c, Opportunity__r.StageName, Opportunity__r.LastModifiedDate, Opportunity__c, Weighted_Revenue__c, Weighted_Quantity__c FROM Sales_Measurement__c  WHERE id =\''+ smTest.id+ '\' ';
        ID salesQuotaBatchProcessId = Database.executeBatch(sc,200);
        Test.StopTest();
    }
    static testmethod void recalculateSFfromMeasurementDiffrntiatd(){
        testData();
        System.runAs(u) { 
            oppTestObj.StageName='Differentiated';
            update oppTestObj;
            //insert salesmeasurement for oliTestObj
            smTest= new Sales_Measurement__c ();
            smTest.Ownerid = listUser[1].id;
            smTest.Opportunity__c = oppTestObj.id;
            smTest.Opportunity_Line_Item_ID__c = oliTestObj.id;
            smTest.Deployment_Date__c = system.today();
            smTest.Quota_Family__c = 'ABS Connectivity';
            insert smTest;    
        }
        // System.runAs ( thisUser ) {
        //    listUser[1].isActive = false;
        //    update listUser[1];
        //}   
        Test.StartTest();
        SalesForecastRecalculationBatch sc = new SalesForecastRecalculationBatch();                
        sc.Query ='select id, LastModifiedDate, Quota_Family__c, Quantity__c, Revenue__c, OwnerMonthFamily__c, Deployment_Month__c, OwnerID, Deployment_Date__c, Opportunity__r.StageName, Opportunity__r.LastModifiedDate, Opportunity__c, Weighted_Revenue__c, Weighted_Quantity__c FROM Sales_Measurement__c  WHERE id =\''+ smTest.id+ '\' ';
        ID salesQuotaBatchProcessId = Database.executeBatch(sc,200);
        Test.StopTest();
    }
    static testmethod void recalculateSFfromMeasurementDefined(){
        testData();
        System.runAs(u) { 
            oppTestObj.StageName='Defined';
            update oppTestObj;
            //insert salesmeasurement for oliTestObj
            smTest= new Sales_Measurement__c ();
            smTest.Ownerid = listUser[1].id;
            smTest.Opportunity__c = oppTestObj.id;
            smTest.Opportunity_Line_Item_ID__c = oliTestObj.id;
            smTest.Deployment_Date__c = system.today();
            smTest.Quota_Family__c = 'ABS Connectivity';
            insert smTest;    
        }
        // System.runAs ( thisUser ) {
        //   listUser[1].isActive = false;
        //    update listUser[1];
        //}   
        Test.StartTest();
        SalesForecastRecalculationBatch sc = new SalesForecastRecalculationBatch();                
        sc.Query ='select id, LastModifiedDate, Quota_Family__c, Quantity__c, Revenue__c, OwnerMonthFamily__c, Deployment_Month__c, OwnerID, Deployment_Date__c, Opportunity__r.StageName, Opportunity__r.LastModifiedDate, Opportunity__c, Weighted_Revenue__c, Weighted_Quantity__c FROM Sales_Measurement__c  WHERE id =\''+ smTest.id+ '\' ';
        ID salesQuotaBatchProcessId = Database.executeBatch(sc,200);
        Test.StopTest();
    }
}