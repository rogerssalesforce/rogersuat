/*
===============================================================================
Class Name   : Test_BrowserUsageController
===============================================================================
PURPOSE:    This class a controller class for BrowserUsageControllerPage.
              
Developer: Deepika
Date: 04/02/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
04/02/2015          Deepika              Original Version
===============================================================================*/
@isTest (seeAllData=true) 
private class Test_BrowserUsageController{
    static testmethod void BrowserUsageControllerPage(){
        test.startTest();
            Account a = new Account();
            a.name = 'Test Act';
            a.Business_Segment__c = 'Alternate';
            a.BillingStreet = 'Street';
            a.BillingCity = 'MyCity';
            a.BillingCountry = 'Canada';
            a.BillingPostalCode = 'L1L 1L1';
            a.BillingState = 'ON';
            a.ParentId=null;
            a.Account_status__c='Assigned';
            insert a;
        	Browser_Usage_Statistics__c BUS=new Browser_Usage_Statistics__c(Name='testBSU',iPad__c=1, Chrome__c=1, iPhone__c=1, Internet_Explorer__c=1, Safari__c=1, Other__c=1, Blackberry__c=1);        	
            insert BUS;    
            ApexPages.StandardController sc = new ApexPages.StandardController(a);
            BrowserUsageController obj = new BrowserUsageController(sc);

        test.stopTest();
    }

}