public abstract class AbstractCloneAllController {

    public Boolean hasSufficientPrivileges {get;set;}
    
    private Id objId ;

    public AbstractCloneAllController (ApexPages.StandardController controller){

        objId = ApexPages.currentPage().getParameters().get('id');
        system.debug('CloneAllController Constructor: Object id = ' +  objId );
        
    }
    
    public virtual AbstractClone getClone(){
        return new Clone();
    }
    
    /*
     * http://developer.force.com/codeshare/projectpage?id=a063000000EWWYrAAP
     * 
     * http://code.google.com/p/salesforce-super-clone/source/browse/trunk/%20salesforce-super-clone/Google%20code%20Super%20Clone/
     * r3 by jinghaili on Jul 10, 2011 
     * 
     */    
        
    public pagereference doCloneAll(){
        
        if(!hasSufficientPrivileges){
            //Raise system Insufficient Privileges message for current record
            return redirect(true, objId); 
        }

        AbstractClone cc=getClone();

        string clondedParentRecordID=''; 
        try{
            string parentObjAPIName=cc.returnAPIObjectName(objId);
            Map<string,string> objLabelobjAPI=cc.getAllChildObjNames(parentObjAPIName,objId);
            
            system.debug('cloneAll(): parentObjAPIName = ' +  parentObjAPIName);
            system.debug('cloneAll(): objLabelobjAPI == null ? ' +  (objLabelobjAPI==null) );
            if (objLabelobjAPI!=null){
                system.debug('cloneAll(): objLabelobjAPI size =' +  objLabelobjAPI.size() );
            }
            
            clondedParentRecordID=cc.startsClone(objId, objLabelobjAPI.values()); 
            system.debug('cloneAll(): cloned record id = ' + clondedParentRecordID);
        }
        catch(System.Exception e){
            system.debug('CloneAllController exception\n' + e.getStackTraceString());
            return redirect( false);
        }
        
        
        cc=null;

        return redirect( true, clondedParentRecordID );
        
    }
    
    
    public pagereference redirect(boolean isSuccess){
        system.debug('redirect(' + isSuccess + ')' ) ;
        return new PageReference('/500/o');
    }
    
    public pagereference redirect(boolean isSuccess, string id){
        system.debug('redirect(' + isSuccess + ','+ id +')' ) ;
        if (isSuccess){
            //Go To Newly Cloned Object in Edit Mode
            return new PageReference('/' + id +'/e');
        }
        
        //Otherwise Go To Object Home screen
        return redirect(isSuccess);
    }
    
    public Boolean getHasSufficientPrivileges(Id currentRecId){
        List<UserRecordAccess> uraObj = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId =:UserInfo.getUserId() AND RecordId =:currentRecId ];
        UserRecordAccess ura = null;

        if(uraObj!=null){

            if(uraObj.size()>0){
                ura = (UserRecordAccess)uraObj.get(0);
            }
        }

        if(uraObj==null || uraObj.size()==0 || !ura.HasEditAccess ){
            ura = null;
            uraObj = null;
            
            return false;
        }
        
        ura = null;
        uraObj = null;
        
        return true;
    }
    
    
}