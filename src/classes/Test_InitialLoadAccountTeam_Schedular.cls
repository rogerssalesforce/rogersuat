/*
===============================================================================
Class Name : Test_InitialLoadAccountTeam_Schedular 
===============================================================================
PURPOSE: This is a test class for InitialLoadAccountTeam_Schedular class

COMMENTS: 

Developer:Aakanksha Patel
Date: 19/3/2015


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
19 Mar 2015               Aakanksha               Created
===============================================================================

*/
@isTest(SeeAllData = false)
private class Test_InitialLoadAccountTeam_Schedular{
 static testmethod void testMethod1(){
                
        Test.StartTest();
        InitialLoadAccountTeam_Schedular obj= new InitialLoadAccountTeam_Schedular();
        String sch = '0 0 23 * * ?';
        system.schedule('InitialLoadAccountTeam_SchedularTesting', sch, obj);
        System.assert(true);
        Test.stopTest();    

    }
}