/*Class Name :Testclass_BISActivitySummarySchedular.
 *Description : .
 *Created By : Rajiv Gangra.
 *Created Date :20/01/2014.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
@isTest(SeeAllData = False)
private class Testclass_BISActivitySummarySchedular{
    static testmethod void vanillaTestCreate(){
        Test.StartTest();
        BatchProcess_Admin__c BA= new BatchProcess_Admin__c();
        BA.Name='SysAdm1';
        BA.Email__c='sfdc.support@rci.rogers.com';
        Upsert BA;
        
        BIS_Activity_Tracking__c bT= new BIS_Activity_Tracking__c();
        bT.RPC_Call__c=true;
        bT.Activity_Type__c='Test@ActivitySummarySchedular';
        bT.Activity_Date__c=System.today();
        insert bT;
        
        system.debug('==============================>'+[select id,name,OwnerMonthType__c from BIS_Activity_Tracking__c where id=:bT.id].OwnerMonthType__c);

        Activity_Target__c aT= new Activity_Target__c();
        aT.Activity_Type__c='Test@ActivitySummarySchedular';
        aT.Target_Month__c=201405;
        aT.Activity_Type__c=bT.Activity_Type__c;
        aT.Target__c =10;
        insert aT;
        
        BIS_Activity_Tracking__c bT1= new BIS_Activity_Tracking__c();
        bT1.RPC_Call__c=False;
        bT1.Activity_Type__c='Test@ActivitySummarySchedular';
        bT1.Activity_Date__c=System.today();
        insert bT1;
        
        Static_Data_Utilities__c ssu= new Static_Data_Utilities__c();
        ssu.Name='BIS Activity Update Check';
        ssu.Value__c='1';
        insert ssu;
        
        Static_Data_Utilities__c ssu1= new Static_Data_Utilities__c();
        ssu1.Name='Activity Summary Cleaned';
        ssu1.Value__c='True';
        insert ssu1;

        Static_Data_Utilities__c ssu2= new Static_Data_Utilities__c();
        ssu2.Name='Activity Summary Cleaned';
        ssu2.Value__c='False';
        insert ssu2;
        
        BIS_ActivitySummarySchedular obj= new BIS_ActivitySummarySchedular();
        String sch = '0 0 23 * * ?';
        system.schedule('BIS_ActivitySummarySchedular', sch, obj);
        System.assert(true);
        Test.stopTest();  
        
        
    }
    static testmethod void vanillaTestWeeklyCreate(){
        Test.StartTest();
        BatchProcess_Admin__c BA= new BatchProcess_Admin__c();
        BA.Name='SysAdm1';
        BA.Email__c='sfdc.support@rci.rogers.com';
        Upsert BA;
        
        BIS_Activity_Tracking__c bT= new BIS_Activity_Tracking__c();
        bT.RPC_Call__c=true;
        bT.Activity_Type__c='Test@ActivitySummarySchedular';
        bT.Activity_Date__c=System.today();
        insert bT;
               
        Activity_Target__c aT= new Activity_Target__c();
        aT.Activity_Type__c='Test@ActivitySummarySchedular';
        aT.Target_Month__c=201405;
        aT.Activity_Type__c=bT.Activity_Type__c;
        aT.Target__c =10;
        insert aT;
        
        BIS_Activity_Tracking__c bT1= new BIS_Activity_Tracking__c();
        bT1.RPC_Call__c=False;
        bT1.Activity_Type__c='Test@ActivitySummarySchedular';
        bT1.Activity_Date__c=System.today();
        insert bT1;
        
        Static_Data_Utilities__c ssu= new Static_Data_Utilities__c();
        ssu.Name='BIS Activity Update Check Weekly';
        ssu.Value__c='1';
        insert ssu;
        
        Static_Data_Utilities__c ssu1= new Static_Data_Utilities__c();
        ssu1.Name='Activity Summary Cleaned';
        ssu1.Value__c='True';
        insert ssu1;
        

        
        BIS_ActivitySummaryWeeklySchedular obj= new BIS_ActivitySummaryWeeklySchedular();
        String sch = '0 0 23 * * ?';
        system.schedule('BIS_ActivitySummaryWeeklySchedular', sch, obj);
        System.assert(true);
        Test.stopTest();  
        
        
    }
}