/*
===============================================================================
 Class Name   : TAG_DeleteARErrorsBatch 
===============================================================================
PURPOSE:    This batch class deletes all AR Errors whose Age__c is greater than
            custom setting value Purge_AREntry__c.
  
Developer: Deepika Rawat
Date: 04/23/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
04/23/2015           Deepika Rawat               Original Version
===============================================================================
*/
global class TAG_DeleteARErrorsBatch implements Database.Batchable<SObject>{
    public Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
    public String ageVal = TAGsettings.Purge_AREntry__c;
    global String query = 'Select Id, Age__c from Assignment_Request_Error__c where Age__c >='+ageVal;
    global Database.QueryLocator start(Database.BatchableContext BC){
            return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Assignment_Request_Error__c> scope){
        system.debug('scope*****************'+scope);
        delete scope;       
    }
    global void finish(Database.BatchableContext BC){
    }
}