/*Class Name  :event_FieldUpdateOnAccount_AI.
 *Description : This Class will support the logic for Event_FieldUpdateOnAccount_AI Trigger before Insert.
 *Created By  : Rajiv Gangra.
 *Created Date :03/09/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
public class event_FieldUpdateOnAccount_AI_AU {

    /*@ Method Name: updateAssignTo
      Description: This Method gets the list of Event and Assigns Field to related Account .
      Return Type: NA.
      Input Param: NA.   
    */
    public void updateAccountInfo(LIST<Event> eList) {
        List<Id> lstAccRelated= New List<Id>();
        List<Id> lstOppRelated= New List<Id>();
        List<Account> lstAccToUpdate= new List<Account>();
        Map<id,Account> mapOppAccount= new Map<id,Account>(); 
        List<Opportunity> lstOppToUpdate= new List<Opportunity>();
        list<User> uList=[select id,Name,Channel__c from User];
        Map<id,string> mapUserChannel= new Map<id,string>(); 
        for(User u:uList){
            mapUserChannel.put(u.id,u.Channel__c);
        }
        try{
            if(eList !=null && eList.size() !=0){
            for( Event e : eList){
                Schema.Sobjecttype inspecttype;
                if(e.WhatId !=null){
                    inspecttype=e.WhatId.getSObjectType();
                }
                 if(String.Valueof(inspecttype) == 'Opportunity'){
                   lstOppRelated.add(e.WhatId);  
                 }else if(String.valueof(inspecttype) == 'Account'){
                   lstAccRelated.add(e.WhatId);
                 }
            
            }
            List<Opportunity> lstRelatedOpp= [select id,name,AccountId from Opportunity where Id IN:lstOppRelated];
            if(lstRelatedOpp !=null && lstRelatedOpp.size() !=0){
                for(Opportunity o:lstRelatedOpp){
                    if(o.AccountId !=null || o.AccountId !=''){
                        lstAccRelated.add(o.AccountId);
                    }
                }
            }
            List<Account> lstRelatedAcc= [select id,name,Last_Activity_Date__c,Last_Activity_Owner__c from Account where Id IN:lstAccRelated];
            if(lstRelatedAcc !=null && lstRelatedAcc.size()!=0){
                for(Account a:lstRelatedAcc){
                 for(Opportunity o:lstRelatedOpp){
                     if(o.AccountId == a.id){
                         mapOppAccount.put(o.id,a);
                     }
                 }
                }
            }
            
            for(Event e:eList){
                DateTime dT = e.LastModifiedDate;
                Date createdDate = date.newinstance(dT.year(), dT.month(), dT.day());
                if(lstRelatedAcc !=null && lstRelatedAcc.size()!=0){
                    for(Account a:lstRelatedAcc){
                        if(e.WhatID==a.id){
                            a.Last_Activity_Date__c=createdDate;
                            a.Last_Activity_Owner__c=e.Ownerid;
                            a.Last_Activity_Channel__c=mapUserChannel.get(e.Ownerid);
                            lstAccToUpdate.add(a);
                        }
                    }
                }
                if(lstRelatedOpp !=null && lstRelatedOpp.size()!=0){
                   for(Opportunity o:lstRelatedOpp){
                        if(e.WhatID==o.id){
                           Account a=mapOppAccount.get(o.id);
                           if(a !=null){
                               a.Last_Activity_Date__c=createdDate;
                               a.Last_Activity_Owner__c=e.Ownerid;
                               a.Last_Activity_Channel__c=mapUserChannel.get(e.Ownerid);
                               lstAccToUpdate.add(a);
                           }
                        }
                    }
                }
            }
            Update lstAccToUpdate;
            }
        }Catch(Exception ex){
            system.debug('Error Occured-------->'+ex);
        }
    }

}