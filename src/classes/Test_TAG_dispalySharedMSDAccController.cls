/**************************************************************************************
Apex Class Name     : Test_TAG_dispalySharedMSDAccountsController
Version             : 1.0 
Created Date        : 2 Feb 2015
Function            : This is the Test Class for dispalySharedMSDAccountsController.
Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Aakanksha Patel             02/02/2015              Original Version
*************************************************************************************/
@isTest
private class Test_TAG_dispalySharedMSDAccController
{  
    private static  Account AccountParent;
    private static  Account AccountParent2;
    private static  Shared_MSD_Code__c SharedMsdCode;
    private static  Shared_MSD_Code__c SharedMsdCode2;
    private static  List<Shared_MSD_Accounts__c> listChild;
    private static  Shared_MSD_Accounts__c ParentSharedMSDAccountChild;
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();    
    
 /*   Description : This method is used to create Test Data. 
    Parameters  : None
    Return Type : void
   */
    private static void setUpData()
    {       
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;
        AccountParent = new Account();
        AccountParent.Name = 'Test_account_with_SharedMSDchild';
        AccountParent.Account_Status__c = 'Assigned';
        AccountParent.ParentID= null;
        AccountParent.BillingPostalCode= 'A1A 1A1';
        insert AccountParent; 
        
        AccountParent2 = new Account();
        AccountParent2.Name = 'Test_account_with_SharedMSDchild2';
        AccountParent2.Account_Status__c = 'Assigned';
        AccountParent2.ParentID= null;
        AccountParent2.BillingPostalCode= 'A1A 1A1';
        insert AccountParent2;
        
        SharedMsdCode = new Shared_MSD_Code__c();
        SharedMsdCode.Name='TestMSD';
        insert SharedMsdCode;
        
        SharedMsdCode2 = new Shared_MSD_Code__c();
        SharedMsdCode2.Name='TestMSD2';
        insert SharedMsdCode2;
        
        listChild = new List<Shared_MSD_Accounts__c>{};
        
        for(Integer i = 0; i < 11; i++){
        Shared_MSD_Accounts__c a = new Shared_MSD_Accounts__c();
        a.Account__c = AccountParent.Id;
        a.Shared_MSD_Code__c = SharedMsdCode.Id;
        listChild.add(a);
        }
        insert listChild;
        
        ParentSharedMSDAccountChild = new Shared_MSD_Accounts__c();
        ParentSharedMSDAccountChild.Account__c = AccountParent2.Id;
        ParentSharedMSDAccountChild.Shared_MSD_Code__c = SharedMsdCode2.Id;
        insert ParentSharedMSDAccountChild;               
     }
        
    /*
     Description : This is is a method to test the Search account
     Parameters  : None
     Return Type : void
   */
    private static testMethod void DisplayAccount()
    {
        Test.startTest();
        setUpData();
        {                
            ApexPages.CurrentPage().getparameters().put('smsdid', SharedMsdCode.id);
            TAG_DispalySharedMSDAccountsController obj = new TAG_DispalySharedMSDAccountsController();  
         }
        Test.stopTest();     
    }
    
    /*
     Description : This is is a method to test SharedMSDAccounts without children. 
     Parameters  : None
     Return Type : void
     */
    private static testMethod void SharedMSDAccountWithoutChildren() {
        Test.startTest();
        setUpData();
        {       
                TAG_DispalySharedMSDAccountsController obj = new TAG_DispalySharedMSDAccountsController();
                system.assert(obj.lstSharedMSDAcc.size() == 0);
        }
        Test.stopTest();     
    }
   
    /*
     Description : This is is a method to test SharedMSDAccounts with children. 
     Parameters  : None
     Return Type : void
    */
    private static testMethod void SharedMSDAccountWithChildren() {
        Test.startTest();
        setUpData();
        {
            ApexPages.CurrentPage().getparameters().put('smsdid', SharedMsdCode2.id);
            TAG_DispalySharedMSDAccountsController obj = new TAG_DispalySharedMSDAccountsController();
        }
        Test.stopTest();     
    } 
}