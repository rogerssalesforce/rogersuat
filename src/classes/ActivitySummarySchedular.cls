/*Class Name :ActivitySummarySchedular.
 *Description : .
 *Created By : Rajiv Gangra.
 *Created Date :08/01/2014.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
global class ActivitySummarySchedular implements schedulable{

    global void execute(SchedulableContext ctx) {
        integer SFCheckPoint = Integer.ValueOf(Static_Data_Utilities__c.getInstance('Activity Update Checking Period').Value__c);
        date dT1 = date.newInstance(system.Today().AddMonths(-SFCheckPoint+1).year(),system.Today().AddMonths(-SFCheckPoint+1).month(),1);
        ActivitySummary_batch TaskObj = new ActivitySummary_batch();
        TaskObj.CronId = ctx.getTriggerId();
        if(Static_Data_Utilities__c.getInstance('Activity Summary Cleaned').Value__c=='True'){
            if(!Test.isRunningTest()){
                TaskObj.Query = 'select CreatedById,RPC_Call__c,OwnerMonthType__c,CreatedDate,Id,OwnerId,Owner.FirstName,Owner.LastName,LastModifiedDate,Activity_Type__c,Activity_Date__c,Sales_Rep__c FROM Activity_log__c where Activity_Date__c >=:dT1 ';
            }else{
                TaskObj.Query = 'select CreatedById,RPC_Call__c,OwnerMonthType__c,CreatedDate,Id,OwnerId,Owner.FirstName,Owner.LastName,Activity_Type__c,Activity_Date__c,Sales_Rep__c FROM Activity_log__c LIMIT 50';
            }
        }else{
            if(!Test.isRunningTest()){
                TaskObj.Query = 'select CreatedById,OwnerMonthType__c,CreatedDate,Id,Start_of_Activity_Month__c FROM Activity_Summary__c where Start_of_Activity_Month__c >=:dT1 AND isDeleted=:delRec AND User_Channel__c NOT IN:newLstBU';
            }else{
                TaskObj.Query = 'select CreatedById,OwnerMonthType__c,CreatedDate,Id,Start_of_Activity_Month__c FROM Activity_Summary__c LIMIT 11';
            }
        }
        ID ActivitySummaryBatchProcessId = Database.executeBatch(TaskObj,200);
    }
}