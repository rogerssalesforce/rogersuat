/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=false)
public class TestApprovalHistory 
{
    private static Account a;
    private static Opportunity o;
    private static Quote q;
    private static Scheduled_Quote_Approval_History__c sqah;
    private static Scheduled_Quote_Approval_History__c sqah1;
    public static testMethod void testmethod1(){
        setAllData();
        Test.startTest();
        System.debug('_________________________Test Started_______________________');
        ScheduleDiscountReportProcessing vfep=new ScheduleDiscountReportProcessing();
       SchedulableContext sc;
       vfep.execute(sc);
    
       Test.stopTest();
        System.debug('_________________________Test Stop_______________________');
    }
    public static void setAllData(){
        /*To create one approval*/
        //Team_Assignment_Governance_Settings__c governance = new Team_Assignment_Governance_Settings__c();
        // governance.New_Acct_Reason_Code__c = 'test';
        // insert governance;
         /*
         ApprovalProcessScheduleJob__c csetting = new ApprovalProcessScheduleJob__c();
         csetting.Name = 'Days';
         csetting.ApprovalProcessDays__c = -1;
         insert csetting;
         
         Assignment_Request__c assignReq = new Assignment_Request__c();
         assignReq.Approval_Override__c = true;
         assignReq.Request_Type__c = 'Owner';
         assignReq.Status__c = 'In-Progress';
         insert assignReq;
         
         Assignment_Request_Item__c reqItem = new Assignment_Request_Item__c();
         reqItem.Assignment_Request__c = assignReq.id;
         reqItem.Approval_Override__c = assignReq.Approval_Override__c;
         reqItem.Business_Case__c = 'test';
         reqItem.Reason_Code__c = '4 - New Account Creation';
         reqItem.Status__c = 'Submitted';
         reqItem.Requested_Item_Type__c='MSD Member';
         reqItem.Member__c = UserInfo.getUserId();
         reqItem.Current_Owner__c= UserInfo.getUserId();  
         insert reqItem;
        ****************************************************************/
      
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('Carrier Account');
        a.BillingStreet = 'Street'; a.Account_Status__c = 'Assigned';
        a.BillingCity = 'Ontario';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'A9A 9A9';
        a.BillingState = 'ON';
        a.ParentId = null;
        a.Pre_approved_pricing__c=False;
        insert a;    
        
        o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.Unified_Comm_Collaboration_Estimated__c =20;
        o.AccountId = a.id;
        o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.today();
        insert o;
        
        
        
        Pricebook2 sp = new Pricebook2();
        sp = [select id from Pricebook2 where IsStandard = true limit 2];
        
        q = new Quote(Name='q1', actualTerm__c = '12', Term__c='12-Months', Pricebook2Id = sp.id, OpportunityId = o.id, requestExtension__c = true);
        Product2 testProd2= new Product2(Name = 'testName');
        insert testProd2;
        insert q;
        List <RecordType > lRtp = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Product2']);
        Map <string,Id> mapRTp = new Map <string,id> ();
        
        for (RecordType rtp : lRtp){
            mapRTp.put(rtp.Name,rtp.id);  
        }
               
        Product2 pInstall  = new Product2();
        pInstall.IsActive = true;
        pInstall.Name = 'Fibre';
        pInstall.Charge_Type__c = 'NRC';
        pInstall.Access_Type__c = 'Ethernet EON';
        pInstall.Access_Type_Group__c = 'Fibre';
        pInstall.Category__c = 'Install';
        pInstall.Service_Type__c = '10 Mbps - Ethernet Fibre Network Install - Test';
        pInstall.RecordTypeId = mapRTp.get('Enterprise Products');
        pInstall.Service_Term__c = '12-Months';
        pInstall.Mark_Up_Factor__c = 1.0;
        pInstall.Start_Date__c = Date.today();
        pInstall.End_Date__c = Date.today()+1;
        insert pInstall;
        ServiceableLocation__c sl2 = new ServiceableLocation__c();
        sl2.Street_Name__c = 'Somewhere';
        sl2.Street_Number__c  = '5';
        sl2.Street_Type__c  = 'Ave';
        sl2.City__c = 'Coty';
        sl2.Postal_Code__c = 'A1A1A1';
        sl2.Province_Code__c = 'ON';
        sl2.CLLI_Code__c = '123113';
        sl2.Access_Type_Group__c = 'Fibre';
        sl2.Access_Type__c = 'ETHERNET EON;ETHERNET CAP';
        insert sl2;
        id sl2Id = sl2.Id;
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Somewhere1';
        s1.Suite_Floor__c = 'test1';
        s1.Street_Number__c  = '5';
        s1.City__c = 'Coty';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.CLLI_SWC__c = '123113';
        s1.ServiceableLocation__c = sl2Id;
        s1.Opportunity__c = o.id;
        insert s1;
        id site1Id = s1.Id;
        PricebookEntry pbeNonStandardInstall = new PricebookEntry();
        pbeNonStandardInstall.Pricebook2Id = sp.id;
        pbeNonStandardInstall.UnitPrice = 50;
        pbeNonStandardInstall.Product2Id = pInstall.id;
        pbeNonStandardInstall.IsActive = true;
        pbeNonStandardInstall.UseStandardPrice = false;
        insert pbeNonStandardInstall;
        Id priceBookEntryInstallId1 = pbeNonStandardInstall.Id;
        QuoteLineItem testQuoteLineItem = new QuoteLineItem(QuoteId = q.Id,  PricebookEntryId = priceBookEntryInstallId1, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = false, discount=20,isMediumDiscount__c=1);
        insert testQuoteLineItem;

        if([select count() from ProcessInstance where targetobjectid=:q.id] < 1)
        {       
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();

            req.setComments('Approved');
            req.setObjectId(q.Id);
            req.setSubmitterId(UserInfo.getUserId());
            req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
            req.setProcessDefinitionNameOrId('Disc_Carrier_Approval_Process');
            req.setSkipEntryCriteria(true);
            String CRON_EXP = '0 0 0 15 3 ? 2022';
            String jobId = System.schedule('ScheduleApexClassTest',CRON_EXP,new ScheduleDiscountReportProcessing());
            Approval.ProcessResult result = Approval.process(req);
            Approval.ProcessWorkitemRequest pwr = new Approval.ProcessWorkitemRequest();
            pwr.setAction('Approve');
            System.debug(req);
            System.debug(result);

        }
        List<ProcessInstance> pros = [SELECT Id, LastModifiedDate FROM ProcessInstance limit 1];
        sqah = new Scheduled_Quote_Approval_History__c();
        sqah.Quote__c = q.id;
        if(pros != null)
            sqah.ProcessInstanceId__c = pros[0].id;
        else
            sqah.ProcessInstanceId__c = 'test';
        sqah.Approval_History__c = 'In-Progress';
        insert sqah;
         
        sqah1 = new Scheduled_Quote_Approval_History__c();
        sqah1.Quote__c = q.id;
        if(pros != null)
            sqah1.ProcessInstanceId__c = pros[0].id;
        else
            sqah1.ProcessInstanceId__c = 'test';
        sqah1.Approval_History__c = 'In-Progress';
        insert sqah1;
       
     /*    List<Scheduled_Quote_Approval_History__c> newList = new List<Scheduled_Quote_Approval_History__c>(sqah);
         updateApprovalHistory UAH = new updateApprovalHistory(newList);
       */ 
    }
}