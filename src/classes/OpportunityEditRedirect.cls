public class OpportunityEditRedirect {
    
    public String newType {get; set;}
    public String accountID {get; set;}
    public String accID {get; set;}
 
    public OpportunityEditRedirect(ApexPages.StandardController controller) {
        this.controller = controller;
        Opportunity opp = (Opportunity)controller.getRecord();
        rType = opp.RecordTypeId; 
        newType = ApexPages.currentPage().getParameters().get('RecordType');
        accID = ApexPages.currentPage().getParameters().get('def_account_id');
        accountID = ApexPages.currentPage().getParameters().get('accid');
    }
 
    public PageReference redirect(){
    
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }

        PageReference returnURL;
        Opportunity o = null;
        try{
            o = [Select id, recordtypeid From Opportunity Where Id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1];
        }catch (Exception ex){
            System.debug('OpportunityEditRedirect.redirect(): Opportunity does not exist!');
            return null;
        }
        
      
        
        // Redirect if Record Type corresponds to custom VisualForce page
       if(!Utils.isEmpty(rType) && rType.contains(mapRTo.get('Wireline - Enterprise Contract Renewal'))) //* Changed from 'Enterprise Contract Renewal' to 'Wireline - Enterprise Contract Renewal' made by Aakanksha on 16th March 2015 */
       {
            returnURL = new PageReference('/apex/opportunityRenewalEditPage');
            
            if(accountID != null){
                returnURL.getParameters().put('accid', AccountID);
            }
            
            if(accID != null){
                returnURL.getParameters().put('def_Account_id', accID);
            }
            returnURL.getParameters().put('RecordType', newType); 
        }else{
             returnURL = new PageReference('/' + o.id + '/e');
             returnURL.getParameters().put('nooverride', '1');
             returnURL.getParameters().put('RecordType', newType); 
        }
     
        returnURL.getParameters().put('id', o.id);
         
        return returnURL.setRedirect(true);
    }
 
    private final ApexPages.StandardController controller;
    public String rType {get; set;}

}