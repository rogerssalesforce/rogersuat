/*
===============================================================================
 Class Name   : Test_TAG_WireLineCon_ATM_Access
===============================================================================
PURPOSE:    This is a Test class for TAG_WireLineCon_ATM_Access Trigger.
              
Developer: Jayavardhan
Date: 03/05/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
03/05/2015           Jayavardhan               Original Version
===============================================================================*/
@isTest (seeAllData=False) 
private class Test_TAG_WireLineCon_ATM_Access{
     private static Account ParentAcc;
     private static Account childAcc;
     private static List<AccountTeamMember> parentAccTeam;
     private static List<Wireline_Contract__c> wirelineObjList;
     private static Profile pEmp = [Select Id from Profile where Name Like '%Rogers%' limit 1];
     private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
     private static User userRec;
     private static User userRec2;
     //create Test Data
     private static void setUpData(){
        TAG_CS.Team_Custom_Object_Access__c='Read';
        TAG_CS.Unassigned_User__c ='Unassigned User';
         insert TAG_CS;
        userRec = new User(LastName = 'Mark O’BrienRoger', Alias = 'alRoger', Email='test@Rogertest.com', Username='test123@Rogertest.com', CommunityNickname = 'nickRoger123', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec;
        userRec2 = new User(LastName = 'Mark O’BrienRoger2', Alias = 'aRoger', Email='test@Rogertest.com', Username='test456@Rogertest2.com', CommunityNickname = 'nickRoger456', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec2;
        
        ParentAcc= new Account(Name='ParentAcc');
        ParentAcc.Account_Status__c = 'Unassigned';
        ParentAcc.ParentID= null;
        ParentAcc.OwnerId= userRec.id;
        ParentAcc.BillingPostalCode = 'A1A 1A1';
        
        insert ParentAcc;
        parentAccTeam = new List<AccountTeamMember>();
        AccountTeamMember newMem = new AccountTeamMember();
        newMem.TeamMemberRole = 'Owner';
        newMem.AccountId = ParentAcc.Id;
        newmem.UserId = ParentAcc.OwnerId; 
        parentAccTeam.add(newMem);
        
        AccountTeamMember newMem2 = new AccountTeamMember();
        newMem2 .TeamMemberRole = 'SDC';
        newMem2 .AccountId = ParentAcc.Id;
        newMem2 .UserId = userRec.Id; 
        parentAccTeam.add(newMem2 );
        
        AccountTeamMember newMem3 = new AccountTeamMember();
        newMem3.TeamMemberRole = 'ISR - Large';
        newMem3.AccountId = ParentAcc.Id;
        newMem3.UserId = userRec2.Id; 
        parentAccTeam.add(newMem3);
        
        insert parentAccTeam;   
        
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with WirelineContract obj records created with account team members
    *******************************************************************************************************/
    static testmethod void testAccountTeamMemberAccess(){
        setUpData(); 
        wirelineObjList = new List<Wireline_Contract__c>();
        Wireline_Contract__c cont1 = new Wireline_Contract__c();
        cont1.Name = 'Cont1';
        cont1.Account_Name__c = ParentAcc.Id;
        cont1.Status__c = 'Signed';
        cont1.Contract_Start_Date__c = system.today() ;
        cont1.Contract_Term_months__c = 1;
        wirelineObjList.add(cont1);
        
        Wireline_Contract__c cont2 = new Wireline_Contract__c();
        cont2.Name = 'cont2';
        cont2.Account_Name__c = ParentAcc.Id;
        cont2.Status__c = 'Signed';
        cont2.Contract_Start_Date__c = system.today().addDays(+5) ;
        cont2.Contract_Term_months__c = 2;
        wirelineObjList.add(cont2);
        
        insert wirelineObjList;
        
        //system.assert(cont2.Account_Name__r.=True);

    }
    
    
    
    
}