@isTest(SeeAllData=true)
private class Shift_eBonding_Test {

    @isTest static void testQuoteWebService() {

        String CustomerName = 'Shift Test' + System.Now();
        String CustomerStreet = '1000 6TH ST E';
        String CustomerCity = 'OWEN SOUND';
        String CustomerProvince = 'ON';
        String CustomerCountry = 'CANADA';
        String TerminationStreet = '1111 6TH ST E';
        String TerminationCity = 'OWEN SOUND';
        String TerminationProvince = 'ON';
        String TerminationCountry = 'CANADA';       
        String Product = 'YLE';
        String ProductBandwidth = 'SM@RT';
        String Access = 'ON NET';
        String ContractTerm = '1-Month';            
        
        String  gblCustomerAddress = CustomerStreet + CustomerCity + CustomerProvince + CustomerCountry;
        
        //Query out the pricebook
        Pricebook2 tempPB = [SELECT Id, Name from Pricebook2 where isStandard = true and isActive=true LIMIT 1];
        Pricebook2 tempCarrierPB = [SELECT Id, Name from Pricebook2 where Name LIKE '%Carrier%' LIMIT 1];

        Account tempAcc = new Account(
            Name = 'tempAccount',
            eBonding_User__c = UserInfo.getUserId(),
            Pricebook__c = tempCarrierPB.Name,
            BillingStreet = '123 Main',
            BillingCity = 'Toronto',
            BillingState = 'ON',
            BillingCountry = 'Canada',
            BillingPostalCode = 'A9A 9A9',
            ParentId = null,
            Account_Status__c = 'Assigned'
        );
        insert tempAcc;
        system.debug('tempAcc :'+tempAcc);

         List<ServiceableLocation__c> tempCustLocList = new List<ServiceableLocation__c>([SELECT Id, Province_Code__c, City__c, Country__c FROM ServiceableLocation__c WHERE eBonding_Key__c = :gblCustomerAddress LIMIT 1]);
        
        List<ServiceableLocation__c> ListService = new List<ServiceableLocation__c>();
        ServiceableLocation__c sl1 =  new ServiceableLocation__c(
            Street_Number__c = '1000',
            Street_Name__c = '6TH',
            Street_Type__c = 'ST',
            Street_Direction__c = 'E',
            City__c = 'OWEN SOUND',
            Country__c = 'CANADA',
            Province_Code__c = 'ON',
            CLLI_Code__c = 'OWSDON59'
        //    eBonding_Key__c = gblCustomerAddress
        );
        //insert sl1;
        ListService.add(sl1);
        system.debug('sl1 :'+sl1);

        ServiceableLocation__c sl2 =  new ServiceableLocation__c(
            Street_Number__c = '1111',
            Street_Name__c = '6TH',
            Street_Type__c = 'ST',
            Street_Direction__c = 'E',
            City__c = 'OWEN SOUND',
            Province_Code__c = 'ON',
            Country__c = 'CANADA',
            CLLI_Code__c = 'OWSDON59' 
        
        );      
       // insert sl2;
        ListService.add(sl2);
        insert  ListService;
        system.debug('sl2 :'+sl2);

        Product2 tempMRCProduct = new Product2(
            Name = 'YLE',
            Mark_Up_Factor__c = 20.00,
            Start_Date__c = Date.Today(),
            End_Date__c = Date.Today()+100,
            Service_Type__c = 'SM@RT', 
            Access_Type__c =  'ON NET',
            Service_Term__c = '1-Month',
            Region_Pricing__c = 'Intra',
            Charge_Type__c = 'MRC',
            isActive = true
        );
        insert tempMRCProduct;
        system.debug('tempMRCProduct :'+tempMRCProduct);

        Product2 tempNRCProduct = new Product2(
            Name = 'YLE',
            Mark_Up_Factor__c = 20.00,
            Start_Date__c = Date.Today(),
            End_Date__c = Date.Today()+100,
            Service_Type__c = 'SM@RT', 
            Access_Type__c =  'ON NET',
            Service_Term__c = '1-Month',
            Region_Pricing__c = 'Metro',
            Charge_Type__c = 'NRC',
            isActive = true
        );  
        insert tempNRCProduct;
        system.debug('tempNRCProduct :'+tempNRCProduct);
        
        Product2 tempMRCProduct1 = new Product2(
            Name = 'YLE',
            Mark_Up_Factor__c = 20.00,
            Start_Date__c = Date.Today(),
            End_Date__c = Date.Today()+100,
            Service_Type__c = 'SM@RT', 
            Access_Type__c =  'ON NET',
            Service_Term__c = '1-Month',
            Region_Pricing__c = 'Inter',
            Charge_Type__c = 'MRC',
            isActive = true
        );
        insert tempMRCProduct1;

        Pricebookentry MRCStdpbe = new Pricebookentry(
            Product2Id = tempMRCProduct.Id,
            Pricebook2Id = tempPB.Id,
            UnitPrice = 100.00,
            isActive = true,
            UseStandardPrice = false            
        );
        Insert MRCStdpbe;
        system.debug('MRCStdpbe :'+MRCStdpbe);

        Pricebookentry NRCStdpbe = new Pricebookentry(
            Product2Id = tempNRCProduct.Id,
            Pricebook2Id = tempPB.Id,   
            UnitPrice =  100.00,
            isActive = true,
            UseStandardPrice = false
        );
        Insert NRCStdpbe;
        system.debug('NRCStdpbe :'+NRCStdpbe);

        Pricebookentry MRCpbe = new Pricebookentry(
            Product2Id = tempMRCProduct.Id,
            Pricebook2Id = tempCarrierPB.Id,
            UnitPrice = 100.00,
            isActive = true,
            UseStandardPrice = false            
        );
        Insert MRCpbe;
        system.debug('MRCpbe :'+MRCpbe);

        Pricebookentry NRCpbe = new Pricebookentry(
            Product2Id = tempNRCProduct.Id,
            Pricebook2Id = tempCarrierPB.Id,    
            UnitPrice =  100.00,
            isActive = true,
            UseStandardPrice = false            
        );
        Insert NRCpbe;
        system.debug('NRCpbe :'+NRCpbe);    

        Map<Integer, String> errorCodeMap = Shift_eBonding_WebService.errorCodeMap;

        //Get the xml response  for a successful request
        String XMLResponse = Shift_eBonding_WebService.getQuoteXML(CustomerName, CustomerStreet, CustomerCity, CustomerProvince, CustomerCountry, 
                                                                    TerminationStreet, TerminationCity, TerminationProvince, TerminationCountry, Product, ProductBandwidth, Access, ContractTerm);
        system.debug('XMLResponse :' + XMLResponse);
        //Query out the sucess log
        List<eBonding_Request__c> successLogList = [SELECT MRC_Product__c, NRC_Product__c, Pricebook__c, XML_Response__c FROM eBonding_Request__c WHERE Requesting_Account__c = :tempAcc.Id LIMIT 1];
        if (!successLogList.isEmpty()) {
            system.assertEquals(successLogList[0].MRC_Product__c, tempMRCProduct.Id);
            system.assertEquals(successLogList[0].NRC_Product__c, tempNRCProduct.Id);
            system.assertEquals(successLogList[0].Pricebook__c, tempCarrierPB.Id);
            system.assertEquals(successLogList[0].XML_Response__c, XMLResponse);
        }

        //Get the xml response  for a failed request (incomplete parameters on termination address)
        String XMLResponseErr = Shift_eBonding_WebService.getQuoteXML(CustomerName, CustomerStreet, CustomerCity, CustomerProvince, CustomerCountry, 
                                                                    '', '', '', '', Product, ProductBandwidth, Access, ContractTerm);
        system.debug('XMLResponseErr :' + XMLResponseErr);
        //Query out the sucess log
        eBonding_Request__c errorLog = [SELECT Error_Code__c, Error_Message__c, XML_Response__c FROM eBonding_Request__c 
                                            WHERE Customer_Name__c = :CustomerName AND Error_Code__c = 600];
        system.assertEquals(errorLog.XML_Response__c, XMLResponseErr);
        system.assertEquals(errorLog.Error_Code__c, 600);
        system.debug('Error Message :'+errorLog.Error_Message__c);
    
    Shift_eBonding_WebService.generateSuccessXML();
        Shift_eBonding_WebService.logSuccessRecord(XMLResponseErr);
    }
        
}