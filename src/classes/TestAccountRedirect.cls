/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
/*
========================================================================
Date                        Name                        Status
March 2015         Aakanksha Patel            Modified(For TAG)
========================================================================
*/
@isTest
private class TestAccountRedirect {

    static Id accountId;
    static Account account1, account2;
    
    
    
    static {
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        account1 = new Account(); 
        account1.name = 'Test Act One';
        account1.Business_Segment__c = 'Alternate';
        account1.RecordTypeId = mapRTa.get('New Account');
        account1.BillingStreet = 'Street1';
        account1.BillingCity = 'MyCity';
        account1.BillingCountry = 'Canada';
        account1.BillingPostalCode = 'L1L1L1';
        account1.BillingState = 'ON';
        insert account1;
        
        account2 = new Account(); 
        account2.name = 'Test Act Two';
        account2.Business_Segment__c = 'Alternate';
        account2.RecordTypeId = mapRTa.get('New Account');  // Channel prospect account...temp put as New Account(23rd March 2015: Aakanksha)
        account2.Phone = '416-555-1212';
        account2.Website = 'www.abc123.com';
        account2.BillingStreet = 'Street1';
        account2.BillingCity = 'MyCity';
        account2.BillingCountry = 'Canada';
        account2.BillingPostalCode = 'L2L2L2';
        account2.BillingState = 'ON';
        insert account2;
       
    }

    static testMethod void newDuplicateAccountControllerTest() { 
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        Account accTest = new Account(); 
        accTest.name = 'Test Act One';
        accTest.Business_Segment__c = 'Alternate';
        accTest.RecordTypeId = mapRTa.get('New Account');
        accTest.BillingStreet = 'Street1';
        accTest.BillingCity = 'MyCity';
        accTest.BillingCountry = 'Canada';
        accTest.BillingPostalCode = 'L1L1L1';
        accTest.BillingState = 'ON';
        
        PageReference pageRef = New PageReference('/apex/addNewChannelAccount'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(accTest);
        NewAccountController controller = new NewAccountController(sc);
        controller.save();
    }
    
    static testMethod void newAccountControllerTest() { 
                
        PageReference pageRef = New PageReference('/apex/addNewChannelAccount'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(new Account());
        NewAccountController controller = new NewAccountController(sc);
        controller.save();
    }
    
    static testMethod void accountEditRedirectTest() {
        PageReference pageRef = New PageReference('/apex/accountEditRedirect?Id=' + String.valueOf(account1.id)); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(account1);
        AccountEditRedirect controller = new AccountEditRedirect(sc);
        controller.redirect();
    }
    
    static testMethod void accountChannelEditRedirectTest() {
        PageReference pageRef = New PageReference('/apex/accountEditRedirect?Id=' + String.valueOf(account2.id)); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(account2);
        AccountEditRedirect controller = new AccountEditRedirect(sc);
        controller.redirect();
    }
    
    static testMethod void accountErrorEditRedirectTest() {
        PageReference pageRef = New PageReference('/apex/accountEditRedirect'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(new Account());
        AccountEditRedirect controller = new AccountEditRedirect(sc);
        controller.redirect();
    }
    
    static testMethod void accountRedirectTest() {
       PageReference pageRef = New PageReference('/apex/accountRedirect'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(new Account());
        AccountRedirect controller = new AccountRedirect(sc);
        controller.redirect();
    }
    
        static testMethod void accountChannelRedirectTest() {
       PageReference pageRef = New PageReference('/apex/accountRedirect?RecordType=01230000000wDXR'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(account2);
        AccountRedirect controller = new AccountRedirect(sc);
        controller.redirect();
    }
}