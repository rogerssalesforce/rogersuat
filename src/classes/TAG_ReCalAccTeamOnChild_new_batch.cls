/*
===============================================================================
 Class Name   : TAG_ReCalAccTeamOnChild_new_batch (comparing team members of child and parent)
===============================================================================
PURPOSE:    This batch class updates AccountTeam after MSD and Shared MSD Owners 
            and Members have been updated, to ensure consistency.
  

Developer: Aakanksha Patel
Date: 06/20/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
06/20/2015           Aakanksha Patel             Original Version
===============================================================================
*/
global class TAG_ReCalAccTeamOnChild_new_batch implements Database.Batchable<SObject>, Database.stateful{
    
    Global String query;
    Global Set<Id> setAccountUpdate = new set<Id>();
    
    global TAG_ReCalAccTeamOnChild_new_batch(Set<Id> setAccountUpdate)
    {
        this.setAccountUpdate = setAccountUpdate;  // constructor copies in arg to local instance vbl
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        if(!Test.isRunningTest()){
        return Database.getQueryLocator('select id, ParentId, Parent.Cascade_Team_to_Child__c, OwnerId, Owner.isActive FROM Account where ParentID!=null and Parent.Cascade_Team_to_Child__c=true and parentID in : setAccountUpdate');
        }
        else{
           return Database.getQueryLocator('select id, ParentId, Parent.Cascade_Team_to_Child__c, OwnerId, Owner.isActive FROM Account where ParentID!=null and Parent.Cascade_Team_to_Child__c=true limit 10');
        }     
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope){
      
        Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
        
        AccountTeamMember atm1;
        Set<Id> AccountPIds = new Set<Id>();
        Set<Id> AccountCIds = new Set<Id>();
        Set<Id> AccTeamCIdsAMem = new Set<Id>();
        Set<Id> AccTeamCIdsRMem = new Set<Id>();
        Set<Id> lstChildAccountstoUpdate = new Set<Id>();
        set<AccountTeamMember> setAccountTeamCreate = new set<AccountTeamMember>();
        set<AccountTeamMember> setAccountTeamDelete = new set<AccountTeamMember>();
        Map<Id, Set<Id>> mapAccUserIdsToAdd = new Map<Id, Set<Id>>(); 
        Map<Id, Set<Id>> mapAccUserIdsToRemove = new Map<Id, Set<Id>>();
        List<Account> accListToUpdate = new List<Account>();
        List<AccountTeamMember> AccParentScopeTeam = new List<AccountTeamMember>();
        List<AccountTeamMember> AccChildScopeTeam = new List<AccountTeamMember>();
        
        for(Account acc: scope)
        {
             AccountPIds.add(acc.ParentId);
             AccountCIds.add(acc.id);
        }
        
        List<AccountTeamMember> alistP = new List<AccountTeamMember>();
        list<AccountTeamMember> listAccTeamP = new list<AccountTeamMember>([Select Id,AccountId,Account.id,TeamMemberRole,UserId FROM AccountTeamMember where (TeamMemberRole = 'Owner' OR TeamMemberRole = 'MSD Owner' OR TeamMemberRole = 'MSD Member') AND AccountId in :AccountPIds]);
        Map<id,List<AccountTeamMember>> maTeamP= new  Map<id,List<AccountTeamMember>>();  //Account and all its AccountTeamMembers with role condition for Parent
         for(AccountTeamMember m : listAccTeamP)
         {
             alistP= maTeamP.get(m.AccountId);
             if(alistP==null)
             {
                 alistP=new List<AccountTeamMember>();
             }
             alistP.add(m);
             maTeamP.put(m.AccountId,alistP);
         }
         
        List<AccountTeamMember> alistC = new List<AccountTeamMember>();
        list<AccountTeamMember> listAccTeamC = new list<AccountTeamMember>([Select Id,AccountId,Account.id,TeamMemberRole,UserId FROM AccountTeamMember where (TeamMemberRole = 'Owner' OR TeamMemberRole = 'MSD Owner' OR TeamMemberRole = 'MSD Member') AND AccountId in :AccountCIds]);
        Map<id,List<AccountTeamMember>> maTeamC= new  Map<id,List<AccountTeamMember>>();  //Account and all its AccountTeamMembers with role condition for child
        
         for(AccountTeamMember mc : listAccTeamC)
         {
             alistC= maTeamC.get(mc.AccountId);
             if(alistC==null)
             {
                 alistC=new List<AccountTeamMember>();
             }
             alistC.add(mc);
             maTeamC.put(mc.AccountId,alistC);
         }
        String strURchild;
        String strURparent;
        
        for(Account accScope : scope) 
        {
            AccParentScopeTeam = maTeamP.get(accScope.ParentId); 
            AccChildScopeTeam = maTeamC.get(accScope.id);
        
            if((AccChildScopeTeam!=null && AccChildScopeTeam.size()!=null) || (AccParentScopeTeam!=null && AccParentScopeTeam.size()!=null))
            {
            // If team member is in Child but not on Parent(Remove)
            for(integer i=0; i<AccChildScopeTeam.size(); i++)
            {
                    strURchild = AccChildScopeTeam[i].UserId + AccChildScopeTeam[i].TeamMemberRole;
                     for(integer j=0; j<AccParentScopeTeam.size(); j++)
                    {
                        strURparent = strURparent + AccParentScopeTeam[j].UserId + AccParentScopeTeam[j].TeamMemberRole;
                    }
                    system.debug('*********strURparent******'+strURparent);
                    if(!strURparent.contains(strURchild))
                    {
                        //Remove the team Member and remove access
                        setAccountTeamDelete.add(AccChildScopeTeam[i]);
                        
                        AccTeamCIdsRMem = mapAccUserIdsToRemove.get(AccChildScopeTeam[i].Account.Id);
                        if(AccTeamCIdsRMem==null)
                             AccTeamCIdsRMem = new Set<Id>();
                        mapAccUserIdsToRemove.put(AccChildScopeTeam[i].Account.Id,AccTeamCIdsRMem);
                        lstChildAccountstoUpdate.add(AccChildScopeTeam[i].Account.Id);
                    }
                    
                    strURchild = '';
                    strURparent= '';
           
            }
            }
            
            if(AccParentScopeTeam!=null && AccParentScopeTeam.size()!=null)
            {
            // If team member is in Parent but not on Child(Add)    
            for(integer i1=0; i1<AccParentScopeTeam.size(); i1++)
            {
                    strURparent = AccParentScopeTeam[i1].UserId + AccParentScopeTeam[i1].TeamMemberRole;
                    system.debug('*********strURparent(ADD)******'+strURparent);
                    if(AccChildScopeTeam!=null && AccChildScopeTeam.size()!=null)
                    for(integer j1=0; j1<AccChildScopeTeam.size(); j1++)
                    {
                        strURchild = strURchild + AccChildScopeTeam[j1].UserId + AccChildScopeTeam[j1].TeamMemberRole;
                    }
                    system.debug('*********strURchild(ADD)******'+strURchild);
                    if(!strURchild.contains(strURparent))
                    {
                        //Create the team Member and give access
                        atm1 = new AccountTeamMember();
                        atm1.AccountId = accScope.id;
                        atm1.UserId=AccParentScopeTeam[i1].UserId;
                        atm1.TeamMemberRole = AccParentScopeTeam[i1].TeamMemberRole;
                        setAccountTeamCreate.add(atm1);
                        system.debug('*********setAccountTeamCreate(ADD)******'+setAccountTeamCreate);
                        
                        AccTeamCIdsAMem = mapAccUserIdsToAdd.get(atm1.Account.Id);
                        If(AccTeamCIdsAMem==null)
                             AccTeamCIdsAMem = new Set<Id>();
                        AccTeamCIdsAMem.add(atm1.UserId);
                        mapAccUserIdsToAdd.put(atm1.Account.Id,AccTeamCIdsAMem);
                        lstChildAccountstoUpdate.add(atm1.Account.Id);

                    }
                    strURchild = '';
                    strURparent= '';
            }
            }   
        }
        update scope;
        
        
        
        
        
        if(lstChildAccountstoUpdate!=null && lstChildAccountstoUpdate.size()>0)
        {
            List<Account> lstChildUpdate =[Select id,Account_Team_to_MAL__c,Recalculate_Team__c,District__c,Parent.District__c,ParentId,Parent.Account_Status__c,OwnerId from Account where Id in :lstChildAccountstoUpdate];  //**---- update list here ----****
            for(Account acc : lstChildUpdate)
            {
                acc.Recalculate_Team__c = false;
                acc.Account_Team_to_MAL__c = true;
                acc.Account_Status__c = acc.Parent.Account_Status__c;
                acc.District__c = acc.Parent.District__c;
                accListToUpdate.add(acc);
            }
        }
       
       Set<Account> setAcctoUpdate = new Set<Account>();
        //Update Recalculate_Team__c flag to false
        if(accListToUpdate.size()>0)
        {
            setAcctoUpdate.addAll(accListToUpdate);
            accListToUpdate.clear();
            accListToUpdate.addAll(setAcctoUpdate);
            
            update accListToUpdate;
        }
        
        if(setAccountTeamCreate.size()!=NULL)
        {
            List<AccountTeamMember> listAccountTeamCreate1 = new List<AccountTeamMember>();
            listAccountTeamCreate1.addAll(setAccountTeamCreate);
            set<AccountTeamMember> temp = new set<AccountTeamMember>(listAccountTeamCreate1);

            listAccountTeamCreate1.clear();
            listAccountTeamCreate1.addAll(temp);
            insert listAccountTeamCreate1;                    
        }
        
        if(setAccountTeamDelete.size()!=NULL)
        {   try
            {
                List<AccountTeamMember> listAccountTeamDelete = new List<AccountTeamMember>();
                listAccountTeamDelete.addAll(setAccountTeamDelete);
                set<AccountTeamMember> temp = new set<AccountTeamMember>(listAccountTeamDelete);

                listAccountTeamDelete.clear();
                listAccountTeamDelete.addAll(temp);
                system.debug('*********listAccountTeamDelete******'+listAccountTeamDelete);
                Database.delete(listAccountTeamDelete,false);
                        
            }catch(exception e)
            {
                System.debug('TAF Batch :Error while deleting : setAccountTeamDelete' +setAccountTeamDelete);
            }
        }
        
        //Call updateAccountTeamAccess class to Give/Remove access from Users after updating AccountTeam
        updateAccountTeamAccess accessCls = new updateAccountTeamAccess();
        accessCls.removeAccessFromTeam(mapAccUserIdsToRemove);
        accessCls.giveAccesstoTeam(mapAccUserIdsToAdd);
       
       
    }
    
    global void finish(Database.BatchableContext BC)
    {       
        Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
        Integer batchSize = Integer.ValueOf(TAGsettings.UpdAccCasFlagBatchSize__c);
        
        TAG_UpdateAccCascadeFlag_batch sc1 = new TAG_UpdateAccCascadeFlag_batch(setAccountUpdate);
        ID batchprocessid = Database.executeBatch(sc1,batchSize);
    }
}