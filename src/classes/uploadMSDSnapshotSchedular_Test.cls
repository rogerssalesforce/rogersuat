/*
===============================================================================
Class Name : uploadMSDSnapshotSchedular_Test
===============================================================================
PURPOSE: This is a test class for uploadMSDSnapshotSchedular class

COMMENTS: 

Developer: Deepika Rawat
Date: 11/11/2013


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
18/9/2013               Deepika               Created
===============================================================================

*/
@isTest(SeeAllData = true)
private class uploadMSDSnapshotSchedular_Test{
 static testmethod void testMethod1(){
                
        Test.StartTest();
        uploadMSDSnapshotSchedular obj= new uploadMSDSnapshotSchedular();
        String sch = '0 0 23 * * ?';
        system.schedule('msdSnapshot', sch, obj);
        System.assert(true);
        Test.stopTest();    

    }
}