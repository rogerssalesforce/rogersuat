/*
===============================================================================
 Class Name   : itRequirements_batch_scheduler_test
===============================================================================

Jacob Klay, 11/11/2014

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
===============================================================================
*/
@isTest(SeeAllData = true)
private class itRequirements_batch_scheduler_test{
 static testmethod void testMethod1(){        
        Test.StartTest();
        itRequirements_batch_scheduler obj= new itRequirements_batch_scheduler();
        String sch = '0 0 23 * * ?';
        system.schedule('ITReqs', sch, obj);
        System.assert(true);
        Test.stopTest();    
    }
}