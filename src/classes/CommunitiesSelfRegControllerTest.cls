/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
@IsTest public with sharing class CommunitiesSelfRegControllerTest {
    @IsTest(SeeAllData=true) 
    public static void testCommunitiesSelfRegController() {
        CommunitiesSelfRegController controller = new CommunitiesSelfRegController();
        //controller.confirmPassword ='false';
        controller.registerUser();
    }
    /*public static void testCommunitiesSelfRegController2() {
        CommunitiesSelfRegController controller = new CommunitiesSelfRegController(); 
        controller.confirmPassword ='true';       
        controller.registerUser();
    } */     
}