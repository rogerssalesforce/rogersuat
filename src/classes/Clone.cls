/*
 * Default implementation of third-party open source deep clone solution:
 *
 * http://code.google.com/p/salesforce-super-clone/source/browse/trunk/%20salesforce-super-clone/Google%20code%20Super%20Clone/src/classes/Clone.cls
 * r3 by jinghaili on Jul 10, 2011 
 * 
 */
//The top class needs to be global as this needs to be accessible for API call
global class Clone extends AbstractClone {
    
}