/*********************************************************************************
Class Name      : CPQ_Test_ProposalTrigger
Description     : This class a test class for CPQ_ProposalTrigger. 
Created By      : Akshay Ahluwalia
Created Date    : 24-Sep-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Akshay Ahluwalia          24-Sep-15             Original version
Alina Balan               30-October-2015     Added testSetSharingsRules, createQuotesAndRelatedData methods
Ramiya Kandeepan          18-Nov-2015          Added Method testUpdatePriceListBasedOnOpportunity 
Deepika Rawat             7-Dec-2015          Added Method testUpdateQuoteStatus
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest (seeAllData=False) 
private class CPQ_Test_ProposalTrigger{
    static User TestAEUser;
    static Account oAccount;
    static Account ocarrierAccount;
    static Account oIntercoAccount;
    static Account oIDVaccount;
    static Opportunity oOpportunity;
    static Opportunity IDVOpportunity;
    static Opportunity oIntercoOpportunity;
    static Opportunity oCarrierOpp;
    //static Apttus_Proposal__Proposal__c oQuote;
    static Apttus_Config2__PriceList__c oPriceList ;
    static Apttus_Config2__PriceList__c oEBUPriceList;
    static Apttus_Config2__PriceList__c EnterprisePriceList;
    static Apttus_Config2__PriceList__c IDVPriceList;
    static Apttus_Config2__PriceList__c IntercoPriceList;
    static Apttus_Config2__PriceList__c CarrierPriceList;
    private static CPQ_Settings__c csMapping;
    private static final Integer CPQ_QUOTES_TOTAL_NUMBER = 200;
    private static final String CPQ_PROPOSAL_PROFILES_SETTING_VALUE = 'EBU - Rogers Account Executive|Read|EBU - Rogers Specialist|Read';
    private static final String CPQ_ACCOUNT_EXECUTIVE = 'EBU - Rogers Account Executive';
    private static final String CPQ_SYSTEM_ADMIN = 'System Administrator';
    private static final String CPQ_ADMIN_USERNAME = 'standarduser_test123@testorg.com';

    /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
     static testMethod void createTestData() {
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');      
        system.runAs(TestAEUser){
            //Create PriceList
            oPriceList = CPQ_Test_Setup.newPriceList();
            oPriceList.name = 'Test Carrier Price List';
            insert oPriceList ;
            
            oEBUPriceList = CPQ_Test_Setup.newPriceList();
            oEBUPriceList.name = 'Test EBU price List';
            insert oEBUPriceList ;
            
            IDVPriceList = CPQ_Test_Setup.newPriceList();
            IDVPriceList.name = 'IDV';
            insert IDVPriceList;
            
            EnterprisePriceList = CPQ_Test_Setup.newPriceList();
            EnterprisePriceList.name = 'Enterprise';
            insert EnterprisePriceList;
            
            IntercoPriceList = CPQ_Test_Setup.newPriceList();
            IntercoPriceList.name = 'Interco';
            insert IntercoPriceList;
            
            CarrierPriceList = CPQ_Test_Setup.newPriceList();
            CarrierPriceList.name = 'Wholesale';
            insert CarrierPriceList;
            
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            Id CarrierRecordId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Carrier Account').getRecordTypeId();
            ocarrierAccount = CPQ_Test_Setup.newAccount('Test Account');
            ocarrierAccount.recordTypeId = CarrierRecordId;
            oCarrierAccount.Price_List__c = CarrierPriceList.id;
            Id NewAccountRecordId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('New Account').getRecordTypeId();
            insert ocarrierAccount;
            oAccount.recordTypeId = NewAccountRecordId;
            insert oAccount;
            
            oIntercoAccount = CPQ_Test_Setup.newAccount('Test Account');
            oIntercoAccount.Interco_Account__c = TRUE;
            insert oIntercoAccount;
            
            
            oIDVaccount = CPQ_Test_Setup.newAccount('Test Account');
            oIDVaccount.recordTypeId = NewAccountRecordId;
            insert oIDVaccount;             
            //Inserting Custom setting CPQ_Settings__c
            csMapping  = CPQ_Settings__c.getOrgDefaults();
            csMapping.AccountPriceListMapping__c= 'Carrier_Account:Test Carrier Price List;Dealer_Account:Test EBU price List;New_Account:Test EBU price List;Submitted_Account:Test EBU price List';
            csMapping.ProposalsProfiles__c = CPQ_PROPOSAL_PROFILES_SETTING_VALUE;
            csMapping.ProposalsProfiles2__c = '';
            csMapping.DefaultOpportunityRecordType__c = 'Rogers EBU - New';
            csMapping.OpportunityQuotePriceListMapping__c = 'IDV:IDV;PED:PED;Standard:Enterprise;Carrier:Wholesale;Interco:Interco';
            csMapping.IntercoPriceListName__c= 'Interco';
            upsert csMapping;
             
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            
            oCarrierOpp = CPQ_Test_Setup.newOpportunity (ocarrierAccount);
            insert oCarrierOpp;
            IDVOpportunity = CPQ_Test_Setup.newOpportunity(oIDVaccount);
            IDVOpportunity.Price_List__c = 'IDV';
            insert IDVOpportunity;
            
            oIntercoOpportunity = CPQ_Test_Setup.newOpportunity(oIntercoAccount);
            oIntercoOpportunity.AccountId = oIntercoAccount.Id;
            insert oIntercoOpportunity;
            
        }
    }    
    /*********************************************************************************
    Method Name    : testSelectPrimaryQuoteDoc
    Description    : Method to test user is able to select Primary doc
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testSelectPrimaryQuoteDoc(){
        createTestData();
        test.startTest();
        system.runAs(TestAEUser){
            Apttus_Proposal__Proposal__c quote= new Apttus_Proposal__Proposal__c();
            quote.Apttus_Proposal__Proposal_Name__c= 'testQuote';
            quote.Apttus_Proposal__Opportunity__c= oOpportunity.id;
            quote.Apttus_Proposal__Account__c= oAccount.id;
            quote.Apttus_Proposal__Approval_Stage__c = 'Draft';
            quote.Apttus_QPConfig__PriceListId__c= oPriceList.id ;
            insert quote;
            Quote.Apttus_Proposal__Approval_Stage__c = 'In Review';
            update quote;
            quote.Apttus_Proposal__Approval_Stage__c = 'Draft';
            update quote;
        }
        test.stopTest();
    }
    
    /*********************************************************************************
    Method Name    : testUpdatePriceListBasedOnAccount
    Description    : Method to test if PriceList is defaulted in On Proposal based on 
                     Account Record Type.
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testUpdatePriceListBasedOnAccount(){
        createTestData();
        test.startTest();
        system.runAs(TestAEUser){
        
            List<Apttus_Proposal__Proposal__c> lstquote= CPQ_Test_Setup.newLstQuote(oOpportunity, oPriceList, 300);
            Integer i = 1;
            for(Apttus_Proposal__Proposal__c quote : lstquote){
                quote.Apttus_Proposal__Account__c= oAccount.id;
                quote.Apttus_Proposal__Proposal_Name__c= 'testQuoteValue ' + i++;
            }

            insert lstquote;
            Set<Id> setInsertedQuoteIds = new Set<Id>();
            for(Apttus_Proposal__Proposal__c quote : lstquote){
                setInsertedQuoteIds.add(quote.id);
            }
            
            List<Apttus_Proposal__Proposal__c > lstinsertedQuote = [select id, name, Apttus_QPConfig__PriceListId__r.name from Apttus_Proposal__Proposal__c where id in :setInsertedQuoteIds];
            for(Apttus_Proposal__Proposal__c insertedquote : lstinsertedQuote ){
                system.debug(insertedquote.Apttus_QPConfig__PriceListId__r.name);
            }

            List<Apttus_Proposal__Proposal__c> lstCarrierquote= CPQ_Test_Setup.newLstQuote(oOpportunity, oPriceList, 300);
            for(Apttus_Proposal__Proposal__c oCarrierquote : lstCarrierquote){
                oCarrierquote.Apttus_Proposal__Account__c= ocarrierAccount.id;
                oCarrierquote.Apttus_Proposal__Proposal_Name__c= 'testQuoteCarierValue ' + i++;
            }

            insert lstCarrierquote;
            
            Set<Id> setInsertedCarrierQuoteIds = new Set<Id>();
            for(Apttus_Proposal__Proposal__c quote : lstCarrierquote){
                setInsertedCarrierQuoteIds.add(quote.id);
            }
            
            List<Apttus_Proposal__Proposal__c > lstinsertedCarrierQuote = [select id, name, Apttus_QPConfig__PriceListId__r.name from Apttus_Proposal__Proposal__c where id in: setInsertedCarrierQuoteIds];
            for(Apttus_Proposal__Proposal__c insertedCarrierquote : lstinsertedCarrierQuote){
                system.assert(insertedCarrierquote.Apttus_QPConfig__PriceListId__r.name != '');
            }
        }
    }
    
    /*********************************************************************************
    Method Name    : testSetSharingsRules
    Description    : Method used to test sharing rules acccess
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testSetSharingsRules(){
        createTestData();
        User testAdminUser = CPQ_Test_Setup.newUser(CPQ_SYSTEM_ADMIN, CPQ_ADMIN_USERNAME);
        insert testAdminUser;
        test.startTest();
        system.runAs(testAdminUser){
            createQuotesAndRelatedData();
            List<Apttus_Proposal__Proposal__c> lstProposals = [SELECT Id, Apttus_Proposal__Account__c, OwnerId FROM Apttus_Proposal__Proposal__c WHERE Apttus_Proposal__Account__c =: oAccount.Id AND Apttus_Proposal__Opportunity__c =: oOpportunity.Id];
            system.assertEquals(lstProposals.size(), CPQ_QUOTES_TOTAL_NUMBER);
            system.assertEquals(csMapping.ProposalsProfiles__c, CPQ_PROPOSAL_PROFILES_SETTING_VALUE);
            system.assertEquals(csMapping.ProposalsProfiles2__c, '');
            //Map<String, String> mapProfilesAndAccess = CPQ_ProposalTriggerHelper.getProfilesAndAccess();
            //system.assertEquals(mapProfilesAndAccess.containsKey(CPQ_ACCOUNT_EXECUTIVE), true);
            CPQ_ProposalTriggerHelper.setSharingsRules(lstProposals);
            User oUser = [SELECT Id, Profile.Name FROM User WHERE Id =: TestAEUser.Id limit 1];
            system.assertEquals(oUser.Profile.Name, CPQ_ACCOUNT_EXECUTIVE);
            Map<Id, Apttus_Proposal__Proposal__c> mapProposals = new Map<Id, Apttus_Proposal__Proposal__c>(lstProposals);
            List<Apttus_Proposal__Proposal__Share> lstProposalShares = [SELECT Id FROM Apttus_Proposal__Proposal__Share WHERE ParentID IN: mapProposals.keySet()];
            system.assertEquals(lstProposalShares.size(), CPQ_QUOTES_TOTAL_NUMBER*2);
        }
        test.stopTest();
    }
    /*********************************************************************************
    Method Name    : createQuotes
    Description    : Method used to insert a batch of quotes
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    private static void createQuotesAndRelatedData() {
        //insert account team member
        AccountTeamMember oAccountTeamMember = CPQ_Test_Setup.newAccountTeamMember(oAccount.Id, TestAEUser.Id);
        insert oAccountTeamMember;
        List<Apttus_Proposal__Proposal__c> lstProposalsToCreate = new List<Apttus_Proposal__Proposal__c>();
        //Loop through each of the records and add them to the list to be created
        for(Integer i=0; i<CPQ_QUOTES_TOTAL_NUMBER; i++){
             Apttus_Proposal__Proposal__c oQuote = CPQ_Test_Setup.newQuote(oOpportunity, oPriceList);
             lstProposalsToCreate.add(oQuote);
        }
        insert lstProposalsToCreate;
    }
 /*********************************************************************************
    Method Name    : testUpdatePriceListBasedOnOpportunity
    Description    : Method to test if PriceList is defaulted in On Proposal based on 
                     Account type and opportunity price lists.
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
 static testmethod void testUpdatePriceListBasedOnOpportunity(){
     createTestData();
     test.startTest();
     system.runAs(TestAEUser){
        Apttus_Proposal__Proposal__c quote= new Apttus_Proposal__Proposal__c();
        quote.Apttus_Proposal__Proposal_Name__c= 'testQuote';
        quote.Apttus_Proposal__Opportunity__c= ocarrierOpp.id;
        quote.Apttus_Proposal__Account__c= ocarrierAccount.id;
        insert quote;
         
        list<Apttus_Proposal__Proposal__c> proposal = [Select id, name, Apttus_QPConfig__PriceListId__r.name from Apttus_Proposal__Proposal__c where id =: quote.id];
         for(Apttus_Proposal__Proposal__c insertedquote : proposal ){
             system.assert(insertedquote.Apttus_QPConfig__PriceListId__r.name == 'Wholesale');
         }
         
        Apttus_Proposal__Proposal__c quote1= new Apttus_Proposal__Proposal__c(); 
        quote1.Apttus_Proposal__Proposal_Name__c= 'testQuote1';
        quote1.Apttus_Proposal__Opportunity__c= oIntercoOpportunity.id;
        quote1.Apttus_Proposal__Account__c= oIntercoAccount.id;
        insert quote1;
        list<Apttus_Proposal__Proposal__c> proposal1 = [Select id, name, Apttus_QPConfig__PriceListId__r.name from Apttus_Proposal__Proposal__c where id =: quote1.id];
         for(Apttus_Proposal__Proposal__c insertedquote : proposal1 ){
             system.assert(insertedquote.Apttus_QPConfig__PriceListId__r.name == 'Interco');
         } 
         
        Apttus_Proposal__Proposal__c quote2= new Apttus_Proposal__Proposal__c(); 
        quote2.Apttus_Proposal__Proposal_Name__c= 'testQuote2';
        quote2.Apttus_Proposal__Opportunity__c= IDVOpportunity.id;
        quote2.Apttus_Proposal__Account__c= oIDVaccount.id;
        insert quote2;
        list<Apttus_Proposal__Proposal__c> proposal2 = [Select id, name, Apttus_QPConfig__PriceListId__r.name from Apttus_Proposal__Proposal__c where id =: quote2.id];
        for(Apttus_Proposal__Proposal__c insertedquote : proposal2 ){
           system.assert(insertedquote.Apttus_QPConfig__PriceListId__r.name == 'IDV');
        } 
     }
    }
    /*********************************************************************************
    Method Name    : testUpdateQuoteStatus
    Description    : Method to test if PriceList is defaulted in On Proposal based on 
                     Account type and opportunity price lists.
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testUpdateQuoteStatus(){
        createTestData();
        test.startTest();
        Apttus_Proposal__Proposal__c updatedQuote = new Apttus_Proposal__Proposal__c();
        Apttus_Proposal__Proposal__c quote= new Apttus_Proposal__Proposal__c();
        quote.Apttus_Proposal__Proposal_Name__c= 'testQuote';
        quote.Apttus_Proposal__Opportunity__c= oOpportunity.id;
        quote.Apttus_Proposal__Account__c= oAccount.id;
        quote.Apttus_Proposal__Approval_Stage__c = 'Draft';
        quote.Apttus_QPConfig__PriceListId__c= oPriceList.id ;
        insert quote;
        //Update Quote To Solution Under Review
        Quote.Apttus_Proposal__Approval_Stage__c = 'Solution Under Review';
        Quote.SE_Status__c = 'Pending Review';
        Quote.Deal_Desk_Status__c = 'Pending Review';
        update quote;
        //Update Quote To SE Status = Return To requestor
        Quote.SE_Status__c = 'Return to Requester';
        update quote;
        updatedQuote = [Select Apttus_Proposal__Approval_Stage__c, Deal_Desk_Status__c from Apttus_Proposal__Proposal__c where id =:quote.id  ];
        System.assert(updatedQuote.Apttus_Proposal__Approval_Stage__c=='Draft');
        System.assert(updatedQuote.Deal_Desk_Status__c=='Return to Requester');
        //Update Quote To SE Status Review Complete Solution Under Review
        Quote.Apttus_Proposal__Approval_Stage__c = 'Solution Under Review';
        Quote.SE_Status__c = 'Review Complete';
        Quote.Deal_Desk_Status__c = 'Pending Review';
        update quote;
        //Update Quote To SE Status = Return To requestor
        Quote.Deal_Desk_Status__c = 'Return to Requester';
        update quote;
        updatedQuote = [Select Apttus_Proposal__Approval_Stage__c from Apttus_Proposal__Proposal__c where id =:quote.id  ];
        System.assert(updatedQuote.Apttus_Proposal__Approval_Stage__c=='Draft');
        //Update Quote To Solution Under Review
        Quote.Apttus_Proposal__Approval_Stage__c = 'Solution Under Review';
        Quote.SE_Status__c = 'Pending Review';
        Quote.Deal_Desk_Status__c = 'Not Required';
        update quote;
        Quote.SE_Status__c = 'Review complete';
        update quote;
        updatedQuote = [Select Apttus_Proposal__Approval_Stage__c from Apttus_Proposal__Proposal__c where id =:quote.id  ];
        System.assert(updatedQuote.Apttus_Proposal__Approval_Stage__c=='Approval Required');
        Quote.Apttus_Proposal__Approval_Stage__c = 'Approved';
        update quote;
        Quote.Apttus_Proposal__Approval_Stage__c = 'Draft';
        update quote;
    } 
}