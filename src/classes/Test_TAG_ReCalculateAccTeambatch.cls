/*
===============================================================================
 Class Name   : Test_TAG_ReCalculateAccTeambatch
===============================================================================
PURPOSE:    This class a controller class for TAG_ReCalculateAccountTeam_batch.
              
Developer: Deepika Rawat
Date: 02/05/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
02/05/2015           Deepika Rawat               Original Version
===============================================================================*/
@isTest (seeAllData=False) 
private class Test_TAG_ReCalculateAccTeambatch{

    private static  Set<Id> setAccountUpdate = new set<ID>();
    private static Account ParentAcc;
    private static Account ParentAcc2;
    private static Account ParentAcc3;
    private static Account ChildAcc1;
    private static Profile pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
    private static User userRec;
    private static User userRec2;
    private static User userRec3;
    private static  MSD_Code__c msdCode; 
    private static Shared_MSD_Code__c sharedMSD;
    private static Shared_MSD_Accounts__c sharedAccMSD;
    private static List<MSD_Member__c> msdMems;
    private static List<Shared_MSD_Member__c> sharedMSDMem;
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
    private static Static_Data_Utilities__c staticData_PageSize = new Static_Data_Utilities__c ();
    private static Static_Data_Utilities__c staticData_Limit = new Static_Data_Utilities__c ();
     //create Test Data
     private static void setUpData(){
     
    
     
        //Data for Custom Settings
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        TAG_CS.Team_Account_Access__c='Read';
        TAG_CS.Team_Custom_Object_Access__c='Read';
        TAG_CS.MSD_Recalcuate_Account_Team_Job__c ='MSD Owner,Owner,MSD Member';
        TAG_CS.Default_Account_Owner_Role__c='Owner';
        TAG_CS.Default_MSD_Member_Role__c='MSD Member';
        TAG_CS.Default_MSD_Owner_Role__c='MSD Owner';
        TAG_CS.GiveAccToNewTeamBatchSize__c = 10;
        TAG_CS.ReCalAccTeamSchChild1BatchSize__c = 10;
        TAG_CS.UpdAccCasFlagBatchSize__c = 10;
        insert TAG_CS;
        staticData_PageSize.Name = 'AccountTransfer_PageSize';
        staticData_PageSize.Value__c ='1';
        insert staticData_PageSize;
        staticData_Limit.Name='AccountTransfer_RecordsLimit';
        staticData_Limit.Value__c='2';
        insert staticData_Limit;
        
        system.debug('pEmp***'+pEmp);

         userRec = new User(LastName = 'Mark O’Roger', Alias = 'alRoger1', Email='test@Rogertest.com', Username='test@Rogertest1.com', CommunityNickname = 'nickRog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec;
        userRec2 = new User(LastName = 'Mark O’Roger2', Alias = 'aRoger2', Email='test@Rogertest.com', Username='test@Rogertest2.com', CommunityNickname = 'nickRog2', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',  Assignment_Approver__c = userRec.id,Channel__c='Business Segment',Owner_Type__c='District');
        insert userRec2;

         userRec3 = new User(LastName = 'Mark O’Roger3', Alias = 'aRoger3', Email='test@Rogertest.com', Username='test@Rogertest3.com', CommunityNickname = 'nickRog3', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Assignment_Approver__c = userRec2.id,Channel__c='Business Segment');
        insert userRec3;

        //Account 1
        ParentAcc= new Account();
        ParentAcc.Name = 'ParentAcc';
        ParentAcc.Account_Status__c = 'Assigned';
        ParentAcc.ParentID= null;
        ParentAcc.OwnerId= userRec.id;
        ParentAcc.Recalculate_Team__c=true;
        ParentAcc.Account_Team_to_MAL__c=false;
        ParentAcc.BillingPostalCode = 'A1A 1A1';
        insert ParentAcc;
        setAccountUpdate.add(ParentAcc.id);
        
        //child of ParentAcc
        ChildAcc1= new Account();
        ChildAcc1.Name = 'ChildAcc1';
        ChildAcc1.Account_Status__c = 'Assigned';
        ChildAcc1.ParentID= ParentAcc.Id;
        ChildAcc1.OwnerId= userRec.id;
        ChildAcc1.Account_Team_to_MAL__c=false;
        ChildAcc1.BillingPostalCode = 'A1A 1A1';
        insert ChildAcc1;


        msdCode = new MSD_Code__c();
        msdCode.OwnerId = userRec.id ;
        msdCode.Account__c = ParentAcc.id ;
        msdCode.MSD_Code_External__c = '789';
        insert msdCode;

        sharedMSD = new Shared_MSD_Code__c();
        sharedMSD.OwnerId = userRec.id ;
        sharedMSD.Name = 'SharedMSD1';
        sharedMSD.MSD_Code_External__c = '12345';
        insert sharedMSD;

        sharedAccMSD = new Shared_MSD_Accounts__c();
        sharedAccMSD.Account__c=ParentAcc.id;
        sharedAccMSD.Shared_MSD_Code__c=sharedMSD.id;
        insert sharedAccMSD;

        MSD_Member__c mem1= new MSD_Member__c();  
        mem1.MSD_Code__c=msdCode.id;
        mem1.User__c=userRec2.id;
        insert mem1;
        Shared_MSD_Member__c mem2= new Shared_MSD_Member__c();
        mem2.Shared_MSD_Code__c=sharedMSD.id;
        mem2.User__c=userRec3.id;
        insert mem2;
        
        AccountTeamMember newMem3 = new AccountTeamMember();
        newMem3.TeamMemberRole = 'Owner';
        newMem3.AccountId = ParentAcc.Id;
        newMem3.UserId = ParentAcc.OwnerId; 
        insert newMem3;
        
        AccountTeamMember newMem = new AccountTeamMember();
        newMem.TeamMemberRole = 'Msd Owner';
        newMem.AccountId = ChildAcc1.Id;
        newMem.UserId = ChildAcc1.OwnerId; 
        insert newMem;
    }
     /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name.
    *******************************************************************************************************/
    static testmethod void testSearchAccount(){
        setUpData(); 
        test.startTest();
     //   ApexPages.StandardSetController sc = new ApexPages.standardSetController(setAccountUpdate);
        Database.BatchableContext scc;
        TAG_ReCalculateAccountTeam_batch sc = new TAG_ReCalculateAccountTeam_batch(setAccountUpdate);
       // ID batchprocessid = Database.executeBatch(sc,200);
        
            sc.query ='select id,Cascade_Team_to_Child__c,  Recalculate_Team__c,Account_Team_to_MAL__c, ParentID,OwnerId, Owner.isActive, Cascade_changes_from_parent_to_child__c FROM Account where Recalculate_Team__c=true and ParentID=null';
            sc.start(scc);
            List<Account> scope = ([select id,Cascade_Team_to_Child__c,  Recalculate_Team__c,Account_Team_to_MAL__c, ParentID,OwnerId, Owner.isActive, Cascade_changes_from_parent_to_child__c FROM Account where Recalculate_Team__c=true and ParentID=null]);
            sc.execute(scc,scope);
            sc.finish(scc);
        
        test.stopTest(); 
        List<AccountTeamMember > atmList = [select id,  Account.Id from AccountTeamMember  where  Account.Id=:ParentAcc.id];
        system.assert( atmList.size()>1);
    }
}