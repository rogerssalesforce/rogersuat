/*
===============================================================================
Class Name   : Test_TAG_SharedMSDMemberUpdateController
===============================================================================
PURPOSE:    This class a controller class for TAG_SharedTeamMemberUpdate Page.
              
Developer: Jayavardhan
Date: 03/09/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
03/09/2015           Jayavardhan               Original Version
===============================================================================*/
@isTest (seeAllData=False) 
private class Test_TAG_SharedMSDMemberUpdateController{
     private static Shared_MSD_Code__c ParentSharedMSD1;
     private static Shared_MSD_Code__c ParentSharedMSD2;
     private static Shared_MSD_Code__c ParentSharedMSD3;
     private static Shared_MSD_Code__c ChildAcc1;
     private static List<Shared_MSD_Member__c> sharedMsdMemberList;
     private static Profile pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
     private static User userRec;
     private static User userRec2;
     private static User userRec3;
     private static User userRec4;
     private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
     private static Static_Data_Utilities__c staticData_PageSize = new Static_Data_Utilities__c ();
     private static Static_Data_Utilities__c staticData_Limit = new Static_Data_Utilities__c ();
     //create Test Data
     private static void setUpData(){
        //Data for Custom Settings
        TAG_CS.Unassigned_User__c ='Unassigned User';       
        TAG_CS.User_Location__c = 'Central,Eastern,National,Western';
        TAG_CS.User_Region__c = 'Alberta,Atlantic,British Columbia,National,Ontario,Prairies,Quebec';
        TAG_CS.User_Channel_1__c = 'ABS,BIS,Business Customer Enablement,Business Customer Loyalty,Business Customer Relations,Business National Support,Business Segment,Commercial Service Delivery,Dealer,Enterprise,Enterprise Service Assurance';
        TAG_CS.User_Channel_2__c = 'Enterprise,Service Delivery,Federal,Field Sales Cable,Large,M2M Reseller,Medium,MSA,NIS,Public Sector,RBS,RCI,Sales Operations,Small,VAR';
        TAG_CS.MSD_SharedMSD_Roles_Available__c = 'Account Development Rep,Account Development Rep - Dealer';
        insert TAG_CS;
        staticData_PageSize.Name = 'SharedMemberUpdate_PageSize';
        staticData_PageSize.Value__c ='1';
        insert staticData_PageSize;
        staticData_Limit.Name='SharedMemberUpdate_RecordsLimit';
        staticData_Limit.Value__c='2';
        insert staticData_Limit;
       
        
         User testu = new User(LastName = 'Mark O’Roger1', Alias = 'er112', Email='test@Rogertest.com', Username='test@Rogertest12.com', CommunityNickname = 'nick23Rog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert testu;

         User testu1 = new User(LastName = 'Mark O’Rogerdsfd1', ManagerId = testu.id, Alias = 'ty112', Email='test@Rogertest.com', Username='test@Trseegertest12.com', CommunityNickname = 'nicw3Rog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US', Assignment_Approver__c = testu.id );
        insert testu1;

        userRec = new User(LastName = 'Mark O’Roger', Alias = 'alRoger1',ManagerId = testu1.id, Email='test@Rogertest.com', Username='test@Rogertest1.com', CommunityNickname = 'nickRog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Region__c = 'Alberta',Sub_Region__c ='Central',Channel__c  ='ABS',Assignment_Approver__c = testu.id);
        insert userRec;
            system.debug('=============userRec>'+userRec);
        userRec2 = new User(LastName = 'Mark O’Roger2', Alias = 'aRoger2',ManagerId = testu1.id, Email='test@Rogertest.com', Username='test@Rogertest2.com', CommunityNickname = 'nickRog2', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Region__c = 'Alberta',Sub_Region__c ='Central',Channel__c  ='ABS',Assignment_Approver__c = testu.id);
        insert userRec2;
            system.debug('=============userRec2>'+userRec2);

         userRec3 = new User(LastName = 'Mark O’Roger3', Alias = 'aRoger3',ManagerId = testu1.id, Email='test@Rogertest.com', Username='test@Rogertest3.com', CommunityNickname = 'nickRog3', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Region__c = 'Alberta',Sub_Region__c ='Central',Channel__c  ='ABS',Assignment_Approver__c = testu.id);
        insert userRec3;
            system.debug('=============userRec3>'+userRec3);
        userRec4 = new User(LastName = 'Raj', FirstName='Jeev',Alias = 'aRoger4',ManagerId = testu1.id, Email='test@Rogertest.com', Username='test@Rogertest4.com', CommunityNickname = 'nickRog4', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Region__c = 'Alberta',Sub_Region__c ='Central',Channel__c  ='ABS',Assignment_Approver__c = testu.id);
        insert userRec4;
        system.debug('=============userRec4>'+userRec4);

        //Shared MSD Code 1
        ParentSharedMSD1= new Shared_MSD_Code__c();
        ParentSharedMSD1.Name = 'ParentSharedMSD1';
        ParentSharedMSD1.Description__c = 'TC Shared MSD1';        
        ParentSharedMSD1.OwnerId= userRec.id;
        
        
        insert ParentSharedMSD1;
        //child of ParentAcc
       /* ChildAcc1= new Account();
        ChildAcc1.Name = 'ChildAcc1';
        ChildAcc1.Account_Status__c = 'Assigned';
        ChildAcc1.ParentID= ParentAcc.Id;
        ChildAcc1.OwnerId= userRec.id;
        insert ChildAcc1;*/
        //Shared MSD Code 2
        ParentSharedMSD2= new Shared_MSD_Code__c();
        ParentSharedMSD2.Name = 'ParentSharedMSD2';
        ParentSharedMSD2.Description__c = 'TC Shared MSD2';        
        ParentSharedMSD2.OwnerId= userRec4.id;
        insert ParentSharedMSD2;
        //Shared MSD Code 2
        ParentSharedMSD3= new Shared_MSD_Code__c();
        ParentSharedMSD3.Name = 'ParentSharedMSD3';
        ParentSharedMSD3.Description__c = 'TC Shared MSD3';        
        ParentSharedMSD3.OwnerId= userRec3.id;
        insert ParentSharedMSD3;
        //instantiate Shared MSD Member List
        sharedMsdMemberList = new List<Shared_MSD_Member__c>();
        
        //Add team members for ParentSharedMSD1
        Shared_MSD_Member__c Smem1 = new Shared_MSD_Member__c();
         Smem1.Shared_MSD_Code__c = ParentSharedMSD1.Id;
         Smem1.Name = UserRec.Name;
         Smem1.User__c = UserRec.Id;
         Smem1.Role__c = 'AE Helper';        
        sharedMsdMemberList.add(Smem1); 
        
       
        //Add team members for ParentSharedMSD2
        Shared_MSD_Member__c Smem2 = new Shared_MSD_Member__c();
         Smem2.Shared_MSD_Code__c = ParentSharedMSD2.Id;
         Smem2.Name = UserRec2.Name;
         Smem2.User__c = UserRec2.Id;
         Smem2.Role__c = 'AE Helper';        
        sharedMsdMemberList.add(Smem2); 
         //Add team members for ParentAcc3
        Shared_MSD_Member__c Smem3 = new Shared_MSD_Member__c();
         Smem3.Shared_MSD_Code__c = ParentSharedMSD3.Id;
         Smem3.Name = UserRec2.Name;
         Smem3.User__c = UserRec2.Id;
         Smem3.Role__c = 'AE Helper';        
        sharedMsdMemberList.add(Smem3); 
        //insert SharedMsdMember List
        insert sharedMsdMemberList;        
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name.
    *******************************************************************************************************/
    static testmethod void testSearchAccount(){
        setUpData(); 
        test.startTest();
            TAG_SharedMSDMemberUpdateController obj = new TAG_SharedMSDMemberUpdateController();
            obj.getreasonCodevalue();       
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();    
           obj.searchFilterData.SharedMSDName='ParentSharedMSD2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listSharedMemberWrap.size()>0);
            //Add new Member
            obj.addRow();
        test.stopTest(); 
    }
     /*******************************************************************************************************
    * @description: This is a negative test method with search the as Child account
    *******************************************************************************************************/
    static testmethod void testSearchAccountNegative(){
        setUpData(); 
        test.startTest();
            TAG_SharedMSDMemberUpdateController obj = new TAG_SharedMSDMemberUpdateController();
            obj.getreasonCodevalue();
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();
            obj.searchFilterData.ownerName='Child';
            obj.search();
        test.stopTest(); 
    }
     /*******************************************************************************************************
    * @description: This is a negative test method with all the search criteria filled, with no record 
                    satisfying the filter criteria
    *******************************************************************************************************/
    static testmethod void testSearchAccountAllFilter(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_SharedMSDMemberUpdateController obj = new TAG_SharedMSDMemberUpdateController();
            obj.getreasonCodevalue();      
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();
            obj.searchFilterData.ownerName='Mark';
            obj.searchFilterData.userName='Acc';
            obj.searchFilterData.RoleName='Test';
            obj.searchFilterData.SharedMSDName='ABC';
            obj.searchFilterData.MSDCodeExternal='1234';
            obj.searchFilterData.Channel='Large';
            obj.searchFilterData.Region='TestRegion';
            obj.searchFilterData.Location='TestLocation';
            obj.searchFilterData.SharedMSDName='ABC';
            obj.searchFilterData.shareMSDObj.MSD_Code_External__c='12345';            
            obj.searchFilterData.sharedMemberObj.Role__c='AE Helper';
            obj.searchFilterData.searchUser.Channel__c='Business Segment';
            obj.searchFilterData.searchUser.Region__c='National';
            obj.searchFilterData.searchUser.Sub_Region__c='National';            
            obj.search();
            obj.backtosearch();
            obj.search();
            obj.close();
            system.assert(obj.showSection2=True);
            system.assert(obj.listSharedMemberWrap ==null);   
        }
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name and Add members
    *******************************************************************************************************/
    static testmethod void testAddNewMember(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_SharedMSDMemberUpdateController obj = new TAG_SharedMSDMemberUpdateController();
            obj.getreasonCodevalue();      
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();
            obj.searchFilterData.SharedMSDName='ParentSharedMSD2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listSharedMemberWrap.size()>0);
           
            //Add 2 new Members
            Apexpages.currentPage().getParameters().put('msdId',ParentSharedMSD2.id);
            obj.addRow();
            obj.listSharedMemberWrap[0].listAddmemberWrap[0].newMemLookup.TAG_New_Owner__c = userRec.id;
            obj.listSharedMemberWrap[0].listAddmemberWrap[0].reasonCode ='Test Reason'; 
            obj.listSharedMemberWrap[0].listAddmemberWrap[0].BusinessCase= 'Test BusinessCode';
            obj.listSharedMemberWrap[0].listAddmemberWrap[0].member.User__c = userRec2.id;
            Apexpages.currentPage().getParameters().put('msdId',ParentSharedMSD2.id);
            obj.addRow();
            obj.listSharedMemberWrap[0].listAddmemberWrap[1].newMemLookup.TAG_New_Owner__c = userRec.id;
            obj.listSharedMemberWrap[0].listAddmemberWrap[1].reasonCode ='Test Reason'; 
            obj.listSharedMemberWrap[0].listAddmemberWrap[1].BusinessCase= 'Test BusinessCode';
            obj.listSharedMemberWrap[0].listAddmemberWrap[1].member.User__c = userRec.id;
            obj.finalpage();
            obj.createRequest();

            obj.getreasonCodevalue();            
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();
           obj.searchFilterData.SharedMSDName='ABC';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listSharedMemberWrap.size()>0);
        }
        test.stopTest(); 
    }
     /*******************************************************************************************************
    * @description: This is a negative test method with search criteria as Account name and Add members
    *******************************************************************************************************/
   /* static testmethod void testAddNewMemberNegative(){
        setUpData(); 
        test.startTest();
            TAG_SharedMSDMemberUpdateController obj = new TAG_SharedMSDMemberUpdateController();
            obj.getreasonCodevalue();            
            obj.searchFilterData.ownerName='Roger';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listSharedMemberWrap.size()>0);
            //Add 2 new Members
            Apexpages.currentPage().getParameters().put('msdId',ParentSharedMSD2.id);
            obj.addRow();
            obj.listSharedMemberWrap[0].listAddmemberWrap[0].reasonCode =''; 
            obj.listSharedMemberWrap[0].listAddmemberWrap[0].BusinessCase= '';
            obj.finalpage();
        test.stopTest(); 
    }*/
        /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name and Add members and
                    remove a row from confirmation page
    *******************************************************************************************************/
    static testmethod void testAddNewMember3(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_SharedMSDMemberUpdateController obj = new TAG_SharedMSDMemberUpdateController();
            obj.getreasonCodevalue();    
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();           
           obj.searchFilterData.SharedMSDName='ParentSharedMSD2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listSharedMemberWrap.size()>0);
            //Add 2 new Members
                  system.debug('============listSharedMemberWrap==3>'+obj.listSharedMemberWrap);
                  
            Apexpages.currentPage().getParameters().put('msdId',ParentSharedMSD2.id);
            obj.addRow();
                  //system.debug('============listSharedMemberWrap==3>'+obj.listSharedMemberWrap[0].listAddmemberWrap[0].reasonCode);
            obj.listSharedMemberWrap[0].listAddmemberWrap[0].newMemLookup.TAG_New_Owner__c = userRec2.id;
            obj.listSharedMemberWrap[0].listAddmemberWrap[0].reasonCode ='Test Reason'; 
            obj.listSharedMemberWrap[0].listAddmemberWrap[0].BusinessCase= 'Test BusinessCode';
            obj.listSharedMemberWrap[0].listAddmemberWrap[0].member.User__c = userRec2.id;
            Apexpages.currentPage().getParameters().put('msdId',ParentSharedMSD2.id);
            obj.addRow();
            obj.listSharedMemberWrap[0].listAddmemberWrap[1].newMemLookup.TAG_New_Owner__c = userRec.id;
            obj.listSharedMemberWrap[0].listAddmemberWrap[1].reasonCode ='Test Reason'; 
            obj.listSharedMemberWrap[0].listAddmemberWrap[1].BusinessCase= 'Test BusinessCode';
            obj.listSharedMemberWrap[0].listAddmemberWrap[1].member.User__c = userRec.id;
            obj.finalpage();
            Apexpages.currentPage().getParameters().put('msdId',ParentSharedMSD2.id);
            Apexpages.currentPage().getParameters().put('index','1');
            obj.removeRow();
            //obj.removeRowFromConfirmationPage();
            obj.createRequest();

        }
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name, Add members and
                    remove the newly added member
    *******************************************************************************************************/
    static testmethod void testAddNewMember2(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_SharedMSDMemberUpdateController obj = new TAG_SharedMSDMemberUpdateController();
            obj.getreasonCodevalue();            
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();
           obj.searchFilterData.SharedMSDName='ParentSharedMSD2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listSharedMemberWrap.size()>0);
                  system.debug('============listSharedMemberWrap==2>'+obj.listSharedMemberWrap);
            //Add 2 new Members
            Apexpages.currentPage().getParameters().put('msdId',ParentSharedMSD2.id);
            obj.addRow();
                 // system.debug('============listSharedMemberWrap==2>'+obj.listSharedMemberWrap[0].listAddmemberWrap[0].reasonCode);
            obj.listSharedMemberWrap[0].listAddmemberWrap[0].newMemLookup.TAG_New_Owner__c = userRec.id;
            obj.listSharedMemberWrap[0].listAddmemberWrap[0].reasonCode ='Test Reason'; 
            obj.listSharedMemberWrap[0].listAddmemberWrap[0].BusinessCase= 'Test BusinessCode';
            //obj.listSharedMemberWrap[0].listAddmemberWrap[0].member.TeamMemberRole= 'SDC';
            obj.listSharedMemberWrap[0].listAddmemberWrap[0].member.User__c = userRec2.id;
            Apexpages.currentPage().getParameters().put('msdId',ParentSharedMSD2.id);
            obj.addRow();
            obj.listSharedMemberWrap[0].listAddmemberWrap[1].reasonCode ='Test Reason'; 
            obj.listSharedMemberWrap[0].listAddmemberWrap[1].BusinessCase= 'Test BusinessCode';
            //obj.listSharedMemberWrap[0].listAddmemberWrap[1].member.TeamMemberRole= 'SDC';
            obj.listSharedMemberWrap[0].listAddmemberWrap[1].member.User__c = userRec.id;
            obj.listSharedMemberWrap[0].listAddmemberWrap[1].newMemLookup.TAG_New_Owner__c = userRec.id;
            //Remove one of the added members
            Apexpages.currentPage().getParameters().put('msdId',ParentSharedMSD2.id);
            Apexpages.currentPage().getParameters().put('index','1');
            obj.removeRow();

            obj.finalpage();
            obj.backtoStep2();
        }
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name, and remove the 
                    existing member.
    *******************************************************************************************************/
    static testmethod void testRemoveExistingMember(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_SharedMSDMemberUpdateController obj = new TAG_SharedMSDMemberUpdateController();
            obj.getreasonCodevalue();         
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();
           obj.searchFilterData.SharedMSDName='ParentSharedMSD2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listSharedMemberWrap.size()>0);
            //Remove Exixting Member
            Apexpages.currentPage().getParameters().put('msdId',ParentSharedMSD2.id);
            obj.removeExistingMem();
            obj.listSharedMemberWrap[0].listmemberWrap[0].reasonCode ='Test Reason'; 
            obj.listSharedMemberWrap[0].listmemberWrap[0].BusinessCase= 'Test BusinessCode';
            obj.listSharedMemberWrap[0].listmemberWrap[0].del = true;
            
            obj.finalpage();
            obj.createRequest();
            List<Assignment_Request_Item__c> aReqItems = [Select Id from Assignment_Request_Item__c where Shared_MSD__c =:ParentSharedMSD2.id];
                  system.debug('=============testRemoveExistingMember>'+aReqItems);
            //system.assert(aReqItems.size()>0);
            obj.getreasonCodevalue();         
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();
            obj.searchFilterData.ownerName='Roger3';
            obj.searchFilterData.Channel = 'ABS';
            obj.searchFilterData.Location = 'Alberta';
            obj.searchFilterData.Region = 'Central';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listSharedMemberWrap.size()>0);
        }
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name, remove the 
                    existing member and remove a selected row from confirmation page
    *******************************************************************************************************/
    static testmethod void testRemoveExistingMember2(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_SharedMSDMemberUpdateController obj = new TAG_SharedMSDMemberUpdateController();
            obj.getreasonCodevalue();        
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();
            obj.searchFilterData.SharedMSDName='ParentSharedMSD2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listSharedMemberWrap.size()>0);
            //Remove Exixting Member
            Apexpages.currentPage().getParameters().put('msdId',ParentSharedMSD2.id);
            obj.removeExistingMem();
            obj.listSharedMemberWrap[0].listmemberWrap[0].reasonCode ='Test Reason'; 
            obj.listSharedMemberWrap[0].listmemberWrap[0].BusinessCase= 'Test BusinessCode';
            obj.listSharedMemberWrap[0].listmemberWrap[0].del = true;
            obj.finalpage();
            obj.listSharedMemberWrap[0].listmemberWrap[0].removed =true;
            obj.undoRemoveItems();
            obj.removeItems();
            obj.removeRowFromConfirmationPage('Test','1');
            
        }
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a negative test method with search criteria as Account name, and remove the 
                    existing member.
    *******************************************************************************************************/
    static testmethod void testRemoveExistingMemberNegative(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_SharedMSDMemberUpdateController obj = new TAG_SharedMSDMemberUpdateController();
            obj.getreasonCodevalue(); 
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();
           obj.searchFilterData.SharedMSDName='ParentSharedMSD2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listSharedMemberWrap.size()>0);
            //Remove Exixting Member
            Apexpages.currentPage().getParameters().put('msdId',ParentSharedMSD2.id);
            obj.removeExistingMem();
            obj.listSharedMemberWrap[0].listmemberWrap[0].reasonCode =''; 
            obj.listSharedMemberWrap[0].listmemberWrap[0].BusinessCase='';
            obj.listSharedMemberWrap[0].listmemberWrap[0].del = true;
            obj.finalpage();
        }
        test.stopTest(); 
    }
}