/*
===============================================================================
Class Name   : BatchUtils_Test
===============================================================================
PURPOSE:    Unit tests for BatchUtils
  

CHANGE HISTORY
===============================================================================
DATE              NAME                        DESC
MM/DD/YYYY
01/29/2014        Michael Appleton            Created
===============================================================================
*/
@isTest (SeeAllData = true)
public class BatchUtils_Test {
    
    static testMethod void testGetDefaultOrgWideEmailAddr() {
        
        Id actualId = BatchUtils.getDefaultOrgWideEmailAddr(Label.Default_OrgWideEmailAddress);
        System.assertNotEquals(actualId, null);

/*        
        OrgWideEmailAddress defOWE = [select id from OrgWideEmailAddress where Address = : Label.Default_OrgWideEmailAddress ][0];
        Id expectedId = defOWE.Id;
        System.assertEquals(expectedId, actualId);
*/        
    }
}