/*********************************************************************************
Class Name      : CPQ_PriceListTriggerHelper
Description     : This class is used as a helper class for PriceList Trigger 
Created By      : Ramiya Kandeepan
Created Date    : 18-November-2015
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Ramiya Kandeepan            18-November-2015            Created class for PriceList Trigger
*********************************************************************************/
public with sharing class CPQ_PriceListTriggerHelper {
    
     /*********************************************************************************************************************
     Method Name    : preventPriceListDeletion
     Description    : Method to prevent deletion of the apttus price list record specified in CPQ_Settings ApttusPriceLists__c
     Return Type    : void
     Parameter      : 1.List<Apttus_Config2__PriceList__c> lstPricelists
     *********************************************************************************************************************/
    public static void preventPriceListDeletion(List<Apttus_Config2__PriceList__c> lstPricelists) {
		CPQ_Settings__c csCustomSetting = CPQ_Settings__c.getInstance();
        Map<String, Id> mapPriceListNameToId = new map<String, id>();
        Set<String> setPriceListNames = new Set<String>();
        
        //Iterationg through Custom Setting to Get the Price List Mapping.
        if(csCustomSetting.OpportunityQuotePriceListMapping__c != null){
            List<String> lstStringMapping = csCustomSetting.OpportunityQuotePriceListMapping__c.split(';');
            
            //Iterating through the Price List mapping value to Derive Mapping
            for(String mapping : lstStringMapping ){
                List<String> lstPriceListNames =  mapping.split(':');
                
                // Checking for the correct mapping format to get PriceList
                if(lstPriceListNames.size() == 2){
                    setPriceListNames.add(lstPriceListNames [1]);
                }
            }           
        }

        //Iterating through All Apttus Price list and prevent deletion from Admin if the Price list is non-deletable
        for (Apttus_Config2__PriceList__c pl : lstPricelists) {
            if(setPriceListNames.contains(pl.Name)){
                pl.addError(Label.CPQ_PriceListDeletionError);
            }
            
       }
    }
}