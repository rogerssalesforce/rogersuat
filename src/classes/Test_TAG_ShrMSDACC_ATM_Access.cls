/*
===============================================================================
 Class Name   : Test_TAG_ShrMSDACC_ATM_Access
===============================================================================
PURPOSE:    This is a Test class for TAG_ShrMSDACC_ATM_Access Trigger.
              
Developer: Jayavardhan
Date: 03/09/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
03/09/2015           Jayavardhan               Original Version
===============================================================================*/
@isTest (seeAllData=False) 
private class Test_TAG_ShrMSDACC_ATM_Access{
     private static Account ParentAcc;
     private static Account childAcc;
      
     private static List<AccountTeamMember> parentAccTeam;
     private static List<Shared_MSD_Accounts__c> msdAccObjList;
     private static Profile pEmp = [Select Id from Profile where Name Like '%Rogers%' limit 1];
     private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
     private static User userRec;
     private static User userRec2;
     //create Test Data
     private static void setUpData(){
        TAG_CS.Team_Custom_Object_Access__c='Read';
        TAG_CS.Unassigned_User__c ='Unassigned User';
         insert TAG_CS;
        userRec = new User(LastName = 'Mark O’BrienRoger', Alias = 'alRoger', Email='test@Rogertest.com', Username='test877@Rogertest.com', CommunityNickname = 'nickRoger877', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec;
        userRec2 = new User(LastName = 'Mark O’BrienRoger2', Alias = 'aRoger', Email='test@Rogertest.com', Username='test888@Rogertest2.com', CommunityNickname = 'nickRoger888', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec2;
        
        ParentAcc= new Account(Name='ParentAcc');
        ParentAcc.Account_Status__c = 'Unassigned';
        ParentAcc.ParentID= null;
        ParentAcc.OwnerId = userRec.Id; 
        ParentAcc.BillingPostalCode = 'A1A 1A1';
        
        insert ParentAcc;
        
        parentAccTeam = new List<AccountTeamMember>();
        AccountTeamMember newMem = new AccountTeamMember();
        newMem.TeamMemberRole = 'Owner';
        newMem.AccountId = ParentAcc.Id;
        newmem.UserId = ParentAcc.OwnerId; 
        parentAccTeam.add(newMem);
        
        AccountTeamMember newMem2 = new AccountTeamMember();
        newMem2 .TeamMemberRole = 'SDC';
        newMem2 .AccountId = ParentAcc.Id;
        newMem2 .UserId = userRec.Id; 
        parentAccTeam.add(newMem2 );
        
        AccountTeamMember newMem3 = new AccountTeamMember();
        newMem3.TeamMemberRole = 'ISR - Large';
        newMem3.AccountId = ParentAcc.Id;
        newMem3.UserId = userRec2.Id; 
        parentAccTeam.add(newMem3);
        
        insert parentAccTeam;   
        
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with Shared MSD Account obj records created with account team members
    *******************************************************************************************************/
    static testmethod void testAccountTeamMemberAccess(){
        setUpData(); 
        msdAccObjList = new List<Shared_MSD_Accounts__c>();
        Shared_MSD_Code__c msd= new Shared_MSD_Code__c();
            msd.ownerId=UserRec.id;
            msd.Name = '987654367';
            msd.Description__c = 'TC Shared MSD';
            insert msd;
            
            Shared_MSD_Accounts__c sharedMsdAcc = new Shared_MSD_Accounts__c();
            sharedMsdAcc.Account__c = ParentAcc.id;
            sharedMsdAcc.Shared_MSD_Code__c = msd.Id;
            msdAccObjList.add(sharedMsdAcc);           
            
            Shared_MSD_Code__c msd2= new Shared_MSD_Code__c();
            msd2.ownerId=UserRec.id;
            msd2.Name = '987654375';
            msd2.Description__c = 'TC Shared MSD1';
            insert msd2;
            
           Shared_MSD_Accounts__c  sharedMsdAcc2 = new Shared_MSD_Accounts__c();
            sharedMsdAcc2.Account__c = ParentAcc.id;
            sharedMsdAcc2.Shared_MSD_Code__c = msd2.Id;
            msdAccObjList.add(sharedMsdAcc2);
            insert msdAccObjList;
            
        

    }  
  
}