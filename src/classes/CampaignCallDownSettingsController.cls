/**
*@Author        Miguel Soares de Lima
*@Created date  4/28/2010
*/
public with sharing class CampaignCallDownSettingsController{

    private List<ColumnSettings> columnSettingsList;
    private CampaignColumns__c campaignColumns;
    public string backPage{ get; set; }
    public map<string,integer> mapCheck= new map<string,integer>();
    public Map<String, Schema.SObjectField> fieldMap= new Map<String, Schema.SObjectField>();
    
    /**
    *Constructor
    */
    public CampaignCallDownSettingsController(){
        
        columnSettingsList = new ColumnSettings[]{};
        if( Apexpages.currentPage().getParameters().get( 'var' ) != null )
            backPage=Apexpages.currentPage().getParameters().get( 'var' );
        else
            backpage='/';
          
        campaignColumns = CampaignColumns__c.getInstance();
        if(campaignColumns==Null ){
        campaignColumns= new CampaignColumns__c();
        }
        String selectedObject = 'campaignColumns__c';
        Map<String, Schema.SObjectType> gdMap = Schema.getGlobalDescribe();
        Schema.Describesobjectresult dsr = gdMap.get(selectedObject).getDescribe();
        fieldMap = dsr.fields.getMap();
       
        Map<String,String> soList = new Map<String,String>();
        integer x=0;
        for(string key : fieldMap.keySet()) {
          if(key.contains('__c')){
          string label= key.replace('__c', '');
          label= label.replace('_', ' ');
          soList.Put(Key,label.subString(0 ,1).ToUpperCase()+label.subString(1));
         }
          
        }
        
       for(String key : soList.keySet()){
       
                try{
                     boolean checkVal;
                   if(campaignColumns.get(key) !=Null){
                        checkVal= boolean.valueof(campaignColumns.get(key));
                    }else{
                        checkVal=True;
                    }
                    columnSettingsList.add( new ColumnSettings( soList.get(key),campaignColumns == null ? True : checkVal));
                    mapCheck.put(key,x);
                    x=x+1;
                }catch(exception ex){
                }
            
        }
        System.debug('-------------------------->'+columnSettingsList);
        /*
        columnSettingsList.add( new ColumnSettings( 'Title',            campaignColumns == null ? false : campaignColumns.TITLE__c ));
        columnSettingsList.add( new ColumnSettings( 'Company',      campaignColumns == null ? false : campaignColumns.COMPANY__c ));
        columnSettingsList.add( new ColumnSettings( 'Phone',            campaignColumns == null ? false : campaignColumns.PHONE__c ));
        columnSettingsList.add( new ColumnSettings( 'Email',            campaignColumns == null ? false : campaignColumns.EMAIL__c ));
        columnSettingsList.add( new ColumnSettings( 'Fax',          campaignColumns == null ? false : campaignColumns.FAX__c ));
        columnSettingsList.add( new ColumnSettings( 'Salutation',   campaignColumns == null ? false : campaignColumns.SALUTATION__c ));
        columnSettingsList.add( new ColumnSettings( 'Owner',            campaignColumns == null ? false : campaignColumns.OWNER__c ));
        columnSettingsList.add( new ColumnSettings( 'Address',      campaignColumns == null ? false : campaignColumns.ADDRESS__c ));*/
        
    } 

    /**
    * Save settings
    *
    * @return   a page reference to return to the original page
    */
    public PageReference saveSettings(){
            
            if (campaignColumns == null) {
                campaignColumns = CampaignColumns__c.getInstance();
                
                if (campaignColumns == null) {
                    campaignColumns = new CampaignColumns__c();
                }
            }
            
            if (campaignColumns.SetupOwnerId != Userinfo.getUserId()) {
                campaignColumns.Id = null;
                campaignColumns.SetupOwnerId = Userinfo.getUserId();
            }

            for(String key : mapCheck.keySet()){
 
             campaignColumns.put(key,columnSettingsList[mapCheck.get(key)].cValue);
            }
        
           /* campaignColumns.ADDRESS__c          = columnSettingsList[ 7 ].cValue;
            campaignColumns.COMPANY__c          = columnSettingsList[ 1 ].cValue;
            campaignColumns.EMAIL__c            = columnSettingsList[ 3 ].cValue;
            campaignColumns.PHONE__c            = columnSettingsList[ 2 ].cValue;
            campaignColumns.FAX__c              = columnSettingsList[ 4 ].cValue;
            campaignColumns.SALUTATION__c       = columnSettingsList[ 5 ].cValue;
            campaignColumns.TITLE__c            = columnSettingsList[ 0 ].cValue;
            campaignColumns.OWNER__c            = columnSettingsList[ 6 ].cValue;*/

            
             upsert campaignColumns;
        
        return new PageReference( backPage );
    }
    
    /**
    *Get columns settings
    *@return columnSettingsList
    */
    public List<ColumnSettings> getcolumnSettingsList(){
        return columnSettingsList;
    }
  
    /**
    *@Author        Miguel Soares de Lima
    *@Created date  4/28/2010
    */
    public class  ColumnSettings{
        public String cName{ get; set; }
        public boolean cValue{ get; set; }
    
        /**
        *constructor
        */
        public ColumnSettings( string colName, boolean colValue ){
            cName=colName;
            cValue=colValue;
        }   
    }

}