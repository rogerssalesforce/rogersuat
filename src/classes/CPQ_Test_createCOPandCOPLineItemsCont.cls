/*********************************************************************************
Class Name      : CPQ_Test_createCOPandCOPLineItemsCont
Description     : This class a test class for CPQ_createCOPandCOPLineItemsCont. 
Created By      : Aakanksha Patel
Created Date    : 26-Nov-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Aakanksha Patel            26-Nov-2015           Method "testCOPandCOPLIwirelessAgreement"
                                                 Method "testCOPandCOPLIadmin3Agreement"
                                                 Method "testCOPandCOPLIwirelineAgreement"
                                                 Method "testCOPandCOPLIengagementAgreement"
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest (seeAllData=False) 
private class CPQ_Test_createCOPandCOPLineItemsCont{
    static COP__c oCOP;
    static COP__c oCOPnew;
    static User TestAEUser;
    static Account oAccount;
    static Product2 oProduct;
    static Opportunity oOpportunity;
    static COP_Line_Item__c oCOPLineItem;
    static Attachment attachmentQuote;
    static Apttus_Proposal__Proposal__c oQuote;
    static Apttus_Config2__PriceList__c oPriceList;
    static Apttus__APTS_Agreement__c oAgreement;
    static Apttus__APTS_Agreement__c oAgreementNew;
    static List<Apttus__AgreementLineItem__c> lstAgreementLineItem;
    
    private static User testSUser;
    private static CPQ_Settings__c csMapping;
  
    private static final String CPQ_AGREEMENTS_PROFILES_SETTING_VALUE = 'EBU - Rogers Specialist|Read';
    private static final String CPQ_AGREEMENTS_PROFILES_SETTING_VALUE2 = 'EBU - Rogers Resellers Sales Rep|Read';
    private static final String CPQ_ACCOUNT_SPECIALIST = 'EBU - Rogers Specialist';
    private static final String CPQ_SYSTEM_ADMIN = 'System Administrator';
    private static final String CPQ_ADMIN_USERNAME = 'standarduser_test123@testorg.com';
    private static final String CPQ_SPECIALIST_USERNAME = 'specialistuser_test123@testorg.com';
    private static final String CPQ_OPPORTUNITY_STAGE_CLOSED = 'Closed Won';
    private static final String CPQ_OPPORTUNITY_CLOSED_RECORDTYPE = 'Rogers EBU - Closed';
    private static final String CPQ_STATUS_CATEGORY_IN_REQUEST ='Request';
    private static final String CPQ_STATUS_SIGNATURE_NOTREQUIRED = 'Signature Not Required';
    private static final String CPQ_STATUS_CATEGORY_IN_SIGNATURES = 'In Signatures';
    private static final String CPQ_STATUS_INTERNAL_SIGNATURE='Internal Signatures';
    private static final String CPQ_COP_STATUS_COMPLETED = 'Completed';
 
    /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testMethod void createTestData() {
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');      
        testSUser = CPQ_Test_Setup.newUser(CPQ_ACCOUNT_SPECIALIST, CPQ_SPECIALIST_USERNAME);
        insert testSUser;
        system.runAs(TestAEUser){
            
            //Create PriceList
            oPriceList = CPQ_Test_Setup.newPriceList();
            insert oPriceList ;
            
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            oAccount.Root_BAN__c = '123456789';
            oAccount.Has_MECA__c = FALSE;
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
            insert oQuote;
            //Create attachment for Quote
            attachmentQuote = CPQ_Test_Setup.newAttachment(oQuote.id);
            insert attachmentQuote;
            
            //Create Product
            oProduct = CPQ_Test_Setup.newProduct();
            
            //RecordType "MECA" for Agreement object
            List<RecordType> lstRecTypeAgreement = [SELECT Id,DeveloperName FROM RecordType WHERE SobjectType = 'Apttus__APTS_Agreement__c' AND DeveloperName= 'MECA'];
       
            //Create Agreement
            oAgreement= CPQ_Test_Setup.newAgreement(oAccount, oOpportunity,oQuote);
            oAgreement.RecordTypeId = lstRecTypeAgreement[0].id;
            
            //Create Agreement
            oAgreementNew = CPQ_Test_Setup.newAgreement(oAccount, oOpportunity,oQuote);
            oAgreementNew.RecordTypeId = lstRecTypeAgreement[0].id;
           
            //Create AgreementLineItems
            lstAgreementLineItem = CPQ_Test_Setup.newlstAgreementLine(oAgreement,1);
                        
            //COP Creation
            oCOP = CPQ_Test_Setup.newCOP(oAgreement);
            
             //Inserting Custom setting CPQ_Settings__c
            csMapping  = CPQ_Settings__c.getOrgDefaults();
            csMapping.AgreementsProfiles__c = CPQ_AGREEMENTS_PROFILES_SETTING_VALUE;
            csMapping.AgreementsProfiles2__c = CPQ_AGREEMENTS_PROFILES_SETTING_VALUE2;
            csMapping.Agreement_Category_Status_to_Close_Opp__c=CPQ_STATUS_CATEGORY_IN_REQUEST+','+CPQ_STATUS_CATEGORY_IN_SIGNATURES;
            csMapping.Agreement_Status_to_Close_Opp__c=CPQ_STATUS_SIGNATURE_NOTREQUIRED+','+CPQ_STATUS_INTERNAL_SIGNATURE;
            upsert csMapping;
            
            insert oAgreement;
            insert oAgreementNew;
          
        }
    }

    /****************************************************************************************************************
    Method Name    : testCOPandCOPLIwirelessAgreement
    Description    : Method for creating COP and COP Line Items for the "Wireless Contract Team" Record Type
    Return Type    : void  
    Parameter      : Nil              
    *****************************************************************************************************************/ 
    static testmethod void testCOPandCOPLIwirelessAgreement()
    {
        createTestData();
        oQuote.Primary_Quote_Document_ID__c = attachmentQuote.id;
        update oQuote;
    
        test.startTest();
        system.runAs(TestAEUser)
        {
            //Product with Wireless COP Team Type
            oProduct.COP_Team__c = 'Wireless Contract Team';
            insert oProduct;
            
            //AgreementLineItems for Wireless COP Type
            lstAgreementLineItem[0].Apttus__ProductId__c = oProduct.id;
            lstAgreementLineItem[0].Apttus__AgreementId__c = oAgreement.id;
            insert lstAgreementLineItem;
            
            //RecordType for the COP record
            RecordType oRecTypeCOP = [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.IsActive, r.Id, r.DeveloperName From RecordType r where r.DeveloperName = 'Contracts_Team_Wireless' and r.SobjectType = 'COP__c'];
            
            //RecordType for the COP Line Item record
            RecordType oRecTypeCOPLI = [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.IsActive, r.Id, r.DeveloperName From RecordType r where r.DeveloperName = 'Contracts_Team_Wireless' and r.SobjectType = 'COP_Line_Item__c'];
             
            //Calling the Controller 
            PageReference pageRef = Page.CPQ_createCOPandCOPLineItems;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Id',oAgreement.id);
            CPQ_createCOPandCOPLineItemsCont controller = new CPQ_createCOPandCOPLineItemsCont();
            controller.createCOP();
            controller.backToAgreement();
        
            //Assertion to check if the COP has been created with Wireless Record Type
            List<COP__c> lstCOP = [select id, Agreement__c,RecordTypeID from COP__c where Agreement__c=:oAgreement.id ];
            system.assert(lstCOP[0].Agreement__c == oAgreement.id);
            system.assert(lstCOP[0].RecordTypeID == oRecTypeCOP.id);
            
            //Assertion to check if the COP Line Item has been created with WireLess Record Type
            List<COP_Line_Item__c> lstCOPLI = [select id, COP__c,RecordTypeID from COP_Line_Item__c where COP__c=:lstCOP[0].id ];
            system.assert(lstCOPLI[0].COP__c == lstCOP[0].id);
            system.assert(lstCOPLI[0].RecordTypeID == oRecTypeCOPLI.id);
            
        }
        
    }
    
    /****************************************************************************************************************
    Method Name    : testCOPandCOPLIadmin3Agreement
    Description    : Method for creating COP and COP Line Items for the "Admin3 (Data Centre)" Record Type
    Return Type    : void  
    Parameter      : Nil              
    *****************************************************************************************************************/ 
    static testmethod void testCOPandCOPLIadmin3Agreement()
    {
        createTestData();
        oQuote.Primary_Quote_Document_ID__c = attachmentQuote.id;
        update oQuote;
    
        test.startTest();
        system.runAs(TestAEUser)
        {
            //Product with 'Admin3 (Data Centre)' COP Team Type
            oProduct.COP_Team__c = 'Admin3';
            insert oProduct;
            
            //AgreementLineItems for 'Admin3 (Data Centre)' COP Type
            lstAgreementLineItem[0].Apttus__ProductId__c = oProduct.id;
            lstAgreementLineItem[0].Apttus__AgreementId__c = oAgreement.id;
            insert lstAgreementLineItem;
            
            //RecordType for the COP record
            RecordType oRecTypeCOP = [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.IsActive, r.Id, r.DeveloperName From RecordType r where r.DeveloperName='Admin3_Data_Centre' and r.SobjectType = 'COP__c'];
            
            //RecordType for the COP Line Item record
            RecordType oRecTypeCOPLI = [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.IsActive, r.Id, r.DeveloperName From RecordType r where r.DeveloperName='Admin3_Data_Centre' and r.SobjectType = 'COP_Line_Item__c'];
             
            //Calling the Controller 
            PageReference pageRef = Page.CPQ_createCOPandCOPLineItems;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Id',oAgreement.id);
            CPQ_createCOPandCOPLineItemsCont controller = new CPQ_createCOPandCOPLineItemsCont();
            controller.createCOP();
            controller.backToAgreement();
             
            //Assertion to check if the COP has been created with Wireless Record Type
            List<COP__c> lstCOP = [select id, Agreement__c,RecordTypeID from COP__c where Agreement__c=:oAgreement.id ];
            system.assert(lstCOP[0].Agreement__c == oAgreement.id);
            system.assert(lstCOP[0].RecordTypeID == oRecTypeCOP.id);
            
            //Assertion to check if the COP Line Item has been created with WireLess Record Type
            List<COP_Line_Item__c> lstCOPLI = [select id, COP__c,RecordTypeID from COP_Line_Item__c where COP__c=:lstCOP[0].id ];
            system.assert(lstCOPLI[0].COP__c == lstCOP[0].id);
            system.assert(lstCOPLI[0].RecordTypeID == oRecTypeCOPLI.id);
          }
        
    }
    
    /****************************************************************************************************************
    Method Name    : testCOPandCOPLIwirelineAgreement
    Description    : Method for creating COP and COP Line Items for the "Service Order Coordinator (Wireline)" Record Type
    Return Type    : void  
    Parameter      : Nil              
    *****************************************************************************************************************/ 
    static testmethod void testCOPandCOPLIwirelineAgreement()
    {
        createTestData();
        oQuote.Primary_Quote_Document_ID__c = attachmentQuote.id;
        update oQuote;
    
        test.startTest();
        system.runAs(TestAEUser)
        {
            //Product with 'Service Order Coordinator (Wireline)' COP Team Type
            oProduct.COP_Team__c = 'Customer Solutions - Operations';
            insert oProduct;
            
            //AgreementLineItems for 'Service Order Coordinator (Wireline)' COP Type
            lstAgreementLineItem[0].Apttus__ProductId__c = oProduct.id;
            lstAgreementLineItem[0].Apttus__AgreementId__c = oAgreement.id;
            insert lstAgreementLineItem;
            
            //RecordType for the COP record
            RecordType oRecTypeCOP = [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.IsActive, r.Id, r.DeveloperName From RecordType r where r.DeveloperName='Service_Order_Coordinator_Wireline' and r.SobjectType = 'COP__c'];
           
            //RecordType for the COP Line Item record
            RecordType oRecTypeCOPLI = [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.IsActive, r.Id, r.DeveloperName From RecordType r where r.DeveloperName='Service_Order_Coordinator_Wireline' and r.SobjectType = 'COP_Line_Item__c'];
           
            //Calling the Controller 
            PageReference pageRef = Page.CPQ_createCOPandCOPLineItems;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Id',oAgreement.id);
            CPQ_createCOPandCOPLineItemsCont controller = new CPQ_createCOPandCOPLineItemsCont();
            controller.createCOP();
            controller.backToAgreement();
            
            //Assertion to check if the COP has been created with Wireless Record Type
            List<COP__c> lstCOP = [select id, Agreement__c,RecordTypeID from COP__c where Agreement__c=:oAgreement.id ];
            system.assert(lstCOP[0].Agreement__c == oAgreement.id);
            system.assert(lstCOP[0].RecordTypeID == oRecTypeCOP.id);
            
            //Assertion to check if the COP Line Item has been created with WireLess Record Type
            List<COP_Line_Item__c> lstCOPLI = [select id, COP__c,RecordTypeID from COP_Line_Item__c where COP__c=:lstCOP[0].id ];
            system.assert(lstCOPLI[0].COP__c == lstCOP[0].id);
            system.assert(lstCOPLI[0].RecordTypeID == oRecTypeCOPLI.id);
        }
     }
     
     /****************************************************************************************************************
    Method Name    : testCOPandCOPLIengagementAgreement
    Description    : Method for creating COP and COP Line Items for the "Engagement Manager (IoT/Services)" Record Type
    Return Type    : void  
    Parameter      : Nil              
    *****************************************************************************************************************/ 
    static testmethod void testCOPandCOPLIengagementAgreement()
    {
        createTestData();
        oQuote.Primary_Quote_Document_ID__c = attachmentQuote.id;
        update oQuote;
    
        test.startTest();
        system.runAs(TestAEUser)
        {
            //Product with 'Engagement Manager (IoT/Services)' COP Team Type
            oProduct.COP_Team__c = 'Engagement Manager';
            insert oProduct;
            
            //AgreementLineItems for 'Engagement Manager (IoT/Services)' COP Type
            lstAgreementLineItem[0].Apttus__ProductId__c = oProduct.id;
            lstAgreementLineItem[0].Apttus__AgreementId__c = oAgreement.id;
            insert lstAgreementLineItem;
            
            //RecordType for the COP record
            RecordType oRecTypeCOP = [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.IsActive, r.Id, r.DeveloperName From RecordType r where r.DeveloperName='Engagement_Manager_IoT_Services' and r.SobjectType = 'COP__c'];
             
            //RecordType for the COP Line Item record
            RecordType oRecTypeCOPLI = [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.IsActive, r.Id, r.DeveloperName From RecordType r where r.DeveloperName='Engagement_Manager_IoT_Services' and r.SobjectType = 'COP_Line_Item__c'];
            
            //Calling the Controller 
            PageReference pageRef = Page.CPQ_createCOPandCOPLineItems;
            Test.setCurrentPage(pageRef);
            
            ApexPages.currentPage().getParameters().put('Id',oAgreement.id);
            CPQ_createCOPandCOPLineItemsCont controller = new CPQ_createCOPandCOPLineItemsCont();
            controller.createCOP();
            controller.backToAgreement();
                        
            //Assertion to check if the COP has been created with Wireless Record Type
            List<COP__c> lstCOP = [select id, Agreement__c,RecordTypeID from COP__c where Agreement__c=:oAgreement.id ];
            system.assert(lstCOP[0].Agreement__c == oAgreement.id);
            system.assert(lstCOP[0].RecordTypeID == oRecTypeCOP.id);
            
            //Assertion to check if the COP Line Item has been created with WireLess Record Type
            List<COP_Line_Item__c> lstCOPLI = [select id, COP__c,RecordTypeID from COP_Line_Item__c where COP__c=:lstCOP[0].id ];
            system.assert(lstCOPLI[0].COP__c == lstCOP[0].id);
            system.assert(lstCOPLI[0].RecordTypeID == oRecTypeCOPLI.id);
        }
     }
     
    /*********************************************************************************************************
    Method Name    : testCopyingCOPLI
    Description    : Method for copying the COP line Items from previous related "Completed" COP to the new COP
    Return Type    : void  
    Parameter      : Nil              
    **********************************************************************************************************/ 
    static testmethod void testCopyingCOPLI()
    {
        createTestData();
        oQuote.Primary_Quote_Document_ID__c = attachmentQuote.id;
        update oQuote;
        system.runAs(TestAEUser)
        {
            test.startTest();
            
            //Product with 'Engagement Manager (IoT/Services)' COP Team Type
            oProduct.COP_Team__c = 'Engagement Manager';
            insert oProduct;
            
            //AgreementLineItems for 'Engagement Manager (IoT/Services)' COP Type
            lstAgreementLineItem[0].Apttus__ProductId__c = oProduct.id;
            lstAgreementLineItem[0].Apttus__AgreementId__c = oAgreement.id;
            insert lstAgreementLineItem;
            
           //RecordType for the COP record
            RecordType oRecTypeCOP = [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.IsActive, r.Id, r.DeveloperName From RecordType r where r.DeveloperName = 'Contracts_Team_Wireless' and r.SobjectType = 'COP__c'];
            
            //COP Creation
            oCOP = CPQ_Test_Setup.newCOP(oAgreement);
            oCOP.RecordTypeID  = oRecTypeCOP.id;
            oCOP.COP_Status__c = CPQ_COP_STATUS_COMPLETED;
            insert oCOP;
            String strCOPid = oCOP.id;
            
            //RecordType for the COP Line Item record
            RecordType oRecTypeCOPLI = [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.IsActive, r.Id, r.DeveloperName From RecordType r where r.DeveloperName = 'Contracts_Team_Wireless' and r.SobjectType = 'COP_Line_Item__c'];
            
            //COP Line Item Creation  with Wireless RecordType
            oCOPLineItem = CPQ_Test_Setup.newCOPLineItem(oCOP);
            oCOPLineItem.RecordTypeID  = oRecTypeCOPLI.id;
            insert oCOPLineItem;
            
            //Account Update for the Wireless ID field
            oAccount.Latest_Wireless_COP__c = strCOPid;
            update oAccount;
            
            //Assertion to check if the Account has been updated with the COP Salesforce ID
            List<Account> lstAccount = [select id, Latest_Wireless_COP__c from Account where Id=:oAccount.id ];
            system.assert(lstAccount[0].Latest_Wireless_COP__c == strCOPid);
          
            //COP Creation
            oCOPnew = CPQ_Test_Setup.newCOP(oAgreementNew);
            oCOPnew.RecordTypeID  = oRecTypeCOP.id;
            insert oCOPnew;
            
            String strNewCOPid = oCOPnew.id;
             
            //Updating the Status of COP
            oCOPnew.COP_Status__c = CPQ_COP_STATUS_COMPLETED;
            update oCOPnew;
             
            //Calling the Controller 
            PageReference pageRef = Page.CPQ_createCOPandCOPLineItems;
            Test.setCurrentPage(pageRef);
            
            ApexPages.currentPage().getParameters().put('Id',oAgreement.id);
            CPQ_createCOPandCOPLineItemsCont controller = new CPQ_createCOPandCOPLineItemsCont();
            controller.createCOP();
            controller.backToAgreement();
            
             //Assertion to check if the COP has been created with Wireless Record Type
            List<COP__c> lstCOP = [select id, Agreement__c,RecordTypeID from COP__c where Agreement__c=:oAgreement.id ];
            system.assert(lstCOP[0].Agreement__c == oAgreement.id);
            system.assert(lstCOP[0].RecordTypeID == oRecTypeCOP.id);
            
            //Assertion to check if the COP Line Item has been created with WireLess Record Type
            List<COP_Line_Item__c> lstCOPLI = [select id, COP__c,RecordTypeID from COP_Line_Item__c where COP__c=:lstCOP[0].id ];
            system.assert(lstCOPLI[0].COP__c == lstCOP[0].id);
            system.assert(lstCOPLI[0].RecordTypeID == oRecTypeCOPLI.id);
            
            List<COP_Line_Item__c> lstCOPLIOld = [select id, COP__c from COP_Line_Item__c where COP__c =:oCOP.id ];
            system.assert(lstCOPLI.size() == lstCOPLIOld.size());
            
            //Assertion to check if the Account has been updated with the new COP ID
            lstAccount = [select id, Latest_Wireless_COP__c from Account where Id=:oAccount.id ];
            system.assert(lstAccount[0].Latest_Wireless_COP__c == strNewCOPid);
            
        }
    }
    
    /*********************************************************************************************************
    Method Name    : testBulkCopyingCOPLI
    Description    : Method for copying the COP line Items from previous related "Completed" COP to the new COP (Bulk)
    Return Type    : void  
    Parameter      : Nil              
    **********************************************************************************************************/ 
    static testmethod void testBulkCopyingCOPLI()
    {
        createTestData();
        oQuote.Primary_Quote_Document_ID__c = attachmentQuote.id;
        update oQuote;
        system.runAs(TestAEUser)
        {
            test.startTest();
            
            //Product with 'Engagement Manager (IoT/Services)' COP Team Type
            oProduct.COP_Team__c = 'Engagement Manager';
            insert oProduct;
            
            //RecordType for the COP record
            RecordType oRecTypeCOP = [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.IsActive, r.Id, r.DeveloperName From RecordType r where r.DeveloperName = 'Contracts_Team_Wireless' and r.SobjectType = 'COP__c'];
            
            //COP Creation
            oCOP = CPQ_Test_Setup.newCOP(oAgreement);
            oCOP.RecordTypeID  = oRecTypeCOP.id;
            oCOP.COP_Status__c = CPQ_COP_STATUS_COMPLETED;
            insert oCOP;
            String strCOPid = oCOP.id;

            
            //RecordType for the COP Line Item record
            RecordType oRecTypeCOPLI = [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.IsActive, r.Id, r.DeveloperName From RecordType r where r.DeveloperName = 'Contracts_Team_Wireless' and r.SobjectType = 'COP_Line_Item__c'];
            
            //COP Line Item Creation  with Wireless RecordType
            oCOPLineItem = CPQ_Test_Setup.newCOPLineItem(oCOP);
            oCOPLineItem.RecordTypeID  = oRecTypeCOPLI.id;
            insert oCOPLineItem;
            
            //AgreementLineItems for 'Engagement Manager (IoT/Services)' COP Type
            lstAgreementLineItem[0].Apttus__ProductId__c = oProduct.id;
            lstAgreementLineItem[0].Apttus__AgreementId__c = oAgreement.id;
            insert lstAgreementLineItem;
            
            //Account Update for the Wireless ID field
            oAccount.Latest_Wireless_COP__c = strCOPid;
            update oAccount;            
            
            List<Apttus__AgreementLineItem__c> lstAgreementLInew = new List<Apttus__AgreementLineItem__c>([Select id,Apttus__ProductId__c,Apttus__AgreementId__c, Apttus__AgreementId__r.Apttus__Account__r.Latest_Wireless_COP__c from Apttus__AgreementLineItem__c where Apttus__AgreementId__c =:oAgreement.id]);
        
            system.assert(lstAgreementLInew[0].Apttus__AgreementId__r.Apttus__Account__r.Latest_Wireless_COP__c == strCOPid);
        
            //Assertion to check if the Account has been updated with the COP Salesforce ID
            List<Account> lstAccount = [select id, Latest_Wireless_COP__c from Account where Id=:oAccount.id ];
            system.assert(lstAccount[0].Latest_Wireless_COP__c == strCOPid);
                     
            List<COP__c> lstCOPnew = new List<COP__c>();
            //Creating COP Records for bulkification testing
            for(integer i=0; i<100; i++)
            {
                oCOPnew = CPQ_Test_Setup.newCOP(oAgreement);
                lstCOPnew.add(oCOPnew);
            }
            insert lstCOPnew;

            String strNewCOPid = lstCOPnew[0].id;   
        
            //Calling the Controller 
            PageReference pageRef = Page.CPQ_createCOPandCOPLineItems;
            Test.setCurrentPage(pageRef);
            
            ApexPages.currentPage().getParameters().put('Id',oAgreement.id);
            CPQ_createCOPandCOPLineItemsCont controller = new CPQ_createCOPandCOPLineItemsCont();
            controller.createCOP();
            controller.backToAgreement();
            
            //Assertion to check if the COP has been created with Wireless Record Type
            List<COP__c> lstCOP = [select id, Agreement__c,RecordTypeID from COP__c where Agreement__c=:oAgreement.id ];
            system.assert(lstCOP[0].Agreement__c == oAgreement.id);
            system.assert(lstCOP[0].RecordTypeID == oRecTypeCOP.id);
            
            //Assertion to check if the COP Line Item has been created with WireLess Record Type on the New COP Record
            List<COP_Line_Item__c> lstCOPLI = [select id, COP__c,RecordTypeID from COP_Line_Item__c where COP__c=:lstCOP[0].id ];
            system.assert(lstCOPLI[0].COP__c == lstCOP[0].id);
            system.assert(lstCOPLI[0].RecordTypeID == oRecTypeCOPLI.id);
            
            List<COP_Line_Item__c> lstCOPLIOld = [select id, COP__c from COP_Line_Item__c where COP__c =:oCOP.id ];
            system.assert(lstCOPLI.size() == lstCOPLIOld.size());
         
        }
    }
  
}