/*********************************************************************************
Class Name      : CPQ_Test_AgreementAcceptDocController 
Description     : This class a test class for CPQ_AgreementAcceptDocController trigger. 
Created By      : Deepika Rawat
Created Date    : 14-Dec-2015
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Deepika Rawat              14-Dec-2015             Original version
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest (seeAllData=False) 
private class CPQ_Test_AgreementAcceptDocController{
    static User TestAEUser;
    static Account oAccount;
    static Opportunity oOpportunity;
    static Apttus_Proposal__Proposal__c oQuote;
    static Attachment attachmentQuote;
    static Apttus_Config2__PriceList__c oPriceList;
    static Apttus__APTS_Agreement__c oAgreement;
    static List<Apttus__APTS_Agreement__c> lstAgreement;
    static List<Case> lstCase;
    static Static_Data_Utilities__c csStatic;
    private static User testSUser;
    private static CPQ_Settings__c csMapping;
    private static AccountTeamMember oAccountTeamMember;
    private static final Integer CPQ_AGREEMENTS_TOTAL_NUMBER = 200;
    private static final String CPQ_AGREEMENTS_PROFILES_SETTING_VALUE = 'EBU - Rogers Specialist|Read';
    private static final String CPQ_AGREEMENTS_PROFILES_SETTING_VALUE2 = 'EBU - Rogers Resellers Sales Rep|Read';
    private static final String CPQ_ACCOUNT_SPECIALIST = 'EBU - Rogers Specialist';
    private static final String CPQ_SYSTEM_ADMIN = 'System Administrator';
    private static final String CPQ_ADMIN_USERNAME = 'standarduser_test123@testorg.com';
    private static final String CPQ_SPECIALIST_USERNAME = 'specialistuser_test123@testorg.com';
    private static final String CPQ_STATUS_CATEGORY_IN_EFFECT = 'In Effect';
    private static final String CPQ_STATUS_ACTIVATED = 'Activated';
    private static final String CPQ_STATUS_CATEGORY_IN_SIGNATURES = 'In Signatures';
    private static final String CPQ_STATUS_FULLY_SIGNED = 'Fully Signed';
    private static final String CPQ_OPPORTUNITY_STAGE_CLOSED = 'Closed Won';
    private static final String CPQ_OPPORTUNITY_CLOSED_RECORDTYPE = 'Rogers EBU - Closed';
    private static final String CPQ_AGREEMENTS_LEGAL_NAME_RECORDTYPE = 'Rogers EBU Agreement - Legal Name Change';
    private static final String CPQ_STATUS_SIGNATURE_NOTREQUIRED = 'Signature Not Required';
    private static final String CPQ_STATUS_CATEGORY_IN_REQUEST ='Request';
    private static final String CPQ_STATUS_INTERNAL_SIGNATURE='Internal Signatures';
    /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testMethod void createTestData() {
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');      
        testSUser = CPQ_Test_Setup.newUser(CPQ_ACCOUNT_SPECIALIST, CPQ_SPECIALIST_USERNAME);
        insert testSUser;
        system.runAs(TestAEUser){
            
            //Create PriceList
            oPriceList = CPQ_Test_Setup.newPriceList();
            insert oPriceList ;
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
            insert oQuote;
            //Create attachment for Quote
            attachmentQuote = CPQ_Test_Setup.newAttachment(oQuote.id);
            insert attachmentQuote;
            //Create Agreement
            oAgreement= CPQ_Test_Setup.newAgreement(oAccount, oOpportunity,oQuote);
            //Create List of Agreements
            lstAgreement= CPQ_Test_Setup.newlstAgreement(oAccount, oOpportunity,oQuote, 300);
            
            //Inserting Custom setting CPQ_Settings__c
            csMapping  = CPQ_Settings__c.getOrgDefaults();
            csMapping.AgreementsProfiles__c = CPQ_AGREEMENTS_PROFILES_SETTING_VALUE;
            csMapping.AgreementsProfiles2__c = CPQ_AGREEMENTS_PROFILES_SETTING_VALUE2;
            csMapping.Agreement_Category_Status_to_Close_Opp__c=CPQ_STATUS_CATEGORY_IN_REQUEST+','+CPQ_STATUS_CATEGORY_IN_SIGNATURES;
            csMapping.Agreement_Status_to_Close_Opp__c=CPQ_STATUS_SIGNATURE_NOTREQUIRED+','+CPQ_STATUS_INTERNAL_SIGNATURE;
            upsert csMapping;
        }
    }
    /*********************************************************************************
    Method Name    : testAgreementUpdate
    Description    : Method to test Agreement Update
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testAgreementUpdate(){
        createTestData();
        
        system.runAs(TestAEUser){
            oQuote.Primary_Quote_Document_ID__c = attachmentQuote.id;
            update oQuote;
            oAgreement.Apttus__Status_Category__c = 'In Signatures';
            oAgreement.Apttus__Status__c  = 'Customer Signatures';
            insert oAgreement;
            test.startTest();
            ApexPages.currentPage().getParameters().put('id', oAgreement.id);
            CPQ_AgreementAcceptDocController obj = new CPQ_AgreementAcceptDocController();
            obj.updateAgreement();
            obj.backToAgreement();
            Apttus__APTS_Agreement__c oAgreementChk = [Select Apttus__Status__c from Apttus__APTS_Agreement__c where id=:oAgreement.id];
            System.Assert(oAgreementChk.Apttus__Status__c == 'Customer Signatures Received');
            Test.stopTest();
        }
    }
}