/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CampaignCallDownControllerTest {

     private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
     static testMethod void testNormalFlow(){
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        TAG_CS.TAG_Ownership_Requests__c = 'Account Owner,MSD Owner,Shared MSD Owner,Account District,MSD District,Shared MSD District';
        TAG_CS.TAG_Membership_Requests__c = 'Account Team,MSD Member,Shared MSD Member';
        insert TAG_CS;


        //campaign
        Campaign testCampaign       = new Campaign();
        testCampaign.name           = 'TestCampaign';
        testCampaign.isActive       = TRUE;
        insert testCampaign;
        
        //Lead
        Lead testLead1          = new Lead();
        testLead1.FirstName     = 'LeadFirstName1';
        testLead1.LastName      = 'LeadLastName1';
        testLead1.Company       = 'Test1';
        testLead1.Street        = '123 abc st';
        testLead1.City          = 'testCity';
        testLead1.State         = 'BC';
        testLead1.PostalCode    ='N1N 1N1';
        testLead1.Country       ='Canada';
        testLead1.Email         = 'Adress2@Adress.com';
        testLead1.Status = 'Open';
        List<String> LeadTypes = new List<String>{'Wireless','Wireline','Data Centre'};
        testLead1.Lead_type__c = LeadTypes[math.round(math.random()*(LeadTypes.size()-1))];
        insert testLead1;
        
        
        
        //Account
        Account a = new Account();
        a.name= 'A45cc56ou67ntTe57804st';
        a.ParentId = null;
        a.Account_Status__c = 'Assigned';
        a.BillingPostalCode ='A1A 1A1';
        insert a;
        //Lead
        Lead testLead2          = new Lead();
        testLead2.FirstName     = 'LeadFirstName2';
        testLead2.LastName      = 'LeadLastName2';
        testLead2.Company       = 'Test2';
        testLead2.Status = 'Open';
        testLead2.Email  = 'deeRog@deeRogTest.com';
        testLead2.Contact_Type__c ='Executive';
        testLead2.Type_of_Sales_Process__c = 'Complex';
        testLead2.Lead_type__c = LeadTypes[math.round(math.random()*(LeadTypes.size()-1))];
        

        insert testLead2;
        
        //Contact
        Contact testContact         = new Contact();
        testContact.FirstName       = 'ContactFirstName';
        testContact.LastName        ='ContactLastName';
        testContact.Email           = 'Adress2@Adress.com';
        testContact.Title           = 'ContactTitile';
        testContact.MailingStreet   ='456 garrik st';
        testContact.MailingCity    = 'testCity'; 
        testContact.MailingState    = 'AB';
        testContact.mailingPostalCode  ='A9A 9A9';
        testContact.MailingCountry  ='CA';
        testContact.phone='123';
        testContact.Accountid=a.id;
        testContact.Contact_Type__c = 'other';
        insert testContact;
        
      
        
        //Contact2
        Contact testContact2        = new Contact();
        testContact2.FirstName      = 'ContactFirstName2';
        testContact2.Email          = null;
        testContact2.LastName       ='ContactLastName2';
        testContact2.Title          = 'ContactTitile2';
        testContact2.phone='123';
        testContact2.Accountid=a.id;
        testContact2.MailingCountry = 'CA';
        testContact2.MailingState = 'AB';
        testContact2.MailingPostalCode = 'A9A 9A9';
        insert testContact2;
        
        //make campaign members
        CampaignMember testCM       = new CampaignMember();
        testCM.leadID               = testLead1.Id;
        testCM.campaignID           = testCampaign.Id;
        testCM.Status               ='Responded';
        insert testCM;
        
        //make campaign members
        CampaignMember testCM1      = new CampaignMember();
        testCM1.leadID              = testLead2.Id;
        testCM1.campaignID          = testCampaign.Id;
        testCM1.Status              = 'Responded';
        insert testCM1;
        
        CampaignMember testCM2      = new CampaignMember();
        testCM2.ContactId           = testContact.Id;
        testCM2.CampaignId          = testCampaign.Id;
        testCM2.Assigned_To__c= UserInfo.getUserId();
        testCM2.Status              = 'Responded';
        insert testCM2;
        
        CampaignMember testCM3      = new CampaignMember();
        testCM3.ContactId           = testContact2.Id;
        testCM3.CampaignId          = testCampaign.Id;
        testCM3.Status              = 'Responded';
        insert testCM3;
        
        //begin tests
        CampaignCallDownController ccdc1 = new CampaignCallDownController();
       
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(testLead2.id);
        lc.setDoNotCreateOpportunity(True); //**IMPORTANT METHOD HERE**
        lc.setAccountId(a.id);
        lc.setContactId(testContact2.id) ;

        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        
        

        
        List<CampaignMember> testCMList = [ SELECT id, ContactId, CampaignId, campaign.Name, campaign.id,LeadId, Status,Lead.Id,Lead.Name,CampaignMember.Priority__c,Due_Date__c,notes__c,Lead.Phone,Lead.Owner.FirstName,Lead.Owner.LastName,Lead.OwnerId,Lead.Salutation,Lead.Company,CampaignMember.Assigned_To__c,Assigned_to__r.FirstName,Assigned_to__r.LastName, Lead.isConverted,Lead.ConvertedContactId FROM CampaignMember WHERE id =: testCM.id OR id =: testCM1.id OR id =: testCM2.id OR id =: testCM3.id ];
        ccdc1.updateAll();
        
        List<CampaignMember> testCMList2 = [ SELECT id, ContactId,campaign.Name,campaign.id, CampaignId, Status FROM CampaignMember WHERE id =: testCM.id OR id =: testCM1.id OR id =: testCM2.id OR id =: testCM3.id ];
        Map<String, String> statusMap = new Map<String, String>();
         ccdc1.getcampaignItems();
        statusMap.put('New','Pending');
        String camp = 'All';
        Map<id,list<SelectOption>> optionsTest = new Map<id,list<SelectOption>>();
        optionsTest.put(testCMList[0].id,ccdc1.getcampaignItems());
        CampaignCallDownController.leadPlusTaskPlusContact objinner = new CampaignCallDownController.leadPlusTaskPlusContact(testCMList[0], statusMap, camp, optionsTest);
        CampaignCallDownController.leadPlusTaskPlusContact objinner2 = new CampaignCallDownController.leadPlusTaskPlusContact(testCM1, statusMap, camp, optionsTest);
        List<CampaignCallDownController.leadPlusTaskPlusContact> objList = new List<CampaignCallDownController.leadPlusTaskPlusContact>();
        objinner.t.Description='Test';
        insert objinner.t;
        objList.add(objinner);
        objList.add(objinner2);
        ccdc1.ownerId = UserInfo.getUserId();
        ccdc1.leadPlusTasks = objList;
        ccdc1.sortByCampName();
        ccdc1.sortByCompany();
        ccdc1.sortBydueDate();
        ccdc1.sortByPhone();
        ccdc1.sortByStatus();
        ccdc1.sortByPriority();
        ccdc1.sortByNotes();
        ccdc1.sortByName();
        ccdc1.sortBySAL();
        ccdc1.updateAll();
        ccdc1.campaignCheck();
        ccdc1.gethasCampaignID();
        ccdc1.getowners();
        ccdc1.newCampaignRefresh();
       
        ccdc1.getName();
        ccdc1.SearchText ='A45cc56ou67ntTe57804st';
        //ccdc1.getmemStatusValue();
        ccdc1.Beginning();
        

        // No changes
       // System.assertEquals( testCMList, testCMList2 );
        
        List<Selectoption>sO=ccdc1.getstatusItems();
        
        ccdc1.camp = testCampaign.Id;
        String s = ccdc1.getCampaignName();
        
        //Campaign name
       // System.assertEquals( s, 'All Leads' );
        
        ccdc1.campaignRefresh();
        s = ccdc1.getCampaignName();
        sO=ccdc1.getstatusItems();
        
        List<Selectoption> options= new List<Selectoption>{ new Selectoption( '--ALL--', '--ALL--'), new Selectoption( 'Sent', 'Sent' ), new Selectoption( 'Responded', 'Responded')};
       
        // Options
        //System.assertEquals( sO[ 0 ].getLabel(), options[ 0 ].getLabel());
        //System.assertEquals( sO[ 1 ].getLabel(), options[ 1 ].getLabel());
        //System.assertEquals( sO[ 2 ].getLabel(), options[ 2 ].getLabel());
        //System.assertEquals( sO[ 0 ].getValue(), options[ 0 ].getValue());
        //System.assertEquals( sO[ 1 ].getValue(), options[ 1 ].getValue());
        //System.assertEquals( sO[ 2 ].getValue(), options[ 2 ].getValue());
        //Campaign name
  //      System.assertEquals( s, 'TestCampaign' );
        
    
    
       if(ccdc1!=null && ccdc1.getLeadPlusTasks()!=null && ccdc1.getLeadPlusTasks().size()>0 && ccdc1.getLeadPlusTasks()[0].statusUpdate!=null){
        ccdc1.getLeadPlusTasks()[0].statusUpdate = 'Sent';
        System.debug('******' + ccdc1.getLeadPlusTasks().size());
        ccdc1.updateAll();
        System.debug('******' + ccdc1.getLeadPlusTasks().size());
        List<CampaignMember> testCMList3 = [ SELECT id, ContactId, CampaignId, Status FROM CampaignMember WHERE id =: testCM.id OR id =: testCM2.id OR id =: testCM3.id ];
        
        String name1 = ccdc1.getLeadPlusTasks()[ 0 ].name;
       // String name2 = ccdc1.getLeadPlusTasks()[ 1 ].name;
        
        ccdc1.sortByName();
        ccdc1.sortByName();

        ccdc1.sortByPhone();
        ccdc1.sortByPhone();
        
        ccdc1.sortByDueDate();
        ccdc1.sortByDueDate();
        
        ccdc1.sortByPriority();
        ccdc1.sortByPriority();
        
        ccdc1.sortByNotes();
        ccdc1.sortByNotes();
        
        ccdc1.sortByStatus();
        ccdc1.sortByStatus();
        
        ccdc1.sortByCampName();
        ccdc1.sortByCampName();
        
        ccdc1.sortBySAL();
        ccdc1.sortBySAL();
        
        ccdc1.sortByCompany();
        ccdc1.sortByCompany();
        
        ccdc1.sortBydueDate();
        ccdc1.sortBydueDate();

        // Changes
    //    System.assert( testCMList != testCMList3, 'The collections not be equals.' );
        
        sO=ccdc1.getcampaignItems();
        
        List<Selectoption> auxCampList = new List<Selectoption>(); 
        auxCampList.add(new SelectOption( '1','SELECT' ));
        
        list<Campaign> listCampaign = new list<Campaign>([Select Name, Id From Campaign Where isactive = true order by Name limit 100]);
        for(Campaign c : listCampaign)
            auxCampList.add( new SelectOption( c.id, c.name ));
            
        Integer k = 0;
    
        ccdc1.getLeadPlusTasks()[ 0 ].getmemStatusValue();
        ccdc1.getLeadPlusTasks()[ 0 ].sett( ccdc1.getLeadPlusTasks()[ 0 ].gett());
        ccdc1.getLeadPlusTasks()[ 0 ].getmemStatus();
    
    
    
       // System.assert( ccdc1.gethasCampaignID());
        
        ccdc1.status='Responded';
        ccdc1.campaignRefresh();
       // System.assert( ccdc1.status == 'Responded');
        
        ccdc1.status='All';
        ccdc1.campaignRefresh();
       // System.assert( ccdc1.status == 'All');
        
        sO=ccdc1.getcampaignItems();
        sO=ccdc1.getstatusItems();
        
        s = ccdc1.getCampaignName();
        boolean b = ccdc1.getName();
        
       } 
      }
  
}