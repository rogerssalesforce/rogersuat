/*
 * Controller for multi attachment component
 */
/*
========================================================================
Date                        Name                        Status
March 2015         Aakanksha Patel            Modified(For TAG)
========================================================================
*/ 
 
public without sharing class MultiAttachmentController 
{
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c(); 

    // the parent object it
    public Id sobjId {get; set;}
    
    // list of existing attachments - populated on demand
    public List<Attachment> attachments;
    
    // list of new attachments to add
    public List<Attachment> newAttachments {get; set;}
    
    // the number of new attachments to add to the list when the user clicks 'Add More'
    public static final Integer NUM_ATTACHMENTS_TO_ADD=5;

    // constructor
    public MultiAttachmentController()
    {
        // instantiate the list with a single attachment
        newAttachments=new List<Attachment>{new Attachment()};
    }   
    
    // retrieve the existing attachments
    public List<Attachment> getAttachments()
    {
        // only execute the SOQL if the list hasn't been initialised
        if (null==attachments)
        {
            attachments=[select Id, ParentId, Name, Description from Attachment where parentId=:sobjId];
        }
        
        return attachments;
    }

    // Add more attachments action method
    public void addMore()
    {
        // append NUM_ATTACHMENTS_TO_ADD to the new attachments list
        for (Integer idx=0; idx<NUM_ATTACHMENTS_TO_ADD; idx++)
        {
            newAttachments.add(new Attachment());
        }
    }    
    
    // Save action method
    public void save()
    {
        List<Attachment> toInsert=new List<Attachment>();
        for (Attachment newAtt : newAttachments)
        {
            if (newAtt.Body!=null)
            {
                newAtt.parentId=sobjId;
                toInsert.add(newAtt);
            }
        }
        insert toInsert;
        newAttachments.clear();
        newAttachments.add(new Attachment());
        
        // null the list of existing attachments - this will be rebuilt when the page is refreshed
        attachments=null;
    }
    
    // Action method when the user is done
    public PageReference done()
    {
        // send the user to the detail page for the sobject
        return new PageReference('/' + sobjId);
    }
    
    /******************************************************
     *
     * Unit Tests
     *
     ******************************************************/
     
    static testMethod void testController()
    {
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rto : lRta)
        {
            mapRTa.put(rto.Name,rto.id);  
        } 
          
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;  
          
        Account a = new Account(); 
        a.name = 'Test Act Opp Web Services';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('Carrier Account');
        a.BillingStreet = 'Street';
        a.BillingCity = 'Ontario';
        a.BillingCountry = 'CA';
        a.BillingPostalCode = 'A9A 9A9';
        a.BillingState = 'ON';
        a.parentId = null ;
        a.Account_Status__c = 'Assigned';
        insert a;
                
        
        MultiAttachmentController controller=new MultiAttachmentController();
        controller.sobjId=a.id;
        
        System.assertEquals(0, controller.getAttachments().size());
        
        System.assertEquals(1, controller.newAttachments.size());
        
        controller.addMore();
        
        System.assertEquals(1 + NUM_ATTACHMENTS_TO_ADD, controller.newAttachments.size());
        
        // populate the first and third new attachments
        List<Attachment> newAtts=controller.newAttachments;
        newAtts[0].Name='Unit Test 1';
        newAtts[0].Description='Unit Test 1';
        newAtts[0].Body=Blob.valueOf('Unit Test 1');

        newAtts[2].Name='Unit Test 2';
        newAtts[2].Description='Unit Test 2';
        newAtts[2].Body=Blob.valueOf('Unit Test 2');
        
        controller.save();
        controller.done();
        
        System.assertEquals(2, controller.getAttachments().size());
        System.assertNotEquals(null, controller.done());
    }
}