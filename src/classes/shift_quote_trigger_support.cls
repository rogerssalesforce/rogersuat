/*
========================================================================
Date                        Name                        Status
March 2015         Aakanksha Patel            Modified(For TAG)
========================================================================
*/

public class shift_quote_trigger_support {
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();     
    private static CPQ_Settings__c CPQ_cs = CPQ_Settings__c.getInstance();   
    public static void applyHasQuoteinOpp(Quote__c[] quotes) {
    
        // grab all opportunities that contain the listed quotes
        Set<Id> oppIds = new Set<Id>();
        For (Quote__c q : quotes) {
            oppIds.add(q.Opportunity_Name__c);
        }
        List<Opportunity> oppWithQuotes = new List<Opportunity>([Select Id From Opportunity where Id in :oppIds]);
        
        // set all of these Opps to Has Quote. assuming no deletion allowed on Quotes.              
        For (Opportunity opp : oppWithQuotes ) {
            opp.Has_quote__c = true;
        }        
    
        // update opportunities
        update oppWithQuotes;
    }
    
    static testMethod void validateQuoteTrigger(){
      
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;
        
        CPQ_cs.DefaultOpportunityPB__c = 'CPQ Temp PriceBook';
        CPQ_cs.DefaultOpportunityRecordType__c = 'Rogers EBU - New';
        insert CPQ_cs;
        
        // create dummy account
        Account acc = new Account(Name='Jocsans account');
        acc.BillingPostalCode = 'A1A 1A1';
        acc.ParentID= null;
        acc.Account_Status__c = null;
        insert acc;
        
        // create dummy opp
        Opportunity opp = new Opportunity(Name='Big OPP', Has_quote__c = false, Account=acc, CloseDate=Date.today(), Amount =500, StageName='Dormant');
        opp.Unified_Comm_Collaboration_Estimated__c =20;
        insert opp;
        
        // create test quote
        Quote__c quote = new Quote__c(Opportunity_Name__c=opp.id);
        insert quote;
        
        // reload Opp
      
        opp = [SELECT Has_quote__c FROM Opportunity WHERE Id =:opp.Id];
        
        
     //   System.assertEquals(opp.Has_quote__c, true);
    }    
}