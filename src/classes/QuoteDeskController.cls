/* ********************************************************************
 * Name : QuoteDeskController
 * Description : Controller. Quote Desk Controller
 * Modification Log
  =====================================================================
 * Ver    Date          Author                  Modification
 ----------------------------------------------------------------------
 * 1.0    12/01/2012    Kevin DesLauriers       Initial Version  
 **********************************************************************/

public without sharing class QuoteDeskController { // maybe without sharing
    public Carrier_Quote__c carrierQuote {get; set;}    // We will want to get rid of this as the Quote Request will have these.
    public Quote_Request__c quoteRequest {get; set;}
    public Integer landingCarrierQuote {get; private set;}
    
    public List<Carrier_Quote__c> carrierQuotes {get; set;}
    public List<Carrier_Quote_Information__c> carrierQuoteInformationList {get; set;}
    public String quoteRequestId  {get; set;}
    public Map<Id, List<Carrier_Quote_Information__c>> carrierQuoteInformationMap {get; set;}
    public Set<Site__c> sites {get; set;}       // Identifies the sites for a Carrier Quote on the VF Page.
    public List<QuoteLineItem> quoteLineItems_3rdParty {get; set;}
    public Boolean renderSelectButton {get; set;}       // Allows Sales to select items
    public Boolean isReadOnly {get; set;}               // identifies if the fields should be read only.
    public Quote q {get; set;}
    public Boolean isCarrierVisible {get; set;}
    public Integer option {get; set;}
    public Map<Id, String> siteMap {get; set;}	// used to display the site name in the VF Page on save
    
    
    public QuoteDeskController (ApexPages.StandardController stdController) {
        String carrierQuoteRequestId = ApexPages.currentPage().getParameters().get('carrierQuoteRequestId');
        quoteRequestId = ApexPages.currentPage().getParameters().get('quoteRequestId');
        option = 1;
        
        /* If both the Quote Request Id and the Carrier Quote Id are missing then there is nothing that we can do. */
        if (Utils.isEmpty(quoteRequestId) && Utils.isEmpty(carrierQuoteRequestId)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Missing Quote Request Id in URL. Please ensure you navigated to the quote desk module correctly.  If you continue having issues, please contact your system adminstrator'));
            return;
        }else if (!Utils.isEmpty(carrierQuoteRequestId)){   // We don't have a carrier quote request Id.  Let's get it from the Carrier Quote Object
            try {
                carrierQuote = [SELECT Id, Quote_Request__c FROM Carrier_Quote__c WHERE Id = :carrierQuoteRequestId];
            }catch(Exception ex){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Invalid Id in URL. Please ensure that if you are editing an existing Carrier Quote you do so via the Edit 3rd Party Quote Button on the Carrier Quote Detail Page.'));
                return;
            }
            
            if (carrierQuote == null || carrierQuote.Quote_Request__c == null){ // We are unable to obtain the Quote Request Id
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Invalid Id in URL. Unable to obtain a valid Quote Request Id.'));
                return;
            }else if (carrierQuote.Quote_Request__c == null){
                quoteRequestId = carrierQuote.Quote_Request__c;
            }
                
        }
        
        // Life is good and we have the Quote Request Id.  Let' see if we can get the Quote Request Object and its Carrier Quotes.
        try{
            quoteRequest = [SELECT Id, Name, Quote__c, Quoting_Stage__c  FROM Quote_Request__c WHERE Id = :quoteRequestId];
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Invalid Id in URL. Unable to obtain a valid Quote Request.Please ensure you navigated to the quote desk module correctly.  If you continue having issues, please contact your system adminstrator'));
            return;
        }
        
        // We have the Quote Request - now to see if there are any Carrier Quotes already.
        
        try{
            carrierQuotes = [SELECT Id, X3rd_Party_Quote__c, Carrier_Name__c, Expiry_Date__c, Facility_Inquiry__c, Selected__c, Receive_Date__c, Requested_Date__c, Quote_Request__c,
                (SELECT Id, Build_Cost__c, Carrier_Quote__c, Comments__c, MRC__c, NRC__c, Service__c, Term__c, Site__c, Site_Display_Name1__c, Site__r.Display_Name__c, Service_Name__c FROM Carrier_Quote_Information__r)
             FROM Carrier_Quote__c WHERE Quote_Request__c = :quoteRequestId order by CreatedDate ASC];
        }catch(Exception ex){
            // There are no Carrier Quotes Associated with the Quote Request
            carrierQuotes = new List<Carrier_Quote__c>();
            landingCarrierQuote = 0;
        }
        
        quoteLineItems_3rdParty = [SELECT id, Site__c, Site__r.Display_Name__c, PriceBookEntry.Product2.Access_Type__c, PriceBookEntry.Product2.Access_Type_Group__c, PriceBookEntry.Name, Service_Type__c FROM QuoteLineItem WHERE QuoteId = :quoteRequest.Quote__c AND PriceBookEntry.Product2.Access_Type__c = 'OFF NET 3rd Party' and Visible_Quote_Line_Item__c = true];
        
        siteMap = new Map<Id, String>();
        // Update the Site Map
        for (QuoteLineItem qli : quoteLineItems_3rdParty){
            siteMap.put(qli.Site__c, qli.Site__r.Display_Name__c);
        }
        
        // Create Map of the Carrier Quote Id to its List of Carrier Quote Information.
        
        carrierQuoteInformationMap = new Map<Id, List<Carrier_Quote_Information__c>>();
        
        
            landingCarrierQuote = 0;
            Boolean foundLandingTab = false;
            
            for (Carrier_Quote__c c : carrierQuotes){
                if (!foundLandingTab && !Utils.isEmpty(carrierQuoteRequestId)){
                    if (carrierQuoteRequestId == c.Id){
                        foundLandingTab = true;
                    }else{
                        landingCarrierQuote++;
                    }
                }
                if (c.Carrier_Quote_Information__r != null && !c.Carrier_Quote_Information__r.isEmpty()){
                    carrierQuoteInformationMap.put(c.Id, c.Carrier_Quote_Information__r );
                }
            } 
            
        
        User currentUser = [SELECT Id, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
    
        // If this is already approved then we cannot select any more options.
        q = [SELECT Id, Opportunity.OwnerId, Discount_Approval__c, In_Enterprise_Approval__c, Engineer_Approval_Process__c FROM Quote WHERE Id = :quoteRequest.quote__c];
        boolean quoteRequestCompleted = quoteRequest.Quoting_Stage__c == 'Closed by Quote Desk';
        
        // renderSelectButton = (quoteRequestCompleted && currentUser.Profile.Name == 'Network Design Consultant') || (quoteRequestCompleted && currentUser.Profile.Name == 'System Administrator') || (quoteRequestCompleted && currentUser.Id == q.Opportunity.OwnerId && ( !(q.Discount_Approval__c || q.In_Enterprise_Approval__c || q.Engineer_Approval_Process__c)));
        renderSelectButton = false;
        isReadOnly = (quoteRequestCompleted || currentUser.Profile.Name != 'Serviceability Team') && currentUser.Profile.Name != 'System Administrator';
        isCarrierVisible = true;//currentUser.Profile.Name == 'Serviceability Team' || currentUser.Profile.Name == 'System Administrator';
    }
 
    
    public PageReference createCarrierQuote(){
        // If there is not a Carrier Quote already.  Create One.
        if (carrierQuotes == null){
            carrierQuote = new Carrier_Quote__c();
            carrierQuote.Quote_Request__c = quoteRequestId;
            INSERT carrierQuote;
        }
        
        return null;
    }
    
    public PageReference removeItem(){
        String carrierInfoId = Apexpages.currentPage().getParameters().get('idParam');
        String carrierQuoteId = Apexpages.currentPage().getParameters().get('idParam2');
         
        if (Utils.isEmpty(carrierInfoId)){
            return null;
        }
        
        List<Carrier_Quote_Information__c> infoItems = carrierQuoteInformationMap.get(carrierQuoteId);
		

		if (infoItems != null){
	        for (Integer i = 0; i<infoItems.size(); ++i){
	            if (infoItems[i].Id == carrierInfoId){
	                infoItems.remove(i);
	                break;
	            }
	        }
		}
		
		try{
        	Carrier_Quote_Information__c cInfo = [SELECT Id FROM Carrier_Quote_Information__c WHERE Id = :carrierInfoId];
        	DELETE cInfo;
		}catch(Exception ex){
			
		}
        return null; 
    }
    
    public PageReference addNewItem(){
        String carrierQuoteId = Apexpages.currentPage().getParameters().get('idParam');
        
        Carrier_Quote_Information__c newCarrierQuoteInfo = new Carrier_Quote_Information__c();
        newCarrierQuoteInfo.Carrier_Quote__c = carrierQuoteId;
        
        
        INSERT newCarrierQuoteInfo;
        
        List<Carrier_Quote_Information__c> carrierInfoList = carrierQuoteInformationMap.get(carrierQuoteId);
        
        if (carrierInfoList == null)
        	carrierInfoList = new List<Carrier_Quote_Information__c>();
        carrierInfoList.add(newCarrierQuoteInfo);
        carrierQuoteInformationMap.put(carrierQuoteId, carrierInfoList);
        
        
        return null;
    }
    
    public PageReference saveAll(){
        String carrierId = ApexPages.currentPage().getParameters().get('idParam');
        String carrierQuote = ApexPages.currentPage().getParameters().get('idParam2');
        String carrierInfoDataList = ApexPages.currentPage().getParameters().get('idParam3');
        
        if (Utils.isEmpty(carrierId)){
            return null;
        } 
        
        try{
            CarrierQuoteObject cQuote = (CarrierQuoteObject)JSON.deserialize(carrierQuote, CarrierQuoteObject.class);
            List<CarrierInfoObject> cQuoteInfoList = (List<CarrierInfoObject>)JSON.deserialize(carrierInfoDataList, List<CarrierInfoObject>.class);
        
            List<Carrier_Quote_Information__c> cQuoteInfoToUpdate = new List<Carrier_Quote_Information__c>();
            List<Id> qliIds = new List<Id>();
            for (Carrier_Quote__c c : carrierQuotes){
                if (c.Id == carrierId){
                    c.X3rd_Party_Quote__c = cQuote.thirdPartyId;
                    c.Carrier_Name__c = cQuote.carrierName;
                    c.Expiry_Date__c = !Utils.isEmpty(cQuote.expiredDate)?date.parse(cQuote.expiredDate):null;
                    c.Facility_Inquiry__c = cQuote.facultyNum;
                    c.Selected__c = cQuote.preferred;
                    c.Receive_Date__c = !Utils.isEmpty(cQuote.receivedDate)?date.parse(cQuote.receivedDate):null;
                    c.Requested_Date__c = !Utils.isEmpty(cQuote.requestedDate)?date.parse(cQuote.requestedDate):null;
                    
                    Integer i = 0;
                    for (Carrier_Quote_Information__c cInfo : c.Carrier_Quote_Information__r){
                        CarrierInfoObject carrierInfoObject2 = cQuoteInfoList.get(i++);
                        cInfo.Service__c = carrierInfoObject2.service;
                        
                        if (!Utils.isEmpty(cInfo.Service__c))
                            qliIds.add(cInfo.Service__c);
                        
                        cInfo.term__c = carrierInfoObject2.term;
                        

                        if (!Utils.isEmpty(carrierInfoObject2.site))
                            cInfo.site__c = carrierInfoObject2.site;
                        cInfo.nrc__c = Utils.isEmpty(carrierInfoObject2.nrc)?null:Double.valueOf((carrierInfoObject2.nrc).remove(','));                        
                        cInfo.mrc__c = Utils.isEmpty(carrierInfoObject2.mrc)?null:Double.valueOf((carrierInfoObject2.mrc).remove(','));
                        cInfo.Comments__c = carrierInfoObject2.comments;
                        cInfo.Build_Cost__c = Utils.isEmpty(carrierInfoObject2.buildCost)?null:Double.valueOf((carrierInfoObject2.buildCost).remove(','));
                        
                    }
                    
                    Map<Id, QuoteLineItem> lineItems = new Map<Id, QuoteLineItem>();
                    
                    if (!qliIds.isEmpty()){
                        lineItems = new Map<Id, QuoteLineItem>([SELECT id, Service_Type__c FROM QuoteLineItem WHERE id IN :qliIds]);
                    }
                                        
                    for (Carrier_Quote_Information__c cInfo : c.Carrier_Quote_Information__r){
                        if (!Utils.isEmpty(cInfo.Service__c) && (lineItems.get(cInfo.Service__c) != null) && lineItems.get(cInfo.Service__c).Service_Type__c != null)
                            cInfo.Service_Name__c = lineItems.get(cInfo.Service__c).Service_Type__c;
                            
                        if (!Utils.isEmpty(cInfo.Site__c) && (siteMap.get(cInfo.Site__c) != null))
                            cInfo.Site_Display_Name1__c = siteMap.get(cInfo.Site__c);
                        
                        cQuoteInfoToUpdate.add(cInfo);
                    }
                    
                    update c;
                    update cQuoteInfoToUpdate;
                    break;
                }
            }
        }catch(Exception ex){
            System.debug('Bad things happened: ' + ex);
        }
        
        return null;
    }
    
    public PageReference saveItem(){
        String carrierInfoId = ApexPages.currentPage().getParameters().get('idParam');
        String carrierId = ApexPages.currentPage().getParameters().get('idParam2');
        String carrierInfoData = ApexPages.currentPage().getParameters().get('idParam3');
        
        if (Utils.isEmpty(carrierInfoId) || Utils.isEmpty(carrierId)){
            return null;
        } 
        
        
        try{
            CarrierInfoObject carrierInfoObject2 = (CarrierInfoObject)JSON.deserialize(carrierInfoData, CarrierInfoObject.class);
            
        
            List<Carrier_Quote_Information__c> cInfo = carrierQuoteInformationMap.get(carrierId);
        
            if (Utils.isEmpty(carrierInfoObject2.service)){
                return null;
            }
        
            
            List<QuoteLineItem> qli = [SELECT Id, Service_Type__c FROM QuoteLineItem WHERE Id = :carrierInfoObject2.service];
            
            for (Carrier_Quote_Information__c c : cInfo){           
                if (c.Id == carrierInfoId){ // There will only be 1
                    c.Service__c = carrierInfoObject2.service;
                    c.Service_Name__c = qli[0].Service_Type__c;
                    c.term__c = carrierInfoObject2.term;
                    c.site__c = carrierInfoObject2.site;
                    c.nrc__c = Utils.isEmpty(carrierInfoObject2.nrc)?null:Double.valueOf((carrierInfoObject2.nrc).remove(','));
                    c.mrc__c = Utils.isEmpty(carrierInfoObject2.mrc)?null:Double.valueOf((carrierInfoObject2.mrc).remove(','));
                    c.Comments__c = carrierInfoObject2.comments;
                    c.Build_Cost__c = Utils.isEmpty(carrierInfoObject2.buildCost)?null:Double.valueOf((carrierInfoObject2.buildCost).remove(','));
                    if (!Utils.isEmpty(c.Site__c) && (siteMap.get(c.Site__c) != null))
                            c.Site_Display_Name1__c = siteMap.get(c.Site__c);
                    upsert c;
                }
                
            }
        }catch(Exception ex){
            System.debug('Bad things happened: ' + ex);
        }
               
        return null;
    }
    
    
    
    /*
        Name:       QuoteDeskController.returnToQuote()
        Function:   Redirects user to the Quote based upon
                    the QuoteDeskController.quoteRequestId
    */
    public PageReference returnToQuote(){
        PageReference pageRef = new PageReference('/' + q.Id);
        pageRef.setRedirect(true);
        
        return pageRef;
    }
    
    
     /*
        Name:       QuoteDeskController.returnToQouteDesk()
        Function:   Redirects user to the Quote Request based upon
                    the QuoteDeskController.quoteRequestId
    */
    public PageReference returnToQuoteRequest(){
        PageReference pageRef = new PageReference('/' + quoteRequestId);
        pageRef.setRedirect(true);
        
        return pageRef;
    }
    
    public PageReference selectCarrierQuoteItem(){
        try{
            String carrierInfoId = ApexPages.currentPage().getParameters().get('idParam');
            
            System.debug('23.' + carrierInfoId);
            
            Carrier_Quote_Information__c info = [SELECT Id, Build_Cost__c, Carrier_Quote__c, Comments__c, MRC__c, NRC__c, Service__c, Term__c, Site__c, Site__r.Display_Name__c FROM Carrier_Quote_Information__c WHERE id = :carrierInfoId];
            
            List<QuoteLineItem> qli = [SELECT Id, PricebookEntryId FROM QuoteLineItem WHERE Id = :info.Service__c AND Quote.Term__c = :info.term__c];
            List<QuoteLineItem> qli1 = [SELECT Id, Cost_Price__c, UnitPrice, PriceBookEntry.Product2.Mark_Up_Factor__c, Quote_Line_Item_Install__c FROM QuoteLineItem WHERE QuoteId = :q.Id AND Site__c = :info.Site__c AND PricebookEntryId = :qli[0].PricebookEntryId];
            qli1[0].Cost_Price__c = info.MRC__c;
            qli1[0].UnitPrice = qli1[0].Cost_Price__c * qli1[0].PriceBookEntry.Product2.Mark_Up_Factor__c;
            UPDATE qli1;
            
            if (qli1 != null){
                QuoteLineItem qliInstall = [SELECT Id, Cost_Price__c, UnitPrice, PriceBookEntry.Product2.Mark_Up_Factor__c FROM QuoteLineItem WHERE Id = :qli1[0].Quote_Line_Item_Install__c];
                qliInstall.Cost_Price__c = info.NRC__c;
                qliInstall.UnitPrice = qliInstall.Cost_Price__c * qliInstall.PriceBookEntry.Product2.Mark_Up_Factor__c;
                UPDATE qliInstall; 
            }
                
            
            
            /* Request to remain on the Quote Request Summary Page */
            /* June 7 - KD */
            //PageReference pageRef = new PageReference('/' + quoteRequest.quote__c);
            //pageRef.setRedirect(true);
            return null;
        }catch(Exception ex){
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an issue with selecting the site and service combination.  Please check with the Quote Desk to ensure that the information was filled out correctly.');
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
    
    /*
        Name:       QuoteDeskController.addCarrierQuote()
        Function:   Creates a new Carrier Quote and refreshes the page.  
                    This will create a new tab on the VF page.
    */
    public PageReference addCarrierQuote(){
        Carrier_Quote__c carrierQuote = new Carrier_Quote__c();
        carrierQuote.Quote_Request__c = quoteRequestId;
        carrierQuote.Carrier_Name__c = 'New';
        INSERT carrierQuote;
        
        Carrier_Quote_Information__c newCarrierQuoteInfo = new Carrier_Quote_Information__c();
        newCarrierQuoteInfo.Carrier_Quote__c = carrierQuote.Id;
        INSERT newCarrierQuoteInfo;
        
        Carrier_Quote__c cQuote = [SELECT Id, X3rd_Party_Quote__c, Carrier_Name__c, Expiry_Date__c, Facility_Inquiry__c, Selected__c, Receive_Date__c, Requested_Date__c, Quote_Request__c,
                (SELECT Id, Build_Cost__c, Carrier_Quote__c, Comments__c, MRC__c, NRC__c, Service__c, Term__c, Site__c, Site__r.Display_Name__c FROM Carrier_Quote_Information__r)
             FROM Carrier_Quote__c WHERE Id = :carrierQuote.Id];
        
        carrierQuotes.add(cQuote);
        carrierQuoteInformationMap.put(cQuote.Id, cQuote.Carrier_Quote_Information__r );
        return null;
    }
    
    public PageReference removeCarrierQuote(){
        String carrierQuoteId = Apexpages.currentPage().getParameters().get('idParam');
        
        if (Utils.isEmpty(carrierQuoteId)){
            return null;
        }

        Carrier_Quote__c cQuote = [SELECT Id FROM Carrier_Quote__c WHERE Id = :carrierQuoteId];
        DELETE cQuote;
        
        return null;
    }
    
    /*
        Name:       QuoteDeskController.massUpload()
        Function:   Redirects user to the VF page that allows upload based on CSV
    */
    public PageReference massUpload(){
        PageReference pageRef = new PageReference('/apex/CarrierQuoteUploadFile?fromEdit=true&RetURL=' + quoteRequestId);
        pageRef.setRedirect(true);
        
        return pageRef;
    }
    
    
    public List<SelectOption> getSiteOptions(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--- Select One ---'));
        sites = new Set<Site__c>();
        
        for (QuoteLineItem qli : quoteLineItems_3rdParty){
            sites.add(qli.Site__r);
        }

        for (Site__c s : sites){
            options.add(new SelectOption(s.Id, s.Display_Name__c));
        }
        
        return options;
    }
    
    public List<SelectOption> getServiceOptions(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--- Select One ---'));
        for (QuoteLineItem qli : quoteLineItems_3rdParty){
            options.add(new SelectOption(qli.Id, qli.Service_Type__c));
        }
        
        return options;
    }
    
    
    public class CarrierInfoObject {
        public String Id;
        public String buildCost;
        public String mrc;
        public String nrc;
        public String comments;
        public String term;
        public String service;
        public String site;
        
            
        
        public CarrierInfoObject(){
            this.id = id;
            this.buildCost = buildCost;
            this.term = term;
            this.mrc = mrc;
            this.nrc = nrc;
            this.service = service;
            this.comments = comments;
            this.site = site;
        }
     }
     
     public class CarrierQuoteObject {
        public String Id;
        public String thirdPartyId;
        public String carrierName;
        public String expiredDate;
        public String facultyNum;
        public Boolean preferred;
        public String receivedDate;
        public String requestedDate;
        
            
        
        public CarrierQuoteObject(){
            this.id = id;
            this.thirdPartyId = thirdPartyId;
            this.carrierName = carrierName;
            this.expiredDate = expiredDate;
            this.facultyNum = facultyNum;
            this.preferred = preferred;
            this.receivedDate = receivedDate;
            this.requestedDate = requestedDate;
        }
     }
    
}