@isTest(seeAllData = false)
public class CPQ_Test_ProposalLineTriggerHelper{
    static User TestAEUser;
    static product2 oProduct;
    static Account oAccount;
    static Account ocarrierAccount;
    static Account oIntercoAccount;
    static Account oIDVaccount;
    static Opportunity oOpportunity;
    static Opportunity IDVOpportunity;
    static Opportunity oIntercoOpportunity;
    static Opportunity oCarrierOpp;
    //static Apttus_Proposal__Proposal__c oQuote;
    static Apttus_Config2__PriceList__c oPriceList ;
    static Apttus_Config2__PriceList__c oEBUPriceList;
    static Apttus_Config2__PriceList__c EnterprisePriceList;
    static Apttus_Config2__PriceList__c IDVPriceList;
    static Apttus_Config2__PriceList__c IntercoPriceList;
    static Apttus_Config2__PriceList__c CarrierPriceList;
    static Apttus_Proposal__Proposal__c oQuote;
    private static CPQ_Settings__c csMapping;
    private static final Integer CPQ_QUOTES_TOTAL_NUMBER = 200;
    private static final String CPQ_PROPOSAL_PROFILES_SETTING_VALUE = 'EBU - Rogers Account Executive|Read|EBU - Rogers Specialist|Read';
    private static final String CPQ_ACCOUNT_EXECUTIVE = 'EBU - Rogers Account Executive';
    private static final String CPQ_SYSTEM_ADMIN = 'System Administrator';
    private static final String CPQ_ADMIN_USERNAME = 'standarduser_test123@testorg.com';

    /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
     static testMethod void createTestData() {
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');      
        system.runAs(TestAEUser){
            //Create PriceList
            oPriceList = CPQ_Test_Setup.newPriceList();
            oPriceList.name = 'Test Carrier Price List';
            insert oPriceList ;
            
            oEBUPriceList = CPQ_Test_Setup.newPriceList();
            oEBUPriceList.name = 'Test EBU price List';
            insert oEBUPriceList ;
            
            IDVPriceList = CPQ_Test_Setup.newPriceList();
            IDVPriceList.name = 'IDV';
            insert IDVPriceList;
            
            EnterprisePriceList = CPQ_Test_Setup.newPriceList();
            EnterprisePriceList.name = 'Enterprise';
            insert EnterprisePriceList;
            
            IntercoPriceList = CPQ_Test_Setup.newPriceList();
            IntercoPriceList.name = 'Interco';
            insert IntercoPriceList;
            
            CarrierPriceList = CPQ_Test_Setup.newPriceList();
            CarrierPriceList.name = 'Wholesale';
            insert CarrierPriceList;
            
             //Create Product 
            oProduct = CPQ_Test_Setup.newProduct();
            oProduct.CSOW_required__c = true;
            insert oProduct;
            
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            Id CarrierRecordId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Carrier Account').getRecordTypeId();
            ocarrierAccount = CPQ_Test_Setup.newAccount('Test Account');
            ocarrierAccount.recordTypeId = CarrierRecordId;
            oCarrierAccount.Price_List__c = CarrierPriceList.id;
            Id NewAccountRecordId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('New Account').getRecordTypeId();
            insert ocarrierAccount;
            oAccount.recordTypeId = NewAccountRecordId;
            insert oAccount;
            
            oIntercoAccount = CPQ_Test_Setup.newAccount('Test Account');
            oIntercoAccount.Interco_Account__c = TRUE;
            insert oIntercoAccount;
            
            
            oIDVaccount = CPQ_Test_Setup.newAccount('Test Account');
            oIDVaccount.recordTypeId = NewAccountRecordId;
            insert oIDVaccount;             
            //Inserting Custom setting CPQ_Settings__c
            csMapping  = CPQ_Settings__c.getOrgDefaults();
            csMapping.AccountPriceListMapping__c= 'Carrier_Account:Test Carrier Price List;Dealer_Account:Test EBU price List;New_Account:Test EBU price List;Submitted_Account:Test EBU price List';
            csMapping.ProposalsProfiles__c = CPQ_PROPOSAL_PROFILES_SETTING_VALUE;
            csMapping.ProposalsProfiles2__c = '';
            csMapping.DefaultOpportunityRecordType__c = 'Rogers EBU - New';
            csMapping.OpportunityQuotePriceListMapping__c = 'IDV:IDV;PED:PED;Standard:Enterprise;Carrier:Wholesale;Interco:Interco';
            csMapping.IntercoPriceListName__c= 'Interco';
            upsert csMapping;
             
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            
            oCarrierOpp = CPQ_Test_Setup.newOpportunity (ocarrierAccount);
            insert oCarrierOpp;
            IDVOpportunity = CPQ_Test_Setup.newOpportunity(oIDVaccount);
            IDVOpportunity.Price_List__c = 'IDV';
            insert IDVOpportunity;
            
            oIntercoOpportunity = CPQ_Test_Setup.newOpportunity(oIntercoAccount);
            oIntercoOpportunity.AccountId = oIntercoAccount.Id;
            insert oIntercoOpportunity;
            
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
            insert oQuote;
            
        }
    } 
     /*********************************************************************************
    Method Name    : testCSOWRequired
    Description    : Method to test if CSOW required Flag is Checked
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testCSOWRequired(){
        createTestData();
        test.startTest();
        system.runAs(TestAEUser){
            Apttus_Proposal__Proposal_Line_Item__c oProposalLine = new Apttus_Proposal__Proposal_Line_Item__c();
            oProposalLine.Apttus_Proposal__Proposal__c = oQuote.id;
            oProposalLine.Apttus_QPConfig__LineNumber__c = 1;
            oProposalLine.Apttus_QPConfig__ItemSequence__c = 1;
            oProposalLine.Apttus_Proposal__Product__c = oProduct.id;
            insert oProposalLine;
             oProposalLine.Apttus_QPConfig__LineNumber__c = 2;
             update oProposalLine;
            
        }
        Apttus_Proposal__Proposal__c oCSOWQuote = [select id, CSOW_Required__c from Apttus_Proposal__Proposal__c where id  = : oQuote.id];
        system.assert(oCSOWQuote != null);
        system.assert(oCSOWQuote.CSOW_Required__c  == true);
        
        test.stopTest();
    }   

}