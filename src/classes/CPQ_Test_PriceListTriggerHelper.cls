@isTest (seeAllData=False) 
public with sharing class CPQ_Test_PriceListTriggerHelper {
    static Apttus_Config2__PriceList__c oPriceList;
    static Apttus_Config2__PriceList__c oPriceListTemp;
    private static CPQ_Settings__c CS;
    private static final String CPQ_PRICELIST_NAME = 'Enterprise';
    private static final String CPQ_SYSTEM_ADMIN = 'System Administrator';
    private static final String CPQ_ADMIN_USERNAME = 'standarduser_test123@testorg.com';
    /*********************************************************************************
    Method Name    : createTestData
    Description    : Method used to create data for test class
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    private static void createTestData() {
        oPriceListTemp = CPQ_Test_Setup.newPriceList(CPQ_PRICELIST_NAME);
        insert oPriceListTemp;
        oPriceList = CPQ_Test_Setup.newPriceList(CPQ_PRICELIST_NAME+1);
        insert oPriceList;
        CPQ_Test_Setup.createSystemSettingRecord();
    }
    
     /*********************************************************************************
    Method Name    : testPreventPriceListDeletion
    Description    : Method used to test PreventPriceListDeletion
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
     private static testmethod void testPreventPriceListDeletion(){
       createTestData();
       User testAdminUser = CPQ_Test_Setup.newUser(CPQ_SYSTEM_ADMIN, CPQ_ADMIN_USERNAME);
       insert testAdminUser;
       test.startTest();
       system.runAs(testAdminUser) {
           oPriceListTemp = [Select Id FROM Apttus_Config2__PriceList__c WHERE Name=:CPQ_PRICELIST_NAME];
           oPriceList = [Select Id FROM Apttus_Config2__PriceList__c WHERE Name=:CPQ_PRICELIST_NAME+'1'];
           system.assertNotEquals(oPriceListTemp, null);
           system.assertNotEquals(oPriceList, null);
           try {
               delete oPriceList;
               delete oPriceListTemp;
           } catch(Exception e) {}
           List<Apttus_Config2__PriceList__c> lstPriceListTemp = [SELECT Id FROM Apttus_Config2__PriceList__c WHERE Name=:CPQ_PRICELIST_NAME];
           List<Apttus_Config2__PriceList__c> lstPriceList = [SELECT Id FROM Apttus_Config2__PriceList__c WHERE Name=:CPQ_PRICELIST_NAME + '1'];
           system.assertEquals(lstPriceListTemp.size(), 1);
           system.assertEquals(lstPriceList.size(), 0);
       }
       test.stopTest();
  
    }
}