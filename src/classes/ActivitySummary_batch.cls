/*Class Name :ActivitySummary_batch.
 *Description : .
 *Created By : Rajiv Gangra.
 *Created Date :02/01/2014.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
global class ActivitySummary_batch implements Database.Batchable<SObject>{
    // Start:Declarations---------------------------------------------------------------------------------------
       public boolean recordsCleaned;
       Public integer SFCheckPoint = Integer.ValueOf(Static_Data_Utilities__c.getInstance('Activity Update Checking Period').Value__c);
       Public date dT1 = date.newInstance(system.Today().AddMonths(-SFCheckPoint+1).year(),system.Today().AddMonths(-SFCheckPoint+1).month(),1);
       static BatchProcess_Admin__c admin = [Select email__c from BatchProcess_Admin__c limit 1];
       global String Query;
       public id CronId;
       public integer x;
       public boolean delRec=False;
       public set<String> newLstBU= new set<String>();
    // End:Declarations----------------------------------------------------------------------------------------

    global Database.QueryLocator start(Database.BatchableContext BC){

        try{
            //get the list of channels coming under BIS users
            for(BIS_Users__c bU:BIS_Users__c.getAll().values()){
                newLstBU.add(bU.Name);  
            }
            system.debug('====================>'+query);
            return database.getQueryLocator(query);
        }catch(exception ex){
            
            AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                                TotalJobItems, CreatedBy.Email,ExtendedStatus
                                from AsyncApexJob where Id =
                                :bc.getJobId()];
            // If there were any errors Send an email to the Apex job's submitter 
            // notifying of job completion  
            String[] toAddresses = new String[] {admin.email__c}; 
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           
            mail.setToAddresses(toAddresses);
            mail.setSubject('EXCEPTION during  ActivitySummary_batch Start ' + a.Status);
            mail.setPlainTextBody ('The batch Apex job processed ' + a.TotalJobItems +
                                    ' batches.\nError :'+ex);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            return null;
        }
        
    }
    /**
        * Process all the related records 
        * @return :null.
    */
    global void execute(Database.BatchableContext BC,List<SObject> Scope) {
        if(Static_Data_Utilities__c.getInstance('Activity Summary Cleaned').Value__c=='True'){
        set<string> uniqueLstStrings= new set<string>();
        List<Activity_Log__c> lstActLog= new List<Activity_Log__c>(); 
        map<string,Activity_Summary__c> mapActivitySummaryToUpsert = new map<string,Activity_Summary__c>();
        map<string,Activity_Target__c> mapActivityTarget = new map<string,Activity_Target__c>();
        map<string,decimal> mapUniqueValueCount = new map<string,decimal>();
        
        // create map of data recived
        for(SObject sO:Scope){
            Schema.Sobjecttype inspecttype=sO.Id.getSObjectType();
           // check the Sobject Name and add to the List required
           string ObjName=String.Valueof(inspecttype);
           // IF task================
            if(ObjName =='Activity_Log__c'){
                lstActLog.add((Activity_Log__c)sO);
                Activity_Log__c t=(Activity_Log__c)sO;
                uniqueLstStrings.add(t.OwnerMonthType__c);
            }

        }
//================================Section to Get the List Of existing Account Summary Records======================================        
        // On the Basis Of all the Unique Ids recived, get the List of Already Existing Record of Account Summary.
            for(Activity_Summary__c aSumm:[SELECT Activity_Month__c,Activity_Target__c,Activity_Type__c,
                            CreatedById,CreatedDate,Id,IsDeleted,LastModifiedById,LastModifiedDate,Name,
                            OwnerId,OwnerMonthType__c,RPC_Call__c,Start_of_Activity_Month__c,SystemModstamp,
                            Total_Number_of_Activity__c,User_Channel__c 
                            FROM Activity_Summary__c where OwnerMonthType__c IN:uniqueLstStrings]){
                           string uniqueValue= aSumm.OwnerMonthType__c.toUpperCase();
                mapActivitySummaryToUpsert.put(uniqueValue,aSumm);
            }
//================================Section to Get the List Of Activity Targer Related======================================        
        
        List<Activity_Target__c> lstActTarget=[SELECT Activity_Type__c,CreatedById,CreatedDate,Id,IsDeleted,LastModifiedById,
                                            LastModifiedDate,Name,OwnerId,OwnerMonthType__c,SystemModstamp,Target_Month__c,Target__c 
                                            FROM Activity_Target__c where OwnerMonthType__c IN:uniqueLstStrings];
        if(lstActTarget !=null && lstActTarget.size()!=0){
            for(Activity_Target__c aTarget: lstActTarget){
                mapActivityTarget.put(aTarget.OwnerMonthType__c.toUpperCase(),aTarget);
            }
        }
//================================Section to map Values Based on Information recived======================================        
        // Check for Activity log List
        if(lstActLog.size()!=0){
            for(Activity_Log__c  actLog:lstActLog){
                if(mapActivitySummaryToUpsert.ContainsKey(actLog.OwnerMonthType__c.toUpperCase())){
                // If the related item found then update the same.    
                    Activity_Summary__c newAccSummary= mapActivitySummaryToUpsert.get(actLog.OwnerMonthType__c.toUpperCase());
                    if(mapActivityTarget.get(newAccSummary.OwnerMonthType__c) !=null){
                        newAccSummary.Activity_Target__c= mapActivityTarget.get(newAccSummary.OwnerMonthType__c).Target__c;
                    }
                    if(newAccSummary.Total_Number_of_Activity__c !=null){
                        newAccSummary.Total_Number_of_Activity__c= newAccSummary.Total_Number_of_Activity__c+1;
                    }else{
                        newAccSummary.Total_Number_of_Activity__c= 1;
                    }
                    //Count RCP call
                    if(actLog.RPC_Call__c){
                        newAccSummary.RPC_Call__c=newAccSummary.RPC_Call__c+1;
                    }
                    mapActivitySummaryToUpsert.put(newAccSummary.OwnerMonthType__c,newAccSummary);
                }else{
                // Create a new one and add to the list
                    Activity_Summary__c newAccSummary= new Activity_Summary__c();
                    if(actLog.Activity_Date__c !=null){
                        newAccSummary.Activity_Month__c=Decimal.ValueOf(String.valueof(actLog.Activity_Date__c).SubString(0,4)+String.valueof(actLog.Activity_Date__c).SubString(5,7));
                        newAccSummary.Start_of_Activity_Month__c=Date.Valueof(string.valueof(actLog.Activity_Date__c).SubString(0,4)+'-'+string.valueof(actLog.Activity_Date__c).SubString(5,7)+'-01 00:00:00');
        
                    }
                    newAccSummary.Activity_Type__c=actLog.Activity_Type__c;
                    newAccSummary.OwnerMonthType__c=actLog.OwnerMonthType__c.toUpperCase();
                    newAccSummary.Ownerid=actLog.Sales_Rep__c;
                    if(mapActivityTarget.get(newAccSummary.OwnerMonthType__c) !=null){
                        newAccSummary.Activity_Target__c= mapActivityTarget.get(newAccSummary.OwnerMonthType__c).Target__c;
                    }
                    if(newAccSummary.Total_Number_of_Activity__c !=null){
                        newAccSummary.Total_Number_of_Activity__c= newAccSummary.Total_Number_of_Activity__c+1;
                    }else{
                        newAccSummary.Total_Number_of_Activity__c= 1;
                    }
                    if(actLog.RPC_Call__c){
                        newAccSummary.RPC_Call__c=1;
                    }else{
                        newAccSummary.RPC_Call__c=0;
                    }
                    mapActivitySummaryToUpsert.put(newAccSummary.OwnerMonthType__c,newAccSummary);
                    
                }
            }
        } 
        
   
//========================== Section to Upsert the map of Activity summary======================================================     
        list<Activity_Summary__c> lstAccSummaryUpsert= new list<Activity_Summary__c>();
        if(mapActivitySummaryToUpsert !=null && mapActivitySummaryToUpsert.size()!=0){
            for(string keyAccSumm:mapActivitySummaryToUpsert.keyset()){
                lstAccSummaryUpsert.add(mapActivitySummaryToUpsert.get(keyAccSumm));
            }
            upsert  lstAccSummaryUpsert; 
        }
        }else{
        // delete the list of summary records
            delete Scope;
        }
    }
    /**
        * Finish the Process do required Updates
        * @return : null.
    */
    global void finish(Database.BatchableContext BC){
      
             // check if this Value is true, this means batch is just done with cleaning records
             if(Static_Data_Utilities__c.getInstance('Activity Summary Cleaned').Value__c=='False'){
                //Abort the Schedule Class.
                System.abortjob(CronId);
                Static_Data_Utilities__c currentCustomSetting=Static_Data_Utilities__c.getInstance('Activity Summary Cleaned');
                currentCustomSetting.Value__c='true';
                upsert currentCustomSetting;
                ActivitySummarySchedular asSchedular= new ActivitySummarySchedular();
                Datetime dt = DateTime.now();
                Integer AttendanceCheck = Integer.valueOf( dt.hour());
                dt = dt.addseconds(2); 
                String sch = String.valueOf( dt.second() ) + ' ' + String.valueOf( dt.minute()   ) + ' ' + String.valueOf( dt.hour() ) + ' '  + String.valueOf( dt.day() ) +  ' ' + String.valueOf( dt.month() ) + ' '  + '? ' + String.valueOf( dt.Year());
                //***** Run scheduler for next job. *****/
                system.schedule( 'Activity Summary Update :: StartTime ::  '+ String.valueof(dt),sch,asSchedular);

             }else{
                  // reset Values for next batch run
                  Static_Data_Utilities__c currentCustomSetting=Static_Data_Utilities__c.getInstance('Activity Summary Cleaned');
                  currentCustomSetting.Value__c='False';
                  upsert currentCustomSetting;
                  AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,  
                  TotalJobItems, CreatedBy.Email, ExtendedStatus  
                  from AsyncApexJob where Id = :BC.getJobId()];  
                 
                 // Email the Batch Job's submitter that the Job is finished.
                 String[] toAddresses = new String[] {admin.email__c}; 
                 Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
                 if(a.NumberOfErrors == 0){ 
                 mail.setToAddresses(toAddresses);  
                 mail.setSubject('ActivitySummary_batch Update Batch Job Status' + a.Status);  
                 mail.setPlainTextBody('The Sales Forecast Update Batch Job completed successfully. Total ' + a.TotalJobItems +  
                  ' batches were processed with '+ a.NumberOfErrors + ' failures.');  
                  }
                 if(a.NumberOfErrors > 0){
                 mail.setToAddresses(toAddresses);  
                 mail.setSubject('ActivitySummary_batch Update Batch Job Status' + a.Status);  
                 mail.setPlainTextBody('The ActivitySummary_batch Update Batch Job completed with errors. Total ' + a.TotalJobItems +  
                  ' batches were processed with '+ a.NumberOfErrors + ' failures. /n ExtendedStatus: ' + a.ExtendedStatus);  
                  }
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
             }
    }

}