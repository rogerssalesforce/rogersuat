/*Class Name :opportunitySnapshotController.
 *Description : Controller Class for opportunitySnapshotTemplate, Contains all the Methods used in tha Page.
 *Created By : Rajiv Gangra.
 *Created Date :11/11/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
public class OpportunitySnapshotController {

     public List<opportunity> opp { get; set; }
     public string accid {get; set;}
     public string stage {get; set;}
     public string channel {get; set;}
     public Map<id,String> mapOwnerName { get; set; }
     public date datename {get; set;}
     public date ToDate{get; set;}
     public String dateString{get; set;}
     public date datenameFinal {get; set;}
    /**
    *Constructor
    */
    public OpportunitySnapshotController(ApexPages.StandardController controller) {
        mapOwnerName=new Map<id,String>();
        List<User> lstUserNames=[select id, Name from User];
        for(User u:lstUserNames){
            mapOwnerName.put(u.id,u.Name);
        }
        accid = ApexPages.currentPage().getParameters().get('id') ;
        datename=System.today().AddMonths(-3);
        dateString= datename.format();
        ToDate= date.parse(dateString);
        this.opp = getOpportunity();
    }
    public List<SelectOption> getStageLst() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('None','--ALL--'));
            list<OpportunityStage> oStageList= [select id,ForecastCategoryName,MasterLabel,IsActive  from OpportunityStage where IsActive =:true];
            for(OpportunityStage oS:oStageList){
                options.add(new SelectOption(oS.MasterLabel,oS.MasterLabel));
            }
            return options;
    }
    
    public List<SelectOption> getChannelLst() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('None','--ALL--'));
            Schema.sObjectType objType = User.getSObjectType();
            Schema.DescribeSObjectResult objDescribe = objType.getDescribe(); 
            map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
            list<Schema.PicklistEntry> values = fieldMap.get('channel__c').getDescribe().getPickListValues();
            for (Schema.PicklistEntry a : values) { //for all values in the picklist list
              options.add(new SelectOption(a.getValue(),a.getValue()));//add the value  to our final list
            }
            return options;
    }
   
    
    public List<Schema.FieldSetMember> getFields() {
        return SObjectType.Opportunity.FieldSets.Opportunity_Field_Set_For_Account.getFields();
    }

    private List<Opportunity> getOpportunity() {
        if(dateString!=null && dateString!=''){
        ToDate= date.parse(dateString);
        }
        
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getFields()) {
            query += f.getFieldPath() + ', ';
        }
        query += 'id FROM Opportunity where AccountID= \'' + String.escapeSingleQuotes(accid) + '\'';
        
        if(Stage !=null && Stage !='None'){
            query +=' AND StageName= \'' +Stage+'\'';   
        }
        if(channel !=null && channel !='None'){
            query +=' AND Channel__c= \'' +channel+'\'';   
        }
        if(dateString!=null && dateString!='' )
        {   
            query +=' AND CloseDate >=: ToDate' ;  
        }
        if(datenameFinal !=null )
        {   
            query +=' AND CloseDate <=: datenameFinal' ;  
        }
        query +=' ORDER BY CloseDate DESC';
        system.debug('query--------------------------->'+query); 
        system.debug('ToDate--------------------------->'+ToDate); 
        system.debug('dateString--------------------------->'+dateString); 
        system.debug('datenameFinal --------------------------->'+datenameFinal ); 
        return Database.query(query);
    }
    public void searchOpp(){
        this.opp = getOpportunity();
    }

}