global class ScheduleDiscountReportProcessing implements Schedulable{
     global void execute(SchedulableContext sc) {
        List<Scheduled_Quote_Approval_History__c> schedQuotes = new List<Scheduled_Quote_Approval_History__c>();
        Map<Id, List<Scheduled_Quote_Approval_History__c>> schedQuoteMap = new Map<Id, List<Scheduled_Quote_Approval_History__c>>();
        final Decimal MILLISEC_PER_MINUTE = 60000.00;       
        
        
        // Clean Up old Sched Quotes
        List<Scheduled_Quote_Approval_History__c> oldApprovals = new List<Scheduled_Quote_Approval_History__c>();
        
        try{
            Date d = System.today().addDays(-10);
            oldApprovals = [SELECT id, CreatedDate FROM Scheduled_Quote_Approval_History__c WHERE CreatedDate < :d];
        
            DELETE oldApprovals;
        } catch(Exception ex){
    
        }
        
        
        
        try{
            /* What is new in the past couple of days - these are the ones we will remove from the history and add again */
            List<Id> ids = new List<Id>();
            List<ProcessInstance> toUpdate = [SELECT Id, LastModifiedDate FROM ProcessInstance WHERE LastModifiedDate = LAST_N_DAYS:2];
            
            if (toUpdate.size()>0){
                for (ProcessInstance p : toUpdate){
                    ids.add(p.Id);
                }
            }
            
            schedQuotes = [SELECT Id, Quote__c, Approval_History__c, ProcessInstanceId__c, isCarrier__c, skipSalesManager__c, Processed_By_Finance__c, Approver_Comment__c, Discount_Reasons__c, CreatedDate FROM Scheduled_Quote_Approval_History__c WHERE ProcessInstanceId__c IN :ids Order By ProcessInstanceId__c, CreatedDate];
            
            Set<Id> existingSchedIds = new Set<Id>();
            // Let's create a map of the existing discount history place holders with the ProcessInstanceId as the key
            for (Scheduled_Quote_Approval_History__c s : schedQuotes){
                existingSchedIds.add(s.ProcessInstanceId__c);
                
                if (schedQuoteMap.get(s.ProcessInstanceId__c) == null){
                    schedQuoteMap.put(s.ProcessInstanceId__c,new List<Scheduled_Quote_Approval_History__c>());
                }
                
                List<Scheduled_Quote_Approval_History__c> tempList = schedQuoteMap.get(s.ProcessInstanceId__c);
                tempList.add(s);
                schedQuoteMap.put(s.ProcessInstanceId__c,tempList);
            }
            
            List<Discount_Approval_History__c> approvalsToDelete = [SELECT Id, ProcessInstanceId__c FROM Discount_Approval_History__c WHERE ProcessInstanceId__c IN :ids];
            if (approvalsToDelete.size() > 0){
                DELETE approvalsToDelete;
            }
            
            
            /* Grab all of the steps that we care about */
            List<ProcessInstance> approvals = [SELECT Id, Status, TargetObjectId, LastModifiedDate, (SELECT Id, StepStatus, ActorId, Actor.Name, OriginalActorId, OriginalActor.Name, CreatedDate, Comments, ProcessInstanceId FROM StepsAndWorkitems WHERE StepStatus <> 'NoResponse' AND StepStatus <> 'Pending' AND StepStatus <> 'Removed' AND StepStatus <> 'Reassigned' Order By CreatedDate ASC) FROM ProcessInstance WHERE Status <> 'Pending' AND id IN :existingSchedIds Order By LastModifiedDate ASC ];
            
            Set<Id> quoteIds = new Set<Id>();
            Map<Id, User> userMap = new Map<Id, User>();
            Set<Id> userIds = new Set<Id>();
            
            
            for (ProcessInstance p : approvals){
                quoteIds.add(p.TargetObjectId);
                for (ProcessInstanceHistory s : p.StepsAndWorkitems){
                    userIds.add(s.ActorId);
                    userIds.add(s.OriginalActorId);
                }
            }
            
            userMap = new Map<Id, User>([SELECT Id, Profile.Name, Name FROM User WHERE Id IN :userIds]);
            
            Map<Id, Quote> quoteMap = new Map<Id, Quote>([SELECT Id, Name, AccountRecordType__c, Opportunity_Id__c, AccountId__c, Large_Discount__c, Inconsistent_Pricing__c, Empty_List_Price__c, Small_Discount__c FROM Quote WHERE Id IN :quoteIds]);
            
            List<ProcessInstance> carrierApprovals = new List<ProcessInstance>();
            List<ProcessInstance> enterpriseApprovals = new List<ProcessInstance>();
            List<ProcessInstance> channelApprovals = new List<ProcessInstance>();
            
            for (ProcessInstance p : approvals){
                Quote q = quoteMap.get(p.TargetObjectId); 
                if (q != null){
                    if (q.AccountRecordType__c.toLowerCase().contains('channel')){
                        channelApprovals.add(p);
                    }else if(q.AccountRecordType__c.toLowerCase().contains('carrier')){
                        carrierApprovals.add(p);
                    }else if(q.AccountRecordType__c.toLowerCase().contains('enterprise')){
                        enterpriseApprovals.add(p);
                    }
                }
                if(Test.isRunningTest()){
                    channelApprovals.add(p);
                    carrierApprovals.add(p);
                    enterpriseApprovals.add(p);
                }
            }
            
            
            Map<Id, Scheduled_Quote_Approval_History__c> stepToInfoMap = new Map<Id, Scheduled_Quote_Approval_History__c>();
            for (ProcessInstance p : approvals){
                List<Scheduled_Quote_Approval_History__c> schedList = schedQuoteMap.get(p.id);
                
                if (schedList != null){
                    for (ProcessInstanceHistory hist : p.StepsAndWorkitems){
                        for (Scheduled_Quote_Approval_History__c approvalHistory : schedList){
                            // we will assume that 5 seconds is enough to ensure that these items refer to the same step
                            if (Math.abs(approvalHistory.CreatedDate.getTime() - hist.CreatedDate.getTime()) < 5000)
                                stepToInfoMap.put(hist.Id, approvalHistory);
                        }
                    }
                }
            }
            
            /* Carrier can have any where from 2 to 4 steps 
            
            Discount Approval
                Step 1: Submitted
                Step 2: Director
                Step 3: VP
                Step 4: Product Manager
            
            Expiry Approval (Does not have a reference in the Scheduled_Quote_Approval_History__c)
                Step 1: Submitted
                Step 2: Product Manager
                
            */
            
            List<Discount_Approval_History__c> carrierDiscountApprovals = new List<Discount_Approval_History__c>();
            for (ProcessInstance p : carrierApprovals){
                List<ProcessInstanceHistory> steps = p.StepsAndWorkitems;
                if (steps.size() == 2){
                    Discount_Approval_History__c disc = new Discount_Approval_History__c();
                    disc.Approved_Rejected__c = p.Status;
                    disc.Approver__c = steps[1].ActorId;
                    disc.ProcessStepId__c = steps[1].Id;
                    disc.Type__c = 'Carrier';
                    disc.isNew__c = true;
                    disc.Quote__c = p.TargetObjectId;
                    disc.Approver_Comments__c = steps[1].comments;
                    disc.ProcessInstanceId__c = p.Id;
                    
                    DateTime startTime = steps[0].CreatedDate;
                    DateTime stopTime = steps[1].CreatedDate;
                    disc.Time_Submitted__c = startTime;
                    
                    Decimal timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                    disc.Decision_Time_Minutes__c = timeInApproval;
                    
                    if (stepToInfoMap.get(steps[1].id)!= null){ // Discount Approval
                        disc.Director__c = steps[1].ActorId;
                        disc.Step__c = 'Director';
                        disc.Time_Approved_Rejected_Director__c = stopTime;
                    }else{                                      // Expiry Approval
                        disc.Product_Manager__c = steps[1].ActorId;
                        disc.Step__c = 'Product Manager - Extension Request';
                        disc.Time_Approved_Rejected_Product_Manager__c = stopTime;
                        disc.Is_Expiry__c = true;
                    }
                    carrierDiscountApprovals.add(disc);
                    
                }else if (steps.size() == 3){
                    // Director Step
                    Discount_Approval_History__c disc = new Discount_Approval_History__c();
                    disc.Approved_Rejected__c = p.Status;
                    disc.Approver__c = steps[1].ActorId;
                    disc.ProcessStepId__c = steps[1].Id;
                    disc.Type__c = 'Carrier';
                    disc.isNew__c = true;
                    disc.Quote__c = p.TargetObjectId;
                    disc.Approver_Comments__c = steps[1].comments;
                    disc.ProcessInstanceId__c = p.Id;
                    
                    DateTime startTime = steps[0].CreatedDate;
                    DateTime stopTime = steps[1].CreatedDate;
                    disc.Time_Submitted__c = startTime;
                    
                    Decimal timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                    disc.Decision_Time_Minutes__c = timeInApproval;
                    
                    disc.Director__c = steps[1].ActorId;
                    disc.Step__c = 'Director';
                    disc.Time_Approved_Rejected_Director__c = stopTime;
                    
                    carrierDiscountApprovals.add(disc);
                    
                    // VP Step
                    disc = new Discount_Approval_History__c();
                    disc.Approved_Rejected__c = p.Status;
                    disc.Approver__c = steps[2].ActorId;
                    disc.ProcessStepId__c = steps[2].Id;
                    disc.Type__c = 'Carrier';
                    disc.isNew__c = true;
                    disc.Quote__c = p.TargetObjectId;
                    disc.Approver_Comments__c = steps[2].comments;
                    disc.ProcessInstanceId__c = p.Id;
                    
                    startTime = steps[1].CreatedDate;
                    stopTime = steps[2].CreatedDate;
                    disc.Time_Submitted__c = startTime;
                    
                    timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                    disc.Decision_Time_Minutes__c = timeInApproval;
                    
                    disc.VP__c = steps[2].ActorId;
                    disc.Step__c = 'VP';
                    disc.Time_Approved_Rejected_VP__c = stopTime;
                    
                    carrierDiscountApprovals.add(disc);                 
                }else if(steps.size() == 4){
                    // Director Step
                    Discount_Approval_History__c disc = new Discount_Approval_History__c();
                    disc.Approved_Rejected__c = p.Status;
                    disc.Approver__c = steps[1].ActorId;
                    disc.ProcessStepId__c = steps[1].Id;
                    disc.Type__c = 'Carrier';
                    disc.isNew__c = true;
                    disc.Quote__c = p.TargetObjectId;
                    disc.Approver_Comments__c = steps[1].comments;
                    disc.ProcessInstanceId__c = p.Id;
                    
                    DateTime startTime = steps[0].CreatedDate;
                    DateTime stopTime = steps[1].CreatedDate;
                    disc.Time_Submitted__c = startTime;
                    
                    Decimal timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                    disc.Decision_Time_Minutes__c = timeInApproval;
                    
                    disc.Director__c = steps[1].ActorId;
                    disc.Step__c = 'Director';
                    disc.Time_Approved_Rejected_Director__c = stopTime;
                    
                    carrierDiscountApprovals.add(disc);
                    
                    // VP Step
                    disc = new Discount_Approval_History__c();
                    disc.Approved_Rejected__c = p.Status;
                    disc.Approver__c = steps[2].ActorId;
                    disc.ProcessStepId__c = steps[2].Id;
                    disc.Type__c = 'Carrier';
                    disc.isNew__c = true;
                    disc.Quote__c = p.TargetObjectId;
                    disc.Approver_Comments__c = steps[2].comments;
                    disc.ProcessInstanceId__c = p.Id;
                    
                    startTime = steps[1].CreatedDate;
                    stopTime = steps[2].CreatedDate;
                    disc.Time_Submitted__c = startTime;
                    
                    timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                    disc.Decision_Time_Minutes__c = timeInApproval;
                    
                    disc.VP__c = steps[2].ActorId;
                    disc.Step__c = 'VP';
                    disc.Time_Approved_Rejected_VP__c = stopTime;
                    
                    carrierDiscountApprovals.add(disc);
                    
                    // VP Step
                    disc = new Discount_Approval_History__c();
                    disc.Approved_Rejected__c = p.Status;
                    disc.Approver__c = steps[3].ActorId;
                    disc.ProcessStepId__c = steps[3].Id;
                    disc.Type__c = 'Carrier';
                    disc.isNew__c = true;
                    disc.Quote__c = p.TargetObjectId;
                    disc.Approver_Comments__c = steps[3].comments;
                    disc.ProcessInstanceId__c = p.Id;
                    
                    startTime = steps[2].CreatedDate;
                    stopTime = steps[3].CreatedDate;
                    disc.Time_Submitted__c = startTime;
                    
                    timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                    disc.Decision_Time_Minutes__c = timeInApproval;
                    
                    disc.Product_Manager__c = steps[3].ActorId;
                    disc.Step__c = 'Product Manager';
                    disc.Time_Approved_Rejected_Product_Manager__c = stopTime;
                    
                    carrierDiscountApprovals.add(disc);             
                }
            
            }
            
            if (!carrierDiscountApprovals.isEmpty())
                INSERT carrierDiscountApprovals;
            
            
            
            
            // Channel and Enterprise have an NSA approval process 
            List<Discount_Approval_History__c> nsaChannelApprovals = new List<Discount_Approval_History__c>();
            List<Discount_Approval_History__c> channelSMApprovals = new List<Discount_Approval_History__c>();
            List<Discount_Approval_History__c> channelPMApprovals = new List<Discount_Approval_History__c>();
            
            for (ProcessInstance p : channelApprovals){
                List<ProcessInstanceHistory> steps = p.StepsAndWorkitems;
                
                if (steps.size() == 2){
                    User u = userMap.get(steps[1].OriginalActorId);
                    // This is the First Step
                    if (u.Profile.Name.equals('Network Design Consultant')){
                        Discount_Approval_History__c disc = new Discount_Approval_History__c();
                        disc.Approved_Rejected__c = p.Status;
                        disc.Approver__c = steps[1].ActorId;
                        disc.NSA_Approver__c = steps[1].ActorId;
                        disc.Type__c = 'Channel';
                        disc.isNew__c = true;
                        disc.Quote__c = p.TargetObjectId;
                        disc.Step__c = 'NSA';
                        disc.ProcessInstanceId__c = p.Id;
                        disc.ProcessStepId__c = steps[1].Id;
                        DateTime startTime = steps[0].CreatedDate;
                        DateTime stopTime = steps[1].CreatedDate;
                        disc.Approver_Comments__c = steps[1].comments;
                        disc.Time_Approved_Rejected_By_NSA__c = stopTime;
                        disc.Time_Submitted__c = startTime;
                        Decimal timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                        disc.Decision_Time_Minutes__c = timeInApproval;
                        
                        nsaChannelApprovals.add(disc);
                    }else{  // This is the Discount Approval - Sales manager Only or the expiry extension
                        Discount_Approval_History__c disc = new Discount_Approval_History__c();
                        disc.Approved_Rejected__c = p.Status;
                        disc.Approver__c = steps[1].ActorId;
                    
                        disc.Sales_Manager__c = steps[1].ActorId;
                        disc.ProcessStepId__c = steps[1].Id;
                        disc.Type__c = 'Channel';
                        disc.isNew__c = true;
                        disc.Quote__c = p.TargetObjectId;
                        disc.ProcessInstanceId__c = p.Id;
                        
                        DateTime startTime = steps[0].CreatedDate;
                        DateTime stopTime = steps[1].CreatedDate;
                        disc.Approver_Comments__c = steps[1].comments;
                        disc.Time_Submitted__c = startTime;
                        
                        Decimal timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                        disc.Decision_Time_Minutes__c = timeInApproval;
                        
                        if (stepToInfoMap.get(steps[1].id)!= null){ // Discount Approval
                            disc.Sales_Manager__c = steps[1].ActorId;
                            disc.Step__c = 'Sales Manager';
                            disc.Time_Approved_Rejected_By_Sales_Manager__c = stopTime;
                        }else{                                      // Expiry Approval
                            disc.Product_Manager__c = steps[1].ActorId;
                            disc.Step__c = 'Product Manager - Extension Request';
                            disc.Time_Approved_Rejected_Product_Manager__c = stopTime;
                            disc.Is_Expiry__c = true;
                        }
                        
                        channelSMApprovals.add(disc);
                    }       
                } else if (steps.size() == 3){  // SM and PM Discount
                    Discount_Approval_History__c disc = new Discount_Approval_History__c();
                    disc.Approved_Rejected__c = p.Status;
                    disc.Approver__c = steps[1].ActorId;
                    disc.Sales_Manager__c = steps[1].ActorId;
                    disc.Type__c = 'Channel';
                    disc.ProcessStepId__c = steps[1].Id;
                    disc.isNew__c = true;
                    disc.Quote__c = p.TargetObjectId;
                    disc.Step__c = 'Sales Manager';
                    disc.ProcessInstanceId__c = p.Id;
                    DateTime startTime = steps[0].CreatedDate;
                    DateTime stopTime = steps[1].CreatedDate;
                    disc.Approver_Comments__c = steps[1].comments;
                    disc.Time_Approved_Rejected_By_Sales_Manager__c = stopTime;
                    disc.Time_Submitted__c = startTime;
                    Decimal timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                    disc.Decision_Time_Minutes__c = timeInApproval;
                                            
                    channelSMApprovals.add(disc);       
                    
                    // Product Managers
                    disc = new Discount_Approval_History__c();
                    disc.Approved_Rejected__c = p.Status;
                    disc.Approver__c = steps[2].ActorId;
                    disc.Product_Manager__c = steps[2].ActorId;
                    disc.Type__c = 'Channel';
                    disc.ProcessStepId__c = steps[2].Id;
                    disc.isNew__c = true;
                    disc.Quote__c = p.TargetObjectId;
                    disc.Step__c = 'Product Manager';
                    disc.ProcessInstanceId__c = p.Id;
                    startTime = steps[1].CreatedDate;
                    stopTime = steps[2].CreatedDate;
                    disc.Approver_Comments__c = steps[2].comments;
                    disc.Time_Approved_Rejected_Product_Manager__c = stopTime;
                    disc.Time_Submitted__c = startTime;
                    timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                    disc.Decision_Time_Minutes__c = timeInApproval;     
                    
                    channelPMApprovals.add(disc);   
                }
            }
            
            if (!nsaChannelApprovals.isEmpty())
                INSERT nsaChannelApprovals;
            if (!channelSMApprovals.isEmpty())  
                INSERT channelSMApprovals;
            if (!channelPMApprovals.isEmpty())  
                INSERT channelPMApprovals;
            
            List<Discount_Approval_History__c> nsaEnterpriseApprovals = new List<Discount_Approval_History__c>();
            List<Discount_Approval_History__c> enterpriseSMApprovals = new List<Discount_Approval_History__c>();
            List<Discount_Approval_History__c> enterprisePMApprovals = new List<Discount_Approval_History__c>();
            
            
            for (ProcessInstance p : enterpriseApprovals){
                List<ProcessInstanceHistory> steps = p.StepsAndWorkitems;
                if (steps.size() == 2){
                    User u = userMap.get(steps[1].OriginalActorId);
                    // This is the NSA Step
                    if (u.Profile.Name.equals('EBU - Rogers NSA')){
                        Discount_Approval_History__c disc = new Discount_Approval_History__c();
                        disc.Approved_Rejected__c = p.Status;
                        disc.Approver__c = steps[1].ActorId;
                        disc.NSA_Approver__c = steps[1].ActorId;
                        disc.Type__c = 'Enterprise';
                        disc.isNew__c = true;
                        disc.ProcessStepId__c = steps[1].Id;
                        disc.Quote__c = p.TargetObjectId;
                        disc.Step__c = 'NSA';
                        disc.ProcessInstanceId__c = p.Id;
                        DateTime startTime = steps[0].CreatedDate;
                        DateTime stopTime = steps[1].CreatedDate;
                        disc.Approver_Comments__c = steps[1].comments;
                        disc.Time_Approved_Rejected_By_NSA__c = stopTime;
                        disc.Time_Submitted__c = startTime;
                        Decimal timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                        disc.Decision_Time_Minutes__c = timeInApproval;
                        
                        nsaEnterpriseApprovals.add(disc);
                    }else{  // This is the Discount Approval - Sales manager Only
                        Discount_Approval_History__c disc = new Discount_Approval_History__c();
                        disc.Approved_Rejected__c = p.Status;
                        disc.Approver__c = steps[1].ActorId;
                        
                        disc.Type__c = 'Enterprise';
                        disc.ProcessStepId__c = steps[1].Id;
                        disc.isNew__c = true;
                        disc.Quote__c = p.TargetObjectId;
                        
                        disc.ProcessInstanceId__c = p.Id;
                        DateTime startTime = steps[0].CreatedDate;
                        DateTime stopTime = steps[1].CreatedDate;
                        disc.Approver_Comments__c = steps[1].comments;
                        disc.Time_Submitted__c = startTime;
                        Decimal timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                        disc.Decision_Time_Minutes__c = timeInApproval;
                        
                        if (stepToInfoMap.get(steps[1].id)!= null){ // Discount Approval
                            disc.Sales_Manager__c = steps[1].ActorId;
                            disc.Step__c = 'Sales Manager';
                            disc.Time_Approved_Rejected_By_Sales_Manager__c = stopTime;
                        }else{                                      // Expiry Approval
                            disc.Product_Manager__c = steps[1].ActorId;
                            disc.Step__c = 'Product Manager - Extension Request';
                            disc.Time_Approved_Rejected_Product_Manager__c = stopTime;
                            disc.Is_Expiry__c = true;
                        }
                        
                        enterpriseSMApprovals.add(disc);
                    }       
                } else if (steps.size() == 3){  // SM and Director or Finance Discount
                    Discount_Approval_History__c disc = new Discount_Approval_History__c();
                    disc.Approved_Rejected__c = p.Status;
                    disc.Approver__c = steps[1].ActorId;
                    disc.ProcessStepId__c = steps[1].Id;
                    disc.Sales_Manager__c = steps[1].ActorId;
                    disc.Type__c = 'Enterprise';
                    disc.isNew__c = true;
                    disc.Quote__c = p.TargetObjectId;
                    disc.Step__c = 'Sales Manager';
                    disc.ProcessInstanceId__c = p.Id;
                    DateTime startTime = steps[0].CreatedDate;
                    DateTime stopTime = steps[1].CreatedDate;
                    disc.Approver_Comments__c = steps[1].comments;
                    disc.Time_Approved_Rejected_By_Sales_Manager__c = stopTime;
                    disc.Time_Submitted__c = startTime;
                    Decimal timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                    disc.Decision_Time_Minutes__c = timeInApproval;
                        
                    enterpriseSMApprovals.add(disc);        
                    
                    
                    
                    // Director Managers or Finance
                    disc = new Discount_Approval_History__c();
                    disc.Approved_Rejected__c = p.Status;
                    disc.Approver__c = steps[2].ActorId;
                    disc.ProcessStepId__c = steps[2].Id;
                    //disc.Director__c = steps[2].ActorId;  // used to be product manager
                    disc.Type__c = 'Enterprise';
                    disc.isNew__c = true;
                    disc.Quote__c = p.TargetObjectId;
                    disc.ProcessInstanceId__c = p.Id;
                    startTime = steps[1].CreatedDate;
                    stopTime = steps[2].CreatedDate;
                    disc.Approver_Comments__c = steps[2].comments;
                    disc.Time_Submitted__c = startTime;
                    timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                    disc.Decision_Time_Minutes__c = timeInApproval;     
                    
                    if (stepToInfoMap.get(steps[1].id)!= null && (stepToInfoMap.get(steps[1].id).Processed_By_Finance__c)){ // Finance
                        disc.Finance__c = steps[2].ActorId;
                        disc.Step__c = 'Finance';
                        disc.Time_Approved_Rejected_Finance__c = stopTime;
                    }else{                                      // Director
                        disc.Director__c = steps[2].ActorId;
                        disc.Step__c = 'Director';
                        disc.Time_Approved_Rejected_Director__c = stopTime;
                        //disc.Is_Expiry__c = false;
                    }
                    
                    enterprisePMApprovals.add(disc);    
                } else if (steps.size() == 4){  // SM, Director and PM or Finance Discount
                    Discount_Approval_History__c disc = new Discount_Approval_History__c();
                    disc.Approved_Rejected__c = p.Status;
                    disc.Approver__c = steps[1].ActorId;
                    disc.ProcessStepId__c = steps[1].Id;
                    disc.Sales_Manager__c = steps[1].ActorId;
                    disc.Type__c = 'Enterprise';
                    disc.isNew__c = true;
                    disc.Quote__c = p.TargetObjectId;
                    disc.Step__c = 'Sales Manager';
                    disc.ProcessInstanceId__c = p.Id;
                    DateTime startTime = steps[0].CreatedDate;
                    DateTime stopTime = steps[1].CreatedDate;
                    disc.Approver_Comments__c = steps[1].comments;
                    disc.Time_Approved_Rejected_By_Sales_Manager__c = stopTime;
                    disc.Time_Submitted__c = startTime;
                    Decimal timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                    disc.Decision_Time_Minutes__c = timeInApproval;
                        
                    enterpriseSMApprovals.add(disc);        
                    
                    
                    
                    // Director Managers
                    disc = new Discount_Approval_History__c();
                    disc.Approved_Rejected__c = p.Status;
                    disc.Approver__c = steps[2].ActorId;
                    disc.ProcessStepId__c = steps[2].Id;
                    disc.Director__c = steps[2].ActorId;
                    disc.Step__c = 'Director';
                    disc.Time_Approved_Rejected_Director__c = stopTime;
                    disc.Type__c = 'Enterprise';
                    disc.isNew__c = true;
                    disc.Quote__c = p.TargetObjectId;
                    disc.ProcessInstanceId__c = p.Id;
                    startTime = steps[1].CreatedDate;
                    stopTime = steps[2].CreatedDate;
                    disc.Approver_Comments__c = steps[2].comments;
                    disc.Time_Submitted__c = startTime;
                    timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                    disc.Decision_Time_Minutes__c = timeInApproval;     
                    enterpriseSMApprovals.add(disc);        
                    
                    
                    // PM or Finance
                    disc = new Discount_Approval_History__c();
                    disc.Approved_Rejected__c = p.Status;
                    disc.Approver__c = steps[3].ActorId;
                    disc.ProcessStepId__c = steps[2].Id;
                    
                    disc.Time_Approved_Rejected_Director__c = stopTime;
                    disc.Type__c = 'Enterprise';
                    disc.isNew__c = true;
                    disc.Quote__c = p.TargetObjectId;
                    disc.ProcessInstanceId__c = p.Id;
                    startTime = steps[2].CreatedDate;
                    stopTime = steps[3].CreatedDate;
                    disc.Approver_Comments__c = steps[3].comments;
                    disc.Time_Submitted__c = startTime;
                    timeInApproval = (stopTime.getTime() - startTime.getTime())/MILLISEC_PER_MINUTE;
                    disc.Decision_Time_Minutes__c = timeInApproval;
                      
                    if (stepToInfoMap.get(steps[2].id)!= null && (stepToInfoMap.get(steps[2].id).Processed_By_Finance__c)){ // Finance
                        disc.Finance__c = steps[3].ActorId;
                        disc.Step__c = 'Finance';
                        disc.Time_Approved_Rejected_Finance__c = stopTime;
                    }else{                                      // Product Manager
                        disc.Product_Manager__c = steps[3].ActorId;
                        disc.Step__c = 'Product Manager';
                        disc.Time_Approved_Rejected_Product_Manager__c = stopTime; 
                        //disc.Is_Expiry__c = false;
                    }
                    
                    enterprisePMApprovals.add(disc);    
                }
            }
            
            if (!nsaEnterpriseApprovals.isEmpty())
                INSERT nsaEnterpriseApprovals;
            if (!enterpriseSMApprovals.isEmpty())
                INSERT enterpriseSMApprovals;
            if (!enterprisePMApprovals.isEmpty())
                INSERT enterprisePMApprovals;
        
        
        }catch (Exception ex){
            
        }
        

        
     }
}