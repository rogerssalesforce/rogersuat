/*********************************************************************************
Class Name      : CPQ_Test_redirectConfigureFlowCont 
Description     : This class a test class for CPQ_redirectConfigureFlowCont. 
Created By      : Deepika Rawat
Created Date    : 1-Nov-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Deepika Rawat              1-Nov-15              Original version
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest (seeAllData=False) 
private class CPQ_Test_redirectConfigureFlowCont{
    static User TestAEUser;
    static User TestDDUser;
    static Account oAccount;
    static product2 oProduct;
    static Opportunity oOpportunity;
    static Apttus_Proposal__Proposal__c oQuote;
    static Attachment attachmentQuote;
    static Apttus_Config2__PriceList__c oPriceList;
    static Apttus_Config2__PriceListItem__c oPriceListItem;
    static ID prodConfigID;
    static Pricebook2 oPricebook;
    static Pricebook2 oPriceBookCustom;
    static List<Apttus_Config2__ProductConfiguration__c> lstProductConfig ;
    private static CPQ_Settings__c cs;
     /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testMethod void createTestData() {
        //Create custom Pricebook 
        oPriceBookCustom = CPQ_Test_Setup.newPriceBook('CPQ Temp PriceBook');
        insert oPriceBookCustom;
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        cs  = CPQ_Settings__c.getOrgDefaults();
        cs.DefaultOpportunityRecordType__c = 'Rogers EBU - New';
        upsert cs;
        //Create newPriceBook
        oPricebook = CPQ_Test_Setup.newPriceBook('Test Pricelist');
        insert oPricebook;
        //Create PriceList
        oPriceList = CPQ_Test_Setup.newPriceList();
        insert oPriceList ;
        // Create Product
        oProduct = CPQ_Test_Setup.newProduct();
        oProduct.Family = 'Wireless - Voice';
        insert oProduct;
        //Create Pricelist item
        oPriceListItem = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oProduct.ID);
        insert oPriceListItem;
        
    }
    /*********************************************************************************
    Method Name    : testRedirectConfigureFlowDraft
    Description    : Test redirect flow as AE with Quote stage = draft
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testRedirectConfigureFlowDraft(){
        //Create AE test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');         
        system.runAs(TestAEUser){
            createTestData();
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
            insert oQuote;
            //Product Configuration
            prodConfigID = CPQ_Test_Setup.newProdictConfiguration(oQuote);
            //Create line items
            CPQ_Test_Setup.createLineItem(prodConfigID,oProduct.id,oPriceListItem.id);
            oQuote.Apttus_Proposal__Approval_Stage__c = 'Draft';
            update oQuote;
            test.startTest();
            ApexPages.currentPage().getParameters().put('id', oQuote.id);
            CPQ_redirectConfigureFlowCont obj = new CPQ_redirectConfigureFlowCont();
            PageReference  pg = obj.openConfigureFlow();
            System.assert(pg.getParameters().get('flow')=='ngFlow');
            test.stopTest();
        }
    }
     /*********************************************************************************
    Method Name    : testRedirectConfigureFlowUnderReview
    Description    : Test redirect flow as AE with Quote stage = 'Solution under review'
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testRedirectConfigureFlowUnderReview(){
        //Create AE test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');         
        system.runAs(TestAEUser){
            createTestData();
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
            insert oQuote;
            //Product Configuration
            prodConfigID = CPQ_Test_Setup.newProdictConfiguration(oQuote);
            //Create line items
            CPQ_Test_Setup.createLineItem(prodConfigID,oProduct.id,oPriceListItem.id);
            oQuote.Apttus_Proposal__Approval_Stage__c = 'Solution Under Review';
            update oQuote;
            test.startTest();
            ApexPages.currentPage().getParameters().put('id', oQuote.id);
            CPQ_redirectConfigureFlowCont obj = new CPQ_redirectConfigureFlowCont();
            PageReference  pg = obj.openConfigureFlow();
            System.assert(pg.getParameters().get('flow')=='repriceFlow');
            test.stopTest();
        }
    }
    /*********************************************************************************
    Method Name    : testRedirectConfigureFlowInReview
    Description    : Test redirect flow as DD with Quote stage = In Review
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testRedirectConfigureFlowInReview(){
        //Create DD test user 
        createTestData();
        TestDDUser= CPQ_Test_Setup.newUser('EBU - Rogers Deal Desk');  
        system.runAs(TestDDUser){
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
            insert oQuote;
            //Product Configuration
            prodConfigID = CPQ_Test_Setup.newProdictConfiguration(oQuote);
            //Create line items
            CPQ_Test_Setup.createLineItem(prodConfigID,oProduct.id,oPriceListItem.id);
            oQuote.Apttus_Proposal__Approval_Stage__c = 'In Review';
            update oQuote;
            test.startTest();
            ApexPages.currentPage().getParameters().put('id', oQuote.id);
            CPQ_redirectConfigureFlowCont obj = new CPQ_redirectConfigureFlowCont();
            PageReference  pg = obj.openConfigureFlow();
            System.assert(pg.getParameters().get('flow')=='finalizeFlow');
            test.stopTest();
        }
    }
    /*********************************************************************************
    Method Name    : testRedirectConfigureFlowApproved
    Description    : Test redirect flow as DD with Quote stage = Approved
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testRedirectConfigureFlowApproved(){
        //Create DD test user 
        TestDDUser= CPQ_Test_Setup.newUser('EBU - Rogers Deal Desk'); 
        createTestData();
        system.runAs(TestDDUser){
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
            insert oQuote;
            //Product Configuration
            prodConfigID = CPQ_Test_Setup.newProdictConfiguration(oQuote);
            //Create line items
            CPQ_Test_Setup.createLineItem(prodConfigID,oProduct.id,oPriceListItem.id);
            oQuote.Apttus_Proposal__Approval_Stage__c = 'Approved';
            update oQuote;
            test.startTest();
            ApexPages.currentPage().getParameters().put('id', oQuote.id);
            CPQ_redirectConfigureFlowCont obj = new CPQ_redirectConfigureFlowCont();
            PageReference  pg = obj.openConfigureFlow();
            System.assert(pg.getParameters().get('flow')=='onlyCloseFlow');
            test.stopTest();
        }
    }
}