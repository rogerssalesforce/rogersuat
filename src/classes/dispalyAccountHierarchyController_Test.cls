/**************************************************************************************
Apex Class Name     : dispalyAccountHierarchyController_Test
Version             : 1.0 
Created Date        : 30 Jan 2015
Function            : This is the Test Class for DisplayAccountHierarchyController.
Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Aakanksha Patel             01/30/2015              Original Version
*************************************************************************************/
@isTest
private class dispalyAccountHierarchyController_Test
{  
    private static  List<Account> listChild;
    private static  Account AccountParent;
    private static  Account AccountChild;
     private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
     private static Static_Data_Utilities__c staticData_PageSize = new Static_Data_Utilities__c ();
     private static Static_Data_Utilities__c staticData_Limit = new Static_Data_Utilities__c ();
       
    /*
    Description : This method is used to create Test Data. 
    Parameters  : None
    Return Type : void
    */
    private static void setUpData()
    {   //Data for Custom Settings
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        TAG_CS.User_Location__c = 'Central,Eastern,National,Western';
        TAG_CS.User_Region__c = 'Alberta,Atlantic,British Columbia,National,Ontario,Prairies,Quebec';
        TAG_CS.User_Channel_1__c = 'ABS,BIS,Business Customer Enablement,Business Customer Loyalty,Business Customer Relations,Business National Support,Business Segment,Commercial Service Delivery,Dealer,Enterprise,Enterprise Service Assurance';
        TAG_CS.User_Channel_2__c = 'Enterprise,Service Delivery,Federal,Field Sales Cable,Large,M2M Reseller,Medium,,MSA,NIS,Public Sector,RBS,RCI,Sales Operations,Small,VAR';  
        insert TAG_CS;
        staticData_PageSize.Name = 'AccountTransfer_PageSize';
        staticData_PageSize.Value__c ='1';
        insert staticData_PageSize;
        staticData_Limit.Name='AccountTransfer_RecordsLimit';
        staticData_Limit.Value__c='2';
        insert staticData_Limit;  
        

        AccountParent = new Account();
        AccountParent.ParentId= null;
        AccountParent.Account_Status__c = 'Assigned';
        AccountParent.Name = 'Test_account_parent_with_child';
        AccountParent.BillingPostalCode = 'A1A 1A1';
        AccountParent.Internal_DUNS__c = String.valueOf(Integer.valueOf(Math.random()*1000000000));
        insert AccountParent; 
        
        listChild = new List<Account>{};
        
        for(Integer i = 0; i < 11; i++){
        Account a = new Account(Name = 'Test_account_child ' + i);
        a.ParentId = AccountParent.Id;
        a.Account_Status__c = 'Assigned';
        a.BillingPostalCode = 'A1A 1A1';
        a.Internal_DUNS__c = String.valueOf(Integer.valueOf(Math.random()*1000000000));
        listChild.add(a);
        }
        insert listChild;
        
        AccountChild = new Account();
        AccountChild.Name = 'Test_only_child';
        AccountChild.ParentId = listChild[0].id;
        AccountChild.BillingPostalCode = 'A1A 1A1';
        AccountChild.Internal_DUNS__c = String.valueOf(Integer.valueOf(Math.random()*1000000000));
        insert AccountChild;
                             
     }
    
    
    /*
     Description : This is is a method to test the Search account
     Parameters  : None
     Return Type : void
     */
    private static testMethod void DisplayAccount()
    {
        Test.startTest();
        setUpData();
        {
                
                ApexPages.CurrentPage().getparameters().put('accid', listChild[0].ParentId);
                TAG_dispalyAccountHierarchyController obj = new TAG_dispalyAccountHierarchyController();
                system.assertEquals(obj.parentAccount.Name,AccountParent.Name);       
        
        }
        Test.stopTest();     
    }
    
    /*
     Description : This is is a method to test Accounts without children. 
     Parameters  : None
     Return Type : void
     */
    private static testMethod void AccountWithoutChildren() {
        Test.startTest();
        setUpData();
        {       
                TAG_dispalyAccountHierarchyController obj = new TAG_dispalyAccountHierarchyController();
                system.assert(obj.lstAllAccount.size() ==0);
        }
        Test.stopTest();     
    }
   
    /*
     Description : This is is a method to test Accounts with one child. 
     Parameters  : None
     Return Type : void
     */
    private static testMethod void AccountWithChildren() {
        Test.startTest();
            setUpData();
            {
                    ApexPages.CurrentPage().getparameters().put('accid', AccountChild.ParentId);
                    TAG_dispalyAccountHierarchyController obj = new TAG_dispalyAccountHierarchyController();
                    system.assertEquals(obj.parentAccount.Name,listChild[0].Name);
                    
                    //system.assert(obj.noOfChilds=='0');
            }
        Test.stopTest();     
    }
 
}