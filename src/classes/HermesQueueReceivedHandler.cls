public without sharing class HermesQueueReceivedHandler {

    public class HermesQueueReceivedHandlerException extends Exception{}

    private static final String LEAD_RECORD_TYPE = 'New_Lead';

    private static Id leadRecordTypeId = null;

    public void handleFromOtherOrg(List<Hermes_Queue__c> queues) {
        List<Lead> leads = new List<Lead>();
        for (Hermes_Queue__c queue : queues) {
            if (queue.Class_Name__c == HermesLead.CLASS_NAME) {
                leads.add(buildLead(queue));
            }
        } 
        if (leads.size() > 0 ) { 
            insert leads;
        }                
    }

    private Lead buildLead(Hermes_Queue__c queue) {
        HermesLead payload = HermesLead.parseJson(queue.Payload__c);

        Lead lead = new Lead();
        lead.RecordTypeId = getLeadRecordType();
        //lead.Lead_Source_Id__c = queue.Source_Id__c;
        lead.LeadSource = queue.Source_Name__c;

        lead.Status = 'New'; 
        //lead.Lead_Created_Date__c = System.now();
        //lead.Lead_Created_By__c = payload.createdBy;
        //lead.Lead_Created_By_Id__c = payload.createdById;

        lead.Company = payload.company;
        lead.FirstName = payload.firstname;
        lead.LastName = payload.lastname;
        lead.Email = payload.email;     
        lead.Phone = payload.phone;
        //lead.Mobile_Phone__c = payload.mobilePhone;
        //lead.Alternate_Phone__c = payload.altPhone; 
        lead.Industry = payload.industry;
        lead.NumberOfEmployees = payload.numberOfEmployees;  

        lead.Street = payload.street; 
        lead.City = payload.city;    
        lead.State = payload.state; 
        lead.Country = payload.country;
        lead.PostalCode = payload.postalcode;

        //lead.Has_Internet__c = payload.hasInternet; 
        //lead.Internet_Provider__c = payload.internetProvider;
        //lead.Is_Rogers_Customer__c = payload.isRogersCustomer;
        //lead.Comment__c = payload.comment;
        //lead.Demo_Date_Time__c = payload.demoDateTime;
        //lead.Follow_Up_Date__c = payload.followUpDate;
        //lead.Follow_Up_Time__c = payload.followUpTime;
        //lead.Priority__c = payload.priority;

        //lead.Internet_Status__c = payload.productInternet;
        //lead.Mobile_Internet_Status__c = payload.productMobileInternet;
        //lead.TV_Status__c = payload.productTV;
        //lead.Phone_Status__c = payload.productPhone;
        //lead.Wireless_Status__c = payload.productWireless;
        //lead.Security_Status__c = payload.productSecurity;

        //lead.OwnerId = CaerusUtil.getCaerusQueue();
        //CaerusUtil.cleanFields(lead);
        return lead;       
    }

    private Id getLeadRecordType() {
        if (leadRecordTypeId != null) return leadRecordTypeId;

        RecordType[] recordTypes = [
            SELECT Id FROM RecordType
                WHERE sObjectType = 'Lead'
                AND developername = :LEAD_RECORD_TYPE
                AND IsActive=true];
        if (recordTypes.size() == 0) {
            throw new HermesQueueReceivedHandlerException('Cannot find RecordType ' + LEAD_RECORD_TYPE);
        }
        leadRecordTypeId = recordTypes[0].Id;
        return leadRecordTypeId;                        
    }
}