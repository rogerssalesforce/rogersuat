/*
===============================================================================
 Class Name   : STIPIntegrationNewOffer
===============================================================================
PURPOSE:    This is the controller class for STIPAppPage vf page.
  

Developer: Deepika Rawat
Date: 19/08/2013

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
19/08/2013         Deepika                     Created
===============================================================================
*/
public with sharing class STIPIntegrationNewOffer { 
    public User loggedInUser{get; set;}
    public String oppid{get;set;}
    public String offeridOnEdit{get;set;}   
    public String currentUserId{get;set;}
    public String offerid{get;set;} 
    public Offers__c offerRec {get;set;}  
    public Opportunity oppRec{get;set;}  
    // This is to display Newoffer section
    public boolean showNewOffer{get;set;}
    // This is to display ViewOffer section
    public boolean showViewOffer{get;set;}
    // This is to display offerDashboard section
    public boolean showDashboard{get;set;}
    // This is to display editOffer section
    public boolean showEditOffer{get;set;}
    
    public STIPIntegrationNewOffer(ApexPages.StandardController controller) {
        showNewOffer = false;
        showViewOffer= false;
        showDashboard= false;
        showEditOffer=false;
        oppid= ApexPages.CurrentPage().getParameters().get('oppid'); 
        offerid= ApexPages.CurrentPage().getParameters().get('offerid');
        offeridOnEdit = ApexPages.CurrentPage().getParameters().get('id');
        currentUserId=UserInfo.getUserId();
        loggedInUser = [select id , Employee_Id__c from User where id=:currentUserId];
        if((oppid!=null && oppid !='') && oppid.startswith('006')){   
            showNewOffer = true;
            oppRec = [select id, Name, Accountid from Opportunity where id=:oppid] ;
        } 
         
        else if(offerid!=null && offerid!='' ){       
            showViewOffer = true;           
            offerRec = [select id, Name from Offers__c where id=:offerid] ;
        } 
        
        else if(offeridOnEdit !=null && offeridOnEdit !=''){
            showEditOffer= true;
            offerRec = [select id, Name from Offers__c where id=:offeridOnEdit];        
        }
        
        else{
           showDashboard=true; 
        }                

    }
}