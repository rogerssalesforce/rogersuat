/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestContractPDF { 
    
    static Map <string,Id> mapRTa, mapRTc;
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c(); 

    static {
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRtc = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Wireline_Contract__c']);
        mapRTc = new Map <string,id> ();
        
        for (RecordType rtc : lRtc){
            mapRTc.put(rtc.Name,rtc.id);  
        }
       
    }

    private static testMethod void myUnitTest() {
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;

        Account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street'; a.Account_Status__c = 'Assigned';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        insert a;   
        
        Wireline_Contract__c c = new Wireline_Contract__c();
        c.Account_Name__c = a.Id;
        c.MRC__c = 1000;
        c.NRC__c = 500;
        c.Price_type__c = 'Standard';
        c.RecordTypeId = mapRTc.get('Enterprise Contract Layout');
        c.Renewal_Type__c = 'Auto-renew';
        c.Work_Category__c = 'New';
        c.Channel__c = 'Retail';
        c.Customer_Type__c = 'Named';
        c.Termination_Option__c = 'Customer';
        c.Renewal_Notification_Period__c = '30 days';
        c.Termination_Notification_Period__c = '30 days';
        c.Contract_Type__c = 'Standard';
        c.Contract_Start_Date__c  = Date.today();
        c.Contract_Term_months__c = 30;
        
        
        insert c;
       
       PageReference pageRef = new PageReference('/apex/contractDocument?id=' + String.valueOf(c.Id));
       Test.setCurrentPage(pageRef);
       Test.startTest();

       ApexPages.StandardController sc = new ApexPages.standardController(c);
       GenerateContractExtension controller = new GenerateContractExtension(sc);
       Test.stopTest();
        
    }
}