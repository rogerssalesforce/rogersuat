/*********************************************************************************
Class Name      : CPQ_AgreementAcceptDocController 
Description     : This class is a controller for CPQ_AgreementAcceptDoc page. 
Created By      : Deepika Rawat
Created Date    : 13-Dec-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                      Date                   Description
Deepika Rawat                13-Dec-15             Original version
----------------------------------------------------------------------------------            
*********************************************************************************/
public with sharing class CPQ_AgreementAcceptDocController {
    public List<Apttus__APTS_Agreement__c> lstAgreement {get;set;}
    /*********************************************************************************
    Method Name    : CPQ_AgreementAcceptDocController
    Description    : This is a constructor class.
    Return Type    : null
    Parameter      : 
    *********************************************************************************/
    public CPQ_AgreementAcceptDocController() {
        String sAgreementId = ApexPages.currentPage().getParameters().get('id');   
        lstAgreement = [Select Apttus__Status_Category__c, Apttus__Status__c from Apttus__APTS_Agreement__c where id=:sAgreementId]; 
    }
    /*********************************************************************************
    Method Name    : updateAgreement
    Description    : This method updates field Apttus__Status__c on Agreement.
    Return Type    : void
    Parameter      : null
    *********************************************************************************/ 
    public PageReference updateAgreement(){
        try{    //Update Agreement Status
            if(lstAgreement!=null && lstAgreement.size()>0){
                lstAgreement[0].Apttus__Status__c = 'Customer Signatures Received';
                update lstAgreement[0];
            }
            PageReference refreshPage = new PageReference('/' + lstAgreement[0].Id);
            return refreshPage ; 
        }
        catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
            return null;
        }
    }
     /*********************************************************************************
    Method Name    : backToAgreement
    Description    : This method opens up Agreement detail page
    Return Type    : PageReference
    Parameter      : null
    *********************************************************************************/ 
    public PageReference backToAgreement(){
        PageReference refreshPage = new PageReference('/' + lstAgreement[0].Id);
        return refreshPage ; 
    }
}