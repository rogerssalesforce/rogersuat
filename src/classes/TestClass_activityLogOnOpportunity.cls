/*Class Name  : TestClass_activityLogOnOpportunity.
 *Description : This is test class for trigger activityLogOnOpportunity .
 *Created By  : Deepika Rawat.
 *Created Date :28/01/2014.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
@isTest(SeeAllData = true)
private class TestClass_activityLogOnOpportunity{
    static testmethod void taskmethod(){
        Account acc= new Account();
        acc.Name = 'testAcc';
        Recursion_blocker.flag = true;
        acc.BillingPostalCode ='A1A 1A1';
        insert acc;
        Opportunity opp = new Opportunity();
        opp.Name='Testopp';
        opp.StageName='Prospecting';
        opp.CloseDate=system.today();
        opp.Accountid = acc.id;
        opp.Unified_Comm_Collaboration_Estimated__c =20;
        Test.starttest();       
        Recursion_blocker.flag = true;
        insert opp;
        
        Recursion_blocker.flag = true;
        opp.Name='TestoppUpdated';
        update opp;
        delete opp;
        Test.stoptest();
    }
}