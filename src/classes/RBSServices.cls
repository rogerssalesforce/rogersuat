/* ********************************************************************
 * Name : RBSServices
 * Description : Web Services. 
 * Modification Log
  =====================================================================
 * Ver    Date          Author                  Modification
 ----------------------------------------------------------------------
 * 1.0    --/--/2012    Kevin DesLauriers       Initial Version
 * 1.1    02/11/2013    Kevin DesLauriers       Clone Quote Request
 * 1.2    02/11/2013    Kevin DesLauriers       Update Clone Quote Request
 **********************************************************************/

global class RBSServices {
    webservice static String validateQuoteExtension(String quoteId){
        if (Utils.isEmpty(quoteId))
            return 'Error on validation - Missing quote id.';
                    
        Quote q;
        try{
            q = [SELECT id, Discount_Approval__c, AccountRecordType__c FROM Quote WHERE id = :quoteId];
        }catch(Exception ex){
            return 'Error on validation - Invalid Quote Id.';
        }
        
        if (q == null)
            return 'Error on validation - Missing quote id.';
        else if (!q.AccountRecordType__c.toLowerCase().contains('carrier') && !q.Discount_Approval__c){
            return 'Error on validation - Quote must be for an Enterprise or Channel Account and must be approved to request an expiry extension.';
        }
        
        
        return '';
    }
    
 webservice static String allowCloneQuoteRequest(String qRequestId){
    
    if (Utils.isEmpty(qRequestId))
        return 'Unable to Clone the Quote Request: Missing Quote Request Id.';
    
    Quote_Request__c quoteRequest;
        
    try{
        quoteRequest = [SELECT Id, Name, Quote__c, Quoting_Stage__c  FROM Quote_Request__c WHERE Id = :qRequestId];
    }catch(Exception ex){
        return 'Unable to Clone the Quote Request: Quote Request Id is invalid.';
    }
        
    User currentUser = [SELECT Id, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
    
    if (quoteRequest.Quoting_Stage__c != 'Closed by Quote Desk')
        return 'Unable to Clone the Quote Request: Only quote requests that have been closed by the Quote Desk may be cloned.';
    
    // boolean quoteRequestCompleted = quoteRequest.Quoting_Stage__c == 'Closed by Quote Desk';
    
    if (!(currentUser.Profile.Name.startsWith('Network Design Consultant') || currentUser.Profile.Name.equals('System Administrator'))){
        return 'Unable to Clone the Quote Request: Only the Sales Engineer or the System Administrator may Clone a Quote Request.';
    }
    
    return '';
    //return quoteRequestCompleted && (currentUser.Profile.Name.equals('Serviceability Team') || currentUser.Profile.Name.equals('System Administrator'));
 }
    
  webservice static id cloneQuoteRequest(String qRequestId){
    
    
    if (Utils.isEmpty(qRequestId))
        return null;
    
    SavePoint sp = Database.setSavepoint();
    
    List <RecordType> lRtqr = new List <RecordType> ([SELECT id, recordtype.Name FROM RecordType WHERE recordtype.SobjectType = 'Quote_Request__c']);
    Map <String, Id> mapRTqr = new Map <String, Id> ();
        
    for (RecordType rtqr : lRtqr){
        mapRTqr.put(rtqr.Name,rtqr.id);  
    }
    
    Quote_Request__c quoteRequest;
        
    try{
        quoteRequest = [SELECT 
            Account__c,
            Circuit_ID__c,
            Diversity__c,
            NPA_NXX__c,
            Port__c,
            Product_Type__c,
            Cos__c,
            MTU_Size__c,
            Other_EVC_Size__c,
            Access__c,
            
            
            //EVC_Size__c,
            //EVPL_EPL__c,
            NNI__c,
            //NPA__c,
            //NXX__c,
            Notes__c,
            Opportunity__c,
            //EVC_Size_Other__c,
            //Port_Size__c,
            //Port_Type__c,
            Quote__c,
            Quote_Request_Record_Type__c,
            Quoting_Stage__c,
            //Reason__c,
            Id,
            Request_Type__c,
            Sales_Engineer__c,
            //Services__c,
            Submit_to_Quote_Desk__c,
            Term__c,
            Original_Quote_Request__c, 
            parentQuoteRequest__c,
                (SELECT Id,
                     X3rd_Party_Quote__c,
                     Carrier_Name__c,
                     Expiry_Date__c,
                     Facility_Inquiry__c,
                     Selected__c,
                     Quote_Request__c,
                     Receive_Date__c,
                     Requested_Date__c
                 FROM Carrier_Quote__r)
          FROM Quote_Request__c WHERE Id = :qRequestId];
    }catch(Exception ex){
        Database.rollback(sp);
        return null;
    }
  
    // If the Quote Request is obtained we will create the new Quote Request
    Quote_Request__c newQuoteRequest = new Quote_Request__c();
        newQuoteRequest.Account__c = quoteRequest.Account__c;
        newQuoteRequest.Circuit_ID__c = quoteRequest.Circuit_ID__c;
        newQuoteRequest.Diversity__c = quoteRequest.Diversity__c;
        newQuoteRequest.NPA_NXX__c = quoteRequest.NPA_NXX__c;
        newQuoteRequest.Port__c = quoteRequest.Port__c;
        newQuoteRequest.NNI__c = quoteRequest.NNI__c;
        newQuoteRequest.Product_Type__c = quoteRequest.Product_Type__c;
        newQuoteRequest.Cos__c = quoteRequest.Cos__c;
        newQuoteRequest.Notes__c = quoteRequest.Notes__c;
        newQuoteRequest.Opportunity__c = quoteRequest.Opportunity__c;
        newQuoteRequest.MTU_Size__c = quoteRequest.MTU_Size__c;
        newQuoteRequest.Other_EVC_Size__c = quoteRequest.Other_EVC_Size__c;
        newQuoteRequest.Access__c = quoteRequest.Access__c;
        newQuoteRequest.Quote__c = quoteRequest.Quote__c;
        newQuoteRequest.Quoting_Stage__c = 'Cloned by Sales Engineer (Pre-Submission)';
    //  newQuoteRequest.Reason__c = quoteRequest.Reason__c;
        newQuoteRequest.Request_Type__c = quoteRequest.Request_Type__c;
        newQuoteRequest.Sales_Engineer__c = quoteRequest.Sales_Engineer__c;
    //  newQuoteRequest.Services__c = quoteRequest.Services__c;
        newQuoteRequest.Submit_to_Quote_Desk__c = quoteRequest.Submit_to_Quote_Desk__c;
        newQuoteRequest.Term__c = quoteRequest.Term__c;
        newQuoteRequest.RecordTypeId = mapRTqr.get('Out of Process');
  
        newQuoteRequest.parentQuoteRequest__c = quoteRequest.Id;
        newQuoteRequest.Original_Quote_Request__c = ((quoteRequest.Original_Quote_Request__c == null)?quoteRequest.Id:quoteRequest.Original_Quote_Request__c);
    
    try{
        INSERT newQuoteRequest;
    }catch(Exception ex){
        Database.rollback(sp);
        return null;
    }
    
    
    
    // Need to add all of the Locations to the Quote Request.
    // This would include all Prospect Sites/Quote Sites.
    
    
        
    List<Quote_Site__c> qSites = [SELECT id, Site__c, Site__r.Is_a_Z_Site__c, Site__r.Display_Name__c FROM Quote_Site__c WHERE Quote__c = :quoteRequest.Quote__c];
        
    List<Location_A__c> locationAs = new List<Location_A__c>();
    List<Location_Z__c> locationZs = new List<Location_Z__c>();
       
    
        
    for (Quote_Site__c qs : qSites){
        if (qs.Site__r.Is_a_Z_Site__c){
            locationZs.add(new Location_Z__c(Name = qs.Site__r.Display_Name__c, Quote_Request__c = newQuoteRequest.Id, Prospect_Site__c = qs.Site__c));
        }else{
            locationAs.add(new Location_A__c(Name = qs.Site__r.Display_Name__c, Quote_Request__c = newQuoteRequest.Id, Prospect_Site__c = qs.Site__c));
        }
    }
        
    try{    
        INSERT locationAs;
        INSERT locationZs;
    }catch(Exception ex){
        Database.rollback(sp);
        return null;
    }
    
    // Grab all of the quoteRequestServices and then query the serviceInfo based on the ServiceRequestInfo__c
    List<QuoteRequestService__c> quoteRequestServices = [SELECT id, Quote_Request__c, Service__c, ServiceRequestInfo__c FROM QuoteRequestService__c WHERE Quote_Request__c = :quoteRequest.Id];
    List<ServiceRequestInfo__c> serviceRequestInfoObjects = new List<ServiceRequestInfo__c>();
    
    if (quoteRequestServices != null){
        Set<Id> serviceRequestInfoIds = new Set<Id>();
        for (QuoteRequestService__c qrs : quoteRequestServices){
            serviceRequestInfoIds.add(qrs.ServiceRequestInfo__c);
        }
        
        serviceRequestInfoObjects = [SELECT Access__c,
            Circuit_ID__c,
            CoS__c,
            Diversity__c,
            EVC__c,
            MTU_Size__c,
            NNI__c,
            NPA_NXX__c,
            Other_EVC_Size__c,
            Port__c,
            Product_Type__c,
            Id,
            Name
         
         FROM ServiceRequestInfo__c WHERE id IN :serviceRequestInfoIds];
    }
    
    // Create a map of the ServiceRequestInfo as the key and the list of the QuoteRequestService items as the value.
    Map<Id, List<QuoteRequestService__c>> oldqrServiceMap = new Map<Id, List<QuoteRequestService__c>>();
    for (QuoteRequestService__c qrs : quoteRequestServices){
        List<QuoteRequestService__c> temp;
        if (oldqrServiceMap.get(qrs.ServiceRequestInfo__c) == null){
            temp = new List<QuoteRequestService__c>();
        }else{
            temp = oldqrServiceMap.get(qrs.ServiceRequestInfo__c);
        }
        temp.add(qrs);      
        oldqrServiceMap.put(qrs.ServiceRequestInfo__c, temp);
    }
    
    List<ServiceRequestInfo__c> newServiceRequestInfo = new List<ServiceRequestInfo__c>(); 
    for (ServiceRequestInfo__c sri : serviceRequestInfoObjects){
        ServiceRequestInfo__c newSRI = new ServiceRequestInfo__c();
        newSRI.Access__c = sri.Access__c;
        newSRI.Circuit_ID__c = sri.Circuit_ID__c; 
        newSRI.CoS__c = sri.CoS__c; 
        newSRI.Diversity__c = sri.Diversity__c; 
        newSRI.EVC__c = sri.EVC__c; 
        newSRI.MTU_Size__c = sri.MTU_Size__c; 
        newSRI.NNI__c = sri.NNI__c; 
        newSRI.NPA_NXX__c = sri.NPA_NXX__c; 
        newSRI.Other_EVC_Size__c = sri.Other_EVC_Size__c; 
        newSRI.Port__c = sri.Port__c; 
        newSRI.Product_Type__c = sri.Product_Type__c; 
        newSRI.Parent_ServiceRequestInfo__c = sri.Id;
        newServiceRequestInfo.add(newSRI);
    }
    
    if (newServiceRequestInfo.size()>0){
        try{
            INSERT newServiceRequestInfo;
        }catch(Exception ex){
            Database.rollback(sp);
            return null;
        }
    }
    
    // allows us to connect the two serviceRequestInfo objects so we can prepare the quoterequestService items correctly
    Map<Id, Id> oldNewSRIMap = new Map<Id, Id>();
    for (ServiceRequestInfo__c sri : newServiceRequestInfo){
        oldNewSRIMap.put(sri.Parent_ServiceRequestInfo__c, sri.Id);
    }
   
   System.debug('Old Ones Map: ' + oldNewSRIMap);
   
    
    List<QuoteRequestService__c> newqrs = new List<QuoteRequestService__c>(); 
    for (ServiceRequestInfo__c newsri : newServiceRequestInfo){
        List<QuoteRequestService__c> oldqrs = oldqrServiceMap.get(newsri.Parent_ServiceRequestInfo__c);
        
        System.debug('Old Ones: ' + oldqrs);
            
        for (QuoteRequestService__c qrs : oldqrs){
            QuoteRequestService__c tempqrs = new QuoteRequestService__c();
            tempqrs.Quote_Request__c = newQuoteRequest.Id;
            tempqrs.Service__c = qrs.Service__c;
            tempqrs.ServiceRequestInfo__c = oldNewSRIMap.get(qrs.ServiceRequestInfo__c);
            
            newqrs.add(tempqrs);
        }
    }
    
    
    
    
    try{
        INSERT newqrs;
    }catch(Exception ex){
        Database.rollback(sp);
        return null;
    }
    
    
    
   
   
    
    // Create the Carrier Quote Objects 
    List<Carrier_Quote__c> carrierQuotes = quoteRequest.Carrier_Quote__r;
    
    // We need to obtain the Carrier Quote Information Objects and we will use these to get them
    Set<Id> cQuoteIds = new Set<Id>();
    List<Carrier_Quote__c> newCarrierQuoteRequests = new List<Carrier_Quote__c>();
    
    if (carrierQuotes!= null && !carrierQuotes.isEmpty()){
        
        for (Carrier_Quote__c cQuote : carrierQuotes){
            Carrier_Quote__c tempCarrierQuote = new Carrier_Quote__c();
                tempCarrierQuote.X3rd_Party_Quote__c = cQuote.X3rd_Party_Quote__c;
                tempCarrierQuote.Carrier_Name__c = cQuote.Carrier_Name__c;
                tempCarrierQuote.Expiry_Date__c = cQuote.Expiry_Date__c;
                tempCarrierQuote.Facility_Inquiry__c = cQuote.Facility_Inquiry__c;
                tempCarrierQuote.Selected__c = cQuote.Selected__c;
                tempCarrierQuote.Parent_Carrier_Quote__c = cQuote.Id;
                tempCarrierQuote.Quote_Request__c = newQuoteRequest.Id;
                tempCarrierQuote.Receive_Date__c = cQuote.Receive_Date__c;
                tempCarrierQuote.Requested_Date__c = cQuote.Requested_Date__c;
                
                cQuoteIds.add(cQuote.Id);
                newCarrierQuoteRequests.add(tempCarrierQuote);
        }
    
        try{
            INSERT newCarrierQuoteRequests;
        }catch(Exception ex){
            Database.rollback(sp);
            return null;
        }
    }
    
    // allows us to connect the two cQuote so we can prepare the cInfo items correctly
    Map<Id, Id> oldNewMap = new Map<Id, Id>();
    try{
        newCarrierQuoteRequests = [SELECT id, Parent_Carrier_Quote__c FROM Carrier_Quote__c WHERE Quote_Request__c =:newQuoteRequest.Id];
    }catch(Exception ex){
        Database.rollback(sp);
        return null;
    }
    
    for (Carrier_Quote__c cQuote : newCarrierQuoteRequests){
        oldNewMap.put(cQuote.Parent_Carrier_Quote__c, cQuote.Id);
    }
    
    // Obtain all of the Carrier Quote Information Items
    List<Carrier_Quote__c> cQuotes = [SELECT Id, 
    (
    SELECT Id,
        Build_Cost__c,
        Carrier_Name__c,
        Carrier_Quote__c,
        Name,
        City__c,
        Comments__c,
        MRC__c,
        NRC__c,
        Postal_Code__c,
        Site__c,
        Province_Code__c,
        Selected__c,
        Service__c,
        Service_Name__c,
        Street_Direction__c,
        Street_Name__c,
        Street_Number__c,
        Street_Type__c,
        Suite_Floor__c,
        Term__c
    FROM Carrier_Quote_Information__r
    ) FROM Carrier_Quote__c WHERE Id IN :cQuoteIds
    ];
    
    // Create a map of the cQuote as the key and the list of the info items as the value.
    Map<Id, List<Carrier_Quote_Information__c>> oldcQuoteMap = new Map<Id, List<Carrier_Quote_Information__c>>();
    
    for (Carrier_Quote__c cQuote : cQuotes){
        oldcQuoteMap.put(cQuote.Id, cQuote.Carrier_Quote_Information__r);
        
    }
    
    List<Carrier_Quote_Information__c> newcInfos = new List<Carrier_Quote_Information__c>(); 
    for (Carrier_Quote__c newcQuote : newCarrierQuoteRequests){
        List<Carrier_Quote_Information__c> oldcInfos = oldcQuoteMap.get(newcQuote.Parent_Carrier_Quote__c);
        
        for (Carrier_Quote_Information__c cInfo : oldcInfos){
            Carrier_Quote_Information__c tempcInfo = new Carrier_Quote_Information__c();
            tempcInfo.Build_Cost__c = cInfo.Build_Cost__c;
            tempcInfo.Carrier_Name__c = cInfo.Carrier_Name__c;
            tempcInfo.Carrier_Quote__c = oldNewMap.get(cInfo.Carrier_Quote__c);
            tempcInfo.City__c = cInfo.City__c;
            tempcInfo.Comments__c = cInfo.Comments__c;
            tempcInfo.MRC__c = cInfo.MRC__c;
            tempcInfo.NRC__c = cInfo.NRC__c;
            tempcInfo.Postal_Code__c = cInfo.Postal_Code__c;
            tempcInfo.Site__c = cInfo.Site__c;
            tempcInfo.Province_Code__c = cInfo.Province_Code__c;
            tempcInfo.Selected__c = cInfo.Selected__c;
            tempcInfo.Service__c = cInfo.Service__c;
            tempcInfo.Service_Name__c = cInfo.Service_Name__c;
            tempcInfo.Street_Direction__c = cInfo.Street_Direction__c;
            tempcInfo.Street_Name__c = cInfo.Street_Name__c;
            tempcInfo.Street_Number__c = cInfo.Street_Number__c;
            tempcInfo.Street_Type__c = cInfo.Street_Type__c;
            tempcInfo.Suite_Floor__c = cInfo.Suite_Floor__c;
            tempcInfo.Term__c = cInfo.Term__c;
            
            newcInfos.add(tempcInfo);
        }
    }
    
    try{
        INSERT newcInfos;
    }catch(Exception ex){
        Database.rollback(sp);
        return null;
    }
    
    
    
    return newQuoteRequest.Id;
    
    
    return null;
  }
    
    
 webservice static void updateBrowserUsage(String userAgent) {
        

        User u = [SELECT Id, LastLoginDate FROM User WHERE Id = :UserInfo.getUserId()];

        Long myDateTime = datetime.now().getTime();
        Long lastLogin = u.LastLoginDate.getTime();
        Long deltaTime = myDateTime - lastLogin;
        
        
        
        System.debug('Delta Time: ' + deltaTime);
        System.debug('User Agent: ' + userAgent);
        // We only want to do this once per logon and we want it to happen directly after they logon (within 10 seconds).
        if (deltaTime > 60000)
            return;
        
        if (userAgent==null)
            return;
            
        userAgent = userAgent.toLowerCase();
        
        User_Login_Statistics__c userLogin = new User_Login_Statistics__c();
        userLogin.User__c = u.id;
        userLogin.Login_Time__c = DateTime.now();
         
    
    
//try{
   //     Browser_Usage_Statistics__c totalUsage = [SELECT iPad__c, Chrome__c, iPhone__c, Internet_Explorer__c, Safari__c, Other__c, Blackberry__c FROM Browser_Usage_Statistics__c LIMIT 1];
       // if (browser==null)
      //      return;
        
        if (userAgent.contains('ipad'))
            userLogin.Browser__c = 'iPad';
        else if (userAgent.contains('iphone'))
            userLogin.Browser__c = 'iPhone';
        else if (userAgent.contains('blackberry'))
            userLogin.Browser__c = 'BlackBerry';
        else if (userAgent.contains('chrome'))
            userLogin.Browser__c = 'Chrome';
        else if (userAgent.contains('safari'))
            userLogin.Browser__c = 'Safari';
        else if (userAgent.contains('msie'))
            userLogin.Browser__c = 'Internet Explorer';
        else if (userAgent.contains('firefox'))
            userLogin.Browser__c = 'Firefox';
        else if (userAgent.contains('opera'))
            userLogin.Browser__c = 'Opera';
        else
            userLogin.Browser__c = 'Other';
            
        INSERT userLogin;
     
 //    }catch(Exception ex){
        
 //    }
     
          }
          
          static testMethod void test1(){
            RBSServices.updateBrowserUsage('opera');
            RBSServices.updateBrowserUsage('ipad');
            RBSServices.updateBrowserUsage('iphone');
            RBSServices.updateBrowserUsage('blackberry');
            RBSServices.updateBrowserUsage('chrome');
            RBSServices.updateBrowserUsage('safari');
            RBSServices.updateBrowserUsage('msie');
            RBSServices.updateBrowserUsage('firefox');
            RBSServices.updateBrowserUsage('');
          }
}