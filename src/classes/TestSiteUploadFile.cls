/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (seeAllData=False)
private class TestSiteUploadFile {
    static SiteUploadFile siteFile;
    private static Map<String, String> mapFieldAssignments = new Map<String, String>{'Suite_NA' => 'Suite_N_A__c', 'Street Name' => 'Street_Name__c', 'Street Type' => 'Street_Type__c', 
    	                                                     'SuiteFloor' => 'Suite_Floor__c', 'City' => 'City__c', 'Postal Code' => 'Postal_Code__c', 'Street Direction' => 'Street_Direction__c', 
    	                                                     'Street Number' => 'Street_Number__c', 'Name' => 'Name', 'Province Code' => 'Province_Code__c'};
    static Blob goodFileContent(){
        Blob sampleBlob=Blob.valueOf(
            'SuiteFloor,Street Number,City,Street Type,Province Code,Postal Code,Street Name,Name,Street Direction,Suite_NA\r\n'+
            '1,1,City1,Ave,ON,A1A1A1A,Unit Test 1,Please Ignore Field,E,false\r\n' +
            '2,2,City2,Ave,ON,A1A1A1A,Unit Test 2,Please Ignore Field,E,false\r\n' +
            '3,3,City3,Ave,ON,A1A1A1A,Unit Test 3,Please Ignore Field,E,false\r\n' +
            '4,4,,Ave,ON,A1A1A1A,Unit Test 4,Please Ignore Field,E,false\r\n' +
            '5,5,City5,Ave,ON,A1A1A1A,,Please Ignore Field,E,false\r\n' +
            '6,6,City6,Ave,ON,A1A1A1A,Unit Test 6,Please Ignore Field,E,false\r\n' +
            '7,,City7,Ave,ON,A1A1A1A,Unit Test 7,Please Ignore Field,E,false'
        );
        
        return sampleBlob;
    } 

    static List <String> goodResult ;
    static void setGoodResult() {
        goodResult = new String[8] ;
        goodResult[0] = 'header';
        goodResult[1] = 'Success';
        goodResult[2] = 'Success';
        goodResult[3] = 'Success';
        goodResult[4] = 'Required fields are missing: [City__c];';
        goodResult[5] = 'Required fields are missing: [Street_Name__c];';
        goodResult[6] = 'Success';
        goodResult[7] = 'Required fields are missing: [Street_Number__c];';
    }
    
    static Blob duplicateRecord(){
        Blob sampleBlob=Blob.valueOf(
            'SuiteFloor,Street Number,City,Street Type,Province Code,Postal Code,Street Name,Name,Street Direction\r\n'+
            '1,1,City1,Ave,ON,A1A1A1A,Unit Test 1,Please Ignore Field,E\r\n' +
            '2,2,City2,Ave,ON,A1A1A1A,Unit Test 2,Please Ignore Field,E\r\n' +
            '1,1,City1,Ave,ON,A1A1A1A,Unit Test 1,Please Ignore Field,E\r\n' +
            '4,4,City4,Ave,ON,A1A1A1A,Unit Test 4,Please Ignore Field,E\r\n' +
            '5,5,City5,Ave,ON,A1A1A1A,Unit Test 5,Please Ignore Field,E\r\n' +
            '6,6,City6,Ave,ON,A1A1A1A,Unit Test 6,Please Ignore Field,E\r\n' +
            '7,7,City7,Ave,ON,A1A1A1A,Unit Test 7,Please Ignore Field,E'
        );
        return sampleBlob;
    } 
    
   
    static Blob badContentMissingField(){
        Blob sampleBlob=Blob.valueOf(
            'SuiteFloor,Street Number,City,Street Type,Province Code,Postal Code,Street Name,Name,Street Direction\r\n'+
            '1,City1,Ave,ON,A1A1A1A,Unit Test 1,Please Ignore Field,E\r\n' +
            '2,City2,Ave,ON,A1A1A1A,Unit Test 2,Please Ignore Field,E\r\n' +
            '3,City3,Ave,ON,A1A1A1A,Unit Test 3,Please Ignore Field,E\r\n' +
            '4,City4,Ave,ON,A1A1A1A,Unit Test 4,Please Ignore Field,E\r\n' +
            '5,City5,Ave,ON,A1A1A1A,Unit Test 5,Please Ignore Field,E\r\n' +
            '6,City6,Ave,ON,A1A1A1A,Unit Test 6,Please Ignore Field,E\r\n' +
            '7,City7,Ave,ON,A1A1A1A,Unit Test 7,Please Ignore Field,E'
        );
        return sampleBlob;
    } 
    
    static Blob badContentMissingHeader(){
        Blob sampleBlob=Blob.valueOf(
            'SuiteFloor,City,Street Type,Province Code,Postal Code,Street Name,Name,Street Direction\r\n'+
            '1,City1,Ave,ON,A1A1A1A,Unit Test 1,Please Ignore Field,E\r\n' +
            '2,City2,Ave,ON,A1A1A1A,Unit Test 2,Please Ignore Field,E\r\n' +
            '3,City3,Ave,ON,A1A1A1A,Unit Test 3,Please Ignore Field,E\r\n' +
            '4,City4,Ave,ON,A1A1A1A,Unit Test 4,Please Ignore Field,E\r\n' +
            '5,City5,Ave,ON,A1A1A1A,Unit Test 5,Please Ignore Field,E\r\n' +
            '6,City6,Ave,ON,A1A1A1A,Unit Test 6,Please Ignore Field,E\r\n' +
            '7,City7,Ave,ON,A1A1A1A,Unit Test 7,Please Ignore Field,E'
        );
        return sampleBlob;
    } 
    
    static Blob badContentIncorrectHeader(){
        Blob sampleBlob=Blob.valueOf(
            'Incorrect,SuiteFloor,Street Number,City,Street Type,Province Code,Postal Code,Street Name,Name,Street Direction\r\n'+
            '1,1,1,City1,Ave,ON,A1A1A1A,Unit Test 1,Please Ignore Field,E\r\n' +
            '1,2,2,City2,Ave,ON,A1A1A1A,Unit Test 2,Please Ignore Field,E\r\n' +
            '1,3,3,City3,Ave,ON,A1A1A1A,Unit Test 3,Please Ignore Field,E\r\n' +
            '1,4,4,City4,Ave,ON,A1A1A1A,Unit Test 4,Please Ignore Field,E\r\n' +
            '1,5,5,City5,Ave,ON,A1A1A1A,Unit Test 5,Please Ignore Field,E\r\n' +
            '1,6,6,City6,Ave,ON,A1A1A1A,Unit Test 6,Please Ignore Field,E\r\n' +
            '1,7,7,City7,Ave,ON,A1A1A1A,Unit Test 7,Please Ignore Field,E'
        );
        return sampleBlob;
    } 
    
    static void verifyResult(){
        System.debug( '\n\nSL-test verifyResult start');
        List <Site__c> sites = [select s.Id , s.Name, s.Suite_Floor__C, s.Street_Number__c
                                        , s.Street_Name__c
                                        , s.Street_Type__c
                                        , s.Street_Direction__c
                                        , s.City__c
                                        , s.Postal_Code__c
                                        , s.Province_Code__c
                                         from Site__c s
                                         where s.Street_Name__c LIKE 'Unit Test%'];
        System.debug( '\n\n\n SL-Test: Saved records: '+sites.size());
        System.assertEquals(sites.size(), 4);
        
        Integer i;
        setGoodResult();            
        for ( Site__c site :sites) {
         
            System.debug( 'Test Site: ' 
                + 'Name: ' + site.Name + '; '
                + '\n \t'
                + 'Id: ' + site.Id + '; '
                + 'SuiteFloor: ' + site.Suite_Floor__c + '; ' 
                + '\n \t'
                + 'Street Number: ' + site.Street_Number__c + '; ' 
                + 'Street Name: ' + site.Street_Name__c + '; ' 
                + '\n \t'
                + 'Street Type: ' + site.Street_Type__c + '; ' 
                + '\n \t'
                + 'Street Direction:' + site.Street_Direction__c + '; '
                + '\n \t'
                + 'City:' + site.City__c + '; '
                + '\n \t'
                + 'Postal Code:' + site.Postal_Code__c + '; '
                + '\n \t'
                + 'Province Code:' + site.Province_Code__c + '; '
                );
        }
        i=0;
        for ( Site_Upload_Records g :siteFile.UploadRecords) {
            System.debug( 'File Content: ' + i + ' ' 
                + g 
                + '\n \t'
                + 'Expected Status:' + goodResult[i]+ '; \n'
                );
            
            if(i>0){
                System.assertEquals( goodResult.get(i), g.Record_Status);
            }
            i++;
        }
       
    }
    
    /*********************************************************************************
    Method Name    : createTestData
    Description    : Method used to create test data
    Return Type    : 
    Parameter      : 
    *********************************************************************************/
    private static void createTestData() {
        List<Field_Assignment__c> lstFieldAssignements = new List<Field_Assignment__c>();
        for (String csvField : mapFieldAssignments.keySet()) {
            Field_Assignment__c oFieldAssignment = new Field_Assignment__c();
            oFieldAssignment.CSV_Field__c = csvField;
            oFieldAssignment.Target_Field__c = mapFieldAssignments.get(csvField);
            oFieldAssignment.Active__c = true;
            oFieldAssignment.Type__c = 'Prospect Sites';
            lstFieldAssignements.add(oFieldAssignment);
        }
        insert lstFieldAssignements;
    }
    
    static testMethod void testsiteupload(){
        createTestData();
        
        PageReference pageRef = Page.SiteUploadFile;
        Test.setCurrentPage(pageRef);
        System.debug ('\n\nSL: testsiteuploadFile: URL=' + pageRef.getUrl());
        
        siteFile = new siteuploadFile();
        siteFile.pageInitialization();
        siteFile.pageExtInitialization();

        siteFile.back();
        Test.setCurrentPage(pageRef);
        siteFile.openError();
        Test.setCurrentPage(pageRef);
        siteFile.openReport();
        Test.setCurrentPage(pageRef);
        siteFile.openUploadFile();
        
        Test.setCurrentPage(pageRef);
        PageReference newDataPage = siteFile.createNew();
        System.assert(newDatapage != null);
        
        // First case: empty file uploaded
        siteFile = new siteuploadFile();
        siteFile.pageInitialization();
        siteFile.contentFile = null;
        PageReference nextPage = siteFile.ReadFile();
        System.assertEquals(null, nextPage);
        
        // Normal case: 
        System.debug ('\n\nSL-test: ReadFile: normal case');
        siteFile = new siteuploadFile();
        siteFile.pageInitialization();
        siteFile.contentFile = goodFileContent();
        nextPage = siteFile.ReadFile();
        
        
        List <Field_Assignment__c> lA = [select id,CSV_Field__c,Target_Field__c from Field_Assignment__c where Active__c = true AND Type__c = 'Prospect Sites'];
        system.assertNotEquals(lA, null);
        system.assertEquals(lA.size(), mapFieldAssignments.size());
        
        verifyResult();
        if ( nextPage != null){
            System.debug ('\n\nSL-test: after ReadFile: URL=' + nextPage.getUrl());
        }
        
        // Bad cases: 
        System.debug ('\n\nSL-test: bad case: Missing Header');
        siteFile = new siteuploadFile();
        siteFile.pageInitialization();
        siteFile.contentFile = badContentMissingHeader();
        nextPage = siteFile.ReadFile();
        
         
        System.debug ('\n\nSL-test: bad case: Incorrect Header');
        siteFile = new siteuploadFile();
        siteFile.pageInitialization();
        siteFile.contentFile = badContentMissingHeader();
        nextPage = siteFile.ReadFile();
        
        System.debug ('\n\nSL-test: bad case: Missing Field');
        siteFile = new siteuploadFile();
        siteFile.pageInitialization();
        siteFile.contentFile = badContentMissingField();
        nextPage = siteFile.ReadFile();
        
        System.debug ('\n\nSL-test: bad case: Duplicate Record');
        siteFile = new siteuploadFile();
        siteFile.pageInitialization();
        siteFile.contentFile = duplicateRecord();
        nextPage = siteFile.ReadFile();
    } // testsiteuploadFile
    
}