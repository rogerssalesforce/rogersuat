/*
===============================================================================
Class Name : SalesMeasurementRecalcSchedular_Test
===============================================================================
PURPOSE: This is a test class for SalesMeasurementRecalculationSchedular class

COMMENTS: 

Developer: Deepika Rawat
Date: 9/11/2013


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
9/11/2013               Deepika               Created
===============================================================================

*/
@isTest(SeeAllData = true)
private class SalesMeasurementRecalcSchedular_Test{
 static testmethod void testMethod1(){                
        Test.StartTest();
        SalesMeasurementRecalculationSchedular obj= new SalesMeasurementRecalculationSchedular();
        String sch = '0 0 23 * * ?';
        system.schedule('TestForecastDeletion', sch, obj);
        System.assert(true);
        Test.stopTest();    

    }
}