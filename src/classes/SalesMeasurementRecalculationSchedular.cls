/*Class Name :SalesMeasurementRecalculationSchedular.
 *Description : .
 *Created By : Deepika Rawat.
 *Created Date :05/11/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
global class SalesMeasurementRecalculationSchedular implements schedulable{

    global void execute(SchedulableContext ctx) {
        integer SFCheckPoint = Integer.ValueOf(Static_Data_Utilities__c.getInstance('Recalculation Period').Value__c);
        date dT1=system.Today().addDays(-SFCheckPoint);
        SalesForecastRecalculationBatch salesMeasurementObj = new SalesForecastRecalculationBatch();
        if(!Test.isRunningTest()){
        salesMeasurementObj.Query = 'select id, LastModifiedDate, Quota_Family__c, Quantity__c, Revenue__c, OwnerMonthFamily__c, Deployment_Month__c, OwnerID, Deployment_Date__c, Opportunity__r.StageName, Opportunity__r.LastModifiedDate, Opportunity__c, Weighted_Revenue__c, Weighted_Quantity__c FROM Sales_Measurement__c where Deployment_Date__c >=:dT1';
        }
        else{
        salesMeasurementObj.Query = 'select id, LastModifiedDate, Quota_Family__c, Quantity__c, Revenue__c, OwnerMonthFamily__c, Deployment_Month__c, OwnerID, Deployment_Date__c, Opportunity__r.StageName, Opportunity__r.LastModifiedDate, Opportunity__c, Weighted_Revenue__c, Weighted_Quantity__c FROM Sales_Measurement__c where Deployment_Date__c >=:dT1 limit 2';
        }
        ID salesMeasurementBatchProcessId = Database.executeBatch(salesMeasurementObj,200);
    }
}