/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestDiscounting {
    static Id carrier_priceEntry1id, carrier_priceEntry2_1id, carrierOppId, oppId, sl1Id, sl2Id, site1Id, site2Id, site3Id, site4Id, site5Id, site6Id, site7Id; 
    static Id priceEntry1id, priceEntry2_1id, priceEntry2id, quote1Id;
    static Opportunity o, o_carrier;
    
    static Product2 pDefaultEnterprise, pCustomLargeEnterpriseMaxMin, pCustomLargeEnterpriseMax, pCustomLargeEnterpriseMin, pCustomLargeEnterpriseNoMaxMin;
    static Product2 pCustomMediumEnterpriseMaxMin, pCustomMediumMediumMax, pCustomMediumEnterpriseMin, pCustomMediumEnterpriseNoMaxMin;
    
    static Product2 pDefaultCarrier, pCustomLargeCarrierMaxMin, pCustomLargeCarrierMax, pCustomLargeCarrierMin, pCustomLargeCarrierNoMaxMin;
    static Product2 pCustomMediumCarrierMaxMin, pCustomMediumMax, pCustomMediumCarrierMin, pCustomMediumCarrierNoMaxMin;
    
    static Boolean withAssertions = true;
    static Pricebook2 sp, sp1;
    static Map <string,Id> mapRTa, mapRTo, mapRTp;
    
    static {
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        
        List <RecordType > lRtp = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Product2']);
        mapRTp = new Map <string,id> ();
        
        for (RecordType rtp : lRtp){
            mapRTp.put(rtp.Name,rtp.id);  
        }
        
        
        sp1 = new Pricebook2();
        sp1 = [select id from Pricebook2 where isStandard = true];
              
        ServiceableLocation__c sl = new ServiceableLocation__c();
        sl.Street_Name__c = 'Somewhere';
        sl.Street_Number__c  = '5';
        sl.Street_Type__c  = 'Ave';
        sl.City__c = 'Coty';
        sl.Postal_Code__c = 'A1A1A1';
        sl.Province_Code__c = 'ON';
        sl.CLLI_Code__c = '123113';
        sl.Access_Type_Group__c = 'NNI';
        sl.Access_Type__c = 'Ethernet Eon';
        insert sl;
        sl1Id = sl.Id;
        
        ServiceableLocation__c sl2 = new ServiceableLocation__c();
        sl2.Street_Name__c = 'Somewhere';
        sl2.Street_Number__c  = '5';
        sl2.Street_Type__c  = 'Ave';
        sl2.City__c = 'Coty';
        sl2.Postal_Code__c = 'A1A1A1';
        sl2.Province_Code__c = 'ON';
        sl2.CLLI_Code__c = '123113';
        sl2.Access_Type_Group__c = 'Fibre';
        sl2.Access_Type__c = 'ETHERNET EON;ETHERNET CAP';
        insert sl2;
        sl2Id = sl2.Id;
        
    }
    
    private static void addEnterpriseInformation(){
        Account a = new Account();
        a.name = 'Test Act - Enterprise Quote Wizard';
        a.Business_Segment__c = 'Alternate'; 
        //a.RecordTypeId = mapRTa.get('New Account');
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street Wizard';
        a.BillingCity = 'MyCity Wizard';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L2L2L2';
        a.BillingState = 'ON';
        a.parentId = null;
        a.Account_Status__c = 'Assigned';
        insert a;       
        
        o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.Unified_Comm_Collaboration_Estimated__c =20;
        o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.today();
        insert o;
        oppId = o.id;
        
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Somewhere4';
        s1.Suite_Floor__c = '11a';
        s1.Street_Number__c  = '7';
        s1.City__c = 'Coty';
        s1.Postal_Code__c = 'A3A1A1';
        s1.Province_Code__c = 'ON';
        s1.CLLI_SWC__c = '123113';
        s1.ServiceableLocation__c = sl1Id;
        s1.Opportunity__c = oppId;
        s1.Is_a_Z_Site__c = true; 
        insert s1;
        site1Id = s1.Id;
        
        Site__c s2 = new Site__c();
        s2.Street_Name__c = 'Somewhere5';
        s2.Suite_Floor__c = '11a';
        s2.Street_Number__c  = '8';
        s2.City__c = 'Coty';
        s2.Postal_Code__c = 'A3A1A1';
        s2.Province_Code__c = 'ON';
        s2.CLLI_SWC__c = '123113';
        s2.ServiceableLocation__c = sl2Id;
        s2.Opportunity__c = oppId; 
        insert s2;
        site2Id = s2.Id;
        
        Site__c s3 = new Site__c();
        s3.Street_Name__c = 'Somewhere6';
        s3.Suite_Floor__c = '11a';
        s3.Street_Number__c  = '9';
        s3.City__c = 'Coty';
        s3.Postal_Code__c = 'A3A1A1';
        s3.Province_Code__c = 'ON';
        s3.CLLI_SWC__c = '123113';
        s3.ServiceableLocation__c = sl2Id;
        s3.Opportunity__c = oppId; 
        s3.Z_Site__c = s1.Id;
        insert s3;
        site3Id = s3.Id;
      
        
        sp = new Pricebook2();
        sp = [select id from Pricebook2 where Name = :'Enterprise PriceBook'];
        
        Quote q = new Quote(Name='q1', actualTerm__c='12', Term__c='12-Months', OpportunityId=oppId, Pricebook2Id = sp.id);
        insert q;
        quote1Id = q.Id; 
        
        Quote_Site__c qs = new Quote_Site__c();
        qs.Site__c = site1Id;
        qs.Quote__c = quote1Id;
        insert qs;
        
        qs = new Quote_Site__c();
        qs.Site__c = site2Id;
        qs.Quote__c = quote1Id;
        insert qs;
        
        qs = new Quote_Site__c();
        qs.Site__c = site3Id;
        qs.Quote__c = quote1Id;
        insert qs;
    }
    
    static private id createProduct(String largeDiscount, Decimal largeMax, Decimal largeMin, String mediumDiscount, Decimal mediumMax, Decimal mediumMin){
        Product2 p  = new Product2();
        p.IsActive = true;
        p.Name = 'Fibre';
        p.Charge_Type__c = 'MRC';
        p.Access_Type__c = 'Ethernet EON';
        p.Access_Type_Group__c = 'Fibre';
        p.Category__c = 'Access';
        p.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
        p.RecordTypeId = mapRTp.get('Enterprise Products');
        p.Service_Term__c = '12-Months';
        p.Mark_Up_Factor__c = 1.0;
        p.Start_Date__c = Date.today();
        p.End_Date__c = Date.today()+1;
        
        if (largeDiscount!=null)
            p.discountThreshold__c = largeDiscount;
            
        if (largeMax!=null)
            p.Max_Required_for_Custom_Discount__c = largeMax;
        
        if (largeMin!=null)
            p.Min_Required_for_Custom_Discount__c = largeMin;
        
        if (mediumDiscount!=null)
            p.discountThresholdMedium__c = mediumDiscount;
        
        if (mediumMax!=null)
            p.Max_Required_for_Custom_Medium_Discount__c = mediumMax;
        
        if (mediumMin!=null)
            p.Min_Required_for_Custom_Medium_Discount__c = mediumMin;
        
        
        insert p;
        
        PricebookEntry peStandard = new PricebookEntry();
        peStandard.Pricebook2Id = sp1.id;
        peStandard.UnitPrice = 60;
        peStandard.Product2Id = p.id;
        peStandard.IsActive = true;
        peStandard.UseStandardPrice = false;
        
        insert peStandard;
        
        PricebookEntry pe1 = new PricebookEntry();
        pe1.Pricebook2Id = sp.id;
        pe1.UnitPrice = 60;
        pe1.Product2Id = p.id;
        pe1.IsActive = true;
        pe1.UseStandardPrice = false;
        
        insert pe1;     
        
        return pe1.id;
    }
    
    static private QuoteLineItem createQuoteLineItem(Id quoteId, Id pricebook2Id, Id siteId, Decimal quantity, Decimal unitPrice, Decimal discount){
        QuoteLineItem qli = new QuoteLineItem();
        qli.PricebookEntryId = pricebook2Id;
        qli.Site__c = siteId;
        qli.Quantity = quantity;
        qli.QuoteId = quoteId;
        qli.UnitPrice = unitPrice;
        qli.Visible_Quote_Line_Item__c = true;        
        if (discount != null)
            qli.Discount = discount;
        return qli;
    }
    
    static testMethod void unitTest1() {
        addEnterpriseInformation();
        //Product2 p = createProduct(largeDiscount, largeMax, largeMin, mediumDiscount, mediumMax, mediumMin);
        Id pbeId = createProduct(null, null, null, null, null, null);
        Test.startTest();
        
        List<QuoteLineItem> listQli_111 = new List<QuoteLineItem>();
       
       QuoteLineItem qli_111 = createQuoteLineItem(quote1Id, pbeId, site1Id, 1, 60, 0);
       QuoteLineItem qli_211 = createQuoteLineItem(quote1Id, pbeId, site1Id, 1, 60, 0.01);
       QuoteLineItem qli_311 = createQuoteLineItem(quote1Id, pbeId, site1Id, 1, 60, 9);
       QuoteLineItem qli_411 = createQuoteLineItem(quote1Id, pbeId, site1Id, 1, 60, 11);
       QuoteLineItem qli_511 = createQuoteLineItem(quote1Id, pbeId, site1Id, 1, 60, 21);
            
        listQli_111.add(qli_111);
        listQli_111.add(qli_211);
        listQli_111.add(qli_311);
        listQli_111.add(qli_411);
        listQli_111.add(qli_511);
            
        insert listQli_111;
        update listQli_111;
        
        listQli_111[0] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli_111[0].id];
        listQli_111[1] = [SELECT isLargeDiscount__c, Charge_Type__c, discount, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli_111[1].id];
        listQli_111[2] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli_111[2].id];
        listQli_111[3] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli_111[3].id];
        listQli_111[4] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli_111[4].id];
        
        if (withAssertions)
        {
            /*System.assertEquals(0, listQli_111[0].isLargeDiscount__c);
            System.assertEquals(0, listQli_111[0].isMediumDiscount__c);
            System.assertEquals(0, listQli_111[0].isSmallDiscount__c);
        
            System.assertEquals(0, listQli_111[1].isLargeDiscount__c);
            System.assertEquals(0, listQli_111[1].isMediumDiscount__c);
            System.assertEquals(1, listQli_111[1].isSmallDiscount__c);
        
            System.assertEquals(0, listQli_111[2].isLargeDiscount__c);
            System.assertEquals(0, listQli_111[2].isMediumDiscount__c);
            System.assertEquals(1, listQli_111[2].isSmallDiscount__c);
        
            System.assertEquals(0, listQli_111[3].isLargeDiscount__c);
            System.assertEquals(0, listQli_111[3].isMediumDiscount__c);
            System.assertEquals(1, listQli_111[3].isSmallDiscount__c);
        
            System.assertEquals(1, listQli_111[4].isLargeDiscount__c);
            System.assertEquals(0, listQli_111[4].isMediumDiscount__c);
            System.assertEquals(0, listQli_111[4].isSmallDiscount__c);*/
            
        }
        
        Test.stopTest();
    }
    
    static testMethod void unitTest2() {
        addEnterpriseInformation();
        //Product2 p = createProduct(largeDiscount, largeMax, largeMin, mediumDiscount, mediumMax, mediumMin);
        Id pbeId = createProduct('30', null, null, '15', null, null);
        Test.startTest();
        
       List<QuoteLineItem> listQli = new List<QuoteLineItem>();
       
       QuoteLineItem qli_11 = createQuoteLineItem(quote1Id, pbeId, site1Id, 1, 60, 0);
       QuoteLineItem qli_21 = createQuoteLineItem(quote1Id, pbeId, site1Id, 1, 60, 1);
       QuoteLineItem qli_31 = createQuoteLineItem(quote1Id, pbeId, site1Id, 1, 60, 11);
       QuoteLineItem qli_41 = createQuoteLineItem(quote1Id, pbeId, site1Id, 1, 60, 16);
       QuoteLineItem qli_51 = createQuoteLineItem(quote1Id, pbeId, site1Id, 1, 60, 21);
       QuoteLineItem qli_61 = createQuoteLineItem(quote1Id, pbeId, site1Id, 1, 60, 31);
            
        listQli.add(qli_11);
        listQli.add(qli_21);
        listQli.add(qli_31);
        listQli.add(qli_41);
        listQli.add(qli_51);
        listQli.add(qli_61);
        
        insert listQli;
        update listQli;
        
        listQli[0] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[0].id];
        listQli[1] = [SELECT isLargeDiscount__c, Charge_Type__c, discount, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[1].id];
        listQli[2] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[2].id];
        listQli[3] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[3].id];
        listQli[4] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[4].id];
        listQli[5] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[5].id];
        
        if (withAssertions)
        {
            /*System.assertEquals(0, listQli[0].isLargeDiscount__c);
            System.assertEquals(0, listQli[0].isMediumDiscount__c);
            System.assertEquals(0, listQli[0].isSmallDiscount__c);
        
            System.assertEquals(0, listQli[1].isLargeDiscount__c);
            System.assertEquals(0, listQli[1].isMediumDiscount__c);
            System.assertEquals(1, listQli[1].isSmallDiscount__c);
        
            System.assertEquals(0, listQli[2].isLargeDiscount__c);
            System.assertEquals(0, listQli[2].isMediumDiscount__c);
            System.assertEquals(1, listQli[2].isSmallDiscount__c);
        
            System.assertEquals(0, listQli[3].isLargeDiscount__c);
            System.assertEquals(1, listQli[3].isMediumDiscount__c);
            System.assertEquals(0, listQli[3].isSmallDiscount__c);
        
            System.assertEquals(0, listQli[4].isLargeDiscount__c);
            System.assertEquals(1, listQli[4].isMediumDiscount__c);
            System.assertEquals(0, listQli[4].isSmallDiscount__c);
        
            System.assertEquals(1, listQli[5].isLargeDiscount__c);
            System.assertEquals(0, listQli[5].isMediumDiscount__c);
            System.assertEquals(0, listQli[5].isSmallDiscount__c);*/
        
        }
        
        Test.stopTest();
    }
    
    static testMethod void unitTest3() {
         addEnterpriseInformation();
        //Product2 p = createProduct(largeDiscount, largeMax, largeMin, mediumDiscount, mediumMax, mediumMin);
        Id pbeId = createProduct('30', 100, 10, null, null, null);
        Test.startTest();
        
        List<QuoteLineItem> listQli = new List<QuoteLineItem>();
       
       QuoteLineItem qli_1 = createQuoteLineItem(quote1Id, pbeId, site1Id, 1, 60, 21);
       QuoteLineItem qli_2 = createQuoteLineItem(quote1Id, pbeId, site1Id, 15, 60, 21);
       QuoteLineItem qli_3 = createQuoteLineItem(quote1Id, pbeId, site1Id, 1, 60, 14);
       QuoteLineItem qli_4 = createQuoteLineItem(quote1Id, pbeId, site1Id, 15, 60, 14);
       QuoteLineItem qli_5 = createQuoteLineItem(quote1Id, pbeId, site1Id, 15, 60, 37);
       QuoteLineItem qli_6 = createQuoteLineItem(quote1Id, pbeId, site1Id, 200, 60, 37);
       QuoteLineItem qli_7 =  createQuoteLineItem(quote1Id, pbeId, site1Id, 200, 60, 21);
        
        listQli.add(qli_1);
        listQli.add(qli_2);
        listQli.add(qli_3);
        listQli.add(qli_4);
        listQli.add(qli_5);
        listQli.add(qli_6);
        listQli.add(qli_7);
        
        insert listQli;
        update listQli;
        
        listQli[0] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[0].id];
        listQli[1] = [SELECT Quantity, Discount, isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[1].id];
        listQli[2] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[2].id];
        listQli[3] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[3].id];
        listQli[4] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[4].id];
        listQli[5] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[5].id];
        listQli[6] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[6].id];
        
        if (withAssertions)
        {
            /*System.assertEquals(1, listQli[0].isLargeDiscount__c);
            System.assertEquals(0, listQli[0].isMediumDiscount__c);
            System.assertEquals(0, listQli[0].isSmallDiscount__c);
        
            System.assertEquals(0, listQli[1].isLargeDiscount__c);
            System.assertEquals(0, listQli[1].isMediumDiscount__c);
            System.assertEquals(1, listQli[1].isSmallDiscount__c);
        
            System.assertEquals(0, listQli[2].isLargeDiscount__c);
            System.assertEquals(0, listQli[2].isMediumDiscount__c);
            System.assertEquals(1, listQli[2].isSmallDiscount__c);
        
            System.assertEquals(0, listQli[3].isLargeDiscount__c);
            System.assertEquals(0, listQli[3].isMediumDiscount__c);
            System.assertEquals(1, listQli[3].isSmallDiscount__c);
        
            System.assertEquals(1, listQli[4].isLargeDiscount__c);
            System.assertEquals(0, listQli[4].isMediumDiscount__c);
            System.assertEquals(0, listQli[4].isSmallDiscount__c);
        
            System.assertEquals(1, listQli[5].isLargeDiscount__c);
            System.assertEquals(0, listQli[5].isMediumDiscount__c);
            System.assertEquals(0, listQli[5].isSmallDiscount__c);
        
            System.assertEquals(1, listQli[6].isLargeDiscount__c);
            System.assertEquals(0, listQli[6].isMediumDiscount__c);
            System.assertEquals(0, listQli[6].isSmallDiscount__c);*/
        }
      
        Test.stopTest();
    }
    
    static testMethod void unitTest4() {
         addEnterpriseInformation();
        //Product2 p = createProduct(largeDiscount, largeMax, largeMin, mediumDiscount, mediumMax, mediumMin);
        Id pbeId = createProduct(null, null, null, '15', 100, 10);
        Test.startTest();
        
       
       List<QuoteLineItem> listQli = new List<QuoteLineItem>();
       
       QuoteLineItem qli_1 = createQuoteLineItem(quote1Id, pbeId, site1Id, 1, 60, 21);
       QuoteLineItem qli_2 = createQuoteLineItem(quote1Id, pbeId, site1Id, 15, 60, 21);
       QuoteLineItem qli_3 = createQuoteLineItem(quote1Id, pbeId, site1Id, 1, 60, 14);
       QuoteLineItem qli_4 = createQuoteLineItem(quote1Id, pbeId, site1Id, 15, 60, 14);
       QuoteLineItem qli_5 = createQuoteLineItem(quote1Id, pbeId, site1Id, 15, 60, 16);
       QuoteLineItem qli_6 = createQuoteLineItem(quote1Id, pbeId, site1Id, 200, 60, 16);
       QuoteLineItem qli_7 =  createQuoteLineItem(quote1Id, pbeId, site1Id, 200, 60, 14);
        
        listQli.add(qli_1);
        listQli.add(qli_2);
        listQli.add(qli_3);
        listQli.add(qli_4);
        listQli.add(qli_5);
        listQli.add(qli_6);
        listQli.add(qli_7);
        
        insert listQli;
        update listQli;
        
        listQli[0] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[0].id];
        listQli[1] = [SELECT Quantity, Discount, isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[1].id];
        listQli[2] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[2].id];
        listQli[3] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[3].id];
        listQli[4] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[4].id];
        listQli[5] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[5].id];
        listQli[6] = [SELECT isLargeDiscount__c, isMediumDiscount__c, isSmallDiscount__c FROM QuoteLineItem WHERE id = :listQli[6].id];
        
        if (withAssertions)
        {
            /*System.assertEquals(1, listQli[0].isLargeDiscount__c);
            System.assertEquals(0, listQli[0].isMediumDiscount__c);
            System.assertEquals(0, listQli[0].isSmallDiscount__c);
        
            System.assertEquals(1, listQli[1].isLargeDiscount__c);
            System.assertEquals(0, listQli[1].isMediumDiscount__c);
            System.assertEquals(0, listQli[1].isSmallDiscount__c);
        
            System.assertEquals(0, listQli[2].isLargeDiscount__c);
            System.assertEquals(0, listQli[2].isMediumDiscount__c);
            System.assertEquals(1, listQli[2].isSmallDiscount__c);
        
            System.assertEquals(0, listQli[3].isLargeDiscount__c);
            System.assertEquals(0, listQli[3].isMediumDiscount__c);
            System.assertEquals(1, listQli[3].isSmallDiscount__c);
        
            System.assertEquals(0, listQli[4].isLargeDiscount__c);
            System.assertEquals(1, listQli[4].isMediumDiscount__c);
            System.assertEquals(0, listQli[4].isSmallDiscount__c);
        
            System.assertEquals(0, listQli[5].isLargeDiscount__c);
            System.assertEquals(1, listQli[5].isMediumDiscount__c);
            System.assertEquals(0, listQli[5].isSmallDiscount__c);
        
            System.assertEquals(0, listQli[6].isLargeDiscount__c);
            System.assertEquals(0, listQli[6].isMediumDiscount__c);
            System.assertEquals(1, listQli[6].isSmallDiscount__c);*/
        }
        
    }
    
    static testMethod void unitTest5() {
        addEnterpriseInformation();
    }
    
    static testMethod void unitTest6() {
        addEnterpriseInformation();
    }    
}