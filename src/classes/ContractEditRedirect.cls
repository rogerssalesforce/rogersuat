public class ContractEditRedirect {
 	
 	public String newType {get; set;}
 	public String accountID {get; set;}
    public String accID {get; set;}

    public ContractEditRedirect(ApexPages.StandardController controller) {
        this.controller = controller;
      	Wireline_Contract__c con = (Wireline_Contract__c)controller.getRecord();
      	rType = con.RecordTypeId; 
      	newType = ApexPages.currentPage().getParameters().get('RecordType');
      	accID = ApexPages.currentPage().getParameters().get('def_account_id');
        accountID = ApexPages.currentPage().getParameters().get('accid');
    }
 
    public PageReference redirect(){
		List <RecordType > lRtc = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Wireline_Contract__c']);
        Map <string,Id> mapRTc = new Map <string,id> ();
        
        for (RecordType rtc : lRtc){
            mapRTc.put(rtc.Name,rtc.id);  
        }
        
	    PageReference returnURL;
	    Wireline_Contract__c c = null;
	    try{
	    	c = [Select id, recordtypeid From Wireline_Contract__c Where Id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1];
	    }catch (Exception ex){
	    	System.debug('ContractEditRedirect.redirect(): Contract does not exist!');
	    	return null;
	    }
	    
	    // Redirect if Record Type corresponds to custom VisualForce page
	     if(!Utils.isEmpty(rType) && rType.contains(mapRTc.get('Enterprise Contract Renewal'))) {
	        returnURL = new PageReference('/apex/ContractRenewalEditPage');
	        
	        if(accountID != null){
	            returnURL.getParameters().put('accid', AccountID);
	        }
	        
	        if(accID != null){
	            returnURL.getParameters().put('def_Account_id', accID);
	        }
	        returnURL.getParameters().put('RecordType', newType); 
	    }else{
	         returnURL = new PageReference('/' + c.id + '/e');
	         returnURL.getParameters().put('nooverride', '1');
	         returnURL.getParameters().put('RecordType', newType); 
	    }
	 
	    returnURL.getParameters().put('id', c.id);
	     
	    return returnURL.setRedirect(true);
    }
 
    private final ApexPages.StandardController controller;
    public String rType {get; set;}

}