/* ********************************************************************
 * Name : SiteWrapper
 * Description : Wrapper. Allows Site to be used in a list with selectboxes.
 * Modification Log
  =====================================================================
 * Ver    Date          Author               Modification
 ----------------------------------------------------------------------
 * 1.0    -/--/2011     Kevin DesLauriers    Initial Version  
 * 1.1    6/25/2012		Kevin DesLauriers	 Allows use with Quote Request
 **********************************************************************/
public class SiteWrapper{
    public Site__c Site {get; set;}
    public Boolean isSelected;
    public Boolean onQuoteRequest;
	public List<QuoteLineItem> lLineItem {get;set;}
	public Set<id> setIdLineItem{get;set;}
	
	public decimal cMonthly {get
		{
			decimal dMonthly = 0;
			for(QuoteLineItem q1 : lLineItem)
			{
				dMonthly = dMonthly + q1.TotalPrice;
			}
			return dMonthly;
		}
		set;}
		
	public decimal cOneTime {get
		{
			decimal dOneTime = 0;
			for(QuoteLineItem q1 : lLineItem)
			{
				dOneTime = dOneTime + q1.One_Time__c;
			}
			return dOneTime;
		}
		set;}

    public SiteWrapper(Site__c s) {
        Site = s;
        isSelected = false;
        onQuoteRequest = false;
    }
    
    public SiteWrapper(Site__c s, Boolean selected) {
        Site = s;
        isSelected = selected;
        onQuoteRequest = false;
    }
    
    public SiteWrapper(Site__c s, Boolean selected, Boolean onQuoteRequest) {
        Site = s;
        isSelected = selected;
        this.onQuoteRequest = onQuoteRequest;
    }
   
    public SiteWrapper(Site__c s, List<QuoteLineItem> lQLI) {
        Site = new Site__c();
        Site = s;
        lLineItem = new List<QuoteLineItem>();
        
        for(QuoteLineItem q1 : lQLI){
        	lLineItem.add(q1);
        }
        
    }
       
    public void setIsSelected(Boolean b) { this.isSelected = b; }
    public Boolean getIsSelected() { return isSelected; }
    
    public void setOnQuoteRequest(Boolean b) { this.onQuoteRequest = b; }
    public Boolean getOnQuoteRequest() { return onQuoteRequest; }
    
    public static testMethod void testMe (){
        Site__c s = new Site__c();
        SiteWrapper sw = new SiteWrapper(s);
        
    }
   
}