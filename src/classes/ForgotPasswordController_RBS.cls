/**
 * An apex page controller that exposes the site forgot password functionality
 */
public with sharing class ForgotPasswordController_RBS {
    public String username {get; set;}   
       
    public ForgotPasswordController_RBS() {}
  
    public PageReference forgotPassword() {
      boolean success = Site.forgotPassword(username);
      PageReference pr = Page.ForgotPasswordConfirm;
      pr.setRedirect(true);
      
      if (success) {        
        return pr;
      }
      return null;
    }
    
     @IsTest(SeeAllData=true) public static void testForgotPasswordController_RBS() {
      // Instantiate a new controller with all parameters in the page
      ForgotPasswordController_RBS controller = new ForgotPasswordController_RBS();
      controller.username = 'test@salesforce.com';       
    
      System.assertEquals(controller.forgotPassword(),null); 
    }
}