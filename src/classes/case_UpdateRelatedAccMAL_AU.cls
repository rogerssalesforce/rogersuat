/*Class Name  : case_UpdateRelatedAccMAL_AU.
 *Description : This Class will support the logic for case After insert/Update Trigger before Insert.
 *Created By  : Rajiv Gangra.
 *Created Date :10/09/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
public class case_UpdateRelatedAccMAL_AU{

    /*@ Method Name: updateAccountMAL
      Description: This Method gets the list of cases and Acounts, Update MAL field on Account As required. .
      Return Type: NA.
      Input Param: NA.   
    */
    public void updateAccountMAL(LIST<Case> cList,LIST<Case> cListOld) {
        list<Id> lstRelAccount= new list<Id>();
        set<Account> setAccountToUpdate= new set<Account>();
        list<Account> lstAccountToUpdate= new list<Account>();
        list<Id> lstRelRecType= new list<Id>();
        Map<id,String> mapCaseRecordTypeName= new Map<id,String>();
        Map<id,String> mapCaseStatus= new Map<id,String>();
        if(clist.size()!=0){
            for(Case c:cList){
                if(c.AccountID !=null){
                    lstRelAccount.add(c.AccountID);
                    lstRelRecType.add(c.RecordTypeId);
                }
            }
            for(Case c:cListOld){
                mapCaseStatus.put(c.id,c.Status);
            }
            List<Account> relAccList= [select id,Name,Pending_MAL_Sync_Flag__c,Internal_DUNS__c,AutoDUNS__c,ownerid,Parent.OwnerID,Parent.Owner.Owner_Manager_Id__c from Account where Id IN:lstRelAccount];
            List<RecordType> relRecordTypeList= [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.LastModifiedDate, r.LastModifiedById, r.IsActive, r.Id, r.DeveloperName, r.Description, r.CreatedDate, r.CreatedById, r.BusinessProcessId From RecordType r where r.id IN:lstRelRecType];
            if(relAccList.size()!=0 && relRecordTypeList.size()!=0 ){
                for(Case c:cList){
                    for(RecordType r:relRecordTypeList){
                        if(c.RecordTypeId ==r.id){
                            mapCaseRecordTypeName.put(c.id,r.DeveloperName);
                        }
                    }
                }
                for(Case c:cList){
                    String recTypeName= mapCaseRecordTypeName.get(c.id);
                    if(recTypeName !=null){
                    if((Static_Data_Utilities__c.getInstance('Account_Update').Value__c !=null && recTypeName==Static_Data_Utilities__c.getInstance('Account_Update').Value__c) || ( Static_Data_Utilities__c.getInstance('New_Account').Value__c !=null && recTypeName==Static_Data_Utilities__c.getInstance('New_Account').Value__c) || (Static_Data_Utilities__c.getInstance('Account_Update_Submitted').Value__c !=null &&  recTypeName==Static_Data_Utilities__c.getInstance('Account_Update_Submitted').Value__c) || (Static_Data_Utilities__c.getInstance('D&B DUNS Update').Value__c !=null && recTypeName==Static_Data_Utilities__c.getInstance('D&B DUNS Update').Value__c)){
                        for(Account a:relAccList){
                            if(c.Status=='Approved'){
                                 if(c.AccountID==a.id){
                                     a.Pending_MAL_Sync_Flag__c=true;
                                     if(recTypeName=='New_Account' && mapCaseStatus.get(c.id) !='Approved'){
                                         a.Account_Status__c='Unassigned';
                                         a.Internal_DUNS__c=a.AutoDUNS__c; 
                                     }
                                     setAccountToUpdate.add(a);
                                 }
                             }else if(c.Status=='Declined - Invalid'){
                                if(recTypeName=='New_Account' && mapCaseStatus.get(c.id) !='Declined - Invalid'){
                                    if(c.AccountID==a.id){
                                        a.Account_Status__c='Invalid';
                                        setAccountToUpdate.add(a);
                                    }
                                 }
                            }
                             
                        }
                    }
                    }
                    
                }
            }
        }
        if(setAccountToUpdate.size()!=0){
            for(Account a:setAccountToUpdate){
                lstAccountToUpdate.add(a);
            }
        }
        Update lstAccountToUpdate;
    }
    /*@ Method Name: updateOwnerId
      Description: This Method used to update the Owner id when a New case of Alert record type is Created. .
      Return Type: NA.
      Input Param: NA.   
    */
    public void updateOwnerId(LIST<Case> cList){
        list<Id> lstRelAccount= new list<Id>();
        list<Id> lstRelRecType= new list<Id>();
        Map<id,String> mapCaseRecordTypeName= new Map<id,String>();
        if(clist.size()!=0){
            for(Case c:cList){
                if(c.AccountID !=null){
                    lstRelAccount.add(c.AccountID);
                    lstRelRecType.add(c.RecordTypeId);
                }
            }
            List<Account> relAccList= [select id,Name,Pending_MAL_Sync_Flag__c,Internal_DUNS__c,AutoDUNS__c,ownerid,Owner.Manager.id,Owner.Isactive,Owner.Manager.Name,Owner.Owner_Manager_Id__c,Owner.Manager_Active__c,Parent.OwnerID,Parent.Owner.Owner_Manager_Id__c,ParentID,Parent.Owner.ISActive,Parent.Owner.Manager_Active__c from Account where Id IN:lstRelAccount];
            
            List<MSD_Code__c> relMSDCodes=[select id,Name,Account__c from MSD_Code__c where Account__c IN: lstRelAccount AND MSD_Code_Status__c='Active'];
            Map<Id,Integer> mapAccountMSDCount= new Map<Id,Integer>();
            for(id accID:lstRelAccount){
                if(relMSDCodes.size()!=0){
                    for(MSD_Code__c msdCode:relMSDCodes){
                        if(msdCode.Account__c == accID){
                            if(mapAccountMSDCount.get(accID) !=null){
                                mapAccountMSDCount.put(accID,mapAccountMSDCount.get(accID)+1);    
                            }else{
                                mapAccountMSDCount.put(accID,1);    
                            }
                        }
                    }
                }
            }
           
            List<RecordType> relRecordTypeList= [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.LastModifiedDate, r.LastModifiedById, r.IsActive, r.Id, r.DeveloperName, r.Description, r.CreatedDate, r.CreatedById, r.BusinessProcessId From RecordType r where r.id IN:lstRelRecType];
            if(relAccList.size()!=0 && relRecordTypeList.size()!=0 ){
                for(Case c:cList){
                    for(RecordType r:relRecordTypeList){
                        if(c.RecordTypeId ==r.id){
                            mapCaseRecordTypeName.put(c.id,r.DeveloperName);
                        }
                    }
                 }
                for(Case c:cList){
                    String recTypeName= mapCaseRecordTypeName.get(c.id);
                    if(recTypeName=='Account_Alert'){
                        for(Account a:relAccList){
                            c.OwnerID=a.OwnerID;
                        }
                    }
                    if(recTypeName=='Account_Update'){
                        for(Account a:relAccList){
                            if(a.Owner.Isactive== true){
                                c.Account_Owner__c=a.OwnerID;
                            }
                            if(a.Owner.Owner_Manager_Id__c !=null && a.Owner.Owner_Manager_Id__c !='' && a.Owner.Manager_Active__c==True){
                                c.Account_Owner_Manager__c=a.Owner.Owner_Manager_Id__c;
                            }
                            if(a.Parent.OwnerID !=null && a.Parent.Owner.ISActive){
                                c.Parent_Account_Owner__c=a.Parent.OwnerID;
                            }
                            if(a.Parent.ID !=null){
                                c.Existing_Parent_Account__c=a.Parent.ID;
                            }
                            if(a.Parent.Owner.Owner_Manager_Id__c !=null && a.Parent.Owner.Manager_Active__c==True){
                                c.Parent_Account_Owner_Manager__c=a.Parent.Owner.Owner_Manager_Id__c;
                            }
                            if(mapAccountMSDCount.get(a.id)!=null){
                                c.Total_MSD__c=mapAccountMSDCount.get(a.id);
                            }else{
                                c.Total_MSD__c=0;
                            }
                        }
                    }
               }
           }
        }
    }
}