/*********************************************************************************
Class Name      : CPQ_AddCustomProductsController
Description     : This class is used as a controller class for CPQ_AddCustomProducts
                  page.
Created By      : Deepika Rawat 
Created Date    : 19-Nov-15  
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Deepika Rawat             19-Nov-15            Original Version 
*********************************************************************************/
public class CPQ_AddCustomProductsController{
    public Apttus_Config2__LineItem__c oLineItem{get; set;}
    public List<Apttus_Config2__LineItem__c> lstOtherLineItems{get; set;}
    public List<String> lstChargeTypes;
    public List<String> lstLOBCOPTeamMapping = new List<String>();
    public String sRelatedProduct{get;set;}
    public String sConfigRequestId;
    public String sLineItemId;
    public String sFlow;
    public String sProposalId;   
    public String sChargeType{get; set;}
    public String sCOPTeam{get; set;} 
    public Boolean bIsNew;
    public String sPriceUom;
    public String sBasePriceMethod;
    public String sPriceMethod;
    public String sPriceType;
    String sProductConfig;
    Set<String> setAdminProfiles = new Set<String>();
    Set<String> setAEProfiles = new Set<String>();
    Set<String> setSEProfiles = new Set<String>();
    Set<String> setDDProfiles = new Set<String>();
    String sUserProfileName;
    Map<String,String> mapLOBCOPTeam;
    Map<String,String> mapLOBCOPTeamAll;
    public Boolean showerror{get;set;}
    
    /*********************************************************************************
    Method Name    : CPQ_AddCustomProductsController
    Description    : Constructor method. This method checks if this is a New Line item 
                     creation request or edit existing line item request based on URL parameters
    Return Type    : 
    Parameter      : 
    *********************************************************************************/
    public CPQ_AddCustomProductsController(){
        sChargeType='';
        sCOPTeam='';
        String sQStage;
        sConfigRequestId = null;
        sUserProfileName= [Select name from profile where id=:userinfo.getProfileId()].name;
        List<Apttus_Proposal__Proposal__c> lstProposal;
        //Get custom setting Data
        CPQ_Settings__c cpqSetting = CPQ_Settings__c.getOrgDefaults();
        lstLOBCOPTeamMapping = cpqSetting.CustomProduct__c.Split(';');
        sPriceUom = cpqSetting.CustomProduct_PriceUom__c;
        sBasePriceMethod = cpqSetting.CustomProduct_BasePriceMethod__c;
        sPriceMethod = cpqSetting.CustomProduct_PriceMethod__c;
        sPriceType = cpqSetting.CustomProduct_PriceType__c;
        List<String> lstAEProfiles = cpqSetting.RogersAE__c.Split(';');
        List<String> lstAEProfiles2 = cpqSetting.RogersAE_2__c.Split(';');
        List<String> lstSEProfiles = cpqSetting.RogersSE__c.Split(';');
        List<String> lstDDProfiles = cpqSetting.RogersDealDesk__c.Split(';');
        List<String> lstSysAdminProfiles = cpqSetting.System_Administrator__c.Split(';');
        setAEProfiles.addAll(lstAEProfiles);
        setAEProfiles.addAll(lstAEProfiles2);
        setSEProfiles.addAll(lstSEProfiles);
        setDDProfiles.addAll(lstDDProfiles);
        setAdminProfiles.addAll(lstSysAdminProfiles);
        //Get URL Parameters
        sProductConfig = ApexPages.currentPage().getParameters().get('id');
        sConfigRequestId = ApexPages.currentPage().getParameters().get('configRequestId');
        sLineItemId = ApexPages.currentPage().getParameters().get('lineItemId');
        sFlow = ApexPages.currentPage().getParameters().get('flow');
        //If User is editing exiting custom product on Cart then get the record else create a new instance
        if(sLineItemId !=null){
            bIsNew = false;
            List<Apttus_Config2__LineItem__c> lstLineItem = [Select ProductPillar__c, LineofBusiness__c,CustomProductName__c, Apttus_Config2__Description__c,
                            RevenueType__c, Apttus_Config2__Quantity__c, Apttus_Config2__ChargeType__c, Apttus_Config2__BasePriceOverride__c,
                            Apttus_Config2__BillingFrequency__c, Apttus_Config2__SellingTerm__c, UnitofMeasure__c, Expiration_Date__c, Eligible_Device_s__c,Presentable_on_Quote__c,
                            MSDCode__c, LineCommit__c, FulfilmentType__c, ProspectSite__c, Apttus_Config2__ConfigurationId__c, TypeOfProduct__c, DataCenter__c, rd_Party_Services__c, RelatedProduct__c,COPTeam__c,CSOW_Required__c, OverageRate__c from Apttus_Config2__LineItem__c where id =:sLineItemId];
            //If this is an edit request pre populate class 'oLineItem' variable with the line item being edited; else initiate 'oLineItem' variable
            if(lstLineItem!=null && lstLineItem.size()>0){
                oLineItem = lstLineItem[0];
                Apttus_Config2__ProductConfiguration__c oProdConfig = [Select id,Apttus_Config2__BusinessObjectId__c from Apttus_Config2__ProductConfiguration__c where id =: oLineItem.Apttus_Config2__ConfigurationId__c];  
                sProductConfig=oLineItem.Apttus_Config2__ConfigurationId__c;
                sConfigRequestId = [select id from Apttus_Config2__TempObject__c where Apttus_Config2__ConfigurationId__c=:sProductConfig ORDER By createdDate ASC limit 1].id;
                sProposalId = oProdConfig.Apttus_Config2__BusinessObjectId__c;  
                lstProposal= [select id, RecordTypeId , Apttus_Proposal__Approval_Stage__c from Apttus_Proposal__Proposal__c where id=:sProposalId];
                sQStage = lstProposal[0].Apttus_Proposal__Approval_Stage__c;
                //Populate sFlow based on Quote's stage and User profile. This will be used to open correct flow based on user permission once user returns back to the Cart
                if(sFlow==null || sFlow==''){
                    if(sQStage == 'Draft' && (setAdminProfiles.contains(sUserProfileName) || setAEProfiles.contains(sUserProfileName) ||setSEProfiles.contains(sUserProfileName) || setDDProfiles.contains(sUserProfileName))){
                        sFlow='ngFlow';
                    }
                    else if((sQStage == 'Solution Under Review' || sQStage == 'Approval Required') && (setAdminProfiles.contains(sUserProfileName) || setAEProfiles.contains(sUserProfileName) ||setSEProfiles.contains(sUserProfileName) || setDDProfiles.contains(sUserProfileName))){
                        sFlow='repriceFlow';
                    }
                    else if(sQStage == 'In Review' && (setDDProfiles.contains(sUserProfileName) || setAdminProfiles.contains(sUserProfileName))){
                        sFlow='finalizeFlow';
                    }
                    else{
                        sFlow='onlyCloseFlow';
                    }

                }
            }
        }
        else{
            bIsNew=true;
            oLineItem  = new Apttus_Config2__LineItem__c();
            Apttus_Config2__ProductConfiguration__c oProdConfig = [Select id,Apttus_Config2__BusinessObjectId__c from Apttus_Config2__ProductConfiguration__c where id =: sProductConfig];       
            sProposalId = oProdConfig.Apttus_Config2__BusinessObjectId__c;  
            lstProposal= [select id, RecordTypeId , Apttus_Proposal__Approval_Stage__c from Apttus_Proposal__Proposal__c where id=:sProposalId];
            sQStage = lstProposal[0].Apttus_Proposal__Approval_Stage__c;
        } 
        
        //Allow user to Add custom products only if Stage is : Draft, Soln under review, In Review
        if(lstProposal.size()>0 && (setAdminProfiles.contains(sUserProfileName) || ((setAEProfiles.contains(sUserProfileName) ||setSEProfiles.contains(sUserProfileName) || setDDProfiles.contains(sUserProfileName)) && sQStage == 'Draft') || ((setDDProfiles.contains(sUserProfileName) || setSEProfiles.contains(sUserProfileName)) && sQStage == 'Solution Under Review') || (setDDProfiles.contains(sUserProfileName) && sQStage == 'In Review'))){
            showerror = false;
            //Get other Products added in the Cart
            lstOtherLineItems = [select id, Apttus_Config2__ProductId__c, Apttus_Config2__ProductId__r.Name from Apttus_Config2__LineItem__c where Apttus_Config2__ConfigurationId__c=:sProductConfig ];
            //Get Charge types available
            lstChargeTypes = cpqSetting.ChargeType__c.Split(';');       
        }
        else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CPQ_addCustomProductErrorMsg));
            showerror = true;
        }
    }

    /*********************************************************************************
    Method Name    : getOtherProducts
    Description    : Prepares Select Options for "Related Product"
    Return Type    : List<SelectOption>
    Parameter      : 
    *********************************************************************************/
    public List<SelectOption> getOtherProducts() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('None','None'));
        //If there are other products added in the cart, add them in the picklist options
        if(lstOtherLineItems!=null && lstOtherLineItems.size()>0){
            for(Apttus_Config2__LineItem__c item:lstOtherLineItems){
                options.add(new SelectOption(item.Apttus_Config2__ProductId__c,item.Apttus_Config2__ProductId__r.Name));
            }
        }
        return options;
    }
    /*********************************************************************************
    Method Name    : getChargeType
    Description    : Prepares Select Options for "ChargeType"
    Return Type    : List<SelectOption>
    Parameter      : 
    *********************************************************************************/
    public List<SelectOption> getChargeType() {
        List<SelectOption> options = new List<SelectOption>();
        //If there are Charge types present, add them in the picklist options
        if(lstChargeTypes!=null && lstChargeTypes.size()>0){
            for(String chageType:lstChargeTypes){
                options.add(new SelectOption(chageType,chageType));
            }
        }
        return options;
    }
    /*********************************************************************************
    Method Name    : getCOPTeam
    Description    : Prepares Select Options for "COP Team"
    Return Type    : List<SelectOption>
    Parameter      : 
    *********************************************************************************/
    public List<SelectOption> getCOPTeam() {
        String sLOBVal= oLineItem.LineofBusiness__c;
        mapLOBCOPTeam = new Map<String,String>();
        mapLOBCOPTeamAll = new Map<String,String>();
        // lstLOBCOPTeamMapping holds LOB and COP mapping. Loop through the map and display COP Team on Form.
        // Based on LOB selected by user on the form, default the COP team field
        if(lstLOBCOPTeamMapping!=null && lstLOBCOPTeamMapping.size()>0){
            for(String lobCOP:lstLOBCOPTeamMapping){
                List<String> lstlobCOP = lobCOP.Split(':');
                if(lstlobCOP!=null && lstlobCOP[0]!=null && lstlobCOP[1]!=null){
                    mapLOBCOPTeam.put(lstlobCOP[0],lstlobCOP[1]);
                    mapLOBCOPTeamAll.put(lstlobCOP[0],lstlobCOP[1]);
                }
            }
        }
        List<SelectOption> options = new List<SelectOption>();
        for(String sLOB:mapLOBCOPTeam.keyset()){
            if(sLOBVal!=null && sLOBVal!='' && sLOBVal==sLOB){
                options.add(new SelectOption(sLOB,mapLOBCOPTeam.get(sLOB)));
                mapLOBCOPTeam.remove(sLOB);
            }
        }
        for(String sLOB:mapLOBCOPTeam.keyset()){
            options.add(new SelectOption(sLOB,mapLOBCOPTeam.get(sLOB)));
        }
        
        return options;
    }
    /*********************************************************************************
    Method Name    : backToCart
    Description    : Returns back to Cart
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public pageReference backToCart(){
        pageReference pr = new PageReference(Site.getPathPrefix()+'/apex/Apttus_Config2__Cart?configRequestId='+sConfigRequestId+'&id='+sProductConfig+'&flow='+sFlow+'&launchState=cart#/cart');
        pr.setRedirect(false);
        return pr;
    }
    /*********************************************************************************
    Method Name    : addProduct
    Description    : Creates Custom Product and returns back to cart
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public pageReference addProduct(){
        try{
            //create or update custom product
            if(oLineItem!=null){
                //  If new custom product is being added populate fields                                                                                      
                if(bIsNew){
                    List<Apttus_Config2__PriceListItem__c> lstPriceListItem = new List<Apttus_Config2__PriceListItem__c>();
                    List<CPQ_CustomProducts__c> lstCustomProduct = CPQ_CustomProducts__c.getall().values();
                    Map<String, String> mapPillarCustomProduct = new Map<String, String>();
                    for(CPQ_CustomProducts__c cp: lstCustomProduct){
                        mapPillarCustomProduct.put(cp.Product_Pillar__c, cp.Custom_Product__c);
                    }
                    String sCustomProductCode = mapPillarCustomProduct.get(oLineItem.ProductPillar__c);
                    //If No Custom Product is present in the system for the Product pillar Selected throw error
                    if(sCustomProductCode!=null && sCustomProductCode!=''){
                        lstPriceListItem = [Select Apttus_Config2__ProductId__c, Apttus_Config2__PriceListId__c, Id from Apttus_Config2__PriceListItem__c where Apttus_Config2__ProductId__r.ProductCode = :sCustomProductCode];
                    }
                    else{
                         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CPQ_CustomProduct_ProductPillarError));
                         return null;
                    }
                    oLineItem.Apttus_Config2__ProductId__c = lstPriceListItem[0].Apttus_Config2__ProductId__c;
                    oLineItem.Apttus_Config2__PriceListId__c = lstPriceListItem[0].Apttus_Config2__PriceListId__c;
                    oLineItem.Apttus_Config2__PriceListItemId__c = lstPriceListItem[0].Id;                   
                    oLineItem.Apttus_Config2__BasePriceOverride__c = oLineItem.Apttus_Config2__BasePriceOverride__c;
                    oLineItem.Apttus_Config2__ItemSequence__c = 1;  
                    oLineItem.Apttus_Config2__SellingTerm__c= 1;  
                    oLineItem.Apttus_Config2__Term__c=1;
                    oLineItem.Apttus_Config2__ConfigurationId__c = sProductConfig;
                    //Populate Cart Line Number on the product being added based on the # of products already added in the cart
                    if(lstOtherLineItems!=null && lstOtherLineItems.size()>0){
                        oLineItem.Apttus_Config2__LineNumber__c = lstOtherLineItems.size()+1;
                        oLineItem.Apttus_config2__PrimaryLineNumber__c = lstOtherLineItems.size()+1;
                    }
                    else{
                        oLineItem.Apttus_Config2__LineNumber__c=1;
                        oLineItem.Apttus_config2__PrimaryLineNumber__c = 1;  
                    }
                    oLineItem.IsCustomProduct__c = true;
                    oLineItem.Apttus_Config2__IsPrimaryLine__c=true;
                    oLineItem.Apttus_Config2__PriceUom__c = sPriceUom;
                    oLineItem.Apttus_Config2__PriceMethod__c = sPriceMethod;
                    oLineItem.Apttus_Config2__BasePriceMethod__c = sBasePriceMethod;
                }
                //Upsert fields based on the values selected on the form
                 oLineItem.From_Custom_Product_Form__c =true;
                oLineItem.Apttus_Config2__BasePrice__c = oLineItem.Apttus_Config2__BasePriceOverride__c;
                oLineItem.Apttus_Config2__ListPrice__c = oLineItem.Apttus_Config2__BasePriceOverride__c;
                oLineItem.Apttus_Config2__AdjustmentAmount__c = null;
                oLineItem.Apttus_Config2__AdjustmentType__c = '';
                oLineItem.Apttus_Config2__PriceType__c = sChargeType;
                    oLineItem.Apttus_Config2__ChargeType__c = sChargeType;
                if(mapLOBCOPTeam!=null)
                    oLineItem.COPTeam__c = mapLOBCOPTeamAll.get(sCOPTeam);
                if(sRelatedProduct!=null && sRelatedProduct!='' && sRelatedProduct!='None' )
                    oLineItem.RelatedProduct__c=Id.valueof(sRelatedProduct);
                if(oLineItem.Apttus_Config2__Quantity__c==0 || oLineItem.Apttus_Config2__Quantity__c==null )
                    oLineItem.Apttus_Config2__Quantity__c=1;
                oLineItem.Apttus_Config2__BaseExtendedPrice__c=oLineItem.Apttus_Config2__BasePriceOverride__c * oLineItem.Apttus_Config2__Quantity__c ;
                oLineItem.Apttus_Config2__ExtendedPrice__c=oLineItem.Apttus_Config2__BasePriceOverride__c * oLineItem.Apttus_Config2__Quantity__c ;
                oLineItem.Apttus_Config2__NetPrice__c=oLineItem.Apttus_Config2__BasePriceOverride__c * oLineItem.Apttus_Config2__Quantity__c;
                oLineItem.Apttus_Config2__NetUnitPrice__c=oLineItem.Apttus_Config2__BasePriceOverride__c * oLineItem.Apttus_Config2__Quantity__c;           
                upsert oLineItem;
            }
            //Reprice Cart
            Apttus_CpqApi.CPQ.UpdatePriceRequestDO objUpdatePriceRequestDO = new Apttus_CpqApi.CPQ.UpdatePriceRequestDO();
            objUpdatePriceRequestDO.CartId = sProductConfig;
            Apttus_CpqApi.CPQ.UpdatePriceResponseDO result = Apttus_CpqApi.CPQWebService.updatePriceForCart(objUpdatePriceRequestDO);
            
            
            //Navigate back to Cart
            pageReference pr = new PageReference(Site.getPathPrefix()+'/apex/Apttus_Config2__Cart?configRequestId='+sConfigRequestId+'&id='+sProductConfig+'&flow='+sFlow+'&launchState=cart#/cart');
            pr.setRedirect(false);
            return pr;
        }catch (exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
            return null;
        }
    }
}