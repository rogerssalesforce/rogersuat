/*
========================================================================
Date                        Name                        Status
March 2015         Aakanksha Patel            Modified(For TAG)
========================================================================
*/

public with sharing class shift_view_spr_extension {

    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();       
    private static CPQ_Settings__c CPQ_cs = CPQ_Settings__c.getInstance();
    
    public SPR__c theSPR;

    public shift_view_spr_extension(ApexPages.StandardController controller) {
        theSPR = (SPR__c) controller.getRecord();
    }

    public list<SPR_Note__c> gettheSPRNoteList() {
        String query = 'select ';
        for(Schema.FieldSetMember f : SObjectType.SPR_Note__c.FieldSets.SPR_Note_full_view.getFields()) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id from SPR_Note__c where SPR__c =\'' + theSPR.Id + '\' order by CreatedDate desc';
        
        return Database.query(query);        
    }
    
    static testMethod void testshift_get_spr_owner_email() {

        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;

        CPQ_cs.DefaultOpportunityPB__c = 'CPQ Temp PriceBook';
        CPQ_cs.DefaultOpportunityRecordType__c = 'Rogers EBU - New';
        insert CPQ_cs;

        // create user
        Profile p = [select id from profile where name='Standard User'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com',
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          localesidkey='en_US', profileid = p.Id,
          timezonesidkey='America/Los_Angeles', username='shifttest1@test.com');
        insert u;

        // create dummy account
        Account a = new Account(Name='shift test account', ParentId = Null, Account_Status__c = 'Assigned',BillingPostalCode = 'A1A 1A1');
        a.ParentID= null;
        insert a;
        
        // create dummy opp
        Opportunity o = new Opportunity(Name='shift test opportunity', AccountId=a.Id, CloseDate=Date.today(), Amount = 500, StageName = 'Dormant');
        o.Unified_Comm_Collaboration_Estimated__c =20;
        insert o;
        
        // create test quote
        Quote__c q = new Quote__c(Opportunity_Name__c=o.id);
        insert q; 
        
        // create SPR
        SPR__c s = new SPR__c(Account__c = a.Id, Opportunity__c = o.Id, Requested_By__c = u.Id, Global_Recipient_1__c = u.Id, Global_Recipient_2__c = u.Id, Global_Recipient_3__c = u.Id, 
                                Global_Recipient_4__c = u.Id, Global_Recipient_5__c = u.Id);
        insert s;
        
        // get SPR owner's email. current user is the same SPR owner.
        string theOwnerEmail = [select Email from User where Id =: UserInfo.getUserId()].Email;
        
        // create SPR note       
        SPR_Note__c sn = new SPR_Note__c(SPR__c = s.Id, Body__c = 'shift 123');
        insert sn;  

        // add page reference        
        PageReference pageRef = Page.shift_view_spr;
        Test.setCurrentPage(pageRef);
        
        // create standard controller
        ApexPages.StandardController sc = new ApexPages.StandardController(s);
        
        // pass controller to the extension
        shift_view_spr_extension ext = new shift_view_spr_extension(sc);
        
        system.assertEquals(1, ext.gettheSPRNoteList().size());
        system.assertEquals('shift 123', ext.gettheSPRNoteList().get(0).Body__c);
    }    
}