global class ScheduleDiscountApproval implements Schedulable{
    global void execute(SchedulableContext sc) { 
        List<Pending_Approval_Submission__c> pendingApprovals = null;
        Map<Id, Pending_Approval_Submission__c> pendingApprovalMap = new Map<Id, Pending_Approval_Submission__c>();
        List<Id> NSAApproved = new List<Id>(); 
        List<Id> quoteIds = new List<Id>();
        
        
        // Let's grab all of the quotes that need to be approved - this includes ones that need to go to Discount Approval and the ones that don't
        try{
            pendingApprovals = [SELECT Id, Next_Approver__c, Quote_to_Approve__c FROM Pending_Approval_Submission__c WHERE Processed__c = false limit 1];
            system.debug('==============>pendingApprovals=size'+pendingApprovals.size());
            system.debug('==============>pendingApprovals'+pendingApprovals);
        }catch(Exception ex){ 
        
        }
        
      String schedTaskUser = '005a00000093Q5x';
    try{
            schedTaskUser = [Select Id, value__c FROM Custom_Property__c WHERE Name = 'schedTaskUser'].value__c;
        }catch(Exception ex){
        }
         
         if (pendingApprovals != null){ 
            // Grab all of the Quote Ids as we will need to query the quotes to see which don't need to go to the next step.
            for (Pending_Approval_Submission__c pending : pendingApprovals){ 
                pendingApprovalMap.put(pending.Quote_to_Approve__c, pending);
                quoteIds.add(pending.Quote_to_Approve__c);
            }
            
            // Any quotes that do not need to go further in the approval process should be set as approved.
            try{
                List<Quote> quotesToApprove = null;
                try{ 
                    if (!quoteIds.isEmpty())
                        quotesToApprove = [SELECT Id, In_Enterprise_Approval__c, Discount_Approval__c, Approval_Process_Started__c, Discount_Approval_Status__c FROM Quote WHERE Id IN :quoteIds AND Discount = 0 AND Empty_List_Price__c = 0 AND Inconsistent_Pricing__c = 0];
                        System.debug('Quotes to Approve: ' + quotesToApprove);
                }catch(Exception e2){
                    
                }
                
                
                // We need to update the quotes that don't need the discount approval process
                List<Id> removeQuotes = new List<Id>();
                if (quotesToApprove != null){
                    for (Quote q : quotesToApprove){
                        q.Discount_Approval__c = true;
                        q.Discount_Approval_Status__c = 'Final Approved';
                        q.ExpirationDate = Date.today() + 60;
                        q.In_Enterprise_Approval__c = false;
                        q.Approval_Process_Started__c = false;
                        q.NSA_Approval_Complete__c = false;
                        removeQuotes.add(q.Id);
                    }
                     
                    UPDATE quotesToApprove;
                
                    // Remove the pending approvals that we need to process
                    System.debug('Time to remove the quotes!!!');
                    for (Id qId : removeQuotes){
                        if (pendingApprovalMap.get(qId) != null){
                            NSAApproved.add(pendingApprovalMap.get(qId).Id);
                            if(!Test.isRunningTest())
                                pendingApprovalMap.remove(qId);
                        }
                    }
                 }     
           }catch(Exception ex){
                
            }
        
            try{
                System.debug('These need to be deleted: ' + NSAApproved);
                List<Pending_Approval_Submission__c> alreadyApproved = [SELECT Id, Next_Approver__c, Quote_to_Approve__c FROM Pending_Approval_Submission__c WHERE Id IN :NSAApproved];
                DELETE alreadyApproved;
            }catch(Exception ex){ 
                
            }
         }
         
         System.debug('Pending to send: ' + pendingApprovalMap.values());
         
         // If we have any left we need to send them for discount approval.
        if (pendingApprovalMap != null && !pendingApprovalMap.isEmpty()){
           List<Id> quotesWithIssues = new List<Id>();
         
            for (Pending_Approval_Submission__c pending : pendingApprovalMap.values()){ 
                Approval.Processsubmitrequest discountApproval= new Approval.ProcessSubmitRequest();
                discountApproval.setObjectId(pending.Quote_to_Approve__c);
                discountApproval.setComments('Autosubmit for Discount Approval');
                discountApproval.setNextApproverIds(new Id[] {pending.Next_Approver__c});
                try{
                  Approval.ProcessResult approvalResult = Approval.process(discountApproval);
                  pending.processed__c = true;
                }catch(Exception ex){
                  // Add Quote to list of quotes that are bad. 
                  quotesWithIssues.add(pending.Quote_to_Approve__c);
                  
                  
                }
                
                
            }
          if (!quotesWithIssues.isEmpty()){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setReplyTo('jose.camacho@rci.rogers.com');
                    List<String> toAddresses = new List<String>();
                    toAddresses.add('kevin.deslauriers@stratus360.com');
                    mail.setSenderDisplayName('Salesforce Administrator');
                    
                    mail.setSubject('Issue with Schedule Task');
                    String body = '';
                    body += '<p>The following quotes need to be reviewed as they are not being process in the scheduled task: <br/>';
                    for (Id qId : quotesWithIssues){
                      body += qId + '<br/>'; 
                    }
                    body += '</p>';
                    mail.setHtmlBody(body);
                    mail.setSaveAsActivity(false);
                    
                    mail.setToAddresses(toAddresses);
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
          }
          if(!Test.isRunningTest())
            UPDATE pendingApprovalMap.values(); 
        }
        
        
        try{
            pendingApprovals = [SELECT Id, Next_Approver__c, Quote_to_Approve__c FROM Pending_Approval_Submission__c WHERE Processed__c = true];
            if (pendingApprovals != null)
                DELETE pendingApprovals;
        }catch(Exception ex){
        
        }
         
        
        
        boolean successSched = false;
        // 3 tries -- add logic to do loop for 3 times and wait to end job, before failing
        for (Integer k = 0; k < 3; k++) {
            if (!successSched) {
                // end job that already run
                // begin
                String soqlString = 'select Id, CronExpression, EndTime, NextFireTime, OwnerId, PreviousFireTime, StartTime, State, TimesTriggered, TimeZoneSidKey from CronTrigger where State = \'Waiting\' and TimesTriggered = 1 and OwnerID = \'' + schedTaskUser + '\' '; 
                
                System.debug('----soqlString----'+soqlString);
                
                List<CronTrigger> listCronTrigger = Database.query(soqlString);
                
                System.debug('Number of jobs: '+listCronTrigger.size());
                If (listCronTrigger.size() > 0) 
                {
                   for (Integer i = 0; i < listCronTrigger.size(); i++)
                   { 
                        if ((listCronTrigger[i].EndTime)==null) {
                            if ((listCronTrigger[i].Id)<>null) {
                                System.abortJob(listCronTrigger[i].Id);
                                System.debug('Job details (Terminated):'+String.valueOf(listCronTrigger[i]));
                            } else
                                {}
                        } else {
                            System.debug('Job details (Not Terminated):'+String.valueOf(listCronTrigger[i]));
                        }
                   } 
                }
                // end     
                
                
                System.debug('-----------ScheduleBatchSummaryTasks---------');
                String hour, min;
                
                if ((String.valueOf(Datetime.now().minute() + 1)).equals('60')){
                    min = '00';
                    if (String.valueOf(Datetime.now().hour()+1).equals('24')){
                        hour = '01';
                    }else{
                        hour = String.valueOf(Datetime.now().hour()+1);             
                    }
                }else{
                    min = String.valueOf(Datetime.now().minute() + 1);
                    hour = String.valueOf(Datetime.now().hour());
                }
                
                String ss = String.valueOf(Datetime.now().second()); 
        
                String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
                System.debug('-----------nextFireTime---------' + nextFireTime);
                
                try {
                    ScheduleDiscountApproval s = new ScheduleDiscountApproval ();
                    System.schedule('Job Started At ' + String.valueOf(Datetime.now()), nextFireTime, s); 
                    successSched = true;
                } catch(Exception ex2){
                    system.debug('some problem2:' + ex2 + ' -- try #' + k);     
                    
                    if (!Test.isRunningTest()) {
                        Integer start = System.Now().millisecond();
                        // delay for 10 seconds before trying
                        while(System.Now().millisecond()< start+10000){ 
                            try {
                            } catch (Exception e2) {
                                start = System.Now().millisecond();
                            }           
                        }
                    }
                }
            }
        }// end for 3 tries
    }
    
}