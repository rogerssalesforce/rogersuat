/*********************************************************************************
Class Name      : CPQ_Test_CreateLegalNameChangeController
Description     : This class a test class for CPQ_CreateLegalNameChangeController. 
Created By      : 
Created Date    : 24-Sep-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Akshay Ahluwalia          23-Sep-15             Original version
Alina Balan               03-Feb-2016           Fixed test classes
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest (seeAllData=False) 
private class CPQ_Test_CreateLegalNameChangeController{
    static User TestAEUser;
    static Account oAccount;
    static Account oAccountRestricted;
    static Opportunity oOpportunity;
    static Apttus_Proposal__Proposal__c oQuote;
    static Attachment attachmentQuote;
    static Apttus_Config2__PriceList__c oPriceList ;
    /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testMethod void createTestData() {
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');      
        system.runAs(TestAEUser){
            //Create PriceList
            oPriceList = CPQ_Test_Setup.newPriceList();
            insert oPriceList ;
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
            
            oAccountRestricted = CPQ_Test_Setup.newAccount('Test Restricted Account');
            oAccountRestricted.Restricted__c = 'Yes';
            insert oAccountRestricted;
        }
    }  
    /*********************************************************************************
    Method Name    : testSelectPrimaryQuoteDoc
    Description    : Method to test user is able to select Primary doc
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testSelectPrimaryQuoteDoc(){
        createTestData();
        test.startTest();
        system.runAs(TestAEUser) {
            PageReference pageRef = Page.CPQ_LegalNameChange;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Id',oAccount.id);
            CPQ_CreateLegalNameChangeController controller = new CPQ_CreateLegalNameChangeController();
            //Test.setMock(HttpCalloutMock.class, new CPQ_CreateLegalNameCalloutMock());
            controller.sResBody = '{"size":80,"totalSize":80,"done":true,"queryLocator":null,"entityTypeName":"CustomField","records":[{"attributes":{"type":"CustomField","url":"/services/data/v33.0/tooling/sobjects/CustomField/00N1b000000Sv4fEAC"},"Id":"00N1b000000Sv4fEAC","DeveloperName":"Status","FullName":"APTS_Agreement__c.Apttus__Status__c"},' + 
                                  '{"attributes":{"type":"CustomField","url":"/services/data/v33.0/tooling/sobjects/CustomField/00N1b000000Sv4eEAC"},"Id":"00N1b000000Sv4eEAC","DeveloperName":"Status_Category","FullName":"APTS_Agreement__c.Apttus__Status_Category__c"},' + 
                                  '{"attributes":{"type":"CustomField","url":"/services/data/v33.0/tooling/sobjects/CustomField/00N1b000000Sv3TEAS"},"Id":"00N1b000000Sv3TEAS","DeveloperName":"Account","FullName":"APTS_Agreement__c.Apttus__Account__c"}]}';
            PageReference pg = controller.CreateLegalNameChangeAgreementRecord();
            system.assertNotEquals(pg, null);
        }
        test.stopTest();
    }
    
    /*********************************************************************************
    Method Name    : testRestricted Account
    Description    : Method to test user is able to select Primary doc
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testRestAccount(){
        createTestData();
        test.startTest();
        system.runAs(TestAEUser){
         PageReference pageRef = Page.CPQ_LegalNameChange;
         Test.setCurrentPage(pageRef);
         ApexPages.currentPage().getParameters().put('Id',oAccountRestricted.id);
         CPQ_CreateLegalNameChangeController controller = new CPQ_CreateLegalNameChangeController();
         PageReference pg = controller.CreateLegalNameChangeAgreementRecord();
         system.assertEquals(pg, null);
        }
    }
    
}