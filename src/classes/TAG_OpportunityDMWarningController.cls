/*Class Name :TAG_OpportunityDMWarningController.
 *Description : Controller Class for showing 'This Account is not Marshalled' Error Message  
                on OpportunityDetailPage
 *Created By : Jayavardhan Raju.
 *Created Date :13/01/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
public class TAG_OpportunityDMWarningController {

     public  ApexPages.StandardController con { get; set;}  
     public String accountType {get;set;}
     public Boolean displayMessage {get;set;}
     public list<Opportunity> oppList {get;set;}
     
    /**
    *Constructor
    */
    public TAG_OpportunityDMWarningController(ApexPages.StandardController controller) {
       con = controller;
       displayMessage = false;       
       String oppId = con.getId();
       list<Opportunity> oppList= [select id,Account.RecordType.Name from Opportunity where Id =:oppId];
       if(oppList!=null && oppList.size()>0){
            accountType = oppList[0].Account.RecordType.Name;
            
            if(accountType!=null && accountType.equalsIgnoreCase('New Account') ){
                displayMessage = true;
            }
            
            
       }       
    }    
}