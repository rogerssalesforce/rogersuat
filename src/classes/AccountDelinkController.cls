/*Class Name :AccountDelinkController .
 *Description : Controller Class for AccountDeLinkingTemplate.
 *Created By : Deepika Rawat.
 *Created Date :05/12/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*/
public class AccountDelinkController {
    //Variable Declaration
    public String searchString{get;set;}
    public Boolean plusRec{get;set;}
    public list<Account> tempAccountList{get;set;}
    public Account aParentObj {get;set;}
    public Account aParentRec {get;set;}//Parent Account selected
    public List<Account> listChildAccount{get;set;}//List of All child Accounts
    public List<Case> listCaseObj{get; set;}// List of Case already existing for selected parent account with record type 'Account Delink'
    public List<Mass_Update_Details__c> lstMassUpdate{get;set;}// List of related Mass Update Details
    public List<AccountWrapper> lstAccselected {get; set;}// To display child accounts already selected to be delinked
    public Case CaseObj{get;set;}//Case object
    public String selectedCaseID {get;set;}
    public String selectedAccountID {get;set;}
    public Map<id, Mass_Update_Details__c > mapMassUpdate = new Map<id, Mass_Update_Details__c >();// Map of Child Account Id and related Mass update Detail
    public boolean showTableCaseExits{get;set;}// to show section when Case Exits
    public boolean showTableNoCase{get;set;}// to show section when new case is to be created
    public boolean showMessage{get;set;}// to show message when no child Account exits
    public boolean OnNew{get;set;} // To check if page is for New Case
    public boolean OnEdit{get;set;} // To check if page is for editting
    public boolean OnView{get;set;} // To check if page is for View
    public boolean showEditonDetail{get;set;}// To show edit button on certain conditions
    public boolean showDeleteonDetail{get;set;}// To show delete button on certain conditions
    public boolean showApprovalButtons{get;set;}//Show approval buttons Approve and Reject to approvers and sys admin
    public boolean shwSubmitForApproval{get;set;}//Show Submit for Approval button to Case Owner, Sys admin and CRM admin
    public boolean shwSubmitForApprovalMsg{get;set;}//Show Submit for Approval button to Case Owner, Sys admin and CRM admin
    public String location{get;set;}//store the link location
    public String CaseId;
    public String ActiveMSDParent{get;set;}
    public ID caseDelinkRecordTypeId = [select Id from recordType where name='Account Delink' and sObjecttype ='Case'].id;
    public String profileId = UserInfo.getProfileId();    
    public Profile profile = [select name from profile where id= :profileId];
    //Variables for pagination
    public ApexPages.StandardSetController con{get; set;}
    public List<AccountWrapper> lstAccWrprNoCaseDisplay {get;set;}
    public integer selectedAccSize{get;set;}
    public Set<Account> selectedAccounts {get;set;}
    public String sortDirection = 'ASC';
    public String sortExp = 'name';
    
    private boolean caseExists() {
        List<Case> cases = [select id, Mass_Update_Parent_Account__c,Notes__c,subject, CreatedById, CaseNumber,OwnerID, status from Case where id=:CaseId];
        if (cases.size() > 0) {
            return true;
        }
        return false;
    }
    
    public String sortExpression{
        get{ return sortExp; }
        set{ //if the column is clicked on then switch between Ascending and Descending modes
            if (value == sortExp)
                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
            else
                sortDirection = 'ASC';
                sortExp = value;
        }
    }
    public String getSortDirection(){
        //if column is not selected 
        if (sortExpression == null || sortExpression == '')
            return 'ASC';
        else
            return sortDirection;
    }
    public void setSortDirection(String value){  
        sortDirection = value;
    }   
    public string sortFullExp = sortExpression  + ' ' + sortDirection;
    public AccountDelinkController(ApexPages.StandardController controller) {
        boolean error = false;
        OnNew = false;
        OnEdit=false;
        OnView=false;
        CaseId = ApexPages.currentPage().getParameters().get('id');
        if(CaseId!='' && CaseId!= null && CaseId.startswith('500')){      
            try {
                listCaseObj= [select id, Mass_Update_Parent_Account__c,Notes__c,subject, CreatedById, CaseNumber,OwnerID, status from Case where id=:CaseId];
            }
            catch (System.QueryException e) {
                error = true;  
            }
            if (listCaseObj.size() < 1) {
                    error = true; 
            }           
        }
        if(!error) {
            this.selectedAccounts = new Set<Account>();
            selectedAccSize = 0;
            aParentObj = new Account(); 
            CaseObj = new Case();
            aParentRec= new Account();
            searchString='';
            location = ApexPages.currentPage().getParameters().get('loc');
            aParentObj.ParentId = ApexPages.currentPage().getParameters().get('parentAcc');
            String onEditStr = ApexPages.currentPage().getParameters().get('OnEdit');
            String onViewStr = ApexPages.currentPage().getParameters().get('OnView');
            List<account> accountList = [SELECT Name FROM Account LIMIT 20];
            con = new ApexPages.StandardSetController(accountList);
            con.setPageSize(20);
            if(onEditStr =='true'){
                OnEdit=true;  
            }
            else if(onViewStr =='True'){
                OnView=true; 
            }
            else if(onEditStr !='true' && onViewStr !='True'){
                OnNew = true;   
            }
            searchChildAccount();
        }
    } 
    //Method to add selected child Accounts to a list to track the records selects on pagination
    public PageReference doSelectedAccounts(){
        for(AccountWrapper aWrap: lstAccWrprNoCaseDisplay){
            if(aWrap.selected == true){
                this.selectedAccounts.add(aWrap.aObj);
            }
            else if(aWrap.selected == false){
                this.selectedAccounts.remove(aWrap.aObj);
            }
        }
        selectedAccSize = selectedAccounts.size();
        return null;
    }
    //Method to return list of available  Child Accounts on NEW page
     public List<AccountWrapper> lstAccWrprDispaly{  
        get{  
            if(con != null){
                lstAccWrprNoCaseDisplay  = new List<AccountWrapper>();
                for(Account childAcc :(List<Account>)con.getRecords()){
                    AccountWrapper awObj = new AccountWrapper();
                    awObj.aObj = childAcc;
                    if(childAcc.MSD_Codes__r.size()>0){
                        awObj.ActiveMSD = 'Yes';
                    }
                    else
                    awObj.ActiveMSD = 'No';
                    if(selectedAccounts!=null && selectedAccounts.size()>0 && selectedAccounts.contains(childAcc)){
                        awObj.selected= true;
                    }
                    else
                    awObj.selected= false;
                    awObj.showCheckbox=true;
                    lstAccWrprNoCaseDisplay.add(awObj);  
                }
                return lstAccWrprNoCaseDisplay;  
            }else{
                return null ;  
            }
        }  
        set;
     } 
    //Method to return list of availble  Child Accounts on EDIT page
     public List<AccountWrapper> lstAccNoCaseDispaly{  
        get{  
            if(con != null){
                lstAccWrprNoCaseDisplay  = new List<AccountWrapper>();
                for(Account childAcc :(List<Account>)con.getRecords()){
                Mass_Update_Details__c mObj= mapMassUpdate.get(childAcc.id);
                if(mObj == null){
                    AccountWrapper awObj = new AccountWrapper();
                    awObj.aObj = childAcc;
                    if(childAcc.MSD_Codes__r.size()>0){
                        awObj.ActiveMSD = 'Yes';
                    }
                    else
                        awObj.ActiveMSD = 'No';
                    if(selectedAccounts!=null && selectedAccounts.size()>0 && selectedAccounts.contains(childAcc)){
                        awObj.selected= true;
                    }
                    else
                    awObj.selected= false;
                    awObj.showCheckbox=true;
                    lstAccWrprNoCaseDisplay.add(awObj);  
                }
                }
                return lstAccWrprNoCaseDisplay;  
            }
            else{
                return null ;  
            }
        }  
        set;
     } 
    public PageReference searchChildAccount(){
    
    PageReference page;
        if (!caseExists() && !OnNew) {
            return new PageReference('/apex/RecordDeleted');
        }
        
        listCaseObj=new List<Case>();
        plusRec=false;
        lstAccselected = new List<AccountWrapper>();
        listChildAccount  = new List<Account>();
        showTableCaseExits =false;
        showTableNoCase = false;
        if(CaseId!='' && CaseId!= null && CaseId.startswith('500')){
            listCaseObj= [select id, Mass_Update_Parent_Account__c,Notes__c,subject, CreatedById, CaseNumber,OwnerID, status from Case where id=:CaseId];
        }
        if(aParentObj.ParentId != null || aParentObj.ParentId!= ''){
            try{
                aParentRec= [select id, OwnerID,Customer_Status__c,Owner.Channel__c,Account_Status__c,Owner.Name,Owner.Manager.isActive, Owner.ManagerID,Internal_Duns__c,Trade_Name__c, name, (SELECT MSD_Code_Status__c FROM MSD_Codes__r where MSD_Code_Status__c='Active' limit 1) from Account where id =: aParentObj.ParentId];
                if(aParentRec.MSD_Codes__r.size()>0){
                    ActiveMSDParent = 'Yes';
                }
                else
                    ActiveMSDParent = 'No';
                
                if(aParentRec.Account_Status__c == 'Pending' || aParentRec.Account_Status__c == 'New'){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'This is a '+ aParentRec.Account_Status__c +' account.'));
                
                }
                else{
                    if(OnView==true && listCaseObj.isEmpty()){
                        listCaseObj = [select id, Mass_Update_Parent_Account__c,Notes__c, Mass_Update_Parent_Account__r.ownerId, subject,CreatedById, CaseNumber,OwnerID, status from Case where Mass_Update_Parent_Account__c=:aParentRec.id and RecordtypeId=:caseDelinkRecordTypeId ];
                    }
                    else if((OnNew==true || OnEdit==true) && listCaseObj.isEmpty() ){
                    listCaseObj = [select id, Mass_Update_Parent_Account__c,Notes__c, subject, CreatedById,Mass_Update_Parent_Account__r.ownerId, CaseNumber,OwnerID, status from Case where Mass_Update_Parent_Account__c=:aParentRec.id and RecordtypeId=:caseDelinkRecordTypeId and status!='Approved' and status!='Rejected' ];
                    }
                    sortFullExp = sortExpression  + ' ' + sortDirection;
                    string sSoqlQuery;
                    string sSoqlQueryCheck;
                    // This is for NEW Case. listCaseObj is empty.
                    if(listCaseObj.size()==0){
                        //Query to select all the child accounts of the selected parent account
                        if(searchString!=null || searchString!=''){
                            sSoqlQuery = 'select id, OwnerID,Customer_Status__c,Owner.Channel__c,Owner.Name, Owner.ManagerID,Internal_Duns__c,Trade_Name__c, name, (SELECT MSD_Code_Status__c FROM MSD_Codes__r where MSD_Code_Status__c=\'Active\' limit 1) from Account where parentid =\''+aParentObj.ParentId +'\'  and ( Name LIKE \'%'+String.escapeSingleQuotes(searchString)+'%\' OR Internal_Duns__c LIKE  \'%'+String.escapeSingleQuotes(searchString)+'%\' OR Trade_Name__c LIKE  \'%'+String.escapeSingleQuotes(searchString)+'%\' ) Order By ' + sortFullExp +' limit 500';
                        }
                        if(searchString==null || searchString==''){
                            sSoqlQuery = 'select id, OwnerID,Customer_Status__c,Owner.Channel__c,Owner.Name, Owner.ManagerID,Internal_Duns__c,Trade_Name__c, name, (SELECT MSD_Code_Status__c FROM MSD_Codes__r where MSD_Code_Status__c=\'Active\' limit 1) from Account where parentid =\''+aParentObj.ParentId +'\' Order By ' + sortFullExp +' limit 500';
                        }
                        
                        listChildAccount = Database.query(sSoqlQuery);
                        if(listChildAccount.size()>0 ){
                            showTableNoCase = true;
                            con = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery)); 
                            con.setPageSize(50);
                            showMessage = false;
                        }
                        else{
                            //When no child account exists for the selected Parent Account show "No child exists" message to the user
                            showMessage=true;
                        }
                        if(!(listChildAccount.size()<500)){
                            if(searchString!=null || searchString!=''){
                                sSoqlQueryCheck = 'select id, OwnerID,Customer_Status__c,Owner.Channel__c,Owner.Name, Owner.ManagerID,Internal_Duns__c,Trade_Name__c, name, (SELECT MSD_Code_Status__c FROM MSD_Codes__r where MSD_Code_Status__c=\'Active\' limit 1) from Account where parentid =\''+aParentObj.ParentId+'\'  and ( Name LIKE \'%'+String.escapeSingleQuotes(searchString)+'%\' OR Internal_Duns__c LIKE  \'%'+String.escapeSingleQuotes(searchString)+'%\' OR Trade_Name__c LIKE  \'%'+String.escapeSingleQuotes(searchString)+'%\' ) Order By ' + sortFullExp +' limit 501';
                            }
                            if(searchString==null || searchString==''){
                                sSoqlQueryCheck = 'select id, OwnerID,Customer_Status__c,Owner.Channel__c,Owner.Name, Owner.ManagerID,Internal_Duns__c,Trade_Name__c, name, (SELECT MSD_Code_Status__c FROM MSD_Codes__r where MSD_Code_Status__c=\'Active\' limit 1) from Account where parentid =\''+aParentObj.ParentId +'\' Order By ' + sortFullExp +' limit 501';
                            }
                            
                            List<Account> checkRecordsList=Database.query(sSoqlQueryCheck);
                            if(checkRecordsList.size()>500 ){
                                plusRec = true;
                                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Warning,'More than 500 records retrieved. Please refine your search criteria.'));
                            }
                            else{
                                plusRec =false;
                            }
                        }
                        
                    }
                    //This is for Case EDIT or Case VIEW
                    else{
                        CaseObj = listCaseObj[0];
                         if(profile.name=='System Administrator' && CaseObj.Status!='New'  ){
                            showDeleteonDetail = true;
                        }
                        if(CaseObj.Status=='New' && (profile.name=='System Administrator' || profile.name=='EBU - Rogers CRM Admin' || CaseObj.OwnerId == UserInfo.getUserId())){
                            showEditonDetail=true;
                        }
                        else if(CaseObj.Status=='In Progress' && (profile.name=='System Administrator' || profile.name=='EBU - Rogers CRM Admin')){
                            showEditonDetail=true;
                        }

                        if(CaseObj.Status=='New' && (profile.name=='System Administrator' || profile.name=='EBU - Rogers CRM Admin' || CaseObj.OwnerId == UserInfo.getUserId())){
                            shwSubmitForApproval=true;
                        }
                        showTableCaseExits = true;
                        showMessage = false;
                        List<Id> listMassUpdate = new List<Id >();
                        
                        if(searchString==null || searchString==''){
                        sSoqlQuery ='select id, OwnerID,Customer_Status__c,Owner.Channel__c,Owner.Name, Owner.ManagerID,Internal_Duns__c,Trade_Name__c, name, (SELECT MSD_Code_Status__c FROM MSD_Codes__r where MSD_Code_Status__c=\'Active\' limit 1) from Account where parentid =\''+aParentObj.ParentId +'\'Order By ' + sortFullExp +' limit 500' ;
                        }
                        else{
                        sSoqlQuery ='select id, OwnerID,Customer_Status__c,Owner.Channel__c,Owner.Name, Owner.ManagerID,Internal_Duns__c,Trade_Name__c, name, (SELECT MSD_Code_Status__c FROM MSD_Codes__r where MSD_Code_Status__c=\'Active\' limit 1) from Account where parentid =\''+aParentObj.ParentId+'\'  and ( Name LIKE \'%'+String.escapeSingleQuotes(searchString)+'%\' OR Internal_Duns__c LIKE  \'%'+String.escapeSingleQuotes(searchString)+'%\' OR Trade_Name__c LIKE  \'%'+String.escapeSingleQuotes(searchString)+'%\' ) Order By ' + sortFullExp +' limit 500' ;
                        }

                        listChildAccount = Database.query(sSoqlQuery);
                        lstMassUpdate = [select id, Case__c, Child_Account__c from Mass_Update_Details__c where Case__c =:listCaseObj[0].id];
                        if(lstMassUpdate.size()>0){
                            for(Mass_Update_Details__c m:lstMassUpdate){
                                mapMassUpdate.put(m.Child_Account__c,m);
                                listMassUpdate.add(m.Child_Account__c);
                            }
                        }  
                       
                        
                        List<Account> tempAccList = new List<Account>();
                        selectedCaseID = String.valueof(listCaseObj[0].id);
                        if(listChildAccount.size()>0){
                            for(Account childAcc :listChildAccount){
                                Mass_Update_Details__c mObj= mapMassUpdate.get(childAcc.id);
                                if(mObj == null){
                                    tempAccList.add(childAcc);
                                }
                            }
                        }
                        con = new ApexPages.StandardSetController(tempAccList);
                        con.setPageSize(50);
                        if(searchString==null || searchString==''){
                            tempAccountList = Database.query('select id, OwnerID,Customer_Status__c,Owner.Channel__c,Owner.Name, Owner.ManagerID,Internal_Duns__c,Trade_Name__c, name, (SELECT MSD_Code_Status__c FROM MSD_Codes__r where MSD_Code_Status__c=\'Active\' limit 1) from Account where parentid =\''+aParentObj.ParentId +'\' and id not in: listMassUpdate limit 501');
                       
                        }
                        else{
                            tempAccountList = Database.query('select id, OwnerID,Customer_Status__c,Owner.Channel__c,Owner.Name, Owner.ManagerID,Internal_Duns__c,Trade_Name__c, name, (SELECT MSD_Code_Status__c FROM MSD_Codes__r where MSD_Code_Status__c=\'Active\' limit 1) from Account where parentid =\''+aParentObj.ParentId+'\'  and ( Name LIKE \'%'+String.escapeSingleQuotes(searchString)+'%\' OR Internal_Duns__c LIKE  \'%'+String.escapeSingleQuotes(searchString)+'%\' OR Trade_Name__c LIKE  \'%'+String.escapeSingleQuotes(searchString)+'%\' ) and id not in: listMassUpdate limit 501');
                        }
                        //tempAccountList =[select id, OwnerID,Customer_Status__c,Owner.Channel__c,Owner.Name, Owner.ManagerID,Internal_Duns__c,Trade_Name__c, name, (SELECT MSD_Code_Status__c FROM MSD_Codes__r where MSD_Code_Status__c='Active' limit 1) from Account where parentid =:aParentObj.ParentId and id not in: listMassUpdate limit 501 ];
                        if(tempAccountList.size()>500 && OnNew !=true){
                                plusRec = true;
                                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.Warning,'More than 500 child Accounts present. Please refine your search criteria.'));
                            }
                            else{
                                plusRec =false;
                         }
                    
                        if(listMassUpdate.size()>0){
                            List<Account> lstAcc = [select id, Customer_Status__c,Owner.Channel__c,Owner.Name,OwnerID,Internal_Duns__c,Trade_Name__c, name, (SELECT MSD_Code_Status__c FROM MSD_Codes__r where MSD_Code_Status__c='Active' limit 1) from Account where id in: listMassUpdate];
                            if(lstAcc.size()>0){
                                for(Account a: lstAcc){
                                    AccountWrapper AccSelected = new AccountWrapper();
                                    AccSelected.aObj = a;
                                    if(a.MSD_Codes__r.size()>0){
                                        AccSelected.ActiveMSD = 'Yes';
                                    }
                                    else
                                        AccSelected.ActiveMSD = 'No';
                                        AccSelected.relatedCase=listCaseObj[0];
                                        AccSelected.selected= false;
                                        AccSelected.showCheckbox=true;
                                        lstAccselected.add(AccSelected);  
                                }
                            }
                        }
                            
                        
                        try{
                            ProcessInstanceWorkitem PIW=[SELECT ActorId,Actor.Type,CreatedById,CreatedDate,Id,IsDeleted,OriginalActorId,ProcessInstance.TargetObjectId,SystemModstamp,ProcessInstance.Status FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =: CaseObj.id];
                            if(PIW.ProcessInstance.Status=='Pending'){
                                if((PIW.Actor.Type !='Queue' && UserInfo.getUserId()==PIW.ActorId) || profile.name=='System Administrator' || profile.name=='EBU - Rogers CRM Admin'){
                                    showApprovalButtons=true; 
                                }
                                else if(PIW.Actor.Type !='Queue'){
                                    // If queue then get the list of members related to queue
                                    set<id> lstQueueMembers= new set<id>();
                                    list<GroupMember> gm=[SELECT GroupId,Id,SystemModstamp,UserOrGroupId FROM GroupMember WHERE GroupId =:PIW.ActorId];
                                    for(GroupMember gItem:gm){
                                        lstQueueMembers.add(gItem.id);
                                    }
                                    // Either member should be part of queue /System Admin / EBU - Rogers CRM Admin
                                    if(profile.name=='System Administrator' || profile.name=='EBU - Rogers CRM Admin' || lstQueueMembers.contains(UserInfo.getUserId())){
                                        showApprovalButtons=True;
                                    }else{
                                        showApprovalButtons=False;
                                    } 
                                }
                                else{
                                    showApprovalButtons=False;
                                }
                            }
                        }catch(exception ex){
                        }
                    }
                }
            }catch(exception e){     
            }
        }
        
        return page;
    }
    public pagereference saveCase(){
    
    if (!caseExists() && !OnNew) {
            return new PageReference('/apex/RecordDeleted');
        }
            
        try{  
            if(listCaseObj.size()==0){

                CaseObj.Mass_Update_Parent_Account__c = aParentObj.ParentId;
                CaseObj.AccountID =aParentObj.ParentID;
                CaseObj.RecordTypeId= caseDelinkRecordTypeId;
                CaseObj.Account_Owner__c=aParentRec.OwnerID;
                CaseObj.Status = 'New';
                CaseObj.OwnerId= UserInfo.getUserId();
                CaseObj.Parent_Account_Owner__c = aParentRec.OwnerID;
                if(aParentRec.Owner.Manager.isActive==true){
                    CaseObj.Account_Owner_Manager__c=aParentRec.Owner.ManagerID;
                }
                if(aParentRec.Owner.Manager.isActive==true){
                    CaseObj.Parent_Account_Owner_Manager__c= aParentRec.Owner.ManagerID;
                }
                listCaseObj.add(CaseObj);
                
                insert listCaseObj;
                
                lstMassUpdate = new List<Mass_Update_Details__c>();
                for(Account acc:selectedAccounts){
                        Mass_Update_Details__c mObj = new Mass_Update_Details__c();
                        mObj.Case__c = CaseObj.id;
                        mObj.Child_Account__c = acc.id;
                        lstMassUpdate.add(mObj);       
                }
                if(lstMassUpdate.size()>0){
                    insert lstMassUpdate;
                    CaseObj.subject = 'Account Delink request: ' +aParentRec.Name+ ' - '+ String.valueof(lstMassUpdate.size()) +' children';
                    update CaseObj;
                }
                Pagereference page = new Pagereference('/apex/AccountDeLinkingTemplate?id='+CaseObj.id+'&loc=500&parentAcc='+CaseObj.Mass_Update_Parent_Account__c+'&OnEdit=false&OnView=True');
                page.setRedirect(true);
                return page ;
            }
            else{
                update CaseObj;
                Pagereference page = new Pagereference('/apex/AccountDeLinkingTemplate?id='+CaseObj.id+'&loc=500&parentAcc='+CaseObj.Mass_Update_Parent_Account__c+'&OnEdit=false&OnView=True');
                page.setRedirect(true);
                return page ;
            }
        }catch(exception e){
            return null;
        }
    }
    public Pagereference remove(){
    
        if (!caseExists() && !OnNew) {
            return new PageReference('/apex/RecordDeleted');
        }
    
        lstMassUpdate = new List<Mass_Update_Details__c>();
        for(AccountWrapper aw:lstAccselected){
            if(aw.selected == true){
                Mass_Update_Details__c m = mapMassUpdate.get(aw.aObj.id);
                lstMassUpdate.add(m);
            }
        }
        if(lstMassUpdate.size()>0){
            delete lstMassUpdate;  
        }
        Pagereference page = new Pagereference('/apex/AccountDeLinkingTemplate?loc=500&parentAcc='+aParentObj.ParentId+'&OnEdit=true'+'&id='+CaseId);
        page.setRedirect(true);
        return page ;
    }
    public Pagereference add(){
        if (!caseExists() && !OnNew) {
            return new PageReference('/apex/RecordDeleted');
        }
        lstMassUpdate = new List<Mass_Update_Details__c>();
        for(Account acc:selectedAccounts){
            Mass_Update_Details__c mObj = new Mass_Update_Details__c();
            mObj.Case__c = CaseObj.id;
            mObj.Child_Account__c = acc.id;
                lstMassUpdate.add(mObj);       
        }
        if(lstMassUpdate.size()>0){
            insert lstMassUpdate;
        }
        Pagereference page = new Pagereference('/apex/AccountDeLinkingTemplate?loc=500&parentAcc='+aParentObj.ParentId+'&OnEdit=true'+'&id='+CaseId);
        page.setRedirect(true);
        return page ;
    }
    public Pagereference onEdit(){
        if (!caseExists() && !OnNew) {
            return new PageReference('/apex/RecordDeleted');
        }
       pagereference page = new PageReference('/apex/AccountDeLinkingTemplate?loc=500&parentAcc='+aParentObj.ParentId+'&OnEdit=true&OnView=false'+'&id='+CaseId);
       page.setRedirect(true); 
       return page;
    }
    
    public pagereference cancel(){
        Pagereference page = new Pagereference('/'+location);
        return page ;
    }

    public pagereference openCase(){
        Pagereference page = new Pagereference('/'+selectedCaseID);
        return page ;
    }
     public pagereference openAccount(){
        Pagereference page = new Pagereference('/'+selectedAccountID);
        return page ;
    }  
    public pagereference submitforApproval(){
    
            if (!caseExists() && !OnNew) {
            return new PageReference('/apex/RecordDeleted');
        }
        
         try{
          AssignmentRule AR = new AssignmentRule(); 
                AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1]; 
               //Creating the DMLOptions for "Assign using active assignment rules" checkbox 
                Database.DMLOptions dmlOpts = new Database.DMLOptions(); 
                dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id; 
            if(listCaseObj.size()==0){
                CaseObj.setOptions(dmlOpts); 
                CaseObj.Mass_Update_Parent_Account__c = aParentObj.ParentId;
                CaseObj.AccountID =aParentObj.ParentID;
                CaseObj.RecordTypeId= caseDelinkRecordTypeId;
                CaseObj.OwnerId= UserInfo.getUserId();
                CaseObj.Account_Owner__c=aParentRec.OwnerID;
                CaseObj.Parent_Account_Owner__c = aParentRec.OwnerID;
                if(aParentRec.Owner.Manager.isActive==true){
                    CaseObj.Account_Owner_Manager__c=aParentRec.Owner.ManagerID;
                }
                if(aParentRec.Owner.Manager.isActive==true){
                    CaseObj.Parent_Account_Owner_Manager__c= aParentRec.Owner.ManagerID;
                }
                listCaseObj.add(CaseObj);
                insert listCaseObj;
                lstMassUpdate = new List<Mass_Update_Details__c>();
                for(Account acc:selectedAccounts){
                        Mass_Update_Details__c mObj = new Mass_Update_Details__c();
                        mObj.Case__c = CaseObj.id;
                        mObj.Child_Account__c = acc.id;
                        lstMassUpdate.add(mObj);       
                }
                if(lstMassUpdate.size()>0){
                    insert lstMassUpdate;
                    CaseObj.subject = 'Account Delink request: ' +aParentRec.Name+ ' - '+ String.valueof(lstMassUpdate.size()) +' children';
                    update CaseObj;
                    Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                    req1.setComments('Submitting request for approval.');
                    req1.setObjectId(CaseObj.id);
                    // Submit the approval request for the account
                    Approval.ProcessResult result = Approval.process(req1);
                    shwSubmitForApprovalMsg = true;
                }
                return null;
            }
            else{
                CaseObj.setOptions(dmlOpts); 
                update CaseObj;
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setComments('Submitting request for approval.');
                req1.setObjectId(CaseObj.id);
                // Submit the approval request for the account
                Approval.ProcessResult result = Approval.process(req1);
                shwSubmitForApprovalMsg = true;
                return null;
            }
        }catch(exception e){
            return null;
        }
   
    } 
    public class AccountWrapper{
        public Account aObj{get;set;}
        public String ActiveMSD{get;set;}
        public boolean selected{get;set;}
        public Boolean showCheckbox{get;set;}
        public Case relatedCase{get;set;}
    } 
    public pagereference ApproveRequest(){
    
        if (!caseExists() && !OnNew) {
            return new PageReference('/apex/RecordDeleted');
        }

        Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
        req2.setComments('Approving request.');
        req2.setAction('Approve');
        ProcessInstanceWorkitem PIW=[SELECT ActorId,Actor.Type,CreatedById,CreatedDate,Id,IsDeleted,OriginalActorId,ProcessInstance.TargetObjectId,SystemModstamp FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =: listCaseObj[0].id];
        req2.setWorkitemId(PIW.id);
        // Submit the request for approval  
        Approval.ProcessResult result2 =  Approval.process(req2);
        Pagereference page = new Pagereference('/'+listCaseObj[0].id);
        return page ;
    }  
     public pagereference RejectRequest(){
         if (!caseExists() && !OnNew) {
            return new PageReference('/apex/RecordDeleted');
        }
        Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
        req2.setComments('Rejecting request.');
        req2.setAction('Reject');
        ProcessInstanceWorkitem PIW=[SELECT ActorId,Actor.Type,CreatedById,CreatedDate,Id,IsDeleted,OriginalActorId,ProcessInstance.TargetObjectId,SystemModstamp FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =: listCaseObj[0].id];
        req2.setWorkitemId(PIW.id);
        Approval.ProcessResult result2 =  Approval.process(req2);
        Pagereference page = new Pagereference('/'+listCaseObj[0].id);
        return page ;
    } 
    
    public pagereference deleteCase(){
        if(!caseExists() && !OnNew) {
        return new PageReference('/apex/RecordDeleted');
    }
        if(CaseObj!= null){
        delete CaseObj;
        Pagereference page = new Pagereference('/'+500);
        return page ;
        }
        return null;
    }  

    /****************Pagination Methods***********************/ 
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }
    //Indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }
     // returns the page number of the current page set
     public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
     }
     // returns the first page of records
     public void first() {
         con.first();
     }
     // returns the last page of records
     public void last() {
         con.last();
     }
     // returns the previous page of records
     public void previous() {
         con.previous();
     }
     // returns the next page of records
     public void next() {
         con.next();
     }
     // returns the PageReference of the original page, if known, or the home page.
     public void cancelpage() {
         con.cancel();
     }
}