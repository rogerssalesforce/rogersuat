/*********************************************************************************
Class Name      : CPQ_AgreementLine_Trigger_Helper
Description     : This class is used as a helper class for Agreement Trigger 
Created By      : Mitali Telang
Created Date    : 21-Aug-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Mitali Telang            26-Aug-2015            Created class for Agreement Line Item Trigger

*********************************************************************************/

public with sharing class CPQ_AgreementLine_Trigger_Helper{

    /*********************************************************************************
    Method Name    : populateCSOWoNAgreement
    Description    : Method to Populate CSOW flag on Agreement, if any product has CSOW Required Checked
    Return Type    : Void
    Parameter      : List<Apttus__AgreementLineItem__c>
    *********************************************************************************/
    
    public static void populateCSOWoNAgreement(List<Apttus__AgreementLineItem__c> lstAgreementLineItems){
        Set<Id> setProductIds = new Set<Id>();
        Set<Id> setOptionIds = new Set<Id>();
        Set<Id> setAgreementIds = new Set<Id>();
        Set<Id> setAgreementWithCSOWId = new Set<Id>();
        Map<Id, List<Id>> mapAgreementProductsList = new Map<Id, List<Id>>();
        for(Apttus__AgreementLineItem__c agreementLineItem: lstAgreementLineItems){
            setAgreementIds.add(agreementLineItem.Apttus__AgreementId__c);
            setProductIds.add(agreementLineItem.Apttus__ProductId__c);
            if(agreementLineItem.Apttus_CMConfig__LineType__c.equalsIgnoreCase('Option')){
                setProductIds.add(agreementLineItem.Apttus_CMConfig__OptionId__c);
            }
        }
        
        System.debug('Product ids : '+ setProductIds);
        
        List<Product2> listProducts = [select id from product2 where id in:setProductIds and CSOW_required__c = true];
        for(product2 product :listProducts){
            for(Apttus__AgreementLineItem__c agreementLineItem: lstAgreementLineItems){
              if(product.id == agreementLineItem.Apttus__ProductId__c || product.id == agreementLineItem.Apttus_CMConfig__OptionId__c){
                 setAgreementWithCSOWId.add(agreementLineItem.Apttus__AgreementId__c); 
              }
                
            }
        }
        
       List<Apttus__APTS_Agreement__c> listAgreementWithCSOW = [select id, CSOW_required__c from Apttus__APTS_Agreement__c where id in :setAgreementWithCSOWId];
       for(Apttus__APTS_Agreement__c agreement : listAgreementWithCSOW){
            agreement.CSOW_required__c = true;
            agreement.Apttus__Status_Category__c = 'In Authoring';
            agreement.Apttus__Status__c = 'Internal Review';
            agreement.CSOW_Contract_Team_Status__c = 'Pending Review';
            
       }
        update listAgreementWithCSOW;   
    }


    /*********************************************************************************
    Method Name    : findProductSchedulesForAgreement
    Description    : Method for Adding the list of product schedules to be included in the Agreement.
    Return Type    : Void
    Parameter      : List<Apttus__AgreementLineItem__c>
    *********************************************************************************/
    
    public static void findProductSchedulesForAgreement(List<Apttus__AgreementLineItem__c> lstAgreementLineItems){
    
        Map<Id, Apttus__APTS_Agreement__c> mapAgreements = new Map<Id, Apttus__APTS_Agreement__c>();
        Map<Id, Product2> mapProducts = new Map<Id, Product2>();
        Set<Id> setAgreementIds = new Set<Id>();
        Set<Id> setProductIds = new Set<Id>();
        Map<Id, List<Apttus__AgreementLineItem__c>> mapAgreementIdLines = new Map<Id, List<Apttus__AgreementLineItem__c>>();
        
        //Iterating through Agreement to product Ids
        for(Apttus__AgreementLineItem__c agreementLineItem: lstAgreementLineItems){
            setAgreementIds.add(agreementLineItem.Apttus__AgreementId__c);
            setProductIds.add(agreementLineItem.Apttus__ProductId__c);
            List<Apttus__AgreementLineItem__c> listAgreementLines;
            //Condtion to check if the Agreement Line item is present in Agreement
            if(mapAgreementIdLines.containsKey(agreementLineItem.Apttus__AgreementId__c)){
                listAgreementLines = mapAgreementIdLines.get(agreementLineItem.Apttus__AgreementId__c);
            } else{
                listAgreementLines = new List<Apttus__AgreementLineItem__c>();
            }
            listAgreementLines.add(agreementLineItem);
            mapAgreementIdLines.put(agreementLineItem.Apttus__AgreementId__c, listAgreementLines);
        }
        
        
        mapProducts.putAll([Select Id, ProductSchedule__c, ProductSchedule__r.ProductScheduleName__c from Product2 where id in: setProductIds And ProductSchedule__c != null]);
     
        mapAgreements.putAll([Select Id, ProdSchdlsIncl__c from Apttus__APTS_Agreement__c where Id in : setAgreementIds]);
        
        //Iterating through product Agreement Ids to associate between product and Agreement Line Item
        for(Id agreementId : mapAgreementIdLines.keySet()){
            List<Apttus__AgreementLineItem__c> listAgreementLines = new List<Apttus__AgreementLineItem__c>();
            listAgreementLines = mapAgreementIdLines.get(agreementId);
            Set<String> productScheduleAgreement = new Set<String>();
            List<String> listProductScheduleAgreement = new List<String>();
            //Iterating agreement Line item list to add prodcut schedules to the agreement line item.
            for(Apttus__AgreementLineItem__c agreementLineItem: listAgreementLines){
                //Associating the Product SChedules to the Agreement Line Item
                if(mapProducts.containsKey(agreementLineItem.Apttus__ProductId__c)){
                    productScheduleAgreement.add(mapProducts.get(agreementLineItem.Apttus__ProductId__c).ProductSchedule__r.ProductScheduleName__c);
                }
            }
            Apttus__APTS_Agreement__c a = mapAgreements.get(agreementId);
            listProductScheduleAgreement.addAll(productScheduleAgreement);
            a.ProdSchdlsIncl__c = String.join(listProductScheduleAgreement, ';');
            mapAgreements.put(agreementId, a);
        }
        
        List<Apttus__APTS_Agreement__c> listAgreement = new List<Apttus__APTS_Agreement__c>();
        listAgreement =  mapAgreements.values();
        
        //Updating Agreements
        if(listAgreement != NULL && listAgreement.size() > 0){
            update listAgreement;
        }
    }
   
    /*********************************************************************************
    Method Name    : includeProductSchedulesForAgreementLine
    Description    : Method for Adding the appropriate product schedule for the Agreemen line item based on the product.
    Return Type    : Void
    Parameter      : List<Apttus__AgreementLineItem__c>
    *********************************************************************************/
   
    public static void includeProductSchedulesForAgreementLine(List<Apttus__AgreementLineItem__c> lstAgreementLineItems){
    
        Set<Id> setProductIds = new Set<Id>();
        Set<Id> setAgreementIds = new Set<Id>();
        List<Product2> listProducts = new List<Product2>();
        Set<Id> setProducts = new Set<Id>();
        Set<Id> setAgreementLinesProductExist = new Set<Id>();
        List<Apttus__AgreementLineItem__c> listAgreementLinesExist = new List<Apttus__AgreementLineItem__c>();
        Map<Id, List<Apttus__AgreementLineItem__c>> mapAgreementIdLines = new Map<Id, List<Apttus__AgreementLineItem__c>>();
        
        //iterating the Line items to get product ids
        for(Apttus__AgreementLineItem__c agreementLineItem: lstAgreementLineItems){
            setAgreementIds.add(agreementLineItem.Apttus__AgreementId__c);
            setProductIds.add(agreementLineItem.Apttus__ProductId__c);
        }    
        
        listAgreementLinesExist = [Select Id, Apttus__ProductId__c, InclProdSchdl__c from Apttus__AgreementLineItem__c where Apttus__AgreementId__c in:setAgreementIds and InclProdSchdl__c ='Yes'];
        
        //Iterating the Line items to add product ids to set.
        for(Apttus__AgreementLineItem__c agreementLine: listAgreementLinesExist){
            setAgreementLinesProductExist.add(agreementLine.Apttus__ProductId__c);
        }
        
        listProducts = [Select Id, ProductSchedule__c, CSOW_Required__c from Product2 where id in: setProductIds And ProductSchedule__c != null];
        
        //Adding products to set
        for(Product2 prod: listProducts){
            setProducts.add(prod.Id);
        }
        
        //Iterating the Agreements to check for multiple agreement line with same product
        for(Id agreement: setAgreementIds){
            set<Id> setProductDuplicate = new Set<Id>();
            // Set<Id> setProductScheduleId = new Set<Id>();
            Map<Id, Set<String>> mapAgreementIdSetProdSchedule = new Map<Id, Set<String>>();
            //Itereating the Agreement line items to check for duplicates
            for(Apttus__AgreementLineItem__c agreementItem: lstAgreementLineItems){
                // checking if the agreement line item is for the Correct Agreement.
                if(agreement == agreementItem.Apttus__AgreementId__c){
                    //checking if the product is duplicate in the agreement.
                    if(setAgreementLinesProductExist.contains(agreementItem.Apttus__ProductId__c)){
                        setProductDuplicate.add(agreementItem.Apttus__ProductId__c);
                    }
                    //Checking the product set for duplicates and marking the Include Product schedule flag accordingly.
                    if(setProducts.contains(agreementItem.Apttus__ProductId__c) && !setProductDuplicate.contains(agreementItem.Apttus__ProductId__c)){
                        setProductDuplicate.add(agreementItem.Apttus__ProductId__c);
                        if(mapAgreementIdSetProdSchedule.get(agreement) == null || !mapAgreementIdSetProdSchedule.get(agreement).contains(agreementItem.Prod_Schedule__c)){
                            agreementItem.InclProdSchdl__c = 'Yes';
                            if(mapAgreementIdSetProdSchedule.get(agreement) == null){
                                Set<String> SetProductScheduleId = new Set<String>();
                                SetProductScheduleId.add(agreementItem.Prod_Schedule__c);
                                mapAgreementIdSetProdSchedule.put(agreement, SetProductScheduleId);
                            }
                            else{
                                mapAgreementIdSetProdSchedule.get(agreement).add(agreementItem.Prod_Schedule__c);
                            }
                             
                        }
                    } else{
                        agreementItem.InclProdSchdl__c = 'No';
                    }
                    
                }
            }
        }
    }
}