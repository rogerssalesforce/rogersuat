/*Class Name  : TestClass_Case_UpdateRelatedAccMAL_AU.
 *Description : This is test class for Case_UpdateRelatedAccMAL_AU.
 *Created By  : Rajiv Gangra.
 *Created Date :11/09/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
@isTest(SeeAllData = true)
private class TestClass_Case_UpdateRelatedAccMAL_AU {
  private static Account accTestObj;
  private static Account accTestObjWithParent;
    private static Case caseTestObj;
    private static Case caseTestObj2;
    private static user objUser;
    static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
    private static void setUpData(){
        //TAG_CS.Unassigned_User__c ='Unassigned User';
        //TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        //TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        //insert TAG_CS;
        Profile profile = [Select Id From Profile Where name = 'System Administrator' Limit 1];
        //Profile profile = [select id from profile where name='System Administrator']; 
        String randomUserName = String.valueOf(System.now().getTime()) + '@test.com';
        objUser = new User(alias = 'test', email='test@test.com',
        emailencodingkey='UTF-8', lastname='TestUser1', languagelocalekey='en_US',
        localesidkey='en_US', profileid = profile.Id,
        timezonesidkey='America/Denver', username=randomUserName, isActive=true, Manager = [select id from user where id =: Userinfo.getUserId()]);
        insert objUser;

       accTestObj = new Account(Name='testAcc8689697');
        accTestObj.BillingCountry= 'CA';
        accTestObj.BillingPostalCode = 'A9A 9A9';
        accTestObj.BillingState = 'ON';
        accTestObj.BillingCity='City';
        accTestObj.BillingStreet='Street';
       accTestObj.Account_Status__c= 'Assigned';
        accTestObj.ParentId = null;
       insert accTestObj;
       
        accTestObjWithParent = new Account(Name='testAcc27896686');
        accTestObjWithParent.BillingCountry= 'CA';
        accTestObjWithParent.BillingPostalCode = 'A9A 9A9';
        accTestObjWithParent.BillingState = 'ON';
        accTestObjWithParent.BillingCity='City';
        accTestObjWithParent.BillingStreet='Str32eet';
        accTestObjWithParent.Account_Status__c= 'Assigned';
        accTestObjWithParent.ParentId = null;
       insert accTestObjWithParent;
       
       MSD_Code__c msd = new MSD_Code__c();
       msd.name = 'test';
       msd.Account__c = accTestObj.Id;
       msd.MSD_Code_Status__c = 'Active';
       Insert msd;
       
       caseTestObj= new case(accountID=accTestObj.id);
       caseTestObj2= new case(accountID=accTestObj.id);
    }
    static testmethod void myUnitWithCaseTest(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            RecordType relRecordTypeList= [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.LastModifiedDate, r.LastModifiedById, r.IsActive, r.Id, r.DeveloperName, r.Description, r.CreatedDate, r.CreatedById, r.BusinessProcessId From RecordType r where r.DeveloperName='Account_Update'];
            caseTestObj.RecordTypeID=relRecordTypeList.id;
            caseTestObj.Status='New';

            Schema.DescribeFieldResult resultProgressReason= Case.In_Progress_Reason__c.getDescribe();        
            caseTestObj.In_Progress_Reason__c = (String)(TestingUtil.getPicklistDefaultValue(resultProgressReason));
            caseTestObj.Date__c = Date.today();
            /**/        
            System.debug('***In Progress Reason : ' + caseTestObj.In_Progress_Reason__c);
            System.debug('***Date (custom): ' + caseTestObj.Date__c);
            /**/        
            
            insert caseTestObj;
            
            caseTestObj.Status='Approved';
            update caseTestObj;
            Account a=[select id,Name,Pending_MAL_Sync_Flag__c from Account where Id =:accTestObj.id];
        //    system.assertequals(a.Pending_MAL_Sync_Flag__c,True);          
            Test.stoptest();
        }
    } 
    static testmethod void myUnitWithCaseTest2(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            RecordType relRecordTypeList= [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.LastModifiedDate, r.LastModifiedById, r.IsActive, r.Id, r.DeveloperName, r.Description, r.CreatedDate, r.CreatedById, r.BusinessProcessId From RecordType r where r.DeveloperName='Account_Update'];
            caseTestObj.RecordTypeID=relRecordTypeList.id;
            caseTestObj.Status='New';

            Schema.DescribeFieldResult resultProgressReason= Case.In_Progress_Reason__c.getDescribe();        
            caseTestObj.In_Progress_Reason__c = (String)(TestingUtil.getPicklistDefaultValue(resultProgressReason));
            caseTestObj.Date__c = Date.today();
            /**/        
            System.debug('***In Progress Reason : ' + caseTestObj.In_Progress_Reason__c);
            System.debug('***Date (custom): ' + caseTestObj.Date__c);
            /**/        
            
            insert caseTestObj;
            
            caseTestObj.Status='Declined - Invalid';
            update caseTestObj;
            Account a=[select id,Name,Pending_MAL_Sync_Flag__c from Account where Id =:accTestObj.id];
    //        system.assertequals(a.Pending_MAL_Sync_Flag__c,false);          
            Test.stoptest();
        }
    } 
    static testmethod void myUnitWithCaseTest3(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            RecordType relRecordTypeList= [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.LastModifiedDate, r.LastModifiedById, r.IsActive, r.Id, r.DeveloperName, r.Description, r.CreatedDate, r.CreatedById, r.BusinessProcessId From RecordType r where r.DeveloperName='New_Account' and sobjecttype = 'Case'];
            caseTestObj.RecordTypeID=relRecordTypeList.id;
            caseTestObj.Status='New';

            Schema.DescribeFieldResult resultProgressReason= Case.In_Progress_Reason__c.getDescribe();        
            caseTestObj.In_Progress_Reason__c = (String)(TestingUtil.getPicklistDefaultValue(resultProgressReason));
            caseTestObj.Date__c = Date.today();
            /**/        
            System.debug('***In Progress Reason : ' + caseTestObj.In_Progress_Reason__c);
            System.debug('***Date (custom): ' + caseTestObj.Date__c);
            /**/        
            
            insert caseTestObj;
            
            caseTestObj.Status='Declined - Invalid';
            update caseTestObj;
            Account a=[select id,Name,Pending_MAL_Sync_Flag__c from Account where Id =:accTestObj.id];
        //    system.assertequals(a.Pending_MAL_Sync_Flag__c,false);          
            Test.stoptest();
        }
    } 
    static testmethod void myUnitWithCaseTest4(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            RecordType relRecordTypeList= [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.LastModifiedDate, r.LastModifiedById, r.IsActive, r.Id, r.DeveloperName, r.Description, r.CreatedDate, r.CreatedById, r.BusinessProcessId From RecordType r where r.DeveloperName='Account_Alert' and sobjecttype = 'Case'];
            caseTestObj.RecordTypeID=relRecordTypeList.id;
            caseTestObj.Status='New';

            Schema.DescribeFieldResult resultProgressReason= Case.In_Progress_Reason__c.getDescribe();        
            caseTestObj.In_Progress_Reason__c = (String)(TestingUtil.getPicklistDefaultValue(resultProgressReason));
            caseTestObj.Date__c = Date.today();
            /**/        
            System.debug('***In Progress Reason : ' + caseTestObj.In_Progress_Reason__c);
            System.debug('***Date (custom): ' + caseTestObj.Date__c);
            /**/        
            
            insert caseTestObj;
            
            caseTestObj.Status='Declined - Invalid';
            update caseTestObj;
            Account a=[select id,Name,Pending_MAL_Sync_Flag__c from Account where Id =:accTestObj.id];
    //        system.assertequals(a.Pending_MAL_Sync_Flag__c,false);          
            Test.stoptest();
        }
    } 
    static testmethod void myUnitWithCaseTest5(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            RecordType relRecordTypeList= [Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, r.LastModifiedDate, r.LastModifiedById, r.IsActive, r.Id, r.DeveloperName, r.Description, r.CreatedDate, r.CreatedById, r.BusinessProcessId From RecordType r where r.DeveloperName='Account_Update' and sobjecttype = 'Case'];
            caseTestObj2.RecordTypeID=relRecordTypeList.id;
            caseTestObj2.Status='New';

            Schema.DescribeFieldResult resultProgressReason= Case.In_Progress_Reason__c.getDescribe();        
            caseTestObj2.In_Progress_Reason__c = (String)(TestingUtil.getPicklistDefaultValue(resultProgressReason));
            caseTestObj2.Date__c = Date.today();
            /**/        
            System.debug('***In Progress Reason : ' + caseTestObj2.In_Progress_Reason__c);
            System.debug('***Date (custom): ' + caseTestObj2.Date__c);
            /**/        
            
            insert caseTestObj2;
            
            caseTestObj2.Status='Declined - Invalid';
            update caseTestObj2;
            Account a=[select id,Name,Pending_MAL_Sync_Flag__c from Account where Id =:accTestObj.id];
      //      system.assertequals(a.Pending_MAL_Sync_Flag__c,false);          
            Test.stoptest();
        }
    } 
  
}