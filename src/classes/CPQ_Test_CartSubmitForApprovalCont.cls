/*********************************************************************************
Class Name      : CPQ_Test_CartSubmitForApprovalCont
Description     : This class a test class for CPQ_CartSubmitForApprovalCont
Created By      : Deepika Rawat
Created Date    : 1-Nov-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Deepika Rawat              1-Nov-15              Original version
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest (seeAllData=False) 
private class CPQ_Test_CartSubmitForApprovalCont{
    static User TestAEUser;
    static Account oAccount;
    static product2 oProduct;
    static Opportunity oOpportunity;
    static Apttus_Proposal__Proposal__c oQuote;
    static Attachment attachmentQuote;
    static Apttus_Config2__PriceList__c oPriceList;
    static Apttus_Config2__PriceListItem__c oPriceListItem;
    static ID prodConfigID;
    static Pricebook2 oPricebook;
    static Pricebook2 oPriceBookCustom;
    static List<Apttus_Config2__ProductConfiguration__c> lstProductConfig ;
    private static CPQ_Settings__c cs;
     /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testMethod void createTestData() {
         //Create custom Pricebook 
        oPriceBookCustom = CPQ_Test_Setup.newPriceBook('CPQ Temp PriceBook');
        insert oPriceBookCustom;
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        cs  = CPQ_Settings__c.getOrgDefaults();
        cs.DefaultOpportunityRecordType__c = 'Rogers EBU - New';
        upsert cs;
        //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');      
        system.runAs(TestAEUser){
            //Create newPriceBook
            oPricebook = CPQ_Test_Setup.newPriceBook('Test PriceBook');
            insert oPricebook;
            //Create PriceList
            oPriceList = CPQ_Test_Setup.newPriceList();
            insert oPriceList ;
            // Create Product
            oProduct = CPQ_Test_Setup.newProduct();
            oProduct.Family = 'Wireless - Voice';
            insert oProduct;
            //Create Pricelist item
            oPriceListItem = CPQ_Test_Setup.newPriceListItem(oPriceList.ID,oProduct.ID);
            insert oPriceListItem;
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
            insert oQuote;
            //Product Configuration
            prodConfigID = CPQ_Test_Setup.newProdictConfiguration(oQuote);
            //Create line items
            CPQ_Test_Setup.createLineItem(prodConfigID,oProduct.id,oPriceListItem.id);
            lstProductConfig = [Select id, Apttus_QPConfig__Proposald__c, Apttus_Config2__Status__c from  Apttus_Config2__ProductConfiguration__c where Apttus_QPConfig__Proposald__c =:oQuote.id ];
        }
    }
    /*********************************************************************************
    Method Name    : testSubmitForApprovalValid
    Description    : Test "Submit for approval" as AE with Saved Quote
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testSubmitForApprovalValid(){
        createTestData();
        oQuote.Apttus_Proposal__Approval_Stage__c = 'Approval Required';
        update oQuote;
        system.runAs(TestAEUser){
            test.startTest();
            ApexPages.currentPage().getParameters().put('id', prodConfigID);
            ApexPages.currentPage().getParameters().put('configrequestid', prodConfigID);
            CPQ_CartSubmitForApprovalCont obj = new CPQ_CartSubmitForApprovalCont();
            PageReference  pg = obj.submitQuoteForApproval();
            Apttus_Proposal__Proposal__c oQuoteTest = [Select Apttus_Proposal__Approval_Stage__c from Apttus_Proposal__Proposal__c  where id=:oQuote.id];
            System.assertEquals(pg.getUrl().containsIgnoreCase('flow=null') || !pg.getUrl().containsIgnoreCase('flow'), true);
            //System.assert(!pg.getUrl().containsIgnoreCase('flow'));
            test.stopTest();
        }
    } 
    /*********************************************************************************
    Method Name    : testSubmitForApprovalNotValid
    Description    : Test "Submit for approval" as AE with Saved Quote
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testSubmitForApprovalNotValid(){
        createTestData();
        oQuote.Apttus_Proposal__Approval_Stage__c = 'Draft';
        update oQuote;
        system.runAs(TestAEUser){
            test.startTest();
            ApexPages.currentPage().getParameters().put('id', prodConfigID);
            ApexPages.currentPage().getParameters().put('configrequestid', prodConfigID);
            CPQ_CartSubmitForApprovalCont obj = new CPQ_CartSubmitForApprovalCont();
            obj.submitQuoteForApproval();
            PageReference  pg = obj.backToCart();
            Apttus_Proposal__Proposal__c oQuoteTest = [Select Apttus_Proposal__Approval_Stage__c from Apttus_Proposal__Proposal__c  where id=:oQuote.id];
            System.assert(pg.getUrl().containsIgnoreCase('flow'));
            System.assert(oQuoteTest.Apttus_Proposal__Approval_Stage__c !='In Review');
            test.stopTest();
        }
    } 
}