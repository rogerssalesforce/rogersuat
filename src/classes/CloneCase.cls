/*
 * Subclass to extend functionality of third-party open source deep clone solution:
 * 
 * http://code.google.com/p/salesforce-super-clone/source/browse/trunk/%20salesforce-super-clone/Google%20code%20Super%20Clone/src/classes/Clone.cls
 * r3 by jinghaili on Jul 10, 2011 
 * 
 * This allows for modified behaviour for Case object cloning.
 *
 */
//The top class needs to be global as this needs to be accessible for API call
global class CloneCase extends AbstractClone {

    /*
    * Modify newly cloned object before returning if needed
    */
    public override Sobject processCopy(String fields,String objAPIName, string parentRecordID){
   
        String queryString = 'select ' + String.escapeSingleQuotes(fields) + ' from ' + String.escapeSingleQuotes(objAPIName) + ' where id=\'' + String.escapeSingleQuotes(parentRecordID) + '\'';
        List<SObject> result = Database.query(queryString);
        
        if(result==null || result.size()==0){
            return null;        
        }
        
        //If this parent is not a Case object, just return clone as is  
        if(result.get(0).getsObjectType()!=Case.getsObjectType() ) {
            return result.get(0).clone(false, true);
        }
        
        Case caseResult = (Case)result.get(0);
        
        //Check if parent Case object has a Closed Date, if so modify cloned Case for validation rules
        try{
            if(caseResult.ClosedDate!=null) {
                caseResult.Status = 'New';
                //NOTE: SF apparently doesnt allow 'caseResult.ClosedDate = null',
                
            }
        }
        catch(System.Exception e){
            system.debug('processCopy(): processing ' + Case.Id + ' threw \n' + e.getStackTraceString() );
        }
            
        queryString = null;
        result = null;
        
        return caseResult.clone(false, true);
        
    }    
}