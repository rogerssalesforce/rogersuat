/*
===============================================================================
Class Name : Test_TAG_DeleteAREntriesSchedular
===============================================================================
PURPOSE: This is a test class for TAG_DeleteAREntriesSchedular class
COMMENTS: 
Developer: Deepika Rawat
Date: 4/23/2015
CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
4/23/2015              Deepika Rawat        Created
===============================================================================

*/
@isTest(SeeAllData = false)
private class Test_TAG_DeleteAREntriesSchedular{
 static testmethod void testMethod1(){      
        Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();  
        TAG_CS.Purge_AREntry__c = '0';
        insert TAG_CS;
        Test.StartTest();
        TAG_DeleteAREntriesSchedular obj= new TAG_DeleteAREntriesSchedular();
        String sch = '0 0 23 * * ?';
        system.schedule('TAG_CreateAccTeamNewChildTesting', sch, obj);
        System.assert(true);
        Test.stopTest();    
    }
}