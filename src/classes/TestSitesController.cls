/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 /*********************************************************************************
Class Name      : TestSitesController
Description     : Test class for SitesController
Created By      : 
Created Date    : 
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Alina Balan          18-November-2015            Refactoring
*********************************************************************************/

@isTest (seeAllData=False) 
private class TestSitesController {
    private static User TestAEUser;
    private static Opportunity oOpportunity;
    private static Boolean withAssertions = true;
    private static ServiceableLocation__c oServiceableLocation1;
    private static ServiceableLocation__c oServiceableLocation2;
    private static ServiceableLocation__c oServiceableLocation3;
    private static ServiceableLocation__c oServiceableLocation4;
    private static final String STREET_NUMBER = '1';
    private static final String CITY = 'Test City';
    private static final String ACCESS_TYPE_GROUP = 'NNI';
    
    /*Static Profile pEmp = [Select Id from Profile where name like '%Rogers%' limit 1];
    static user userRec1;*/

    private static void createTestData() {
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        //Create test user 
        TestAEUser = CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');
        //User TestAEUser2 = CPQ_Test_Setup.newUser('Test User AE', 'EBU - Rogers Account Executive');
        //insert TestAEUser2;
        //User testAdminUser = CPQ_Test_Setup.newUser('System Administrator', 'standarduser_test123@testorg.com');
        //insert testAdminUser;

        system.runAs(TestAEUser ) {

            //Create Account
            Account oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            //oOpportunity.OwnerId = TestAEUser2.Id;
            insert oOpportunity;
 
            //create serviceable locations
            oServiceableLocation1 = CPQ_Test_Setup.createServiceablelocation(ACCESS_TYPE_GROUP, STREET_NUMBER, CITY);
            insert oServiceableLocation1;
            oServiceableLocation2 = CPQ_Test_Setup.createServiceablelocation(ACCESS_TYPE_GROUP, STREET_NUMBER + '1', CITY);
            insert oServiceableLocation2;
            oServiceableLocation3 = CPQ_Test_Setup.createServiceablelocation(ACCESS_TYPE_GROUP, STREET_NUMBER + '2', CITY);
            insert oServiceableLocation3;
            oServiceableLocation4 = CPQ_Test_Setup.createServiceablelocation(ACCESS_TYPE_GROUP, '0', 'aaa');
            insert oServiceableLocation4;
            
            CLLIPostalCode__c oPostalCode = CPQ_Test_Setup.createPostalCode();
            insert oPostalCode;
            SitesController_Setting__c sitesController_cs= new SitesController_Setting__c();
            sitesController_cs.Quote_Request__c='100';
            insert sitesController_cs;
            
            Site__c oSite = CPQ_Test_Setup.createSite(oOpportunity, oServiceableLocation1, STREET_NUMBER, CITY);
            insert oSite;
            Quote oQuote = CPQ_Test_Setup.createQuote(oOpportunity);
            insert oQuote;
            Quote_Site__c oQuoteSite = CPQ_Test_Setup.createQuoteSite(oQuote, oSite);
            insert oQuoteSite;
        }
    }
    
    static testMethod void defaultBlankTest() {
        createTestData();
        system.runAs(TestAEUser) {
            test.startTest();
            PageReference pageRef = New PageReference('/apex/addSites?retURL=' + String.valueOf(oOpportunity.Id)); 
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController sc = new ApexPages.standardController(oOpportunity);
            SitesController controller = new SitesController(sc);
            Apexpages.currentPage().getParameters().put('serviceableLocationName', '');
            Apexpages.currentPage().getParameters().put('suite', '');
            Apexpages.currentPage().getParameters().put('streetNumber', '');
            Apexpages.currentPage().getParameters().put('streetName', '');
            Apexpages.currentPage().getParameters().put('streetType', '');
            Apexpages.currentPage().getParameters().put('streetDirection', '');
            Apexpages.currentPage().getParameters().put('city', '');
            Apexpages.currentPage().getParameters().put('postalCode', '');
            Apexpages.currentPage().getParameters().put('cninni', 'false');
            Apexpages.currentPage().getParameters().put('accessTypeGroups', '');
            Apexpages.currentPage().getParameters().put('networkAccessTypes', '');
            
            List<ServiceableLocation__c> locations1 = [SELECT Id FROM ServiceableLocation__c LIMIT 20];
            List<ServiceableLocation__c> locations2 = [SELECT Id FROM ServiceableLocation__c where CNINNI__c != '' LIMIT 20];
            controller.runSearch();
            if (withAssertions) {
                System.assertEquals(controller.lstServiceableLocations.size(), locations1.size());
            }
        
            ApexPages.currentPage().getParameters().put('cninni', 'true');
            controller.runSearch();
            if (withAssertions) {
                System.assertEquals(controller.lstServiceableLocations.size(), locations2.size()); 
            }
        
            controller.toggleSort();
            test.stopTest();
        }
    }
    
    static testMethod void defaultBlankTestDuplicateCheck() {
        createTestData();
        system.runAs(TestAEUser) {
            test.startTest();
            PageReference pageRef = New PageReference('/apex/addSites'); 
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController sc = new ApexPages.standardController(oOpportunity);
            SitesController controller = new SitesController(sc);
            Apexpages.currentPage().getParameters().put('serviceableLocationName', '');
            Apexpages.currentPage().getParameters().put('suite', '');
            Apexpages.currentPage().getParameters().put('streetNumber', '');
            Apexpages.currentPage().getParameters().put('streetName', '');
            Apexpages.currentPage().getParameters().put('streetType', '');
            Apexpages.currentPage().getParameters().put('streetDirection', '');
            Apexpages.currentPage().getParameters().put('city', '');
            Apexpages.currentPage().getParameters().put('postalCode', '');
            Apexpages.currentPage().getParameters().put('cninni', 'false');
            Apexpages.currentPage().getParameters().put('accessTypeGroups', '');
            Apexpages.currentPage().getParameters().put('dupCheck', CPQ_Constants.INT_TRUE_VALUE);
            
            List<ServiceableLocation__c> locations1 = [SELECT Id FROM ServiceableLocation__c LIMIT 20];
            controller.runSearch();
            if (withAssertions) {
                System.assertEquals(controller.lstServiceableLocations.size(), locations1.size());
            }
            test.stopTest();
        }
    }
    
    static testMethod void exactMatchTest() {
        createTestData();
        system.runAs(TestAEUser) {
            test.startTest();
            PageReference pageRef = New PageReference('/apex/addSites?retURL=' + String.valueOf(oOpportunity.Id)); 
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController sc = new ApexPages.standardController(oOpportunity);
            SitesController controller = new SitesController(sc);
            String sServiceableLocationName = CPQ_Test_Setup.SERVICEABLE_LOCATION_SUITE_FLOOR + STREET_NUMBER + CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_NAME + CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_TYPE 
                                            + CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_DIRECTION + CITY + CPQ_Test_Setup.SERVICEABLE_LOCATION_PROVINCE_CODE + CPQ_Test_Setup.SERVICEABLE_LOCATION_CLLI_CODE;
            Apexpages.currentPage().getParameters().put('serviceableLocationName', sServiceableLocationName);
            Apexpages.currentPage().getParameters().put('suite', CPQ_Test_Setup.SERVICEABLE_LOCATION_SUITE_FLOOR);
            Apexpages.currentPage().getParameters().put('streetNumber', STREET_NUMBER);
            Apexpages.currentPage().getParameters().put('streetName', CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_NAME);
            Apexpages.currentPage().getParameters().put('streetType', CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_TYPE);
            Apexpages.currentPage().getParameters().put('streetDirection', CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_DIRECTION);
            Apexpages.currentPage().getParameters().put('city', CITY);
            Apexpages.currentPage().getParameters().put('postalCode', CPQ_Test_Setup.SERVICEABLE_LOCATION_POSTAL_CODE);
            Apexpages.currentPage().getParameters().put('cninni', 'false');
            Apexpages.currentPage().getParameters().put('accessTypeGroups', ACCESS_TYPE_GROUP);
            Apexpages.currentPage().getParameters().put('networkAccessTypes', CPQ_Test_Setup.SERVICEABLE_LOCATION_NETWORK_ACCESS_TYPE);
            
            controller.runSearch();
            if (withAssertions) {
                Boolean bFound = false;
                for (ServiceableLocationWrapper sl : controller.lstServiceableLocations){
                    if (sl.getServiceableLocation().Id == oServiceableLocation1.Id)
                        bFound = true;
                }
                System.assertEquals(bFound, true);
            }
            test.stopTest();
        }
    }
    
    static testMethod void testAddNew() {
        createTestData();
        system.runAs(TestAEUser) {
            test.startTest();
            ModifyServiceableLocation controllerA = new ModifyServiceableLocation();
            controllerA.addSite();
            ApexPages.StandardController sc1 = new ApexPages.standardController(new ServiceableLocation__c());
            controllerA = new ModifyServiceableLocation(sc1);
            controllerA.addSite();
            //    EnterpriseSalesToolBoxController controllerB = new EnterpriseSalesToolBoxController();
            //    String test1 = controllerB.baseURL;
            PageReference pageRef = New PageReference('/apex/addSites?edit=1&retURL=' + String.valueOf(oOpportunity.Id)); 
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController sc = new ApexPages.standardController(oOpportunity);
            SitesController controller = new SitesController(sc);
            Apexpages.currentPage().getParameters().put('isNewSite', 'true');
            controller.editServiceableLocation();
            String sServiceableLocationName = CPQ_Test_Setup.SERVICEABLE_LOCATION_SUITE_FLOOR + STREET_NUMBER + CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_NAME + CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_TYPE 
                                                + CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_DIRECTION + CITY + CPQ_Test_Setup.SERVICEABLE_LOCATION_PROVINCE_CODE + CPQ_Test_Setup.SERVICEABLE_LOCATION_CLLI_CODE;
            Apexpages.currentPage().getParameters().put('isNewSite', 'false');
            Apexpages.currentPage().getParameters().put('serviceableLocationName', sServiceableLocationName);
            Apexpages.currentPage().getParameters().put('suite', CPQ_Test_Setup.SERVICEABLE_LOCATION_SUITE_FLOOR);
            Apexpages.currentPage().getParameters().put('streetNumber', STREET_NUMBER);
            Apexpages.currentPage().getParameters().put('streetName', CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_NAME);
            Apexpages.currentPage().getParameters().put('streetType', CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_TYPE);
            Apexpages.currentPage().getParameters().put('streetDirection', CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_DIRECTION);
            Apexpages.currentPage().getParameters().put('city', CITY);
            Apexpages.currentPage().getParameters().put('postalCode', CPQ_Test_Setup.SERVICEABLE_LOCATION_POSTAL_CODE);
            Apexpages.currentPage().getParameters().put('cninni', 'false');
            Apexpages.currentPage().getParameters().put('accessTypeGroups', ACCESS_TYPE_GROUP);
            controller.runSearch();
            ServiceableLocationWrapper slw1 = controller.lstServiceableLocations[0];
            Apexpages.currentPage().getParameters().put('SiteId', slw1.getServiceableLocation().id);
            PageReference pg = controller.editServiceableLocation();
            system.assertNotEquals(pg, null);
        
            Apexpages.currentPage().getParameters().put('SiteId', '');
            Apexpages.currentPage().getParameters().put('isNew', 'false');
            pg = controller.editServiceableLocation();
            system.assertEquals(pg, null);
        
            Apexpages.currentPage().getParameters().put('SiteId', '');
            controller.checkExistingQuote();
            system.assertEquals(controller.bDisplayDuplicateMessage, false);
                
            PageReference pgNew = controller.addNewSite();
            system.assertNotEquals(pgNew, null);
            test.stopTest();
        }
    }
    
    static testMethod void checkDuplicateAccountTest() {
        createTestData();
        system.runAs(TestAEUser) {
            test.startTest();
            PageReference pageRef = New PageReference('/apex/addSites?edit=1&retURL=' + String.valueOf(oOpportunity.Id)); 
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController sc = new ApexPages.standardController(oOpportunity);
            SitesController controller = new SitesController(sc);
            String sServiceableLocationName = CPQ_Test_Setup.SERVICEABLE_LOCATION_SUITE_FLOOR + STREET_NUMBER + CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_NAME + CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_TYPE 
                                              + CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_DIRECTION + CITY + CPQ_Test_Setup.SERVICEABLE_LOCATION_PROVINCE_CODE + CPQ_Test_Setup.SERVICEABLE_LOCATION_CLLI_CODE;
            Apexpages.currentPage().getParameters().put('serviceableLocationName', sServiceableLocationName);
            Apexpages.currentPage().getParameters().put('suite', CPQ_Test_Setup.SERVICEABLE_LOCATION_SUITE_FLOOR);
            Apexpages.currentPage().getParameters().put('streetNumber', STREET_NUMBER);
            Apexpages.currentPage().getParameters().put('streetName', CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_NAME);
            Apexpages.currentPage().getParameters().put('streetType', CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_TYPE);
            Apexpages.currentPage().getParameters().put('streetDirection', CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_DIRECTION);
            Apexpages.currentPage().getParameters().put('city', CITY);
            Apexpages.currentPage().getParameters().put('postalCode', CPQ_Test_Setup.SERVICEABLE_LOCATION_POSTAL_CODE);
            Apexpages.currentPage().getParameters().put('cninni', 'false');
            Apexpages.currentPage().getParameters().put('accessTypeGroups', ACCESS_TYPE_GROUP);
            controller.runSearch();
            
            ServiceableLocationWrapper slw1 = controller.lstServiceableLocations[0];
            Apexpages.currentPage().getParameters().put('SiteId', slw1.getServiceableLocation().Id);
            Apexpages.currentPage().getParameters().put('accountName', 'Test Account');
            controller.checkExistingQuote();
            Apexpages.currentPage().getParameters().put('serviceableLocationName', sServiceableLocationName);
            Apexpages.currentPage().getParameters().put('suite', CPQ_Test_Setup.SERVICEABLE_LOCATION_SUITE_FLOOR);
            Apexpages.currentPage().getParameters().put('streetNumber', STREET_NUMBER);
            Apexpages.currentPage().getParameters().put('streetName', CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_NAME);
            Apexpages.currentPage().getParameters().put('streetType', CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_TYPE);
            Apexpages.currentPage().getParameters().put('streetDirection', CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_DIRECTION);
            Apexpages.currentPage().getParameters().put('city', CITY);
            Apexpages.currentPage().getParameters().put('postalCode', CPQ_Test_Setup.SERVICEABLE_LOCATION_POSTAL_CODE);
            Apexpages.currentPage().getParameters().put('cninni', 'false');
            Apexpages.currentPage().getParameters().put('accessTypeGroups', ACCESS_TYPE_GROUP);
            controller.runSearch();
            
            ServiceableLocationWrapper slw2 = controller.lstServiceableLocations[0];
            Apexpages.currentPage().getParameters().put('SiteId', slw2.getServiceableLocation().Id);
            Apexpages.currentPage().getParameters().put('accountName', 'Test Act');
            controller.checkExistingQuote();
            
            Apexpages.currentPage().getParameters().put('SiteId', slw2.getServiceableLocation().Id);
            Apexpages.currentPage().getParameters().put('accountName', 'Test Account');
            controller.checkExistingQuote();
            
            Apexpages.currentPage().getParameters().put('SiteId', oServiceableLocation3.Id);
            Apexpages.currentPage().getParameters().put('accountName', 'Test Act');
            controller.checkExistingQuote();
            
            test.stopTest();
        }
     }
   
   static testMethod void deleteSelectedSitesTest() {
        createTestData();
        system.runAs(TestAEUser) {
            test.startTest();
            PageReference pageRef = New PageReference('/apex/addSites?edit=1&retURL=' + String.valueOf(oOpportunity.Id)); 
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController sc = new ApexPages.standardController(oOpportunity);
            SitesController controller = new SitesController(sc);
            String sServiceableLocationName = CPQ_Test_Setup.SERVICEABLE_LOCATION_SUITE_FLOOR + STREET_NUMBER + CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_NAME + CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_TYPE 
                                            + CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_DIRECTION + CITY + CPQ_Test_Setup.SERVICEABLE_LOCATION_PROVINCE_CODE + CPQ_Test_Setup.SERVICEABLE_LOCATION_CLLI_CODE;
            Apexpages.currentPage().getParameters().put('serviceableLocationName', sServiceableLocationName);
            Apexpages.currentPage().getParameters().put('suite', CPQ_Test_Setup.SERVICEABLE_LOCATION_SUITE_FLOOR);
            Apexpages.currentPage().getParameters().put('streetNumber', STREET_NUMBER);
            Apexpages.currentPage().getParameters().put('streetName', CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_NAME);
            Apexpages.currentPage().getParameters().put('streetType', CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_TYPE);
            Apexpages.currentPage().getParameters().put('streetDirection', CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_DIRECTION);
            Apexpages.currentPage().getParameters().put('city', CITY);
            Apexpages.currentPage().getParameters().put('postalCode', CPQ_Test_Setup.SERVICEABLE_LOCATION_POSTAL_CODE);
            Apexpages.currentPage().getParameters().put('cninni', 'false');
            Apexpages.currentPage().getParameters().put('accessTypeGroups', ACCESS_TYPE_GROUP);
            Apexpages.currentPage().getParameters().put('networkAccessTypes', CPQ_Test_Setup.SERVICEABLE_LOCATION_NETWORK_ACCESS_TYPE);
            
            controller.runSearch();
            ServiceableLocationWrapper slw1 = controller.lstServiceableLocations[0];
            List<Id> lstIds = new List<Id>();
            lstIds.add(slw1.getServiceableLocation().Id);
            Apexpages.currentPage().getParameters().put('SiteIds', slw1.getServiceableLocation().Id);
            controller.deleteServiceableLocations();
            Integer result = [SELECT count() FROM ServiceableLocation__c WHERE id IN :lstIds];
            System.assertEquals(result, 0);
            
            Apexpages.currentPage().getParameters().put('SiteIds', '123');
            controller.deleteServiceableLocations();
            
            test.stopTest();
        }
   }
    
   static testMethod void addSelectedSitesTest() {
        createTestData();
        system.runAs(TestAEUser) {
            test.startTest();
            PageReference pageRef = New PageReference('/apex/addSites?retURL=' + String.valueOf(oOpportunity.Id)); 
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController sc = new ApexPages.standardController(oOpportunity);
            SitesController controller = new SitesController(sc);
            List<Site__c> lstSitesBefore = [Select Id, Name, Street_Name__c FROM Site__c where Street_Name__c =: CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_NAME];
            Integer intSitesBeforeSize = 0;
        
            if (lstSitesBefore != null) {
                intSitesBeforeSize = lstSitesBefore.size();
            }
            Apexpages.currentPage().getParameters().put('serviceableLocationName', '');
            Apexpages.currentPage().getParameters().put('suiteMap', Utils.get15CharId(oServiceableLocation1.Id) + '@' + '2@1');
            Apexpages.currentPage().getParameters().put('suite', '');
            Apexpages.currentPage().getParameters().put('streetNumber', '');
            Apexpages.currentPage().getParameters().put('streetName', CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_NAME);
            Apexpages.currentPage().getParameters().put('streetType', '');
            Apexpages.currentPage().getParameters().put('streetDirection', '');
            Apexpages.currentPage().getParameters().put('city', '');
            Apexpages.currentPage().getParameters().put('postalCode', '');
            Apexpages.currentPage().getParameters().put('cninni', 'true');
            Apexpages.currentPage().getParameters().put('accessTypeGroups', '');
            controller.runSearch();
                        
            Integer i = 0, j = 0;
            for (ServiceableLocationWrapper slw : controller.lstServiceableLocations){
                slw.setIsSelected(true);
                j++;
            }
            controller.addSites();
            List<Site__c> lstSitesAfter = [Select Id, Name, Street_Name__c FROM Site__c where Street_Name__c =: CPQ_Test_Setup.SERVICEABLE_LOCATION_STREET_NAME];
            System.assertEquals(lstSitesAfter.size() - intSitesBeforeSize, j);
            
            controller.getAccessTypeGroups();
            controller.getNetworkAccessTypes();
            String debug = controller.debugSoql;
            controller.returnToOpportunity();

            test.stopTest();
        }
    }
}