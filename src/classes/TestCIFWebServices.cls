/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
/*
========================================================================
Date                        Name                        Status
March 2015         Aakanksha Patel            Modified(For TAG)
========================================================================
*/
@isTest
private class TestCIFWebServices {

    static Id oppId, sl1Id, sl2Id, site1Id, site2Id, site3Id, site4Id, site5Id, site6Id, site7Id; 
    static Id priceBookEntryAccessId1, priceBookEntryInstallId1, quote1Id;
    static Opportunity o, o_carrier;
    static Product2 pAccess, pInstall;
    static Boolean withAssertions = true;
    static Quote q;
    static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();   
    static List<PricebookEntry> pbeList = new List<PricebookEntry>();
    
    static void testData() {
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        
        List <RecordType > lRtp = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Product2']);
        Map <string,Id> mapRTp = new Map <string,id> ();
        
        for (RecordType rtp : lRtp){
            mapRTp.put(rtp.Name,rtp.id);  
        }
       
  /*     TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;
    */   
        Account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        a.parentId= null;
        a.Account_Status__c = 'Assigned';
        insert a;       
        
        o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.Unified_Comm_Collaboration_Estimated__c =20;
        o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.today();
        insert o;
        oppId = o.id;
        
        Pricebook2 sp = new Pricebook2();
        sp = [select id from Pricebook2 where Name = :'Enterprise PriceBook'];
        
        Pricebook2 carrier_sp = new Pricebook2();
        carrier_sp = [select id from Pricebook2 where Name = :'Carrier PriceBook'];
        
        Pricebook2 sp1 = new Pricebook2();
        sp1 = [select id from Pricebook2 where isStandard = true];
        
        q = new Quote(Name='q1', actualTerm__c = '12', Term__c='12-Months', OpportunityId=oppId, Pricebook2Id = sp.id);
        insert q;
        quote1Id = q.Id;
        
        ServiceableLocation__c sl = new ServiceableLocation__c();
        sl.Street_Name__c = 'Somewhere';
        sl.Street_Number__c  = '5';
        sl.Street_Type__c  = 'Ave';
        sl.City__c = 'Coty';
        sl.Postal_Code__c = 'A1A1A1';
        sl.Province_Code__c = 'ON';
        sl.CLLI_Code__c = '123113';
        sl.Access_Type_Group__c = 'NNI';
        sl.Access_Type__c = 'Ethernet Eon';
        insert sl;
        sl1Id = sl.Id;
        
        Site__c s1 = new Site__c();
        s1.Suite_Floor__c = '11a';
        s1.Street_Name__c = 'Somewhere1';
        s1.Street_Number__c  = '5';
        s1.City__c = 'Coty';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.CLLI_SWC__c = '123113';
        s1.ServiceableLocation__c = sl1Id;
        s1.Opportunity__c = oppId;
        insert s1;
        site1Id = s1.Id;        
        
        Quote_Site__c qs = new Quote_Site__c();
        qs.Site__c = site1Id;
        qs.Quote__c = quote1Id;
        insert qs;
        
        /* Products for Original Term */
        // Install Product
        pInstall  = new Product2();
        pInstall.IsActive = true;
        pInstall.Name = 'Fibre';
        pInstall.Charge_Type__c = 'NRC';
        pInstall.Access_Type__c = 'Ethernet EON';
        pInstall.Access_Type_Group__c = 'Fibre';
        pInstall.Category__c = 'Install';
        pInstall.Service_Type__c = '10 Mbps - Ethernet Fibre Network Install - Test';
        pInstall.RecordTypeId = mapRTp.get('Enterprise Products');
        pInstall.Service_Term__c = '12-Months';
        pInstall.Mark_Up_Factor__c = 1.0;
        pInstall.Start_Date__c = Date.today();
        pInstall.End_Date__c = Date.today()+1;
        insert pInstall;
        
        // Access Product
        pAccess  = new Product2();
        pAccess.IsActive = true;
        pAccess.Name = 'Fibre';
        pAccess.Charge_Type__c = 'MRC';
        pAccess.Access_Type__c = 'Ethernet EON';
        pAccess.Access_Type_Group__c = 'Fibre';
        pAccess.Category__c = 'Access';
        pAccess.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
        pAccess.RecordTypeId = mapRTp.get('Enterprise Products');
        pAccess.Service_Term__c = '12-Months';
        pAccess.Product_Install_Link__c = pInstall.Id;
        pAccess.Mark_Up_Factor__c = 1.0;
        pAccess.Start_Date__c = Date.today();
        pAccess.End_Date__c = Date.today()+1;
        insert pAccess;
        
        
        
        PricebookEntry pbeAccessStandard = new PricebookEntry();
        pbeAccessStandard.Pricebook2Id = sp1.id;
        pbeAccessStandard.UnitPrice = 60;
        pbeAccessStandard.Product2Id = pAccess.id;
        pbeAccessStandard.IsActive = true;
        pbeAccessStandard.UseStandardPrice = false;
        pbeList.add(pbeAccessStandard);
        
        PricebookEntry pbeNonStandardAccess = new PricebookEntry();
        pbeNonStandardAccess.Pricebook2Id = sp.id;
        pbeNonStandardAccess.UnitPrice = 60;
        pbeNonStandardAccess.Product2Id = pAccess.id;
        pbeNonStandardAccess.IsActive = true;
        pbeNonStandardAccess.UseStandardPrice = false;
        pbeList.add(pbeNonStandardAccess);
        

        PricebookEntry pbeInstallStandard = new PricebookEntry();
        pbeInstallStandard.Pricebook2Id = sp1.id;
        pbeInstallStandard.UnitPrice = 50;
        pbeInstallStandard.Product2Id = pInstall.id;
        pbeInstallStandard.IsActive = true;
        pbeInstallStandard.UseStandardPrice = false;
        pbeList.add(pbeInstallStandard);
        
        PricebookEntry pbeNonStandardInstall = new PricebookEntry();
        pbeNonStandardInstall.Pricebook2Id = sp.id;
        pbeNonStandardInstall.UnitPrice = 50;
        pbeNonStandardInstall.Product2Id = pInstall.id;
        pbeNonStandardInstall.IsActive = true;
        pbeNonStandardInstall.UseStandardPrice = false;
        pbeList.add(pbeNonStandardInstall);
        insert pbeList;
        priceBookEntryAccessId1 = pbeNonStandardAccess.Id;
        priceBookEntryInstallId1 = pbeNonStandardInstall.Id;
          
        List<QuoteLineItem> qList= new List<QuoteLineItem>();
       QuoteLineItem qliInstall1 = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = priceBookEntryInstallId1, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = false);                       
       qList.add(qliInstall1);
       QuoteLineItem qliAccess1 = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = priceBookEntryAccessId1, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = true, Quote_Line_Item_Install__c = qliInstall1.Id);      
       qList.add(qliAccess1);

       insert qList;
       
       // Sync the Quote for the Opportunity
        o.SyncedQuoteId =  q.id;
        
        update o;
    }
    
    static testMethod void webserviceUnitTests() {
        testData();
        Profile pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
        User user1 = new User(LastName = 'Mark O’Roger', Alias = 'alRoger1', Email='test@Rogertest.com', Username='test@Rogertest1.com', CommunityNickname = 'nickRog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Region__c = 'Alberta',Sub_Region__c ='Central',Channel__c  ='ABS');
        insert user1 ;
        
        CIF__c cif = new CIF__c();
        cif.Opportunity__c = user1.Id; 
        cif.Opportunity_Name__c = o.Id; 
        insert cif;
        
        Test.startTest();
        CIFWebServices.checkForCIFAttachment('' + cif.Id);
        CIFWebServices.validateCIFSites('' + cif.Id);        
        CIFWebServices.createCIF('' + o.Id);
        
        /*
        Id resultId = CIFWebServices.createCustomerCIF(o.Id);
        Opportunity opp = [SELECT Id, Customer_Facing_COP__c FROM Opportunity WHERE Id = :o.Id];
        System.assertEquals(opp.Customer_Facing_COP__c, resultId);
        */
        CIFWebServices.updateBaseURL(cif.Id);
        
        cif.UpdatedCOP__c = true;
        CIFWebServices.resetUpdatedCOP(q.id);
        
      //  CIF__c cifResult = [SELECT Id, UpdatedCOP__c FROM CIF__c WHERE Id = :cif.Id];
    //    System.assertEquals(false, cifResult.UpdatedCOP__c);
        
        List<Id> quoteIds = new List<Id>();
        quoteIds.add('' + q.Id);
        CIFWebServices.updateCOP(quoteIds);
        
        CIFWebServices.encryptCOP('' + cif.Id);
        Test.stopTest();
    }
}