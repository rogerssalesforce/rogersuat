/*
===============================================================================
 Class Name   : Tag_RefreshTeamOnChild_batch
===============================================================================
PURPOSE:    This batch class will cascade AccountTeam from Parent to Child trversing 
            from Child Account to parent accounts.
  
Developer: Deepika Rawat
Date: 06/16/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
06/16/2015          Deepika Rawat               Original Version
===============================================================================
*/
global class Tag_RefreshTeamOnChild_batch implements Database.Batchable<SObject>{
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'Select id, ParentId, OwnerId, District__c, Parent.District__c, Account_Owner_to_MAL__c, Account_Team_to_MAL__c, Parent.OwnerId from Account where ParentId!=null and Account_Team_to_MAL__c=false';
        return Database.getQueryLocator(query); 
    }
    global void execute(Database.BatchableContext BC, List<Account> scope){
        Set<Id> childAccIds = new Set<Id>();
        List<Account> childAccToUpdate = new List<Account>();
        for(Account child :scope){
           childAccIds.add(child.Id);
        }
        for(Account childAcc :scope){
            childAcc.Account_Team_to_MAL__c=true;
            if(childAcc.Parent.District__c!=null)
               childAcc.District__c = childAcc.Parent.District__c;
            if(childAcc.Parent.OwnerId!=null){
               childAcc.OwnerId= childAcc.Parent.OwnerId; 
               childAcc.Account_Owner_to_MAL__c=true;
            }
            childAccToUpdate.add(childAcc);
        }
        TAG_SyncAccountTeamMembers obj = new TAG_SyncAccountTeamMembers();
        obj.addTeamMembersTraversingFromChild(childAccIds);
        
        if(childAccToUpdate.size()>0){
            update childAccToUpdate;
        }
    }
    global void finish(Database.BatchableContext BC){
    }
}