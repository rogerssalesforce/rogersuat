/*********************************************************************************
Class Name      : CPQ_Test_OpportunityTriggerHelper
Description     : This class a test class for CPQ_OpportunityTriggerHelper. 
Created By      : Aakanksha Patel
Created Date    : 19-Oct-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Aakanksha Patel             19-Oct-15            Original version
Joseph Toms                 20-Nov-2015           Added testvalidateOpportunityStageQualify()
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest (seeAllData=False) 
private class CPQ_Test_OpportunityTriggerHelper{
    static User TestAEUser;
    static Account oAccount;
    static Opportunity oOpportunity;
    static Pricebook2 oPriceBook;
    static Apttus_Config2__PriceList__c oPriceList;
    static List<Opportunity> lstOpportunities;
    private static CPQ_Settings__c cs;
 
    /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testMethod void createTestData() {
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');  
        
        Id OppRecordId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Rogers EBU - New').getRecordTypeId(); //EBU - Unified Opportunity
         system.assertNotEquals(OppRecordId, null);
         
         system.runAs(TestAEUser){
           //Custom Setting value
           //CPQ_Settings__c cs = CPQ_Settings__c.getInstance();
            
           //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
         
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            oOpportunity.recordTypeId = OppRecordId;
            
            system.assertNotEquals(oOpportunity.recordTypeId, null);
            
           //Create List of Opportunities (bulk testing)
            lstOpportunities= CPQ_Test_Setup.newlstOpportunity(oAccount, 300);
            
            //Create custom Pricebook 
            oPriceBook = CPQ_Test_Setup.newPriceBook('CPQ Temp PriceBook');
            insert oPriceBook;
            
            cs  = CPQ_Settings__c.getOrgDefaults();
            cs.DefaultOpportunityRecordType__c = 'Rogers EBU - New';
            upsert cs;
        }
    }

    /*********************************************************************************
    Method Name    : testAssignPricebookToOpp
    Description    : Method to test if default PriceBook is assigned to a new Opportunity
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testAssignPricebookToOpp()
    {
        createTestData();
        system.runAs(TestAEUser)
        {   
            test.startTest();
           
           system.assertEquals(cs.DefaultOpportunityRecordType__c, 'Rogers EBU - New');
           system.assertNotEquals(oOpportunity.recordTypeId, null);
           Id OppRecordId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(cs.DefaultOpportunityRecordType__c).getRecordTypeId();
           system.assertNotEquals(OppRecordId, null);

            insert oOpportunity;
            //Quering to get the opp record with defalut pricelist populated          
            Opportunity oOpp = [SELECT Name,PriceBook2Id FROM Opportunity WHERE Id = :oOpportunity.Id];
            system.assertEquals(oOpp.PriceBook2Id,oPriceBook.id);
      
            Test.stopTest();
       }
    } 

     /*********************************************************************************
    Method Name    : testAssignPricebookToOppBulk
    Description    : Method to test if default PriceBook is assigned to Opportunities(Bulk Testing)
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testAssignPricebookToOppBulk()
    {
        createTestData();
        system.runAs(TestAEUser)
        {
            test.startTest();
           
            insert lstOpportunities;
            //Quering to get the opp record with defalut pricelist populated   (Bulkification) 
            for(Opportunity oOpp :[SELECT Name,PriceBook2Id FROM Opportunity WHERE Id = :lstOpportunities])
            {
                 system.assertEquals(oOpp.PriceBook2Id,oPriceBook.id);
            }
            
            Test.stopTest();
       }
    }
    
    /*********************************************************************************
    Method Name    : testvalidateOpportunityStageQualify
    Description    : Method to test check  primary contact exist for Opportunity before changing stage to Qualify 
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    
    static testmethod void testvalidateOpportunityStageQualify(){
    //Create test data 
    createTestData();    
    system.runAs(TestAEUser)
    {
        test.startTest();             
        insert oOpportunity;
        System.AssertNotEquals(oOpportunity.id, null);              
            try {
                // change stage to Qualify 
                oOpportunity.StageName='Qualify';
                update oOpportunity;        
                //An error should have thrown by trigger but not,so fail the test         
                System.AssertNotEquals(oOpportunity.StageName, 'Qualify');         
            }catch(Exception e) {        
                Boolean receivedExpectedError =  e.getMessage().contains(Label.CPQ_NoPrimary_Contact) ? true : false;
                // check expected error received 
                System.AssertEquals(receivedExpectedError, true);
            } 
        // Create OpportunityContactRole and change stage to Qualify .
        Contact oContact =  CPQ_Test_Setup.newContact(oAccount);
        insert oContact;       
        OpportunityContactRole contactRole =  CPQ_Test_Setup.newOpportunityContactRole(oOpportunity,oContact);       
        insert contactRole; 
        //change stage to Qualify       
        oOpportunity.StageName='Qualify';
        oOpportunity.Description='Proceed to next Stage';
        update oOpportunity;  
        // update successfully       
        System.AssertEquals(oOpportunity.StageName, 'Qualify');     
        test.stopTest();
    
    }
    
  }
    /*********************************************************************************
    Method Name    : testvalidateOpportunityStageQualifyBulk
    Description    : Method to test check  primary contact exist for Opportunity before changing stage to Qualify 
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/     
    static testmethod void testvalidateOpportunityStageQualifyBulk(){
        //create test data 
        createTestData();        
        system.runAs(TestAEUser)
        {
        test.startTest();             
        insert lstOpportunities;
        List<Opportunity> newlstOpportunity = new List<Opportunity>();      
        // for each opportunity change stage to Qualify 
        for(Opportunity  oOpportunity1:lstOpportunities){                  
            oOpportunity1.StageName='Qualify';
            newlstOpportunity.add(oOpportunity1);
        }
        try {
            // update opportunities 
            update newlstOpportunity;        
            //An error should have thrown by trigger but not,so fail the test         
            System.AssertNotEquals(oOpportunity.StageName, 'Qualify');         
        }catch(Exception e) {        
            Boolean receivedExpectedError =  e.getMessage().contains(Label.CPQ_NoPrimary_Contact) ? true : false;
            // received expected error 
            System.AssertEquals(receivedExpectedError, true);
        }    
        test.stopTest();        
        }        
    } 
}