/*********************************************************************************
Class Name      : CPQ_Test_SelectPrimaryQuoteDoccontoller  
Description     : This class a test class for CPQ_SelectPrimaryQuoteDoccontoller. 
Created By      : Deepika Rawat
Created Date    : 23-Sep-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
Deepika Rawat              23-Sep-15             Original version
----------------------------------------------------------------------------------            
*********************************************************************************/
@isTest (seeAllData=False) 
private class CPQ_Test_SelectPrimaryQuoteDoccontoller{
    static User TestAEUser;
    static Account oAccount;
    static Opportunity oOpportunity;
    static Apttus_Proposal__Proposal__c oQuote;
    static Attachment attachmentQuote;
    static Apttus_Config2__PriceList__c oPriceList ;
    /*********************************************************************************
    Method Name    : createTestData
    Description    : Method to create Test data
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testMethod void createTestData() {
        //Create System Custom settings 
        CPQ_Test_Setup.createSystemSettingRecord();
        //Create test user 
        TestAEUser= CPQ_Test_Setup.newUser('EBU - Rogers Account Executive');      
        system.runAs(TestAEUser){
            //Create PriceList
            oPriceList = CPQ_Test_Setup.newPriceList();
            insert oPriceList ;
            //Create Account
            oAccount = CPQ_Test_Setup.newAccount('Test Account');
            insert oAccount;
            //Create Opportunity
            oOpportunity = CPQ_Test_Setup.newOpportunity(oAccount);
            insert oOpportunity;
            //Create Quote
            oQuote = CPQ_Test_Setup.newQuote(oOpportunity,oPriceList );
            insert oQuote;
            attachmentQuote = CPQ_Test_Setup.newAttachment(oQuote.id);
            insert attachmentQuote;
        }
    }  
    /*********************************************************************************
    Method Name    : testSelectPrimaryQuoteDoc
    Description    : Method to test user is able to select Primary doc
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testSelectPrimaryQuoteDoc(){
        createTestData();
        system.runAs(TestAEUser){
             test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(oQuote);
            CPQ_SelectPrimaryQuoteDoccontoller cont = new CPQ_SelectPrimaryQuoteDoccontoller(sc);
            cont.getDocs();
            cont.sDocSelected = attachmentQuote.id;
            cont.updateDoc();
            Apttus_Proposal__Proposal__c oQuoteTest = [Select Primary_Quote_Document_ID__c from Apttus_Proposal__Proposal__c where id =: oQuote.id ];
            system.debug('oQuote.Primary_Quote_Document_ID__c**********'+oQuote.Primary_Quote_Document_ID__c);
            system.assert(oQuoteTest.Primary_Quote_Document_ID__c == attachmentQuote.id);
            Test.stopTest();
        }
    }
    /*********************************************************************************
    Method Name    : testRemovePrimaryQuoteDoc
    Description    : Method to test user is able to select Primary doc
    Return Type    : void  
    Parameter      : Nil              
    *********************************************************************************/ 
    static testmethod void testRemovePrimaryQuoteDoc(){
        createTestData();
        oQuote.Primary_Quote_Document_ID__c = attachmentQuote.id;
        update oQuote;
        system.runAs(TestAEUser){
            test.startTest();
            oQuote.Primary_Quote_Document_ID__c =String.valueof(attachmentQuote.id);
            ApexPages.StandardController sc = new ApexPages.StandardController(oQuote);
            CPQ_SelectPrimaryQuoteDoccontoller cont = new CPQ_SelectPrimaryQuoteDoccontoller(sc);
            cont.getDocs();
            cont.sDocSelected = '--None--';
            cont.updateDoc();
            Apttus_Proposal__Proposal__c oQuoteTest = [Select Primary_Quote_Document_ID__c from Apttus_Proposal__Proposal__c where id =: oQuote.id ];
            system.debug('oQuote.Primary_Quote_Document_ID__c**********'+oQuote.Primary_Quote_Document_ID__c);
            system.assert(oQuoteTest.Primary_Quote_Document_ID__c == null);
            Test.stopTest();
        }
    }
}