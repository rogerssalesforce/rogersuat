/*
===============================================================================
 Class Name   : InitialLoadAccountTeam_Schedular
===============================================================================
PURPOSE:    This schedular class runs the InitialLoadAccountTeam_batch class 
            with a batch of 200 records. 

Developer: Deepika Rawat
Date: 01/25/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
01/25/2015          Deepika                     Original Version
===============================================================================
*/
global class InitialLoadAccountTeam_Schedular implements schedulable{

    global void execute(SchedulableContext ctx) {
        InitialLoadAccountTeam_batch sc = new InitialLoadAccountTeam_batch();
        ID batchprocessid = Database.executeBatch(sc,200);
        system.debug('sc*************' +sc);
    }

}