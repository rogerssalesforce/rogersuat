/*********************************************************************************
Class Name      : CPQ_Extension_Quote 
Description     : Class extension for Apttus_Proposal__Proposal__c
Created By      : Alina Balan
Created Date    : 18-Dec-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                      Date                   Description
Alina Balan                18-Dec-15             Original version, US-0382
----------------------------------------------------------------------------------            
*********************************************************************************/

public with sharing class CPQ_Extension_Quote {
    private final ApexPages.StandardController stdController;
    private static final String ABANDON_PARAM = 'abandon';
    private static final String STAGE_ABANDONED = 'abandoned';
    
    /*********************************************************************************
    Method Name    : CPQ_Extension_Quote
    Description    : Class constructor
    Return Type    : 
    Parameter      : 
    *********************************************************************************/
    public CPQ_Extension_Quote(ApexPages.StandardController stdController) {
        this.stdController = stdController;
    }
    
    /*********************************************************************************
    Method Name    : autoRun
    Description    : This method we will invoke on page load
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference autoRun() {
        PageReference pageRef;
        Apttus_Proposal__Proposal__c oQuote = [SELECT Id, Apttus_Proposal__Opportunity__c FROM Apttus_Proposal__Proposal__c WHERE Id =: stdController.getId()];
        String sAbandonParameterValue = ApexPages.currentPage().getParameters().get(ABANDON_PARAM);
        //check if there is "abandon" param in querystring
        Boolean bAbandon = false;
        if (!String.isBlank(sAbandonParameterValue)) {
            bAbandon = Boolean.valueOf(sAbandonParameterValue); 
        } 
        //check if abandon param has "true" value
        if (bAbandon) {
            oQuote.Apttus_Proposal__Approval_Stage__c = STAGE_ABANDONED;
        } else { //do synchronization actions
            oQuote.Synced_with_Opportunity__c = true;
        }
        try {
            update oQuote;
        } catch(Exception ex) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
            return null;
        }
        //if the quote is abbandoned, return
        if (bAbandon) {
            pageRef = new PageReference(Site.getPathPrefix() + '/' + oQuote.Id);
            pageRef.setRedirect(true);
            return pageRef;
        }
        //desynchronize the other quotes
        List<Apttus_Proposal__Proposal__c> lstQuotes = [SELECT Id FROM Apttus_Proposal__Proposal__c WHERE Apttus_Proposal__Opportunity__c =: oQuote.Apttus_Proposal__Opportunity__c AND Id !=: oQuote.Id];
        //iterating through all quotes with the same opportunity id
        for (Apttus_Proposal__Proposal__c quote : lstQuotes) {
            quote.Synced_with_Opportunity__c = false;
        }
        //check if the quotes list has any element
        if (lstQuotes.size() > 0) {
            try {
                update lstQuotes;
            } catch(Exception ex) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
                return null;
            }
        }
        // Redirect the user back to the apttus page
        pageRef = new PageReference(Site.getPathPrefix() + '/apex/Apttus_QPConfig__ProposalSyncWithOpportunity?id=' + oQuote.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }
}