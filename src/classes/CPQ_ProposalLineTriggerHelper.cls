/*********************************************************************************
Class Name      : CPQ_ProposalLineTriggerHelper
Description     : This class is used as a helper class for Proposal Line Trigger 
Created By      : Akshay Ahluwalia
Created Date    : 07-Jan-15
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Akshay Ahluwalia            07-Jan-15            Created class for Proposal Line Item Trigger
*********************************************************************************/

public with sharing class CPQ_ProposalLineTriggerHelper {
    
    public static boolean blTriggersPreviouslyRan = false;
    
    /*********************************************************************************
    Method Name    : populateCSOWoNProposal
    Description    : Method to Populate CSOW flag on Proposal, if any product has CSOW Required Checked
    Return Type    : Void
    Parameter      : List<Apttus_Proposal__Proposal_Line_Item__c>
    *********************************************************************************/
    public static void updateCSOWOnProposal(List<Apttus_Proposal__Proposal_Line_Item__c> lstProposalLines){
    
        System.Debug('CPQ_ProposalLineTriggerHelper.updateCSOWOnProposal() Limits.getQueries START ' + Limits.getQueries());
        System.Debug('CPQ_ProposalLineTriggerHelper.updateCSOWOnProposal() Limits.getLimitQueries START ' + Limits.getLimitQueries());
    
        Set<Apttus_Proposal__Proposal__c> setProposal = new Set<Apttus_Proposal__Proposal__c>();
        Set<Apttus_Proposal__Proposal_Line_Item__c> setProposalLineItem = new Set<Apttus_Proposal__Proposal_Line_Item__c>();
        lstProposalLines = [SELECT Apttus_Proposal__Proposal__c,COPTeam__c ,Apttus_Proposal__Product__r.COP_Team__c, IsCustomProduct__c, Apttus_Proposal__Product__r.CSOW_Required__c, Apttus_Proposal__Proposal__r.CSOW_Required__c FROM Apttus_Proposal__Proposal_Line_Item__c WHERE id in : Trigger.newMap.keySet()];
        //Iterating over the Proposal lines to check if CSOW is Required for Product
        for(Apttus_Proposal__Proposal_Line_Item__c proposalLine : lstProposalLines)
        {
             // Updating the Proposal with CSOW required as true if the any of the product requires CSOW
             if(proposalLine.Apttus_Proposal__Product__r.CSOW_Required__c && !proposalLine.Apttus_Proposal__Proposal__r.CSOW_Required__c)
             {
                 setProposal.add(new Apttus_Proposal__Proposal__c(id = proposalLine.Apttus_Proposal__Proposal__c, CSOW_Required__c = True));
             }
        }
        update new List<Apttus_Proposal__Proposal__c>(setProposal);
    
        System.Debug('CPQ_ProposalLineTriggerHelper.updateCSOWOnProposal() Limits.getQueries END ' + Limits.getQueries());
        System.Debug('CPQ_ProposalLineTriggerHelper.updateCSOWOnProposal() Limits.getLimitQueries END ' + Limits.getLimitQueries());
    }
    
 
    /*********************************************************************************
    Method Name    : updateProductFamiliesOnProposal
    Description    : Method to Populate Product Families of the products added to the Proposal
                     This is the background field/method to distinguish Products based on the families
                     in the templates
    Return Type    : Void
    Parameter      : List<Apttus_Proposal__Proposal_Line_Item__c>
    *********************************************************************************/
     public static void updateProductFamiliesOnProposal(List<Apttus_Proposal__Proposal_Line_Item__c> lstProposalLines){
     
        System.Debug('CPQ_ProposalLineTriggerHelper.updateProductFamiliesOnProposal() Limits.getQueries START ' + Limits.getQueries());
        System.Debug('CPQ_ProposalLineTriggerHelper.updateProductFamiliesOnProposal() Limits.getLimitQueries START ' + Limits.getLimitQueries());
        
        List<Id> lstProductIds = new list<Id>();
        Set<Id> setProposalIds = new Set<Id>();
        List<Id> lstProposalWithCSOWIds = new List<Id>();
        //Looping over the Proposal lines to fetch proposal ids and product ids
        for(Apttus_Proposal__Proposal_Line_Item__c proposalLine : lstProposalLines){
          lstProductIds.add(proposalLine.Apttus_Proposal__Product__c);
          setProposalIds.add(proposalLine.Apttus_Proposal__Proposal__c);
        }
        
        List<Product2> lstProducts = [select id, family from product2 where id in: lstProductIds];
        Map<Id, Set<String>> mapProposalProductFamilies = new Map<Id, Set<String>>();
        //looping over the products and proposals to Add the Product Family to the proposal
        for(Id proposalId : setProposalIds){
            for(product2 product : lstProducts){
                // Adding the product family to the Corresponding Proposal
                if(mapProposalProductFamilies != null && mapProposalProductFamilies.get(proposalId) != null){
                   mapProposalProductFamilies.get(proposalId).add(product.family);
                }
                else if(mapProposalProductFamilies.get(proposalId) == null){
                    Set<String> setProductFamilies = new Set<String>();
                    setProductFamilies.add(product.family);
                    mapProposalProductFamilies.put(proposalId, setProductFamilies);
                }
            }
        }
        List<Apttus_Proposal__Proposal__c> lstProposal = [select id, Product_Famlies__c from Apttus_Proposal__Proposal__c where id in:setProposalIds];
        //Looping over the proposals to add Product family to the field
        for(Apttus_Proposal__Proposal__c proposal : lstProposal){
            String families = '';
            for(String family : mapProposalProductFamilies.get(proposal.id)){
                if(families == ''){
                    families = family; 
                }
                else{
                    families = families + ';'+ family;   
                }
            }
             proposal.Product_Famlies__c = families;
        }
        update lstProposal;
    
        System.Debug('CPQ_ProposalLineTriggerHelper.updateProductFamiliesOnProposal() Limits.getQueries END ' + Limits.getQueries());
        System.Debug('CPQ_ProposalLineTriggerHelper.updateProductFamiliesOnProposal() Limits.getLimitQueries END ' + Limits.getLimitQueries());
    }
}