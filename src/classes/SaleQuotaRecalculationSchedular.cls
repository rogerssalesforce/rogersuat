/*Class Name :SaleQuotaRecalculationSchedular.
 *Description : .
 *Created By : Deepika Rawat.
 *Created Date :05/11/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
global class SaleQuotaRecalculationSchedular implements schedulable{

    global void execute(SchedulableContext ctx) {
        integer SFCheckPoint = Integer.ValueOf(Static_Data_Utilities__c.getInstance('Recalculation Period').Value__c);
        date dT1=system.Today().addDays(-SFCheckPoint);
        SalesForecastRecalculationBatch salesQuotaObj = new SalesForecastRecalculationBatch();
        if(!Test.isRunningTest()){
        salesQuotaObj.Query = 'select id,LastModifiedDate,Owner.FirstName,Owner.LastName,Quota_Family__c,Forecast_Month__c,OwnerID,Quantity__c,Revenue__c,OwnerMonthFamily__c FROM Sales_Quota__c';
        }
        else{
        salesQuotaObj.Query = 'select id,LastModifiedDate,Owner.FirstName,Owner.LastName,Quota_Family__c,Forecast_Month__c,OwnerID,Quantity__c,Revenue__c,OwnerMonthFamily__c FROM Sales_Quota__c limit 2';
        }
        ID salesQuotaBatchProcessId = Database.executeBatch(salesQuotaObj,200);
    }
}