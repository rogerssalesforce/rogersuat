/* 
* Name          : Shift_Web2LeadExtension_Test
* Author        : Shift CRM
* Description   : Test coverage for Extension to web to lead
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 03/06/2014       Hermes      1.0         Initial
*/

@isTest
private class Shift_Web2LeadExtension_Test {

    static testMethod void Web2LeadTest() {
        Lead lead1 = new Lead(Company='Test Company', LastName = 'Test1', Phone='416-555-1212', Referral_Agent_ID__c='123456', City='city', Country='country', PostalCode='M1C3E2', State='Ontario', Street='Sesame Street', Same_As_Head_Office__c = true, Status='Open',Lead_Type__c='Wireless' );
        PageReference pageRef = New PageReference('/apex/WebToLead2'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(lead1);
        Web2LeadExtension controller = new Web2LeadExtension(sc);
        controller.webLead.Same_As_Head_Office__c = true;
        controller.saveLead();
        controller.getFrenchEmployeeSegments();
        controller.getFrenchSalutations();
     }
     
     static testMethod void Web2LeadTestFrench1() {
        Lead lead1 = new Lead(Company='Test Company', LastName = 'Test1', Phone='416-555-1212', Referral_Agent_ID__c='123456', City='city', Country='country', PostalCode='M1C3E2', State='ON', Street='Sesame Street', Same_As_Head_Office__c = true, Lead_Rep_Type__c = 'Solutions d’affaires Rogers', Status='Open',Lead_Type__c='Wireless' );
        PageReference pageRef = New PageReference('/apex/WebToLeadFrench'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(lead1);
        Web2LeadExtension controller = new Web2LeadExtension(sc);
        controller.webLead.Same_As_Head_Office__c = true;
        controller.saveLead();
        controller.getFrenchEmployeeSegments();
        controller.getFrenchSalutations();
     }
     
     static testMethod void Web2LeadTestFrench2() {
        Lead lead1 = new Lead(Company='Test Company', Phone='416-555-1212', LastName = 'Test1', Referral_Agent_ID__c='123456', City='city', Country='country', PostalCode='M1C3E2', State='Ontario', Street='Sesame Street', Same_As_Head_Office__c = true, Lead_Rep_Type__c = 'Sans-fil', Status='Open',Lead_Type__c='Wireless' );
        PageReference pageRef = New PageReference('/apex/WebToLeadFrench'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(lead1);
        Web2LeadExtension controller = new Web2LeadExtension(sc);
        controller.webLead.Same_As_Head_Office__c = true;
        controller.saveLead();
        controller.getFrenchEmployeeSegments();
        controller.getFrenchSalutations();
     }
     
     static testMethod void Web2LeadTestFrench3() {
        Lead lead1 = new Lead(Company='Test Company', Phone='416-555-1212', LastName = 'Test1', Referral_Agent_ID__c='123456', City='city', Country='country', PostalCode='M1C3E2', State='ON', Street='Sesame Street', Same_As_Head_Office__c = true, Lead_Rep_Type__c = 'Autre', Status='Open',Lead_Type__c='Wireless' );
        PageReference pageRef = New PageReference('/apex/WebToLeadFrench'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(lead1);
        Web2LeadExtension controller = new Web2LeadExtension(sc);
        controller.webLead.Same_As_Head_Office__c = true;
        controller.saveLead();
        controller.getFrenchEmployeeSegments();
        controller.getFrenchSalutations();
     }
     
     static testMethod void Web2LeadTestFrench4() {
        Lead lead1 = new Lead(Company='Test Company', Phone='416-555-1212', LastName = 'Test1', Referral_Agent_ID__c='123456', City='city', Country='country', PostalCode='M1C3E2', State='ON', Street='Sesame Street', Same_As_Head_Office__c = true, Lead_Rep_Type__c = 'MDU – Unité résidentielle multiple', Status='Open',Lead_Type__c='Wireless' );
        PageReference pageRef = New PageReference('/apex/WebToLeadFrench'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(lead1);
        Web2LeadExtension controller = new Web2LeadExtension(sc);
        controller.webLead.Same_As_Head_Office__c = true;
        controller.saveLead();
        controller.getFrenchEmployeeSegments();
        controller.getFrenchSalutations();
     }
     
     static testMethod void Web2LeadTestFrench5() {
        Lead lead1 = new Lead(Company='Test Company', Phone='416-555-1212', LastName = 'Test1', Referral_Agent_ID__c='123456', City='city', Country='country', PostalCode='M1C3E2', State='ON', Street='Sesame Street', Same_As_Head_Office__c = true, Lead_Rep_Type__c = 'CMA – Comptes commerciaux majeurs', Status='Open',Lead_Type__c='Wireless' );
        PageReference pageRef = New PageReference('/apex/WebToLeadFrench'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(lead1);
        Web2LeadExtension controller = new Web2LeadExtension(sc);
        controller.webLead.Same_As_Head_Office__c = true;
        controller.saveLead();
        controller.getFrenchEmployeeSegments();
        controller.getFrenchSalutations();
     }
     
     static testMethod void Web2LeadTestFrench6() {
        Lead lead1 = new Lead(Company='Test Company', Phone='416-555-1212', LastName = 'Test1', Referral_Agent_ID__c='123456', City='city', Country='country', PostalCode='M1C3E2', State='ON', Street='Sesame Street', Same_As_Head_Office__c = true, Lead_Rep_Type__c = 'Câble', Status='Open',Lead_Type__c='Wireless' );
        PageReference pageRef = New PageReference('/apex/WebToLeadFrench'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(lead1);
        Web2LeadExtension controller = new Web2LeadExtension(sc);
        controller.webLead.Same_As_Head_Office__c = true;
        controller.saveLead();
        controller.getFrenchEmployeeSegments();
        controller.getFrenchSalutations();
     }
     
     static testMethod void Web2LeadTestFrench7() {
        Lead lead1 = new Lead(Company='Test Company', Phone='416-555-1212', LastName = 'Test1', Referral_Agent_ID__c='123456', City='city', Country='country', PostalCode='M1C3E2', State='ON', Street='Sesame Street', Same_As_Head_Office__c = true, Lead_Rep_Type__c = 'Techniciens sur le terrain', Status='Open',Lead_Type__c='Wireless' );
        PageReference pageRef = New PageReference('/apex/WebToLeadFrench'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(lead1);
        Web2LeadExtension controller = new Web2LeadExtension(sc);
        controller.webLead.Same_As_Head_Office__c = true;
        controller.saveLead();
        controller.getFrenchEmployeeSegments();
        controller.getFrenchSalutations();
     }
}