/*********************************************************************************
Class Name      : CPQ_CartSubmitForApprovalCont
Description     : This class is used as a controller class for CPQ_CartSubmitForApproval
                  page.
Created By      : Deepika Rawat 
Created Date    : 29-Oct-15  
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------  
Deepika Rawat             29-Oct-15              Original Version 
Aakanksha Patel           18-Jan-16              Added Validation error
Akshay Ahluwalia          20-Jan-16              Added Method to set approval Level 
*********************************************************************************/
public with sharing class CPQ_CartSubmitForApprovalCont {
    public String sConfigRequestId {get;set;}
    public String sCartId {get;set;}
    public String sProposalId;
    public List<Apttus_Proposal__Proposal__c> lstProposal;
    public  List<Apttus_Config2__LineItem__c> lstLineItems = new List<Apttus_Config2__LineItem__c>();
    Set<String> setAllowedProfiles = new Set<String>();
    String sUserProfileName;
    String sflow;
    Date TodayDate = Date.Today();
    Boolean bFlag = FALSE;

    /*********************************************************************************
    Method Name    : CPQ_CartSubmitForApprovalCont
    Description    : Constructor method
    Return Type    : 
    Parameter      : 
    *********************************************************************************/
    public CPQ_CartSubmitForApprovalCont (){
        system.debug('CART: Cart Submitted For Approval');
        String sURLId = ApexPages.currentPage().getParameters().get('id');
        Schema.DescribeSObjectResult proposalSchema = Apttus_Proposal__Proposal__c.sObjectType.getDescribe();
        String sProposalkeyPrefix = proposalSchema.getKeyPrefix();
        sUserProfileName= [Select name from profile where id=:userinfo.getProfileId()].name;
        CPQ_Settings__c cpqSetting = CPQ_Settings__c.getOrgDefaults();
        List<String> lstAEProfiles = cpqSetting.RogersAE__c.Split(';');
        List<String> lstAEProfiles2 = cpqSetting.RogersAE_2__c.Split(';');
        List<String> lstSEProfiles = cpqSetting.RogersSE__c.Split(';');
        List<String> lstDDProfiles = cpqSetting.RogersDealDesk__c.Split(';');
        List<String> lstSysAdminProfiles = cpqSetting.System_Administrator__c.Split(';');
        setAllowedProfiles.addAll(lstAEProfiles);
        setAllowedProfiles.addAll(lstAEProfiles2);
        setAllowedProfiles.addAll(lstSEProfiles);
        setAllowedProfiles.addAll(lstDDProfiles);
        setAllowedProfiles.addAll(lstSysAdminProfiles );
        //Check if Submit for approval is clicked from Cart or Quote Deatail page
        if(!sURLId.startswith(sProposalkeyPrefix)){
            sCartId = ApexPages.currentPage().getParameters().get('id'); 
            sConfigRequestId = ApexPages.currentPage().getParameters().get('configrequestid');  
            sflow  = ApexPages.currentPage().getParameters().get('flow');
            Apttus_Config2__ProductConfiguration__c oProdConfig = [Select id,Apttus_Config2__BusinessObjectId__c from Apttus_Config2__ProductConfiguration__c where id =: sCartId];   
            sProposalId = oProdConfig.Apttus_Config2__BusinessObjectId__c;  
            
            lstLineItems = [Select id,Apttus_Config2__StartDate__c, Apttus_Config2__LineType__c, Apttus_Config2__OptionId__r.Approval_Level__c, Apttus_Config2__ProductId__r.Approval_Level__c from Apttus_Config2__LineItem__c where Apttus_Config2__ConfigurationId__c =:oProdConfig.id];
        }
        else if(sURLId.startswith(sProposalkeyPrefix)){
            sProposalId = sURLId;   
            List<Apttus_Config2__ProductConfiguration__c> lstProdConfig = [Select id,  Apttus_QPConfig__Proposald__c, Apttus_Config2__Status__c from  Apttus_Config2__ProductConfiguration__c where Apttus_QPConfig__Proposald__c =:sProposalId and (Apttus_Config2__Status__c ='Saved' or Apttus_Config2__Status__c ='Finalized') ];
            if(lstProdConfig!=null && lstProdConfig.size()>1){
                for(Apttus_Config2__ProductConfiguration__c config :lstProdConfig){
                    if(config.Apttus_Config2__Status__c=='Saved'){
                        sCartId = config.id;
                    }
                }
            }
            else
                sCartId = lstProdConfig[0].id;
        }
        lstProposal= [select id, RecordTypeId, Approval_Level__c, Apttus_Proposal__Approval_Stage__c from Apttus_Proposal__Proposal__c where id=:sProposalId];
        
        //Iterating through Line Items associated with the cart to check the Start Date of each item: Aakanksha
        for(Apttus_Config2__LineItem__c oQuoteLI :lstLineItems)
        {
            if(oQuoteLI.Apttus_Config2__StartDate__c < TodayDate)
                    bFlag = TRUE;
        }
        
        
    }
    /*********************************************************************************
    Method Name    : submitQuoteForApproval
    Description    : Based on Quote lifecycle allows user to Submit Quote for Approval
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference submitQuoteForApproval(){
        SetApprovalLevel(lstLineItems, lstProposal[0]);
       //Allow SE, AE, DD and System Admin to Submit quote for approval if Approval stage is Approval Required; else display error message
        if(setAllowedProfiles.contains(sUserProfileName) && lstProposal.size()>0 && lstProposal[0].Apttus_Proposal__Approval_Stage__c == 'Approval Required')
        {
            //Added a condition to validate if Estimated start date of the Products added in the cart have more than today's date : Aakanksha Patel
            if(bFlag == FALSE)
            {
                //Apttus_CPQApi.CPQ.FinalizeCartRequestDO request = new Apttus_CPQApi.CPQ.FinalizeCartRequestDO();
                //request.CartId = sCartId; 
                //Apttus_CPQApi.CPQ.FinalizeCartResponseDO resp = Apttus_CPQApi.CPQWebService.finalizeCart(request); 
                PageReference refreshPage = new //PageReference(Site.getPathPrefix()+'/apex/Apttus_Approval__ApprovalContextSubmit?sObjectType=Apttus_Proposal__Proposal__c&sObjectId='+sProposalId);
                PageReference(Site.getPathPrefix()+'/apex/Apttus_CQApprov__CartApprovals?finalizeClass=Apttus_QPConfig.ProposalActionCallback&action=submit&configRequestId='+sConfigRequestId+'&headerSObjectId='+sCartId+'&Id='+sCartId+'&includeHeaderApprovals=true&returnButtonLabel=Return+to+Cart&flow='+sflow);
                return refreshPage;
            }
            else 
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CPQ_CartSubmitForApprovalStartDateError));
                return null;
            }
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CPQ_CartSubmitForAprrovalErrorMessage));
            return null;
        }  
    }
    /*********************************************************************************
    Method Name    : backToCart
    Description    : Returns back to Cart
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public PageReference backToCart(){
        PageReference refreshPage = new PageReference(Site.getPathPrefix()+'/apex/apttus_config2__Cart?autoFinalize=true&configRequestId='+sConfigRequestId+'&flow='+sflow+'&id='+sCartId);
        return refreshPage;
    }
    
     /*********************************************************************************
    Method Name    : SetApprovalLevel
    Description    : Sets the approval level on proposal based on cart line items
    Return Type    : PageReference
    Parameter      : 
    *********************************************************************************/
    public void SetApprovalLevel(List<Apttus_Config2__LineItem__c> lstLineItems, Apttus_Proposal__Proposal__c oProposal){
        system.debug('CART: SetApprovalLevel');
        Decimal imaxDiscountPercentage = 0;
        Integer iHighestApprovalLevel = 0;
        CPQ_Settings__c cs = CPQ_Settings__c.getInstance();
        Set<String> setProductFamiliesToSelect  = new Set<String>();
        // Reseting the Proposal value, to avoid stale data.
        oProposal.Approval_Level__c = null;
        List<CPQ_QuotePricingAggregationThreshold__c> lstPricingThreshold = [select Approval_Level__c, price_type__c, threshold_type__c, threshold__c, Type_of_Product__c from CPQ_QuotePricingAggregationThreshold__c order by Approval_Level__c DESC];
        // Setting the Product families
        // Code commented as US-534 is still not finalized.
        /*
        for(CPQ_QuotePricingAggregationThreshold__c csPricingThreshold : lstPricingThreshold ){
            setProductFamiliesToSelect(csPricingThreshold.Type_of_Product__c);
        }*/
        
        // Checking the Maximum Approval level of the Custom Setting Records
        if(lstPricingThreshold != null && lstPricingThreshold.size()>0){
            iHighestApprovalLevel = Integer.ValueOf(lstPricingThreshold[0].Approval_Level__c);
        }
        
        // Code commented as US-534 is still not finalized.
        /*for(Apttus_Config2__LineItem__c lineItem : lstLineItems){
            if(lineItem.MaxDiscountExcessPercent__c > imaxDiscountPercentage){
                imaxDiscountPercentage = lineItem.MaxDiscountExcessPercent__c;
            }
        }
        if(imaxDiscountPercentage > 0){
            oProposal.Approval_Level__c = 5;
            if(oProposal.Approval_Level__c >= iHighestApprovalLevel){
                    update oProposal;
                    return;
                }
        }*/
        
        //Looping over the line items to get the Line Item which requires maximum Approval
        for(Apttus_Config2__LineItem__c lineItem : lstLineItems){
            //Checking if approval level of the line is more than the set approval level
            if((!lineItem.Apttus_Config2__LineType__c.equalsIgnorecase('Option')) && (oProposal.Approval_Level__c == null || lineItem.Apttus_Config2__ProductId__r.Approval_Level__c > oProposal.Approval_Level__c )){
                oProposal.Approval_Level__c =  lineItem.Apttus_Config2__ProductId__r.Approval_Level__c;
                //Checking if the Approval level is equal to highest approval level
                if(oProposal.Approval_Level__c == iHighestApprovalLevel){
                    update oProposal;
                    return;
                }
            }
            else if(lineItem.Apttus_Config2__LineType__c.equalsIgnorecase('Option') && lineItem.Apttus_Config2__OptionId__r.Approval_Level__c > oProposal.Approval_Level__c) {
                oProposal.Approval_Level__c = lineItem.Apttus_Config2__OptionId__r.Approval_Level__c;
                //Checking if the Approval level is equal to highest approval level
                if(oProposal.Approval_Level__c == iHighestApprovalLevel){
                    update oProposal;
                    return;
                }
            }
            
        }
        
        // Code commented as US-534 is still not finalized.
        /*String sPriceListsConsidered = cs.QuotePricingAggregationPriceLists__c;
        Set<String> setPricingAggregationPriceLists = new Set<String>();
        if(sPriceListsConsidered !=null){
            List<String> lstPriceList = sPriceListsConsidered.split(';');
            setPricingAggregationPriceLists = new Set<String>(lstPriceList);
        }
       // AggregateResult lstAgreegateResult = [SELECT Rogers_ProductFamily__c, Apttus_Config2__ProductId__r.TypeOfProduct__c, Apttus_Config2__PriceType__c, SUM(Apttus_Config2__NetPrice__c) NetPriceSum, SUM(Apttus_Config2__ExtendedPrice__c) ExtendedPriceSum FROM Apttus_Config2__LineItem__c WHERE Apttus_QPConfig__Proposald__c = : oProposal.id AND Rogers_ProductFamily__c IN : setProductFamiliesToSelect GROUP BY Rogers_ProductFamily__c, Apttus_Config2__ProductId__r.TypeOfProduct__c, Apttus_Config2__PriceType__c];
        for(String priceList : setPricingAggregationPriceLists){
            if(oProposal.Apttus_Proposal__Opportunity__r.Price_List__c.equalsIgnoreCase(priceList)){
                                
                //TODO point 8
                //update oProposal;
                break;
            }
        }*/
        
        update oProposal;
    }
    
}