public with sharing class userTriggerHelper{
    @future
    public static void handleInactiveUsers(Set<Id> lstUserIds){
        List<Assignment_Request_Item__c> lstARI = new List<Assignment_Request_Item__c>();
        List<Assignment_Request_Error__c> lstARError = new List<Assignment_Request_Error__c>();        
        lstARI = [Select Id,Account__c,Action__c, Name, Assignment_Request__c, Assignment_Request__r.Name, New_Owner__r.Name, CreatedBy.Email, Business_Case__c, Current_Owner__c, New_Owner__c, New_Owner__r.IsActive, Member__c,Member__r.Name, Member__r.IsActive, MSD__c,New_District__c, Requested_Item_Type__c, Role__c, Shared_MSD__c, Territory_City__c, NPA__c,Territory_Region__c from Assignment_Request_Item__c where (New_Owner__c in :lstUserIds or Member__c in :lstUserIds ) and (Status__c ='Submitted' or Status__c='Approved')];
        Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
        List<String> toAddresses = new List<String>();
        if(TAGsettings.TAG_User_Inactive_Email__c!=null && TAGsettings.TAG_User_Inactive_Email__c.startsWith('005')){
            List<User> usr = [Select id, Email from User where id = :TAGsettings.TAG_User_Inactive_Email__c and isActive=true];
            toAddresses.add(usr[0].Email);
        }
        else if ((TAGsettings.TAG_User_Inactive_Email__c!=null && TAGsettings.TAG_User_Inactive_Email__c.startsWith('00G'))){
            List<GroupMember> lstGroup = [SELECT Groupid, Group.Name, UserOrGroupId FROM GroupMember WHERE Groupid= : TAGsettings.TAG_User_Inactive_Email__c];
            Set<Id> usrIds = new Set<Id>();
            for(GroupMember grp: lstGroup){
                usrIds.add(grp.UserOrGroupId);
            }
            list<User> ulist = [Select id, Email from User where id in :usrIds and isActive=true];
            for(User u: ulist){
                toAddresses.add(String.valueof(u.Email));
            }
        }

        if(lstARI.size()>0 ){
            system.debug('lstARI*******'+lstARI);
            Assignment_Request_Error__c errRec;
            String header = 'Inactive User Name, Inactive User Id , Assignment Request, Assignment Request Item \n';
            String finalstr = header ;
            String recordString;
            List<processinstanceworkitem> pendingWorkItems = [SELECT processinstance.targetobjectid,processinstance.targetobject.name, processinstance.targetobject.type, processinstance.CreatedDate, processinstance.CreatedById, processinstance.CreatedBy.Name FROM processinstanceworkitem WHERE processinstance.targetobject.type = 'Assignment_Request_Item__c' and processinstance.targetobjectid in : lstARI ];
            List<Approval.ProcessRequest> processRequests = new List<Approval.ProcessRequest>();
            Approval.ProcessWorkitemRequest req2;
            for(processinstanceworkitem item:pendingWorkItems){
                req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments(Label.UserTrigger_InactiveUser);
                req2.setAction('Reject'); //This is the action that is approve in your case, you can set it to Reject also
                req2.setWorkitemId(item.id);
                processRequests.add(req2);

            }

            for(Assignment_Request_Item__c ari : lstARI){
               ari.Status__c = 'Cancelled'; 
               if(ari.New_Owner__c!=null && !ari.New_Owner__r.isActive)
                    recordString = '"'+ari.New_Owner__r.Name+'","' +ari.New_Owner__c+'","' +ari.Assignment_Request__r.Name+'","'+ari.Name+'"\n';
                if(ari.Member__c!=null && !ari.Member__r.isActive)
                    recordString = '"'+ari.Member__r.Name+'","' +ari.Member__r+'","' +ari.Assignment_Request__r.Name+'","'+ari.Name+'"\n';
                finalstr = finalstr +recordString;
            }
            List<Approval.ProcessResult>  result2 =  Approval.process(processRequests,false);
            update lstARI;
            
         //   if(!test.isRunningTest())
          //  {
            
            Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
            blob csvBlob = Blob.valueOf(finalstr);
            string csvname= 'TAG Inactive User List.csv';
            csvAttc.setFileName(csvname);
            csvAttc.setBody(csvBlob);
            Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
           
            String body = 'Hi,<br/><br/>Some TAG Users are now inactive in the system. Attached is the list of Users and Assignment Request related to them. <br/><br/>'; 
            body += 'These Assignment Request Items are now cancelled in the system and will no longer be processed.';
            body += '<br/> <br/> Regards, <br/> Salesforce Team'; 
            email.setToAddresses(toAddresses); 
            email.setSubject('Notification : TAG Users Inactive');        
            email.setSaveAsActivity(false);  
            email.setHtmlBody(body); 
            
                OrgWideEmailAddress orgEmailId=[select id, Address, DisplayName from OrgWideEmailAddress limit 1] ;
                email.setOrgWideEmailAddressId(orgEmailId.id); 
            
                email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
                
          //  }    
        }
    }
    
    public static LIST<id> UsersAccOwnerType(Set<Id> lstUserIdsOwnerType){
    
        List<id> ListUIds = new List<id>();
        List<Assignment_Request_Item__c> lstARI = new List<Assignment_Request_Item__c>();
        lstARI = [Select Id,Account__c,Action__c, Name, Assignment_Request__c, Assignment_Request__r.Name, New_Owner__r.Name, CreatedBy.Email, Business_Case__c, Current_Owner__c, Current_Owner__r.IsActive, Current_Owner__r.Owner_Type__c, New_Owner__c, New_Owner__r.IsActive, Member__c,Member__r.Name, Member__r.IsActive, MSD__c,New_District__c, Requested_Item_Type__c, Role__c, Shared_MSD__c, Territory_City__c, NPA__c,Territory_Region__c from Assignment_Request_Item__c where (New_Owner__c in :lstUserIdsOwnerType or Current_Owner__c in :lstUserIdsOwnerType ) and (Current_Owner__r.Owner_Type__c ='Account') and (Status__c ='Submitted')];
        list<id> ListTemp = new list<id>(lstUserIdsOwnerType);
        
        if(!lstARI.isEmpty())
        {
            for(integer i =0; i<ListTemp.size() ; i++)
            {
                for(Assignment_Request_Item__c ari : lstARI)
                {
                    if(ListTemp[i] == ari.New_Owner__c || ListTemp[i] == ari.Current_Owner__c)
                        ListUIds.add(ListTemp[i]);
                }
            }
                
        }
        return ListUIds;
     
    }
    
    
}