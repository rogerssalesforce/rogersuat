/*=============================================================================
 Class Name   : updateAccountTeamAccess
===============================================================================
PURPOSE:    This class is to give or reomve access from Updated AccountTeam.
            This class will be called from below classes:
            1. reCalculateAccountTeam_batch
  
Developer: Deepika Rawat
Date: 02/26/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
02/26/2015           Deepika Rawat               Original Version
===============================================================================*/
public class updateAccountTeamAccess{
    public Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();

     /***********************************************************************************************************************************************
     * @description: This method is used to Access to the AccountTeam on below objects
                        and custom objects :
                        1. Account
                        2. Contact
                        3. Opportunity
                        4. Case
                        5. Wireline_Contract__c
                        6. Billing_Account__c
                        7. MSD_Code__c
                        8. MSD__c
                        9. Shared_MSD_Accounts__c
                       10. Shared_MSD_Code
                       based on custom settings Team_Account_Access__c and Team_Custom_Object_Access__c on
                       Team_Assignment_Governance_Settings__c

     * @param: MAP<Id, Set<Id>>.This is the Map of Account IDs and TeamMember UserIds to whom give Access on 
                                the above Objects
     * @return type: void.
     ************************************************************************************************************************************************/
    public void giveAccesstoTeam(Map<Id, Set<Id>> mapAccountNewTeamId){
        if(mapAccountNewTeamId!=null){
            //Sharing Contact, Opportunity and Case with Team Members
            List<Account> listAccounts = [Select Id, OwnerId from Account where id in : mapAccountNewTeamId.keySet()];
            List<AccountShare> newAccshares = new List<AccountShare>();
            for(Account acc: listAccounts){
                Set<Id> teamMembers = mapAccountNewTeamId.get(acc.Id);
                for(Id memId : teamMembers){
                    if(memId!=acc.OwnerId){
                        AccountShare share = new AccountShare();
                        share.AccountId =acc.Id;
                        share.UserOrGroupId=memId;
                        share.AccountAccessLevel =  TAGsettings.Team_Account_Access__c;
                        share.ContactAccessLevel=TAGsettings.Team_Account_Access__c;
                        share.OpportunityAccessLevel=TAGsettings.Team_Account_Access__c;
                        share.CaseAccessLevel=TAGsettings.Team_Account_Access__c;
                        newAccshares.add(share); 
                    }
                }            
            }
            //Sharing Wireline_Contract__c with Team Members
            List<Wireline_Contract__c> lstWireline = [Select id, OwnerId, Account_Name__c from Wireline_Contract__c where Account_Name__c in:mapAccountNewTeamId.keySet()];
            Map<Id, Set<Id>> mapAccWireLine = new Map<Id, Set<Id>>();
            Map<Id, Id> mapWireLineOwnerID = new Map<Id, Id>();
            for(Wireline_Contract__c wObj : lstWireline){
                Set<Id> wObjIds = mapAccWireLine.get(wObj.Account_Name__c);
                if(wObjIds==null)
                    wObjIds = new Set<Id>();
                wObjIds.add(wObj.id);
                mapAccWireLine.put(wObj.Account_Name__c, wObjIds);
                mapWireLineOwnerID.put(wObj.id,wObj.OwnerID);
            }
            List<Wireline_Contract__share> newWirelineshares = new List<Wireline_Contract__share>();
            for(Id accId: mapAccountNewTeamId.keySet()){
                Set<Id> wirelineIds = mapAccWireLine.get(accId);
                if(wirelineIds!=null && wirelineIds.size()>0){
                    Set<Id> teamMembers = mapAccountNewTeamId.get(accId);
                    for(Id wObj : wirelineIds){
                        for(Id memId : teamMembers){
                            if(mapWireLineOwnerID.get(wObj)!=memId){
                                Wireline_Contract__share share = new Wireline_Contract__share();
                                share.AccessLevel =TAGsettings.Team_Custom_Object_Access__c;
                                share.ParentID=wObj;
                                share.UserOrGroupId=memId;
                                share.RowCause=Schema.Wireline_Contract__share.RowCause.AccountTeamMember__c;
                                newWirelineshares.add(share);   
                            }
                        } 
                    }
                }
            }
            //Sharing Billing_Account__c with Team Members
            List<Billing_Account__c> lstBillingAccounts = [Select id, Account__c,OwnerId from Billing_Account__c where Account__c in:mapAccountNewTeamId.keySet()];
            Map<Id, Set<Id>> mapAccBilling = new Map<Id, Set<Id>>();
            Map<Id, Id> mapBillingOwnerID = new Map<Id, Id>();
            for(Billing_Account__c baObj : lstBillingAccounts){
                Set<Id> baObjIds = mapAccBilling.get(baObj.Account__c);
                if(baObjIds==null)
                    baObjIds = new Set<Id>();
                baObjIds.add(baObj.id);
                mapAccBilling.put(baObj.Account__c, baObjIds);
                mapBillingOwnerID.put(baObj.Id,baObj.OwnerId);
            }
            List<Billing_Account__share> newBillingAccshares = new List<Billing_Account__share>();
            for(Id accId: mapAccountNewTeamId.keySet()){
                Set<Id> BAIds = mapAccBilling.get(accId);
                if(BAIds!=null && BAIds.size()>0){
                    Set<Id> teamMembers = mapAccountNewTeamId.get(accId);
                    for(Id baObj : BAIds){
                        for(Id memId : teamMembers){
                            if(mapBillingOwnerID.get(baObj)!=memId){
                                Billing_Account__share share = new Billing_Account__share();
                                share.AccessLevel =TAGsettings.Team_Custom_Object_Access__c;
                                share.ParentID=baObj;
                                share.UserOrGroupId=memId;
                                share.RowCause=Schema.Billing_Account__share.RowCause.AccountTeamMember__c;
                                newBillingAccshares.add(share);  
                            }
                        } 
                    }
                }  
            }
            //Sharing MSD_Code__c with Team Members
            List<MSD_Code__c> lstMSDCode = [Select id, Account__c, OwnerId from MSD_Code__c where Account__c in:mapAccountNewTeamId.keySet()];
            Map<Id, Id> mapMSDCodeOwnerID = new Map<Id, Id>();
            Map<Id, Set<Id>> mapAccMSDCode = new Map<Id, Set<Id>>();
            for(MSD_Code__c msd : lstMSDCode){
                Set<Id> msdIds = mapAccMSDCode.get(msd.Account__c);
                if(msdIds==null)
                    msdIds = new Set<Id>();
                msdIds.add(msd.id);
                mapAccMSDCode.put(msd.Account__c, msdIds);
                mapMSDCodeOwnerID.put(msd.id,msd.ownerId);
            }
            List<MSD_Code__share> newMSDshares = new List<MSD_Code__share>();

            for(Id accId: mapAccountNewTeamId.keySet()){
                Set<Id> msdIds = mapAccMSDCode.get(accId);
                if(msdIds!=null && msdIds.size()>0){
                    Set<Id> teamMembers = mapAccountNewTeamId.get(accId);
                    for(Id msdObj : msdIds){
                        for(Id memId : teamMembers){
                            if(mapMSDCodeOwnerID.get(msdObj)!=memId){
                            MSD_Code__share share = new MSD_Code__share();
                            share.AccessLevel =TAGsettings.Team_Custom_Object_Access__c;
                            share.ParentID=msdObj;
                            share.UserOrGroupId=memId;
                            share.RowCause=Schema.MSD_Code__share.RowCause.AccountTeamMember__c;
                            newMSDshares.add(share); 
                            }
                        } 
                    }
                }     
            }
            //Sharing MSD__c with Team Members
            List<MSD__c> lstMSDSnapshot = [Select id, AccountId__c,OwnerId from MSD__c where AccountId__c in:mapAccountNewTeamId.keySet()];
            Map<Id, Id> mapMSDOwnerID = new Map<Id, Id>();
            Map<Id, Set<Id>> mapAccMSDSnapshot = new Map<Id, Set<Id>>();
            for(MSD__c msd : lstMSDSnapshot){
                Set<Id> msdIds = mapAccMSDSnapshot.get(msd.AccountId__c);
                if(msdIds==null)
                    msdIds = new Set<Id>();
                msdIds.add(msd.id);
                mapAccMSDSnapshot.put(msd.AccountId__c, msdIds);
                mapMSDOwnerID.put(msd.id,msd.OwnerId);
            }
            List<MSD__share> newMSDSnapshotshares = new List<MSD__share>();

            for(Id accId: mapAccountNewTeamId.keySet()){
                Set<Id> msdIds = mapAccMSDSnapshot.get(accId);
                if(msdIds!=null && msdIds.size()>0){
                    Set<Id> teamMembers = mapAccountNewTeamId.get(accId);
                    for(Id msdObj : msdIds){
                        for(Id memId : teamMembers){
                            if(mapMSDOwnerID.get(msdObj)!=memId){
                                MSD__share share = new MSD__share();
                                share.AccessLevel =TAGsettings.Team_Custom_Object_Access__c;
                                share.ParentID=msdObj;
                                share.UserOrGroupId=memId;
                                share.RowCause=Schema.MSD__share.RowCause.AccountTeamMember__c;
                                newMSDSnapshotshares.add(share);  
                            }                  
                        } 
                    }
                }
            }
            //Sharing Shared_MSD_Accounts__c with Team Members
            List<Shared_MSD_Accounts__c> lstSharedAccMSD = [Select id, Account__c,OwnerId, Shared_MSD_Code__r.id, Shared_MSD_Code__r.Ownerid from Shared_MSD_Accounts__c where Account__c in:mapAccountNewTeamId.keySet()];
            Map<Id, Set<Id>> mapAccASharedMSD = new Map<Id, Set<Id>>();
            Map<Id, Id> mapSharedMSDAccOwnerID = new Map<Id, Id>();
            Map<Id, Id> mapSharedMSDOwnerID = new Map<Id, Id>();
            Map<Id, Set<Id>> mapSharedMSD = new Map<Id, Set<Id>>();
            Set<Id> sharedMSDIds = new Set<Id>();
            for(Shared_MSD_Accounts__c msd : lstSharedAccMSD){
                Set<Id> sharedMSDId = mapAccASharedMSD.get(msd.Account__c);
                if(sharedMSDId==null)
                    sharedMSDId = new Set<Id>();
                sharedMSDId.add(msd.id);
                mapAccASharedMSD.put(msd.Account__c, sharedMSDId);

                Set<Id> msdIds = mapSharedMSD.get(msd.Account__c);
                if(msdIds==null)
                    msdIds = new Set<Id>();
                msdIds.add(msd.Shared_MSD_Code__r.id);
                mapSharedMSD.put(msd.Account__c, msdIds);
                
                sharedMSDIds.add(msd.Shared_MSD_Code__r.id);
                mapSharedMSDAccOwnerID.put(msd.id,msd.OwnerId);
                mapSharedMSDOwnerID.put(msd.Shared_MSD_Code__r.id,msd.Shared_MSD_Code__r.OwnerId);
            }
            List<Shared_MSD_Accounts__share> newSharedAccMSDshares = new List<Shared_MSD_Accounts__share>();
            for(Id accId: mapAccountNewTeamId.keySet()){
                Set<Id> msdIds = mapAccASharedMSD.get(accId);
                if(msdIds!=null && msdIds.size()>0){
                    Set<Id> teamMembers = mapAccountNewTeamId.get(accId);
                    for(Id msdObj : msdIds){
                        for(Id memId : teamMembers){
                            if(mapSharedMSDAccOwnerID.get(msdObj)!=memId){
                                Shared_MSD_Accounts__share share = new Shared_MSD_Accounts__share();
                                share.AccessLevel =TAGsettings.Team_Custom_Object_Access__c;
                                share.ParentID=msdObj;
                                share.UserOrGroupId=memId;
                                share.RowCause=Schema.Shared_MSD_Accounts__share.RowCause.AccountTeamMember__c;
                                newSharedAccMSDshares.add(share);      
                            }
                        } 
                    }
                }
            }
            //Sharing Shared_MSD_Code with Team Members
            List<Shared_MSD_Code__share> newSharedMSDshares = new List<Shared_MSD_Code__share>();
            for(Id accId: mapAccountNewTeamId.keySet()){
                Set<Id> msdIds = mapSharedMSD.get(accId);
                if(msdIds!=null && msdIds.size()>0){
                    Set<Id> teamMembers = mapAccountNewTeamId.get(accId);
                    for(Id msdObj : msdIds){
                        for(Id memId : teamMembers){
                            if(mapSharedMSDOwnerID.get(msdObj)!=memId){
                                Shared_MSD_Code__share share = new Shared_MSD_Code__share();
                                share.AccessLevel =TAGsettings.Team_Custom_Object_Access__c;
                                share.ParentID=msdObj;
                                share.UserOrGroupId=memId;
                                share.RowCause=Schema.Shared_MSD_Code__share.RowCause.AccountTeamMember__c;
                                newSharedMSDshares.add(share); 
                            }
                        } 
                    }
                }
            }
            system.debug('newAccshares*****'+newAccshares);
            system.debug('newWirelineshares*****'+newWirelineshares);
            system.debug('newSharedAccMSDshares*****'+newSharedAccMSDshares);
            system.debug('newSharedMSDshares*****'+newSharedMSDshares);
            system.debug('newMSDSnapshotshares*****'+newMSDSnapshotshares);
            system.debug('newBillingAccshares*****'+newBillingAccshares);
            system.debug('newMSDshares*****'+newMSDshares);
            if(newAccshares!=null && newAccshares.size()>0)
                Database.insert(newAccshares);
            if(newWirelineshares!=null && newWirelineshares.size()>0)
                 Database.insert(newWirelineshares);
            if(newSharedAccMSDshares!=null && newSharedAccMSDshares.size()>0)
                 Database.insert(newSharedAccMSDshares);
            if(newSharedMSDshares!=null && newSharedMSDshares.size()>0)
                 Database.insert(newSharedMSDshares);
            if(newMSDSnapshotshares!=null && newMSDSnapshotshares.size()>0)
                 Database.insert(newMSDSnapshotshares);
            if(newBillingAccshares!=null && newBillingAccshares.size()>0)
                 Database.insert(newBillingAccshares);
            if(newMSDshares!=null && newMSDshares.size()>0)
                 Database.insert(newMSDshares);
        }          
    }

     /***********************************************************************************************************************************************
     * @description: This method is used to remove access from the AccountTeam on below objects
                        and custom objects :
                        1. Wireline_Contract__c
                        2. Billing_Account__c
                        3. MSD_Code__c
                        4. MSD__c
                        5. Shared_MSD_Accounts__c
                        6. Shared_MSD_Code
                       based on custom settings Team_Account_Access__c and Team_Custom_Object_Access__c on
                       Team_Assignment_Governance_Settings__c

     * @param: MAP<Id, Set<Id>>. This is the Map of Account IDs and TeamMember UserIds from whom to remove   
                                 accessfrom the above Objects
     * @return type: void.
     ************************************************************************************************************************************************/
    public void removeAccessFromTeam(Map<Id,Set<Id>> mapAccountNewTeamId){
        if(mapAccountNewTeamId.size()>0){
            //Remove sharing from Wireline_Contract__c with Team Members
            List<Wireline_Contract__share> existingWirelineshares = [select UserOrGroupId, ParentID, Parent.Account_Name__c, RowCause from Wireline_Contract__share where RowCause= 'AccountTeamMember__c' and Parent.Account_Name__c in : mapAccountNewTeamId.keySet()];
            Map<String, Wireline_Contract__share> mapAccUserWirelinShare =new Map<String, Wireline_Contract__share>();
            for(Wireline_Contract__share wShare : existingWirelineshares){
                String uniq = String.valueof(wShare.ParentID) + String.valueof(wShare.UserOrGroupId);
                mapAccUserWirelinShare.put(uniq,wShare);
            }
            Set<Wireline_Contract__share> setWirelineSharestoDelete = new Set<Wireline_Contract__share>();
            List<Wireline_Contract__share> lstWirelineSharestoDelete = new List<Wireline_Contract__share>();
            List<Wireline_Contract__c> lstWireline = [Select id, Account_Name__c from Wireline_Contract__c where Account_Name__c in:mapAccountNewTeamId.keySet()];
            Map<Id, Set<Id>> mapAccWireLine = new Map<Id, Set<Id>>();
            for(Wireline_Contract__c wObj : lstWireline){
                Set<Id> wObjIds = mapAccWireLine.get(wObj.Account_Name__c);
                if(wObjIds==null)
                    wObjIds = new Set<Id>();
                wObjIds.add(wObj.id);
                mapAccWireLine.put(wObj.Account_Name__c, wObjIds);
            }
            List<Wireline_Contract__share> newWirelineshares = new List<Wireline_Contract__share>();
            for(Id accId: mapAccountNewTeamId.keySet()){
                Set<Id> wirelineIds = mapAccWireLine.get(accId);
                if(wirelineIds!=null && wirelineIds.size()>0){
                    Set<Id> teamMembers = mapAccountNewTeamId.get(accId);
                    for(Id wObj : wirelineIds){
                        for(Id memId : teamMembers){
                            String uniq = String.valueof(wObj) + String.Valueof(memID);
                            Wireline_Contract__share delShare= mapAccUserWirelinShare.get(uniq);
                            if(delShare!=null)
                                setWirelineSharestoDelete.add(delShare);                 
                        } 
                    }
                }
            }
            if(setWirelineSharestoDelete.size()>0)
                lstWirelineSharestoDelete.addAll(setWirelineSharestoDelete);

            //Remove sharing from Billing_Account__c with Team Members
            List<Billing_Account__share> existingBAshares = [select UserOrGroupId, ParentID, RowCause, Parent.Account__c from Billing_Account__share where RowCause= 'AccountTeamMember__c' and Parent.Account__c in:mapAccountNewTeamId.keyset()];
            Set<Billing_Account__share> setBASharestoDelete = new Set<Billing_Account__share>();
            List<Billing_Account__share> lstBASharestoDelete = new List<Billing_Account__share>();
            Map<String, Billing_Account__share> mapAccUserBAShare =new Map<String, Billing_Account__share>();
            for(Billing_Account__share baShare : existingBAshares){
                String uniq = String.valueof(baShare.ParentID) + String.valueof(baShare.UserOrGroupId);
                mapAccUserBAShare.put(uniq,baShare);
            }
             List<Billing_Account__c> lstBillingAccounts = [Select id, Account__c from Billing_Account__c where Account__c in:mapAccountNewTeamId.keySet()];
            Map<Id, Set<Id>> mapAccBilling = new Map<Id, Set<Id>>();
            for(Billing_Account__c baObj : lstBillingAccounts){
                Set<Id> baObjIds = mapAccBilling.get(baObj.Account__c);
                if(baObjIds==null)
                    baObjIds = new Set<Id>();
                baObjIds.add(baObj.id);
                mapAccBilling.put(baObj.Account__c, baObjIds);
            }
            List<Billing_Account__share> newBillingAccshares = new List<Billing_Account__share>();
            for(Id accId: mapAccountNewTeamId.keySet()){
                Set<Id> BAIds = mapAccBilling.get(accId);
                if(BAIds!=null && BAIds.size()>0){
                    Set<Id> teamMembers = mapAccountNewTeamId.get(accId);
                    for(Id baObj : BAIds){
                        for(Id memId : teamMembers){
                            String uniq =  String.valueof(baObj) + String.Valueof(memID);
                            Billing_Account__share delShare= mapAccUserBAShare.get(uniq);
                            if(delShare!=null)
                                setBASharestoDelete.add(delShare);                    
                        } 
                    }
                }  
            }
            if(setBASharestoDelete.size()>0)
                lstBASharestoDelete.addAll(setBASharestoDelete);

            //Remove sharing from MSD_Code__c with Team Members
            List<MSD_Code__share> existingMSDCodeshares = [select UserOrGroupId, ParentID, RowCause, Parent.Account__c from MSD_Code__share where RowCause= 'AccountTeamMember__c' and Parent.Account__c in:mapAccountNewTeamId.keyset()];
            Set<MSD_Code__share> setMSDSharestoDelete = new Set<MSD_Code__share>();
            List<MSD_Code__share> lstMSDSharestoDelete = new List<MSD_Code__share>();
            Map<String, MSD_Code__share> mapAccUserMSDShare =new Map<String, MSD_Code__share>();
            for(MSD_Code__share msdShare : existingMSDCodeshares){
                String uniq = String.valueof(msdShare.ParentID) + String.valueof(msdShare.UserOrGroupId);
                mapAccUserMSDShare.put(uniq,msdShare);
            }
             List<MSD_Code__c> lstMSDCode = [Select id, Account__c from MSD_Code__c where Account__c in:mapAccountNewTeamId.keySet()];
            Map<Id, Set<Id>> mapAccMSDCode = new Map<Id, Set<Id>>();
            for(MSD_Code__c msd : lstMSDCode){
                Set<Id> msdIds = mapAccMSDCode.get(msd.Account__c);
                if(msdIds==null)
                    msdIds = new Set<Id>();
                msdIds.add(msd.id);
                mapAccMSDCode.put(msd.Account__c, msdIds);
            }
            List<MSD_Code__share> newMSDshares = new List<MSD_Code__share>();

            for(Id accId: mapAccountNewTeamId.keySet()){
                Set<Id> msdIds = mapAccMSDCode.get(accId);
                if(msdIds!=null && msdIds.size()>0){
                    Set<Id> teamMembers = mapAccountNewTeamId.get(accId);
                    for(Id msdObj : msdIds){
                        for(Id memId : teamMembers){
                           String uniq =  String.valueof(msdObj) + String.Valueof(memID);
                            MSD_Code__share delShare= mapAccUserMSDShare.get(uniq);
                            if(delShare!=null)
                                setMSDSharestoDelete.add(delShare);           
                        } 
                    }
                }     
            }
            if(setMSDSharestoDelete.size()>0)
                lstMSDSharestoDelete.addAll(setMSDSharestoDelete);

            //Remove MSD__c sharing with Team Members
            List<MSD__share> existingMSDSnapshotshares = [select UserOrGroupId, ParentID, RowCause,Parent.AccountId__c from MSD__share where RowCause= 'AccountTeamMember__c' and Parent.AccountId__c in:mapAccountNewTeamId.keyset()];
            Set<MSD__share> setMSDSnapshotSharestoDelete = new Set<MSD__share>();
            List<MSD__share> lstMSDSnapshotSharestoDelete = new List<MSD__share>();
            Map<String, MSD__share> mapAccUserMSDSnapshotShare =new Map<String, MSD__share>();
            for(MSD__share msdShare : existingMSDSnapshotshares){
                String uniq = String.valueof(msdShare.ParentID) + String.valueof(msdShare.UserOrGroupId);
                mapAccUserMSDSnapshotShare.put(uniq,msdShare);
            }
            List<MSD__c> lstMSDSnapshot = [Select id, AccountId__c from MSD__c where AccountId__c in:mapAccountNewTeamId.keySet()];
            Map<Id, Set<Id>> mapAccMSDSnapshot = new Map<Id, Set<Id>>();
            for(MSD__c msd : lstMSDSnapshot){
                Set<Id> msdIds = mapAccMSDSnapshot.get(msd.AccountId__c);
                if(msdIds==null)
                    msdIds = new Set<Id>();
                msdIds.add(msd.id);
                mapAccMSDSnapshot.put(msd.AccountId__c, msdIds);
            }
            List<MSD__share> newMSDSnapshotshares = new List<MSD__share>();

            for(Id accId: mapAccountNewTeamId.keySet()){
                Set<Id> msdIds = mapAccMSDSnapshot.get(accId);
                if(msdIds!=null && msdIds.size()>0){
                    Set<Id> teamMembers = mapAccountNewTeamId.get(accId);
                    for(Id msdObj : msdIds){
                        for(Id memId : teamMembers){
                           String uniq =  String.valueof(msdObj) + String.Valueof(memID);
                            MSD__share delShare= mapAccUserMSDSnapshotShare.get(uniq);
                            if(delShare!=null)
                                setMSDSnapshotSharestoDelete.add(delShare);                     
                        } 
                    }
                }
            }
            if(setMSDSnapshotSharestoDelete.size()>0)
                lstMSDSnapshotSharestoDelete.addAll(setMSDSnapshotSharestoDelete);

            //Remove Shared_MSD_Accounts__c sharing with Team Members
            List<Shared_MSD_Accounts__share> existingSharedAccMSDshares = [select UserOrGroupId, ParentID, RowCause,Parent.Account__c from Shared_MSD_Accounts__share where RowCause= 'AccountTeamMember__c' and Parent.Account__c in:mapAccountNewTeamId.keyset()];
            Set<Shared_MSD_Accounts__share> setSharedAccMSDSharestoDelete = new Set<Shared_MSD_Accounts__share>();
            List<Shared_MSD_Accounts__share> lstSharedAccMSDSharestoDelete = new List<Shared_MSD_Accounts__share>();
            Map<String, Shared_MSD_Accounts__share> mapAccUserSharedAccMSDShare =new Map<String, Shared_MSD_Accounts__share>();
            for(Shared_MSD_Accounts__share msdShare : existingSharedAccMSDshares){
                String uniq = String.valueof(msdShare.ParentID) + String.valueof(msdShare.UserOrGroupId);
                mapAccUserSharedAccMSDShare.put(uniq,msdShare);
            }
            List<Shared_MSD_Accounts__c> lstSharedAccMSD = [Select id, Account__c, Shared_MSD_Code__r.id from Shared_MSD_Accounts__c where Account__c in:mapAccountNewTeamId.keySet()];
            Map<Id, Set<Id>> mapAccASharedMSD = new Map<Id, Set<Id>>();
            Map<Id, Set<Id>> mapSharedMSD = new Map<Id, Set<Id>>();
            Set<Id> sharedMSDIds = new Set<Id>();
            for(Shared_MSD_Accounts__c msd : lstSharedAccMSD){
                Set<Id> sharedMSDId = mapAccASharedMSD.get(msd.Account__c);
                if(sharedMSDId==null)
                    sharedMSDId = new Set<Id>();
                sharedMSDId.add(msd.id);
                mapAccASharedMSD.put(msd.Account__c, sharedMSDId);

                Set<Id> msdIds = mapSharedMSD.get(msd.Account__c);
                if(msdIds==null)
                    msdIds = new Set<Id>();
                msdIds.add(msd.Shared_MSD_Code__r.id);
                mapSharedMSD.put(msd.Account__c, msdIds);
                
                sharedMSDIds.add(msd.Shared_MSD_Code__r.id);
            }
            List<Shared_MSD_Accounts__share> newSharedAccMSDshares = new List<Shared_MSD_Accounts__share>();
            for(Id accId: mapAccountNewTeamId.keySet()){
                Set<Id> msdIds = mapAccASharedMSD.get(accId);
                if(msdIds!=null && msdIds.size()>0){
                    Set<Id> teamMembers = mapAccountNewTeamId.get(accId);
                    for(Id msdObj : msdIds){
                        for(Id memId : teamMembers){
                            String uniq =  String.valueof(msdObj) + String.Valueof(memID);
                            Shared_MSD_Accounts__share delShare= mapAccUserSharedAccMSDShare.get(uniq);
                            if(delShare!=null)
                                setSharedAccMSDSharestoDelete.add(delShare);                
                        } 
                    }
                }
            }
            if(setSharedAccMSDSharestoDelete.size()>0)
                lstSharedAccMSDSharestoDelete.addAll(setSharedAccMSDSharestoDelete);

            //Sharing Shared_MSD_Code with Team Members
            
            List<Shared_MSD_Code__share> existingSharedMSDshares = [select UserOrGroupId, ParentID, RowCause from Shared_MSD_Code__share where RowCause= 'AccountTeamMember__c' and ParentId in:sharedMSDIds];
            List<Shared_MSD_Code__share> lstSharedMSDSharestoDelete = new List<Shared_MSD_Code__share>();
            Set<Shared_MSD_Code__share> setSharedMSDSharestoDelete = new Set<Shared_MSD_Code__share>();
            Map<String, Shared_MSD_Code__share> mapAccUserSharedMSDShare =new Map<String, Shared_MSD_Code__share>();
            for(Shared_MSD_Code__share msdShare : existingSharedMSDshares){
                String uniq = String.valueof(msdShare.ParentID) + String.valueof(msdShare.UserOrGroupId);
                mapAccUserSharedMSDShare.put(uniq,msdShare);
            }
            List<Shared_MSD_Code__share> newSharedMSDshares = new List<Shared_MSD_Code__share>();
            for(Id accId: mapAccountNewTeamId.keySet()){
                Set<Id> msdIds = mapSharedMSD.get(accId);
                if(msdIds!=null && msdIds.size()>0){
                    Set<Id> teamMembers = mapAccountNewTeamId.get(accId);
                    for(Id msdObj : msdIds){
                        for(Id memId : teamMembers){
                            String uniq =  String.valueof(msdObj) + String.Valueof(memID);
                            Shared_MSD_Code__share delShare= mapAccUserSharedMSDShare.get(uniq);
                            if(delShare!=null)
                                setSharedMSDSharestoDelete.add(delShare);   
                        } 
                    }
                }
            }
            if(setSharedMSDSharestoDelete.size()>0)
                lstSharedMSDSharestoDelete.addAll(setSharedMSDSharestoDelete);
            if(lstWirelineSharestoDelete!=null && lstWirelineSharestoDelete.size()>0)
                 Database.delete(lstWirelineSharestoDelete);
            if(lstBASharestoDelete!=null && lstBASharestoDelete.size()>0)
                 Database.delete(lstBASharestoDelete);
            if(lstMSDSharestoDelete!=null && lstMSDSharestoDelete.size()>0)
                 Database.delete(lstMSDSharestoDelete);
            if(lstMSDSnapshotSharestoDelete!=null && lstMSDSnapshotSharestoDelete.size()>0)
                 Database.delete(lstMSDSnapshotSharestoDelete);
            if(lstSharedAccMSDSharestoDelete!=null && lstSharedAccMSDSharestoDelete.size()>0)
                 Database.delete(lstSharedAccMSDSharestoDelete);
            if(lstSharedMSDSharestoDelete!=null && lstSharedMSDSharestoDelete.size()>0)
                 Database.delete(lstSharedMSDSharestoDelete);
        }
    }
}