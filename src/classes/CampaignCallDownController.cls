/**

Last update 2010-07-15
labs@salesforce.com

* with sharing added
* squashed bugs around single / multiple updates
* added limits to SOQL -- 100 for Campaigns 1000 for Campaign Members
* fixed custom settings manager
* updated Static Resources 

*/
public without sharing class CampaignCallDownController{
    
    //setup appropriate variables
    private Boolean             hasCampaignID = false;
    private string              thisCampaign;
    private Lead[]              lead; 
    private Contact[]           contact;
    private CampaignMember[]    members; //the members for that campaign
    private Lead[]              relevantLeads; 
    public List<leadPlusTaskPlusContact>   leadPlusTasks;
    private Map<String, Boolean>            callDownColumns;    
    private List<Campaign>      activeCampaigns;
    public String               camp { get;set; } //campaign ID
    public String               status{ get;set; } //status ID
    public String               SearchText{ get;set; } //Search String
    private Map<String, String> cStatusMap;
    private boolean             cStatusMapIsSet;
    public string               campBackup;
    public string               backRef{ get; set; }
    public list<SelectOption>   owners{get;set;}
    public string               ownerId{ get; set; }
    public Map<String,Map<string,boolean>>  headerMap= new Map<String,Map<string,boolean>> ();
    List<id> lstCampIds;
    public string               sortColumn{ get; set; }
    public Map<id,list<SelectOption>>  mapCampSatus;
    public CampaignColumns__c mc {get;set;}
    public Boolean myFlag{get; set;}
    private Map<id, String> mapCampMemStatus;
    Private string sortingField;
    private string sortingOrder;
    public boolean AccLimitExceed {get;set;}
    /**
    *Constructor
    */
    public CampaignCallDownController(){
        
        relevantLeads = new Lead[ 0 ];
        leadPlusTasks = new leadPlusTaskPlusContact[]{};
        callDownColumns = new Map<String, Boolean>();
        mapCampMemStatus= new Map<id, String>();
        
        //get columns settings and set to Map
        if( ApexPages.currentPage().getUrl() != null ){
            camp = ApexPages.currentPage().getUrl().substring( 1 );
        }
        campBackup = camp;
        backRef = '/apex/CampaignCallDownSettings?var=/' + camp;
        cStatusMap = new Map<String, String>();
        cStatusMapIsSet = false;
        owners = new list<SelectOption>();
        lstCampIds= new List<id>();
        
    }
    public void campaignCheck(){
        mc = CampaignColumns__c.getInstance();
        if(mc==null){
            mc= new CampaignColumns__c();
            mc.SetupOwnerId = Userinfo.getUserId();
            insert mc;
        }
    }        
    /**
    *Return if has campaign id
    *@return hasCampaignID boolean flag
    */
    public Boolean gethasCampaignID(){
        return hasCampaignID;
    }
    
    /**
    *Return the list of owners
    *@return owners
    */
    public List<SelectOption> getowners(){
        return owners;
    }
    
    
    /**
    *Update all status and log , retuen page reference for redirection
    *@return null
    */
    public PageReference updateAll(){
        List<leadPlusTaskPlusContact> cleanRecords = new List<leadPlusTaskPlusContact>();
        List<leadPlusTaskPlusContact> changedRecords = new List<leadPlusTaskPlusContact>();
        List<Task> tList= new List<Task>();
        task tNew;
        
        List<CampaignMember> cmList = new List<CampaignMember>();
         for( leadPlusTaskPlusContact current :leadPlusTasks ){
          //status
          String cmStatus;
          if(current.statusUpdate != null && current.statusUpdate != current.memStatus){
            //update status
            CampaignMember cm = [Select Id, Status From CampaignMember Where id = :current.MemberId limit 1];
            cm.status = current.statusUpdate;
            cmStatus=cm.status;
            cmList.add(cm);
            
          }else{
              cmStatus=mapCampMemStatus.get(current.MemberId);
          }
          if (current.t.Description != '' && current.t.Description != null) {
          //log a call
            tNew= new task();
            
            If(current.t.id !=null){
                tNew.ActivityDate = System.today();
                tNew.WhoId = current.t.WhoId;
                tNew.Status = current.t.Status;
                //tNew.Subject = 'Call '+cmStatus+' '+current.campName;
        		tNew.Subject = 'Call '+current.campName; //Ramiya Kandeepan - removed status from subject on Sept 10 based on  REQ – 00810 
                tNew.Disposition__c = cmStatus; //Ramiya Kandeepan - added on Sept 10 based on  REQ – 00810
                tNew.Description =current.t.Description;
                tNew.WhatId = current.t.WhatId;
                tList.add(tNew);
            }else{
                //current.t.Subject = 'Call '+cmStatus+' '+current.campName;
 				current.t.Subject = 'Call '+current.campName;  //Ramiya Kandeepan - removed status from subject on Sept 10 based on  REQ – 00810 
                current.t.Disposition__c = cmStatus; //Ramiya Kandeepan - added on Sept 10 based on  REQ – 00810
                tList.add(current.t);
            }
            
          }

           changedRecords.add(current);
         }
         
         //Update CampaignMembers 
         if(cmList.size() > 0) {
             update cmList;
         }
         cmList = null;

         //Insert/Update Tasks
         if(tList.size() > 0) {
             Upsert tList;
         }
         tList= null;
         tNew = null;

         leadPlusTasks = cleanRecords;
         leadPlusTasks.addall(changedRecords);
         campaignRefresh();
        return null;
    }
    
    /**
    *Filter Owners for leads
    */
    private void filterOwner(){
        if( ownerId == null || ownerId == '--ALL--' || leadPlusTasks.size() < 1 )
          return;
        else{
            list<leadPlusTaskPlusContact> newLptList = new list<leadPlusTaskPlusContact>();
            for( leadPlusTaskPlusContact lpt : leadPlusTasks ){
                if( lpt.ownerId == ownerId )
                  newLptList.add(lpt);
            }
            leadPlusTasks = newLptList;
        }
    }
    /**
    *Refresh campaign list
    *@return page reference
    */
    public void newCampaignRefresh(){
        counter=0;  
        total_size=0; //used to show user the total size of the list
        total_Pages=0;
        current_Page=0;
        AccLimitExceed=False;
        campaignRefresh();
    } 
    
    /**
    *Refresh campaign list
    *@return page reference
    */
    public PageReference campaignRefresh(){
        Map<String, String>ownerMap = new Map<String, String>();
        Map<String, String>AssignedMap = new Map<String, String>();
        relevantLeads.clear(); leadPlusTasks.clear(); 
        cStatusMapIsSet = false;
        lstCampIds.clear();
        if(PageList_Size !=null){
            list_size=Integer.Valueof(PageList_Size);
        }

        if ( (camp != null && camp.length( ) >3) || camp=='ALL' ){
            //set some variable values
            hasCampaignID = true;           
            try{
                String selectedObject = 'Contact';
                Map<String, Schema.SObjectType> gdMap = Schema.getGlobalDescribe();
                Schema.Describesobjectresult dsr = gdMap.get(selectedObject).getDescribe();
                Map<String, Schema.SObjectField> fieldMap= dsr.fields.getMap();
                String contactFieldLst='';
                
                for(string key : fieldMap.keySet()) {
                  contactFieldLst=contactFieldLst+'Contact.'+key+',';
                }
                if(camp=='ALL'){
                    for(Campaign campID:activeCampaigns){
                        lstCampIds.add(campID.id);
                    }
                }else{
                    lstCampIds.add(camp);
                }
                id currentUserId = Userinfo.getUserId();
                system.debug('Search String========================================>'+SearchText);
                if(SearchText==null){
                    SearchText= '';
                }
                string tempInput = '%' + SearchText + '%';
                if( status == null || status == '--ALL--' ){
                  total_size=[select Count() from CampaignMember where CampaignId IN :lstCampIds AND Assigned_To__c=:currentUserId AND (Contact.Account.name like :tempInput OR lead.Company like :tempInput OR Contact.Account.name =:SearchText OR lead.Company=:SearchText) Limit 2001];
                  if(total_size>2000){
                        total_size=2000; 
                        AccLimitExceed=true;   
                  }
                  members = Database.query('select Id,ContactId,campaign.EndDate,Due_Date__c, LeadId,Priority__c,Notes__c, Status,Assigned_To__c,Assigned_To__r.firstName,Assigned_To__r.lastname, CampaignId,Campaign.Name,Lead.Id,Lead.IsConverted,Lead.ConvertedContactId,Lead.Salutation,Lead.Title,Lead.Name,Lead.Company,Lead.Phone,Lead.Street,Lead.State,Lead.PostalCode,Lead.Country,Lead.City,Lead.Email, Lead.OwnerId, Lead.Owner.FirstName, Lead.Owner.LastName, Lead.Owner.Id,'+contactFieldLst+'Lead.Fax,Contact.Owner.FirstName,Contact.Owner.LastName,Contact.Account.name from CampaignMember where CampaignId IN :lstCampIds and Assigned_To__c=:currentUserId and (Contact.Account.name like :tempInput OR lead.Company like :tempInput OR Contact.Account.name =:SearchText OR lead.Company=:SearchText) order by Campaign.Name limit :list_size offset :counter');
               }else{
                  total_size=[select Count() from CampaignMember where CampaignId IN :lstCampIds and Assigned_To__c=:currentUserId and Status = :status and Assigned_To__c=:currentUserId  and (Contact.Account.name like :tempInput OR lead.Company like :tempInput OR Contact.Account.name =:SearchText OR lead.Company=:SearchText)];
                  members = Database.query('select Id,ContactId,campaign.EndDate,Due_Date__c, LeadId,Priority__c,Notes__c, Status,Assigned_To__c,Assigned_To__r.firstName,Assigned_To__r.lastname, CampaignId,Campaign.Name,Lead.Id,Lead.IsConverted,Lead.ConvertedContactId,Lead.Salutation,Lead.Title,Lead.Name,Lead.Company,Lead.Phone,Lead.Street,Lead.State,Lead.PostalCode,Lead.Country,Lead.City,Lead.Email, Lead.OwnerId, Lead.Owner.FirstName, Lead.Owner.LastName, Lead.Owner.Id,'+contactFieldLst+'Lead.Fax,Contact.Owner.FirstName,Contact.Owner.LastName,Contact.Account.name from CampaignMember where CampaignId IN :lstCampIds and Status = :status and Assigned_To__c=:currentUserId  and (Contact.Account.name like :tempInput OR lead.Company like :tempInput OR Contact.Account.name =:SearchText OR lead.Company=:SearchText) order by Campaign.Name limit :list_size offset :counter');
               }
               total_Pages=(total_size/list_size)+1;
               current_Page=(Counter/list_size)+1;

            }catch( Exception e ){
                ApexPages.addMessages( e );
            }
            if( members!=null && members.size() > 0 )            
                thisCampaign = members[ 0 ].Campaign.Name;
            else
                thisCampaign ='';
                    
                //loop through all leads, add relevent leads and their status
            if(members!=null)
            {
            for ( CampaignMember m : members ){
                if(m.Assigned_To__c != null && m.Assigned_To__c ==UserInfo.getUserId()){
                    if(m.contact.OwnerId !=null || m.lead.OwnerId !=null){
                        leadPlusTaskPlusContact lpt = new leadPlusTaskPlusContact( m, getcStatusMap( camp ), camp,mapCampSatus );
                        leadPlusTasks.add( lpt );
                        ownerMap.put( lpt.ownerId, lpt.ownerFirstName + ' ' + lpt.ownerLastName );
                     }
                     if(m.Assigned_To__c !=null){
                        AssignedMap.put( m.Assigned_To__c, m.Assigned_To__r.firstname + ' ' +  m.Assigned_To__r.LastName );
                    }
                }
                mapCampMemStatus.put(m.id,m.status);
            }
            }
            
            
            
            //set owners
            owners = ownerMapToSelectOption( ownerMap );
            
            //filter for owner
            filterOwner();
            
            if( leadPlusTasks.size() > 1 )
                quickSortAsc( 'NAME',0, leadPlusTasks.size()-1 );
        }else{ 
            hasCampaignID=false;
            camp=this.campBackup;
        }
        return null;
    }
    
    /**
    *With a map of owners and return a list of select option
    *@param ownerMap
    *@return returnVal
    */
    private List<SelectOption> ownerMapToSelectOption( Map<String, String> ownerMap ){
        List<SelectOption> returnVal=new List<SelectOption>();
        Set<String> keys = new Set<String>();
        keys = ownerMap.keySet();
        returnVal.add( new SelectOption( '--ALL--', '--ALL--' ));
        for ( String s: keys ){
            returnVal.add( new SelectOption(s, ownerMap.get( s )));
        }
        
        return returnVal;
    }

    /**
    *Sort by Campaign name
    *@return null
    * Last Modified : Rajiv Added Sort Functionality 9/10/13
    */
    Public String Value;
    public PageReference sortByCampName(){
        if( !leadPlusTasks.IsEmpty() && leadPlusTasks.size() > 1 ){
            Value=leadPlusTasks[0].campName;
            quickSortAsc( 'CAMPNAME',0, leadPlusTasks.size()-1 );
            if( Value==leadPlusTasks[0].campName){
                quickSortDes( 'CAMPNAME',0, leadPlusTasks.size()-1 );
            }
        }
        sortColumn='CAMPNAME';
        return null;
    }

    
    /**
    *Sort by name
    *@return null
    * Last Modified : Rajiv Added Sort Functionality 7/10/13
    */
    public PageReference sortByName(){
        if( !leadPlusTasks.IsEmpty() && leadPlusTasks.size() > 1 ){
            Value=leadPlusTasks[0].Name;
            quickSortAsc( 'NAME',0, leadPlusTasks.size()-1 );
            if( Value==leadPlusTasks[0].Name){
                quickSortDes( 'NAME',0, leadPlusTasks.size()-1 );
            }
        }
        sortColumn='NAME';
        return null;
    }

    /**
    *Sort by Salutation
    *@return PageReference = null
    * Created By : Rajiv Added sort/Reserse Sort Functionality 7/10/13
    */
    public PageReference sortBySAL(){
        if(  !leadPlusTasks.IsEmpty() && leadPlusTasks.size() > 1 ){
            Value= leadPlusTasks[0].Salutation;
            quickSortAsc( 'SAL',0, leadPlusTasks.size()-1 );
            if( Value==leadPlusTasks[0].Salutation){
                quickSortDes( 'SAL',0, leadPlusTasks.size()-1 );
            }
        }
        sortColumn='SAL';
        return null;
    }
    
    
    /**
    *Sort by Company
    *@return PageReference = null
    * Last Modified : Rajiv Added Reserse Sort Functionality 7/10/13
    */
    public PageReference sortByCompany(){
        if( !leadPlusTasks.IsEmpty() &&  leadPlusTasks.size() > 1 ){
            Value=leadPlusTasks[0].COMPANY;
            quickSortAsc( 'COMPANY',0, leadPlusTasks.size()-1 );
            if( Value==leadPlusTasks[0].COMPANY){
                quickSortDes( 'COMPANY',0, leadPlusTasks.size()-1 );
            }
        }
        sortColumn='COMPNAME';
        return null;
    } 
    
    /**
    *Sort by due Date
    *@return PageReference = null
    * Last Modified : Rajiv Added Sort Functionality 9/6/13
    */
    public PageReference sortBydueDate(){
        if(  !leadPlusTasks.IsEmpty() && leadPlusTasks.size() > 1 ){
            Value=leadPlusTasks[0].dueDate;
            quickSortAsc( 'DD',0, leadPlusTasks.size()-1 );
            if( Value==leadPlusTasks[0].dueDate){
                quickSortDes( 'DD',0, leadPlusTasks.size()-1 );
            }
        }
        sortColumn='DD';
        return null;
    }
    

    /**
    *Sort by priority
    *@return PageReference = null
    * Last Modified : Rajiv Added Sort Functionality 9/6/13
    */
    public PageReference sortByPriority(){
        if(  !leadPlusTasks.IsEmpty() && leadPlusTasks.size() > 1 ){
            Value=leadPlusTasks[0].priority;
            quickSortAsc( 'PRIORITY',0, leadPlusTasks.size()-1 );
            if( Value==leadPlusTasks[0].priority){
                quickSortDes( 'PRIORITY',0, leadPlusTasks.size()-1 );
            }
        }
        sortColumn='PRIORITY';
        return null;
    }
    
    /**
    *Sort by Notes
    *@return PageReference = null
    * Last Modified : Rajiv Added Sort Functionality 7/10/13
    */
    public PageReference sortByNotes(){
        if(  !leadPlusTasks.IsEmpty() && leadPlusTasks.size() > 1 ){
            Value=leadPlusTasks[0].notes;
            quickSortAsc( 'NOTES',0, leadPlusTasks.size()-1 );
            if( Value==leadPlusTasks[0].notes){
                quickSortDes( 'NOTES',0, leadPlusTasks.size()-1 );
            }
        }
        sortColumn='NOTES';
        return null;
    }
    
    /**
    *Sort by Status
    *@return PageReference = null
    * Last Modified : Rajiv Added Sort Functionality 9/6/13
    */
    public PageReference sortByStatus(){
        if(  !leadPlusTasks.IsEmpty() && leadPlusTasks.size() > 1 ){
            Value=leadPlusTasks[0].memStatus;
            quickSortAsc( 'STATUS',0, leadPlusTasks.size()-1 );
            if( Value==leadPlusTasks[0].memStatus){
                quickSortDes( 'STATUS',0, leadPlusTasks.size()-1 );
            }
        }
        sortColumn='STATUS';
        return null;
    } 


    /**
    *Sort by Phone
    *@return PageReference = null
    * Created By : Rajiv Added sort/Reserse Sort Functionality 7/10/13
    */
    public PageReference sortByPhone(){
        if(  !leadPlusTasks.IsEmpty() && leadPlusTasks.size() > 1 ){
            Value= leadPlusTasks[0].Phone;
            quickSortAsc( 'PHONE',0, leadPlusTasks.size()-1 );
            if( Value==leadPlusTasks[0].Phone){
                quickSortDes( 'PHONE',0, leadPlusTasks.size()-1 );
            }
         }
        sortColumn='PHONE';
        return null;
    }
   
    /**
    *Implementation of Quick sort Ascending
    *@param key
    *@param left
    *@param right
    */
    private void quickSortAsc( String key, integer left, integer right ){
      integer index = partitionAsc( key, left, right );

      if ( left < index - 1 )
        quickSortAsc( key, left, index - 1 );
      if (index < right )
        quickSortAsc( key, index, right );
    }
    
    /**
    *Implementation of Quick sort Descending
    *@param key
    *@param left
    *@param right
    */
    private void quickSortDes( String key, integer left, integer right ){
      integer index = partitionDes( key, left, right );

      if ( left < index - 1 )
        quickSortDes( key, left, index - 1 );
      if (index < right )
        quickSortDes( key, index, right );
    }
    
    /**
    *Aux method for implement Quick Sort Ascending
    *@param key
    *@param left
    *@param right
    *@return i
    */
    private integer partitionAsc( String key, integer left, integer right ){
      integer i = left, j = right;
      leadPlusTaskPlusContact tmp;
      leadPlusTaskPlusContact pivot = leadPlusTasks[( left + right ) / 2 ];
      while ( i <= j ){
        while ( compareAsc( key, leadPlusTasks[ i ], pivot ))
          i++;
        while ( compareAsc( key, pivot,leadPlusTasks[ j ]))
          j--;
        if ( i <= j ){
          tmp = leadPlusTasks[ i ];
          leadPlusTasks[  i] = leadPlusTasks[ j ];
          leadPlusTasks[ j ] = tmp;
          i++;
          j--;
        }
      }
      return i;
  }
  /**
    *Aux method for implement Quick Sort Descending
    *@param key
    *@param left
    *@param right
    *@return i
    */
    private integer partitionDes( String key, integer left, integer right ){
      integer i = left, j = right;
      leadPlusTaskPlusContact tmp;
      leadPlusTaskPlusContact pivot = leadPlusTasks[( left + right ) / 2 ];
      while ( i <= j ){
        while ( compareDes( key, leadPlusTasks[ i ], pivot ))
          i++;
        while ( compareDes( key, pivot,leadPlusTasks[ j ]))
          j--;
        if ( i <= j ){
          tmp = leadPlusTasks[ i ];
          leadPlusTasks[  i] = leadPlusTasks[ j ];
          leadPlusTasks[ j ] = tmp;
          i++;
          j--;
        }
      }
      return i;
  }
    
    /**
    *Compare 2 leads Ascending order
    *@sortKey
    *leadPlusTaskPlusContact
    *leadPlusTaskPlusContact
    *@return Boolean
    */
    private boolean compareAsc( string sortKey, leadPlusTaskPlusContact r1, leadPlusTaskPlusContact r2 ){
        System.debug('------------------------------>'+r1.memStatus+'//'+r2.memStatus+'//'+r1.Name+'//'+r2.Name);
       if( sortKey == 'STATUS')
         return r1.memStatus < r2.memStatus;
       else if(sortKey == 'PHONE')
        return r1.Phone < r2.Phone;
       else if(sortKey == 'ASSIGNEDTO')
        return r1.ASSIGNEDTO < r2.ASSIGNEDTO;
       else if(sortKey == 'DD')
        return r1.dueDate < r2.dueDate;
       else if(sortKey == 'NOTES')
        return r1.notes < r2.notes;
       else if(sortKey == 'PRIORITY')
        return r1.PRIORITY < r2.PRIORITY;
       else if(sortKey == 'CAMPNAME')
        return r1.campName < r2.campName;
       else if(sortKey == 'SAL')
        return r1.SALUTATION < r2.SALUTATION;
       else if( sortKey == 'COMPANY' )
         return r1.COMPANY < r2.COMPANY;
       else 
         return r1.NAME < r2.NAME;
    }

    /**
    *Compare 2 leads decsnding order
    *@sortKey
    *leadPlusTaskPlusContact
    *leadPlusTaskPlusContact
    *@return Boolean
    */
    private boolean compareDes( string sortKey, leadPlusTaskPlusContact r1, leadPlusTaskPlusContact r2 ){
        System.debug('------------------------------>'+r1.memStatus+'//'+r2.memStatus+'//'+r1.Name+'//'+r2.Name);
       if( sortKey == 'STATUS')
         return r1.memStatus > r2.memStatus;
       else if(sortKey == 'PHONE')
        return r1.Phone > r2.Phone;
       else if(sortKey == 'ASSIGNEDTO')
        return r1.ASSIGNEDTO > r2.ASSIGNEDTO;
       else if(sortKey == 'DD')
        return r1.dueDate > r2.dueDate;
       else if(sortKey == 'NOTES')
        return r1.notes > r2.notes;
       else if(sortKey == 'PRIORITY')
        return r1.priority > r2.priority;
       else if(sortKey == 'CAMPNAME')
        return r1.campName > r2.campName;
       else if(sortKey == 'SAL')
        return r1.SALUTATION > r2.SALUTATION;
       else if( sortKey == 'COMPANY' )
         return r1.COMPANY > r2.COMPANY;
       else 
         return r1.NAME > r2.NAME;
    }

    /**
    *Return a status map
    *@param cId
    *@return cStatusMap
    */
    public Map<String,String> getcStatusMap( string cId ){
        if( !cStatusMapIsSet ){
            mapCampSatus= new Map<id,list<SelectOption>>(); 
            CampaignMemberStatus[] cStatus = [ Select Label,campaignId From CampaignMemberStatus where campaignId IN : lstCampIds ORDER BY Label ASC ];
            List<SelectOption> options = new List<SelectOption>();
            for(id sCampID:lstCampIds){
                for(CampaignMemberStatus cS:cStatus){
                    if(cS.campaignId==sCampID){
                        options.add( new SelectOption( cS.Label, cS.Label));                      
                    }
                }
                //options.sort();
                mapCampSatus.put(sCampID,options);
            }
            cStatusMap.clear();
            /*
            List<String> aList = new List<String>();
            for ( CampaignMemberStatus s:cStatus ) {
                aList.add(s.Label);
            }
            aList.sort();
            For(String str:aList) {
               cStatusMap.put(str, '');
            }
            */
                      
            for ( CampaignMemberStatus s:cStatus )
                
                //cStatusMap.put( s.Label, '' );
                cStatusMap.put( s.Label, s.Label );
            cStatusMapIsSet = true;
        }  
        return cStatusMap;
    }
    
    /**
    *Return campaign items
    *@return options
    */
    public List<SelectOption> getcampaignItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption( '1', 'SELECT' ));
        options.add(new SelectOption( 'ALL', 'ALL' ));
        for( Campaign c:getactiveCampaigns()){
            options.add( new SelectOption( c.ID, c.Name ));
        }
        return options;
    }
   
    /**
    *Return status items option list 
    *@return options
    */
    public List<Selectoption> getstatusItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add( new SelectOption( '--ALL--', '--ALL--' ));
        if ( camp == null || camp == '1' ){
            //options.sort();
            return options;
            }
        else    {
            List<String> optionsList = new List<String>();
            for ( String k : getcStatusMap( camp ).values())
                optionsList.add(k);
            for(integer i=0; i < optionsList.size(); i++)
                optionsList[i] = optionsList[i].toUpperCase() + optionsList[i];
            optionsList.sort();
            for(integer i=0; i < optionsList.size(); i++)
                optionsList[i] = optionsList[i].substring( optionsList[i].length() / 2 );
            for ( String k : optionsList)
                options.add( new SelectOption( k, k ));
            /*
            for ( String k : getcStatusMap( camp ).values())
                options.add( new SelectOption( k, k ));
            */         
            return options;
        }
             
    }

    /**
    *Return a list of campaigns options
    *@return activeCampaigns
    */
    public List<Campaign> getactiveCampaigns(){
        if( activeCampaigns == null ){
            list<id> lstCampId= new list<id>();
            List<CampaignMember> cMemberLst=[select id,Assigned_To__c,CampaignId from CampaignMember where Assigned_To__c=:userInfo.getUserId()];
            for(CampaignMember cM:cMemberLst){
                lstCampId.add(cM.CampaignId);
            }
            activeCampaigns = [ Select Name, Id, Type,EndDate,ownerID From Campaign where isActive = true AND id IN :lstCampId and Type=:'Phone Call' and (EndDate >: system.today() OR EndDate=null) order by name LIMIT 100];
        }
        return activeCampaigns;
    }
    
    /**
    *Return the name
    *@retutn callDownColumns.get('Name')
    */
    public Boolean getName(){
        return callDownColumns.get( 'Name' );
    }
    
    /**
    *Return the campaign name
    */
    public String getCampaignName(){
        if ( hasCampaignID ){
            return thisCampaign;
        } else{
            return 'All Leads';
        }   
    }
        
    /**
    *Return lead Plus Task
    */
    public List<leadPlusTaskPlusContact> getLeadPlusTasks(){
        return leadPlusTasks;   
    }

    
    /**
    * Inner class to wrap Lead or Contact sobject.
    */
    public class leadPlusTaskPlusContact{
        //variable for each piece of the combined record

        public Task t;
        public String memStatus      {get; set;}
        public Map<String, String> localCStatusMap;
        public string ID             {get; set;}
        public string ASSIGNEDTO           {get; set;}
        public string NAME           {get; set;}
        public string PHONE          {get; set;}
        public string priority       {get; set;}
        public String dueDateValue  {get; set;}
        public String dueDate   {get; set;}
        public string notes          {get; set;}
        public string cORl           {get; set;}
        public string statusUpdate   {get; set;}
        public string memberID       {get; set;}
        public string ownerFirstName {get; set;}
        public string ownerLastName  {get; set;}
        public string ownerId        {get; set;}
        public string accountName    {get; set;}
        public string callType       {get; set;}
        public string campName       {get; set;}
        public string campNameID     {get; set;}
        public string SALUTATION     {get; set;}
        public string COMPANY        {get; set;}
        public list<SelectOption> status   {get; set;}

        /**
        *Constructor
        *@param m is a campaign member
        *@param statusMap a map of atatuses
        *@param camp id of campaign
        */  
        public leadPlusTaskPlusContact( CampaignMember m, Map<String, String> statusMap, string camp,Map<id,list<SelectOption>> mapCampSatus ){
            if ( m.Lead.id != null && m.Lead.isConverted == false){
                cORl = 'lead';
                //l=incomingLead;
                ID = m.Lead.Id;
                NAME = m.Lead.Name;
                PHONE = m.Lead.Phone;
                ownerFirstName = m.Lead.Owner.FirstName; ownerLastName = m.Lead.Owner.LastName; 
                ownerId = m.Lead.OwnerId;
                SALUTATION=m.Lead.Salutation;
                COMPANY = m.Lead.Company; 
                                
            }else if(m.Lead.id != null && m.Lead.isConverted == true) {
                //converted lead
                cORl = 'contact';
                ID = m.Lead.ConvertedContactId;
                NAME = m.Lead.Name;
                PHONE = m.Lead.Phone;
                ownerFirstName = m.Lead.Owner.FirstName; ownerLastName = m.Lead.Owner.LastName; 
                ownerId = m.Lead.OwnerId;
                SALUTATION=m.Lead.Salutation;
                COMPANY = m.Lead.Company; 
            
            }else{
               cORl = 'contact';
               ID               = m.Contact.Id;
               NAME             = m.Contact.Name;
               PHONE            = m.Contact.Phone;
 
               ownerFirstName   = m.Contact.Owner.FirstName; 
               ownerLastName = m.Contact.Owner.LastName; 
               ownerId          = m.Contact.OwnerId;
               SALUTATION       = m.Contact.Salutation;
               if ( m.Contact.Account.name != null )
                    COMPANY = m.Contact.Account.name;
               else
                  COMPANY = '';
            }
            t = new Task();
            t.ActivityDate = System.today();
            t.WhoId = ID;
            t.Status = 'Completed';
            t.Subject = '';
            t.Description = '';
            if ( m.Lead.id == null ){
                if(camp !='ALL' ){
                t.WhatId = camp;
                }else{
                t.WhatId=m.campaignID;
                }
            } else if (m.Lead.id != null && m.Lead.isConverted == true){
                t.WhatId=m.campaignID;
            }
            memberID = m.ID;
            memStatus = m.Status;
            localCStatusMap = statusMap;
            statusUpdate = m.Status;
            ASSIGNEDTO=m.Assigned_to__r.FirstName+' '+m.Assigned_to__r.LastName;
            priority=m.Priority__c;
            if(m.Due_Date__c !=null){
                dueDateValue= String.Valueof(m.Due_Date__c.month()+'/'+m.Due_Date__c.day()+'/'+m.Due_Date__c.year());
            }
            if(m.Due_Date__c !=null){
                dueDate=String.Valueof(m.Due_Date__c.year()+'/'+m.Due_Date__c.month()+'/'+m.Due_Date__c.day());
            }else{
                dueDate='';
            }
            notes=m.notes__c;
            
            campName=m.campaign.Name;
            campNameID=m.campaign.id;
            status=mapCampSatus.get(m.campaign.id);

        }

        /**
        *Return a task
        *@return t
        */
        public Task gett(){
            return t;
        }
        
        /**
        *Set the task propertie
        *@param tsk
        */
        public Void sett( Task tsk ){
            t = tsk;
        }
        
        /**
        *Return member status
        *@return memStatus
        */
        public String getmemStatus(){
            return memStatus;
        }

        /**
        *Return a list of members status values
        */
        public List<SelectOption> getmemStatusValue(){
            List<SelectOption> options = new List<SelectOption>();
            options.add( new SelectOption( memStatus, memStatus )); 
            for( String k: localCStatusMap.keySet()){
                if( k != memStatus )
                    options.add( new SelectOption( k, k ));
            }
            return options;
        }
        
   
    }
/* Code for Pagenation :Start*/
   private integer counter=0;  //keeps track of the offset
   private integer list_size=10; //sets the page size or number of rows
   public integer total_size; //used to show user the total size of the list
   public integer total_Pages{get;Set;}
   public integer current_Page{get;Set;}
   public string PageList_Size{get;set;}
   public string pageTotalRecord_Size{get;set;}
 
   public PageReference Beginning() { //user clicked beginning
      counter = 0;
      campaignRefresh();
      return null;
   }
 
   public PageReference Previous() { //user clicked previous button
      counter -= list_size;
      campaignRefresh();
      return null;
   }
 
   public PageReference Next() { //user clicked next button
      counter += list_size;
      campaignRefresh();
      return null;
   }
 
   public PageReference End() { //user clicked end
      counter = total_size - math.mod(total_size, list_size);
      campaignRefresh();
      return null;
   }
 
   public Boolean getDisablePrevious() {
      //this will disable the previous and beginning buttons
      if (counter>0) return false; else return true;
   }
 
   public Boolean getDisableNext() { //this will disable the next and end buttons
      if (counter + list_size <= total_size) return false; else return true;
   }
 
   public Integer getTotal_size() {
      return total_size;
   }
 
   public Integer getPageNumber() {
      return counter/list_size + 1;
   }
 
   public Integer getTotalPages() {
      if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;
      } else {
         return (total_size/list_size);
      }
   }
/* Code for Pagenation :End*/
}