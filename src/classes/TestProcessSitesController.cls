/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestProcessSitesController {
    
    static Id oppId, site1Id, site2Id, site3Id, sl1Id, sl2Id;
    static Boolean withAssertions = true;
    
    static {
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        
        Account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street'; a.Account_Status__c = 'Assigned';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
       // a.parentId= null;
        insert a;       
        
        Opportunity o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Unified_Comm_Collaboration_Estimated__c =20;
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.today();
        o.Unified_Comm_Collaboration_Estimated__c =20;
        insert o;
        oppId = o.id;
        
        CLLIPostalCode__c pcToCLLI1 = new CLLIPostalCode__c(Postal_Code__c = 'A1A1A1', CLLI_Code__c = '123113');
        insert pcToCLLI1;   
        CLLIPostalCode__c pcToCLLI2 = new CLLIPostalCode__c(Postal_Code__c = 'A1A1A2', CLLI_Code__c = '223113');
        insert pcToCLLI2;   
    }
    
    static testMethod void noSitesOnOppTest() {
       
       PageReference pageRef = New PageReference('/apex/processSites?retURL=' + String.valueOf(oppId)); 
       Test.setCurrentPage(pageRef);
       
       ProcessSitesController controller = new ProcessSitesController();

        if (withAssertions){
            System.assertEquals(controller.prospectSites.size(), 0);
            System.assertEquals(controller.renderSiteTable, false);
            System.assertEquals(controller.renderSiteMessage, true);
        }
    }
    
    static testMethod void containsSitesOnOppNoServiceableTest() {
        
        Site__c s1 = new Site__c();
        s1.Suite_Floor__c = '11a';
        s1.Street_Name__c = 'Somewhere';
        s1.Street_Number__c  = '5';
        s1.City__c = 'City';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.ServiceableLocation__c = null;
        s1.Opportunity__c = oppId;
        insert s1;
        site1Id = s1.Id;
        
        Site__c s2 = new Site__c();
        s2.Suite_Floor__c = '11a';
        s2.Street_Name__c = 'Someplace';
        s2.Street_Number__c  = '5';
        s2.City__c = 'City';
        s2.Postal_Code__c = 'A1A1A2';
        s2.Province_Code__c = 'ON';
        s2.ServiceableLocation__c = null;
        s2.Opportunity__c = oppId;
        insert s2;
        site2Id = s2.Id;
               
       PageReference pageRef = New PageReference('/apex/processSites?retURL=' + String.valueOf(oppId)); 
       Test.setCurrentPage(pageRef);
       
       ProcessSitesController controller = new ProcessSitesController();

        if (withAssertions){
            System.assertEquals(controller.prospectSites.size(), 2);
            System.assertEquals(controller.renderSiteTable, true);
            System.assertEquals(controller.renderSiteMessage, false);
            
        }
       
       
    }
    
    static testMethod void sitesOnOppAllServiceableTest() {
        
        ServiceableLocation__c sl1 = new ServiceableLocation__c();
        sl1.Name = '5SomewhereAveCityON123113';
        sl1.Street_Name__c = 'Somewhere';
        sl1.Street_Number__c  = '5';
        sl1.Street_Type__c  = 'Ave';
        sl1.City__c = 'City';
        sl1.Province_Code__c = 'ON';
        sl1.CLLI_Code__c = '123113';
        sl1.Access_Type_Group__c = 'NNI';
        insert sl1;
        
        ServiceableLocation__c sl2 = new ServiceableLocation__c();
        sl2.Name = '5SomeplaceAveCityON223113';
        sl2.Street_Name__c = 'Someplace';
        sl2.Street_Number__c  = '5';
        sl2.Street_Type__c  = 'Ave';
        sl2.City__c = 'City';
        sl2.Province_Code__c = 'ON';
        sl2.CLLI_Code__c = '223113';
        sl2.Access_Type_Group__c = 'NNI';
        insert sl2;
        
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Somewhere';
        s1.Suite_Floor__c = '11a';
        s1.Street_Number__c  = '5';
        s1.Street_Type__c  = 'Ave';
        s1.City__c = 'City';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.ServiceableLocation__c = null;
        s1.Opportunity__c = oppId;
        s1.Type__c = 'NNI';
        insert s1;
        site1Id = s1.Id;
        
        Site__c s2 = new Site__c();
        s2.Street_Name__c = 'Someplace';
        s2.Suite_Floor__c = '11a';
        s2.Street_Number__c  = '5';
        s2.Street_Type__c  = 'Ave';
        s2.City__c = 'City';
        s2.Postal_Code__c = 'A1A1A2';
        s2.Province_Code__c = 'ON';
        s2.ServiceableLocation__c = null;
        s2.Opportunity__c = oppId;
        s2.Type__c = 'NNI';
        insert s2;
        site2Id = s2.Id;
                
       
       PageReference pageRef = New PageReference('/apex/processSites?retURL=' + String.valueOf(oppId)); 
       Test.setCurrentPage(pageRef);
       
       ProcessSitesController controller = new ProcessSitesController();
       
       List<Site__c> sites = [SELECT Id, Name, ServiceableLocation__c FROM Site__c where Opportunity__c = :oppId];
                    
       if (withAssertions){
            System.assertEquals(controller.prospectSites.size(), 2);
            System.assertEquals(controller.syncList.size(), 0);
            controller.syncSites(); 
           
           System.assertEquals(sites.size(), 2);
           
       }
       
    }
    
    static testMethod void containsSomeServiceableTest() {
       
       ServiceableLocation__c sl1 = new ServiceableLocation__c();
        sl1.Name = '5SomewhereAveCityON123113';
        sl1.Street_Name__c = 'Somewhere';
        sl1.Street_Number__c  = '5';
        sl1.Street_Type__c  = 'Ave';
        sl1.City__c = 'City';
        sl1.Province_Code__c = 'ON';
        sl1.CLLI_Code__c = '123113';
        sl1.Access_Type_Group__c = 'NNI';
        insert sl1;
            
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Somewhere';
        s1.Suite_Floor__c = '11a';
        s1.Street_Number__c  = '5';
        s1.Street_Type__c  = 'Ave';
        s1.City__c = 'City';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.ServiceableLocation__c = null;
        s1.Opportunity__c = oppId;
        s1.Type__c = 'NNI';
        insert s1;
        site1Id = s1.Id;
        
        Site__c s2 = new Site__c();
        s2.Street_Name__c = 'Someplace';
        s2.Suite_Floor__c = '11a';
        s2.Street_Number__c  = '5';
        s2.Street_Type__c  = 'Ave';
        s2.City__c = 'City';
        s2.Postal_Code__c = 'A1A1A2';
        s2.Province_Code__c = 'ON';
        s2.ServiceableLocation__c = null;
        s2.Opportunity__c = oppId;
        insert s2;
        site2Id = s2.Id;
        
        Site__c s3 = new Site__c();
        s3.Street_Name__c = 'Somewhere';
        s3.Suite_Floor__c = '11a';
        s3.Street_Number__c  = '5';
        s3.Street_Type__c  = 'Ave';
        s3.City__c = 'City';
        s3.Postal_Code__c = 'A1A1A3';
        s3.Province_Code__c = 'ON';
        s3.ServiceableLocation__c = null;
        s3.Opportunity__c = oppId;
        s3.Type__c = 'CNI';
        insert s3;
        site3Id = s3.Id;
        
       
       PageReference pageRef = New PageReference('/apex/processSites?retURL=' + String.valueOf(oppId)); 
       Test.setCurrentPage(pageRef);
       
       ProcessSitesController controller = new ProcessSitesController();
       
       List<Site__c> sites = [SELECT Id, Name, ServiceableLocation__c FROM Site__c where Opportunity__c = :oppId];
                    
       if (withAssertions){
            System.assertEquals(controller.syncList.size(), 0);
            System.assertEquals(controller.prospectSites.size(), 3);
            controller.syncSites(); 
            sites = [SELECT Id, Name, ServiceableLocation__c FROM Site__c where Opportunity__c = :oppId AND ServiceableLocation__c != null];
            System.assertEquals(sites.size(), 0);
       }
    }
        
    
    static testMethod void checkWithNoPartialMatchTest() {
        ServiceableLocation__c sl1 = new ServiceableLocation__c();
        sl1.Name = '5ASomewhereAveCityON123113';
        sl1.Street_Name__c = 'Somewhere';
        sl1.Street_Number__c  = '5';
        sl1.Suite_Floor__c  = 'A';
        sl1.Street_Type__c  = 'Ave';
        sl1.City__c = 'City';
        sl1.Province_Code__c = 'ON';
        sl1.CLLI_Code__c = '123113';
        sl1.Access_Type_Group__c = 'NNI';
        insert sl1;
        
        ServiceableLocation__c sl2 = new ServiceableLocation__c();
        sl2.Name = '5ASomeplaceAveCityON223113';
        sl2.Street_Name__c = 'Someplace';
        sl2.Street_Number__c  = '5';
        sl2.Suite_Floor__c  = 'A';
        sl2.Street_Type__c  = 'Ave';
        sl2.City__c = 'City';
        sl2.Province_Code__c = 'ON';
        sl2.CLLI_Code__c = '223113';
        sl2.Access_Type_Group__c = 'NNI';
        insert sl2;
        
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Somewhere';
        s1.Suite_Floor__c = '11a';
        s1.Street_Number__c  = '6';
        s1.Street_Type__c  = 'Ave';
        s1.City__c = 'City';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.ServiceableLocation__c = null;
        s1.Opportunity__c = oppId;
        insert s1;
        site1Id = s1.Id;
        
        Site__c s2 = new Site__c();
        s2.Street_Name__c = 'Someplace';
        s2.Suite_Floor__c = '11a';
        s2.Street_Number__c  = '6';
        s2.Street_Type__c  = 'Ave';
        s2.City__c = 'City';
        s2.Postal_Code__c = 'A1A1A2';
        s2.Province_Code__c = 'ON';
        s2.ServiceableLocation__c = null;
        s2.Opportunity__c = oppId;
        insert s2;
        site2Id = s2.Id;
       
       
       PageReference pageRef = New PageReference('/apex/processSites?retURL=' + String.valueOf(oppId)); 
       Test.setCurrentPage(pageRef);
       
       ProcessSitesController controller = new ProcessSitesController();
       controller.syncSites();  
                    
       if (withAssertions){
            controller.activeSite = site1Id;
            controller.check();
            
            System.assertEquals(controller.renderTable, false);
            System.assertEquals(controller.renderMessage, true);
            System.assertEquals(controller.resultList.size(), 0);
       }
    }
    
    static testMethod void checkWithPartialMatchTest() {
  ServiceableLocation__c sl1 = new ServiceableLocation__c();
        sl1.Name = '5ASomeplaceAveCityON123113';
        sl1.Street_Name__c = 'Someplace';
        sl1.Street_Number__c  = '5';
        sl1.Suite_Floor__c  = 'A';
        sl1.Street_Type__c  = 'Ave';
        sl1.City__c = 'City';
        sl1.Province_Code__c = 'ON';
        sl1.CLLI_Code__c = '123113';
        sl1.Access_Type_Group__c = 'NNI';
        insert sl1;
        sl1Id = sl1.Id;
        
        ServiceableLocation__c sl2 = new ServiceableLocation__c();
        sl2.Name = '5BSomeplaceAveCityON123113';
        sl2.Street_Name__c = 'Someplace';
        sl2.Street_Number__c  = '5';
        sl2.Suite_Floor__c  = 'B';
        sl2.Street_Type__c  = 'Ave';
        sl2.City__c = 'City';
        sl2.Province_Code__c = 'ON';
        sl2.CLLI_Code__c = '123113';
        sl2.Access_Type_Group__c = 'NNI';
        insert sl2;
        sl2Id = sl2.Id;
        
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Someplace';
        s1.Suite_Floor__c = '11a';
        s1.Street_Number__c  = '5';
        s1.Street_Type__c  = 'Ave';
        s1.City__c = 'City';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.ServiceableLocation__c = null;
        s1.Opportunity__c = oppId;
        insert s1;
        site1Id = s1.Id;
        
       
       PageReference pageRef = New PageReference('/apex/processSites?retURL=' + String.valueOf(oppId)); 
       Test.setCurrentPage(pageRef);
       
       ProcessSitesController controller = new ProcessSitesController();
       controller.syncSites();  
                    
       if (withAssertions){
            controller.activeSite = site1Id;
            controller.check();
            
            System.assertEquals(controller.renderTable, true);
            System.assertEquals(controller.renderMessage, false);
            System.assertEquals(controller.resultList.size(), 2);
            System.assertEquals(controller.prospectSites.size(), 1);
            controller.currentLocation = sl2.Name;
            controller.updateServiceableLocation();
            controller.processUpdates();
            List<Site__c> sites = [SELECT Id, Name, ServiceableLocation__c FROM Site__c where ServiceableLocation__c = :sl2Id];
            System.assertEquals(sites.size(), 1);
            System.assertEquals(sites[0].Id, site1Id);
       }
    }
    
    static testMethod void uncheckTest() {
       
        ServiceableLocation__c sl1 = new ServiceableLocation__c();
        sl1.Name = '5ASomeplaceAveCityON123113';
        sl1.Street_Name__c = 'Someplace';
        sl1.Street_Number__c  = '5';
        sl1.Suite_Floor__c  = 'A';
        sl1.Street_Type__c  = 'Ave';
        sl1.City__c = 'City';
        sl1.Province_Code__c = 'ON';
        sl1.CLLI_Code__c = '123113';
        sl1.Access_Type_Group__c = 'NNI';
        insert sl1;
        sl1Id = sl1.Id;
        
        ServiceableLocation__c sl2 = new ServiceableLocation__c();
        sl2.Name = '5BSomeplaceAveCityON123113';
        sl2.Street_Name__c = 'Someplace';
        sl2.Street_Number__c  = '5';
        sl2.Suite_Floor__c  = 'B';
        sl2.Street_Type__c  = 'Ave';
        sl2.City__c = 'City';
        sl2.Province_Code__c = 'ON';
        sl2.CLLI_Code__c = '123113';
        sl2.Access_Type_Group__c = 'NNI';
        insert sl2;
        sl2Id = sl2.Id;
        
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Someplace';
        s1.Suite_Floor__c = '11a';
        s1.Street_Number__c  = '5';
        s1.Street_Type__c  = 'Ave';
        s1.City__c = 'City';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.ServiceableLocation__c = null;
        s1.Opportunity__c = oppId;
        insert s1;
        site1Id = s1.Id;
        
       
       PageReference pageRef = New PageReference('/apex/processSites?retURL=' + String.valueOf(oppId)); 
       Test.setCurrentPage(pageRef);
       
       ProcessSitesController controller = new ProcessSitesController();
       controller.syncSites();  
                    
       if (withAssertions){
            controller.activeSite = site1Id;
            controller.check();
            
            System.assertEquals(controller.renderTable, true);
            System.assertEquals(controller.renderMessage, false);
            System.assertEquals(controller.resultList.size(), 2);
            System.assertEquals(controller.prospectSites.size(), 1);
            controller.currentLocation = sl2.Name;
            controller.updateServiceableLocation();
            controller.removeServiceableLocation();
            controller.processUpdates();
            List<Site__c> sites = [SELECT Id, Name, ServiceableLocation__c FROM Site__c where ServiceableLocation__c = :sl2Id];
            System.assertEquals(sites.size(), 0);
            
       }
       
    }
    
        static testMethod void checkWithMultiplePartialMatchTest() {
        ServiceableLocation__c sl1 = new ServiceableLocation__c();
        sl1.Name = '5ASomeplaceAveCityON123113';
        sl1.Street_Name__c = 'Someplace';
        sl1.Street_Number__c  = '5';
        sl1.Suite_Floor__c  = 'A';
        sl1.Street_Type__c  = 'Ave';
        sl1.City__c = 'City';
        sl1.Province_Code__c = 'ON';
        sl1.CLLI_Code__c = '123113';
        sl1.Access_Type_Group__c = 'NNI';
        insert sl1;
        sl1Id = sl1.Id;
        
        ServiceableLocation__c sl2 = new ServiceableLocation__c();
        sl2.Name = '5BSomeplaceAveCityON123113';
        sl2.Street_Name__c = 'Someplace';
        sl2.Street_Number__c  = '5';
        sl2.Suite_Floor__c  = 'B';
        sl2.Street_Type__c  = 'Ave';
        sl2.City__c = 'City';
        sl2.Province_Code__c = 'ON';
        sl2.CLLI_Code__c = '123113';
        sl2.Access_Type_Group__c = 'NNI';
        insert sl2;
        sl2Id = sl2.Id;
        
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Someplace';
        s1.Suite_Floor__c = '11a';
        s1.Street_Number__c  = '5';
        s1.Street_Type__c  = 'Ave';
        s1.City__c = 'City';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.ServiceableLocation__c = null;
        s1.Opportunity__c = oppId;
        insert s1;
        site1Id = s1.Id;
        
        Site__c s2 = new Site__c();
        s2.Street_Name__c = 'Someplace';
        s2.Suite_Floor__c = '11a';
        s2.Street_Number__c  = '5';
        s2.Street_Type__c  = 'Rd';
        s2.City__c = 'City';
        s2.Postal_Code__c = 'A1A1A1';
        s2.Province_Code__c = 'ON';
        s2.ServiceableLocation__c = null;
        s2.Opportunity__c = oppId;
        insert s2;
        site2Id = s2.Id;
        
       
       PageReference pageRef = New PageReference('/apex/processSites?retURL=' + String.valueOf(oppId)); 
       Test.setCurrentPage(pageRef);
       
       ProcessSitesController controller = new ProcessSitesController();
       controller.syncSites();  
                    
       if (withAssertions){
            controller.activeSite = site1Id;
            controller.check();
            
            System.assertEquals(controller.renderTable, true);
            System.assertEquals(controller.renderMessage, false);
            System.assertEquals(controller.resultList.size(), 2);
            System.assertEquals(controller.prospectSites.size(), 2);
            controller.currentLocation = sl1.Name;
            controller.updateServiceableLocation();
            
            controller.activeSite = site2Id;
            controller.check();
            
            System.assertEquals(controller.renderTable, true);
            System.assertEquals(controller.renderMessage, false);
            System.assertEquals(controller.resultList.size(), 2);
            System.assertEquals(controller.prospectSites.size(), 2);
            controller.currentLocation = sl2.Name;
            controller.updateServiceableLocation();
            controller.removeServiceableLocation();
            controller.updateServiceableLocation();
            
            controller.processUpdates();
            List<Site__c> sites = [SELECT Id, Name, ServiceableLocation__c FROM Site__c where ServiceableLocation__c = :sl1Id];
            System.assertEquals(sites.size(), 1);
            System.assertEquals(sites[0].Id, site1Id);
            
            sites = [SELECT Id, Name, ServiceableLocation__c FROM Site__c where ServiceableLocation__c = :sl2Id];
            System.assertEquals(sites.size(), 1);
            System.assertEquals(sites[0].Id, site2Id);
            
            controller.removeServiceableLocation();
            controller.returnToOpportunity();
            controller.updateServiceableLocation();
       }
    }
   
}