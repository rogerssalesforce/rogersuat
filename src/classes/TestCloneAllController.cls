@isTest (seeAllData=true) 
private class TestCloneAllController {
    
    private static Account ParentAcc;
    private static Case caseTestObj;

    static ID caseRecordTypeId = [select Id from recordType where name='PRT Request' and sObjecttype ='Case' limit 1].id;
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c(); 

    private static void setUpData(){
      /*  TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;
        */ParentAcc= new Account(Name='testAcc');
        ParentAcc.BillingCountry= 'CA';
        ParentAcc.BillingPostalCode = 'A9A 9A9';
        ParentAcc.BillingState = 'MA';
        ParentAcc.BillingCity='City';
        ParentAcc.BillingStreet='Street';
        ParentAcc.Account_Status__c = 'Assigned';
        insert ParentAcc;
        caseTestObj = new Case();
        caseTestObj.AccountID =ParentAcc.id;
        caseTestObj.Mass_Update_Parent_Account__c = ParentAcc.id;
        caseTestObj.RecordTypeId = caseRecordTypeId;
        caseTestObj.status='New';

        Schema.DescribeFieldResult resultProgressReason= Case.In_Progress_Reason__c.getDescribe();        
        caseTestObj.In_Progress_Reason__c = (String)(TestingUtil.getPicklistDefaultValue(resultProgressReason));
        caseTestObj.Date__c = Date.today();
        /**/        
        System.debug('***In Progress Reason : ' + caseTestObj.In_Progress_Reason__c);
        System.debug('***Date (custom): ' + caseTestObj.Date__c);
        /**/        
        
        insert caseTestObj;    
    }

    static testmethod void testHasSufficientPrivileges(){

        setUpData();        
        test.startTest();
        Apexpages.currentPage().getParameters().put('id',caseTestObj.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(caseTestObj); 
        AbstractCloneAllController cls = new CloneAllCaseController(controller ); 
        cls.getClone();
        cls.doCloneAll();
        cls.redirect(true);
        AbstractCloneAllController cls1 = new CloneAllController(controller ); 
        cls1.getClone();
        
        Boolean actualHasPrivileges = cls.getHasSufficientPrivileges(caseTestObj.id);
        System.assert(actualHasPrivileges);
        test.stopTest();   
    }
    
}