/*
===============================================================================
Class Name : updateSalesMeasurement_Test
===============================================================================
PURPOSE: This is a test class for updateSalesMeasurement Trigger

COMMENTS: 

Developer: Deepika Rawat
Date: 9/11/2013


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
9/11/2013               Deepika               Created
===============================================================================
*/
@isTest (SeeAllData = true)
private class updateSalesMeasurement_Test{
    private static Account accTestObj;
    private static OpportunityLineItem  oliTestObj;
    private static Opportunity oppTestObj;
    private static Sales_Measurement__c smTest;
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c(); 
    private static List<User> listUser =[Select id from User where isActive= True and profile.name='System Administrator'  limit 2];
     static testmethod void testMethod1(){
    /*    TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;
      */ accTestObj = new Account(Name='testAcc');
       accTestObj.BillingCountry= 'CA';
       accTestObj.BillingPostalCode = 'A9A 9A9';
       accTestObj.BillingState = 'MA';
       accTestObj.BillingCity='City';
       accTestObj.BillingStreet='Street';
       insert accTestObj;
       
       
       
       Product2 p2 = new Product2(Name='pro01',ProductCode='Fulfillment');
       insert p2;
       
       Pricebook2 stdpb = [SELECT Id FROM Pricebook2 WHERE IsStandard = true];
       PricebookEntry pbe = new PricebookEntry(Pricebook2Id = stdpb.Id, Product2Id = p2.Id, UnitPrice = 1, isActive=True);
       insert pbe;
       
       oppTestObj = new Opportunity(Name='testAcc');
       oppTestObj.StageName='Cloased Won';
       oppTestObj.CloseDate=system.today();
       oppTestObj.AccountID=accTestObj.id;
       oppTestObj.ownerid =listUser[0].id;
       oppTestObj.Unified_Comm_Collaboration_Estimated__c =20;
       oppTestObj.Pricebook2ID = stdpb.id;
       insert oppTestObj;
       
       oliTestObj= new OpportunityLineItem();
        oliTestObj.ARPU__c=10;
        oliTestObj.Start_Date__c=system.today();
        oliTestObj.Quantity=10;
        oliTestObj.Installment_Period__c='Monthly';
        oliTestObj.of_Installments__c=10;
        oliTestObj.Monthly_Value__c=100;
        oliTestObj.Minimum_Contract_Value__c=100;
        oliTestObj.PricebookEntryId= pbe.id;
        oliTestObj.OpportunityId=oppTestObj.id;
        oliTestObj.TotalPrice=0;
        oliTestObj.Product_Family__c='Cable - TV';
        insert oliTestObj;
        
       smTest= new Sales_Measurement__c ();
       smTest.Opportunity__c = oppTestObj.id;
       smTest.Opportunity_Line_Item_ID__c = oliTestObj.id;
       insert smTest;
       
       delete oliTestObj;
       
       
     }
}