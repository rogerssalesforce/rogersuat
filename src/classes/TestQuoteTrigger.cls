/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestQuoteTrigger {

    static Id oppId, sl1Id, sl2Id, site1Id, site2Id; 
    static Id priceEntry1id, priceEntry2_1id,priceEntry3_1id, priceEntry2id, quote1Id, pe1Id;
    static Opportunity o;
    static Product2 p, pInstall;
    static Boolean withAssertions = true;
    

static {
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        
        List <RecordType > lRtp = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Product2']);
        Map <string,Id> mapRTp = new Map <string,id> ();
        
        for (RecordType rtp : lRtp){
            mapRTp.put(rtp.Name,rtp.id);  
        }
       
        Account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street'; a.Account_Status__c = 'Assigned';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        insert a;       
        
        o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.Unified_Comm_Collaboration_Estimated__c =20;
        o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.today();
        insert o;
        oppId = o.id;
        
        Pricebook2 sp = new Pricebook2();
        sp = [select id from Pricebook2 where Name = :'Enterprise PriceBook'];
    
        Pricebook2 sp1 = new Pricebook2();
        sp1 = [select id from Pricebook2 where isStandard = true];
    
        Quote q = new Quote(Name='q1', Term__c='3-Years', OpportunityId=oppId, Pricebook2Id = sp.id);
        insert q;
        quote1Id = q.Id;
        
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Somewhere1';
        s1.Suite_Floor__c = '11a';
        s1.Street_Number__c  = '5';
        s1.City__c = 'Coty';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.CLLI_SWC__c = '123113';
        s1.ServiceableLocation__c = null;
        s1.Opportunity__c = oppId;
        insert s1;
        site1Id = s1.Id;
        
        Site__c s2 = new Site__c();
        s2.Street_Name__c = 'Somewhere2';
        s2.Suite_Floor__c = '11a';
        s2.Street_Number__c  = '5';
        s2.City__c = 'Coty';
        s2.Postal_Code__c = 'A2A1A1';
        s2.Province_Code__c = 'ON';
        s2.CLLI_SWC__c = '123113';
        s2.ServiceableLocation__c = null;
        s2.Opportunity__c = oppId;
        insert s2;
        site2Id = s2.Id;

        List<Quote_Site__c> lstSite = new List<Quote_Site__c>();
        
        Quote_Site__c qs = new Quote_Site__c();
        qs.Site__c = site1Id;
        qs.Quote__c = quote1Id;
        lstSite.add(qs);
        
        Quote_Site__c qs2 = new Quote_Site__c();
        qs2.Site__c = site2Id;
        qs2.Quote__c = quote1Id;
        lstSite.add(qs2);

        insert lstSite;
        
        // Install Product
        pInstall  = new Product2();
        pInstall.IsActive = true;
        pInstall.Name = 'Fibre';
        pInstall.Charge_Type__c = 'NRC';
        pInstall.Access_Type__c = 'Ethernet EON';
        pInstall.Access_Type_Group__c = 'Fibre';
        pInstall.Category__c = 'Install';
        pInstall.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
        pInstall.RecordTypeId = mapRTp.get('Enterprise Products');
        pInstall.Service_Term__c = '3-Years';
        pInstall.Mark_Up_Factor__c = 1.0;
        pInstall.Start_Date__c = Date.today();
        pInstall.End_Date__c = Date.today()+1;
        insert pInstall;
        
        // Product 1
        p  = new Product2();
        p.IsActive = true;
        p.Name = 'Fibre';
        p.Charge_Type__c = 'NRC';
        p.Access_Type__c = 'Ethernet EON';
        p.Access_Type_Group__c = 'Fibre';
        p.Category__c = 'Access';
        p.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
        p.RecordTypeId = mapRTp.get('Enterprise Products');
        p.Service_Term__c = '3-Years';
        p.Product_Install_Link__c = pInstall.Id;
        p.Mark_Up_Factor__c = 1.0;
        p.Start_Date__c = Date.today();
        p.End_Date__c = Date.today()+1;
        insert p;
        
        Product2 p3  = new Product2();
        p3.IsActive = true;
        p3.Name = 'Fibre';
        p3.Charge_Type__c = 'MRC';
        p3.Access_Type__c = 'Ethernet EON';
        p3.Access_Type_Group__c = 'Fibre';
        p3.Category__c = 'Access';
        p3.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
        p3.RecordTypeId = mapRTp.get('Enterprise Products');
        p3.Service_Term__c = '3-Years';
        p3.Product_Install_Link__c = pInstall.Id;
        p3.Mark_Up_Factor__c = 1.0;
        p3.Start_Date__c = Date.today();
        p3.End_Date__c = Date.today()+1;
        insert p3;
        
        PricebookEntry peStandard = new PricebookEntry();
        peStandard.Pricebook2Id = sp1.id;
        peStandard.UnitPrice = 60;
        peStandard.Product2Id = p.id;
        peStandard.IsActive = true;
        peStandard.UseStandardPrice = false;
        
        insert peStandard;
        
        PricebookEntry pe1 = new PricebookEntry();
        pe1.Pricebook2Id = sp.id;
        pe1.UnitPrice = 60;
        pe1.Product2Id = p.id;
        pe1.IsActive = true;
        pe1.UseStandardPrice = false;
        
        insert pe1;
        priceEntry1id = pe1.Id;
        
        peStandard = new PricebookEntry();
        peStandard.Pricebook2Id = sp1.id;
        peStandard.UnitPrice = 60;
        peStandard.Product2Id = pInstall.id;
        peStandard.IsActive = true;
        peStandard.UseStandardPrice = false;
        
        insert peStandard;
        
        pe1 = new PricebookEntry();
        pe1.Pricebook2Id = sp.id;
        pe1.UnitPrice = 60;
        pe1.Product2Id = pInstall.id;
        pe1.IsActive = true;
        pe1.UseStandardPrice = false;
        
        insert pe1;
        
        //Product 2
        Product2 p1  = new Product2();
        p1.IsActive = true;
        p1.Name = 'ProductMe1';
        p1.Charge_Type__c = 'MRC';
        p1.Mark_Up_Factor__c = 1.0;
        p1.Start_Date__c = Date.today();
        p1.End_Date__c = Date.today()+1;
        insert p1;
        
        PricebookEntry pe21Standard = new PricebookEntry();
        pe21Standard.Pricebook2Id = sp1.id;
        pe21Standard.UnitPrice = 60;
        pe21Standard.Product2Id = p1.id;
        pe21Standard.IsActive = true;
        pe21Standard.UseStandardPrice = false;
        
        insert pe21Standard;
        
        PricebookEntry pe21 = new PricebookEntry();
        pe21.Pricebook2Id = sp.id;
        pe21.UnitPrice = 60;
        pe21.Product2Id = p1.id;
        pe21.IsActive = true;
        pe21.UseStandardPrice = false;
        
        insert pe21;
        priceEntry2_1id = pe21.Id;
        
        PricebookEntry pe31Standard = new PricebookEntry();
        pe31Standard .Pricebook2Id = sp1.id;
        pe31Standard .UnitPrice = 60;
        pe31Standard .Product2Id = p3.id;
        pe31Standard .IsActive = true;
        pe31Standard .UseStandardPrice = false;
        
        insert pe31Standard;
        
        PricebookEntry pe31 = new PricebookEntry();
        pe31.Pricebook2Id = sp.id;
        pe31.UnitPrice = 60;
        pe31.Product2Id = p3.id;
        pe31.IsActive = true;
        pe31.UseStandardPrice = false;
        
        insert pe31;
        priceEntry3_1id = pe31.Id;
      

    }

    static testMethod void testDiscountField() {
        //Line Item Product 1
        List<QuoteLineItem> qliList = new List<QuoteLineItem>();
        QuoteLineItem qli = new QuoteLineItem();
        qli.PricebookEntryId =priceEntry1id;
        qli.Site__c = site1Id;
        qli.Quantity = 1;
        qli.QuoteId = quote1Id;
        qli.UnitPrice = 10;
        qli.discount__c = 10;
        qli.Visible_Quote_Line_Item__c = true;
        qliList.add(qli);
        
        //Line Item Product 2
        QuoteLineItem qli2 = new QuoteLineItem();
        qli2.PricebookEntryId = priceEntry1id;
        qli2.Site__c = site2Id;
        qli2.Quantity = 1;
        qli2.QuoteId = quote1Id;
        qli2.UnitPrice = 20;
        qli.discount__c = 20;
        qli2.Visible_Quote_Line_Item__c = true;
         qliList.add(qli2);
        
        //Line Item Product 3 - NRC
        QuoteLineItem qli3 = new QuoteLineItem();
        qli3.PricebookEntryId = priceEntry3_1id ;
        qli3.Site__c = site2Id;
        qli3.Quantity = 1;
        qli3.QuoteId = quote1Id;
        qli3.UnitPrice = 20;
        qli3.discount__c = 20;
        qli3.Visible_Quote_Line_Item__c = true;
        qliList.add(qli3);
        
        Test.startTest();
            insert qliList;
        Test.stopTest();
    }
}