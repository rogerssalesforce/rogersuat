public with sharing class SiteServiceSummaryController {
    public String showTable {get; set;}
    public List<Location_A__c> aLocations {get; set;}
    public List<Location_Z__c> zLocations {get; set;}
    public List<Site__c> aSites {get; set;}
    public List<Site__c> zSites {get; set;}
    public Map<Id, List<QuoteRequestService__c>> siteA_QRSMap {get; set;}
    public Map<Id, List<QuoteRequestService__c>> siteZ_QRSMap {get; set;}
    public List<QuoteRequestService__c> qServices {get; set;}
    public List<QuoteRequestService__c> servicesA {get; set;}
    public List<QuoteRequestService__c> servicesZ {get; set;}
    public String quoteRequestId {get; set;}
    public Boolean allowServiceSelection {get; set;}
    public ServiceRequestInfo__c serviceInfo {get; set;}
    public Quote_Request__c qRequest {get; set;}
    
    public SiteServiceSummaryController (ApexPages.StandardController stdController) {
        quoteRequestId = stdController.getId();
        
         try{
            aLocations = [SELECT id, Prospect_Site__c, Prospect_Site__r.Display_Name__c FROM Location_A__c WHERE Quote_Request__c = :quoteRequestId];
        }catch(Exception ex){
            aLocations = new List<Location_A__c>();
        }
        
        try{
            zLocations = [SELECT id, Prospect_Site__c, Prospect_Site__r.Display_Name__c FROM Location_Z__c WHERE Quote_Request__c = :quoteRequestId];
        }catch(Exception ex){
            zLocations = new List<Location_Z__c>();
        }
        
        qRequest = [SELECT id 
            ,Quote__c
            ,Quote__r.Name
            ,Quote__r.Account_Name__c
            ,Quote__r.QuoteNumber__c
            ,Request_Type__c 
            ,Opportunity__r.Name
            ,Name
            ,(select Id, Name, Service__c, Service__r.Id, Quote_Request__c, ServiceRequestInfo__c from QuoteRequestServices__r)
             FROM Quote_Request__c WHERE id = :quoteRequestId];
             
        prepareServiceMaps();
    }
    
    /* A Quote Request has Quote Request Services */
    /* A Quote Request Service is linked to a Service Request Info Object */
    public void prepareServiceMaps(){
        
        qRequest = [SELECT id 
            ,Quote__c
            ,Quoting_Stage__c
            ,Name
            ,Quote__r.Name
            ,Quote__r.Account_Name__c
            ,Quote__r.QuoteNumber__c
            ,Request_Type__c 
            ,(select Id, Name, Service__c, Service__r.Service_Type__c, Service__r.Id, Quote_Request__c, ServiceRequestInfo__c
            ,Service__r.Site__r.id
            ,Service__r.SiteDisplayName__c
            ,ServiceRequestInfo__r.Access__c
            ,ServiceRequestInfo__r.Circuit_ID__c
            ,ServiceRequestInfo__r.CoS__c
            ,ServiceRequestInfo__r.Other_CoS__c
            ,ServiceRequestInfo__r.Notes__c
            ,ServiceRequestInfo__r.CreatedById
            ,ServiceRequestInfo__r.CreatedDate
            ,ServiceRequestInfo__r.IsDeleted
            ,ServiceRequestInfo__r.Diversity__c
            ,ServiceRequestInfo__r.EVC__c
            ,ServiceRequestInfo__r.EVC_Size__c
            ,ServiceRequestInfo__r.EVPL_EPL__c
            ,ServiceRequestInfo__r.LastModifiedById
            ,ServiceRequestInfo__r.LastModifiedDate
            ,ServiceRequestInfo__r.MTU_Size__c
            ,ServiceRequestInfo__r.NNI__c
            ,ServiceRequestInfo__r.NPA__c
            ,ServiceRequestInfo__r.NPA_NXX__c
            ,ServiceRequestInfo__r.NXX__c
            ,ServiceRequestInfo__r.EVC_Size_Other__c
            ,ServiceRequestInfo__r.Other_EVC_Size__c
            ,ServiceRequestInfo__r.OwnerId
            ,ServiceRequestInfo__r.Port__c
            ,ServiceRequestInfo__r.Port_Size__c
            ,ServiceRequestInfo__r.Port_Type__c
            ,ServiceRequestInfo__r.Product_Type__c
            ,ServiceRequestInfo__r.Id
            ,ServiceRequestInfo__r.Name
             from QuoteRequestServices__r)
             FROM Quote_Request__c WHERE id = :quoteRequestId];
        
        // These are the ones that have information
        qServices = new List<QuoteRequestService__c>();
        
        if (qRequest != null && qRequest.QuoteRequestServices__r != null){
            qServices = qRequest.QuoteRequestServices__r;
        }
                
        siteZ_QRSMap = new Map<Id, List<QuoteRequestService__c>>();
        siteA_QRSMap = new Map<Id, List<QuoteRequestService__c>>();
            
        
        Set<Id> siteAIds = new Set<Id>();
        for (Location_A__c a : aLocations){
            siteAIds.add(a.Prospect_Site__c);
        }
        
        aSites = [SELECT id, Display_Name__c FROM Site__c WHERE Id IN :siteAIds];
        // init the map
        for (Site__c s : aSites){
            siteA_QRSMap.put(s.Id, new List<QuoteRequestService__c>());
        }
        
        
        Set<Id> siteZIds = new Set<Id>();
        for (Location_Z__c z : zLocations){
            siteZIds.add(z.Prospect_Site__c);
        }
        
        zSites = [SELECT id, Display_Name__c FROM Site__c WHERE Id IN :siteZIds];
        // init the map
        for (Site__c s : zSites){
            siteZ_QRSMap.put(s.Id, new List<QuoteRequestService__c>());
        }
        
        List<QuoteLineItem> qliAs = [SELECT id, Site__c, Site__r.Display_Name__c, PriceBookEntry.Product2.Access_Type__c, PriceBookEntry.Product2.Access_Type_Group__c, PriceBookEntry.Name, Service_Type__c FROM QuoteLineItem WHERE Site__c IN :siteAIds AND QuoteId = :qRequest.Quote__c];
        List<QuoteLineItem> qliZs = [SELECT id, Site__c, Site__r.Display_Name__c, PriceBookEntry.Product2.Access_Type__c, PriceBookEntry.Product2.Access_Type_Group__c, PriceBookEntry.Name, Service_Type__c FROM QuoteLineItem WHERE Site__c IN :siteZIds AND QuoteId = :qRequest.Quote__c];
        
        
        
        
        // Need to traverse the aLineItems and place them in the wrapper
        servicesA = new List<QuoteRequestService__c>();
        servicesZ = new List<QuoteRequestService__c>();
        
        
        for(QuoteLineItem qli : qliAs) {
            if (qServices!= null){
                for (QuoteRequestService__c qs : qServices){
                    if (qs.Service__r.Id == qli.Id){
                        servicesA.add(qs);
                    }
                }
            }
                
        }
            
        for(QuoteLineItem qli : qliZs) {
            if (qServices!= null){
                for (QuoteRequestService__c qs : qServices){
                    if (qs.Service__r.Id == qli.Id){
                        servicesZ.add(qs);
                    }
                }
            }
        }
        
        for (QuoteRequestService__c sA : servicesA){
            List<QuoteRequestService__c> lineItems = siteA_QRSMap.get(sA.Service__r.Site__r.id);
            if (lineItems == null){
                lineItems = new List<QuoteRequestService__c>();
            }
            
            lineItems.add(sA);
            siteA_QRSMap.put(sA.Service__r.Site__r.id, lineItems);
        }
        
        for (QuoteRequestService__c sZ : servicesZ){
            List<QuoteRequestService__c> lineItems = siteZ_QRSMap.get(sZ.Service__r.Site__r.id);
            if (lineItems == null){
                lineItems = new List<QuoteRequestService__c>();
            }
            
            lineItems.add(sZ);
            siteZ_QRSMap.put(sZ.Service__r.Site__r.id, lineItems);
        }
    }
    
    public void previewPDF(){
        String sTable;  // a table for one of the sites
        List<String> lLocationTable = new List<String>();   // This will store all of the tables and then we can grab each and place in the showTable
        
        for(Id sId : siteA_QRSMap.keySet()){   
            for (QuoteRequestService__c qrs : siteA_QRSMap.get(sId)){
                sTable = '';
                if(  qrs.Service__c!=null && qrs.Service__r.SiteDisplayName__c!=null && qrs.Service__r.Service_Type__c!=null ){
                
                sTable = '<span style="padding-left:5px;" class="font7"><b>'+Label.Site+': '  + ' : ' + qrs.Service__r.SiteDisplayName__c.toUpperCase() + ' - ' + qrs.Service__r.Service_Type__c.toUpperCase();
                sTable += '</b></span>'; 
                }
                sTable += '<br/><table  width="100%" border="0" cellpadding = "0" cellspacing = "0">';
                sTable += '<tr><th colspan="2" style="padding-left:5px;padding-top:5px;">Service Information</th></tr>';
                sTable += '<tr>';
                sTable += '<td width="20%" style="padding-left:5px;" align="left" class="font7b">Product Type: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.Product_Type__c)?'':qrs.ServiceRequestInfo__r.Product_Type__c) + '</td>';
                sTable += '<td width="20%" align="left" class="font7b">NPA_NXX: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.NPA_NXX__c)?'':qrs.ServiceRequestInfo__r.NPA_NXX__c) + '</td>';
                sTable += '</tr>';
                 sTable += '<tr>';
                sTable += '<td width="20%" style="padding-left:5px;" align="left" class="font7b">Access: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.Access__c)?'':qrs.ServiceRequestInfo__r.Access__c) + '</td>';
                sTable += '<td width="20%" align="left" class="font7b">Circuit: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.Circuit_ID__c)?'':qrs.ServiceRequestInfo__r.Circuit_ID__c) + '</td>';
                sTable += '</tr>';
                sTable += '<tr>';
                sTable += '<td width="20%" style="padding-left:5px;" align="left" class="font7b">CoS: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.CoS__c)?'':qrs.ServiceRequestInfo__r.CoS__c) + '</td>';
                sTable += '<td width="20%" align="left" class="font7b">Diversity__c: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.Diversity__c)?'':qrs.ServiceRequestInfo__r.Diversity__c) + '</td>';
                sTable += '</tr>';
                sTable += '<tr>';
                sTable += '<td width="20%" style="padding-left:5px;" align="left" class="font7b">EVC: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.EVC__c)?'':qrs.ServiceRequestInfo__r.EVC__c) + '</td>';
                sTable += '<td width="20%" align="left" class="font7b">NNI: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.NNI__c)?'':qrs.ServiceRequestInfo__r.NNI__c) + '</td>';
                sTable += '</tr>';
                sTable += '<tr>';
                sTable += '<td width="20%" style="padding-left:5px;" align="left" class="font7b">Other EVC Size: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.Other_EVC_Size__c)?'':qrs.ServiceRequestInfo__r.Other_EVC_Size__c) + '</td>';
                sTable += '<td width="20%" align="left" class="font7b">MTU Size: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.MTU_Size__c)?'':qrs.ServiceRequestInfo__r.MTU_Size__c) + '</td>';
                sTable += '</tr>';
                sTable += '<tr>';
                sTable += '<td width="20%" style="padding-left:5px;" align="left" class="font7b">Other Cos: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.Other_Cos__c)?'':qrs.ServiceRequestInfo__r.Other_Cos__c) + '</td>';
                sTable += '<td width="20%" align="left" class="font7b">Port: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.Port__c)?'':qrs.ServiceRequestInfo__r.Port__c) + '</td>';
                sTable += '</tr>';
                
                sTable += '<tr>';
                sTable += '<td width="100%" colspan="4" style="padding-top:5px;padding-bottom:5px;padding-left:5px;" align="left" class="font7b"><b>Notes:</b></td>';
                sTable += '</tr>';
                sTable += '<tr>';
                sTable += '<td width="100%" colspan="4" style="padding-left:5px;padding-right:5px;padding-bottom:5px;" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.Notes__c)?'':qrs.ServiceRequestInfo__r.Notes__c) + '</td>';
                sTable += '</tr>';
                
                sTable += '</table>';
                lLocationTable.add(sTable);
            }
        }
        showTable = '<h1>Location As</h1>';
        showTable = '<table width="100%" border="0" cellpadding = "0" cellspacing = "0">';
        Integer i = 1;
        for(String s1:lLocationTable){   
                //Wrap Table in side Table to avoid page break inside table 
                showTable += '<tr><td><table  style="margin-left:5px;margin-right:5px;page-break-inside: avoid;"  width="98%" border="1" cellpadding = "0" cellspacing = "0">';
                showTable += '<tr>';
                showTable += '<td>' + s1 + '</td>' ;
                showTable += '</tr>';
                showTable += '</table></td></tr><tr><td>&nbsp;</td></tr>';
                
                i++;
        }
            
        showTable += '</table>';
        
        for(Id sId : siteZ_QRSMap.keySet()){   
            for (QuoteRequestService__c qrs : siteZ_QRSMap.get(sId)){
                sTable = '';
                 if(  qrs.Service__c!=null && qrs.Service__r.SiteDisplayName__c!=null && qrs.Service__r.Service_Type__c!=null ){
                sTable = '<span style="padding-left:5px;" class="font7"><b>'+Label.Site+': '  + ' : ' + qrs.Service__r.SiteDisplayName__c.toUpperCase() + ' - ' + qrs.Service__r.Service_Type__c.toUpperCase();
                sTable += '</b></span>'; 
                 }
                sTable += '<br/><table  width="100%" border="0" cellpadding = "0" cellspacing = "0">';
                sTable += '<tr><th colspan="2" style="padding-left:5px;padding-top:5px;">Service Information</th></tr>';
                sTable += '<tr>';
                sTable += '<td width="20%" style="padding-left:5px;" align="left" class="font7b">Product Type: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.Product_Type__c)?'':qrs.ServiceRequestInfo__r.Product_Type__c) + '</td>';
                sTable += '<td width="20%" align="left" class="font7b">NPA_NXX: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.NPA_NXX__c)?'':qrs.ServiceRequestInfo__r.NPA_NXX__c) + '</td>';
                sTable += '</tr>';
                 sTable += '<tr>';
                sTable += '<td width="20%" style="padding-left:5px;" align="left" class="font7b">Access: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.Access__c)?'':qrs.ServiceRequestInfo__r.Access__c) + '</td>';
                sTable += '<td width="20%" align="left" class="font7b">Circuit: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.Circuit_ID__c)?'':qrs.ServiceRequestInfo__r.Circuit_ID__c) + '</td>';
                sTable += '</tr>';
                sTable += '<tr>';
                sTable += '<td width="20%" style="padding-left:5px;" align="left" class="font7b">CoS: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.CoS__c)?'':qrs.ServiceRequestInfo__r.CoS__c) + '</td>';
                sTable += '<td width="20%" align="left" class="font7b">Diversity__c: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.Diversity__c)?'':qrs.ServiceRequestInfo__r.Diversity__c) + '</td>';
                sTable += '</tr>';
                sTable += '<tr>';
                sTable += '<td width="20%" style="padding-left:5px;" align="left" class="font7b">EVC: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.EVC__c)?'':qrs.ServiceRequestInfo__r.EVC__c) + '</td>';
                sTable += '<td width="20%" align="left" class="font7b">NNI: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.NNI__c)?'':qrs.ServiceRequestInfo__r.NNI__c) + '</td>';
                sTable += '</tr>';
                sTable += '<tr>';
                sTable += '<td width="20%" style="padding-left:5px;" align="left" class="font7b">Other EVC Size: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.Other_EVC_Size__c)?'':qrs.ServiceRequestInfo__r.Other_EVC_Size__c) + '</td>';
                sTable += '<td width="20%" align="left" class="font7b">MTU Size: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.MTU_Size__c)?'':qrs.ServiceRequestInfo__r.MTU_Size__c) + '</td>';
                sTable += '</tr>';
                sTable += '<tr>';
                sTable += '<td width="20%" style="padding-left:5px;" align="left" class="font7b">Port: </td>';
                sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(qrs.ServiceRequestInfo__r.Port__c)?'':qrs.ServiceRequestInfo__r.Port__c) + '</td>';
                sTable += '<td width="20%" align="left" class="font7b"></td>';
                sTable += '<td width="30%" align="left" class="font7b"></td>';
                sTable += '</tr>';
                
                sTable += '</table>';
                lLocationTable.add(sTable);
            }
        }
        showTable = '<h1>Location Zs</h1>';
        showTable = '<table width="100%" border="0" cellpadding = "0" cellspacing = "0">';
        i = 1;
        for(String s1:lLocationTable){   
                //Wrap Table in side Table to avoid page break inside table 
                showTable += '<tr><td><table  style="margin-left:5px;margin-right:5px;page-break-inside: avoid;"  width="98%" border="1" cellpadding = "0" cellspacing = "0">';
                showTable += '<tr>';
                showTable += '<td>' + s1 + '</td>' ;
                showTable += '</tr>';
                showTable += '</table></td></tr><tr><td>&nbsp;</td></tr>';
                
                i++;
        }
            
        showTable += '</table>';
    }
    
    public class ServiceWrapper{
        public QuoteLineItem qli {get; set;}
        public Boolean hasInfo;  
        public Boolean isDisabled;
        public Boolean isActive;       
        
        public void sethasInfo(Boolean b) { this.hasInfo = b; }
        public Boolean gethasInfo() { return hasInfo; }
        
        public void setIsDisabled(Boolean b) { this.isDisabled = b; }
        public Boolean getIsDisabled() { return isDisabled; }
        
        public void setIsActive(Boolean b) { this.isActive = b; }
        public Boolean getIsActive() { return isActive; }
        
        public ServiceWrapper() {
            // constructor
        }
        
        public ServiceWrapper(QuoteLineItem qli) { 
            this.qli = qli;
            this.hasInfo = false;
            this.isActive = false; 
            this.isDisabled = false;
        }
        /*
        public ServiceWrapper(QuoteLineItem qli, Boolean hasInfo, Boolean isActive) {
            this.qli = qli;
            this.hasInfo = hasInfo;
            this.isActive = isActive;
            this.isDisabled = false;
        }
        */
        
        /* hasInfo and isCurrent */
        public ServiceWrapper(QuoteLineItem qli, Boolean hasInfo, Boolean isDisabled, Boolean isActive) {
            this.qli = qli;
            this.hasInfo = hasInfo;
            this.isActive = isActive;
            this.isDisabled = isDisabled;
        }
        
        /* hasInfo and isCurrent */
        public ServiceWrapper(QuoteLineItem qli, Boolean hasInfo, Boolean isActive) {
            this.qli = qli;
            this.hasInfo = hasInfo;
            this.isActive = isActive;
            this.isDisabled = false;
        }
    }
    
}