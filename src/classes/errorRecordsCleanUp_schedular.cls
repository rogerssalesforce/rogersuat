/*
===============================================================================
 Class Name   : errorRecordsCleanUp_schedular
===============================================================================
PURPOSE:    Schedules errorRecordsCleanUp_batch with batches of 200 records.

Author	 : Jacob Klay, Rogers Communications
Date	 : 24-June-2o14

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
===============================================================================
*/
global class errorRecordsCleanUp_schedular implements schedulable{

    global void execute(SchedulableContext ctx) {
       
        errorRecordsCleanUp_batch sc = new errorRecordsCleanUp_batch();
        ID batchprocessid = Database.executeBatch(sc);
        system.debug('sc*************' +sc);
    }

}