/*
===============================================================================
 Class Name   : TAG_DistOwnrUpdate_Account_batch 
===============================================================================
PURPOSE:  (This Class will support the logic for District Owner before Update Trigger.)  
            Get list of all Account for a District Owner and 
            update them with owner and set a flag which is used for MAL integration.
  

Developer: Aakanksha Patel
Date: 06/29/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
06/29/2015           Aakanksha Patel             Original Version
===============================================================================
*/
global class TAG_DistOwnrUpdate_Account_batch implements Database.Batchable<SObject>, Database.stateful{
    
    Global String query;
    Global Map<ID,District__c> mapIdDistrict;
    Global set<ID> accountIds = new set<ID>();
        
    global TAG_DistOwnrUpdate_Account_batch(Map<ID,District__c> mapIdDistrict)
    {
        this.mapIdDistrict = mapIdDistrict;  // constructor copies in arg to local instance vbl
    }
      
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        set<Id>  tempSet = new Set<Id>();
        tempSet = mapIdDistrict.keyset();
       
       if(!Test.isRunningTest()){
        return Database.getQueryLocator('select id,OwnerId,District__c,Account_Owner_to_MAL__c,District__r.District_Owner__c from Account where District__c IN : tempSet');
        }
        else{
           return Database.getQueryLocator('select id,OwnerId,District__c,Account_Owner_to_MAL__c,District__r.District_Owner__c from Account where District__c IN : tempset limit 10');
        }     
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope)
    {
        Set<Account> setaccountUpdate = new Set<Account>();
       
        for(Account accRec: scope) 
        {
            if(accRec.District__r.District_Owner__c!=null)
            {
                accRec.OwnerId =accRec.District__r.District_Owner__c;
            }
            accRec.Account_Owner_to_MAL__c = TRUE;
            accRec.Recalculate_Team__c = TRUE;                
            accountIds.add(accRec.Id);
            setaccountUpdate.add(accRec);
        }
        update scope;
        
        if(setaccountUpdate.size()!=NULL)
        {
            List<Account> listAccountUpdate = new List<Account>();
            listAccountUpdate.addAll(setaccountUpdate);
            set<Account> temp = new set<Account>(listAccountUpdate);

            listAccountUpdate.clear();
            listAccountUpdate.addAll(temp);
            update listAccountUpdate;
        }
    
    }
    
    global void finish(Database.BatchableContext BC)
    {       
         Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
         Integer batchSize = Integer.ValueOf(TAGsettings.DistrictOwnerUpdateBatch_size__c);
        
         TAG_DistOwnrUpdate_MSD_batch sc1 = new TAG_DistOwnrUpdate_MSD_batch(mapIdDistrict,accountIds);
         ID batchprocessid = Database.executeBatch(sc1,batchSize);
    }
}