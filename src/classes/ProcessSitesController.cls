public with sharing class ProcessSitesController {

    public Opportunity opp {get; set;}
    private Id oppId;
    public Id activeSite {get; set;}
    public String currentLocation {get; set;}
    public List<SiteWrapper> prospectSites {get; set;}
    public List<ServiceableLocationWrapper> resultList {get; set;}
    public Boolean renderTable {get; set;}
    public Boolean renderMessage {get; set;}
    public Boolean renderSiteTable {get; set;}
    public Boolean renderSiteMessage {get; set;}
    public Map<String, Site__c> prospectSitesMap {get; set;}
    public List<Site__c> syncList {get; set;}
    public Map<Id, ServiceableLocation__c> matchingResultsMap {get; set;}   //Site is matched with a ServiceableLocation
    private static final String SERVICEABLE_LOCATION = 'Serviceable Location';
    private String[] adminEmailList;
    
    public ProcessSitesController() {
        try{
        	oppId = ApexPages.currentPage().getParameters().get('retURL');
        } catch (Exception ex){  // In the case that the user uses the application incorrectly and selects a site before hitting process Sites
        	oppId = ApexPages.currentPage().getParameters().get('retURL').substring(1);
        }
        opp = [SELECT Id, AccountId From Opportunity WHERE Id = :oppId];
        
        try{
            Custom_Property__c property = [SELECT Id, Value__c FROM Custom_Property__c WHERE Name = 'adminEmail' LIMIT 1];
            
            if (property != null)
                try{
                    adminEmailList = new String[]{property.Value__c};
                }catch(Exception ex){
                    adminEmailList = new String[0];
                } 
            else
                adminEmailList = new String[0];
        }catch(Exception ex){
             adminEmailList = new String[0];
        }
        
        /* Prepare the collections for use later on within the class */
        matchingResultsMap = new Map<Id, ServiceableLocation__c>();
        prospectSitesMap = new Map<String, Site__c>();
        syncList = new List<Site__c>();
        
        // Obtain any prospective sites
        loadProspectSites(oppId, false);      
        
        /* Check if any prospectSites now have a perfect match with a Serviceable Location.  If so, remove them from the prospectSites
           so that they do not display on the page and add them to the list of sites that need to be updated and matched
           with the serviceable location.
        */  
     
     	List<String> uniqueList = new List<String>();
        for(ServiceableLocation__c match : [SELECT Id, Name, Access_Type__c, Access_Type_Group__c, LocationKey__c FROM ServiceableLocation__c WHERE LocationKey__c IN :prospectSitesMap.keySet()]){
            // Need to ensure that there are no duplicates in the Serviceable Location List that was returned.
            // If there is we should only do it once.
            if (!Utils.listContains(uniqueList, match.LocationKey__c)){
	            uniqueList.add(match.LocationKey__c);
	            Site__c s = prospectSitesMap.get(match.LocationKey__c.toLowerCase());
	            if (s!=null){  // This should not be a problem since we know the key is in there but it does not hurt.
	            	s.ServiceableLocation__c = match.Id;
	            	removeSite(s.Id);
	            	s.Access_Type__c = match.Access_Type__c;
	            	s.Access_Type_Group__c = match.Access_Type_Group__c;
	            	syncList.add(s);
	        	}
            }else{
            	Utils.sendEmail('You are receiving this message because a site was process and there were multiple ' +
            	'Serviceable Locations with the matching Location Key: ' + match.LocationKey__c, 'Action Required: Duplicate Serviceable Location', adminEmailList);
            }
        }
        if (prospectSites.size() == 0)
        	renderSiteTable = false;
        else
        	renderSiteTable = true;
        
        renderSiteMessage = !renderSiteTable;
        
    }
    /*
        syncSites is meant to be called through an action when the page loads to Update all of the sites that have an exact match
        with a Serviceable Location.  This is done in a seperate method through an action since you cannot call DML through the
        constructor.
    */
    public PageReference syncSites(){
        for (Site__c site : syncList){
            UPSERT site;
        }
        
        return null;
    }
    
    /* Helper method that is used to remove a site from the prospectSites.*/
    private void removeSite(Id siteId){
        Site__c s;
        Integer i = 0;
        for (SiteWrapper currentSite : prospectSites){
            if (currentSite.Site.Id == siteId){
                break;
            }
            i++;
        }
        prospectSites.remove(i);
    }
    
    /* When you choose a Serviceable Location for a particular site this will update the ServiceableLocation for that site.*/
    public PageReference updateServiceableLocation(){
        if (activeSite!=null){
        	
            matchingResultsMap.put(activeSite, [SELECT Id, Name, Access_Type__c, Access_Type_Group__c, CNINNI__c FROM ServiceableLocation__c WHERE Name = :currentLocation LIMIT 1]);
        
            Site__c s;
            Integer i = 0;
            for (SiteWrapper currentSite : prospectSites){
                    if (currentSite.Site.Id == activeSite){
                        s = currentSite.Site;
                        break;
                    }
                    i++;
            }
            s.ServiceableLocation__r = matchingResultsMap.get(activeSite);
            s.Access_Type__c = s.ServiceableLocation__r.Access_Type__c;
            s.Access_Type_Group__c = s.ServiceableLocation__r.Access_Type_Group__c;
            s.Type__c = s.ServiceableLocation__r.CNINNI__c;
            if(Utils.isEmpty(s.Type__c))
            	s.Type__c = SERVICEABLE_LOCATION;
            
            SiteWrapper sw = new SiteWrapper(s);
            prospectSites.set(i, sw);
            
        }
        return null;
    }
    
    /* This is meant to remove a servicable location from a site when you uncheck it in the table. */
     public PageReference removeServiceableLocation(){
        if (activeSite!=null){
            matchingResultsMap.remove(activeSite);
        
            Site__c s;
            Integer i = 0;
            for (SiteWrapper currentSite : prospectSites){
                    if (currentSite.Site.Id == activeSite){
                        s = currentSite.Site;   
                        break;
                    }
                    i++;
            }
            if (s!=null){
            	s.ServiceableLocation__r.Name = '';
            	SiteWrapper sw = new SiteWrapper(s);
            	prospectSites.set(i, sw);
            }
        }
        
        return null;
    }
 
    /* 
        When the user checks a site this method is run and a call to check for possible serviceable locations is called.
        If there is no possible locations then a variable is set to decide if we will render the table or display a message
        indicating that no site have been found.
     */
    public PageReference check(){
        if (activeSite!=null){
            Site__c s;
            Integer i = 0;
            for (SiteWrapper currentSite : prospectSites){
                if (currentSite.Site.Id == activeSite){
                    s = currentSite.Site;
                    break;
                }
                i++;
            }
                
            resultList = getPossibleServiceableLocations(s);
            if (resultList.isEmpty())
                renderTable = false;
            else
                renderTable = true;
            renderMessage = !renderTable;
        }

        return null;
    }
    
    /* 
        Helper method that obtains all of the Serviceable Locations based upon a partial match for a particular Site.
    */
    private List<ServiceableLocationWrapper> getPossibleServiceableLocations(Site__c thisSite){
        List<ServiceableLocationWrapper> wrapperList = new List<ServiceableLocationWrapper>();
        
      
        List<ServiceableLocation__c> servList = new List<ServiceableLocation__c>();
        
        List<String> clliCodes = new List<String>();
        
        if (!Utils.isEmpty(thisSite.Postal_Code__c))
            for (CLLIPostalCode__c code : [SELECT Id, Postal_Code__c, CLLI_Code__c FROM CLLIPostalCode__c where Postal_Code__c = :thisSite.Postal_Code__c]){
                if (!Utils.listContains(clliCodes, code.CLLI_Code__c))
                    clliCodes.add(code.CLLI_Code__c);
            }
        
        String query = 'SELECT  Id, Name, Suite_Floor__c, CNINNI__c, Street_Number__c, Street_Name__c, Street_Type__c, Street_Direction__c, Province_Code__c, Postal_Code__c, City__c, Access_Type__c, Access_Type_Group__c FROM serviceableLocation__c WHERE City__c = \''+ String.escapeSingleQuotes(thisSite.City__c) + '\' and Street_Number__c = \''+ String.escapeSingleQuotes(thisSite.Street_Number__c) + '\' and Street_Name__c = \''+String.escapeSingleQuotes(thisSite.Street_Name__c)+'\'';
        
        if (!Utils.isEmpty(thisSite.Postal_Code__c))
            query += ' and CLLI_Code__c IN :clliCodes';
                
        try{
            servList = Database.query(query);
        } catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            
        }       
        
        ServiceableLocation__c selected = matchingResultsMap.get(thisSite.Id);
        String name;
        if (selected != null)
            name = selected.Name;
        
        for(ServiceableLocation__c s : servList){
            ServiceableLocationWrapper sw = new ServiceableLocationWrapper(s);
            sw.candidateSiteId = thisSite.Id;
            
            if (name==s.Name)
                sw.setIsSelected(true);
            
            wrapperList.add(sw);
        }                                       
        return wrapperList;                                         
    }
    
    /* Called from the Constructor to obtain all of the sites that do not have a serviceable location.  
       This method will also generate the map that will have the site name as a key to quickly obtain the 
       site based on the name for matching to a Serviceable Location.
    */
    private void loadProspectSites(Id opportunityId, Boolean reload) {
        try{
            if(reload || prospectSites == null) {  //allow for a reload in the case that there is an error
                prospectSites= new List<SiteWrapper>();
                for(Site__c s : [select s.Id , s.Name, s.Suite_Floor__C, s.Street_Number__c
                                        , s.Street_Name__c
                                        , s.Street_Type__c
                                        , s.Street_Direction__c
                                        , s.City__c
                                        , s.LocationKey_del__c
                                        , s.Postal_Code__c
                                        , s.Province_Code__c
                                        , s.ServiceableLocation__r.Id
                                        , s.ServiceableLocation__r.Name, s.Type__c
                                         from Site__c s
                                         where Opportunity__c = : opportunityId
                                         and ServiceableLocation__c = null
                                         order by CreatedDate]) {
                    
                                       
                    prospectSites.add(new SiteWrapper(s));
                    prospectSitesMap.put(s.Name.toLowerCase(), s);
                    //prospectSitesMap.put(s.LocationKey_del__c.toLowerCase(), s);
                    
                    //System.debug('\n\n\n\n\n\nprospectSites Before: '+ prospectSitesMap);
                    
                }
            }
        }
        catch(DMLException e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
        }
    }
  
    /* All the sites that have been set with a servicable location will be updated through this methods. */
    public PageReference processUpdates(){
        Boolean hasErrors = false;
        
        for (SiteWrapper s : prospectSites){
            if (matchingResultsMap.containsKey(s.Site.Id)){
                s.Site.ServiceableLocation__c = matchingResultsMap.get(s.Site.Id).Id;
                try{
                	upsert s.Site;
                }catch(DMLException ex){
            		hasErrors = true;
                }
            }
        }
        if (!hasErrors){
        	PageReference oppPage = new ApexPages.StandardController(opp).view();
        	oppPage.setRedirect(true);
        
        	return oppPage;
        }else{
        	loadProspectSites(oppId, true); 
        	resultList.clear();  
        	renderTable = false; 
        	return null;
        }
    }
    
     public PageReference returnToOpportunity(){
    	PageReference oppPage = new ApexPages.StandardController(opp).view();
        oppPage.setRedirect(true);
        return oppPage;
    }
}