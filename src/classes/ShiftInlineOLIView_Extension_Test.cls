/*
@Name:  ShiftInlineOLIView_Extension_Test 
@Description: Test the Visualforce extension for the ShiftInlineOLIView page to display the Opportunity Line Items
@Dependancies: ShiftInlineOLIView_Extension
@Version: 1.0.0

===VERSION HISTORY ===
Date        | Version Number | Author | Description
04-14-2014  | 1.0.0          | Jocsan | Initial
*/
@isTest (SeeAllData=true) // Needed to get the Product Entries
private class ShiftInlineOLIView_Extension_Test {
    private static List<Opportunity> theOpportunityList;
    private static List<Account> theAccountList;
    private static Integer aggregatedProductNumber;
    
    static testMethod void testGetOli() {
        dataGenerator(1);
        ApexPages.Standardcontroller sc = New ApexPages.StandardController(theOpportunityList[0]);
        ShiftInlineOLIView_Extension ext = new ShiftInlineOLIView_Extension(sc);
        //System.assertEquals(aggregatedProductNumber, ext.theOLIList.size(),'Wrong number of OLI displayed');        
    }

    static testMethod void testCancel() {
        dataGenerator(1);
        ApexPages.Standardcontroller sc = New ApexPages.StandardController(theOpportunityList[0]);
        ShiftInlineOLIView_Extension ext = new ShiftInlineOLIView_Extension(sc);
        ext.theCancel();
        //System.assertEquals(aggregatedProductNumber, ext.theOLIList.size(),'Wrong number of OLI displayed');        
    }

    static testMethod void testUpdate() {
        dataGenerator(1);
        ApexPages.Standardcontroller sc = New ApexPages.StandardController(theOpportunityList[0]);
        ShiftInlineOLIView_Extension ext = new ShiftInlineOLIView_Extension(sc);
        if(ext.theOliList!=null && ext.theOliList.size()>0)
        ext.theOliList[0].quantity = 100;
        ext.theSave();
        
        //OpportunityLineItem theUpdatedOli = [select Id, Quantity from OpportunityLineItem where Id =: ext.theOLIList[0].id];
        //System.assertEquals(100, theUpdatedOli.Quantity,'Wrong number of OLI displayed');        
    }
    
    static void dataGenerator (Integer numberOfRecords) {

        theAccountList = new List<Account>();       
        for (integer i=0; i<numberOfRecords; i++) {
            Account theAccount = new Account(Name = 'AccountTest'+i);    
            theAccountList.add(theAccount);

        }
        insert theAccountList; 
        
        //Get the Opportunity Record types PS,Bling,Playback and SIM
        Schema.DescribeSObjectResult opportunitySchema = Schema.SObjectType.Opportunity; 
 
        Id oppRt = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Data Centre - RFP').getRecordTypeId();             
        
        theOpportunityList = new List <Opportunity>();   
        for (integer i=0; i<numberOfRecords; i++) {            
            Opportunity theOpportunity1 = new Opportunity (Name = 'theOpportunity '+i, 
                                                           RecordTypeId = oppRt,
                                                           AccountId = theAccountList[i].id,
                                                           CloseDate = date.Today(),
                                                           Unified_Comm_Collaboration_Estimated__c =20,
                                                           StageName = 'Identify',
                                                           Amount = 50 );
            theOpportunityList.add(theOpportunity1);
        }         
        insert theOpportunityList;
        
        List<Product2> currentProductList = new List<Product2> ([select Name, isActive, Aggregated_Product__c from Product2 where Aggregated_Product__c  = true]);    
        aggregatedProductNumber = currentProductList.size();       
    }     
}