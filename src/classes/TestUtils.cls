/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
/*
========================================================================
Date                        Name                        Status
March 2015         Aakanksha Patel            Modified(For TAG)
========================================================================
*/ 
 
@isTest
private class TestUtils {

    static Boolean withAssertion = true;

    static testMethod void removeNullTest() {
        // Check for null String, non null string, String is empty String
        String s1 = null;
        String s2 = 'Test';
        String s3 = '';
        
        String resultS1 = Utils.removeNull(s1);
        String resultS2 = Utils.removeNull(s2);
        String resultS3 = Utils.removeNull(s3);
        
        if (withAssertion){
            System.assertEquals(resultS1, '');
            System.assertEquals(resultS2, s2);
            System.assertEquals(resultS3, '');
        }
    }
    
    static testMethod void isEmptyTest() {
        // Check a string that is empty, that is null, that is not empty
        // Check for null String, non null string, String is empty String
        String s1 = null;
        String s2 = 'Test';
        String s3 = '';
        Decimal d1 = 0.0;
        Decimal d2 = null;
        
        Boolean resultS1 = Utils.isEmpty(s1);
        Boolean resultS2 = Utils.isEmpty(s2);
        Boolean resultS3 = Utils.isEmpty(s3);
        Boolean resultD1 = Utils.isEmpty(d1);
        Boolean resultD2 = Utils.isEmpty(d2);
        
        if (withAssertion){
            System.assertEquals(resultS1, true);
            System.assertEquals(resultS2, false);
            System.assertEquals(resultS3, true);
            System.assertEquals(resultD1, false);
            System.assertEquals(resultD2, true);
        }
    }
    
    static testMethod void containsNullTest() {
        // Check null list, check list that is non null and has no nulls, check list that is non null 
        // and contains a null, check an empty list
        List<String> list1 = new List<String>();
        List<String> list2 = new List<String>();
        List<String> list3 = null;      // Null List
        
        // Non Empty List - with no nulls
        list1.add('Test1');
        list1.add('Test2');
        
        // Non-Empty List - with a null
        list2.add(null);
        list2.add('Test');
               
        Boolean resultList1 = Utils.containsNull(list1);
        Boolean resultList2 = Utils.containsNull(list2);
        Boolean resultList3 = Utils.containsNull(list3);
        
        if (withAssertion){
            System.assertEquals(resultList1, false);
            System.assertEquals(resultList2, true);
            System.assertEquals(resultList3, false);
        }
    }
    
    static testMethod void listContainsTest() {
        // Check null list, null string, list contains string, list does not contain string, list
        // contains null value and the string is null
        
        String s1 = null;
        String s2 = 'Test1';
        String s3 = '';
        String s4 = 'Test';
        
        List<String> list1 = new List<String>();
        List<String> list2 = new List<String>();
        List<String> list3 = null;      // Null List
        
         // Non Empty List - with no nulls
        list1.add('Test1');
        list1.add('Test2');
        
        // Non-Empty List - with a null
        list2.add(null);
        list2.add('Test');
        
        Boolean resultS1 = Utils.listContains(list3, s1);
        Boolean resultS2 = Utils.listContains(list1, s1);
        Boolean resultS3 = Utils.listContains(list2, s1);
        Boolean resultS4 = Utils.listContains(list1, s2);
        Boolean resultS5 = Utils.listContains(list1, s4);
        
        if (withAssertion){
            System.assertEquals(resultS1, false);
            System.assertEquals(resultS2, false);
            System.assertEquals(resultS3, true);
            System.assertEquals(resultS4, true);
            System.assertEquals(resultS5, false);
        }
        
    }
    
    static testMethod void testParseList(){
        String goodTest1 = '1. ItemA\n2. ItemB\n3. ItemC';
        String goodTest2 = '1. ItemA';
        String emptyTest = '';
        String nullTest = null;
        
        Test.startTest();
        System.assertEquals(3,Utils.parseList(goodTest1).size());
        System.assertEquals(1,Utils.parseList(goodTest2).size());
        System.assertEquals(0,Utils.parseList(emptyTest).size());
        System.assertEquals(0,Utils.parseList(nullTest).size());
        
        Test.stopTest();
        
    }
    
    static testMethod void testRemoveSpacesFromList(){
        List<String> emptyList = new List<String>();
        List<String> nullList = null;
        List<String> goodListHasNoSpaces = new List<String>{'A', 'B', 'C', 'D'}; 
        List<String> goodListHasSpaces = new List<String>{' A', 'B ', ' C ', 'D'};
                
        Test.startTest();
        System.assertEquals(0,Utils.removeSpacesFromList(emptyList).size());
        System.assertEquals(null,Utils.removeSpacesFromList(nullList));
        System.assertEquals(goodListHasNoSpaces,Utils.removeSpacesFromList(goodListHasNoSpaces));
        System.assertEquals(goodListHasNoSpaces,Utils.removeSpacesFromList(goodListHasSpaces));
        
        Test.stopTest();
    }
    
    static testMethod void testValidateNumericList(){
        List<String> emptyList = new List<String>();
        List<String> nullList = null;
        List<String> goodTest = new List<String>{'1', '2', '3', '4'}; 
        List<String> badTest1 = new List<String>{' A', '1', 'C', 'D'};
        List<String> badTest2 = new List<String>{' 1', '2', '3', '4D'};
        List<String> badTest3 = new List<String>{'1', '2', '3', '4'};
                
        Test.startTest();
        System.assertEquals(true,Utils.validateNumericList(emptyList, 10));
        System.assertEquals(true,Utils.validateNumericList(nullList, 10));
        System.assertEquals(true,Utils.validateNumericList(goodTest, 10));
        System.assertEquals(false,Utils.validateNumericList(badTest1, 10));
        System.assertEquals(false,Utils.validateNumericList(badTest2, 10));
        System.assertEquals(false,Utils.validateNumericList(badTest3, 2));
        
        Test.stopTest();
    }
    
    static testMethod void testSendEmail(){
        String message = 'This is the body of an email.';
        String subject = 'This is the subject';
        String[] toAddresses = new String[]{'test_email@testing.com'};
        
        Test.startTest();
        Utils.sendEmail(message, subject, toAddresses);
        Utils.sendEmail(message, subject, null);
        Utils.sendEmail(message, null, toAddresses);
        Utils.sendEmail(null, subject, toAddresses);
        Test.stopTest();
        
    }
    
    static testMethod void testGetMonth(){
        String message = 'This is the body of an email.';
        String subject = 'This is the subject';
        String[] toAddresses = new String[]{'test_email@testing.com'};
        
        Test.startTest();
           for (Integer i=0; i<20; i++){
               Utils.getMonth(i, Utils.ENGLISH);
               Utils.getMonth(i, Utils.FRENCH);
               Utils.getMonth(i, -1);
           }
        Test.stopTest();
    }
    
    static testMethod void testRemoveAndConvertHTML(){
        String testString1 = 'This is a String';
        String testString2 = 'This<br> is <br>a String';
        String testString4 = 'This<br/> is <br/>a String';
        String testString3 = 'This\n is \na String';
        
        Test.startTest();
        System.assertEquals(testString1, Utils.removeHTML(testString1));
        System.assertEquals(testString1, Utils.removeHTML(testString4));
        System.assertEquals(testString1, Utils.convertToHTML(testString1));
        System.assertEquals(testString4, Utils.convertToHTML(testString3));
        System.assertEquals(testString1, Utils.removeHTML(testString2));        
        System.assertEquals(testString2, Utils.convertToHTML(testString2));               
        Test.stopTest();
    
    }
    static testMethod void testTranslateDateAndFormatDate(){
        
        String dateString = '3 months';
        Date testDate = Date.today();
        Date testDate2 = Date.newinstance(1960, 2, 7);
        Test.startTest();
        Utils.translateDate(dateString, Utils.ENGLISH, Utils.FRENCH);
        Utils.translateDate(dateString, Utils.FRENCH, Utils.ENGLISH);    
        Utils.formatDate(testDate, Utils.ENGLISH);
        Utils.formatDate(testDate2, Utils.ENGLISH);        
        Utils.formatDate(testDate, Utils.FRENCH);
        Utils.formatDate(null, Utils.FRENCH);
        
        Test.stopTest();
    }
    
    static testMethod void testDisplay(){
        Decimal temp1 = null;
        Decimal temp2 = 3.2;
        
        List<String> values = new List<String>();
        Test.startTest();
        Utils.parseList(values, ',');
        Utils.makeMultiPicklist(values);
        values.add('1');
        Utils.parseList(values, ',');
        Utils.makeMultiPicklist(values);
        values.add('2');
        Utils.parseList(values, ',');
        Utils.makeMultiPicklist(values);
        
        
        System.assertEquals('', Utils.display(temp1));
        System.assertEquals('3.2', Utils.display(temp2)); 
        Test.stopTest();
    }
    
    static testMethod void testGenerate18CharId(){
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
       
        Account a = new Account();
        a.name = 'Test Act - Utils test';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street Wizard';
        a.BillingCity = 'Ontario';
        a.BillingCountry = 'CA';
        a.BillingPostalCode = 'A9A 9A9';
        a.BillingState = 'ON';
        a.Parentid = null;
        a.Account_Status__c = 'Assigned';
        insert a;       
        
        
        String id15 = ('' + a.Id).substring(0, 15);
        String id14 = ('' + a.Id).substring(0, 14);
        Test.startTest();
        Utils.generate18CharId(id15);
        Utils.generate18CharId(null);
        Utils.generate18CharId(id14);
        Test.stopTest();
        
        
    }
    
    static testMethod void removeNullsAndSpacesTest(){
        String s = '   this is a test   ';
        Utils.removeNullsAndSpaces(s);
        s = null;
        Utils.removeNullsAndSpaces(s);      
    }
    
    static testMethod void pickValues(){
        String objectName = 'Quote';
        String fieldName = 'Status';
        Utils.getPicklistValues(objectName, fieldName);     
    }
    
    static testMethod void testValidEmail(){
        String e1 = 'test@test.com';
        String e2 = 'testtest.com';
        
        Boolean e1Result = Utils.isValidEmail(e1);
        Boolean e2Result = Utils.isValidEmail(e2);
        
        if (withAssertion){
            System.assertEquals(e1Result, true);
            System.assertEquals(e2Result, false);
        }
    }
    
        
}