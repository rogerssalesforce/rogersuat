@isTest(seeAlLData=true) 
private class RemoteServiceTest {
    
    static testMethod void loadDataWithoutOpp() {
        RemoteService controller1 = new RemoteService();
        
        System.assertNotEquals(0, RemoteService.loadData('test').size());
        
        //controller.loadDataAction();
        
        Account a = new Account();
        a.name= 'A45cc56ou67ntTe57804st';
        a.ParentId = null;
        a.Account_Status__c = 'Assigned';                
        insert a;
                
        Opportunity testOpp = new Opportunity(
                                accountid= a.id,
                                Name = 'testOpp',
                                StageName = 'testOpp',
                                CloseDate = Date.today().addDays(2),
                                Unified_Comm_Collaboration_Estimated__c =20);
        insert testOpp;
    
        Quote__c testQuote = new Quote__c(
                                Opportunity_Name__c = testOpp.Id);
        insert testQuote;
        
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(testQuote);        
        RemoteService controller = new RemoteService(stdController);        
        controller.loadDataAction();
        
        RemoteService.RemoteDataItem test = 
                    new RemoteService.RemoteDataItem('test', 1, Double.valueOf('2'), Double.valueOf('2'));
    }
    
    static testMethod void loadDataWithOpp() {
    
        String JSONString = '{"RemoteDataItem":{' + 
        '"service":"TestService",' +
        '"id":"12345",' +
        '"quantity":"10",' +
        '"discount":"5",' +
        '"price":"50"}}';
        
        Account a = new Account();
        a.name= 'A45cc56ou67ntTe57804st';
        a.ParentId = null;
        a.Account_Status__c = 'Assigned';                
        insert a;
        
        Billing_Account__c billAcc = new Billing_Account__c(Account__c = a.id, Billing_Number__c = '100');
        insert billAcc;
        
        Opportunity testOpp = new Opportunity(
                                accountid= a.id,
                                Billing_Account__c = billAcc.id,
                                Name = 'testOpp',
                                StageName = 'testOpp',
                                CloseDate = Date.today().addDays(2),
                                Unified_Comm_Collaboration_Estimated__c =20);
        insert testOpp;
    
        Quote__c testQuote = new Quote__c(
                                Opportunity_Name__c = testOpp.Id);
        insert testQuote;
        
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(testQuote);
        
        RemoteService controller = new RemoteService(stdController);
        
        System.assertNotEquals(0, RemoteService.loadData('test').size());
        
        controller.loadDataAction();
        
        controller.insertQuoteLineItemsList();
        RemoteService.parseResponce(JSONString);
    }
}