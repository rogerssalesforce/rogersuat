<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Data package shares with other organizations</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Class_Name__c</fullName>
        <description>The APEX class object that serialized into Payload__c</description>
        <externalId>false</externalId>
        <label>Class Name</label>
        <length>50</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Last_Modified_TImestamp__c</fullName>
        <externalId>false</externalId>
        <formula>LastModifiedDate</formula>
        <label>Last Modified TImestamp</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Payload__c</fullName>
        <description>The JSON representation of the OBJECT (Class_Name__c).
Payload__c = JSON.serialize(OBJECT)</description>
        <externalId>false</externalId>
        <label>Payload</label>
        <length>5000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>Source_Id__c</fullName>
        <description>The source (Lead/Account/Opportunity) object Id from the source org</description>
        <externalId>false</externalId>
        <label>Source Id</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Source_Name__c</fullName>
        <description>The source application name (e.g. Vicinity, OutRank)</description>
        <externalId>false</externalId>
        <label>Source Name</label>
        <length>50</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Target_Connection_Name__c</fullName>
        <description>The target Salesforce connection name</description>
        <externalId>false</externalId>
        <label>Target Connection Name</label>
        <length>100</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Target_Id__c</fullName>
        <description>The object Id created from this Hermes Queue</description>
        <externalId>false</externalId>
        <label>Target Id</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Target_Name__c</fullName>
        <description>The target application name</description>
        <externalId>false</externalId>
        <label>Target Name</label>
        <length>50</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Hermes Queue</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Source_Name__c</columns>
        <columns>Source_Id__c</columns>
        <columns>Target_Connection_Name__c</columns>
        <columns>Target_Name__c</columns>
        <columns>Target_Id__c</columns>
        <columns>Last_Modified_TImestamp__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>HQ{0000000}</displayFormat>
        <label>Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Hermes Queues</pluralLabel>
    <searchLayouts>
        <searchResultsAdditionalFields>Source_Name__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
