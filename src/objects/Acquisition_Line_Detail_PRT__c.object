<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>PRT Request</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Data_Only_BB_Smartphone__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Please specify the amount of Data Only (BB/Smartphone) lines the account currently has</inlineHelpText>
        <label>Data Only (BB/Smartphone)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Data_Only_M2M__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Please specify the number of Data Only M2M lines the account has</inlineHelpText>
        <label>Data Only M2M</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Data_Only_Tablet_MBB__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Please specify the number of Data Only (Tablet/MBB) the account has</inlineHelpText>
        <label>Data Only (Tablet/MBB)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Other_Provider__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Please specify the provider of the account. This field is only enabled if Other (Specify in Textbox) is selected as the Provider.</inlineHelpText>
        <label>Other Provider</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PRTForm__c</fullName>
        <externalId>false</externalId>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Acquisition Line Details</relationshipLabel>
        <relationshipName>Acquisition_Line_Details</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Provider__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Please select the provider the account is currently subscribed to</inlineHelpText>
        <label>Provider</label>
        <picklist>
            <picklistValues>
                <fullName>Rogers</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Bell</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Telus</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Other (Specify in Textbox)</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Voice_Only__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Please specify the number of Voice Only lines the account has</inlineHelpText>
        <label>Voice Only</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Voice_and_Data_BB__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Please specify the amount of Voice and Data (BB) lines the account has</inlineHelpText>
        <label>Voice and Data (BB)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Voice_and_Data_Smartphone__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Please specify the number of Voice and Data (Smartphone) lines the account has</inlineHelpText>
        <label>Voice and Data (Smartphone)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Acquisition Line Detail</label>
    <nameField>
        <displayFormat>A-{0000}</displayFormat>
        <label>Acquisition Line Detail Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Acquisition Line Details</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>Blank_Other_Provider</fullName>
        <active>true</active>
        <description>PRT Request</description>
        <errorConditionFormula>AND(PRTForm__r.RecordType.Name = &quot;PRT Request&quot;, ISPICKVAL( Provider__c ,&quot;Other (Specify in Textbox)&quot;), ISBLANK( Other_Provider__c))</errorConditionFormula>
        <errorDisplayField>Other_Provider__c</errorDisplayField>
        <errorMessage>Please provide the name of the other provider</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Other_Provider_Not_Other_Chosen</fullName>
        <active>true</active>
        <description>PRT Request</description>
        <errorConditionFormula>AND(PRTForm__r.RecordType.Name = &quot;PRT Request&quot;, NOT(ISPICKVAL( Provider__c ,&quot;Other (Specify in Textbox)&quot;)), NOT(ISBLANK( Other_Provider__c)))</errorConditionFormula>
        <errorDisplayField>Other_Provider__c</errorDisplayField>
        <errorMessage>An existing provider has already been selected</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>PRT_Case_Related_List_Edit</fullName>
        <active>true</active>
        <description>PRT Case</description>
        <errorConditionFormula>NOT(OR(ISPICKVAL(PRTForm__r.Status,&quot;New&quot;),ISPICKVAL(PRTForm__r.Status, &quot;Additional Information Required&quot;)))</errorConditionFormula>
        <errorMessage>You do not have permission to edit this record</errorMessage>
    </validationRules>
</CustomObject>
