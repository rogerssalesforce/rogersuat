<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Track Sales Rep quotas per Product Family</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Churn__c</fullName>
        <externalId>false</externalId>
        <label>Churn</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Forecast_Month__c</fullName>
        <description>Quota month - Year Month format</description>
        <externalId>false</externalId>
        <inlineHelpText>YYYYMM</inlineHelpText>
        <label>Sales Month</label>
        <precision>6</precision>
        <required>true</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OwnerIDMonthFamily__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>ID when inserting/upserting quota data</description>
        <externalId>true</externalId>
        <label>OwnerIDMonthFamily</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>OwnerMonthFamily__c</fullName>
        <externalId>false</externalId>
        <formula>Owner:User.FirstName+ Owner:User.LastName+TEXT(Forecast_Month__c)+TEXT(Quota_Family__c)</formula>
        <label>OwnerMonthFamily</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Quota_Family__c</fullName>
        <externalId>false</externalId>
        <label>Quota Family</label>
        <picklist>
            <picklistValues>
                <fullName>ABS Connectivity</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ABS - Hardware</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ABS - Software</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Cable - TV</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Cable - Internet</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Cable - Phone</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Managed and Professional Services</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>M2M Reseller</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Wireless - Voice</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Wireless - All Data</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Revenue__c</fullName>
        <externalId>false</externalId>
        <label>Revenue</label>
        <precision>17</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>Sales Quota</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>OWNER.FIRST_NAME</columns>
        <columns>OWNER.LAST_NAME</columns>
        <columns>Quota_Family__c</columns>
        <columns>Forecast_Month__c</columns>
        <columns>Quantity__c</columns>
        <columns>Revenue__c</columns>
        <columns>Churn__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>SQ{0000}</displayFormat>
        <label>Sales Quota Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Sales Quotas</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
