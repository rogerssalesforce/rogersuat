<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>BME - A Defect represents a failure of the application to produce the expected results, as identified by a Test Case Step. Defects are associated to Test Case Steps and can be associated back to Requirements via the Defect Requirement Join object.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Actual_Closed_Date__c</fullName>
        <description>Field used to capture the date when the defect was set to &apos;Closed&apos;</description>
        <externalId>false</externalId>
        <inlineHelpText>Date the Defect was Closed</inlineHelpText>
        <label>Actual Closed Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Actual_Result__c</fullName>
        <externalId>false</externalId>
        <label>Actual Result</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Age__c</fullName>
        <externalId>false</externalId>
        <formula>TODAY() -  DATEVALUE( CreatedDate )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Age</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Assigned_to__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Assigned to</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Defects</relationshipName>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Closed_Date__c</fullName>
        <externalId>false</externalId>
        <label>Expected Closed Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Defect_Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>New</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Submitted</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>In Progress</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Fixed</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rejected</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Deferred</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Reopened</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Converted to Change Request</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Needs Discussion</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Design</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MR Approved</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Dev. in Progress</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Ready for Staging</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>User Testing Completed</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Ready for UAT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Deployed into UAT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Validated in UAT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Failed in UAT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Deployed into Production</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Failed in Production</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Closed</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Detailed_Description__c</fullName>
        <externalId>false</externalId>
        <label>Detailed Description</label>
        <length>32000</length>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Expected_Result__c</fullName>
        <externalId>false</externalId>
        <label>Expected Result</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Impacted_Object__c</fullName>
        <externalId>false</externalId>
        <label>Impacted Objects</label>
        <picklist>
            <picklistValues>
                <fullName>Activities</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Campaigns</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Leads</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Accounts</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Contacts</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Opportunities</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Quotes</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Contracts</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Cases</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Solutions</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Open_Date__c</fullName>
        <externalId>false</externalId>
        <label>Open Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Relevant_User_Profiles__c</fullName>
        <externalId>false</externalId>
        <label>Relevant User Profiles</label>
        <picklist>
            <picklistValues>
                <fullName>Rogers ABS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Business Care</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Business Engagement</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Business Support</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Cable Sales Manager</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Cable Sales Rep</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Contract Team</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Credit Op</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers CRM Admin</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Dealer Management</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Enterprise Sales Manager</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Enterprise Sales Rep</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Executive</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Marketing</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers NIS Sales Manager</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers NIS Sales Rep</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers NPC</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Partner Dealer</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Resellers Sales Manager</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Resellers Sales Rep</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Restricted Sales Rep</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rogers Retention</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>true</sorted>
        </picklist>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Requirement__c</fullName>
        <externalId>false</externalId>
        <formula>Test_Case__r.Requirement__r.Name</formula>
        <label>Related Requirement</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Resolution__c</fullName>
        <description>Field to capture the resolution for the defect</description>
        <externalId>false</externalId>
        <inlineHelpText>Summary of the final resolution to this defect</inlineHelpText>
        <label>Final Resolution</label>
        <length>32000</length>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Severity__c</fullName>
        <externalId>false</externalId>
        <label>Severity</label>
        <picklist>
            <picklistValues>
                <fullName>High</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Medium</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Low</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Status_Update__c</fullName>
        <description>Holds pertinent status related information</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter any status updates in this field</inlineHelpText>
        <label>Comment</label>
        <length>32768</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Steps_to_Reproduce__c</fullName>
        <externalId>false</externalId>
        <label>Steps to Reproduce</label>
        <length>32768</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Summary__c</fullName>
        <externalId>false</externalId>
        <label>Summary</label>
        <length>1000</length>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Test_Case_Module__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(Test_Case__r.Module__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Test Case Module</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Test_Case_Step__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Test Case Step</label>
        <referenceTo>Test_Case_Step__c</referenceTo>
        <relationshipLabel>Defects</relationshipLabel>
        <relationshipName>Defects</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Test_Case__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Test Case</label>
        <referenceTo>Test_Case__c</referenceTo>
        <relationshipLabel>Defects</relationshipLabel>
        <relationshipName>Defects</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Tester__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Defects1</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <picklist>
            <picklistValues>
                <fullName>System Defect</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Integration Defect</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>FDD Defect</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Test Script Defect</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>External System Defect</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Defect</label>
    <listViews>
        <fullName>Ad_Hoc</fullName>
        <columns>NAME</columns>
        <columns>Summary__c</columns>
        <columns>Defect_Status__c</columns>
        <columns>Severity__c</columns>
        <columns>Type__c</columns>
        <columns>Assigned_to__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>Open_Date__c</columns>
        <columns>Test_Case__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CREATED_DATE</field>
            <operation>greaterOrEqual</operation>
            <value>10/1/2013 12:00 AM</value>
        </filters>
        <filters>
            <field>CREATED_DATE</field>
            <operation>lessOrEqual</operation>
            <value>1/1/2014 12:00 AM</value>
        </filters>
        <filters>
            <field>Defect_Status__c</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </filters>
        <label>Ad Hoc</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All_Defects</fullName>
        <columns>NAME</columns>
        <columns>Summary__c</columns>
        <columns>Defect_Status__c</columns>
        <columns>Severity__c</columns>
        <columns>Type__c</columns>
        <columns>Assigned_to__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>Open_Date__c</columns>
        <columns>Test_Case__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All Defects</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All_Fixed_Defects</fullName>
        <columns>NAME</columns>
        <columns>Defect_Status__c</columns>
        <columns>Summary__c</columns>
        <columns>Severity__c</columns>
        <columns>Assigned_to__c</columns>
        <columns>Type__c</columns>
        <columns>Open_Date__c</columns>
        <columns>Test_Case__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Defect_Status__c</field>
            <operation>equals</operation>
            <value>Fixed</value>
        </filters>
        <label>All Fixed Defects</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All_In_Progress_Defects</fullName>
        <columns>NAME</columns>
        <columns>Defect_Status__c</columns>
        <columns>Summary__c</columns>
        <columns>Severity__c</columns>
        <columns>Type__c</columns>
        <columns>Open_Date__c</columns>
        <columns>Assigned_to__c</columns>
        <columns>Test_Case__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Defect_Status__c</field>
            <operation>notEqual</operation>
            <value>Rejected,Closed,Fixed</value>
        </filters>
        <filters>
            <field>Type__c</field>
            <operation>notEqual</operation>
            <value>FDD Defect</value>
        </filters>
        <label>All In Progress Defects</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All_Open_Defects</fullName>
        <columns>NAME</columns>
        <columns>Summary__c</columns>
        <columns>Defect_Status__c</columns>
        <columns>Severity__c</columns>
        <columns>Type__c</columns>
        <columns>Assigned_to__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>Open_Date__c</columns>
        <columns>Test_Case__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Defect_Status__c</field>
            <operation>notEqual</operation>
            <value>Closed,Rejected,Converted to Change Request,Deployed into Production</value>
        </filters>
        <label>All Open Defects</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All_Open_Defects_Not_Fixed</fullName>
        <columns>NAME</columns>
        <columns>Summary__c</columns>
        <columns>Defect_Status__c</columns>
        <columns>Severity__c</columns>
        <columns>Type__c</columns>
        <columns>Assigned_to__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>Open_Date__c</columns>
        <columns>Test_Case__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Defect_Status__c</field>
            <operation>contains</operation>
            <value>Submitted,In Progress,Reopened</value>
        </filters>
        <label>All Open Defects - Not Fixed</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All_Open_FDD_Defects</fullName>
        <columns>NAME</columns>
        <columns>Summary__c</columns>
        <columns>Defect_Status__c</columns>
        <columns>Severity__c</columns>
        <columns>Resolution__c</columns>
        <columns>Open_Date__c</columns>
        <columns>Test_Case__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Defect_Status__c</field>
            <operation>notEqual</operation>
            <value>Rejected,Closed,Fixed</value>
        </filters>
        <filters>
            <field>Type__c</field>
            <operation>equals</operation>
            <value>FDD Defect</value>
        </filters>
        <label>All Open FDD Defects</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All_Rejected_Defects</fullName>
        <columns>NAME</columns>
        <columns>Defect_Status__c</columns>
        <columns>Summary__c</columns>
        <columns>Severity__c</columns>
        <columns>Assigned_to__c</columns>
        <columns>Type__c</columns>
        <columns>Open_Date__c</columns>
        <columns>Test_Case__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Defect_Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </filters>
        <filters>
            <field>Type__c</field>
            <operation>notEqual</operation>
            <value>FDD Defect</value>
        </filters>
        <label>All Rejected Defects</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All_Submitted_Defects</fullName>
        <columns>NAME</columns>
        <columns>Summary__c</columns>
        <columns>Open_Date__c</columns>
        <columns>Assigned_to__c</columns>
        <columns>Severity__c</columns>
        <columns>Type__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>Defect_Status__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Defect_Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </filters>
        <label>All Submitted Defects</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Closed_System_Defects</fullName>
        <columns>NAME</columns>
        <columns>Defect_Status__c</columns>
        <columns>Summary__c</columns>
        <columns>Type__c</columns>
        <columns>Assigned_to__c</columns>
        <columns>Severity__c</columns>
        <columns>Open_Date__c</columns>
        <columns>Actual_Closed_Date__c</columns>
        <columns>Test_Case__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Defect_Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </filters>
        <filters>
            <field>Type__c</field>
            <operation>equals</operation>
            <value>System Defect</value>
        </filters>
        <label>Closed System Defects</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>EBU_Consolidation_Unresolved_defects</fullName>
        <columns>NAME</columns>
        <columns>Test_Case_Module__c</columns>
        <columns>Test_Case__c</columns>
        <columns>Age__c</columns>
        <columns>Defect_Status__c</columns>
        <columns>Severity__c</columns>
        <columns>Type__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>Summary__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Defect_Status__c</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </filters>
        <filters>
            <field>Open_Date__c</field>
            <operation>greaterThan</operation>
            <value>1/1/2015</value>
        </filters>
        <label>EBU Consolidation - Unresolved defects</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>My_Defects</fullName>
        <columns>NAME</columns>
        <columns>Summary__c</columns>
        <columns>Defect_Status__c</columns>
        <columns>Severity__c</columns>
        <columns>Resolution__c</columns>
        <columns>Open_Date__c</columns>
        <columns>Test_Case__c</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Mine</filterScope>
        <label>My Defects</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>My_Open_Defects</fullName>
        <columns>NAME</columns>
        <columns>Summary__c</columns>
        <columns>Defect_Status__c</columns>
        <columns>Severity__c</columns>
        <columns>Resolution__c</columns>
        <columns>Open_Date__c</columns>
        <columns>Test_Case__c</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Mine</filterScope>
        <filters>
            <field>Defect_Status__c</field>
            <operation>notEqual</operation>
            <value>Closed,Rejected</value>
        </filters>
        <label>My Open Defects</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Unassigned_Defects</fullName>
        <columns>NAME</columns>
        <columns>Defect_Status__c</columns>
        <columns>Summary__c</columns>
        <columns>Severity__c</columns>
        <columns>Assigned_to__c</columns>
        <columns>Type__c</columns>
        <columns>Open_Date__c</columns>
        <columns>Test_Case__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Defect_Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </filters>
        <filters>
            <field>Type__c</field>
            <operation>notEqual</operation>
            <value>FDD Defect</value>
        </filters>
        <label>Unassigned Defects</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Waiting_for_Retest_Defects</fullName>
        <columns>NAME</columns>
        <columns>Summary__c</columns>
        <columns>Defect_Status__c</columns>
        <columns>Severity__c</columns>
        <columns>Assigned_to__c</columns>
        <columns>Open_Date__c</columns>
        <columns>Test_Case__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Defect_Status__c</field>
            <operation>equals</operation>
            <value>Fixed</value>
        </filters>
        <filters>
            <field>Type__c</field>
            <operation>notEqual</operation>
            <value>FDD Defect</value>
        </filters>
        <label>Waiting for Retest Defects</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>D-{00000}</displayFormat>
        <label>Defect ID</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Defects</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Summary__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Severity__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Defect_Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Resolution__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Open_Date__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Summary__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Defect_Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Severity__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Resolution__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Open_Date__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Summary__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Defect_Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Severity__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Resolution__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Open_Date__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Summary__c</searchFilterFields>
        <searchFilterFields>Severity__c</searchFilterFields>
        <searchFilterFields>Defect_Status__c</searchFilterFields>
        <searchFilterFields>Open_Date__c</searchFilterFields>
        <searchResultsAdditionalFields>Summary__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Defect_Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Severity__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Resolution__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Open_Date__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Force_Final_Resolution_on_Closed_Defect</fullName>
        <active>true</active>
        <description>If Status = Closed, Final Resolution must not be Null</description>
        <errorConditionFormula>AND(  ISPICKVAL(Defect_Status__c, &apos;Closed&apos;), ISBLANK( Resolution__c ))</errorConditionFormula>
        <errorDisplayField>Resolution__c</errorDisplayField>
        <errorMessage>Please enter a Final Resolution before closing this Defect.</errorMessage>
    </validationRules>
</CustomObject>
