<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <content>addSiteRedirect</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Objects within this table will act as the parent object for a series of Prospective Sites. Prospective Sites that have the same locations will have the same ServiceableLocation.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Access_Type_Group__c</fullName>
        <externalId>false</externalId>
        <label>Access Type Group</label>
        <picklist>
            <picklistValues>
                <fullName>Fibre</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Cable</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NAP</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NNI</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CNI</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Wireless</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Other Access</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ET1</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>T1 TDM</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DSL</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Wireless Zone 1</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Wireless Zone 2</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Wireless Zone 3</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Wireless Zone Remote</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Access_Type__c</fullName>
        <externalId>false</externalId>
        <label>Access Type</label>
        <picklist>
            <picklistValues>
                <fullName>OFF NET Rogers Build</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>OFF NET 3rd Party</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NEARNET - BUILDING TYPE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NEARNET</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ON NET</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BUILD IN PROGRESS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NON STANDARD</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Build_Date__c</fullName>
        <description>The date in which the Serviceable location went from Near Net to On Net.  Currently, both ON Net and Near Net sites are listed as ON Net.</description>
        <externalId>false</externalId>
        <label>Build Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>CLLI_Code__c</fullName>
        <description>CLLI Code for a serviceable location that will allow the Serviceable Location to be matched to one or many difference postal codes.</description>
        <externalId>true</externalId>
        <label>CLLI Code</label>
        <length>8</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CNINNI__c</fullName>
        <description>Indicates whether this Serviceable Location is a CNI or an NNI</description>
        <externalId>false</externalId>
        <label>CNI NNI</label>
        <picklist>
            <picklistValues>
                <fullName>CNI</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NNI</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>City__c</fullName>
        <description>Place holder for City for the location of the serviceable location.</description>
        <externalId>false</externalId>
        <label>City</label>
        <length>75</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Country__c</fullName>
        <defaultValue>&quot;Canada&quot;</defaultValue>
        <description>Name of the country where the site is located</description>
        <externalId>false</externalId>
        <inlineHelpText>Name of the country where the site is located</inlineHelpText>
        <label>Country</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Display_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Suite_Floor__c &amp; &apos; &apos; &amp;  Street_Number__c &amp; &apos; &apos; &amp;  Street_Name__c &amp; &apos; &apos; &amp;  Street_Type__c &amp;  IF(Street_Direction__c==null, &apos;&apos;, &apos; &apos; +Street_Direction__c) &amp; &apos;, &apos; &amp; City__c &amp; &apos; &apos; &amp;  Province_Code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Display Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>External_ID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>External ID</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Legacy_RBS_CreatedById_Emp__c</fullName>
        <externalId>false</externalId>
        <label>Legacy_RBS_CreatedById_Emp</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Legacy_RBS_LastModifiedById_Emp__c</fullName>
        <externalId>false</externalId>
        <label>Legacy_RBS_LastModifiedById_Emp</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Legacy_RBS_OwnerId_Emp__c</fullName>
        <externalId>false</externalId>
        <label>Legacy_RBS_OwnerId_Emp</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Legacy_RBS_SrvcblLctnId__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>Legacy_RBS_SrvcblLctnId</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>LocationKey__c</fullName>
        <description>Formula field used for matching - should be a unique field</description>
        <externalId>false</externalId>
        <formula>Suite_Floor__c &amp; Street_Number__c &amp; Street_Name__c &amp; Street_Type__c &amp; Street_Direction__c &amp; City__c &amp; Province_Code__c &amp; CLLI_Code__c &amp; TEXT( CNINNI__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>LocationKey</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Mapnet_ID__c</fullName>
        <description>Identify the Mapnet index for future linking and integration</description>
        <externalId>false</externalId>
        <inlineHelpText>MapNet Site Index</inlineHelpText>
        <label>Mapnet ID</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Network_Access_Type__c</fullName>
        <description>Describes what network the access will be on (RBS, Blink, Atria) This field will be available on the CIF Sites for each site.</description>
        <externalId>false</externalId>
        <label>Network Access Type</label>
        <picklist>
            <picklistValues>
                <fullName>ACI</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ATRIA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ATRIA - AH</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ATRIA - PE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BLINK</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BLINK - RED</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BLINK - PE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BLINK - PEC</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>COMPTON</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>FCI</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>GT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ROGERS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ROGERS - 50M</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ROGERS - AH</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ROGERS - EON</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ROGERS - RPATS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ROGERS - PE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ROGERS - GT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WIRELESS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>RBS</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Postal_Code__c</fullName>
        <externalId>true</externalId>
        <label>Postal Code</label>
        <length>7</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Province_Code__c</fullName>
        <description>Place holder for Province Code for the location of the serviceable location.</description>
        <externalId>false</externalId>
        <label>Province</label>
        <length>3</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Region_Access_Type__c</fullName>
        <externalId>false</externalId>
        <formula>IF(INCLUDES(Access_Type__c, &quot;NEARNET&quot;), &quot;NEARNET&quot;,
IF(INCLUDES(Access_Type__c, &quot;NEARNET - BUILDING TYPE&quot;), &quot;NEARNET&quot;, 
IF(INCLUDES(Access_Type__c, &quot;OFF NET Rogers Build&quot;), &quot;OFFNET&quot;, 
IF(INCLUDES(Access_Type__c, &quot;OFF NET 3rd Party&quot;), &quot;OFFNET&quot;, 
IF(INCLUDES(Access_Type__c, &quot;ON NET&quot;), &quot;ONNET&quot;, 
IF(INCLUDES(Access_Type__c, &quot;ON NET - ACCELERATED&quot;), &quot;ONNET&quot;,&quot;&quot;))))))
+ &quot;-&quot;
+ Region__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Region Access Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Region__c</fullName>
        <description>Identify the Region serve by Customer Provisioning and identify the SOI time.</description>
        <externalId>false</externalId>
        <label>Region</label>
        <length>14</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Services__c</fullName>
        <description>Each address is classified as &quot;Internet Services Only&quot; or &quot;Multiple Site Services&quot;. 
(Note: Future option could include &quot;Cable Services&quot;)</description>
        <externalId>false</externalId>
        <inlineHelpText>Each address is classified as &quot;Internet Services Only&quot; or &quot;Multiple Site Services&quot;.</inlineHelpText>
        <label>Services</label>
        <picklist>
            <picklistValues>
                <fullName>INTERNET SERVICES ONLY</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MULTIPLE SITE SERVICES</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Street_Direction__c</fullName>
        <description>Place holder for Street Direction for the location of the serviceable location.</description>
        <externalId>false</externalId>
        <label>Street Direction</label>
        <length>5</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Street_Name__c</fullName>
        <description>Place holder for Street Name for the location of the serviceable location.</description>
        <externalId>false</externalId>
        <label>Street Name</label>
        <length>100</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Street_Number__c</fullName>
        <description>Place holder for Street Numberfor the location of the serviceable location.</description>
        <externalId>false</externalId>
        <label>Street Number</label>
        <length>8</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Street_Type__c</fullName>
        <description>Place holder for Street Type for the location of the serviceable location.</description>
        <externalId>false</externalId>
        <label>Street Type</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Suite_Floor__c</fullName>
        <description>Place holder for Suite Floor for the location of the serviceable location.</description>
        <externalId>false</externalId>
        <label>Suite/Floor</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>X18_Char_Id__c</fullName>
        <externalId>false</externalId>
        <formula>CASESAFEID(Id)</formula>
        <label>18 Char Id</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>eBonding_Key__c</fullName>
        <externalId>false</externalId>
        <formula>SUBSTITUTE(UPPER(Suite_Floor__c &amp; Street_Number__c &amp; Street_Name__c &amp; Street_Type__c &amp; Street_Direction__c &amp; City__c &amp; Province_Code__c &amp;  Country__c) , &quot; &quot;, &quot;&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>eBonding Key</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>ServiceableLocation</label>
    <nameField>
        <label>ServiceableLocation Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>ServiceableLocations</pluralLabel>
    <searchLayouts>
        <searchResultsAdditionalFields>Network_Access_Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Access_Type_Group__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Access_Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Services__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Region_Access_Type__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Read</sharingModel>
</CustomObject>
